#ifndef PROCESSMANAGER_H
#define PROCESSMANAGER_H

#include "managers/librarymanager.h"
#include "managers/settingsmanager.h"
#include "delegates/filepickerdelegate.h"
#include "delegates/processorlistdelegate.h"


#include <QObject>
#include <QDir>
#include <QStandardItemModel>

namespace Ui {
class MainWindow;
}

class processManager : public QObject
{
    Q_OBJECT
    void setSelected(QModelIndex index);
    void updateUi();
    void loadDir();
    void sortFiles();
    void fileGroupButtonsVisibilityUpdate();
    void lastImageFindAndCache();
    void fileGroupBottomClick();
    void listWidgetsSetIndex(QModelIndex index);
public:
    explicit processManager(QObject *parent = 0);

    class Selected
        {
        public:
            Selected(){}
            QString title;
            QString path;
            QString thumbNail;
            QModelIndex Index;
            QStringList fileList;
            int id;
            int isPano;

        };

    class Files
        {
        public:
            Files(){}
            void reset(){ origiAll.clear(); panorama.clear(); hdr.clear(); postProcess.clear(); ev0.clear(); ev1.clear(); ev2.clear(); }
            QStringList origiAll;
            QStringList ev0;
            QStringList ev1;
            QStringList ev2;
            QStringList panorama;
            QStringList hdr;
            QStringList postProcess;
        };

    // instantiated classes
    Files       *files;
    Selected    *selected;
    filePickerDelegate *fDelegate;
    processorListDelegate *listDelegate;
    filePickerDelegate *browserDelegate;

    QSqlQueryModel *worxModel;
    QString defaultQuery;
    QString droppedFolder;

    void attachUi(Ui::MainWindow *u);
    void attachLibMan(libraryManager *l);

    QStandardItemModel *fileModel;
    QDir *dir;
    libraryManager *libMan;
    settingsManager *setMan;
    Ui::MainWindow *ui;
    void initConnections();
    void feedFileList(QStringList fls);
    void init();
    QString setCache();
    void selectByTitle(QString title);
    QStringList fList;
    void attachSetMan(settingsManager *s);
    QStringList filesToArg(QStringList list, QString ext);
signals:

    void openWithPS(QStringList arg);
    void openWithPhotoMatix(QStringList arg);
    void openWithPTGui(QStringList arg);

public slots:

    void fileGroupSelector();
    void select(QModelIndex index);
    void dialog_addNew();
    void dialog_clonePts();
    void deleteProject();
    void dialog_moveToArchive();
    void thumbContextMenu(const QPoint &pos);
    void treeContextMenu(const QPoint &pos);
    void animateSideBar();
    void listBack();
    void treeItemDBLClick(QModelIndex index);
    void listTo();
    void pathToClipboard();
    void folderOpen();
    void dialog_properties();
    void filesToHDR();
    void filesToPS();
    void filesToPTGui();
    void filesToPTGuiBatch();
    void loadFileInfo(QModelIndex index);
    void fileOpen();
    void fileEdit();
private slots:
    void fileDoubleClick(QModelIndex index);
};

#endif // PROCESSMANAGER_H
