#include "mainwindow.h"
#include <QApplication>
#include <QSplashScreen>

void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    Q_UNUSED(type);
    Q_UNUSED(context);
    Q_UNUSED(msg);
}

int main(int argc, char *argv[])
{
    // DISABLE QDEBUG OUTPUT !!!
            qInstallMessageHandler(myMessageOutput);
        //

    QApplication a(argc, argv);

    /*
    QPixmap pixmap(":/icons/icons/splash-1.jpg");
    QSplashScreen *splash= new QSplashScreen(pixmap.scaled(640,480,Qt::KeepAspectRatio , Qt::SmoothTransformation),Qt::WindowStaysOnTopHint);
    splash->show();
    splash->showMessage(QObject::tr("Setting up the main window..."), Qt::TopRightCorner, Qt::white);
    */


    /*
    QPixmap pixmap(":/icons/icons/splash-1.jpg");

    QSplashScreen splash(pixmap.scaled(320,240,Qt::KeepAspectRatio , Qt::SmoothTransformation) );
    splash.show();
    */
    QFile file(":/stylesheets/stylesheets/pscs6.css");
        if(file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            a.setStyleSheet(file.readAll());
            file.close();
        }


    MainWindow w;
    //w.splash=splash;
    w.show();
    //w.showMaximized();

    //splash->close();
    return a.exec();
}


