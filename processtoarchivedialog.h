#ifndef PROCESSTOARCHIVEDIALOG_H
#define PROCESSTOARCHIVEDIALOG_H

#include <QDialog>

namespace Ui {
class processToArchiveDialog;
}

class processToArchiveDialog : public QDialog
{
    Q_OBJECT

public:
    explicit processToArchiveDialog(QWidget *parent = 0);
    ~processToArchiveDialog();
    QString sourceFolder;
    QString targetFolder;
    QString defaultTargetBaseDir;
    QStringList titleList;

    QList<QPair<QString,QString> > moveList;

    void setDefaultBase(QString path);
    void setTitleList(QStringList t);
    void setSource(QString s);
    bool copyRecursively(const QString &from, const QString &to);
private slots:
    void titleToFolderName(QString s);
    void next();
    void copyProgress(quint64 done, quint64 all, qint64 tmr);
private:
    Ui::processToArchiveDialog *ui;
};

#endif // PROCESSTOARCHIVEDIALOG_H
