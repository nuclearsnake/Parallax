#ifndef SMOOTHSCROLLLISTVIEW_H
#define SMOOTHSCROLLLISTVIEW_H

#include <QListView>
#include <QWidget>
#include <QTimer>

class SmoothScrollListView : public QListView
{
    Q_OBJECT
    void wheelEvent(QWheelEvent *event);
    //bool event(QEvent *e);
    //virtual  viewportEvent(QEvent *e);
    //bool viewportEvent(QEvent *e);

public:
    explicit SmoothScrollListView(QWidget *parent = 0);
    QWidget *w;
    QTimer *timer;
    int scrollBuffer;
    int eventCounter;
    int targetPos;
    int counter;
    bool first;

signals:

public slots:

private slots:
    void scrollTimed();
};

#endif // SMOOTHSCROLLLISTVIEW_H
