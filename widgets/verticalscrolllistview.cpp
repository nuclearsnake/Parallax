#include "verticalscrolllistview.h"

#include <QDebug>
#include <QEvent>

#include <QWheelEvent>
#include <QScrollArea>
#include <QScrollBar>

VerticalScrollListView::VerticalScrollListView(QWidget *parent) :
    QListView(parent)
{
}

void VerticalScrollListView::wheelEvent(QWheelEvent *event)
{

    qDebug() << "event" << event;
    horizontalScrollBar()->setValue( horizontalScrollBar()->value() - event->delta() );
}
