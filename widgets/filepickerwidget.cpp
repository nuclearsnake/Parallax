#include "filepickerwidget.h"

//filePickerWidget

#include <QDebug>
#include <QEvent>
#include <QMouseEvent>
#include <QDrag>
#include <QPixmap>
#include <QMimeData>


filePickerWidget::filePickerWidget(QWidget *parent) :
    QListView(parent)
{
}

void filePickerWidget::dragMoveEvent(QDragMoveEvent *e)
{
    qDebug() << e;
}

void filePickerWidget::mousePressEvent(QMouseEvent *event)
{
    qDebug() << event;
    if (event->button() == Qt::LeftButton
             /*&& this->geometry().contains(event->pos())*/ )
        {

             QDrag *drag = new QDrag(this);
             QMimeData *mimeData = new QMimeData;

             //mimeData->setText( this->indexAt(event->pos()).data().toString() );
             QList<QUrl> urls;


             QModelIndex index = this->indexAt(event->pos());
             QString filePath = index.sibling(index.row(),1).data().toString();

             qDebug() << index;
             qDebug() << filePath;

             urls << QUrl::fromLocalFile( filePath );
             QString test = "z:\\!Cache\\20140413-DSC_8746_0.tif.jpg";
             //mimeData->setData("Shell IDList Array",test.toLatin1() );
             mimeData->setUrls(urls);
             QByteArray ba;
             ba = urls.at(0).toString().toLatin1();
             qDebug() << ba;
             //mimeData->setData("FileName","Z:\\Parallax\\imported\\!cache\\1300~1.JPG");

             qDebug() << mimeData;

             drag->setMimeData(mimeData);
             QPixmap pix; // = new QPixmap(this->indexAt(event->pos()).data().toString()) ;
             pix.load(filePath);
             pix = pix.scaled(50,50,Qt::KeepAspectRatio);
             drag->setPixmap(pix);


             //Qt::DropAction dropAction = drag->exec(); //UNUSED ?

         }
}
