#ifndef FILEPICKERWIDGET_H
#define FILEPICKERWIDGET_H

//filePickerWidget

#include <QListView>

class filePickerWidget : public QListView
{
    Q_OBJECT
public:
    explicit filePickerWidget(QWidget *parent = 0);

signals:

public slots:

protected:
    void dragMoveEvent(QDragMoveEvent *e);
    void mousePressEvent(QMouseEvent *event);
};

#endif // FILEPICKERWIDGET_H
