#ifndef BARGRAPHWIDGET_H
#define BARGRAPHWIDGET_H

#include <QWidget>
#include <QPainter>
#include <QDebug>
#include <QVector>

class BarGraphWidget : public QWidget
{
    Q_OBJECT
    void paintEvent(QPaintEvent *event);
public:
    explicit BarGraphWidget(QWidget *parent = 0);
    QMap<QString,int> values;

signals:

public slots:

};

#endif // BARGRAPHWIDGET_H
