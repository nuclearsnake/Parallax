#include "smoothscrolllistview.h"

#include <QDebug>
#include <QEvent>

#include <QWheelEvent>
#include <QScrollArea>
#include <QScrollBar>

SmoothScrollListView::SmoothScrollListView(QWidget *parent) :
    QListView(parent)
{
    w = parent;
    scrollBuffer = 0;
    timer = new QTimer(this);
    timer->setInterval(10);
    timer->stop();
    first = true;
    eventCounter = 0;
    counter = 0;

    connect ( timer     , SIGNAL(timeout())     , this  , SLOT(scrollTimed()) );
}

void SmoothScrollListView::scrollTimed()
{
    //qDebug() << "timeout";
    //scrollBuffer += 10;
    //qDebug() << "  scrollBuffer" << scrollBuffer;
    //verticalScrollBar()->setValue( targetPos );

    int delta = verticalScrollBar()->value() - targetPos;
    //qDebug()  << "delta" << delta;



    float sp = 10;
    ++counter;

    /*
    if ( abs(delta) >  300 ) sp =  30;
    if ( abs(delta) > 1000 ) sp = abs(delta) / 10 ;
    //if ( abs(delta) > 2000 ) sp = 200;
    if ( abs(delta) <   30 ) sp =   1;

     qDebug()  << verticalScrollBar()->value() << "==>" << targetPos << "==>" << delta << "==>" << sp;
     */
    sp = abs(delta) / 5 ;
        if ( abs(delta) <   5 ) sp =   1;

        /*
         qDebug() << "             #" << counter;
         qDebug() << "  current pos:"  << verticalScrollBar()->value();
         qDebug() << "   tagret pos:"  << targetPos ;
         qDebug() << "        delta:"  << delta ;
         qDebug() << "           sp:"  << sp;
         */

    if (delta < 0 ) verticalScrollBar()->setValue(verticalScrollBar()->value()+sp);
    if (delta > 0 ) verticalScrollBar()->setValue(verticalScrollBar()->value()-sp);
    if (delta == 0) { timer->stop() ; first=true; }
}

/*
bool SmoothScrollListView::event(QEvent *e)
{
    //qDebug() << "EVENT:" << e;
    //e->ignore();
    eventCounter++;
    qDebug() << QString::number(eventCounter).rightJustified(4,' ') << e;
    //e->ignore();
    return QWidget::event(e);
}
*/


/*
bool SmoothScrollListView::viewportEvent(QEvent *e)
{
    eventCounter++;

    qDebug() << QString::number(eventCounter).rightJustified(4,' ') << e;
    qDebug() << e->type();

    // WHEEL event (31)
    if ( e->type() == 31 )
        {
            QWheelEvent *event = static_cast<QWheelEvent *>(e);

            if (first) targetPos = verticalScrollBar()->value() - event->delta() / 1;
                else targetPos = targetPos - event->delta() / 1;

            if (targetPos<0) targetPos =0;
            if (targetPos>verticalScrollBar()->maximum()) targetPos=verticalScrollBar()->maximum();
            qDebug() << "MAX" << verticalScrollBar()->maximum();
            timer->start();

            first = false;

            //qDebug()  << "WHEEL event" << QWheelEvent::QEvent(e).delta();
            return true;
        }

    // PAINT event (12)
    if ( e->type() == 12 )
        {
            qDebug() << "paint event" << verticalScrollBar()->value();
        }

    return QAbstractScrollArea::viewportEvent(e);
}

*/


void SmoothScrollListView::wheelEvent(QWheelEvent *event)
{

    if (first) targetPos = verticalScrollBar()->value() - event->delta() / 1;
        else targetPos = targetPos - event->delta() / 1;
    if (targetPos<0) targetPos =0;
    if (targetPos>verticalScrollBar()->maximum()) targetPos=verticalScrollBar()->maximum();
    timer->start();

    first = false;



}

