#include "bargraphwidget.h"

BarGraphWidget::BarGraphWidget(QWidget *parent) :
    QWidget(parent)
{
}


void BarGraphWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    qDebug() << "PAINTEVENT!";
    qDebug() << this->width();
    qDebug() << values.count();
    QPainter painter(this);
    painter.setPen(QColor("#777"));
    painter.setBrush(QColor("#777"));
    /*
    painter.drawLine(1,1,this->width(),this->height());
    painter.drawLine(1,this->height(),this->width(),1);
    painter.drawRect(10,this->height()-110,10,100);
    */

    int i;
    int gap = 6;
    int barWidth = (this->width() - gap*values.count() ) / values.count();
    int barMaxVal = 0;

    // get maximum value
    foreach (int v,values)
        {
            if (v>barMaxVal) barMaxVal = v;
        }


    // draw horizontal axis
    painter.setPen(QColor("#AAA"));
    painter.drawLine(0,this->height()-14,this->width(),this->height()-14);

    // draw horizontal axis ticks
    i=0;
    foreach (int v,values)
        {
            Q_UNUSED(v);
            ++i;
            painter.setPen(QColor("#AAA"));
            //painter.drawRect( (barWidth+gap) *(i-1)+gap/2,this->height()-8,barWidth,4);
            painter.drawLine( (barWidth+gap) *(i-1)+gap/2 + barWidth/2 , this->height()-14, (barWidth+gap) *(i-1)+gap/2 + barWidth/2 , this->height()-12  );

        }



    // horizontal axis labels
    i=0;
    foreach (QString v,values.keys())
        {
            ++i;
            painter.setPen(QColor("#AAA"));
            painter.drawText( (barWidth+gap) *(i-1)+gap/2, this->height()-12,1*barWidth,12 , Qt::AlignCenter , v);

        }



    // draw the bars and labels
    i=0;
    foreach (int v,values)
        {
            ++i;
            painter.setPen(QColor("#777"));
            painter.setBrush(QColor("#777"));
            painter.drawRect( (barWidth+gap) *(i-1)+gap/2,this->height()-15-((this->height()-45)*v/barMaxVal),barWidth,((this->height()-45)*v/barMaxVal));
            painter.setPen(QColor("#AAA"));
            painter.drawText( (barWidth+gap) *(i-1)+gap/2,this->height()-15-((this->height()-45)*v/barMaxVal)-20,1*barWidth,20 , Qt::AlignCenter , QString::number(v));
        }

}
