#include "linegraphwidget.h"

lineGraphWidget::lineGraphWidget(QWidget *parent) : QWidget(parent)
{

}

void lineGraphWidget::clear()
{
    values.clear();
    values1.clear();
    labels.clear();
}

void lineGraphWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
   /*
    values.insert("a",10);
    values.insert("b",15);
    values.insert("c",19);
    */

    //qDebug() << labels;
    //qDebug() << values;
    //qDebug() << values1;

    if (values.count())
    {

    //qDebug() << "PAINTEVENT!";
    //qDebug() << this->width();
    //qDebug() << values.count();
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, false);
    painter.setPen( QColor("#777"));
    painter.setBrush(QColor("#777"));


    //int i; //UNUSED
    //int gap = 6; //UNUSED
    //int barWidth = (this->width() - gap*values.count() ) / values.count(); //UNUSED
    int maxV = 0;
    int maxV1 = 0;
    int minV = 10000000;
    int minV1 = 10000000;
    int startDay =  labels.at(0) ;

    // get maximum value
    foreach (int v,values)
        {
            if (v>maxV) maxV = v;
            if (v<minV) minV = v;
        }

    foreach (int v,values1)
        {
            if (v>maxV1) maxV1 = v;
            if (v<minV1) minV1 = v;
        }

    if (maxV==0 ) maxV =1;
    if (maxV1==0) maxV1=1;

    if (minV==maxV) maxV=minV+1;
    if (minV1==maxV1) maxV1=minV1+1;

    int days = labels.at(labels.count()-1) - labels.at(0);
    qDebug() << "days=" << days;

    //qDebug() << "MAXVAL" << maxV;
    //qDebug() << "MAXVAL1" << maxV1;
    // draw horizontal axis
    painter.setPen(QColor("#AAA"));
    painter.drawLine( 35+2 , this->height()-14 ,this->width(),this->height()-14);

    painter.drawLine( 35+2 , this->height()-14 , 35+2 , this->height()-10  );
    painter.drawText( 35+2 , this->height()-12 , 120 , 12 , Qt::AlignLeft ,  startX );
    painter.drawLine( this->width()-1 , this->height()-14,this->width()-1 , this->height()-10  );
    painter.drawText( this->width()-1-120  , this->height()-12 , 120 , 12 , Qt::AlignRight ,  endX );

    // ticks
    int ticks=0;
    //qDebug() << "A";

    if (days>90)  // months
    {
        ticks = days/30;
        painter.drawText( 0,this->height()-22,35,12,Qt::AlignRight , "months" );
        qDebug() << "months=" << ticks;
    }
    else
    {
        ticks = days;
        painter.drawText( 0,this->height()-22,35,12,Qt::AlignRight , "days" );
        qDebug() << "days=" << ticks;
    }

    //qDebug() << "B";

    if (ticks)
    {
        for (int i=0;i<ticks;++i)
        {
            int x = 35 + 2 + (i+1)*(this->width()-35-2)/ticks;
            painter.drawLine( x , this->height()-14 , x , this->height()-12  );
        }

    }

    //qDebug() << "C";


    // draw vertical axis
    painter.setPen(QColor("#AAA"));
    painter.drawLine( 35+2 ,this->height()-14, 35+2  ,2);
    painter.drawLine(35,7,35+2,7);
    painter.drawLine(35,30,35+2,30);
    painter.drawLine(35,this->height()-60+7,35+2,this->height()-60+7);
    painter.drawLine(35,23+this->height()-60+7,35+2,23+this->height()-60+7);

    painter.setPen(QColor("#0A0"));
    painter.drawText( 0,0,35,12,Qt::AlignRight , QString::number(maxV) );
    painter.drawText( 0,this->height()-60,35,12,Qt::AlignRight , QString::number(minV) );

    painter.setPen(QColor("#A55"));
    painter.drawText( 0,23,35,12,Qt::AlignRight , QString::number(maxV1) );
    painter.drawText( 0,23+this->height()-60,35,12,Qt::AlignRight , QString::number(minV1) );

    painter.setPen(QPen( QColor("#777") , 1 , Qt::DotLine  ));
    painter.drawLine(35,7,this->width(),7);
    painter.drawLine(35,30,this->width(),30);

    // qDebug() << "D";
    /*
    qDebug() << values;
    qDebug() << values1;
    qDebug() << labels;
    qDebug() << values.count();
    qDebug() << values.at(1);
    qDebug() << labels.at(1);

    */
    painter.setRenderHint(QPainter::Antialiasing, true);
    QPen pen1;
    pen1.setWidth(2);
    pen1.setColor(QColor("#0A0"));
    painter.setPen(pen1);

    // qDebug() << "E";

    for (int i=1; i<values.count();i++)
    {

        // values

        pen1.setColor(QColor("#0A0"));
        pen1.setWidth(2);
        painter.setPen(pen1);

        int w = this->width()-40;
        int h = this->height()-21-39;
        int h1 = this->height()-47-15;
        float x1 = (labels.at(i-1)-startDay+0.0) /  (labels.at(labels.count()-1)-startDay);
        float x2 = (labels.at(i)-startDay+0.0)   /  (labels.at(labels.count()-1)-startDay);

        // qDebug() << "F";

        if (!disableV)
          painter.drawLine(
                             w * x1 + 39  ,
                             h - h * (values.at(i-1)-minV) / (maxV-minV) + 7 ,
                             w * x2   +39 ,
                             h - h * (values.at(i)-minV)   / (maxV-minV) + 7
                    );

        /*
        // no stretch
        painter.drawLine(
                    (this->width()-2-35-3)*(labels.at(i-1)-startDay)/(labels.at(labels.count()-1)-startDay) + 2 +35+2 ,
                    this->height()-14-5-2 - (this->height()-14-5-2)*values.at(i-1)/maxV  + 2 +5,
                    (this->width()-2-35-3)*(labels.at(i)-startDay)/(labels.at(labels.count()-1)-startDay)   +2+35+2  ,
                    this->height()-14-5-2 - (this->height()-14-5-2)*values.at(i)/maxV    + 2 +5

                    );
        */

        // values1

        pen1.setColor(QColor("#A00"));
        pen1.setWidth(2);
        painter.setPen(pen1);

        // qDebug() << "G";

        if (!disableV1)
         painter.drawLine(
                    w *x1 + 39 ,
                    h1 - h1 * (values1.at(i-1)-minV1)/(maxV1-minV1)  +32 ,
                    w *x2   +39  ,
                    h1 - h1 * (values1.at(i)-minV1)/(maxV1-minV1)    + 32

                    );

        // qDebug() << "H";

        /*
        // no stretch
        painter.drawLine(
                    (this->width()-2-35-3)*(labels.at(i-1)-startDay)/(labels.at(labels.count()-1)-startDay) + 2 +35+2 ,
                    this->height()-14 - 30 -3 - (this->height()-14-30-2)*values1.at(i-1)/maxV1  + 2 +30 ,
                    (this->width()-2-35-3)*(labels.at(i)-startDay)/(labels.at(labels.count()-1)-startDay)   +2+35+2  ,
                    this->height()-14 - 30-3 - (this->height()-14-30-2)*values1.at(i)/maxV1    + 2 +30

                    );
        */

    }


    qDebug() << "DONE";

    }

}

