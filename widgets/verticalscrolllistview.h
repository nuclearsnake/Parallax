#ifndef VERTICALSCROLLLISTVIEW_H
#define VERTICALSCROLLLISTVIEW_H

#include <QListView>

class VerticalScrollListView : public QListView
{
    Q_OBJECT
public:
    explicit VerticalScrollListView(QWidget *parent = 0);

signals:

public slots:

private slots:
    void wheelEvent(QWheelEvent *event);
};

#endif // VERTICALSCROLLLISTVIEW_H
