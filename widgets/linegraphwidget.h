#ifndef LINEGRAPHWIDGET_H
#define LINEGRAPHWIDGET_H

#include <QWidget>
#include <QPainter>
#include <QDebug>
#include <QVector>
#include <QString>
#include <QList>
#include <QPair>

class lineGraphWidget : public QWidget
{
    Q_OBJECT
    void paintEvent(QPaintEvent *event);
public:
    explicit lineGraphWidget(QWidget *parent = 0);
    //QMap<int,int> values;
    //QList<QPair<int,int> > values;
    QList<int> values;
    QList<int> values1;
    QList<int> labels;
    QString startX;
    QString endX;
    bool disableV = false;
    bool disableV1= false;
    void clear();
signals:

public slots:
};

#endif // LINEGRAPHWIDGET_H
