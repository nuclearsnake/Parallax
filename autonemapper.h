#ifndef AUTONEMAPPER_H
#define AUTONEMAPPER_H

#include <QDialog>
#include <QStandardItemModel>

namespace Ui {
class auToneMapper;
}

class auToneMapper : public QDialog
{
    Q_OBJECT

public:
    explicit auToneMapper(QWidget *parent = 0);
    ~auToneMapper();
    QString projectPath;
    QString libraryPath;
    QStandardItemModel* model;
    int currentItem;
    int allItems;
    QStringList commandList;

    void setProjectPath(QString path);
private slots:
    void init();
    void getTemplates(QString branch);
    void go();
    void test();
    void runProc(QString command);
    void runProxy();
private:
    Ui::auToneMapper *ui;
};

#endif // AUTONEMAPPER_H
