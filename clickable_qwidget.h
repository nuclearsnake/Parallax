#ifndef CLICKABLE_QWIDGET_H
#define CLICKABLE_QWIDGET_H

#include <QWidget>
#include <QMouseEvent>
#include <QEvent>


class clickable_QWidget : public QWidget
{
    Q_OBJECT
public:
    explicit clickable_QWidget(QWidget *parent = 0);

    void mousePressEvent(QMouseEvent *ev);
    void paintEvent(QPaintEvent *);

signals:
    void Mouse_Pressed();

public slots:

};

#endif // CLICKABLE_QWIDGET_H
