#include "processmanager.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QMenu>
#include <QAction>
#include <QMessageBox>
#include <QImageReader>

#include <extensions/tools.h>
#include <threadedimagecacher.h>
#include <dialogs/addprocessdialog.h>
#include <dialogs/ptsclonerdialog.h>
#include <dialogs/yesnodialog.h>
#include <processtoarchivedialog.h>


processManager::processManager(QObject *parent) :
    QObject(parent)
{
    selected        = new Selected();
    files           = new Files();
    fileModel       = new QStandardItemModel();
    fDelegate       = new filePickerDelegate();
    fDelegate->thumbWidth = 220;
    listDelegate    = new processorListDelegate();
    browserDelegate = new filePickerDelegate();
    browserDelegate->thumbWidth = 220;
    dir             = new QDir;
}

void processManager::init()
{
    worxModel = new QSqlQueryModel(this);
    defaultQuery = "SELECT  projectname,id,path,isPano,thumbnail,icondata FROM projects WHERE isFinished == 0 AND (isDeleted == 0 OR isDeleted IS NULL)   ORDER BY projectname";
    worxModel->setQuery(defaultQuery,libMan->db);
    ui->listView_processTree->setModel(worxModel);
    ui->listView_processBrowser->setModel(worxModel);
    ui->listView_processNewBrowser->setModel(worxModel);

    // delegates
    listDelegate->cacheFolder       = setCache();
    fDelegate->cacheFolder          = setCache();
    browserDelegate->cacheFolder    = setCache();

    // tree
    ui->listView_processTree->setItemDelegate(listDelegate);

    // browser
    tools::listViewIconMode(ui->listView_processNewBrowser);
    ui->listView_processNewBrowser->setItemDelegate(browserDelegate);

    // files
    tools::listViewIconMode(ui->listView_processFiles);
    ui->listView_processFiles->setModel(fileModel);
    ui->listView_processFiles->setItemDelegate(fDelegate);
    ui->listView_processFiles->setContextMenuPolicy(Qt::CustomContextMenu);

}

void processManager::initConnections()
{
    // tree
    connect( ui->listView_processTree       , SIGNAL(clicked(QModelIndex))  , this  ,SLOT(select(QModelIndex)) );
    connect( ui->listView_processNewBrowser , SIGNAL(clicked(QModelIndex))  , this  ,SLOT(select(QModelIndex)) );

    // browser
    connect( ui->listView_processNewBrowser , SIGNAL(doubleClicked(QModelIndex))    , this  , SLOT(treeItemDBLClick(QModelIndex)) );
    connect( ui->pushButton_aniMate         , SIGNAL(clicked())                     , this  , SLOT(animateSideBar()) );

    // file group buttons
    connect( ui->pushButton_processFileGroupOrigall     , SIGNAL(clicked()) , this  , SLOT(fileGroupSelector()) );
    connect( ui->pushButton_processFileGroupEV0         , SIGNAL(clicked()) , this  , SLOT(fileGroupSelector()) );
    connect( ui->pushButton_processFileGroupEV1         , SIGNAL(clicked()) , this  , SLOT(fileGroupSelector()) );
    connect( ui->pushButton_processFileGroupEV2         , SIGNAL(clicked()) , this  , SLOT(fileGroupSelector()) );
    connect( ui->pushButton_processFileGroupPanorama    , SIGNAL(clicked()) , this  , SLOT(fileGroupSelector()) );
    connect( ui->pushButton_processFileGroupHDR         , SIGNAL(clicked()) , this  , SLOT(fileGroupSelector()) );
    connect( ui->pushButton_processFileGroupPostProcess , SIGNAL(clicked()) , this  , SLOT(fileGroupSelector()) );


    // buttons & dialogs
    connect( ui->actionAdd_newProcess           , SIGNAL(triggered())   , this  , SLOT(dialog_addNew())         );
    connect( ui->actionAdd_new_Process_toolBar  , SIGNAL(triggered())   , this  , SLOT(dialog_addNew())         );
    connect( ui->pushButton_processDelete       , SIGNAL(clicked())     , this  , SLOT(deleteProject())         );
    connect( ui->pushButton_processPathToClipboard  , SIGNAL(clicked()) , this  , SLOT(pathToClipboard())       );
    connect( ui->pushButton_processOpenFolder   , SIGNAL(clicked())     , this  , SLOT(folderOpen())            );
    connect( ui->pushButton_processProperties   , SIGNAL(clicked())     , this  , SLOT(dialog_properties())     );
    connect( ui->pushButton_processToArchive    , SIGNAL(clicked())     , this  , SLOT(dialog_moveToArchive())  );
    connect( ui->pushButton_processHDR          , SIGNAL(clicked())     , this  , SLOT(filesToHDR())            );
    connect( ui->pushButton_processAlltoPS      , SIGNAL(clicked())     , this  , SLOT(filesToPS())             );
    connect( ui->pushButton_processStich        , SIGNAL(clicked())     , this  , SLOT(filesToPTGui())          );
    connect( ui->pushButton_processBatchStich   , SIGNAL(clicked())     , this  , SLOT(filesToPTGuiBatch())     );
    connect( ui->pushButton_processClonePTS     , SIGNAL(clicked())     , this  , SLOT(dialog_clonePts())       );
    connect( ui->pushButton_processFileOpen     , SIGNAL(clicked())     , this  , SLOT(fileOpen())              );
    connect( ui->pushButton_processFileEdit     , SIGNAL(clicked())     , this  , SLOT(fileEdit())              );

    // files
    connect( ui->listView_processFiles  , SIGNAL(clicked(QModelIndex))          , this  ,SLOT(loadFileInfo(QModelIndex)) );
    connect( ui->listView_processFiles  , SIGNAL(doubleClicked(QModelIndex))    , this  , SLOT(fileDoubleClick(QModelIndex))  );
    connect( ui->listView_processFiles  , SIGNAL(customContextMenuRequested(QPoint))    , this  , SLOT(thumbContextMenu(QPoint)) );


}

// @COMMON

void processManager::folderOpen()
{
    tools::openFolderInExplorer(selected->path);
}

void processManager::pathToClipboard()
{
    tools::copyToClipboard(selected->path);
}

void processManager::fileOpen()
{
    tools::openLocalFile( selected->path + ui->label_processFileName->text() );
}

void processManager::fileEdit()
{
    QStringList arg;
    arg <<  QDir::toNativeSeparators( selected->path + ui->label_processFileName->text() );
    emit openWithPS(arg);
}

void processManager::fileDoubleClick(QModelIndex index)
{
      tools::openLocalFile( selected->path + index.data().toString() );
}

void processManager::treeItemDBLClick(QModelIndex index)
{
    animateSideBar();
    select(index);
}

void processManager::setSelected(QModelIndex index)
{
    selected->title     = tools::dataFromIndex(index,0);
    selected->id        = tools::dataFromIndex(index,1).toInt();
    selected->path      = tools::dataFromIndex(index,2);
    selected->isPano    = tools::dataFromIndex(index,3).toInt();
    selected->thumbNail = tools::dataFromIndex(index,4);
}

void processManager::updateUi()
{
    ui->label_processTitle->setText(selected->title);
    ui->label_processPath->setText(selected->path);
    (selected->isPano==1) ? ui->label_processPano->setText("P") :  ui->label_processPano->setText("");
}

void processManager::loadDir()
{
    dir->setPath( selected->path );
    dir->setFilter(QDir::NoDotAndDotDot | QDir::Files );
    dir->setSorting(QDir::Time | QDir::Reversed);
    fileModel->clear();
    files->reset();
    selected->fileList = dir->entryList();
}

void processManager::sortFiles()
{
    for (int i=0; i<selected->fileList.count();++i)
        {
            QString f = selected->fileList.at(i);
            if (selected->isPano==0)
                {
                    if ( f.endsWith("_-1.tif") || f.endsWith("_0.tif") || f.endsWith("_1.tif")  || f.endsWith("_2.tif") )
                        files->origiAll << f ;
                }
                else
                {
                    if (f.endsWith("_0.tif")) files->ev0 << f;
                    if (f.endsWith("_1.tif")) files->ev1 << f;
                    if (f.endsWith("_2.tif")) files->ev2 << f;
                }
            if ( (f.contains("_ev") & f.endsWith(".tif")) || f.endsWith("pts") )  files->panorama << f;
            if ( (f.endsWith("hdr")) || f.endsWith("xmp") || (f.endsWith("tif") & f.contains("tonemapped_")) )  files->hdr << f;
            if (  f.endsWith("psd") )  files->postProcess << f;
        }
}

void processManager::fileGroupButtonsVisibilityUpdate()
{
    ui->pushButton_processFileGroupOrigall->setVisible(files->origiAll.count()!=0);
    ui->pushButton_processFileGroupEV0->setVisible(files->ev0.count()!=0);
    ui->pushButton_processFileGroupEV1->setVisible(files->ev1.count()!=0);
    ui->pushButton_processFileGroupEV2->setVisible(files->ev2.count()!=0);
    ui->pushButton_processFileGroupPanorama->setVisible(files->panorama.count()!=0);
    ui->pushButton_processFileGroupHDR->setVisible(files->hdr.count()!=0);
    ui->pushButton_processFileGroupPostProcess->setVisible(files->postProcess.count()!=0);
}

void processManager::lastImageFindAndCache()
{
    bool found = false;
    QString lastImage;
    for (int i=selected->fileList.count()-1 ; i>0 ; --i )
        {
            QString f = selected->fileList.at(i);
            if (f.endsWith("tif") || f.endsWith("jpg") || f.endsWith("psd") )
                {
                    if (!found) lastImage=f;
                    found = true;
                }
        }

    if ( ( lastImage!="" ) & ( lastImage!=selected->thumbNail ) )
        {
            libMan->db.exec("UPDATE projects SET thumbnail = '"+lastImage+"' WHERE id="+ QString::number(selected->id));
            worxModel->setQuery(defaultQuery,libMan->db);
        }


    if (!QFile::exists(libMan->libraryFolder+"cache/"+lastImage+".jpg"))
        {
            QStringList filesTobeCached;
            filesTobeCached << selected->path+"/"+lastImage;
            threadedImageCacher *tCacher = new threadedImageCacher(this);
            tCacher->cacheFolder = setCache();
            tCacher->filesTobeCached = filesTobeCached;
            connect ( tCacher   , SIGNAL(itemLoaded())  , ui->listView_processTree , SLOT(repaint()) );
            tCacher->start();
        }
}

void processManager::fileGroupBottomClick()
{
    bool clicked = false;
    if ( ( !clicked ) & ( files->postProcess.count()!=0 ) ) { ui->pushButton_processFileGroupPostProcess->click(); clicked = true; }
    if ( ( !clicked ) & ( files->hdr.count()!=0         ) ) { ui->pushButton_processFileGroupHDR->click(); clicked = true; }
    if ( ( !clicked ) & ( files->panorama.count()!=0    ) ) { ui->pushButton_processFileGroupPanorama->click(); clicked = true; }
    if ( ( !clicked ) & ( files->ev2.count()!=0         ) ) { ui->pushButton_processFileGroupEV2->click(); clicked = true; }
    if ( ( !clicked ) & ( files->ev1.count()!=0         ) ) { ui->pushButton_processFileGroupEV1->click(); clicked = true; }
    if ( ( !clicked ) & ( files->ev0.count()!=0         ) ) { ui->pushButton_processFileGroupEV0->click(); clicked = true; }
    if ( ( !clicked ) & ( files->origiAll.count()!=0    ) ) { ui->pushButton_processFileGroupOrigall->click(); clicked = true; }
    // load all if no group
    if ( !clicked ) feedFileList(dir->entryList());
}

void processManager::listWidgetsSetIndex(QModelIndex index)
{
    ui->listView_processTree->setCurrentIndex(index);
    ui->listView_processNewBrowser->setCurrentIndex(index);
}

void processManager::select(QModelIndex index)
{
    setSelected(index);
    updateUi();
    loadDir();
    sortFiles();
    fileGroupButtonsVisibilityUpdate();
    lastImageFindAndCache();
    fileGroupBottomClick();
    listWidgetsSetIndex(index);
}

void processManager::feedFileList(QStringList fls)
{
    fileModel->clear();
    QStringList filesTobeCached;
    for (int i=0;  i < fls.count() ; ++i)
        {
            QString f = fls.at(i);
            if ( f.endsWith("tif") || f.endsWith("psd") || f.endsWith("jpg") )
                if ( !QFile::exists( setCache() + f + ".jpg") )
                    filesTobeCached << selected->path+"/"+f;

            QList<QStandardItem *> row;

            row << new QStandardItem(f);
            row << new QStandardItem(selected->path+"/"+f);
            fileModel->appendRow(row);
        }

    if (filesTobeCached.count()>0)
        {
            threadedImageCacher *tCacher = new threadedImageCacher(this);
            tCacher->cacheFolder = setCache();
            tCacher->filesTobeCached = filesTobeCached;
            connect ( tCacher   , SIGNAL(itemLoaded())  , ui->listView_processFiles , SLOT(repaint()) );
            tCacher->start();
        }
}

void processManager::fileGroupSelector()
{
    QString sndr = sender()->objectName();

    ui->pushButton_processFileGroupOrigall->setChecked(sndr == "pushButton_processFileGroupOrigall");
    ui->pushButton_processFileGroupEV0->setChecked(sndr == "pushButton_processFileGroupEV0");
    ui->pushButton_processFileGroupEV1->setChecked(sndr == "pushButton_processFileGroupEV1");
    ui->pushButton_processFileGroupEV2->setChecked(sndr == "pushButton_processFileGroupEV2");
    ui->pushButton_processFileGroupPanorama->setChecked(sndr == "pushButton_processFileGroupPanorama");
    ui->pushButton_processFileGroupHDR->setChecked(sndr == "pushButton_processFileGroupHDR");
    ui->pushButton_processFileGroupPostProcess->setChecked(sndr == "pushButton_processFileGroupPostProcess");

    ui->pushButton_processHDR->setEnabled(false);
    ui->pushButton_processStich->setEnabled(false);
    ui->pushButton_processAlltoPS->setEnabled(false);
    ui->pushButton_processClonePTS->setEnabled(false);
    ui->pushButton_processBatchStich->setEnabled(false);

    ui->widget_9->setVisible(false);

    if (sndr=="pushButton_processFileGroupOrigall")
        {
            fList = files->origiAll;
            ui->pushButton_processHDR->setEnabled(true);
        }
    if (sndr=="pushButton_processFileGroupEV0")
        {
            fList = files->ev0;
            //ui->pushButton_processHDR->setEnabled(true);
            ui->pushButton_processStich->setEnabled(true);
        }
    if (sndr=="pushButton_processFileGroupEV1")
        {
            fList = files->ev1;
            //ui->pushButton_processHDR->setEnabled(true);
            ui->pushButton_processStich->setEnabled(true);
        }
    if (sndr=="pushButton_processFileGroupEV2")
        {
            fList = files->ev2;
            //ui->pushButton_processHDR->setEnabled(true);
            ui->pushButton_processStich->setEnabled(true);
        }
    if (sndr=="pushButton_processFileGroupPanorama")
        {
            fList = files->panorama;
            ui->pushButton_processHDR->setEnabled(true);
            ui->pushButton_processBatchStich->setEnabled(true);
            ui->pushButton_processClonePTS->setEnabled(true);
        }
    if (sndr=="pushButton_processFileGroupHDR")
        {
            fList = files->hdr;
            ui->pushButton_processAlltoPS->setEnabled(true);
        }
    if (sndr=="pushButton_processFileGroupPostProcess") fList = files->postProcess;

    feedFileList(fList);

}

void processManager::loadFileInfo(QModelIndex index)
{
    ui->widget_9->setVisible(true);
    ui->label_process_fileinfoResolution->setVisible(false);
    ui->label_process_fileinfoResolutionLabel->setVisible(false);
    //ui->label_process_fileinfoPreview->setVisible(false);
    ui->pushButton_processFileEdit->setEnabled(false);

    QString fileName = tools::dataFromIndex(index,0);
    ui->label_processFileName->setText(fileName);
    ui->label_process_fileinfoType->setText(fileName.split(".").last().toUpper() );

    QFileInfo *fileInfo = new QFileInfo;
    fileInfo->setFile(selected->path+"/"+fileName);

    ui->label_process_fileinfoSize->setText( tools::kiloMegaGiga( fileInfo->size() ) );
    QString date;
    date.sprintf("%04d.%02d.%02d",fileInfo->created().date().year(),fileInfo->created().date().month(),fileInfo->created().date().day() );
    ui->label_process_fileinfoCreated->setText(date);

    QPixmap pix;

    if ( (fileName.endsWith("tif")) || (fileName.endsWith("psd")) || (fileName.endsWith("jpg")) )
        {
            QImageReader reader(selected->path+"/"+fileName);
            if (reader.supportsOption(QImageIOHandler::Size))   //qDebug() << "Size:" << reader.size();
                {
                    ui->label_process_fileinfoResolution->setVisible(true);
                    ui->label_process_fileinfoResolutionLabel->setVisible(true);
                    ui->label_process_fileinfoResolution->setText(
                                QString::number(reader.size().width()) + " x " + QString::number(reader.size().height())
                                + " (" + QString::number(reader.size().width()*reader.size().height()/1000/1000) + " MPix)"  );
                }
            //ui->label_process_fileinfoPreview->setVisible(true);
            pix = QPixmap(setCache()+fileName+".jpg");
            ui->pushButton_processFileEdit->setEnabled(true);

        }
        else
        {
            pix = QPixmap(":/icons/icons/file-icon-b.png");
        }

    ui->label_process_fileinfoPreview->setPixmap(pix.scaled(100,100,Qt::KeepAspectRatio,Qt::SmoothTransformation) );


}

QStringList processManager::filesToArg(QStringList list,QString ext)
{
    QStringList arg;
    for (int i=0;i<list.count();++i)
        {
            QString f = list.at(i);
            if (f.endsWith(ext)) arg << selected->path.replace("/","\\") + f;
        }
    return arg;
}


void processManager::filesToPTGuiBatch()
{
    QStringList arg;
    arg << "-batch" << filesToArg(fList,".pts");
    emit openWithPTGui(arg);
}

void processManager::filesToPTGui()
{
    QStringList arg;
    arg << filesToArg(fList,".tif");
    emit openWithPTGui(arg);
}

void processManager::filesToPS()
{
    QStringList arg;
    arg << filesToArg(fList,".tif");
    emit openWithPS(arg);
}

void processManager::filesToHDR()
{
    QStringList arg;
    arg << filesToArg(fList,".tif");
    emit openWithPhotoMatix(arg);
}

void processManager::attachUi(Ui::MainWindow *u)
{
    ui = u;
}

void processManager::attachLibMan(libraryManager *l)
{
    libMan = l;
}

void processManager::attachSetMan(settingsManager *s)
{
    setMan = s;
}

void processManager::deleteProject()
{
    yesNoDialog  ynDialog;
    ynDialog.setMessage( "Are you sure to delete this item?" );
    ynDialog.setDetail("This practically cannot be undone!");
    if (ynDialog.exec())
        {
            if (selected->id!=0)
                {
                    QString query="UPDATE projects SET isDeleted=1 WHERE id="+QString::number(selected->id);
                    qDebug() << query;
                    libMan->db.exec(query);
                    worxModel->setQuery(defaultQuery,libMan->db);
                    select(worxModel->index(0,0));
                }
        }
}

void processManager::animateSideBar()
{
    int animTime = 150;
    int min = 230;
    int max =  ui->main_frame->width()-545 ;
    if (ui->stackedWidget_processLeft->maximumWidth()==min)
        {
            QPropertyAnimation *animation1 = new QPropertyAnimation(ui->stackedWidget_processLeft, "maximumWidth");
            animation1->setDuration(animTime);
            animation1->setStartValue(min);
            animation1->setEndValue(max);

            QPropertyAnimation *animation2 = new QPropertyAnimation(ui->stackedWidget_processLeft, "minimumWidth");
            animation2->setDuration(animTime);
            animation2->setStartValue(min);
            animation2->setEndValue(max);

            QParallelAnimationGroup *group = new QParallelAnimationGroup(this);
            group->addAnimation(animation1);
            group->addAnimation(animation2);

            group->start();
            ui->stackedWidget_processLeft->setCurrentIndex(1);
            ui->pushButton_aniMate->setIcon(QIcon(":/icons/icons/stylesheet-branch-left.png"));

            connect(group   , SIGNAL(finished())     , this    ,   SLOT(listTo()) );
            //ui->listView_processTree->setItemDelegate(browserDelegate);
            //qDebug() << ui->listView_archiveTree->viewMode();

            //connect(group   , SIGNAL(finished())     , this    ,   SLOT(animationFeedback()) );
            //ui->pushButton_toggleFolderNavi->setIcon(QIcon(":/icons/icons/stylesheet-branch-left.png"));
        }
        else
        {
            QPropertyAnimation *animation1 = new QPropertyAnimation(ui->stackedWidget_processLeft, "maximumWidth");
            animation1->setDuration(animTime);
            animation1->setStartValue(max);
            animation1->setEndValue(min);

            QPropertyAnimation *animation2 = new QPropertyAnimation(ui->stackedWidget_processLeft, "minimumWidth");
            animation2->setDuration(animTime);
            animation2->setStartValue(max);
            animation2->setEndValue(min);

            QParallelAnimationGroup *group = new QParallelAnimationGroup(this);
            group->addAnimation(animation1);
            group->addAnimation(animation2);

            group->start();

            ui->pushButton_aniMate->setIcon(QIcon(":/icons/icons/stylesheet-branch-closed.png"));
            connect(group   , SIGNAL(finished())     , this    ,   SLOT(listBack()) );
            //ui->pushButton_toggleFolderNavi->setIcon(QIcon(":/icons/icons/stylesheet-branch-up.png"));
        }
}

void processManager::listTo()
{
    ui->listView_processNewBrowser->scrollTo(ui->listView_processNewBrowser->currentIndex(),QAbstractItemView::PositionAtCenter);
}

void processManager::listBack()
{
    ui->stackedWidget_processLeft->setCurrentIndex(0);
    ui->listView_processTree->scrollTo(ui->listView_processTree->currentIndex(),QAbstractItemView::PositionAtCenter);
}

void processManager::selectByTitle(QString title)
{
    bool found = false;
    int i;
    for (i=0;i<worxModel->rowCount();++i)
        {
            if (worxModel->index(i,0).data().toString()==title) found = true;
            if (found) break;
        }
    if (found) select(worxModel->index(i,0));
}

//----@DIALOGS----

void processManager::treeContextMenu(const QPoint &pos)
{
    QPoint globalPos = ui->listView_processTree->mapToGlobal(pos);

    QModelIndex index = ui->listView_processTree->indexAt(pos);
    QString theId  = index.sibling(index.row(),1).data().toString();
    QString isPano = index.sibling(index.row(),3).data().toString();

    QMenu contextMenu("Context menu");

    contextMenu.addAction(new QAction(tr("Delete"), this));

    QAction *action = new QAction( tr("Panorama") , this  );
        action->setCheckable(true);
        if (isPano=="1") action->setChecked(true);
        contextMenu.addAction(action);

    contextMenu.addAction(new QAction(tr("Edit..."), this));
    contextMenu.addAction(new QAction(tr("Finish"), this));

    QAction* selectedItem = contextMenu.exec(globalPos);
    if (selectedItem)
        {
            if (selectedItem->text() == "Delete")
                {
                    yesNoDialog  ynDialog;
                    ynDialog.setMessage( "Are you sure to delete this item?" );
                    ynDialog.setDetail("This practically cannot be undone!");
                    if (ynDialog.exec())
                        {
                            QString query="UPDATE projects SET isDeleted=1 WHERE id="+theId;
                            libMan->db.exec(query);
                            //worxModel->setQuery(setMan->processTreeQuery,libMan->db);
                        }
                }

            if (selectedItem->text() == "Panorama")
                {
                    QString qp="0";
                    if (selectedItem->isChecked()) qp = "1";
                    QString query="UPDATE projects SET isPano="+qp+" WHERE id="+theId;
                    qDebug() << query;
                    libMan->db.exec(query);
                    //worxModel->setQuery(setMan->processTreeQuery,libMan->db);
                    //this->selectProcessItem(index);
                    ui->listView_processTree->setCurrentIndex(index);
                    QTimer::singleShot(250,this,SLOT(processFocus()));
                }
        }
}

void processManager::thumbContextMenu(const QPoint &pos )
{
    qDebug() << "CM";
    QListView *view = static_cast<QListView *>(sender());
    //QAbstractItemModel *model  = view->model(); //UNUSED

    QModelIndex index = view->indexAt(pos);

    QString filename = index.sibling(index.row(),0).data().toString();
    QString filepath = selected->path+filename;

    QPoint globalPos = view->mapToGlobal(pos);

    QMenu contextMenu("Context menu");

     contextMenu.addAction(new QAction("Delete",this));
     contextMenu.addAction(new QAction("Preview",this));
     contextMenu.addAction(new QAction("Edit",this));

    QAction* selectedItem = contextMenu.exec(globalPos);
    if (selectedItem)
        {
            if (selectedItem->text() == "Delete")
                {
                    yesNoDialog  ynDialog;
                    ynDialog.setMessage( "Are you sure to delete this file?" );
                    ynDialog.setDetail(filepath);
                    if (ynDialog.exec())
                        {
                            QFile::remove(filepath);
                            //this->wuff1();
                        }
                }

            if (selectedItem->text() == "Preview")
                {
                    //processDoubleClick(index);
                    fileDoubleClick(index);

                }

            if (selectedItem->text() == "Edit")
                {
                    QStringList arg;
                    arg <<  QDir::toNativeSeparators( filepath );
                    emit openWithPS(arg);
                }
        }

}



void processManager::dialog_moveToArchive()
{
    processToArchiveDialog paDialog;
    //paDialog.setDefaultBase(setMan->value("GeneralSettings/defaultArchivePath"));
    paDialog.setSource(selected->path);

    QSqlQueryModel *getTitles = new QSqlQueryModel();
    getTitles->setQuery("SELECT title FROM archive",libMan->db);
    while (getTitles->canFetchMore()) getTitles->fetchMore();

    QList<QString> titleList;

    for (int i=0; i<getTitles->rowCount();++i)
        {
            titleList << getTitles->index(i,0).data().toString().toUpper();
        }


    paDialog.setTitleList(titleList);

    if ( paDialog.exec() )
        {
            //archive->dialog_addNew(paDialog.targetFolder);
        }
}

void processManager::dialog_addNew()
{
    addProcessDialog aDialog;
    if (droppedFolder!="") aDialog.preSelect(droppedFolder);
    if (aDialog.exec())
    {
        QString date ; date.sprintf("%d-%02d-%02d" ,  QDate::currentDate().year()  , QDate::currentDate().month() , QDate::currentDate().day());
        QString query="INSERT INTO projects  (projectname,path,isFinished,isPano,added) VALUES ('"+aDialog.name+"','"+aDialog.path+"/',0,"+ QString::number( aDialog.panoMode) +",'"+date+"')";
        libMan->db.exec(query);
        worxModel->setQuery(defaultQuery,libMan->db);
        selectByTitle(aDialog.name);
    }
    droppedFolder="";
}

void processManager::dialog_properties()
{
    addProcessDialog aDialog;
    aDialog.editMode( selected->path , selected->title , selected->isPano);
    if (aDialog.exec())
    {
        //QString date ; date.sprintf("%d-%02d-%02d" ,  QDate::currentDate().year()  , QDate::currentDate().month() , QDate::currentDate().day());
        //QString query="INSERT INTO projects  (projectname,path,isFinished,isPano,added) VALUES ('"+aDialog.name+"','"+aDialog.path+"/',0,"+ QString::number( aDialog.panoMode) +",'"+date+"')";
        QString query="UPDATE projects SET projectname='"+aDialog.name+"' , isPano = "+ QString::number( aDialog.panoMode) +" WHERE id = " + QString::number( selected->id );
        libMan->db.exec(query);
        worxModel->setQuery(defaultQuery,libMan->db);
        selectByTitle(aDialog.name);
    }
}

void processManager::dialog_clonePts()
{
    PtsClonerDialog pclDlg;
    pclDlg.setProjectPath( selected->path );
    pclDlg.exec();
}

QString processManager::setCache()
{
    return libMan->libraryFolder+"cache/";
}

