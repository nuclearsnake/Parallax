#ifndef THREADEDIMAGECACHER_H
#define THREADEDIMAGECACHER_H

#include <QThread>
#include <QStringList>

class threadedImageCacher : public QThread
{
    Q_OBJECT
public:
    explicit threadedImageCacher(QObject *parent = 0);
    QStringList filesTobeCached;
    QString cacheFolder;
    void run();
signals:

    void itemLoaded();

public slots:

};

#endif // THREADEDIMAGECACHER_H
