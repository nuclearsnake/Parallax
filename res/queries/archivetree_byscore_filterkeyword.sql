SELECT t1.title,t1.id,t1.projectDir,t1.keywords,t1.date,t1.image,t1.notes, COALESCE(ROUND(AVG(t1.subscore),2),0.0) score
FROM
    (
    SELECT  archive.id,archive.title,archive.projectDir,archive.keywords,archive.date,archive.image,archive.notes ,sites.site
            ,ROUND( ( CAST(sites.views  AS REAL ) / sa.maxViews )* 5 ,2) subscore
    FROM archive
    LEFT JOIN sites ON sites.projectId=archive.id
    LEFT JOIN
        (
        SELECT sites.site, MAX(sites.views) maxViews
        FROM sites
        GROUP BY sites.site
        ) sa  ON sa.site = sites.site
    WHERE sites.site != 'FB'
    AND keywords LIKE '%'
    --AND archive.title LIKE '%'
    UNION
    SELECT  archive.id,archive.title,archive.projectDir,archive.keywords,archive.date,archive.image,archive.notes ,sites.site
            ,ROUND( ( CAST(sites.favs  AS REAL ) / sa.maxFavs )* 5 ,2) subscore
    FROM archive
    LEFT JOIN sites
    ON sites.projectId=archive.id
    LEFT JOIN
        (
        SELECT sites.site, MAX(sites.favs) maxFavs
        FROM sites
        GROUP BY sites.site
        ) sa ON sa.site = sites.site
    --WHERE archive.title LIKE '%'
    WHERE keywords LIKE '%'
    ) t1
GROUP BY t1.title
ORDER BY ROUND(AVG(t1.subscore),4) DESC , t1.title
