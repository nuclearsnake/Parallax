--     0         1      2              3            4        5         6
SELECT t1.title, t1.id, t1.projectDir, t1.keywords, t1.date, t1.image, t1.notes
--      7                                                           8                 9                 10
      , COALESCE(ROUND( AVG(t1.subscore)    *[P2]  ,2),0.0) score , t1.location_lat , t1.location_lng , t1.location_text
FROM
    (
    SELECT  archive.id,archive.title,archive.projectDir,archive.keywords,archive.date,archive.image,archive.notes ,sites.site
            , archive.location_lat , archive.location_lng , archive.location_text
            ,ROUND( ( CAST(sites.views  AS REAL ) / sa.maxViews )* 5 ,2) subscore
    FROM archive
    LEFT JOIN sites ON sites.projectId=archive.id
    LEFT JOIN
        (
        SELECT sites.site, MAX(sites.views) maxViews
        FROM sites
        GROUP BY sites.site
        ) sa  ON sa.site = sites.site
    WHERE sites.site != 'FB'
    AND archive.title LIKE '%'
    UNION
    SELECT  archive.id,archive.title,archive.projectDir,archive.keywords,archive.date,archive.image,archive.notes ,sites.site
            , archive.location_lat , archive.location_lng , archive.location_text
            ,ROUND( ( CAST(sites.favs  AS REAL ) / sa.maxFavs )* 5 ,2) subscore
    FROM archive
    LEFT JOIN sites
    ON sites.projectId=archive.id
    LEFT JOIN
        (
        SELECT sites.site, MAX(sites.favs) maxFavs
        FROM sites
        GROUP BY sites.site
        ) sa ON sa.site = sites.site
    WHERE archive.title LIKE '%'
    ) t1
GROUP BY t1.title
ORDER BY COALESCE(ROUND( AVG(t1.subscore)    *[P2]  ,2),0.0) DESC , t1.title
