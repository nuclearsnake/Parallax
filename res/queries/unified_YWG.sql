--     0         1      2              3            4        5         6
SELECT t1.title, t1.id, t1.projectDir, t1.keywords, t1.date, t1.image, t1.notes
--      7                                                               8                 9                 10                 11
      , COALESCE(ROUND( AVG(t1.subscore) * 5    *[P2]  ,2),0.0) score , t1.location_lat , t1.location_lng , t1.location_text , t1.collections
--      12
      , (SELECT GROUP_CONCAT(site) FROM sites s WHERE s.projectid=t1.id ORDER BY site) AS sites
FROM
    (
    SELECT t3.id, t3.title, t3.projectDir, t3.keywords, t3.date, t3.image, t3.notes , t3.site
                , t3.location_lat , t3.location_lng , t3.location_text , t3.collections,'V'  , /*t3.subscore*t3.subscore as subscore  /* t3.subscore --*/ sqrt.sqrt as subscore --t2.subscore
        FROM
        (
            SELECT  archive.id,archive.title,archive.projectDir,archive.keywords,archive.date,archive.image,archive.notes ,sites.site
                    , archive.location_lat , archive.location_lng , archive.location_text , archive.collections,'V'
                    ,ROUND( ( (CAST(sites.views  AS REAL )-sa.minViews) / (sa.maxViews-sa.minViews) ) ,2) subscore,sites.id as siteid
            FROM archive
            LEFT JOIN sites ON sites.projectId=archive.id
            LEFT JOIN
                (
                SELECT tmm1.site site,tmm1.year year,MAX(tmm1.maxViews) maxViews,MIN(tmm1.minViews) minViews FROM
                (
                    SELECT  sites.site,
                            MAX(sites.views) maxViews,
                            MIN(sites.views) as minViews,
                            substr(sites.date,1,4)+0 year
                    FROM sites
                            WHERE sites.notcount IS NULL
                            AND sites.site != 'FB'
                            AND sites.site != 'PX'
                    GROUP BY substr(sites.date,1,4)+0,sites.site

                    UNION

                    SELECT  sites.site,
                            MAX(sites.views) maxViews,
                            MIN(sites.views) as minViews,
                            substr(sites.date,1,4)+1 year
                    FROM sites
                            WHERE sites.notcount IS NULL
                            AND sites.site != 'FB'
                            AND sites.site != 'PX'
                    GROUP BY substr(sites.date,1,4)+1,sites.site
                )tmm1
                GROUP BY tmm1.year,tmm1.site
                ) sa  ON sa.site = sites.site AND sa.year = substr(sites.date,1,4)+0
            WHERE --sites.site != 'FB'
                --AND
                notcount IS NULL
            AND archive.title LIKE '%'
            --AND sites.site='5P'
            [FILTER]
         ) t3
         LEFT JOIN sqrt ON sqrt.base = t3.subscore
        WHERE t3.siteid IN (SELECT MAX(id) FROM sites WHERE sites.projectid=t3.id GROUP BY sites.site) OR t3.siteid IS NULL

    UNION

    SELECT t2.id, t2.title, t2.projectDir, t2.keywords, t2.date, t2.image, t2.notes , t2.site
            , t2.location_lat , t2.location_lng , t2.location_text , t2.collections,'F' , /* t2.subscore*t2.subscore as subscore /*t2.subscore --,*/  sqrt.sqrt as subscore  --t2.subscore
        FROM
        (

            SELECT  archive.id,archive.title,archive.projectDir,archive.keywords,archive.date,archive.image,archive.notes ,sites.site
                    , archive.location_lat , archive.location_lng , archive.location_text , archive.collections,'F'
                    ,ROUND( ( (CAST(sites.favs  AS REAL ) - sa.minFavs)  / (sa.maxFavs-sa.MinFavs) ) ,2) subscore,sites.id as siteid
            FROM archive
            LEFT JOIN sites
            ON sites.projectId=archive.id
            LEFT JOIN
                (
                SELECT tmm2.site site,tmm2.year year,MAX(tmm2.maxFavs) maxFavs,MIN(tmm2.minFavs) minFavs FROM
                (
                    SELECT  sites.site,
                            MAX(sites.favs) maxFavs ,
                            MIN(sites.favs) as minFavs,
                            substr(sites.date,1,4)+0 year
                    FROM sites
                            WHERE sites.notcount IS NULL
                            AND sites.site != 'FB'
                           -- AND sites.site != 'VB'
                    GROUP BY substr(sites.date,1,4)+0,sites.site

                    UNION

                    SELECT  sites.site,
                            MAX(sites.favs) maxFavs ,
                            MIN(sites.favs) as minFavs,
                            substr(sites.date,1,4)+1 year
                    FROM sites
                            WHERE sites.notcount IS NULL
                            AND sites.site != 'FB'
                           -- AND sites.site != 'VB'
                    GROUP BY substr(sites.date,1,4)+1,sites.site
                )tmm2
                GROUP BY tmm2.year,tmm2.site
                ) sa ON sa.site = sites.site AND sa.year = substr(sites.date,1,4)+0
            --WHERE sites.site != 'VB' AND archive.title LIKE '%'
            WHERE --sites.site != 'FB'
                --AND
                archive.title LIKE '%'
                AND notcount IS NULL
                --AND sites.site='5P'
            [FILTER]
         ) t2
         LEFT JOIN sqrt ON sqrt.base = t2.subscore
         WHERE t2.siteid IN (SELECT MAX(id) FROM sites WHERE sites.projectid=t2.id GROUP BY sites.site) OR t2.siteid IS NULL


    ) t1
GROUP BY t1.title
ORDER BY  [ORDERBY] --COALESCE(ROUND( AVG(t1.subscore)    *[P2]  ,2),0.0) DESC , t1.title
