SELECT t1.title, ROUND(AVG(t1.subscore)*5*[P2],2) score
FROM
    (
    SELECT t3.id, t3.title, t3.projectDir, t3.keywords, t3.date, t3.image, t3.notes , t3.site
                    , t3.location_lat , t3.location_lng , t3.location_text , t3.collections,'V'  , /* t3.subscore --*/ sqrt.sqrt as subscore --t2.subscore
            FROM
            (
        SELECT  archive.id,archive.title,archive.projectDir,archive.keywords,archive.date,archive.image,archive.notes ,sites.site
                , archive.location_lat , archive.location_lng , archive.location_text , archive.collections,'V'
                ,ROUND( ( (CAST(sites.views  AS REAL )-sa.minViews) / (sa.maxViews-sa.minViews) ) ,2) subscore
        FROM archive
        LEFT JOIN sites ON sites.projectId=archive.id
        LEFT JOIN
            (
            SELECT  sites.site,
                    MAX(sites.views) maxViews,
                    MIN(sites.views) as minViews,
                    substr(sites.date,1,4) year
            FROM sites
                    WHERE sites.notcount IS NULL
                    AND sites.site != 'FB'
            GROUP BY substr(sites.date,1,4),sites.site
            ) sa  ON sa.site = sites.site AND sa.year = substr(sites.date,1,4)
        WHERE sites.site != 'FB'
            AND notcount IS NULL
        AND archive.title LIKE '%'
        --AND sites.site='5P'
        AND archive.title='%1'
     ) t3
     LEFT JOIN sqrt ON sqrt.base = t3.subscore


UNION

SELECT t2.id, t2.title, t2.projectDir, t2.keywords, t2.date, t2.image, t2.notes , t2.site
        , t2.location_lat , t2.location_lng , t2.location_text , t2.collections,'F' , /*t2.subscore --,*/  sqrt.sqrt as subscore --t2.subscore
    FROM
    (

        SELECT  archive.id,archive.title,archive.projectDir,archive.keywords,archive.date,archive.image,archive.notes ,sites.site
                , archive.location_lat , archive.location_lng , archive.location_text , archive.collections,'F'
                ,ROUND( ( (CAST(sites.favs  AS REAL ) - sa.minFavs)  / (sa.maxFavs-sa.MinFavs) ) ,2) subscore
        FROM archive
        LEFT JOIN sites
        ON sites.projectId=archive.id
        LEFT JOIN
            (
            SELECT  sites.site,
                    MAX(sites.favs) maxFavs ,
                    MIN(sites.favs) as minFavs,
                    substr(sites.date,1,4) year
            FROM sites
                    WHERE sites.notcount IS NULL
                    AND sites.site != 'FB'
            GROUP BY substr(sites.date,1,4),sites.site
            ) sa ON sa.site = sites.site AND sa.year = substr(sites.date,1,4)
        --WHERE sites.site != 'VB' AND archive.title LIKE '%'
        WHERE sites.site != 'FB'
            AND archive.title LIKE '%'
            AND notcount IS NULL
            --AND sites.site='5P'
            AND archive.title='%1'
     ) t2
     LEFT JOIN sqrt ON sqrt.base = t2.subscore

    ) t1
GROUP BY t1.title
