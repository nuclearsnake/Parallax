SELECT t5.title,t5.score,t5.DA,t5.`5P`,t5.`G+`,t5.FB,t5.FL FROM
(
SELECT t4.title
    , CASE WHEN COALESCE(t4.DA,t4.`5P`,t4.`G+`,t4.FB,t4.FL) IS NOT NULL 
         THEN 
         ROUND(   (COALESCE(2*t4.DA,0) + COALESCE(2*t4.`5P`,0) + COALESCE(2*t4.`G+`,0) + COALESCE(t4.FB,0) + COALESCE(2*t4.FL,0)) /
            (
                CASE WHEN t4.DA   IS NULL THEN 0 ELSE 2 END +
                CASE WHEN t4.`5P` IS NULL THEN 0 ELSE 2 END +
                CASE WHEN t4.`G+` IS NULL THEN 0 ELSE 2 END +
                CASE WHEN t4.FB   IS NULL THEN 0 ELSE 1 END +
                CASE WHEN t4.FL   IS NULL THEN 0 ELSE 2 END                                 
            ) ,2 )
         ELSE
             0
         END AS score
 
    ,t4.DA
    ,t4.`5P`
    ,t4.`G+`
    ,t4.FB
    ,t4.FL

FROM
    (
    SELECT 
    
        t3.title 
        
        , SUM(DA) DA, SUM(`5P`) `5P`,SUM(`G+`) `G+`,SUM(FB) FB,SUM(FL) FL
    FROM
        (
        SELECT   t2.title
               , CASE WHEN t2.site = 'DA' THEN t2.subscore ELSE NULL END as DA
               , CASE WHEN t2.site = '5P' THEN t2.subscore ELSE NULL END as `5P`
               , CASE WHEN t2.site = 'G+' THEN t2.subscore ELSE NULL END as `G+`
               , CASE WHEN t2.site = 'FB' THEN t2.subscore ELSE NULL END as FB
               , CASE WHEN t2.site = 'FL' THEN t2.subscore ELSE NULL END as FL
        FROM
            (
            SELECT t1.title,t1.site , ROUND( AVG(t1.subscore) ,2) subscore
            FROM
                (
                SELECT archive.id,archive.title title,sites.site, 
                   ROUND( ( CAST(sites.views  AS REAL ) / sa.maxViews )* 5 ,3) subscore         
                FROM archive 
                LEFT JOIN sites ON sites.projectId=archive.id 
                LEFT JOIN 
                    ( 
                    SELECT sites.site, MAX(sites.views) maxViews 
                    FROM sites 
                    GROUP BY sites.site 
                    ) sa 
                    ON sa.site = sites.site                
                WHERE sites.site != 'FB'  
                               
                UNION
                
                SELECT archive.id,archive.title title,sites.site, 
                   ROUND( ( CAST(sites.favs  AS REAL ) / sa.maxFavs )* 5 ,3) subscore       
                FROM archive 
                LEFT JOIN sites ON sites.projectId=archive.id 
                LEFT JOIN 
                    ( 
                    SELECT sites.site, MAX(sites.favs) maxFavs
                    FROM sites 
                    GROUP BY sites.site 
                    ) sa 
                    ON sa.site = sites.site                
               
                ) t1
            GROUP BY t1.title,t1.site
            ) t2
        ) t3
    GROUP BY t3.title
    ) t4
) t5
ORDER BY score DESC