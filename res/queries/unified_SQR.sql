--     0         1      2              3            4        5         6
SELECT t1.title, t1.id, t1.projectDir, t1.keywords, t1.date, t1.image, t1.notes
--      7                                                               8                 9                 10                 11
      , COALESCE(ROUND( AVG(t1.subscore) * 5    *[P2]  ,2),0.0) score , t1.location_lat , t1.location_lng , t1.location_text , t1.collections
FROM
    (
    SELECT t3.id, t3.title, t3.projectDir, t3.keywords, t3.date, t3.image, t3.notes , t3.site
                , t3.location_lat , t3.location_lng , t3.location_text , t3.collections,'F'  , /* t3.subscore --*/ sqrt.sqrt as subscore --t2.subscore
        FROM
        (
            SELECT  archive.id,archive.title,archive.projectDir,archive.keywords,archive.date,archive.image,archive.notes ,sites.site
                    , archive.location_lat , archive.location_lng , archive.location_text , archive.collections,'V'
                    ,ROUND( ( (CAST(sites.views  AS REAL )-sa.minViews) / (sa.maxViews-sa.minViews) ) ,2) subscore
            FROM archive
            LEFT JOIN sites ON sites.projectId=archive.id
            LEFT JOIN
                (
                SELECT  sites.site,
                        MAX(sites.views) maxViews,
                        MIN(sites.views) as minViews
                FROM sites
                        WHERE sites.notcount IS NULL
                GROUP BY sites.site
                ) sa  ON sa.site = sites.site
            WHERE sites.site != 'FB'
                AND notcount IS NULL
            AND archive.title LIKE '%'
            --AND sites.site='5P'
            [FILTER]
         ) t3
         LEFT JOIN sqrt ON sqrt.base = t3.subscore


    UNION

    SELECT t2.id, t2.title, t2.projectDir, t2.keywords, t2.date, t2.image, t2.notes , t2.site
            , t2.location_lat , t2.location_lng , t2.location_text , t2.collections,'F' , /*t2.subscore --,*/  sqrt.sqrt as subscore --t2.subscore
        FROM
        (

            SELECT  archive.id,archive.title,archive.projectDir,archive.keywords,archive.date,archive.image,archive.notes ,sites.site
                    , archive.location_lat , archive.location_lng , archive.location_text , archive.collections,'F'
                    ,ROUND( ( (CAST(sites.favs  AS REAL ) - sa.minFavs)  / (sa.maxFavs-sa.MinFavs) ) ,2) subscore
            FROM archive
            LEFT JOIN sites
            ON sites.projectId=archive.id
            LEFT JOIN
                (
                SELECT  sites.site,
                        MAX(sites.favs) maxFavs ,
                        MIN(sites.favs) as minFavs
                FROM sites
                        WHERE sites.notcount IS NULL
                GROUP BY sites.site
                ) sa ON sa.site = sites.site
            --WHERE sites.site != 'VB' AND archive.title LIKE '%'
            WHERE archive.title LIKE '%'
                AND notcount IS NULL
                --AND sites.site='5P'
            [FILTER]
         ) t2
         LEFT JOIN sqrt ON sqrt.base = t2.subscore

    ) t1
GROUP BY t1.title
ORDER BY  [ORDERBY] --COALESCE(ROUND( AVG(t1.subscore)    *[P2]  ,2),0.0) DESC , t1.title
