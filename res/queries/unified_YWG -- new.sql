SELECT archive.title title
     , archive.id id
     , archive.projectdir projectdir
     , archive.keywords kewywords
     , archive.date date
     , archive.image image
     , archive.notes notes
     , COALESCE(ROUND( score.score  * 5 *[P2] , 2) , 0.0 ) score
     , archive.location_lat location_lat
     , archive.location_lng location_lng
     , archive.location_text location_text
     , archive.collections collections
     , sts.sites sites
FROM archive
LEFT JOIN score ON archive.id=score.projectid
LEFT JOIN (SELECT projectid,GROUP_CONCAT(site) sites FROM sites GROUP BY projectid) sts ON sts.projectid = archive.id
--LEFT JOIN sqrt ON sqrt.base = score.score
WHERE 1  [FILTER]
ORDER BY [ORDERBY]
