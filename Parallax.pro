#-------------------------------------------------
#
# Project created by QtCreator 2014-08-01T12:54:27
#
#-------------------------------------------------

QT       += core gui sql webkitwidgets webkit
CONFIG   += openssl-linked

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Parallax
TEMPLATE = app

UI_DIR = !build
RCC_DIR = !build
MOC_DIR = !build
#OBJECTS_DIR = !build

#Debug:DESTDIR = !debug
#Release:DESTDIR = !release


SOURCES += main.cpp\
	mainwindow.cpp \
    dialogs/addarchivedialog.cpp \
    dialogs/addcollectiondialog.cpp \
    dialogs/addprocessdialog.cpp \
    dialogs/addsitedialog.cpp \
    dialogs/archiveeditdialog.cpp \
    dialogs/calendardialog.cpp \
    dialogs/compresswizarddialog.cpp \
    dialogs/libraryprefsdialog.cpp \
    dialogs/organizerdialogold.cpp \
    dialogs/organizerdialog.cpp \
    dialogs/organizerduplicatefiledialog.cpp \
    dialogs/organizerfilemovedialog.cpp \
    dialogs/ptsclonerdialog.cpp \
    dialogs/settingsdialog.cpp \
    dialogs/yesnodialog.cpp \
    dialogs/tablebrowserdialog.cpp \
    delegates/archivebrowserdelegate.cpp \
    delegates/collectionlistdelegate.cpp \
    delegates/processorbrowserdelegate.cpp \
    delegates/processorlistdelegate.cpp \
    managers/archivemanager.cpp \
    delegates/filepickerdelegate.cpp \
    managers/librarymanager.cpp \
    managers/settingsmanager.cpp \
    managers/uimanager.cpp \
    extensions/clickable_qlabel.cpp \
    extensions/overlay.cpp \
    extensions/tools.cpp \
    extensions/threadedimageloader.cpp \
    widgets/bargraphwidget.cpp \
    widgets/filepickerwidget.cpp \
    dialogs/siteupdater.cpp \
    widgets/smoothscrolllistview.cpp \
    widgets/verticalscrolllistview.cpp \
    autonemapper.cpp \
    processtoarchivedialog.cpp \
    mainwindow_processor.cpp \
    mainwindow_archive.cpp \
    threadedimagecacher.cpp \
    processmanager.cpp \
    dialogs/droppedselectdialog.cpp \
    clickable_qwidget.cpp \
    siteupdaternew.cpp \
    dialogs/syncdialog.cpp \
    widgets/linegraphwidget.cpp \
    dialogs/uploadtimesdialog.cpp \
    dialogs/extstatsdialog.cpp \
    dialogs/archivefolderflattendialog.cpp \
    dialogs/followersandpageviewsdialog.cpp \
    dialogs/addsaledialog.cpp \
    dialogs/uploaderdialog.cpp \
    dialogs/instagsdialog.cpp

HEADERS  += mainwindow.h \
    dialogs/addarchivedialog.h \
    dialogs/addcollectiondialog.h \
    dialogs/addprocessdialog.h \
    dialogs/addsitedialog.h \
    dialogs/calendardialog.h \
    dialogs/compresswizarddialog.h \
    dialogs/libraryprefsdialog.h \
    dialogs/organizerduplicatefiledialog.h \
    dialogs/organizerdialog.h \
    dialogs/organizerdialogold.h \
    dialogs/organizerfilemovedialog.h \
    dialogs/yesnodialog.h \
    delegates/archivebrowserdelegate.h \
    dialogs/archiveeditdialog.h \
    delegates/collectionlistdelegate.h \
    delegates/filepickerdelegate.h \
    delegates/processorbrowserdelegate.h \
    delegates/processorlistdelegate.h \
    dialogs/ptsclonerdialog.h \
    dialogs/tablebrowserdialog.h \
    managers/archivemanager.h \
    managers/librarymanager.h \
    dialogs/settingsdialog.h \
    managers/settingsmanager.h \
    managers/uimanager.h \
    dialogs/siteupdater.h \
    widgets/bargraphwidget.h \
    extensions/clickable_qlabel.h \
    widgets/filepickerwidget.h \
    extensions/overlay.h \
    widgets/smoothscrolllistview.h \
    extensions/tools.h \
    widgets/verticalscrolllistview.h \
    extensions/threadedimageloader.h \
    autonemapper.h \
    processtoarchivedialog.h \
    threadedimagecacher.h \
    processmanager.h \
    dialogs/droppedselectdialog.h \
    clickable_qwidget.h \
    siteupdaternew.h \
    dialogs/syncdialog.h \
    widgets/linegraphwidget.h \
    dialogs/uploadtimesdialog.h \
    dialogs/extstatsdialog.h \
    dialogs/archivefolderflattendialog.h \
    dialogs/followersandpageviewsdialog.h \
    dialogs/addsaledialog.h \
    dialogs/uploaderdialog.h \
    dialogs/instagsdialog.h

FORMS    += \
    ui/addarchivedialog.ui \
    ui/addcollectiondialog.ui \
    ui/addprocessdialog.ui \
    ui/addsitedialog.ui \
    ui/mainwindow.ui \
    ui/archiveeditdialog.ui \
    ui/calendardialog.ui \
    ui/compresswizarddialog.ui \
    ui/libraryprefsdialog.ui \
    ui/organizerdialog.ui \
    ui/organizerdialogold.ui \
    ui/organizerduplicatefiledialog.ui \
    ui/organizerfilemovedialog.ui \
    ui/ptsclonerdialog.ui \
    ui/settingsdialog.ui \
    ui/siteupdater.ui \
    ui/tablebrowserdialog.ui \
    ui/yesnodialog.ui \
    autonemapper.ui \
    processtoarchivedialog.ui \
    ui/droppedselectdialog.ui \
    siteupdaternew.ui \
    dialogs/syncdialog.ui \
    dialogs/uploadtimesdialog.ui \
    dialogs/extstatsdialog.ui \
    dialogs/archivefolderflattendialog.ui \
    dialogs/followersandpageviewsdialog.ui \
    dialogs/addsaledialog.ui \
    dialogs/uploaderdialog.ui \
    dialogs/instagsdialog.ui

RESOURCES += \
    res/res.qrc \
    res/stylesheets.qrc \
    res/queries.qrc \
    res/html.qrc
