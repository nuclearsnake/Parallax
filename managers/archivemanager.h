#ifndef ARCHIVEMANAGER_H
#define ARCHIVEMANAGER_H

#include <QObject>
#include <QSqlQueryModel>
#include <QStandardItemModel>
#include <QWidgetItem>
#include <QTreeWidgetItem>
#include <QList>
#include <QString>
#include <QTimer>
#include <QFileSystemModel>
#include <QIcon>
#include <QLabel>
#include <QWidget>

#include "librarymanager.h"
#include "clickable_qwidget.h"

#include "delegates/archivebrowserdelegate.h"
#include "delegates/filepickerdelegate.h"

namespace Ui {
class MainWindow;
}

class archiveManager : public QObject
{
    Q_OBJECT



    QStringList getItemTypeCollections();

public:
    explicit archiveManager(QObject *parent = 0);

    //new
    QString scoringMode;

    // class definitions
    class SidebarItem
        {
        public:
            explicit SidebarItem(QString Name,clickable_QWidget *Header,QLabel *ToggleIcon,QWidget *Content,QWidget *Shadow)
                {
                    this->name          = Name;
                    this->header        = Header;
                    this->toggleIcon    = ToggleIcon;
                    this->content       = Content;
                    this->shadow        = Shadow;
                    visible             = true;
                }

            void toggle()
                {
                qDebug() << "                   TOGGLE:" << this->content->isVisible() << this->getVisible();
                if (this->getVisible())
                    {
                        qDebug() << "                   TOGGLE: OFF";
                        this->content->setVisible(false);
                        this->shadow->setVisible(false);
                        this->setVisible(false);
                        this->toggleIcon->setPixmap(QPixmap(":/icons/icons/stylesheet-branch-left.png"));
                    }
                    else
                    {
                        qDebug() << "                   TOGGLE: ON";
                        this->content->setVisible(true);
                        this->shadow->setVisible(true);
                        this->setVisible(true);
                        this->toggleIcon->setPixmap(QPixmap(":/icons/icons/stylesheet-branch-open.png"));
                    }

                }

            QString             name;
            QLabel              *toggleIcon;
            QWidget             *content;
            QWidget             *shadow;
            clickable_QWidget   *header;
            bool                visible;

            bool getVisible() const
            {
                return visible;
            }
            void setVisible(bool value)
            {
                visible = value;
            }
    };

    class Icons
    {
    public:
        Icons(){}
            QIcon arrow_right;
            QIcon arrow_down;

        };

    class Selected
        {
        public:
            Selected(){}
            QString image;
            QString title;
            int     id;
            QString folder;
            QString date;
            QPixmap pixmap;
            float   pixmapAspectRatio;
            int     pixmapHeight;
            int     pixmapWidth;
        };

    // instantiated classes
    Selected    *selected;
    Icons       *icons;
    QTimer      *fullMapTimer;
    QList<SidebarItem*> sidebarItems;

    // UI
    Ui::MainWindow *ui;

    // delegates
    archiveBrowserDelegate  *abDelegate;
    filePickerDelegate      *fDelegate;

    //models
    QStandardItemModel  *fModel;
    QSqlQueryModel      *model;
    QSqlQueryModel      *collectionModel;
    QStandardItemModel  *modelTree;
    QStandardItemModel  *modelThumb;
    QFileSystemModel    *fileModel;
    QSqlQueryModel      *searchModel;

    // managers
    libraryManager *libMan;

    // variables
    bool    treeGrouped;
    QString groupType;
    QString filterMode;
    QList<QString> additionalFilter;
    QString additionalFilter_notinSite;
    QString filterOpt;
    QString browserLastBranch;
    QList<QPair<QString,QString> > urlList;
    bool additionalFilter_On;
    bool filterOneSite_On;
    QString onesiteFilter;

    // functions
    QString calculateFolderSize(QString f);
    QTreeWidgetItem folderTree(QString folder);
    QModelIndex findItem(QString title);
    QList<QString> getTitles();
    void treeModelConvert();
    void thumbModelConvert();
    void filterDo(QString filterStr);
    void attachUi(Ui::MainWindow *u);
    void attachLibMan(libraryManager *l);
    void testUi();
    void createFilterMenu();
    void browserHighlightBranch(QModelIndex index);
    void setSelected(QModelIndex index);
    void sidebarMapUpdate(QModelIndex index);
    void debugSelected();
    void updateUI(QModelIndex index);
    void fileNaviUpdate(QString folder);
    void previewImageLoad();

    void sitesGet();
    void scoreGet();
    void browserSelectByTitle();
    void init();
    void collectionModelQueryReset();
    void groupSwitcher();
    void initConnections();
    void browserFocus();
    void treeFocus();
    QModelIndex modelTreeIndexOf(QString title);
    void reConvert();
    void modelSetQueryAndFetch(QString query);
    void refresh();
    void init_1();
    void init_2();

    bool selectByTitle(QString title);
    void selectFirst();
    void browserHighlightClear(QModelIndex index);
    void sidebarImageLoad();
    void buildGraph();
    QSqlQueryModel *interpolator(QString val);
    void getSales();
signals:
    void overlayMsg(QString msg);
    void openWithPS(QStringList arg);
    void openWithPhotoMatix(QStringList arg);
    void openWithPTGui(QStringList arg);

public slots:
    void fileNaviThumbResize();
    // archive tree
    void treeSwitchGrouped();
    void groupBySelect();
    void treeExpandCollapse(bool state);

    // filter
    void filter(QString text);

    // files
    void fileDoubleClick(QModelIndex index);
    void fileNaviLoadFolder(QModelIndex index);
    void fileNaviToggle();
    void fileNaviSetRoot();

    // web
    void webBack();
    void webForward();
    void webUrlUpdate(QUrl url);
    void webProgress(int p);

    // browser
    void browserSetThumbSize(int s);
    void browserZoomlevelHideText();
    void browserDoubleClick(QModelIndex index);
    void browserSelect(QModelIndex index);

    // common
    void treeSetIndex(QModelIndex index);
    void select(QModelIndex index);
    void focusLists();

    // map
    void fullMapRefresh();

    // collections
    void collect();
    void collectionTargetCycle();
    void collectionSelect(QModelIndex index);
    void collectionContextMenu(const QPoint &pos);

    // notes
    void noteUpdateButtonEnable();

    // dialogs
    void dialog_collectionAdd();
    void dialog_organizer();
    void dialog_browserAll();
    void dialog_notesBrowser();
    void dialog_sitesBrowser();
    void dialog_edit();
    void dialog_compressor();
    void dialog_scores();
    void dialog_addNewProxy();
    void dialog_addNew(QString preDir);
    void dialog_siteUpdater();
    void dialog_addSite();
    void dialog_siteManager();

    // tabs
    void tab_folderSizes();
    void tab_monthlyUploads();

    // sidebar
    void sidebarMapSaveLoc();
    void sidebarMapPin(QString s);
    void keywordCloud();
    void keywordCloudDoubleClicked(QModelIndex index);
    void keywordCloudClicked(QModelIndex index);
    void quickTagClick(QModelIndex index);
    void quickTag();
    void advancedFilterNotin_toggle(bool en);
    void advancedFilterNotin();
    void tabClose(int i);

    void siteClick();
    void keywordAdd();
    void keywordCompleter(QString text);
    void keywordsToClipboradSpaces();
    void keywordsToClipboradCommas();
    void notesUpdate();
    void keywordsUpdate();
    void keywordUpdateButtonEnable();
    void filterMenu(QAction *action);
    void filterClear();
    void fileContextMenu(const QPoint &pos);
    void browserContextMenu(const QPoint &pos);
    void selectNext();
    void selectPrev();
    void previewImageResize();
    void tabChanged(int tab);
    void tableDblClick(QModelIndex index);
    void sidebarBlockToggle();
    void titleToClipboard();
    void dialog_siteUpdater_single();
    void dialog_syncSentinel();
    void dialog_uploadTimes();



    void dialog_flattenFolder();
    void dialog_FPV();
    void dialog_addSale();
    void dialog_uploader();
private slots:
    void openFolderInExplorer();
    void splitter1Moved(int a, int b);
};

#endif // ARCHIVEMANAGER_H
