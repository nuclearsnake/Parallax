#include "settingsmanager.h"

#include <QDebug>


settingsManager::settingsManager(QObject *parent) :
    QObject(parent)
{
    programName     =   "Parallax";

    versionMain     =    0;
    versionSub      =   93;
    versionBuild    =  332;

    programVersion.sprintf("%d.%02d.%03d",versionMain,versionSub,versionBuild);
    //programVersion  =   QString::number(versionMain) + "." + QString::number(versionSub) + "." + QString::number(versionBuild);
    processTreeQueryDateSort = "SELECT  projectname,id,path,isPano,thumbnail,icondata FROM projects WHERE isFinished == 0  ORDER BY id";
    //                                  0           1  2    3      4         5
    processTreeQueryNameSort = "SELECT  projectname,id,path,isPano,thumbnail,icondata FROM projects WHERE isFinished == 0 AND (isDeleted == 0 OR isDeleted IS NULL)  ORDER BY projectname";
    archiveTreeQuery         = "SELECT  title,id,projectDir,keywords,date,image,notes FROM archive ORDER BY title";
    processTreeQuery         = processTreeQueryNameSort;
    QCoreApplication::setOrganizationName("SkyLark Corp.");
    QCoreApplication::setOrganizationDomain("realitydream.hu");
    QCoreApplication::setApplicationName("Parallax");
    settings = new QSettings ;
}

void settingsManager::save(QString key,QString value)
{
    settings->sync();
    settings->setValue(key,value);
    settings->sync();
}

void settingsManager::remove(QString key)
{
    settings->sync();
    settings->remove(key);
    settings->sync();
}

bool settingsManager::contains(QString key)
{
    settings->sync();
    return settings->contains(key);

}

QString settingsManager::value(QString key)
{
    settings->sync();
    return settings->value(key).toString();

}

void settingsManager::saveHistory(QString value)
{
    settings->sync();
    if ( settings->contains("History/lastFilePath") )
        if (settings->value("History/lastFilePath").toString()!=value)
            {
                if (settings->contains("History/lastFilePath_3"))
                    settings->setValue("History/lastFilePath_4",settings->value("History/lastFilePath_3").toString());
                if (settings->contains("History/lastFilePath_2"))
                    settings->setValue("History/lastFilePath_3",settings->value("History/lastFilePath_2").toString());
                if (settings->contains("History/lastFilePath_1"))
                    settings->setValue("History/lastFilePath_2",settings->value("History/lastFilePath_1").toString());
                settings->setValue("History/lastFilePath_1",settings->value("History/lastFilePath").toString());
            }
    settings->setValue("History/lastFilePath",value);
    settings->sync();
}

QStringList settingsManager::loadHistory()
{
    QStringList result;
    if (settings->contains("History/lastFilePath"))   result << settings->value("History/lastFilePath").toString();
    if (settings->contains("History/lastFilePath_1")) result << settings->value("History/lastFilePath_1").toString();
    if (settings->contains("History/lastFilePath_2")) result << settings->value("History/lastFilePath_2").toString();
    if (settings->contains("History/lastFilePath_3")) result << settings->value("History/lastFilePath_3").toString();
    if (settings->contains("History/lastFilePath_4")) result << settings->value("History/lastFilePath_4").toString();

    return result;
}

void settingsManager::setDb(QSqlDatabase database)
{
    qDebug() << "setdb";
    qDebug() << database;
    //db = d;
    //db.open();
    //d.open();
    QSqlQueryModel *modelTest = new QSqlQueryModel(this);
    modelTest->setQuery("SELECT * FROM archive",database);
    qDebug() << "libman db set";
    qDebug() << modelTest->rowCount();
}

void settingsManager::saveWindowParams(QByteArray geometry, QByteArray state)
{
    this->save("GeneralSettings/mainWindowGeometry",geometry.toBase64());
    qDebug() << "    save window geometry";
    this->save("GeneralSettings/mainWindowState",state.toBase64());
    qDebug() << "    save window state";
}

