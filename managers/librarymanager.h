#ifndef LIBRARYMANAGER_H
#define LIBRARYMANAGER_H

#include <QObject>
#include <QtSql>


class libraryManager : public QObject
{
    Q_OBJECT
public:
    explicit libraryManager(QObject *parent = 0);
    QString libraryPath;
    QString libraryFolder;
    QSqlDatabase db;
    float scoreBias;
    void loadFrom(QString path);
    QString loadQuery(QString fileName);
    void archiveZoomlevelSave(int value);
    void archiveSelectedTitleSave(QString value);
    void processSelectedTitleSave(QString value);
    void archiveTreeExpandStateSave(int state);
    void archiveTreeGroupedStateSave(int state);
    void archiveGroupTypeSave(QString value);


    void currentModuleSave(QString value);
    void archiveFileNaviSizeSave(QString value);
    void archiveFileNaviVisibleSave(QString value);
    QString getCacheFolder();
    void archiveSidebarStatusSave(QString value);
signals:
    void libraryLoaded();

public slots:

    void open();
    void createNew();
    void load();
};

#endif // LIBRARYMANAGER_H
