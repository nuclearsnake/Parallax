#ifndef SETTINGSMANAGER_H
#define SETTINGSMANAGER_H

#include <QtSql>
#include <QObject>
#include <QCoreApplication>
#include <QSettings>
#include <QString>
#include <QStringList>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlQueryModel>


class settingsManager : public QObject
{
    Q_OBJECT
public:
    explicit    settingsManager(QObject *parent = 0);
    QSettings   *settings;
    void        save(QString key, QString value);
    void        saveHistory(QString value);
    QStringList loadHistory();
    QString     programName;
    int         versionMain;
    int         versionSub;
    int         versionBuild;
    QString     programVersion;
    QString     processTreeQuery;
    QString     processTreeQueryDateSort;
    QString     processTreeQueryNameSort;
    QString     archiveTreeQuery;
    QSqlDatabase db;

    void remove(QString key);
    bool contains(QString key);
    QString value(QString key);
    void setDb(QSqlDatabase database);

    void saveWindowParams(QByteArray geometry,QByteArray state);
signals:

public slots:

};

#endif // SETTINGSMANAGER_H
