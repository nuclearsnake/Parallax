#include "archivemanager.h"
#include "extensions/tools.h"
#include "widgets/bargraphwidget.h"
#include "dialogs/yesnodialog.h"
#include "dialogs/addcollectiondialog.h"
#include "dialogs/organizerdialog.h"
#include "dialogs/tablebrowserdialog.h"
#include "dialogs/addarchivedialog.h"
#include "dialogs/compresswizarddialog.h"
#include "dialogs/siteupdater.h"
#include "siteupdaternew.h"
#include "dialogs/addsitedialog.h"
#include "dialogs/syncdialog.h"
#include "dialogs/uploadtimesdialog.h"
#include "dialogs/extstatsdialog.h"
#include "dialogs/archivefolderflattendialog.h"
#include "dialogs/followersandpageviewsdialog.h"
#include "dialogs/addsaledialog.h"
#include "dialogs/uploaderdialog.h"

#include "delegates/collectionlistdelegate.h"

#include "ui_mainwindow.h"

#include "threadedimagecacher.h"

#include <QDirIterator>
#include <QMenu>
#include <QWebFrame>
#include <QWebHistory>
#include <QNetworkDiskCache>
#include <QVBoxLayout>
#include <QTableView>
#include <QCompleter>
#include <QClipboard>
#include <QAction>
#include <QDesktopServices>
#include <QShortcut>
#include <QKeySequence>
#include <QScrollBar>


archiveManager::archiveManager(QObject *parent) :
    QObject(parent)
{
    // init classes
    selected    = new Selected();
    icons       = new Icons();
    icons->arrow_down   = QIcon(":/icons/icons/stylesheet-branch-open.png");
    icons->arrow_right  = QIcon(":/icons/icons/stylesheet-branch-closed.png");

    // init models
    modelTree       = new QStandardItemModel(this);
    modelThumb      = new QStandardItemModel(this);
    model           = new QSqlQueryModel(this);



    // defaults
    this->treeGrouped=true;
    this->groupType="A-Z";
    this->filterMode="T"; // TITLE filter mode default
    this->additionalFilter_notinSite="DA";
    this->additionalFilter_On = false;


}

//----@INITS----

void archiveManager::attachUi(Ui::MainWindow *u)
{
    ui = u;
}

void archiveManager::attachLibMan(libraryManager *l)
{
    libMan = l;


}

void archiveManager::init()
{
    // models
    fileModel   = new QFileSystemModel(this);
    fModel      = new QStandardItemModel(this);

    // delegates
    ui->listView_collections->setItemDelegate(new collectionListDelegate());
    abDelegate = new archiveBrowserDelegate();
    fDelegate = new filePickerDelegate;
    fDelegate->thumbWidth = 100;
    // ui <=> model


    // browser
    ui->listView_archiveBrowser->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
    ui->widget_af_ni->setVisible(false); // advanced filter

    // tree
    ui->treeView_archiveTree->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->listView_archiveTree->setVisible(false);

    // buttons
    ui->pushButton_archiveFilterClear->setToolTip("Clear filter");
    ui->pushButton_archiveCopyKeyComma->setToolTip("Copy to clipboard with ','");
    ui->pushButton_archiveCopyKeySpace->setToolTip("Copy to clipboard with spaces");    
    ui->pushButton_archiveTreeExpand->setIcon(QIcon(":/icons/icons/stylesheet-branch-closed.png"));
    ui->pushButton_locSave->setEnabled(false);
    ui->pushButton_archiveSearchByKeywords->setVisible(false);
    ui->pushButton_archiveSearchByTitle->setVisible(false);

    // FILES

        // tree
        //ui->treeView_archiveFiles->setVisible(false);
        ui->treeView_archiveFiles->setModel(fileModel);
        ui->treeView_archiveFiles->hideColumn(1);
        ui->treeView_archiveFiles->hideColumn(2);
        ui->treeView_archiveFiles->hideColumn(3);
        ui->treeView_archiveFiles->hideColumn(4);
        ui->treeView_archiveFiles->hideColumn(5);
        ui->treeView_archiveFiles->hideColumn(6);


        // list
        tools::listViewIconMode(ui->listView_archiveFiles);
        ui->listView_archiveFiles->setModel(fModel);
        ui->listView_archiveFiles->setEditTriggers(QAbstractItemView::NoEditTriggers);
        ui->listView_archiveFiles->setItemDelegate(fDelegate);
        ui->listView_archiveFiles->setWrapping(false);
        ui->listView_archiveFiles->setFlow(QListView::LeftToRight);
        ui->listView_archiveFiles->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded );
        ui->listView_archiveFiles->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        ui->listView_archiveFiles->setDragEnabled(true);



    // LABELS
    ui->label_archiveDate->setText("");
    ui->label_archiveTitle->setText("");



    //mapRefreshTimer = new QTimer(this);
    //connect (  archive->fullMapTimer  , SIGNAL(timeout())     , archive  , SLOT(fullMapRefresh()) );
    //connect ( ui->tabWidget_archiveCenter   , SIGNAL(currentChanged(int))   ,   this    , SLOT(archiveTabBarClickProxy(int)) );
    //archive->fileModel = new QFileSystemModel(this);




    // tab bar
    ui->tabWidget_archiveCenter->tabBar()->tabButton(0, QTabBar::RightSide)->resize(0,0);
    ui->tabWidget_archiveCenter->tabBar()->tabButton(1, QTabBar::RightSide)->resize(0,0);
    ui->tabWidget_archiveCenter->tabBar()->tabButton(2, QTabBar::RightSide)->resize(0,0);
    ui->tabWidget_archiveCenter->tabBar()->tabButton(3, QTabBar::RightSide)->resize(0,0);

    // network cache
    QNetworkDiskCache *cache = new QNetworkDiskCache(this);
    cache->setCacheDirectory(QStandardPaths::standardLocations( QStandardPaths::DataLocation ).at(0) + "/cache");
    cache->setMaximumCacheSize(1073741824);

    // web views
    ui->webView->page()->networkAccessManager()->setCache(cache);
    ui->webView_fullMap->page()->networkAccessManager()->setCache(cache);
    ui->webView_Map->page()->networkAccessManager()->setCache(cache);
    ui->webView_fullMap->page()->settings()->setAttribute(QWebSettings::DeveloperExtrasEnabled, true);
    ui->webView->page()->settings()->setAttribute(QWebSettings::DeveloperExtrasEnabled, true);
    connect ( ui->webView_Map->page()->mainFrame()   , SIGNAL( titleChanged(QString))  , this  ,SLOT(archiveMapPin(QString)) ) ;

    // context menus
    ui->listView_collections->setContextMenuPolicy(Qt::CustomContextMenu);
    ui->listView_archiveBrowser->setContextMenuPolicy(Qt::CustomContextMenu);
    ui->listView_processTree->setContextMenuPolicy(Qt::CustomContextMenu);
    ui->listView_archiveFiles->setContextMenuPolicy(Qt::CustomContextMenu);

    // other
    ui->listView_quickTag->setVisible(false);
    ui->splitter->setStretchFactor(0,1);
    ui->splitter->setStretchFactor(1,0);


    // TEST
   // ui->widget_archive_sidebar_map_content->setVisible(false);

    ui->scrollArea->setStyleSheet("QScrollBar:vertical{width:5px;background:#232323;}QScrollBar::handle:vertical{}");


}

void archiveManager::init_1()
{
    collectionModel = new QSqlQueryModel(this);
    collectionModel->setQuery("SELECT c.name,c.type,c.param,c.id,var.key FROM collections c LEFT JOIN var ON var.value = c.id AND var.key='targetCollection' ORDER BY name COLLATE NOCASE",libMan->db);
    ui->listView_collections->setModel(collectionModel);
}

void archiveManager::init_2()
{
    model = new QSqlQueryModel(this);
    model->setQuery("SELECT  title,id,projectDir,keywords,date,image,notes FROM archive ORDER BY title",libMan->db);
    while(model->canFetchMore()) model->fetchMore();
    ui->listView_archiveTree->setModel(model);
    treeModelConvert();
    thumbModelConvert();
    ui->treeView_archiveTree->setModel(modelTree);
    ui->listView_archiveBrowser->setModel(modelThumb);
    abDelegate->libraryFolder = libMan->libraryFolder;
    abDelegate->thumbSize = 180;
    abDelegate->parentWidth = ui->listView_archiveBrowser->width();
    abDelegate->style = "";
    ui->listView_archiveBrowser->setItemDelegate(abDelegate);
    tools::listViewIconMode(ui->listView_archiveBrowser);
    ui->tabWidget_archiveTrees->setTabText(0,"Archives ("+QString::number(model->rowCount())+")");
    keywordCompleter("");

    // sidebar items init

    sidebarItems << new  SidebarItem("preview",
                                 ui->widget_archive_sidebar_preview_header,
                                  ui->label_archive_sidebar_preview_toggle,
                                 ui->widget_archive_sidebar_preview_content,
                                 ui->widget_archive_sidebar_preview_shadow);

    sidebarItems <<  new  SidebarItem("info",
                                 ui->widget_archive_sidebar_info_header,
                                  ui->label_archive_sidebar_info_toggle,
                                 ui->widget_archive_sidebar_info_content,
                                 ui->widget_archive_sidebar_info_shadow);
    sidebarItems <<  new  SidebarItem("map",
                                 ui->widget_archive_sidebar_map_header,
                                  ui->label_archive_sidebar_map_toggle,
                                 ui->widget_archive_sidebar_map_content,
                                 ui->widget_archive_sidebar_map_shadow);
    sidebarItems <<  new  SidebarItem("score",
                                 ui->widget_archive_sidebar_score_header,
                                  ui->label_archive_sidebar_score_toggle,
                                 ui->widget_archive_sidebar_score_content,
                                 ui->widget_archive_sidebar_score_shadow);
    sidebarItems <<  new  SidebarItem("sites",
                                 ui->widget_archive_sidebar_sites_header,
                                  ui->label_archive_sidebar_sites_toggle,
                                 ui->widget_archive_sidebar_sites_content,
                                 ui->widget_archive_sidebar_sites_shadow);
    sidebarItems <<  new  SidebarItem("keywords",
                                 ui->widget_archive_sidebar_keywords_header,
                                  ui->label_archive_sidebar_keywords_toggle,
                                 ui->widget_archive_sidebar_keywords_content,
                                 ui->widget_archive_sidebar_keywords_shadow);
    sidebarItems <<  new  SidebarItem("notes",
                                 ui->widget_archive_sidebar_notes_header,
                                  ui->label_archive_sidebar_notes_toggle,
                                 ui->widget_archive_sidebar_notes_content,
                                 ui->widget_archive_sidebar_notes_shadow);
    sidebarItems <<  new  SidebarItem("tools",
                                 ui->widget_archive_sidebar_tools_header,
                                  ui->label_archive_sidebar_tools_toggle,
                                 ui->widget_archive_sidebar_tools_content,
                                 ui->widget_archive_sidebar_tools_shadow);


    /*
    if(ui->widget_archive_sidebar_preview_content->isVisible())
        {
            ui->widget_archive_sidebar_preview_content->setVisible(false);
            ui->label_archive_sidebar_preview_toggle->setPixmap(QPixmap(":/icons/icons/stylesheet-branch-left.png"));
            */

}

void archiveManager::initConnections()
{
    // TREE
    connect( ui->treeView_archiveTree   , SIGNAL(clicked(QModelIndex))  , this  , SLOT(select(QModelIndex)) );

    // BROWSER
    connect( ui->listView_archiveBrowser    , SIGNAL(clicked(QModelIndex))                  , this  , SLOT(browserSelect(QModelIndex)) );
    connect( ui->listView_archiveBrowser    , SIGNAL(doubleClicked(QModelIndex))            , this  , SLOT(browserDoubleClick(QModelIndex)));
    connect( ui->listView_archiveBrowser    , SIGNAL(customContextMenuRequested(QPoint))    , this  , SLOT(browserContextMenu(QPoint))  );
    connect( ui->horizontalSlider_archiveThumbSize  , SIGNAL(valueChanged(int))             , this  , SLOT(browserSetThumbSize(int)) );

    // KEYWORDS
    connect( ui->plainTextEdit_archiveKeywords      , SIGNAL(textChanged()) , this  , SLOT(keywordUpdateButtonEnable()) );
    connect( ui->pushButton_archiveUpdateKeywords   , SIGNAL(clicked())     , this  , SLOT(keywordsUpdate()) );
    connect( ui->pushButton_archiveCopyKeySpace     , SIGNAL(clicked())     , this  , SLOT(keywordsToClipboradSpaces())   );
    connect( ui->pushButton_archiveCopyKeySpace_1   , SIGNAL(clicked())     , this  , SLOT(keywordsToClipboradSpaces())   );
    connect( ui->pushButton_archiveCopyKeyComma     , SIGNAL(clicked())     , this  , SLOT(keywordsToClipboradCommas())   );
    connect( ui->pushButton_archiveCopyKeyComma_1   , SIGNAL(clicked())     , this  , SLOT(keywordsToClipboradCommas())   );
    connect( ui->pushButton_quickTag                , SIGNAL(clicked())     , this  , SLOT(quickTag()) );
    connect( ui->listView_quickTag                  , SIGNAL(clicked(QModelIndex))  , this  , SLOT(quickTagClick(QModelIndex))  );
    connect( ui->lineEdit_addKeyword                , SIGNAL(returnPressed())       , this  , SLOT(keywordAdd()) );

    // NOTES
    connect( ui->plainTextEdit_notes            , SIGNAL(textChanged()) , this  , SLOT(noteUpdateButtonEnable()) );
    connect( ui->pushButton_archiveUpdateNotes  , SIGNAL(clicked())     , this  , SLOT(notesUpdate()) );


    // GROUPING
    connect( ui->pushButton_archiveGroupByAz    , SIGNAL(clicked()) , this  , SLOT(groupBySelect())     );
    connect( ui->pushButton_archiveGroupByDate  , SIGNAL(clicked()) , this  , SLOT(groupBySelect())     );
    connect( ui->pushButton_archiveGroupByLoc   , SIGNAL(clicked()) , this  , SLOT(groupBySelect())     );
    connect( ui->pushButton_archiveGroupByScore , SIGNAL(clicked()) , this  , SLOT(groupBySelect())     );
    connect( ui->pushButton_archiveTreeGroup    , SIGNAL(clicked()) , this  , SLOT(treeSwitchGrouped()) );
    connect( ui->pushButton_archiveTreeExpand   , SIGNAL(clicked(bool)) , this  , SLOT(treeExpandCollapse(bool)) );

    // SITES
    connect( ui->label_siteDaIcon           , SIGNAL(Mouse_Pressed())   , this  , SLOT(siteClick()) );
    connect( ui->label_site500pxIcon        , SIGNAL(Mouse_Pressed())   , this  , SLOT(siteClick()) );
    connect( ui->label_siteFlickrIcon       , SIGNAL(Mouse_Pressed())   , this  , SLOT(siteClick()) );
    connect( ui->label_sitePixotoIcon       , SIGNAL(Mouse_Pressed())   , this  , SLOT(siteClick()) );
    connect( ui->label_siteFBIcon           , SIGNAL(Mouse_Pressed())   , this  , SLOT(siteClick()) );
    connect( ui->label_siteYoupicIcon       , SIGNAL(Mouse_Pressed())   , this  , SLOT(siteClick()) );
    connect( ui->label_siteRdIcon           , SIGNAL(Mouse_Pressed())   , this  , SLOT(siteClick()) );
    connect( ui->label_siteGPIcon           , SIGNAL(Mouse_Pressed())   , this  , SLOT(siteClick()) );
    connect( ui->label_siteViewbugIcon      , SIGNAL(Mouse_Pressed())   , this  , SLOT(siteClick()) );
    connect( ui->label_siteIgIcon           , SIGNAL(Mouse_Pressed())   , this  , SLOT(siteClick()) );

    connect( ui->pushButton_sitesManage     , SIGNAL(clicked())         , this  , SLOT(dialog_siteManager()) );
    connect( ui->pushButton_sitesManage_1   , SIGNAL(clicked())         , this  , SLOT(dialog_siteManager()) );
    connect( ui->pushButton_addNewSite      , SIGNAL(clicked())         , this  , SLOT(dialog_addSite()) );
    connect( ui->pushButton_addNewSite_1    , SIGNAL(clicked())         , this  , SLOT(dialog_addSite()) );
    connect( ui->pushButton_sitesUpdate     , SIGNAL(clicked())         , this  , SLOT(dialog_siteUpdater_single()) );
    connect( ui->pushButton_addSale         , SIGNAL(clicked())         , this  , SLOT(dialog_addSale())  );
    connect( ui->pushButton_upload          , SIGNAL(clicked())         , this  , SLOT(dialog_uploader()) );

    // FILTER
    connect( ui->lineEdit_archiveFilter         , SIGNAL(textChanged(QString))  , this  , SLOT(filter(QString)) );
    connect( ui->pushButton_archiveFilterClear  , SIGNAL(clicked())             , this  , SLOT(filterClear())   );
    connect( ui->checkBox_aF_notin              , SIGNAL(clicked(bool))         , this  , SLOT(advancedFilterNotin_toggle(bool)) );
    connect( ui->radioButton_FUP                , SIGNAL(clicked(bool))         , this  , SLOT(advancedFilterNotin_toggle(bool)) );
    connect( ui->radioButton_FNUP               , SIGNAL(clicked(bool))         , this  , SLOT(advancedFilterNotin_toggle(bool)) );
    connect( ui->label_aF_ni5P                  , SIGNAL(Mouse_Pressed())       , this  , SLOT(advancedFilterNotin()) );
    connect( ui->label_aF_niDA                  , SIGNAL(Mouse_Pressed())       , this  , SLOT(advancedFilterNotin()) );
    connect( ui->label_aF_niFB                  , SIGNAL(Mouse_Pressed())       , this  , SLOT(advancedFilterNotin()) );
    connect( ui->label_aF_niFL                  , SIGNAL(Mouse_Pressed())       , this  , SLOT(advancedFilterNotin()) );
    connect( ui->label_aF_niGP                  , SIGNAL(Mouse_Pressed())       , this  , SLOT(advancedFilterNotin()) );
    connect( ui->label_aF_niPX                  , SIGNAL(Mouse_Pressed())       , this  , SLOT(advancedFilterNotin()) );
    connect( ui->label_aF_niYP                  , SIGNAL(Mouse_Pressed())       , this  , SLOT(advancedFilterNotin()) );
    connect( ui->label_aF_niVB                  , SIGNAL(Mouse_Pressed())       , this  , SLOT(advancedFilterNotin()) );
    connect( ui->label_aF_niRD                  , SIGNAL(Mouse_Pressed())       , this  , SLOT(advancedFilterNotin()) );
    connect( ui->label_aF_niIG                  , SIGNAL(Mouse_Pressed())       , this  , SLOT(advancedFilterNotin()) );

    // COLLECTIONS
    connect ( ui->listView_collections      , SIGNAL(clicked(QModelIndex))                  , this  , SLOT(collectionSelect(QModelIndex)) );
    connect ( ui->listView_collections      , SIGNAL(customContextMenuRequested(QPoint))    , this  , SLOT(collectionContextMenu(QPoint)) );
    connect ( ui->pushButton_addCollection  , SIGNAL(clicked())                             , this  , SLOT(dialog_collectionAdd()) );

    // KEYBOARD SHORTCUTS
    //new QShortcut ( QKeySequence(Qt::Key_D)     ,this   ,SLOT(collectionTargetCycle()));

    // MISC
    connect( ui->label_sideBarPreview           , SIGNAL(Mouse_Pressed())           , this  , SLOT(focusLists()) );
    connect( ui->pushButton_naviPrev            , SIGNAL(clicked())                 , this  , SLOT(selectPrev()) );
    connect( ui->pushButton_naviNext            , SIGNAL(clicked())                 , this  , SLOT(selectNext()) );
    connect( ui->splitter                       , SIGNAL(splitterMoved(int,int))    , this  , SLOT(splitter1Moved(int,int)) );
    connect( ui->pushButton_archiveCopyTitle    , SIGNAL(clicked())                 , this  , SLOT(titleToClipboard()));

    // TABS
    connect( ui->tabWidget_archiveCenter    , SIGNAL(tabCloseRequested(int))    , this  , SLOT(tabClose(int))   );
    connect( ui->tabWidget_archiveCenter    , SIGNAL(currentChanged(int))       , this  , SLOT(tabChanged(int)) );


    // SIDEBAR
    connect( ui->webView_Map        , SIGNAL(titleChanged(QString)) , this  , SLOT(sidebarMapPin(QString)) );
    connect( ui->pushButton_locSave , SIGNAL(clicked())             , this  , SLOT(sidebarMapSaveLoc()) );
    connect( ui->pushButton_archiveEditData             , SIGNAL(clicked())         , this  , SLOT(dialog_edit()) );
    connect( ui->pushButton_archiveEditData_1           , SIGNAL(clicked())         , this  , SLOT(dialog_edit()) );
    connect( ui->widget_archive_sidebar_preview_header  , SIGNAL(Mouse_Pressed())   , this  , SLOT(sidebarBlockToggle()) );
    connect( ui->widget_archive_sidebar_info_header     , SIGNAL(Mouse_Pressed())   , this  , SLOT(sidebarBlockToggle()) );
    connect( ui->widget_archive_sidebar_map_header      , SIGNAL(Mouse_Pressed())   , this  , SLOT(sidebarBlockToggle()) );
    connect( ui->widget_archive_sidebar_score_header    , SIGNAL(Mouse_Pressed())   , this  , SLOT(sidebarBlockToggle()) );
    connect( ui->widget_archive_sidebar_sites_header    , SIGNAL(Mouse_Pressed())   , this  , SLOT(sidebarBlockToggle()) );
    connect( ui->widget_archive_sidebar_keywords_header , SIGNAL(Mouse_Pressed())   , this  , SLOT(sidebarBlockToggle()) );
    connect( ui->widget_archive_sidebar_notes_header    , SIGNAL(Mouse_Pressed())   , this  , SLOT(sidebarBlockToggle()) );
    connect( ui->widget_archive_sidebar_tools_header    , SIGNAL(Mouse_Pressed())   , this  , SLOT(sidebarBlockToggle()) );


    // FILES
    connect( ui->treeView_archiveFiles              , SIGNAL(clicked(QModelIndex))  , this  , SLOT(fileNaviLoadFolder(QModelIndex)) );
    connect( ui->pushButton_fileListToRoot          , SIGNAL(clicked())             , this  , SLOT(fileNaviSetRoot()) );
    connect( ui->pushButton_toggleFolderNavi        , SIGNAL(clicked())             , this  , SLOT(fileNaviToggle()) );
    connect( ui->pushButton_toggleFolderNavi2       , SIGNAL(clicked())             , this  , SLOT(fileNaviToggle()) );
    connect( ui->pushButton_archiveOpenFolder       , SIGNAL(clicked())             , this  , SLOT(openFolderInExplorer()) );
    connect( ui->pushButton_archiveOpenFolder_1     , SIGNAL(clicked())             , this  , SLOT(openFolderInExplorer()) );
    connect( ui->pushButton_archiveCompressor       , SIGNAL(clicked())             , this  , SLOT(dialog_compressor()) );
    connect( ui->pushButton_archiveCompressor_1     , SIGNAL(clicked())             , this  , SLOT(dialog_compressor()) );
    connect( ui->pushButton_archiveFlattenFolder    , SIGNAL(clicked())             , this  , SLOT(dialog_flattenFolder())  );
    connect( ui->pushButton_archiveOrganizeFolder   , SIGNAL(clicked())             , this  , SLOT(dialog_organizer()) );
    connect( ui->pushButton_archiveOrganizeFolder_1 , SIGNAL(clicked())             , this  , SLOT(dialog_organizer()) );
    connect( ui->listView_archiveFiles              , SIGNAL(doubleClicked(QModelIndex))            , this  , SLOT(fileDoubleClick(QModelIndex)) );
    connect( ui->listView_archiveFiles              , SIGNAL(customContextMenuRequested(QPoint))    , this  , SLOT(fileContextMenu(QPoint)) );

    // MAIN MENU
    connect( ui->actionAdd_newArchive   , SIGNAL(triggered())   , this  , SLOT(dialog_addNewProxy())    );
    connect( ui->actionNotes            , SIGNAL(triggered())   , this  , SLOT(dialog_notesBrowser())   );
    connect( ui->actionTable_View       , SIGNAL(triggered())   , this  , SLOT(dialog_browserAll())     );
    connect( ui->actionFolder_Sizes     , SIGNAL(triggered())   , this  , SLOT(tab_folderSizes())       );
    connect( ui->actionSites            , SIGNAL(triggered())   , this  , SLOT(dialog_sitesBrowser())   );
    connect( ui->actionScore            , SIGNAL(triggered())   , this  , SLOT(dialog_scores())         );
    connect( ui->actionSite_updater     , SIGNAL(triggered())   , this  , SLOT(dialog_siteUpdater())    );
    connect( ui->actionMonthly_uploads  , SIGNAL(triggered())   , this  , SLOT(tab_monthlyUploads())    );
    connect( ui->actionKeywords         , SIGNAL(triggered())   , this  , SLOT(keywordCloud())          );
    connect( ui->actionSync_with_Sentinel,SIGNAL(triggered())   , this  , SLOT(dialog_syncSentinel())   );
    connect( ui->actionUpload_Times     , SIGNAL(triggered())   , this  , SLOT(dialog_uploadTimes())    );
    connect( ui->actionFollowers_Pageviews,SIGNAL(triggered())  , this  , SLOT(dialog_FPV())            );



}




/*  ┌─┬───┬─┐
 *  │█│   │ │   @LEFT_SIDEBAR
 *  │█│   │ │
 *  └─┴───┴─┘
 */

//-----@TREE----

void archiveManager::treeFocus()
{
    QModelIndex index = modelTreeIndexOf(selected->title);
    if (!index.isValid()) return;
    ui->treeView_archiveTree->setCurrentIndex( index );
    ui->treeView_archiveTree->scrollTo(index,QAbstractItemView::PositionAtCenter);
}

void archiveManager::treeSetIndex(QModelIndex index)
{
    ui->treeView_archiveTree->setCurrentIndex( index );
    select(index);
}

void archiveManager::treeExpandCollapse(bool state)
{
    (state) ? ui->treeView_archiveTree->expandAll() : ui->treeView_archiveTree->collapseAll();
    (state) ? ui->pushButton_archiveTreeExpand->setIcon(icons->arrow_down) : ui->pushButton_archiveTreeExpand->setIcon(icons->arrow_right);
}

void archiveManager::treeSwitchGrouped()
{
    ui->pushButton_archiveTreeGroup->setFlat(!treeGrouped);
    treeGrouped = !treeGrouped;
    refresh();
}

void archiveManager::groupSwitcher()
{
    ui->pushButton_archiveGroupByAz->setFlat(   groupType=="A-Z"    );
    ui->pushButton_archiveGroupByDate->setFlat( groupType=="DATE"   );
    ui->pushButton_archiveGroupByScore->setFlat(groupType=="SCORE"  );
    ui->pushButton_archiveGroupByLoc->setFlat(  groupType=="LOC"    );
}

void archiveManager::groupBySelect()
{
    QString sndr = sender()->objectName();;

    this->scoringMode = tools::queryFirst(libMan->db,"SELECT value FROM var WHERE key='scoring'");
    if (this->scoringMode=="") this->scoringMode="LNR";
    qDebug() << "                 " << this->scoringMode;

    QString queryName;
    if (scoringMode=="LNR") queryName = "unified_LNR.sql";
    if (scoringMode=="SQR") queryName = "unified_SQR.sql";
    if (scoringMode=="YWG") queryName = "unified_YWG.sql";
    qDebug() << scoringMode;
    qDebug() << queryName;

    // calculate scorebias
    qDebug() << "old scorebias=" << libMan->scoreBias;
    QString query=libMan->loadQuery(queryName);
    query.replace("[P2]","1");
    if (filterOneSite_On) query.replace("[FILTER]",onesiteFilter);
      else query.replace("[FILTER]"  , "");
    query.replace("[ORDERBY]" , "score.score * 5  DESC , title" );
    //qDebug() << query;
    modelSetQueryAndFetch(query);
    qDebug() << "TOP base SCORE=" << model->record(0).value("score").toString() << " (" << model->record(0).value("title").toString()<<") ";
    libMan->scoreBias = 5.0 / model->record(0).value("score").toFloat() ;
    model->clear();
    qDebug() << "new scorebias=" << libMan->scoreBias;
    // done

    query=libMan->loadQuery(queryName);
    query.replace("[P2]",QString::number(libMan->scoreBias));
    if (filterOneSite_On) query.replace("[FILTER]",onesiteFilter);
      else query.replace("[FILTER]"  , "");

    //qDebug()<<query;



    if (sndr == "pushButton_archiveGroupByAz")
        {
            groupType="A-Z";
            query.replace("[ORDERBY]" , "title" );
        }
    if (sndr == "pushButton_archiveGroupByDate")
        {
            groupType="DATE";
            query.replace("[ORDERBY]" , "date" );
        }
    if (sndr == "pushButton_archiveGroupByScore")
        {
            groupType="SCORE";
            query.replace("[ORDERBY]" , "score.score  * 5  DESC , title" );
        }
    if (sndr == "pushButton_archiveGroupByLoc")
        {
            groupType="LOC";
            query.replace("[ORDERBY]" , "location_text,title" );
        }

    qDebug()<<query;

    groupSwitcher();
    modelSetQueryAndFetch(query);
    refresh();
}

//-----@COLLECTIONS-----

QStringList archiveManager::getItemTypeCollections()
{
    QStringList result;
    for (int i=0;i < collectionModel->rowCount();i++)
        if (collectionModel->index(i,1).data().toString()=="item") result << collectionModel->index(i,3).data().toString();
    return result;
}

void archiveManager::collectionTargetCycle()
{
    QString newTarget;
    QStringList itemTypeCollections = getItemTypeCollections();
    int colCount = itemTypeCollections.count();
    QString curTarget = tools::modelSearch(collectionModel,4,3,"targetCollection");
    int curIndex = itemTypeCollections.indexOf(curTarget);
    (curIndex+1 == colCount) ? newTarget = itemTypeCollections.at(0) : newTarget = itemTypeCollections.at(curIndex+1);

    emit overlayMsg("Target collection set to: '" + tools::modelSearch(collectionModel,3,0,newTarget) + "'");

    libMan->db.exec("UPDATE var SET value='"+newTarget+"' WHERE key='targetCollection'");
    collectionModelQueryReset();
}

void archiveManager::collectionContextMenu(const QPoint &pos)
{
    QPoint globalPos    = ui->listView_collections->mapToGlobal(pos);
    QModelIndex index   = ui->listView_collections->indexAt(pos);

    QString id     = tools::dataFromIndex(index,3);
    QString type   = tools::dataFromIndex(index,1);
    QString text   = tools::dataFromIndex(index,2);
    QString name   = tools::dataFromIndex(index,0);
    QString target = tools::dataFromIndex(index,4);

    QMenu contextMenu("Context menu");

    if ( ( type=="item" ) & ( target=="" ) )
        contextMenu.addAction(new QAction("Set as target"   , this));

        contextMenu.addAction(new QAction("Remove"          , this));
        contextMenu.addAction(new QAction("Edit..."         , this));

    QAction* selectedItem = contextMenu.exec(globalPos);

    if (selectedItem)
        {
            if (selectedItem->text()=="Set as target")
                {
                    libMan->db.exec("INSERT OR REPLACE INTO var (id,key,value) "
                                    "VALUES ( (SELECT id FROM var WHERE key = 'targetCollection' ) , 'targetCollection' , '"+
                                    id+"' )" );
                    this->collectionModelQueryReset();
                }

            if (selectedItem->text()=="Remove")
                {
                    yesNoDialog  ynDialog;
                    ynDialog.setMessage( "Are you sure to delete this item?" );
                    ynDialog.setDetail("Collection: "+name);
                    if (ynDialog.exec())
                        {
                            QString query="DELETE FROM collections WHERE id="+id;
                            libMan->db.exec(query);
                            this->collectionModelQueryReset();
                        }
                }

            if (selectedItem->text()=="Edit...")
                {
                    addCollectionDialog *acDialog=new addCollectionDialog();
                    acDialog->setEditMode(name,type,text);
                    if (acDialog->exec())
                        {
                            QString query = "UPDATE collections SET name='"+acDialog->name+"' , "
                                            "type='"+acDialog->type+"' , "
                                            "param='"+acDialog->param+"' "
                                            "WHERE id="+id;
                            libMan->db.exec(query);
                            this->collectionModelQueryReset();
                        }
                }
        }
}

void archiveManager::collectionSelect(QModelIndex index)
{
    if (tools::dataFromIndex(index,1)=="smart") ui->lineEdit_archiveFilter->setText(tools::dataFromIndex(index,2));
        else                                    ui->lineEdit_archiveFilter->setText( "#col:" + tools::dataFromIndex(index,3));
}

void archiveManager::collectionModelQueryReset()
{
    collectionModel->setQuery(" SELECT c.name,c.type,c.param,c.id,var.key FROM collections "
                              " c LEFT JOIN var ON var.value = c.id AND var.key='targetCollection' "
                              " ORDER BY name  COLLATE NOCASE " , libMan->db    );
}

void archiveManager::collect()
{
    QString targetCollectionName;
    QString targetCollectionID;

    for (int i=0;i<collectionModel->rowCount();++i)
        {
            if (collectionModel->index(i,4).data().toString()=="targetCollection")
                {
                    targetCollectionName = collectionModel->index(i,0).data().toString();
                    targetCollectionID   = collectionModel->index(i,3).data().toString();
                }
        }

    QString curColls = tools::queryFirst(libMan->db,"SELECT collections FROM archive WHERE title='"+ selected->title+"'");

    if (curColls.contains("#"+targetCollectionID+"#"))
        {
            // REMOVE
            QString query = "UPDATE archive SET collections=replace(collections,'#"
                            +targetCollectionID +"#','') "
                            "WHERE id="+ QString::number( selected->id );
            libMan->db.exec(query);

            emit overlayMsg("'" + selected->title + "'" + " removed from collection: '" + targetCollectionName + "'");

            if (ui->lineEdit_archiveFilter->text()=="#col:"+targetCollectionID)
                {
                    QModelIndex cIndex = ui->listView_archiveBrowser->currentIndex();

                    // advance to next (or prev if there is no next =] )
                    QModelIndex newIndex;
                    newIndex = ui->listView_archiveBrowser->model()->index(cIndex.row(),0);
                    if (!newIndex.isValid())
                        {
                            newIndex = ui->listView_archiveBrowser->model()->index(cIndex.row()-1,0);
                        }
                        else
                        {
                            ui->listView_archiveBrowser->setCurrentIndex( newIndex  );
                            browserSelect(ui->listView_archiveBrowser->currentIndex());
                        }
                }
        }
        else
        {
            // ADD
            QString query = "UPDATE archive SET collections="
                            "(case when collections is null then '' else collections end) || '#"
                            + targetCollectionID
                            + "#' WHERE id=" + QString::number( selected->id );

            libMan->db.exec(query);
            emit overlayMsg( "'" +  selected->title + "'" + " added to collection: '" + targetCollectionName + "'" );
        }
}



/*  ┌─┬───┬─┐
 *  │ │   │ │   @FILTER
 *  │█│   │ │
 *  └─┴───┴─┘
 */


void archiveManager::filterMenu(QAction *action)
{
    QString key;
    if (action->text()=="Title")    key="T";
    if (action->text()=="Keywords") key="K";
    if (action->text()=="Notes")    key="N";
    if (action->text()=="Date")     key="D";
    if (action->text()=="Location") key="L";
    if (action->isChecked()) filterOpt+=key;
        else if ( filterOpt.size() > 1 ) filterOpt.replace(key,"");
                    else action->setChecked(true);
    filter("");
    static_cast<QMenu *>(this->sender())->show();
}

void archiveManager::filterClear()
{
    ui->lineEdit_archiveFilter->setText("");
    filter("");
}

void archiveManager::createFilterMenu()
{
    QMenu *filterMenu = new QMenu();
    filterMenu->addAction(tools::menuAddCheckable("Title"   ,true));
    filterMenu->addAction(tools::menuAddCheckable("Keywords",true));
    filterMenu->addAction(tools::menuAddCheckable("Notes"   ,true));
    filterMenu->addAction(tools::menuAddCheckable("Date"    ,true));
    filterMenu->addAction(tools::menuAddCheckable("Location",true));
    //*
    connect ( filterMenu   , SIGNAL(triggered(QAction*))   , this    , SLOT(filterMenu(QAction*)) );
    filterOpt = "TKNDL";

    ui->pushButton_filterFields->setMenu(filterMenu);

    fullMapTimer = new QTimer(); // ????

}


/*  ┌─┬───┬─┐
 *  │ │███│ │   PREVIEW IMAGE
 *  │ │   │ │
 *  └─┴───┴─┘
 */

void archiveManager::previewImageLoad()
{
    qDebug() << "  LOAD  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << libMan->libraryFolder + "previews/" + selected->image;
    selected->pixmap.load(libMan->libraryFolder + "previews/" + selected->image);
    selected->pixmapWidth = selected->pixmap.width();
    selected->pixmapHeight = selected->pixmap.height();
    selected->pixmapAspectRatio = (float) selected->pixmapWidth  / (float) selected->pixmapHeight ;
}

void archiveManager::sidebarImageLoad()
{
    ui->label_sideBarPreview->setPixmap(selected->pixmap.scaled(ui->label_sideBarPreview->width()-14,ui->label_sideBarPreview->height()-14,Qt::KeepAspectRatio , Qt::SmoothTransformation));
}

void archiveManager::previewImageResize()
{
    int containerWidth=ui->label_previewImage->width();
    int containerHeight=ui->label_previewImage->height();
    float containerAspectRatio = (float) containerWidth / (float) containerHeight;

    if (selected->pixmapAspectRatio>containerAspectRatio)
        {
            int newWidth = selected->pixmapWidth;
            if ( containerWidth<selected->pixmapWidth ) newWidth=containerWidth;
            ui->label_previewImage->setPixmap(selected->pixmap.scaled(newWidth,newWidth/selected->pixmapAspectRatio,Qt::KeepAspectRatio , Qt::SmoothTransformation));
        }
        else
        {
            int newHeight=selected->pixmapHeight;
            if (containerHeight<selected->pixmapHeight) newHeight=containerHeight;
            ui->label_previewImage->setPixmap(selected->pixmap.scaled(newHeight*selected->pixmapAspectRatio,newHeight,Qt::KeepAspectRatio , Qt::SmoothTransformation));
        }
}


/*  ┌─┬───┬─┐
 *  │ │   │ │   @FILES
 *  │ │███│ │
 *  └─┴───┴─┘
 */




void archiveManager::splitter1Moved(int a, int b)
{
    Q_UNUSED(a);
    Q_UNUSED(b);
    previewImageResize();
    fileNaviThumbResize();
}

void archiveManager::fileNaviThumbResize()
{
    if ( ui->listView_archiveFiles->height() > 60 )
    {
        fDelegate->thumbWidth = ui->listView_archiveFiles->height()-53;
        ui->listView_archiveFiles->adjustSize();
        if ( !ui->listView_archiveFiles->horizontalScrollBar()->isVisible() )
            {
                //fDelegate->thumbWidth = ui->listView_archiveFiles->height()-20;
                //ui->listView_archiveFiles->adjustSize();
            }
    }
}

void archiveManager::openFolderInExplorer()
{
    tools::openFolderInExplorer(selected->folder);
}

void archiveManager::fileDoubleClick(QModelIndex index)
{
    tools::openLocalFile(tools::dataFromIndex(index,1));
}

void archiveManager::fileContextMenu(const QPoint &pos)
{
    QPoint globalPos    = ui->listView_archiveFiles->mapToGlobal(pos);
    QModelIndex index   = ui->listView_archiveFiles->indexAt(pos);
    QString fileName    = tools::dataFromIndex(index,0);
    QString filePath    = tools::dataFromIndex(index,1);

    QMenu contextMenu("Context menu");

    QAction *menuTitle = new QAction(  fileName , 0  );
        menuTitle->setDisabled(true);
        QFont font ;
        font.setBold(true);
        menuTitle->setFont(font);
        contextMenu.addAction(menuTitle);

    /*
    contextMenu.addAction(new QAction("File Info"   , this));
    contextMenu.addAction(new QAction("Preview"     , this));
    contextMenu.addAction(new QAction("Edit"        , this));
    */

    QAction *fileInfo = new QAction("File Info",this);
    fileInfo->setIcon(QIcon(":/icons/icons/Info.png"));
    QAction *preview = new QAction("Preview",this);
    preview->setIcon(QIcon(":/icons/icons/eye.png"));
    QAction *edit = new QAction("Edit",this);
    edit->setIcon(QIcon(":/icons/icons/appIcon-PhotoShop.png"));
    QAction *deleteFile = new QAction("Delete",this);
    deleteFile->setIcon(QIcon(":/icons/icons/close-icon-red-circle.png"));


    contextMenu.addAction(fileInfo);
    contextMenu.addAction(preview);
    contextMenu.addAction(edit);
    contextMenu.addAction(deleteFile);


    QAction* selectedItem = contextMenu.exec(globalPos);
    if (selectedItem)
        {
            if (selectedItem->text() == "Preview")
                {
                    tools::openLocalFile(filePath);
                }

            if (selectedItem->text() == "Edit")
                {
                    QStringList arg;
                    arg << "" + filePath.replace("//","/").replace("/","\\") + "";  // ????
                    emit openWithPS(arg);
                }
            if (selectedItem->text() == "Delete")
                {
                    yesNoDialog  ynDialog;
                    ynDialog.setMessage( "Are you sure to delete this item?" );
                    ynDialog.setDetail(filePath);
                    if (ynDialog.exec())
                    {
                        QFile::remove(filePath);
                        this->fileNaviUpdate(selected->folder);
                    }
                }
        }
}

void archiveManager::fileNaviSetRoot()
{
    ui->treeView_archiveFiles->setCurrentIndex(QModelIndex());
    fileNaviLoadFolder(QModelIndex());
}

void archiveManager::fileNaviToggle()
{
    if (ui->widget_archiveFolderNavi->isVisible())
        {
            ui->widget_archiveFolderNavi->setVisible(false);
            ui->pushButton_toggleFolderNavi->setIcon(QIcon(":/icons/icons/stylesheet-branch-up.png"));
            ui->widget_archiveFolderNavi->setProperty("status","hidden");

        }
        else
        {
            ui->widget_archiveFolderNavi->setVisible(true);
            ui->pushButton_toggleFolderNavi->setIcon(QIcon(":/icons/icons/stylesheet-branch-open.png"));
            ui->widget_archiveFolderNavi->setProperty("status","visible");
        }

    QTimer::singleShot(10,this,SLOT( previewImageResize() ));

}

void archiveManager::fileNaviLoadFolder(QModelIndex index) // archiveLoadImageFromTree vót
{

    fModel->clear();
    fDelegate->cacheFolder = libMan->getCacheFolder();

    QString rootPath = tools::fileIndexToPath(index);
    if (rootPath=="") rootPath = selected->folder  + "/";

    QDir *dir = new QDir();
    dir->setPath(rootPath);
    dir->setFilter(QDir::NoDotAndDotDot | QDir::Files);

    QStringList filesTobeCached;

    for (int i = 0;i<dir->entryList().count();++i)
        {
            QString f = dir->entryList().at(i);
            if ( f.endsWith("tif") || f.endsWith("psd") || f.endsWith("jpg") )
                if ( !QFile::exists(libMan->getCacheFolder() + f + ".jpg") )
                    filesTobeCached << rootPath+"/"+f;

            QList<QStandardItem *> row;

            row << new QStandardItem(f);
            row << new QStandardItem(rootPath+"/"+f);
            fModel->appendRow(row);
        }

    if (filesTobeCached.count()>0)
        {
            threadedImageCacher *tCacher = new threadedImageCacher(this);
            tCacher->cacheFolder = libMan->getCacheFolder();
            tCacher->filesTobeCached = filesTobeCached;
            connect ( tCacher   , SIGNAL(itemLoaded())  , ui->listView_archiveFiles , SLOT(repaint()) );
            tCacher->start();
        }
}


void archiveManager::fileNaviUpdate(QString folder)
{

    if (folder=="")
        {
            ui->widget_archiveFolderNavi->setCurrentIndex(1);
            ui->label_dirWarning->setText("WARNING: Folder not set!");
            return;
        }

    if (!QDir(folder).exists())
        {
            ui->widget_archiveFolderNavi->setCurrentIndex(1);
            ui->label_dirWarning->setText("WARNING: Folder '"+folder+"' not found!");
            return;
        }

    if (ui->widget_archiveFolderNavi->currentIndex()==1)  //; removed !!!
        {
            ui->widget_archiveFolderNavi->setCurrentIndex(0);
            fileNaviThumbResize();
        }

    fileModel->setRootPath(folder);

    fileModel->setFilter(QDir::NoDotAndDotDot | QDir::Dirs);
    ui->treeView_archiveFiles->setRootIndex(fileModel->index(folder));
    ui->treeView_archiveFiles->expandAll();
    ui->treeView_archiveFiles->setCurrentIndex(QModelIndex());
    ui->treeView_archiveFiles->expandAll();

    ui->treeView_archiveFiles->setVisible(tools::subDirCount(folder)!=0);
    fileNaviLoadFolder(QModelIndex());
    ui->pushButton_fileListToRoot->setText(folder);
}


/*  ┌─┬───┬─┐
 *  │ │^^^│ │   @TAB BAR & TABS
 *  │ │   │ │
 *  └─┴───┴─┘
 */

void archiveManager::tabChanged(int tab)
{
    if (tab==0) previewImageResize();
    if (tab==1) browserSetThumbSize(ui->horizontalSlider_archiveThumbSize->value());
    if (tab==3) fullMapRefresh();  // ????
}

void archiveManager::tabClose(int i)
{
    ui->tabWidget_archiveCenter->removeTab(i);
}

void archiveManager::tab_monthlyUploads()
{
    int tabFound = -1;
    int tabNum;
    for (int i = 0;i<ui->tabWidget_archiveCenter->count();++i)
        {
           if (ui->tabWidget_archiveCenter->tabText(i) == "Monthly uploads") tabFound=i;
        }

    if (tabFound != -1)
        {
            tabNum = tabFound;
        }
        else
        {
            tabNum = ui->tabWidget_archiveCenter->addTab(new QWidget(),"Monthly uploads");
        }

    ui->tabWidget_archiveCenter->setCurrentIndex(tabNum);
    ui->tabWidget_archiveCenter->tabBar()->tabButton(tabNum, QTabBar::RightSide)->resize(12,12);

    QMap<QString,int> values;
    QSqlQueryModel *statModel = new QSqlQueryModel(this);
    statModel->setQuery("SELECT substr(date,3,5) , COUNT(url) FROM sites  WHERE date != '' AND site!='RD' AND date > '2013-12-31' GROUP BY substr(date,3,5) ORDER BY substr(date,0,8) ",libMan->db);
/*
    for (int y=2014;y<2016;++y)
        {
            for (int m=1;m<13;++m)
                {
                    qDebug() << QString::number(y)+"-"+QString::number(m).rightJustified(2,'0');
                    values.insert(QString::number(y)+"-"+QString::number(m).rightJustified(2,'0'),0);
                }
        }
*/
    for (int i=0;i<statModel->rowCount();++i )
        {
            qDebug() << i << statModel->index(i,0).data().toString()
                     << statModel->index(i,1).data().toInt() ;
            values.insert(statModel->index(i,0).data().toString(),statModel->index(i,1).data().toInt());
        }

    BarGraphWidget *graph = new BarGraphWidget();
    QVBoxLayout *layout = new QVBoxLayout();

    graph->values = values;
    graph->setMaximumHeight(300);
    graph->setMinimumHeight(300);
    layout->addWidget(graph);
    layout->addStretch(1);

    ui->tabWidget_archiveCenter->widget(tabNum)->setLayout(layout);
}

void archiveManager::tab_folderSizes()
{
    int tabFound = -1;
    int tabNum;
    bool refresh=false;
    for (int i = 0;i<ui->tabWidget_archiveCenter->count();++i)
        {
            if (ui->tabWidget_archiveCenter->tabText(i) == "Folder sizes") tabFound=i;
        }

    if (tabFound != -1)
        {
            tabNum = tabFound;
            refresh = true;
        }
        else
        {
            tabNum = ui->tabWidget_archiveCenter->addTab(new QWidget(),"Folder sizes");
        }

    ui->tabWidget_archiveCenter->setCurrentIndex(tabNum);
    ui->tabWidget_archiveCenter->tabBar()->tabButton(tabNum, QTabBar::RightSide)->resize(12,12);


    QVBoxLayout *folderSizeLayout = new QVBoxLayout();

        QHBoxLayout *layout1 = new QHBoxLayout();
            QLabel *label_folderSizeInfo;
            if (!refresh) label_folderSizeInfo = new QLabel();
                else label_folderSizeInfo = static_cast<QLabel *>( ui->tabWidget_archiveCenter->currentWidget()->children().at(1) );
            QPushButton *pushButton_refresh = new QPushButton();
            pushButton_refresh->setText("Refresh");
            layout1->addWidget(label_folderSizeInfo);
            layout1->addStretch(1);
            layout1->addWidget(pushButton_refresh);

        QHBoxLayout *layout2 = new QHBoxLayout();
            QLabel *label_filt = new QLabel("Filters: ");

            QCheckBox *checkBox_0sub;
            if (!refresh) checkBox_0sub = new QCheckBox("0 Subfolders");
                else checkBox_0sub = static_cast<QCheckBox *>( ui->tabWidget_archiveCenter->currentWidget()->children().at(4) );
            QCheckBox *checkBox_large;
            if (!refresh) checkBox_large  = new QCheckBox("large");
                else checkBox_large = static_cast<QCheckBox *>( ui->tabWidget_archiveCenter->currentWidget()->children().at(5) );

            if (!refresh) layout2->addWidget(label_filt);
            if (!refresh) layout2->addWidget(checkBox_0sub);
            if (!refresh) layout2->addWidget(checkBox_large);

            layout2->addStretch(1);

        QProgressBar *progressBar;
            if (!refresh) progressBar = new QProgressBar();
                else progressBar = static_cast<QProgressBar *>( ui->tabWidget_archiveCenter->currentWidget()->children().at(6) );
            progressBar->setFixedHeight(15);

        QTableView *tableView_folderSize;
            if (!refresh) tableView_folderSize = new QTableView();
                 else tableView_folderSize = static_cast<QTableView *>( ui->tabWidget_archiveCenter->currentWidget()->children().at(7) );
            tableView_folderSize->setEditTriggers(QAbstractItemView::NoEditTriggers);
            tableView_folderSize->setSelectionBehavior( QAbstractItemView::SelectRows );

    if (!refresh)
        {
            folderSizeLayout->addLayout(layout1);
            folderSizeLayout->addLayout(layout2);
            folderSizeLayout->addWidget(progressBar);
            folderSizeLayout->addWidget(tableView_folderSize);
            ui->tabWidget_archiveCenter->widget(tabNum)->setLayout(folderSizeLayout);
        }

    tableView_folderSize->horizontalHeader()->setStretchLastSection(true);
    tableView_folderSize->setSortingEnabled(true);

    progressBar->setVisible(true);

    QStandardItemModel *customModel = new QStandardItemModel();
    customModel->clear();
    customModel->setColumnCount(6);

    tableView_folderSize->setModel(customModel);
    tableView_folderSize->setColumnWidth(0,200);
    tableView_folderSize->setColumnWidth(1,400);
    tableView_folderSize->setColumnWidth(2,60);
    tableView_folderSize->setColumnWidth(3,60);
    tableView_folderSize->setColumnWidth(4,60);


    QStringList labels;
    labels << "title" << "projectDir" << "id" << "subfolders" << "files" << "size";
    customModel->setHorizontalHeaderLabels(labels);
    qint64 totalSize=0;
    int i;
    int num=0;
    for (i=0; i< model->rowCount();++i)
        {
            QSqlRecord record =  model->record(i);
            QList<QStandardItem*> items;
            items   << new QStandardItem( record.field(0).value().toString() );
            items   << new QStandardItem( record.field(2).value().toString() );
            QStandardItem *id = new QStandardItem( record.field(1).value().toString().rightJustified(4,' ') );
            id->setData(Qt::AlignRight, Qt::TextAlignmentRole);
            items   <<  id;

            QString folder = record.field(2).value().toString();

            int fileCount=0;
            int folderCount=0;
            qint64 fileSize=0;

            QDirIterator iterator(QDir(folder).absolutePath(), QDirIterator::Subdirectories);
            while (iterator.hasNext())
                {
                    iterator.next();
                    if (!iterator.fileInfo().isDir())
                        {
                            QString filename = iterator.fileName();
                            ++fileCount;
                            fileSize+=iterator.fileInfo().size();
                        }
                    else if( (iterator.fileName()!=".") & (iterator.fileName()!="..") ) ++folderCount;
                }

            QStandardItem* itemCount1 = new QStandardItem( QString::number(folderCount).rightJustified(4,' ') );
            itemCount1->setData(Qt::AlignRight, Qt::TextAlignmentRole);
            items   << itemCount1;

            QStandardItem* itemCount = new QStandardItem( QString::number(fileCount).rightJustified(4,' ') );
            itemCount->setData(Qt::AlignRight, Qt::TextAlignmentRole);
            items   << itemCount;

            QStandardItem* itemSize = new QStandardItem( tools::numGrouped(fileSize).rightJustified(20,' ') + " bytes" );
            itemSize->setData(Qt::AlignRight, Qt::TextAlignmentRole);
            items   << itemSize;

            if (checkBox_0sub->checkState() == Qt::Checked)
                {
                    if ( folderCount == 0)
                        {
                            if (checkBox_large->checkState()==Qt::Checked)
                                {
                                    if (fileSize > 512000000)
                                        {
                                            customModel->appendRow(items);
                                            totalSize+=fileSize;
                                            ++num;
                                        }
                                }
                                else
                                {
                                    customModel->appendRow(items);
                                    totalSize+=fileSize;
                                    ++num;
                                }
                        }
                }
                else
                {
                    if (checkBox_large->checkState()==Qt::Checked)
                        {
                            if (fileSize > 512000000)
                                {
                                    customModel->appendRow(items);
                                    totalSize+=fileSize;
                                    ++num;
                                }
                        }
                        else
                        {
                            customModel->appendRow(items);
                            totalSize+=fileSize;
                            ++num;
                        }
                }


            progressBar->setValue(100*i/model->rowCount());
            label_folderSizeInfo->setText("Projects: <b>" + QString::number(num)
                  + "</b>    &nbsp;&nbsp;&nbsp;     Total bytes: <b>"
                  + tools::numGrouped(totalSize) + "</b>  &nbsp;&nbsp;&nbsp;   Average bytes: <b>"
                  + tools::numGrouped( totalSize / (num+0.000000000001) ) +"</b>  &nbsp;&nbsp;&nbsp;    reading: <b>"
                  + QString::number(i+1) + " / " + QString::number(model->rowCount()) +"</b>" );
            if(i % 10 == 0) QCoreApplication::processEvents();
        }

    label_folderSizeInfo->setText("Projects: <b>" + QString::number(num) + "</b>    &nbsp;&nbsp;&nbsp;     Total bytes: <b>" +  tools::numGrouped(totalSize) + "</b>  &nbsp;&nbsp;&nbsp;   Average bytes: <b>" + tools::numGrouped( totalSize / (num+(1E-32)) ) +"</b>" );
    progressBar->setVisible(false);

    if (tableView_folderSize->property("firstrun").toBool())
        {
            int sortSect = tableView_folderSize->horizontalHeader()->sortIndicatorSection();
            int sortOrd  =  tableView_folderSize->horizontalHeader()->sortIndicatorOrder();
            if (sortOrd==1) tableView_folderSize->sortByColumn(sortSect,Qt::DescendingOrder);
                else tableView_folderSize->sortByColumn(sortSect,Qt::AscendingOrder);
        }
        else  tableView_folderSize->sortByColumn(5,Qt::DescendingOrder);
    tableView_folderSize->setProperty("firstrun" , true);

    connect ( pushButton_refresh    , SIGNAL(clicked())                 , this  , SLOT(tab_folderSizes()) );
    connect ( tableView_folderSize  , SIGNAL(doubleClicked(QModelIndex)), this  , SLOT(tableDblClick(QModelIndex)) );
}


/*  ┌─┬───┬─┐
 *  │ │███│ │   MAP (FULL)
 *  │ │███│ │
 *  └─┴───┴─┘
 */

void archiveManager::fullMapRefresh()
{
    fullMapTimer->stop();
    ui->webView_fullMap->page()->mainFrame()->evaluateJavaScript("clearAllMarkers();");

    for(int i=0; i<model->rowCount();++i)
        {
            QString loc_lat = model->index(i,8).data().toString();
            if (loc_lat!="")
                {
                    QString title   = model->index(i, 0).data().toString();
                    QString image   = model->index(i, 5).data().toString();
                    QString loc_lng = model->index(i, 9).data().toString();
                    QString loc_txt = model->index(i,10).data().toString();
                    QString jsCmd = "addMarker(" +loc_lat + ","+loc_lng+",'"+title+"','file:///F:/!BOX/Box%20Sync/Parallax/imported/cache/"+image+"');";
                    ui->webView_fullMap->page()->mainFrame()->evaluateJavaScript(jsCmd);
                }
        }
    ui->webView_fullMap->page()->mainFrame()->evaluateJavaScript("zoomBounds(); initClusters(); calculateClusters(); drawClustersAndMarkers(); ");
}


/*  ┌─┬───┬─┐
 *  │ │███│ │   WEB
 *  │ │███│ │
 *  └─┴───┴─┘
 */

void archiveManager::webBack()
{
    if ( ui->webView->history()->currentItemIndex() > 1 ) ui->webView->history()->back();
}

void archiveManager::webForward()
{
    ui->webView->history()->forward();
}

void archiveManager::webUrlUpdate(QUrl url)
{
    ui->lineEdit_archiveUrl->setText(url.toDisplayString());
}

void archiveManager::webProgress(int p)
{
    ui->progressBar_web->setValue(p);
}


/*  ┌─┬───┬─┐
 *  │ │███│ │   @BROWSER
 *  │ │███│ │
 *  └─┴───┴─┘
 */

void archiveManager::browserFocus()
{
    QModelIndex index;
    bool found = false;
    int i;
    for (i=0;i<modelThumb->rowCount();++i)
        {
            if ( modelThumb->index(i,0).data().toString()==selected->title) found = true;
            if (found) break;
        }
    if (found)
        {
            index = modelThumb->index(i,0);
            ui->listView_archiveBrowser->setCurrentIndex( index );
            ui->listView_archiveBrowser->scrollTo(modelThumb->index(i,0),QAbstractItemView::PositionAtCenter);
        }
}

void archiveManager::browserContextMenu(const QPoint &pos)
{
    QPoint globalPos = ui->listView_archiveBrowser->mapToGlobal(pos);

    QList<QAction*> collections;
    QModelIndex index = ui->listView_archiveBrowser->indexAt(pos);
    QSqlQuery qry =  libMan->db.exec("SELECT collections FROM archive WHERE id="+
                                             index.sibling(index.row(),1).data().toString());
    qry.first();
    QString curColls = qry.value(0).toString();

    QMenu contextMenu("Context menu");
    QMenu *addCollectionMenu = contextMenu.addMenu(tr("Set collection..."));
    /*
    QMenu *uploadMenu        = contextMenu.addMenu(tr("Upload to..."));
            uploadMenu->addAction(new QAction(tr("Upload to 500px"), this));
            uploadMenu->addAction(new QAction(tr("Upload to deviantArt"), this));
    */
    QString targetCollection;

    for (int i=0; i < collectionModel->rowCount();++i)
        {
            if (collectionModel->index(i,1).data().toString()=="item")
                {
                    QAction *action = new QAction( collectionModel->index(i,0).data().toString() , this  );
                    action->setCheckable(true);
                    if ( curColls.contains("#"+collectionModel->index(i,3).data().toString()+"#") ) action->setChecked(true);
                    action->setProperty( "id" , collectionModel->index(i,3).data().toString() );

                    collections << action;

                    if ( collectionModel->index(i,4).data().toString()=="targetCollection") targetCollection=collectionModel->index(i,3).data().toString();

                    addCollectionMenu->addAction(action);
                }
        }

    if ( curColls.contains("#"+targetCollection+"#") )
             contextMenu.addAction(new QAction(tr("Remove from target collection"), this));
        else contextMenu.addAction(new QAction(tr("Add to target collection"), this));

    contextMenu.addSeparator();
    contextMenu.addAction(new QAction(tr("Edit..."), this));
    contextMenu.addSeparator();
    contextMenu.addAction(new QAction(tr("Add site"), this));
    contextMenu.addAction(new QAction(tr("Update sites"), this));
    contextMenu.addAction(new QAction(tr("Open folder"), this));
    contextMenu.addAction(new QAction(tr("Extended stats"), this));
    contextMenu.addAction(new QAction(tr("Upload"), this));

    QAction* selectedItem = contextMenu.exec(globalPos);

    if (selectedItem)
        {

            foreach( QAction *action, collections )
                {
                    if (action->text()==selectedItem->text())
                        {
                            QString query;
                            if (action->isChecked()==true)
                                {
                                    query = "UPDATE archive SET collections=(case when collections is null then '' else collections end) || '#"+
                                            action->property("id").toString() +
                                            "#' WHERE id="+index.sibling(index.row(),1).data().toString();
                                }
                                else
                                {
                                    query = "UPDATE archive SET collections=replace(collections,'#"
                                            +action->property("id").toString() +"#','') "
                                            "WHERE id="+index.sibling(index.row(),1).data().toString();
                                }
                            libMan->db.exec(query);
                        }
                }

            if (selectedItem->text()=="Extended stats")
                {
                    extStatsDialog esD ;
                    esD.attachLibman(libMan);
                    //qDebug() << index.sibling(index.row(),1).data().toInt();
                    esD.selectedId = index.sibling(index.row(),1).data().toInt();
                    esD.title      = index.sibling(index.row(),0).data().toString();
                    esD.score      =  index.sibling(index.row(),7).data().toString(); //QString::number( libMan->scoreBias ) ; //
                    esD.date       = index.sibling(index.row(),4).data().toString();
                    esD.scoreBias  = libMan->scoreBias;
                    esD.exec();
                }

            if (selectedItem->text()=="Upload")
                {
                    browserSelect(index);
                    dialog_uploader();
                    //this->uploader("500px");
                }

            if (selectedItem->text()=="Add to target collection")
                {
                    QString query = "UPDATE archive SET collections=(case when collections is null then '' else collections end) || '#"+
                                    targetCollection +
                                    "#' WHERE id="+index.sibling(index.row(),1).data().toString();
                    libMan->db.exec(query);
                }

            if (selectedItem->text()=="Remove from target collection")
                {
                    QString query = "UPDATE archive SET collections=replace(collections,'#"
                                    +targetCollection +"#','') "
                                    "WHERE id="+index.sibling(index.row(),1).data().toString();
                    libMan->db.exec(query);
                }
            if (selectedItem->text()=="Open folder")
                {
                    openFolderInExplorer();
                }

            if (selectedItem->text()=="Update sites")
                {
                    siteUpdaterNew sU;
                    sU.setSingleMode(index.sibling(index.row(),1).data().toInt(),"");
                    sU.attachLibman(libMan);
                    sU.exec();
                    select(index.sibling(index.row(),1));
                    modelSetQueryAndFetch(model->query().lastQuery() );
                    refresh();
                    scoreGet();
                    sitesGet();
                }

            if (selectedItem->text()=="Edit...")
                {
                    addArchiveDialog aeDialog;

                    QList<QString> titleList = getTitles();

                    aeDialog.setTitleList(titleList);
                    aeDialog.setTitle(  index.sibling(index.row(),0).data().toString() );
                    aeDialog.setFolder( index.sibling(index.row(),2).data().toString() );
                    aeDialog.setImage(  index.sibling(index.row(),5).data().toString() );
                    aeDialog.setDate(   index.sibling(index.row(),4).data().toString() );

                    if (aeDialog.exec())
                    {
                        QString query = "UPDATE archive SET title = '"+aeDialog.title
                                        +"'  ,projectDir = '"+aeDialog.folder
                                        +"'       ,date  = '"+aeDialog.date
                                        +"'    ,keywords = '"+aeDialog.keywords+"' WHERE id = "
                                        + QString::number( selected->id );

                        libMan->db.exec(query);
                    }
                }
        }
}



void archiveManager::advancedFilterNotin_toggle(bool en)
{
    qDebug() << en << ui->checkBox_aF_notin->isChecked() << ui->radioButton_FNUP->isChecked() ;
    ui->widget_af_ni->setVisible( ui->checkBox_aF_notin->isChecked() );
    additionalFilter_On = ui->checkBox_aF_notin->isChecked() && ui->radioButton_FNUP->isChecked() ;
    filterOneSite_On    = ui->checkBox_aF_notin->isChecked() && ui->radioButton_FUP->isChecked() ;
    qDebug() << "filteronesite is" << filterOneSite_On;
    advancedFilterNotin();
}

void archiveManager::advancedFilterNotin()
{
    qDebug() << "archiveAdvFilterNotin()" ;
    qDebug() << sender()->objectName();

    ui->label_aF_niDA->setPixmap(QPixmap(":/icons/icons/siteIconDeviantArt_GREY.png"));
    ui->label_aF_niGP->setPixmap(QPixmap(":/icons/icons/siteIconGoogle+_GREY.png"));
    ui->label_aF_niFB->setPixmap(QPixmap(":/icons/icons/siteIconFaceBook_GREY.png"));
    ui->label_aF_niFL->setPixmap(QPixmap(":/icons/icons/siteIconFlickr_GREY.png"));
    ui->label_aF_ni5P->setPixmap(QPixmap(":/icons/icons/siteIcon500px_GREY.png"));
    ui->label_aF_niPX->setPixmap(QPixmap(":/icons/icons/siteIconPixoto1_GREY.png"));
    ui->label_aF_niYP->setPixmap(QPixmap(":/icons/icons/siteIconYoupic_GREY.png"));
    ui->label_aF_niVB->setPixmap(QPixmap(":/icons/icons/siteIconViewbug_GREY.png"));
    ui->label_aF_niRD->setPixmap(QPixmap(":/icons/icons/siteIconRd_GREY.png"));
    ui->label_aF_niIG->setPixmap(QPixmap(":/icons/icons/siteIconInstagram_GREY.png"));

    QString site;

    if ( sender()->objectName() == "label_aF_niDA" || sender()->objectName() == "radioButton_FNUP" || sender()->objectName() == "radioButton_FUP" || sender()->objectName() == "checkBox_aF_notin" )
        {
            ui->label_aF_niDA->setPixmap(QPixmap(":/icons/icons/siteIconDeviantArt.png"));
            site = "DA";
            onesiteFilter = " AND sites.site ='DA' ";
        }



    if ( sender()->objectName() == "label_aF_niGP" )
        {
            ui->label_aF_niGP->setPixmap(QPixmap(":/icons/icons/siteIconGoogle+.png"));
            site = "G+";
            onesiteFilter = " AND sites.site ='G+' ";
        }



    if ( sender()->objectName() == "label_aF_niFB" )
        {
            ui->label_aF_niFB->setPixmap(QPixmap(":/icons/icons/siteIconFaceBook.png"));
            site = "FB";
            onesiteFilter = " AND sites.site ='FB' ";
        }



    if ( sender()->objectName() == "label_aF_niFL" )
        {
            ui->label_aF_niFL->setPixmap(QPixmap(":/icons/icons/siteIconFlickr.png"));
            site = "FL";
            onesiteFilter = " AND sites.site ='FL' ";
        }



    if ( sender()->objectName() == "label_aF_ni5P" )
        {
            ui->label_aF_ni5P->setPixmap(QPixmap(":/icons/icons/siteIcon500px.png"));
            site = "5P";
            onesiteFilter = " AND sites.site ='5P' ";
        }



    if ( sender()->objectName() == "label_aF_niPX" )
        {
            ui->label_aF_niPX->setPixmap(QPixmap(":/icons/icons/siteIconPixoto1.png"));
            site = "PX";
            onesiteFilter = " AND sites.site ='PX' ";
        }



    if ( sender()->objectName() == "label_aF_niYP" )
        {
            ui->label_aF_niYP->setPixmap(QPixmap(":/icons/icons/siteIconYoupic.png"));
            site = "YP";
            onesiteFilter = " AND sites.site ='YP' ";
        }



    if ( sender()->objectName() == "label_aF_niVB" )
        {
            ui->label_aF_niVB->setPixmap(QPixmap(":/icons/icons/siteIconViewbug.png"));
            site = "VB";
            onesiteFilter = " AND sites.site ='VB' ";
        }



    if ( sender()->objectName() == "label_aF_niRD" )
        {
            ui->label_aF_niRD->setPixmap(QPixmap(":/icons/icons/siteIconRd.png"));
            site = "RD";
            onesiteFilter = " AND sites.site ='RD' ";
        }



    if ( sender()->objectName() == "label_aF_niIG" )
        {
            ui->label_aF_niIG->setPixmap(QPixmap(":/icons/icons/siteIconInstagram.png"));
            site = "IG";
            onesiteFilter = " AND sites.site ='IG' ";
        }



    QSqlQueryModel *tempModel = new QSqlQueryModel(this);
    QString query = "SELECT * FROM ( SELECT archive.title,archive.id,sites.site FROM archive LEFT  JOIN "
                    " (SELECT * FROM sites WHERE sites.site='"+site+"') sites ON sites.projectid = archive.id "
                    " ORDER BY archive.id ) t1  WHERE t1.site IS NULL";
    tempModel->setQuery(query,libMan->db);
    while (tempModel->canFetchMore()) tempModel->fetchMore();
    additionalFilter.clear();
    for (int i = 0; i<tempModel->rowCount();++i)
        {
            additionalFilter << tempModel->index(i,0).data().toString();
        }
    treeModelConvert();
    //if (ui->pushButton_archiveTreeExpand->isChecked()) ui->treeView_archiveTree->expandAll();
    if (filterOneSite_On) filterDo(ui->lineEdit_archiveFilter->text());
}


void archiveManager::browserSetThumbSize(int s)
{
    ui->label_archiveThumbSize->setText(QString::number(s) );
    QTimer::singleShot(3000,this,SLOT(browserZoomlevelHideText()));
    abDelegate->thumbSize = 180+s*4*10;
    abDelegate->parentWidth = ui->listView_archiveBrowser->width();
    ui->listView_archiveBrowser->adjustSize();
}

void archiveManager::browserZoomlevelHideText()
{
    ui->label_archiveThumbSize->setText("");
}

void archiveManager::browserSelectByTitle()
{
    bool found = false;
    int i;
    for (i=0;i<modelThumb->rowCount();++i)
        {
            if (modelThumb->index(i,0).data().toString()==selected->title) found = true;
            if (found) break;
        }

    if (found) ui->listView_archiveBrowser->setCurrentIndex(modelThumb->index(i,0));
        else
            {
                ui->listView_archiveBrowser->selectionModel()->reset();
                ui->listView_archiveBrowser->repaint();
            }
}

void archiveManager::browserSelect(QModelIndex index)
{
    QString title=modelThumb->index(index.row(),0).data().toString();
    qDebug() << "TITLE" << title;
    QList<QStandardItem*> fList = modelTree->findItems(title,Qt::MatchRecursive);

    if (fList.count()!=0)
        {
            QModelIndex index= fList.first()->index();
            ui->treeView_archiveTree->setCurrentIndex( index );
            select(index);
        }
}

void archiveManager::browserDoubleClick(QModelIndex index)
{
    browserSelect(index);
    ui->tabWidget_archiveCenter->setCurrentIndex(0);
}

void archiveManager::browserHighlightClear(QModelIndex index)
{
    if ( browserLastBranch != index.parent().data().toString()  ) abDelegate->filterRole="";
    ui->listView_archiveBrowser->repaint();
}

void archiveManager::browserHighlightBranch(QModelIndex index)
{

            browserLastBranch = tools::dataFromIndex(index,0);
            QString branchText = tools::dataFromIndex(index,0);

            if (groupType=="A-Z")      abDelegate->filterRole="title:"+branchText.mid(0,1);
            if (groupType=="DATE")     abDelegate->filterRole= "date:"+branchText.mid(0,4);
            if (groupType=="LOC")      abDelegate->filterRole=  "loc:"+branchText.split(" (").at(0);
            if (groupType=="SCORE")    abDelegate->filterRole="score:"+branchText;

            ui->listView_archiveBrowser->repaint();

            bool found = false;
            int i;
            for (i=0;i < modelThumb->rowCount();++i)
                {
                    if ( modelThumb->index(i,0).data().toString()==index.child(0,0).data().toString()) found = true;
                    if (found) break;
                }

            if (found) ui->listView_archiveBrowser->scrollTo(modelThumb->index(i,0),QAbstractItemView::PositionAtTop);




}

void archiveManager::setSelected(QModelIndex index)
{
    selected->title    = tools::dataFromIndex(index,0);
    selected->id       = tools::dataFromIndex(index,1).toInt();
    selected->folder   = tools::dataFromIndex(index,2);
    selected->date     = tools::dataFromIndex(index,4);
    selected->image    = tools::dataFromIndex(index,5);
}

void archiveManager::debugSelected()
{
    qDebug() << "          archiveSelectedTitle="   << selected->title;
    qDebug() << "          archiveSelectedId="      << selected->id;
    qDebug() << "          archiveSelectedDate="    << selected->date;
    qDebug() << "          archiveSelectedFolder="  << selected->folder;
    qDebug() << "          archiveSelectedImage="   << selected->image;
}


void archiveManager::updateUI(QModelIndex index)
{
    QString keyWords        = index.sibling(index.row(),3).data().toString();
    QString notes           = index.sibling(index.row(),6).data().toString();
    QString loc_txt         = index.sibling(index.row(),10).data().toString();

    ui->label_archiveTitle->setText(selected->title);
    ui->label_archiveInfoTitle->setText(selected->title);

    QString tnum = tools::extractNumber(selected->title);
    QString numRoman = tools::toRoman(tnum.toInt());
    QString title2 = selected->title;
    if (tnum.length()) numRoman.append('.');
    title2.replace(tnum,numRoman);
    title2.replace("pt.","");


    ui->label_archiveInfoTitle_2->setText(title2);


    ui->label_archiveInfoID->setText( QString::number( selected->id));
    ui->label_archiveInfoDate->setText(selected->date);
    ui->label_archiveDate->setText(selected->date);
    QStringList locSplit=loc_txt.split(",");
    if (locSplit.count()>0) loc_txt = locSplit.at(0);
    ui->label_archiveInfoLocation->setText(loc_txt);
    if (loc_txt=="") ui->label_archiveInfoLocation->setText("-not set-");
    ui->plainTextEdit_archiveKeywords->setPlainText(keyWords);
    ui->plainTextEdit_notes->setPlainText(notes);
    ui->pushButton_archiveUpdateKeywords->setEnabled(false);
    ui->pushButton_archiveUpdateNotes->setEnabled(false);
}

void archiveManager::selectNext()
{
    int parentRow   = ui->treeView_archiveTree->currentIndex().parent().row();
    int row         = ui->treeView_archiveTree->currentIndex().row();

    int newRow=row+1;
    int newParentRow=parentRow;
    if (!modelTree->index(parentRow,0).child(newRow,0).isValid())
        {
            newRow=0;
            newParentRow=parentRow+1;
        }

    if (modelTree->index(newParentRow,0).child(newRow,0).isValid())
        {
            ui->treeView_archiveTree->setCurrentIndex( modelTree->index(newParentRow,0).child(newRow,0) );
            select(ui->treeView_archiveTree->currentIndex());
        }
}

void archiveManager::selectPrev()
{
    if ( !ui->treeView_archiveTree->currentIndex().isValid() ) return ;
    int parentRow   = ui->treeView_archiveTree->currentIndex().parent().row();
    int row         = ui->treeView_archiveTree->currentIndex().row();

    if (!( (parentRow==0) & (row==0)))
        {
            int newRow=row-1;
            int newParentRow = parentRow;
            if (row==0)
                {
                    if ( parentRow>0)
                        {
                            newParentRow = parentRow-1;
                            int i;
                            for (i=1000;i>=0;--i)
                                {
                                    if ( modelTree->index(newParentRow,0).child(i,0).isValid() ) break;
                                }
                            newRow=i;
                        }
                }

            ui->treeView_archiveTree->setCurrentIndex( modelTree->index(newParentRow,0).child(newRow,0) );
            select(ui->treeView_archiveTree->currentIndex());
        }
}

void archiveManager::select(QModelIndex index)
{
    qDebug() << "      --> archiveSelect()";

    qDebug() << "  1";
    ui->listView_quickTag->setVisible(false);
    if (tools::dataFromIndex(index,1)=="")
        {
            qDebug() << "  2";
            browserHighlightBranch(index);
            qDebug() << "  3";
            ui->tabWidget_archiveCenter->setCurrentIndex(1);
            qDebug() << "  4";
        }
        else
        {
            qDebug() << "  5";
            browserHighlightClear(index);
            qDebug() << "  6";
            setSelected(index);
            qDebug() << "  7";
            sidebarMapUpdate(index);
            qDebug() << "  8";
            //debugSelected();
            qDebug() << "  9";
            updateUI(index);
            qDebug() << " 10";
            fileNaviUpdate(selected->folder);
            qDebug() << " 11";
            previewImageLoad();
            qDebug() << " 12";
            previewImageResize();
            qDebug() << " 13";
            sidebarImageLoad();
            qDebug() << " 14";
            sitesGet();
            qDebug() << " 15";
            scoreGet();
            qDebug() << " 16";
            //buildGraph();
            qDebug() << " 17";
            browserSelectByTitle();
            qDebug() << " 18";
            getSales();
            qDebug() << " 19";

        }

    qDebug() << "      <-- archiveSelect()";
}

void archiveManager::getSales()
{
    QString query = "SELECT date,site,saletype,amount FROM sales WHERE archiveid="+QString::number(selected->id) + " ORDER BY date";
    QSqlQueryModel *m = new QSqlQueryModel(this);
    m->setQuery(query,libMan->db);
    ui->tableView_sales->setModel(m);
    ui->tableView_sales->setColumnWidth(0,55);
    ui->tableView_sales->setColumnWidth(1,60);
    ui->tableView_sales->setColumnWidth(2,50);
    ui->tableView_sales->setColumnWidth(3,50);
    //ui->tableView_sales->horizontalHeader()->stretchLastSection();

    QSqlQueryModel *m1 = new QSqlQueryModel(this);
    query = "SELECT COUNT(*) as cnt,SUM(amount) as sum FROM sales WHERE archiveid="+QString::number(selected->id);
    m1->setQuery(query,libMan->db);
    ui->label_salesCount->setText(m1->record(0).value("cnt").toString());
    ui->label_salesTotal->setText(m1->record(0).value("sum").toString()
                                  + " (" + QString::number(m1->record(0).value("sum").toFloat()*288) + ")"
                                  );

}

QSqlQueryModel* archiveManager::interpolator(QString val)
{
    qDebug() << "start interpolate:" + val;
    QSqlDatabase dbm = QSqlDatabase::addDatabase("QSQLITE");
    dbm.setDatabaseName(":memory:");
    qDebug() << "dbm open" << dbm.open();

    dbm.exec("DROP TABLE itp");
    dbm.exec("CREATE TABLE itp (  id   INTEGER PRIMARY KEY AUTOINCREMENT, date TEXT, day  INTEGER, DA   INTEGER,  FB   INTEGER, "
             " [5P] INTEGER, FL   INTEGER, PX   INTEGER, YP   INTEGER, RD INTEGER, VB INTEGER , IG INTEGER , SUM  INTEGER         );");


    QSqlQueryModel *tmpModel = new QSqlQueryModel(this);

    QString query = " SELECT s.site,l.views,l.favs,l.update_date,JULIANDAY(l.update_date) day FROM sites s "
                    " LEFT JOIN archive a ON a.id=s.projectid "
                    " LEFT JOIN siteupdatelog l ON l.siteid=s.id "
                    " WHERE a.id=" + QString::number(selected->id) +
                    " AND s.site!='G+' "
                    " ORDER BY update_date"
            ;
    tmpModel->setQuery(query,libMan->db);

    for (int i=0; i<tmpModel->rowCount(); ++i)
    {
        dbm.exec ( " INSERT OR REPLACE INTO itp (id,date,day) VALUES ( (SELECT id FROM itp WHERE date='"
                            + tmpModel->record(i).value("update_date").toString() +  "' ) "
                            " , '" + tmpModel->record(i).value("update_date").toString() +"' ,"
                            + tmpModel->record(i).value("day").toString() +
                            ")"   );
    }

    for (int i=0; i<tmpModel->rowCount(); ++i)
    {
        dbm.exec ( " UPDATE itp SET `"
                          +tmpModel->record(i).value("site").toString()+
                          "` = "
                          +tmpModel->record(i).value(val).toString()+
                          " WHERE date = '"
                          +tmpModel->record(i).value("update_date").toString()+"' " );
    }

    query = "SELECT * FROM itp ORDER BY date";
    tmpModel->setQuery(query,dbm);
    int cnt = tmpModel->rowCount();
    query="";
    for (int i=0; i < cnt ;++i)
    {
        for (int s=0;s<7;++s)
        {
            QString site;
            if (s==0) site = "DA";
            if (s==1) site = "FB";
            if (s==2) site = "5P";
            if (s==3) site = "FL";
            if (s==4) site = "PX";
            if (s==5) site = "YP";
            if (s==6) site = "RD";
            if (s==7) site = "VB";
            if (s==8) site = "IG";

            int prevVal=0;
            int nextVal=0;
            int interVal=0;
            bool prevFound = false;
            bool nextFound = false;
            int prevDay = 0;
            int nextDay = 0;
            int curDay =0;
            int val = tmpModel->record(i).value(site).toInt();
            if (val==0)
            {
                curDay = tmpModel->record(i).value("day").toInt();
                for (int ii=i;ii>=0;--ii)
                {
                    if (!prevFound && tmpModel->record(ii).value(site).toInt() >0 )
                    {
                        prevVal = tmpModel->record(ii).value(site).toInt();
                        prevDay = tmpModel->record(ii).value("day").toInt();
                        prevFound = true;
                    }
                }

                if (prevFound)
                {
                    for (int ii=i;ii<cnt;++ii)
                    {
                        if (!nextFound && tmpModel->record(ii).value(site).toInt() >0)
                        {
                            nextVal = tmpModel->record(ii).value(site).toInt();
                            nextDay = tmpModel->record(ii).value("day").toInt();
                            nextFound = true;
                        }
                    }
                }

                if (prevFound && nextFound)
                {
                    interVal = prevVal+(nextVal-prevVal)/(nextDay-prevDay)*(curDay-prevDay);
                }

                if (prevFound && !nextFound)
                {
                    interVal = prevVal;
                }

                if (interVal !=0)
                {
                    dbm.exec("UPDATE itp SET `" + site + "`=" + QString::number(interVal)
                           + " WHERE id=" + tmpModel->record(i).value("id").toString() );

                }
            }
        }

    }

    query = "SELECT *,COALESCE(DA,0)+COALESCE(FB,0)+COALESCE(`5P`,0)+COALESCE(FL,0)+COALESCE(PX,0)+COALESCE(YP,0)+COALESCE(RD,0)+COALESCE(VB,0)+COALESCE(IG,0) summ FROM itp ORDER BY date";
    tmpModel->setQuery(query,dbm);
    return tmpModel;
}

void archiveManager::buildGraph()
{

    QSqlQueryModel* tmpModel     = interpolator("views");
    QSqlQueryModel* tmpModelfavs = interpolator("favs");

    ui->widget_lineGraph->clear();
    ui->widget_lineGraph->startX = tmpModel->record(0).value("date").toString();
    ui->widget_lineGraph->endX   = tmpModel->record(tmpModel->rowCount()-1).value("date").toString();

    for (int i=0;i<tmpModel->rowCount();++i)
    {       
        ui->widget_lineGraph->values << tmpModel->record(i).value("summ").toInt();
        ui->widget_lineGraph->values1 << tmpModelfavs->record(i).value("summ").toInt();
        ui->widget_lineGraph->labels << tmpModel->record(i).value("day").toInt();

    }

     ui->widget_lineGraph->repaint();




}

/*  ┌─┬───┬─┐
 *  │ │   │█│   @RIGHT SIDEBAR
 *  │ │   │█│
 *  └─┴───┴─┘
 */

//----@KEYWORDS----

void archiveManager::sidebarBlockToggle()
{
    QString sndr = sender()->objectName();



    qDebug() << sndr;
    qDebug() << sidebarItems.at(0)->name;
    qDebug() << sender();

    for (int i=0; i<sidebarItems.count();++i)
        {
            if( sender() == sidebarItems.at(i)->header)
                {
                    sidebarItems.at(i)->toggle();
                }
        }

    /*
    if (sndr=="widget_archive_sidebar_preview_header")
        {
            if(ui->widget_archive_sidebar_preview_content->isVisible())
                {
                    ui->widget_archive_sidebar_preview_content->setVisible(false);
                    ui->label_archive_sidebar_preview_toggle->setPixmap(QPixmap(":/icons/icons/stylesheet-branch-left.png"));
                }
                else
                {
                    ui->widget_archive_sidebar_preview_content->setVisible(true);
                    ui->label_archive_sidebar_preview_toggle->setPixmap(QPixmap(":/icons/icons/stylesheet-branch-open.png"));
                }
        }
    if (sndr=="widget_archive_sidebar_info_header")
        {
            if(ui->widget_archive_sidebar_info_content->isVisible())
                {
                    ui->widget_archive_sidebar_info_content->setVisible(false);
                    ui->label_archive_sidebar_info_toggle->setPixmap(QPixmap(":/icons/icons/stylesheet-branch-left.png"));
                }
                else
                {
                    ui->widget_archive_sidebar_info_content->setVisible(true);
                    ui->label_archive_sidebar_info_toggle->setPixmap(QPixmap(":/icons/icons/stylesheet-branch-open.png"));
                }
        }
     */
}

void archiveManager::keywordsUpdate()
{
    QString kv = ui->plainTextEdit_archiveKeywords->toPlainText();
    kv.replace(",","");
    QString query="UPDATE archive SET keywords='"+kv+"' WHERE id="+QString::number(selected->id) ;
    libMan->db.exec(query);
    ui->pushButton_archiveUpdateKeywords->setEnabled(false);
    QModelIndex index = ui->listView_archiveTree->currentIndex();

    filter("");
    ui->listView_archiveTree->setCurrentIndex(index);
    focusLists();
}

void archiveManager::keywordUpdateButtonEnable()
{
    ui->pushButton_archiveUpdateKeywords->setEnabled(true);
}

void archiveManager::keywordsToClipboradSpaces()
{
    QClipboard *clipboard = QApplication::clipboard() ;
    clipboard->setText(ui->plainTextEdit_archiveKeywords->toPlainText());
    emit overlayMsg("Keywords copied to clipboard with spaces.");
}

void archiveManager::keywordsToClipboradCommas()
{
    QClipboard *clipboard = QApplication::clipboard() ;
    clipboard->setText(ui->plainTextEdit_archiveKeywords->toPlainText().replace(" ",","));
    emit overlayMsg("Keywords copied to clipboard with commas.");
}

void archiveManager::titleToClipboard()
{
    QClipboard *clipboard = QApplication::clipboard() ;
    clipboard->setText(ui->label_archiveInfoTitle_2->text());
    emit overlayMsg("Title '"+ ui->label_archiveInfoTitle_2->text() +"' copied to clipboard.");
}



void archiveManager::keywordAdd()
{

    qDebug() << "ADD" << qrand() ;

    QString space = " ";
    if (ui->plainTextEdit_archiveKeywords->toPlainText()=="") space = "";
    ui->plainTextEdit_archiveKeywords->moveCursor(QTextCursor::End);
    ui->plainTextEdit_archiveKeywords->insertPlainText( space + ui->lineEdit_addKeyword->text() );
    ui->plainTextEdit_archiveKeywords->moveCursor(QTextCursor::End);

    ui->lineEdit_addKeyword->setText("");

    quickTag();

    /*ui->lineEdit_addKeyword->setText("");
    ui->lineEdit_addKeyword->setText("");*/

}

void archiveManager::keywordCompleter(QString text)
{
    Q_UNUSED(text);
    qDebug() << "completer";
    QStringList completionlist;

    //completionlist << "sky" << "sunset" << "skyscape" << "scenery" << "panorama" << "hdr" << "valami" << "vaaa";

    QSqlQueryModel *tmpModel = new QSqlQueryModel(this);
    tmpModel->setQuery("SELECT keywords FROM archive WHERE keywords != ''",libMan->db);

    while (tmpModel->canFetchMore()) tmpModel->fetchMore();

    for (int i=0;i<tmpModel->rowCount();++i)
        {
            foreach(QString s,tmpModel->index(i,0).data().toString().split(" ")) if (!completionlist.contains(s)) completionlist << s;
        }

    completionlist.sort();

    qDebug() << completionlist;

    QCompleter *completer =  new QCompleter(completionlist,this);
    completer->setCaseSensitivity(Qt::CaseInsensitive);
    completer->setCompletionMode(QCompleter::PopupCompletion );
    completer->setWrapAround(false);

    ui->lineEdit_addKeyword->setCompleter(completer);
    //ui->lineEdit_addKeyword->blockSignals(true);

    connect ( completer ,   SIGNAL(activated(QString) ) , this , SLOT(completerTest(QString)) );



}

void archiveManager::keywordCloud()
{
    int tabFound = -1;
    int tabNum;

    for (int i = 0;i<ui->tabWidget_archiveCenter->count();++i)
        {
            if (ui->tabWidget_archiveCenter->tabText(i) == "Keywords") tabFound=i;
        }

    if (tabFound != -1)
        {
            tabNum = tabFound;
        }
        else
        {
            tabNum = ui->tabWidget_archiveCenter->addTab(new QWidget(),"Keywords");
        }

    ui->tabWidget_archiveCenter->setCurrentIndex(tabNum);
    ui->tabWidget_archiveCenter->tabBar()->tabButton(tabNum, QTabBar::RightSide)->resize(12,12);

    QSqlQueryModel *tmpModel = new QSqlQueryModel(this);
    tmpModel->setQuery("SELECT keywords from archive WHERE keywords != \"\"",libMan->db);
    while (tmpModel->canFetchMore()) tmpModel->fetchMore();

    QMap<QString,int> kwList;

    for (int i=0; i<tmpModel->rowCount();++i)
        {
            QStringList kws = tmpModel->index(i,0).data().toString().split(" ");
            foreach (QString k,kws)
                {
                    if ( (k!="") & (k!=" ")  ) kwList[k]++;
                }
        }

    QStandardItemModel *qtagModel = new QStandardItemModel(this);

    QMap<QString, int>::iterator i;
    for (i = kwList.begin(); i != kwList.end(); ++i)
        {
            QList<QStandardItem *> row;
            row << new QStandardItem( i.key() + " (" + QString::number(i.value()) + ")" );
            row << new QStandardItem ( QString::number( i.value() ).rightJustified(4,'0'));
            qtagModel->appendRow(row);
        }
    qtagModel->sort(1,Qt::DescendingOrder);
    qDebug() << qtagModel;

    QVBoxLayout *layout = new QVBoxLayout();
    QListView *keywordCloud = new QListView();

    keywordCloud->setModel(qtagModel);
    keywordCloud->setViewMode(QListWidget::IconMode);
    keywordCloud->setResizeMode(QListWidget::Adjust );
    keywordCloud->setDragEnabled(false);
    keywordCloud->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    keywordCloud->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    keywordCloud->setSpacing(0);
    keywordCloud->setEditTriggers(QAbstractItemView::NoEditTriggers);
    keywordCloud->setStyleSheet(
                               "QListView {font-size:14px; background:#434343;border:1px solid #434343;color:#EEE; padding:0px; }"
                               "QListView::item {padding:6px;outline:0px;margin:0px;background-color:#626262;border:1px solid #434343;show-decoration-selected: 0;}"
                               "QListView::item:selected {background:#929292;}"
                               "QListView::item:hover {border:1px solid #AAA;}"
                                );
    layout->addWidget(keywordCloud);

    ui->tabWidget_archiveCenter->widget(tabNum)->setLayout(layout);

    connect ( keywordCloud  , SIGNAL(clicked(QModelIndex))          , this  , SLOT(archiveKeywordCloudClicked(QModelIndex)) );
    connect ( keywordCloud  , SIGNAL(doubleClicked(QModelIndex))    , this  , SLOT(archiveKeywordCloudDblClicked(QModelIndex)) );
}

void archiveManager::keywordCloudDoubleClicked(QModelIndex index)
{
    keywordCloudClicked(index);
    ui->tabWidget_archiveCenter->setCurrentIndex(1);
}

void archiveManager::keywordCloudClicked(QModelIndex index)
{
    ui->pushButton_archiveSearchByKeywords->click();
    ui->lineEdit_archiveFilter->setText(  index.model()->index(index.row(),0).data().toString().split(" ").first()  );
}

void archiveManager::quickTagClick(QModelIndex index)
{
    ui->plainTextEdit_archiveKeywords->moveCursor(QTextCursor::End);
    ui->plainTextEdit_archiveKeywords->insertPlainText( " " + ui->listView_quickTag->model()->index(index.row(),0).data().toString() );
    ui->plainTextEdit_archiveKeywords->moveCursor(QTextCursor::End);
    this->quickTag();
}

void archiveManager::quickTag()
{
    QSqlQueryModel *tmpModel = new QSqlQueryModel(this);

    QString query;

    if ( ui->plainTextEdit_archiveKeywords->toPlainText() == "" )
       {
            query = "    SELECT t1.keywords,t1.match FROM ( SELECT  keywords, 1 + ";
            foreach (QString s , selected->title.split(" "))
                {
                    if ( ( s!=" " ) & ( s!="" ) ) query+="CASE WHEN title LIKE '%"+s+"%' then 330 else 0 END +";
                }
            query+= " 0 as match FROM archive  WHERE keywords!='' ) t1 ORDER BY match DESC ";
       }
       else
       {
            query = "    SELECT t1.keywords,t1.match FROM ( SELECT  keywords, 1 + ";
            foreach (QString s , selected->title.split(" "))
                {
                    if ( ( s!=" " ) & ( s!="" ) ) query+="CASE WHEN title LIKE '%"+s+"%' then 100 else 0 END +";
                }
            foreach (QString s , ui->plainTextEdit_archiveKeywords->toPlainText().split(" "))
                {
                    if ( ( s!=" " ) & ( s!="" ) ) query+="CASE WHEN keywords LIKE '%"+s+"%' then 100 else 0 END +";
                }
            query+= " 0 as match FROM archive  WHERE keywords!='' ) t1 ORDER BY match DESC ";
        }

    tmpModel->setQuery(query,libMan->db);
    while (tmpModel->canFetchMore()) tmpModel->fetchMore();

    QMap<QString,int> kwList;

    for (int i=0; i<tmpModel->rowCount();++i)
        {
            QStringList kws = tmpModel->index(i,0).data().toString().split(" ");
            int weight = tmpModel->index(i,1).data().toInt();
            foreach (QString k,kws)
                {
                    if ( (k!="") & (k!=" ") & (!ui->plainTextEdit_archiveKeywords->toPlainText().contains(k)) ) kwList[k]+=weight;
                }
        }

    QStandardItemModel *qtagModel = new QStandardItemModel(this);

    QMap<QString, int>::iterator i;
    for (i = kwList.begin(); i != kwList.end(); ++i)
        {
            QList<QStandardItem *> row;
            row << new QStandardItem( i.key() );
            row << new QStandardItem ( QString::number( i.value() ).rightJustified(4,'0'));
            qtagModel->appendRow(row);
        }

    qtagModel->sort(1,Qt::DescendingOrder);

    for (int i = 0 ; i<20 ;++i) qDebug() << qtagModel->index(i,0).data().toString()<< qtagModel->index(i,1).data().toString();

    ui->listView_quickTag->setModel(qtagModel);
    ui->listView_quickTag->setViewMode(QListWidget::IconMode);
    ui->listView_quickTag->setVisible(true);
}

//----MAP----

void archiveManager::sidebarMapUpdate(QModelIndex index)
{
    QString loc_lat         = index.sibling(index.row(),8).data().toString();
    QString loc_lng         = index.sibling(index.row(),9).data().toString();
    QString loc_txt         = index.sibling(index.row(),10).data().toString();

    ui->lineEdit_locLat->setText("");
    ui->lineEdit_locLng->setText("");
    ui->plainTextEdit_loc->setPlainText("");

    if (loc_lat!="")
        {
            ui->lineEdit_locLat->setText(loc_lat);
            if (loc_lng!="")
                {
                    ui->lineEdit_locLng->setText(loc_lng);
                    QString jsCmd = "setMarker(" +loc_lat + ","+loc_lng+",'"+loc_txt+"');";
                    ui->webView_Map->page()->mainFrame()->evaluateJavaScript(jsCmd);
                    if (loc_txt!="")
                        {
                            ui->plainTextEdit_loc->setPlainText(loc_txt);
                        }
                }
        }
        else ui->webView_Map->page()->mainFrame()->evaluateJavaScript("setDefault();");
}

void archiveManager::sidebarMapPin(QString s)
{
    QStringList loc = s.split("#");
    if (loc.count()>1)
       {
            QString locLat = loc.at(0);
            QString locLng = loc.at(1);
            ui->lineEdit_locLat->setText(locLat);
            ui->lineEdit_locLng->setText(locLng);
            if (loc.count()>2) ui->plainTextEdit_loc->setPlainText(loc.at(2));
            ui->pushButton_locSave->setEnabled(true);
       }
}

void archiveManager::sidebarMapSaveLoc()
{
    QString query=  "UPDATE archive SET "
                    "location_lat='" + ui->lineEdit_locLat->text() + "', "
                    "location_lng='" + ui->lineEdit_locLng->text() + "', "
                    "location_text='"+ ui->plainTextEdit_loc->toPlainText()+ "' "
                    "WHERE id="+ QString::number( selected->id );

    libMan->db.exec(query);
    ui->pushButton_locSave->setEnabled(false);
}


//----@NOTES----

void archiveManager::notesUpdate()
{
    QString query="UPDATE archive SET notes='"+ui->plainTextEdit_notes->toPlainText()+"' WHERE id="+QString::number(selected->id) ;
    libMan->db.exec(query);
    ui->pushButton_archiveUpdateNotes->setEnabled(false);

    filter("");

    QModelIndex index=modelTree->findItems( ui->label_archiveInfoTitle->text() ,Qt::MatchRecursive).first()->index();
    ui->treeView_archiveTree->setCurrentIndex( index );
    select(index);
}

void archiveManager::noteUpdateButtonEnable()
{
    ui->pushButton_archiveUpdateNotes->setEnabled(true);
}


//----@SCORE----

void archiveManager::scoreGet()
{
    qDebug() << "        --> archiveGetScore()";

    //QString query=libMan->loadQuery("score.sql");
    QString query="SELECT score FROM score WHERE projectid=" + QString::number(selected->id);

    //query.replace("%1",selected->title);
    //query.replace("[P2]",QString::number(libMan->scoreBias));
    qDebug() << "            title:" << selected->title;
    qDebug() << query;

    QSqlQueryModel *scoreModel = new QSqlQueryModel(this);

    scoreModel->setQuery(query,libMan->db);

    float score = scoreModel->index(0,0).data().toFloat() * libMan->scoreBias * 5;
    qDebug() << libMan->scoreBias;
    qDebug() << score;
    QString scoreDisp = "";
    scoreDisp.sprintf("%1.2f",score);
    ui->label_score->setText(scoreDisp);
    if (score==0) ui->label_score->setText("0.00");

    ui->label_scoreStar1->setPixmap(QPixmap(":/icons/icons/star_off_d_32.png"));
    ui->label_scoreStar2->setPixmap(QPixmap(":/icons/icons/star_off_d_32.png"));
    ui->label_scoreStar3->setPixmap(QPixmap(":/icons/icons/star_off_d_32.png"));
    ui->label_scoreStar4->setPixmap(QPixmap(":/icons/icons/star_off_d_32.png"));
    ui->label_scoreStar5->setPixmap(QPixmap(":/icons/icons/star_off_d_32.png"));

    if (score>=0.25) ui->label_scoreStar1->setPixmap(QPixmap(":/icons/icons/star_half_d_32.png"));
    if (score>=0.75) ui->label_scoreStar1->setPixmap(QPixmap(":/icons/icons/star_32.png"));
    if (score>=1.25) ui->label_scoreStar2->setPixmap(QPixmap(":/icons/icons/star_half_d_32.png"));
    if (score>=1.75) ui->label_scoreStar2->setPixmap(QPixmap(":/icons/icons/star_32.png"));
    if (score>=2.25) ui->label_scoreStar3->setPixmap(QPixmap(":/icons/icons/star_half_d_32.png"));
    if (score>=2.75) ui->label_scoreStar3->setPixmap(QPixmap(":/icons/icons/star_32.png"));
    if (score>=3.25) ui->label_scoreStar4->setPixmap(QPixmap(":/icons/icons/star_half_d_32.png"));
    if (score>=3.75) ui->label_scoreStar4->setPixmap(QPixmap(":/icons/icons/star_32.png"));
    if (score>=4.25) ui->label_scoreStar5->setPixmap(QPixmap(":/icons/icons/star_half_d_32.png"));
    if (score>=4.75) ui->label_scoreStar5->setPixmap(QPixmap(":/icons/icons/star_32.png"));

    qDebug() << "        <-- archiveGetScore()";
}

//----SITES----

void archiveManager::siteClick()
{
    QString url="";
    QString siteId;
    QString sndr=sender()->objectName();
    if (sndr == "label_siteDaIcon")         siteId="DA";
    if (sndr == "label_site500pxIcon")      siteId="5P";
    if (sndr == "label_siteFlickrIcon")     siteId="FL";
    if (sndr == "label_siteViewbugIcon")    siteId="VB";
    if (sndr == "label_siteFBIcon")         siteId="FB";
    if (sndr == "label_sitePixotoIcon")     siteId="PX";
    if (sndr == "label_siteGPIcon")         siteId="G+";
    if (sndr == "label_siteYoupicIcon")     siteId="YP";
    if (sndr == "label_siteRdIcon")         siteId="RD";
    if (sndr == "label_siteIgIcon")         siteId="IG";

    qDebug() << selected->id;

    QString query = "SELECT s.url FROM sites s LEFT JOIN archive a ON a.id=s.projectid WHERE a.id="
                    +QString::number(selected->id)+" AND s.site='"+siteId+"' ";
    qDebug()<<query;
    QSqlQueryModel *tmpModel = new QSqlQueryModel(this);
    tmpModel->setQuery(query,libMan->db);
    while(tmpModel->canFetchMore()) tmpModel->fetchMore();
    qDebug() << tmpModel->record(0).value("url").toString();
    url = tmpModel->record(0).value("url").toString();


    tmpModel->clear();
    query = "SELECT value FROM var WHERE KEY='browser'";
    tmpModel->setQuery(query,libMan->db);
    QString ss = tmpModel->index(0,0).data().toString();
    if (url!="")
    {
        if (ss=="EXT" || ss=="")
        {
             QDesktopServices::openUrl(QUrl(url));
        }
        if (ss=="INT")
        {
            ui->tabWidget_archiveCenter->setCurrentIndex(2);
            ui->lineEdit_archiveUrl->setText(url);
            ui->webView->load( QUrl( url ));
        }
    }

    /*
    QString query = " SELECT * FROM sites s "
                    " LEFT JOIN siteupdatelog l ON l.siteid=s.id "
                    " WHERE s.projectid = " + QString::number(selected->id) +
                    " AND s.site = '" + siteId + "' "
            ; */
    /*
    QString query = "SELECT l.id id,l.siteid siteid, l.views views,l.favs favs, l.comments comments,l.update_date update_date, "
                    " CAST(julianday(l.update_date) as INTEGER) - "
                    " ( "
                    "    SELECT MIN(CAST(julianday(l1.update_date) as INTEGER)) "
                    "    FROM sites s1 "
                    "    LEFT JOIN siteupdatelog l1 ON l1.siteid=s1.id "
                    "    WHERE s1.projectid = " + QString::number(selected->id) +
                    "    AND s1.site='" + siteId + "' "
                    " )  days "
                    " FROM sites s  "
                    " LEFT JOIN siteupdatelog l "
                    " ON l.siteid=s.id  "
                    " WHERE s.projectid = "  + QString::number(selected->id) +
                    " AND s.site = '" + siteId + "' "
                    " ORDER BY l.update_date,l.id"
            ;

    QSqlQueryModel *tmpModel = new QSqlQueryModel(this);
    tmpModel->setQuery(query,libMan->db);
    while(tmpModel->canFetchMore()) tmpModel->fetchMore();


    qDebug() << query;

    qDebug() << tmpModel->rowCount();

    //ui->widget_lineGraph->values.insert("a",10);
    //ui->widget_lineGraph->values.insert("b",13);
    //ui->widget_lineGraph->values.insert("x",12);

    ui->widget_lineGraph->clear();
    ui->widget_lineGraph->startX = tmpModel->record(0).value("update_date").toString();
    ui->widget_lineGraph->endX   = tmpModel->record(tmpModel->rowCount()-1).value("update_date").toString();
    //ui->widget_lineGraph->values.insert(siteId,12);
    if (tmpModel->rowCount())
    {
        for (int i=0;i<tmpModel->rowCount();i++)
        {
            qDebug() << i << tmpModel->record(i).value("days").toString() << tmpModel->record(i).value("views").toInt() << tmpModel->record(i).value("update_date").toString() ;
            //ui->widget_lineGraph->values.insert( tmpModel->record(i).value("days").toInt() , tmpModel->record(i).value("views").toInt() );
            ui->widget_lineGraph->values << tmpModel->record(i).value("views").toInt();
            ui->widget_lineGraph->values1 << tmpModel->record(i).value("favs").toInt();
            ui->widget_lineGraph->labels << tmpModel->record(i).value("days").toInt();
        }
    }

    qDebug() << ui->widget_lineGraph->values;

    ui->widget_lineGraph->repaint();
    */

    /*
    bool found = false;
    for (int i=0; i<urlList.count();++i)
        {
            url = urlList.at(i).second;
            if (urlList.at(i).first == siteId ) found = true;
            if (urlList.at(i).first == siteId ) break;
        }
    if (found)
        {
            ui->tabWidget_archiveCenter->setCurrentIndex(2);
            ui->lineEdit_archiveUrl->setText(url);
            ui->webView->load( QUrl( url ));
        }
    */
}

void archiveManager::sitesGet()
{
    qDebug() << "        --> archiveGetSites()";
    qDebug() << "            title:" << selected->title;
    qDebug() << "            id:" << selected->id;
    QString query = "SELECT url,views,favs FROM sites WHERE projectId="+QString::number(selected->id);
    QSqlQueryModel *siteModel=new QSqlQueryModel(this);
    siteModel->setQuery(query,libMan->db);

    urlList.clear();

    // reset counters
    ui->label_siteTotalViews->setText("0");
    ui->label_siteTotalFavs->setText("0");

    // reset DA
    ui->label_siteDaIcon->setPixmap(QPixmap(":/icons/icons/siteIconDeviantArt_GREY.png"));
    ui->label_siteDaIcon->setCursor(Qt::ArrowCursor);
    ui->label_siteDaViews->setText("-");
    ui->label_siteDaFavs->setText("-");

    // reset G+
    ui->label_siteGPIcon->setPixmap(QPixmap(":/icons/icons/siteIconGoogle+_GREY.png"));
    ui->label_siteGPIcon->setCursor(Qt::ArrowCursor);
    ui->label_siteGPViews->setText("-");
    ui->label_siteGPFavs->setText("-");

    // reset FB
    ui->label_siteFBIcon->setPixmap(QPixmap(":/icons/icons/siteIconFaceBook_GREY.png"));
    ui->label_siteFBIcon->setCursor(Qt::ArrowCursor);
    ui->label_siteFBViews->setText("-");
    ui->label_siteFBFavs->setText("-");

    // reset Flickr
    ui->label_siteFlickrIcon->setPixmap(QPixmap(":/icons/icons/siteIconFlickr_GREY.png"));
    ui->label_siteFlickrIcon->setCursor(Qt::ArrowCursor);
    ui->label_siteFlickrViews->setText("-");
    ui->label_siteFlickrFavs->setText("-");

    // reset 500px
    ui->label_site500pxIcon->setPixmap(QPixmap(":/icons/icons/siteIcon500px_GREY.png"));
    ui->label_site500pxIcon->setCursor(Qt::ArrowCursor);
    ui->label_site500pxViews->setText("-");
    ui->label_site500pxFavs->setText("-");

    // reset Pixoto
    ui->label_sitePixotoIcon->setPixmap(QPixmap(":/icons/icons/siteIconPixoto1_GREY.png"));
    ui->label_sitePixotoIcon->setCursor(Qt::ArrowCursor);
    ui->label_sitePixotoViews->setText("-");
    ui->label_sitePixotoFavs->setText("-");

    // reset youpic
    ui->label_siteYoupicIcon->setPixmap(QPixmap(":/icons/icons/siteIconYoupic_GREY.png"));
    ui->label_siteYoupicIcon->setCursor(Qt::ArrowCursor);
    ui->label_siteYoupicViews->setText("-");
    ui->label_siteYoupicFavs->setText("-");

    // reset viewbug
    ui->label_siteViewbugIcon->setPixmap(QPixmap(":/icons/icons/siteIconViewbug_GREY.png"));
    ui->label_siteViewbugIcon->setCursor(Qt::ArrowCursor);
    ui->label_siteViewbugViews->setText("-");
    ui->label_siteViewbugFavs->setText("-");

    // reset realitydream.hu
    ui->label_siteRdIcon->setPixmap(QPixmap(":/icons/icons/siteIconRd_GREY.png"));
    ui->label_siteRdIcon->setCursor(Qt::ArrowCursor);
    ui->label_siteRdViews->setText("-");
    ui->label_siteRdFavs->setText("-");

    // reset Instagram
    ui->label_siteIgIcon->setPixmap(QPixmap(":/icons/icons/siteIconInstagram_GREY.png"));
    ui->label_siteIgIcon->setCursor(Qt::ArrowCursor);
    ui->label_siteIgViews->setText("-");
    ui->label_siteIgFavs->setText("-");

    int totalViews=0;
    int totalFavs=0;

    for (int i=0; i<siteModel->rowCount();++i)
        {
            QString siteUrl     = siteModel->index(i,0).data().toString();
            QString siteViews   = siteModel->index(i,1).data().toString();
            QString siteFavs    = siteModel->index(i,2).data().toString();
            if ( siteUrl.contains("deviantart.com") )
                {
                    ui->label_siteDaIcon->setPixmap(QPixmap(":/icons/icons/siteIconDeviantArt.png"));
                    ui->label_siteDaIcon->setCursor(Qt::PointingHandCursor);
                    ui->label_siteDaViews->setText( tools::numNA( tools::numGrouped( siteViews.toInt()) ) );
                    ui->label_siteDaFavs->setText(  tools::numNA( tools::numGrouped(  siteFavs.toInt()) ) );
                    QString site = "deviantArt";
                    urlList.append(qMakePair(site,siteUrl));
                    totalViews+=siteViews.toInt();
                    totalFavs +=siteFavs.toInt();
                }

            if ( siteUrl.contains("500px.com") )
                {
                    ui->label_site500pxIcon->setPixmap(QPixmap(":/icons/icons/siteIcon500px.png"));
                    ui->label_site500pxIcon->setCursor(Qt::PointingHandCursor);
                    ui->label_site500pxViews->setText(tools::numNA( tools::numGrouped( siteViews.toInt()) ));
                    ui->label_site500pxFavs->setText(tools::numGrouped( siteFavs.toInt() ));
                    QString site = "500px";
                    urlList.append(qMakePair(site,siteUrl));
                    totalViews+=siteViews.toInt();
                    totalFavs +=siteFavs.toInt();
                }

            if ( siteUrl.contains("facebook.com") )
                {
                    ui->label_siteFBIcon->setPixmap(QPixmap(":/icons/icons/siteIconFaceBook.png"));
                    ui->label_siteFBIcon->setCursor(Qt::PointingHandCursor);
                    ui->label_siteFBViews->setText( tools::numNA( tools::numGrouped( siteViews.toInt()) ) );
                    ui->label_siteFBFavs->setText(  tools::numNA( tools::numGrouped(  siteFavs.toInt()) ) );
                    QString site = "Facebook";
                    urlList.append(qMakePair(site,siteUrl));
                    totalViews+=siteViews.toInt();
                    totalFavs +=siteFavs.toInt();
                }

            if ( siteUrl.contains("instagram") )
                {
                    ui->label_siteIgIcon->setPixmap(QPixmap(":/icons/icons/siteIconInstagram.png"));
                    ui->label_siteIgIcon->setCursor(Qt::PointingHandCursor);
                    ui->label_siteIgViews->setText( tools::numNA( tools::numGrouped( siteViews.toInt()) ) );
                    ui->label_siteIgFavs->setText(  tools::numNA( tools::numGrouped(  siteFavs.toInt()) ) );
                    QString site = "Instagram";
                    urlList.append(qMakePair(site,siteUrl));
                    totalViews+=siteViews.toInt();
                    totalFavs +=siteFavs.toInt();
                }

            if ( siteUrl.contains("flickr") )
                {
                    ui->label_siteFlickrIcon->setPixmap(QPixmap(":/icons/icons/siteIconFlickr.png"));
                    ui->label_siteFlickrIcon->setCursor(Qt::PointingHandCursor);
                    ui->label_siteFlickrViews->setText(tools::numNA( tools::numGrouped( siteViews.toInt()) ));
                    ui->label_siteFlickrFavs->setText( tools::numNA( tools::numGrouped(  siteFavs.toInt()) ));
                    QString site = "Flickr";
                    urlList.append(qMakePair(site,siteUrl));
                    totalViews+=siteViews.toInt();
                    totalFavs +=siteFavs.toInt();
                }

            if ( siteUrl.contains("plus.google") )
                {
                    ui->label_siteGPIcon->setPixmap(QPixmap(":/icons/icons/siteIconGoogle+.png"));
                    ui->label_siteGPIcon->setCursor(Qt::PointingHandCursor);
                    ui->label_siteGPViews->setText(tools::numNA( tools::numGrouped( siteViews.toInt()) ));
                    ui->label_siteGPFavs->setText( tools::numNA( tools::numGrouped(  siteFavs.toInt()) ));
                    QString site = "Google+";
                    urlList.append(qMakePair(site,siteUrl));
                    totalViews+=siteViews.toInt();
                    totalFavs +=siteFavs.toInt();
                }
            if ( siteUrl.contains("pixoto") )
                {
                    ui->label_sitePixotoIcon->setPixmap(QPixmap(":/icons/icons/siteIconPixoto1.png"));
                    ui->label_sitePixotoIcon->setCursor(Qt::PointingHandCursor);
                    ui->label_sitePixotoViews->setText(tools::numNA( tools::numGrouped( siteViews.toInt()) ));
                    ui->label_sitePixotoFavs->setText( tools::numNA( tools::numGrouped(  siteFavs.toInt()) ));
                    QString site = "Pixoto";
                    urlList.append(qMakePair(site,siteUrl));
                    totalViews+=siteViews.toInt();
                    totalFavs +=siteFavs.toInt();
                }

            if ( siteUrl.contains("youpic") )
                {
                    ui->label_siteYoupicIcon->setPixmap(QPixmap(":/icons/icons/siteIconYoupic.png"));
                    ui->label_siteYoupicIcon->setCursor(Qt::PointingHandCursor);
                    ui->label_siteYoupicViews->setText(tools::numNA( tools::numGrouped( siteViews.toInt()) ));
                    ui->label_siteYoupicFavs->setText( tools::numNA( tools::numGrouped(  siteFavs.toInt()) ));
                    QString site = "Youpic";
                    urlList.append(qMakePair(site,siteUrl));
                    totalViews+=siteViews.toInt();
                    totalFavs +=siteFavs.toInt();
                }

            if ( siteUrl.contains("viewbug") )
                {
                    ui->label_siteViewbugIcon->setPixmap(QPixmap(":/icons/icons/siteIconViewbug.png"));
                    ui->label_siteViewbugIcon->setCursor(Qt::PointingHandCursor);
                    ui->label_siteViewbugViews->setText(tools::numNA( tools::numGrouped( siteViews.toInt()) ));
                    ui->label_siteViewbugFavs->setText( tools::numNA( tools::numGrouped(  siteFavs.toInt()) ));
                    QString site = "viewbug";
                    urlList.append(qMakePair(site,siteUrl));
                    totalViews+=siteViews.toInt();
                    totalFavs +=siteFavs.toInt();
                }

            if ( siteUrl.contains("realitydream.hu") )
                {
                    ui->label_siteRdIcon->setPixmap(QPixmap(":/icons/icons/siteIconRd.png"));
                    ui->label_siteRdIcon->setCursor(Qt::PointingHandCursor);
                    ui->label_siteRdViews->setText(tools::numNA( tools::numGrouped( siteViews.toInt()) ));
                    ui->label_siteRdFavs->setText("N/A");
                    QString site = "realitydream.hu";
                    urlList.append(qMakePair(site,siteUrl));
                    totalViews+=siteViews.toInt();
                    totalFavs +=siteFavs.toInt();
                }
        }

    ui->label_siteTotalViews->setText( tools::numGrouped( totalViews ) );
    ui->label_siteTotalFavs->setText(  tools::numGrouped( totalFavs  ) );
    QSqlQueryModel *tmpModel2 = new QSqlQueryModel(this);
    tmpModel2->setQuery("SELECT MIN(julianday(Date('now')) - julianday(s.updated)) , MAX(julianday(Date('now')) - julianday(s.updated)) FROM archive p LEFT JOIN sites s ON s.projectid=p.id WHERE s.url NOT NULL AND s.site !='G+' AND s.site !='RD' AND s.site !='VB' AND p.id="+QString::number(selected->id),libMan->db);
    int minUp = tmpModel2->index(0,0).data().toInt();
    int maxUp = tmpModel2->index(0,1).data().toInt();
    QString colorMin;
    QString colorMax;
    (minUp>=10) ? colorMin="#F20" : colorMin="#DDD";
    (maxUp>=10) ? colorMax="#F20" : colorMax="#DDD";
    ui->label_archive_uptoDate->setText(  "    <span style=\"color:"+colorMin+"\"> "+QString::number(minUp) +" </span>"
                                          " to <span style=\"color:"+colorMax+"\"> "+QString::number(maxUp)+" </span> day(s)" );

    qDebug() << "            loaded:" << urlList.count() << " sites";
    qDebug() << "        <-- archiveGetSites()";
}


/*  ┌─┬───┬─┐
 *  │ │   │ │   @COMMON
 *  │ │   │ │
 *  └─┴───┴─┘
 */

void archiveManager::modelSetQueryAndFetch(QString query)
{
    model->setQuery(query,libMan->db);
    while(model->canFetchMore()) model->fetchMore();
}

void archiveManager::refresh()
{
    reConvert();
    treeExpandCollapse(ui->pushButton_archiveTreeExpand->isChecked());
    focusLists();
}

void archiveManager::reConvert()
{
    if (ui->lineEdit_archiveFilter->text()!="")
        {
            filter("");
        }
        else
        {
            treeModelConvert();
            ui->treeView_archiveTree->setModel(modelTree);
        }
}

QModelIndex archiveManager::modelTreeIndexOf(QString title)
{
    Q_UNUSED(title);
    QList<QStandardItem *> findList = modelTree->findItems( selected->title ,Qt::MatchRecursive);
    if (findList.count()>0)  return findList.first()->index();
        else return QModelIndex();
}

void archiveManager::focusLists()
{
    if ( selected->title == "") return;
    treeFocus();
    browserFocus();
}

void archiveManager::testUi()
{
    //ui->tabWidget->tabBar()->setVisible(false);
    ui->tabWidget->tabBar()->setVisible(true);
}



QModelIndex archiveManager::findItem(QString title)
{
    QModelIndex index=modelTree->findItems(title,Qt::MatchRecursive).first()->index();
    return index;
}

QList<QString> archiveManager::getTitles()
{
    QSqlQueryModel *gt = new QSqlQueryModel(this);
    gt->setQuery("SELECT title FROM archive",libMan->db);
    while (gt->canFetchMore()) gt->fetchMore();

    QList<QString> titleList;

    for (int i=0; i<gt->rowCount();++i) titleList << gt->index(i,0).data().toString();

    return titleList;
}

void archiveManager::filter(QString text)
{
    qDebug() << "--> archiveFilter()";
    qDebug() << "    "<< text;
    qDebug() << "----------------------";
    QModelIndex index = ui->listView_collections->currentIndex();
    if (index.isValid())
        {
            QString cType = index.model()->index(index.row(),1).data().toString();
            QString cText = index.model()->index(index.row(),2).data().toString();
            QString cId   = index.model()->index(index.row(),3).data().toString();
            if ( ( cType=="smart" ) & ( cText != text )       ) ui->listView_collections->selectionModel()->reset();
            if ( ( cType=="item"  ) & ( "#col:"+cId != text ) ) ui->listView_collections->selectionModel()->reset();
        }

    filterDo(ui->lineEdit_archiveFilter->text());

    ui->treeView_archiveTree->setModel(modelTree);
    if (ui->pushButton_archiveTreeExpand->isChecked()) ui->treeView_archiveTree->expandAll();
    ui->tabWidget_archiveTrees->setTabText(0,"Archives ("+QString::number(model->rowCount())+") *");

    if (ui->tabWidget_archiveCenter->currentIndex()==3)
        {
            fullMapTimer->stop();
            fullMapTimer->setInterval(1000);
            fullMapTimer->start();
        }

    qDebug() << "<-- archiveFilter()";
}

void archiveManager::filterDo(QString filterStr)
{
    qDebug() << "archivefilter";

    this->scoringMode = tools::queryFirst(libMan->db,"SELECT value FROM var WHERE key='scoring'");
    if (this->scoringMode=="") this->scoringMode="LNR";
    qDebug() << "                 " << this->scoringMode;

    QString queryName;
    if (scoringMode=="LNR") queryName = "unified_LNR.sql";
    if (scoringMode=="SQR") queryName = "unified_SQR.sql";
    if (scoringMode=="YWG") queryName = "unified_YWG.sql";
    qDebug() << scoringMode;
    qDebug() << queryName;

    // calculate scorebias
    qDebug() << "old scorebias=" << libMan->scoreBias;
    QString query=libMan->loadQuery(queryName);
    query.replace("[P2]","1");
    if (filterOneSite_On) query.replace("[FILTER]",onesiteFilter);
      else query.replace("[FILTER]"  , "");
    query.replace("[ORDERBY]" , "score.score * 5  DESC , title" );
    //qDebug() << query;
    modelSetQueryAndFetch(query);
    qDebug() << "TOP base SCORE=" << model->record(0).value("score").toString() << " (" << model->record(0).value("title").toString()<<") ";
    libMan->scoreBias = 5.0 / model->record(0).value("score").toFloat() ;
    model->clear();
    qDebug() << "new scorebias=" << libMan->scoreBias;
    // done


    query=libMan->loadQuery(queryName);
    query.replace("[P2]",QString::number(libMan->scoreBias));

    QString filter;

    if (filterStr.startsWith("#col:"))
        {
            filter = "AND archive.collections LIKE '%#"+ filterStr.replace("#col:","") +"#%'";
        }
        else
        {

        if (filterStr.split(",").count()>1)
            {
                QString filterQ="AND ( 1 = 0 ";

                foreach (QString filterI,filterStr.split(","))
                    if (filterI!="")
                    {

                                    if (filterOpt.contains("T")) filterQ += "   OR archive.title LIKE '%"+filterI+"%'  ";
                                    if (filterOpt.contains("K")) filterQ += "   OR archive.keywords LIKE '%"+filterI+"%'  ";
                                    if (filterOpt.contains("N")) filterQ += "   OR archive.notes LIKE '%"+filterI+"%'  ";
                                    if (filterOpt.contains("L")) filterQ += "   OR archive.location_text LIKE '%"+filterI+"%'  ";
                                    if (filterOpt.contains("D")) filterQ += "   OR archive.date LIKE '%"+filterI+"%'  ";

                    }

                filterQ+=" )";

               filter =  filterQ;

            }
            else
            {

                QString filterQ="AND ( 1 = 0 ";

                if (filterOpt.contains("T")) filterQ += "   OR archive.title LIKE '%"+filterStr+"%'  ";
                if (filterOpt.contains("K")) filterQ += "   OR archive.keywords LIKE '%"+filterStr+"%'  ";
                if (filterOpt.contains("N")) filterQ += "   OR archive.notes LIKE '%"+filterStr+"%'  ";
                if (filterOpt.contains("L")) filterQ += "   OR archive.location_text LIKE '%"+filterStr+"%'  ";
                if (filterOpt.contains("D")) filterQ += "   OR archive.date LIKE '%"+filterStr+"%'  ";

                filterQ+=" )";

                filter = filterQ;
            }
        }

    if (filterOneSite_On) filter+=onesiteFilter;

    query.replace("[FILTER]",filter);

    QString searchQuery = query;
    searchQuery.replace("[ORDERBY]" , "title" );

    if (groupType=="A-Z")   query.replace("[ORDERBY]" , "title" );
    if (groupType=="DATE")  query.replace("[ORDERBY]" , "date" );
    if (groupType=="LOC")   query.replace("[ORDERBY]" , "location_text,title" );
    if (groupType=="SCORE") query.replace("[ORDERBY]" , "score.score  * 5  DESC , title" );

    qDebug() << query;

    model->setQuery(query,libMan->db);
    while(model->canFetchMore()) model->fetchMore();

    searchModel->setQuery(searchQuery,libMan->db);
    while(searchModel->canFetchMore()) searchModel->fetchMore();

    treeModelConvert();
}

QTreeWidgetItem archiveManager::folderTree(QString folder)
{
    /*
    QStringList folderTree = folder.split("/");
    if (folderTree.count()==1) folderTree=folder.split("\\");
    QTreeWidgetItem parentItem;
    //parentItem = new QTreeWidgetItem;

    if (folderTree.count()>1)
        {



            for (int i=folderTree.count()-1;i>-1;--i)
                {
                    QTreeWidgetItem childItem;
                    //childItem = new QTreeWidgetItem;
                    childItem.setText(0,folderTree[i]);
                    //childItem->setExpanded(true);
                    if (i!=folderTree.count()-1)  childItem.addChild(&parentItem);
                    parentItem = childItem;
                }

        }
    return parentItem;
    */
    Q_UNUSED(folder);
    QTreeWidgetItem w;
    return w;
}

void archiveManager::thumbModelConvert()
{
    qDebug() << "      --> thumbModelConvert()";

    qDebug() << "      <-- thumbModelConvert()";
}

void archiveManager::treeModelConvert()
{
    /****************************   MODEL TREE EXPERIMENT  ***************************************/
    qDebug() << "      --> archiveTreeModelConvert()";

    //archiveModelTree = new QStandardItemModel(this);
    modelTree->clear();

    QString prevFirstLetter="";
    int childCount=-1;

    QStandardItem *parent       = new QStandardItem();
    QStandardItem *prevParent   = new QStandardItem();
    QStandardItem *child        = new QStandardItem();
    //QList<QStandardItem> *row          =new QList<QStandardItem>; // unused
    //QStandardItemModel *abc  = new QStandardItemModel(); //unused


    modelThumb->clear();


    for (int i=0;i<model->rowCount();++i)
        {
            QSqlRecord record = model->record(i);
            QList<QStandardItem *> row;
            //parent = parent->clone();
            QString title = record.field(0).value().toString();
            for (int j = 0;j<record.count();++j)
                {
                    row << new QStandardItem( record.field(j).value().toString() );
                    //qDebug() << record.field(j).value().toString();
                    //child = child->clone();
                    //child->setText(   record.field(j).value().toString());
                    //parent->setChild(0,j,child);
                }
            //qDebug() << parent;
            //qDebug() << "              TITLE=" << title;
            if (additionalFilter_On)
                {
                    if (additionalFilter.contains(title))  modelThumb->appendRow(row);
                }
                else
                {
                    modelThumb->appendRow(row);
                }
        }

    //for (int i=0;i<=model->rowCount();++i)
      //  {
            //QSqlRecord record = model->record(i);
            //QList<QStandardItem *> row;
            //for (int j = 0;j<record.count();++j)
              //  {
                    //row << new QStandardItem( record.field(j).value().toString() );
              //  }

            //modelThumb->appendRow(row);
       // }

    if (treeGrouped)
    {
    //qDebug() << "          TREE GROUPED";
    for (int i=0;i<=model->rowCount();++i)
        {


            QString firstLetter;

            if (groupType=="A-Z")
                {
                    firstLetter=model->index(i,0).data().toString().at(0);
                    if (firstLetter.toInt()>0) firstLetter="#";
                    //firstLetter="#";
                }
            if (groupType=="DATE") firstLetter=model->index(i,4).data().toString().mid(0,4);

            if (groupType=="SCORE")
                {
                    //firstLetter=model->index(i,7).data().toString().mid(0,3);
                    //if ( firstLetter=="0" ) {  firstLetter="0.0"; }
                    float score = model->index(i,7).data().toFloat();

                    if (score >= 4.50)          firstLetter ="5.00 - 4.50";
                    else if (score >= 4.00)     firstLetter ="4.50 - 4.00";
                    else if (score >= 3.50)     firstLetter ="4.00 - 3.50";
                    else if (score >= 3.00)     firstLetter ="3.50 - 3.00";
                    else if (score >= 2.50)     firstLetter ="3.00 - 2.50";
                    else if (score >= 2.00)     firstLetter ="2.50 - 2.00";
                    else if (score >= 1.50)     firstLetter ="2.00 - 1.50";
                    else if (score >= 1.00)     firstLetter ="1.50 - 1.00";
                    else if (score >= 0.50)     firstLetter ="1.00 - 0.50";
                    else                        firstLetter ="0.50 - 0.00";

                    //qDebug() << model->index(i,7).data().toString() << score << firstLetter ;

                    if (model->index(i,7).data().toString() == "") firstLetter = "*END*";


                }

            if (groupType=="LOC")
                {
                    QString loc = model->index(i,10).data().toString();
                    if (loc=="")
                        {
                            firstLetter = "** No location **";
                        }
                        else
                        {
                            QStringList locSplit = loc.split(",");
                            //qDebug() << locSplit;
                            if (locSplit.count()>0)
                                {
                                    /*if (locSplit.at(2)==" Ausztria")
                                        {
                                            firstLetter=locSplit.at();
                                        }
                                        else */ firstLetter=locSplit.at(0);
                                }
                                else firstLetter = "** Invalid location **";
                        }
                }

            //firstLetter = "A";
            //qDebug() << "FL:" << firstLetter;
            if (firstLetter!=prevFirstLetter)
                {
                    int counter=childCount;
                    childCount=0;

                    parent = parent->clone();
                    parent->setText( firstLetter );


                    for (int j=0;j<11;++j)
                        {
                            child = child->clone();
                            child->setText(   model->index(i,j).data().toString());
                            parent->setChild(childCount,j,child);
                        }

                    if (prevFirstLetter!="" /*|| i+1==model->rowCount() */)
                         {
                            prevParent->setText( "" +  prevParent->text() + " (" + QString::number(counter+1) + ")");
                            modelTree->appendRow(prevParent);

                            //qDebug() << ">>appendRow" << prevParent->text();
                            //archiveModelTree->app
                            //qDeleteAll(prevParent);
                            prevParent->removeRows(0,500);
                            prevParent->removeColumns(0,500);
                            parent->removeRows(0,500);
                            parent->removeColumns(0,500);

                            //childCount=-1;
                         }

                }
                else
                {

                   if (prevFirstLetter!="")
                        {
                            ++childCount;

                           for (int j=0;j<11;++j)
                               {
                                   child = child->clone();
                                   child->setText(   model->index(i,j).data().toString());
                                   parent->setChild(childCount,j,child);
                               }
                        }

                }

            prevParent = parent;
            prevFirstLetter=firstLetter;


        }

    }
    else
    {
        //qDebug() << "          TREE *NOT* GROUPED";
        parent->setText("ALL (" + QString::number( model->rowCount() ) + ")");

        for (int i=0;i<=model->rowCount();++i)
            {
                ++childCount;
                for (int j=0;j<11;++j)
                    {

                        child = child->clone();
                        child->setText(   model->index(i,j).data().toString());
                        parent->setChild(childCount,j,child);
                    }
            }

        modelTree->appendRow(parent);
    }

    //qDebug() << archiveModelTree->index(0,0).data();
    //qDebug() << archiveModelTree->index(1,0).data();

    qDebug() << "      <-- archiveTreeModelConvert()";
    /**************************** END OF MODEL TREE EXPERIMENT  ***************************************/
}

QString archiveManager::calculateFolderSize(QString f)
{
    int fileCount=0;
    qint64 fileSize=0;
    QDirIterator iterator(QDir(f).absolutePath(), QDirIterator::Subdirectories);
       while (iterator.hasNext())
       {
          iterator.next();
          if (!iterator.fileInfo().isDir())
          {
             QString filename = iterator.fileName();
             ++fileCount;
             fileSize+=iterator.fileInfo().size();
             //qDebug() << fileCount << fileSize;
          }
       }

    QString fileSizeStr =  tools::numGrouped(fileSize);

    return "<b>" + fileSizeStr + "</b> Bytes in <b>"+ QString::number( fileCount ) +"</b> files";
}

/*  ┌─┬───┬─┐
 *  │┌┴───┴┐│  DIALOGS
 *  │└┬───┬┘│
 *  └─┴───┴─┘
 */

void archiveManager::dialog_uploader()
{
    uploaderDialog *UP = new uploaderDialog();
    UP->title   = selected->title;
    UP->title2  = ui->label_archiveInfoTitle_2->text();
    UP->date    = selected->date;
    UP->folder  = selected->folder;
    UP->id      = selected->id;
    UP->keywords= ui->plainTextEdit_archiveKeywords->toPlainText().replace(" ",",");
    UP->exec();
}

void archiveManager::dialog_addSale()
{
    addSaleDialog *asD = new addSaleDialog();
    asD->attachLibMan(libMan);
    asD->archiveID    = selected->id;
    asD->archiveTitle = selected->title;
    if (asD->exec())
    {
        getSales();
    }
}

void archiveManager::dialog_FPV()
{
    followersAndPageviewsDialog *FPV = new followersAndPageviewsDialog();
    FPV->attachLibMan(libMan);
    FPV->exec();
}


void archiveManager::dialog_flattenFolder()
{
    archiveFolderFlattenDialog *aF = new archiveFolderFlattenDialog();
    aF->rootFolder = selected->folder;
    if (aF->exec())
    {
        this->fileNaviUpdate(selected->folder);
    }
    else
    {
        this->fileNaviUpdate(selected->folder);
    }
}

void archiveManager::dialog_uploadTimes()
{
    uploadTimesDialog *uT = new uploadTimesDialog();
    uT->attachLibman(libMan);
    uT->exec();
}

void archiveManager::dialog_syncSentinel()
{
    syncDialog *sY=new syncDialog();
    sY->attachLibMan(libMan);
    //sY->connectRemoteDB();

    sY->exec();
    modelSetQueryAndFetch(model->query().lastQuery() );
    refresh();
    scoreGet();
    sitesGet();

}

void archiveManager::dialog_collectionAdd()
{
    addCollectionDialog *acDialog=new addCollectionDialog();
    if (acDialog->exec())
        {
            QString query;
            if (acDialog->type == "smart")
                query="INSERT INTO collections (name,type,param) VALUES ('"+acDialog->name+"','smart','"+acDialog->param+"')";
            if (acDialog->type == "item")
                query="INSERt INTO collections (name,type) VALUES ('"+acDialog->name+"','item')";
            libMan->db.exec(query);
            collectionModel->setQuery("SELECT c.name,c.type,c.param,c.id,var.key FROM collections c LEFT JOIN var ON var.value = c.id AND var.key='targetCollection' ORDER BY name COLLATE NOCASE",libMan->db);
        }
}

void archiveManager::dialog_siteManager()
{
    tableBrowserDialog smDialog;
    smDialog.setDb(libMan->db);
    smDialog.setModule("sitesManage");
    smDialog.setParam(QString::number(selected->id));
    if (smDialog.exec())
        {

        }
}

void archiveManager::dialog_addSite()
{



    AddSiteDialog aSDialog;
    aSDialog.setId(selected->id);
    aSDialog.setTitle(selected->title);
    if (aSDialog.exec())
        {
            QString siteId ="";
            QString urlPart = aSDialog.theUrl.split("/").at(2);
            QString date ; date.sprintf("%d-%02d-%02d" ,  QDate::currentDate().year()  , QDate::currentDate().month() , QDate::currentDate().day());

            if (urlPart.contains("deviantart"))         siteId="DA";
            if (urlPart.contains("500px"))              siteId="5P";
            if (urlPart.contains("flickr"))             siteId="FL";
            if (urlPart.contains("google"))             siteId="G+";
            if (urlPart.contains("facebook"))           siteId="FB";
            if (urlPart.contains("pixoto"))             siteId="PX";
            if (urlPart.contains("youpic"))             siteId="YP";
            if (urlPart.contains("viewbug"))            siteId="VB";
            if (urlPart.contains("realitydream.hu"))    siteId="RD";
            if (urlPart.contains("instagram"))    siteId="IG";
            /*
            QString query = "INSERT or REPLACE INTO sites (id,projectId,site,url,title,date,time,views,favs,comments,updated) VALUES ( (SELECT id FROM sites WHERE projectId==" +
                            QString::number( selected->id ) + " AND url LIKE '%"+ urlPart +"%') , "+
                            QString::number( selected->id ) +
                            ",'"+siteId+"' " +
                            ",'"+aSDialog.theUrl+"','"+aSDialog.origTitle+"','"+
                            aSDialog.submitDate+"','"+aSDialog.submitTime+"',"+QString::number(aSDialog.statViews)+","+
                            QString::number(aSDialog.statFavs)+","+QString::number(aSDialog.statComments)+" ,'" +date+  "' )";
            */
            QString query = "INSERT INTO sites (projectId,site,url,title,date,time,views,favs,comments,updated) VALUES (  " +
                            QString::number( selected->id ) +
                            ",'"+siteId+"' " +
                            ",'"+aSDialog.theUrl+"','"+aSDialog.origTitle+"','"+
                            aSDialog.submitDate+"','"+aSDialog.submitTime+"',"+QString::number(aSDialog.statViews)+","+
                            QString::number(aSDialog.statFavs)+","+QString::number(aSDialog.statComments)+" ,'" +date+  "' )";

            qDebug() << query;
            libMan->db.exec(query);

            // a fenti siteid nem ugyanaz!!! mint alább!!! :P
            query = "SELECT * FROM sites WHERE url='"+aSDialog.theUrl+"'";
            QSqlQueryModel *tmpModel = new QSqlQueryModel(this);
            tmpModel->setQuery(query,libMan->db);
            siteId = tmpModel->record(0).value("id").toString();

            query = "INSERT  INTO siteupdatelog (siteid,views,favs,comments,update_date) VALUES ( "
                            + siteId + ","
                            +QString::number(aSDialog.statViews)+","
                            +QString::number(aSDialog.statFavs)+","
                            +QString::number(aSDialog.statComments)+",'"
                            +date+  "' )";
            qDebug() << query;
            libMan->db.exec(query);

            // UPDATE SCORE

            query = " DROP TABLE IF EXISTS tmp_minmax";
            qDebug() << query;
            libMan->db.exec(query);

            query =" CREATE TABLE tmp_minmax AS"
                   "     SELECT 1 id,MAX(views) maxviews,MAX(favs) maxfavs,MIN(views) minviews,MIN(favs) minfavs "
                   "     FROM"
                   "     ("
                   "         SELECT projectid,SUM(views) views,SUM(favs) favs  FROM sites  GROUP BY projectid"
                   "     ) t1";
            qDebug() << query;
            libMan->db.exec(query);

            query ="  DROP TABLE IF EXISTS tmp_siteminmax_year";
            qDebug() << query;
            libMan->db.exec(query);

            query = "  CREATE TABLE tmp_siteminmax_year AS"
                  "      SELECT site,substr(date,1,4) year,MIN(views) minviews,MAX(views) maxviews, MIN(favs) minfavs, MAX(favs) maxfavs "
                  "      FROM sites"
                  "      GROUP BY site, substr(date,1,4)";
            qDebug() << query;
            libMan->db.exec(query);

            query ="  DROP TABLE IF EXISTS score";
            qDebug() << query;
            libMan->db.exec(query);

            query ="  CREATE TABLE score AS"
                  "    SELECT sa6.projectid projectid, ROUND(sa6.score_yrst*5+sa6.scoreva+sa6.scorefa,3) score"
                  "    FROM"
                  "    ("
                  "      SELECT sa5.projectid,sa5.score_yrst,qv.sqrtsqrt scoreva,qf.sqrtsqrt scorefa"
                  "      FROM"
                  "      ("
                  "         SELECT sa4.projectid,sa4.score_yrst"
                  "               ,ROUND( ( (CAST(sa4.sumviews  AS REAL )-sa4.minViewsa) / (sa4.maxViewsa-sa4.minViewsa) ) ,2) scoreva"
                  "               ,ROUND( ( (CAST(sa4.sumfavs   AS REAL )-sa4.minfavsa ) / (sa4.maxfavsa -sa4.minfavsa ) ) ,2) scorefa"
                  "         FROM"
                  "         ("
                  "           SELECT sa3.projectid projectid,ROUND((AVG(sa3.scorev) + AVG(sa3.scoref) ) / 2 , 2) score_yrst,SUM(views) sumviews, SUM(favs) sumfavs"
                  "                 ,mm.minviews minviewsa,mm.minfavs minfavsa ,mm.maxviews maxviewsa ,mm.maxfavs maxfavsa"
                  "           FROM"
                  "           ("
                  "             SELECT sa2.id,sa2.projectid,sa2.site,sa2.views,sa2.favs,qv.sqrtsqrt as scorev,qf.sqrtsqrt as scoref"
                  "             FROM"
                  "             ("
                  "               SELECT sa1.id,sa1.projectid,sa1.site  ,sa1.views,sa1.favs     "
                  "                    , CASE WHEN sa1.notcount IS NULL THEN ROUND( ( (CAST(sa1.views  AS REAL )-sa1.minViews) / (sa1.maxViews-sa1.minViews) ) ,2) ELSE NULL END scorev"
                  "                    , CASE WHEN sa1.notcount IS NULL THEN ROUND( ( (CAST(sa1.favs   AS REAL )-sa1.minfavs ) / (sa1.maxfavs -sa1.minfavs ) ) ,2) ELSE NULL END scoref"
                  "               FROM"
                  "               ("
                  "           SELECT s.id,a.title,s.site,s.views,s.favs,s.projectid,s.notcount"
                  "                       ,smmy.minviews,smmy.minfavs,smmy.maxviews,smmy.maxfavs       "
                  "                 FROM archive a"
                  "                 LEFT JOIN sites s ON a.id=s.projectid"
                  "                 LEFT JOIN tmp_siteminmax_year smmy ON smmy.site = s.site AND substr(s.date,1,4) = smmy.year  "
                  "                 WHERE s.id IN (SELECT MAX(id) FROM sites WHERE sites.projectid = a.id GROUP BY sites.site)"
                  "               ) sa1"
                  "             ) sa2"
                  "             LEFT JOIN sqrt qv ON qv.base=sa2.scorev"
                  "             LEFT JOIN sqrt qf ON qf.base=sa2.scoref"
                  "           ) sa3"
                  "           LEFT JOIN tmp_minmax mm"
                  "           GROUP BY sa3.projectid"
                  "         ) sa4"
                  "       ) sa5"
                  "       LEFT JOIN sqrt qv ON qv.base=sa5.scoreva"
                  "       LEFT JOIN sqrt qf ON qf.base=sa5.scorefa"
                  "     ) sa6"
                  "  ORDER BY ROUND(sa6.score_yrst+sa6.scoreva+sa6.scorefa,3) DESC";
            qDebug() << query;
            libMan->db.exec(query);
            // end


            //archive->filter( ui->lineEdit_archiveFilter->text() );

            QList<QStandardItem*> fList = modelTree->findItems(selected->title ,Qt::MatchRecursive);

            if (fList.count()!=0)
                {
                    QModelIndex index= fList.first()->index();
                    ui->treeView_archiveTree->setCurrentIndex( index );
                    select(index);
                }

            modelSetQueryAndFetch(model->query().lastQuery() );
            refresh();
        }
}

void archiveManager::dialog_siteUpdater()
{
    siteUpdaterNew sU;
    sU.attachLibman(libMan);
    sU.exec();
    modelSetQueryAndFetch(model->query().lastQuery() );
    refresh();
    scoreGet();
    sitesGet();
}

void archiveManager::dialog_siteUpdater_single()
{
    siteUpdaterNew sU;
    sU.setSingleMode(selected->id,selected->title);
    sU.attachLibman(libMan);
    sU.exec();
    modelSetQueryAndFetch(model->query().lastQuery() );
    refresh();
    scoreGet();
    sitesGet();
}

void archiveManager::dialog_addNewProxy()
{
    dialog_addNew("");
}

void archiveManager::dialog_addNew(QString preDir)
{


    addArchiveDialog aaDialog;
    QSqlQueryModel *getTitles = new QSqlQueryModel(this);
    getTitles->setQuery("SELECT title FROM archive",libMan->db);
    while (getTitles->canFetchMore()) getTitles->fetchMore();
    QList<QString> titleList;
    for (int i=0; i<getTitles->rowCount();++i)
        {
            titleList << getTitles->index(i,0).data().toString();
        }

    aaDialog.setTitleList(titleList);

    if (preDir!="") aaDialog.folderSelect(preDir);

    if (aaDialog.exec())
    {
        QString srcPath = aaDialog.image;
        QString imgName = aaDialog.title+".jpg";
        QString dstPath = libMan->libraryFolder+"previews/"+imgName;

        QFile fileHandler(srcPath);
        fileHandler.copy(dstPath);

        QString query = "INSERT INTO archive (title,projectDir,image,date,keywords) VALUES ('"+aaDialog.title+"','"+aaDialog.folder+"','"+imgName+"','"+aaDialog.date+"','"+aaDialog.keywords+"') ";
        libMan->db.exec(query);
        /*  !!!!!!!!
        model->setQuery(setMan->archiveTreeQuery,libMan->db);
        while(model->canFetchMore()) model->fetchMore();
        */
        filter("");
        QString title = aaDialog.title;

        QList<QStandardItem*> fList = modelTree->findItems(title,Qt::MatchRecursive);

        if (fList.count()!=0)
            {
                QModelIndex index= fList.first()->index();
                ui->treeView_archiveTree->setCurrentIndex( index );
                select(index);
            }
    }
}

bool archiveManager::selectByTitle(QString title)
{
    QList<QStandardItem*> fList = modelTree->findItems(title,Qt::MatchRecursive);

    if (fList.count()!=0)
        {
            QModelIndex index= fList.first()->index();
            ui->treeView_archiveTree->setCurrentIndex( index );
            select(index);
            return true;
        }
    return false;
}

void archiveManager::selectFirst()
{
    ui->treeView_archiveTree->setCurrentIndex( modelTree->index(0,0).child(0,0) );
    select(ui->treeView_archiveTree->currentIndex());
}

void archiveManager::dialog_compressor()
{
    compressWizardDialog cpDialog;
    cpDialog.setFolder(selected->folder);
    cpDialog.init();
    cpDialog.exec();
    ui->label_fileSize->setText ( calculateFolderSize(selected->folder) );
}

void archiveManager::dialog_edit()
{
    addArchiveDialog aeDialog;

    aeDialog.setTitleList(getTitles());
    aeDialog.setTitle(  selected->title    );
    aeDialog.setFolder( selected->folder   );
    aeDialog.setImage(  selected->image    );
    aeDialog.setDate(   selected->date     );

    if (aeDialog.exec())
    {
        QString query = "UPDATE archive SET title = '"+aeDialog.title
                        +"'  ,projectDir = '"+aeDialog.folder
                        +"'       ,date  = '"+aeDialog.date
                        +"'    ,keywords = '"+aeDialog.keywords+"' WHERE id = "
                        + QString::number( selected->id );

        libMan->db.exec(query);

        // ???
        /*
        model->setQuery(setMan->archiveTreeQuery,libMan->db);
        while(model->canFetchMore()) model->fetchMore();
        this->archiveFilter("");
        this->archiveTreeSetIndex( findItem(aeDialog.title) );
        */
    }
}

void archiveManager::dialog_organizer()
{
    organizerDialog orgDialog;
    orgDialog.setFolder(selected->folder);
    orgDialog.init();
    if (orgDialog.exec())
        {
            this->fileNaviUpdate(selected->folder);
        }
    else
    {
        this->fileNaviUpdate(selected->folder);
    }
}

void archiveManager::dialog_browserAll()
{
    tableBrowserDialog notesDialog;
    notesDialog.setDb(libMan->db);
    notesDialog.setModule("allArchives");
    if (notesDialog.exec())
        {
            QString selectedTitle = notesDialog.selecedTitle;
            QModelIndex index=modelTree->findItems( selectedTitle ,Qt::MatchRecursive).first()->index();
            ui->treeView_archiveTree->setCurrentIndex( index );
            select(index);
        }
}

void archiveManager::dialog_scores()
{
    qDebug() << "IIIIIIIIIIIIIIIIIIIIIIIIIIIIII";
    tableBrowserDialog scDialog;
    scDialog.setDb(libMan->db);
    scDialog.setModule("scoresBrowser");
    if (scDialog.exec())
        {
            QString selectedTitle = scDialog.selecedTitle;
            QModelIndex index=modelTree->findItems( selectedTitle ,Qt::MatchRecursive).first()->index();
            ui->treeView_archiveTree->setCurrentIndex( index );
            select(index);
        }
}

void archiveManager::dialog_notesBrowser()
{
    tableBrowserDialog notesDialog;
    notesDialog.setDb(libMan->db);
    notesDialog.setModule("notesBrowser");
    if (notesDialog.exec())
        {
            QString selectedTitle = notesDialog.selecedTitle;
            QModelIndex index=modelTree->findItems( selectedTitle ,Qt::MatchRecursive).first()->index();
            ui->treeView_archiveTree->setCurrentIndex( index );
            select(index);
        }
}

void archiveManager::dialog_sitesBrowser()
{
    tableBrowserDialog sbDialog;
    sbDialog.setDb(libMan->db);
    sbDialog.setModule("sitesBrowser");
    if (sbDialog.exec())
        {
            QString selectedTitle = sbDialog.selecedTitle;
            QModelIndex index=modelTree->findItems( selectedTitle ,Qt::MatchRecursive).first()->index();
            ui->treeView_archiveTree->setCurrentIndex( index );
            select(index);
        }
}


void archiveManager::tableDblClick(QModelIndex index)
{
    selectByTitle(tools::dataFromIndex(index,0));
    ui->tabWidget_archiveCenter->setCurrentIndex(0);
}



