#ifndef UIMANAGER_H
#define UIMANAGER_H

#include <QMainWindow>
#include <QCoreApplication>

namespace Ui {
class MainWindow;
}

class uiManager : public QMainWindow
{
    Q_OBJECT
public:
    explicit uiManager(QWidget *parent = 0);
    Ui::MainWindow *ui;

signals:

public slots:

};

#endif // UIMANAGER_H
