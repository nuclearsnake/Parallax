#include "librarymanager.h"

#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <QString>
#include <QTextStream>


libraryManager::libraryManager(QObject *parent) :
    QObject(parent)
{
    //scoreBias = 2.05;
}

void libraryManager::archiveGroupTypeSave( QString value )
{
    // save arrchive tree grouping and sort type
    QString query = "insert or replace into var (id, key, value) values  ((select id from var where key = 'archiveGroupType'), 'archiveGroupType', '"+ value +"');";
    db.exec(query);
    qDebug() << "    save arrchive tree grouping" << value;
}

QString libraryManager::getCacheFolder()
{
    return libraryFolder+"cache/";
}

void libraryManager::archiveTreeGroupedStateSave(int state)
{
    // save arrchive tree grouping
    QString value;
    if ( state ) value="1"; else value ="0";
    QString query = "insert or replace into var (id, key, value) values  ((select id from var where key = 'archiveTreeGrouped'), 'archiveTreeGrouped', '"+ value +"');";
    db.exec(query);
    qDebug() << "    save arrchive tree grouping" << value;
}

void libraryManager::archiveTreeExpandStateSave(int state)
{
    // save arrchive tree expand state
    QString value;
    if (state) value="1"; else value ="0";
    QString query = "insert or replace into var (id, key, value) values  ((select id from var where key = 'archiveTreeExpanded'), 'archiveTreeExpanded', '"+ value +"');";
    db.exec(query);
    qDebug() << "    save archive tree expand state" << value;
}

void libraryManager::archiveSelectedTitleSave(QString value)
{
    if (value !="")
        {
            QString query = "insert or replace into var (id, key, value) values  ((select id from var where key = 'lastArchiveTitle'), 'lastArchiveTitle', '"+value+"');";
            db.exec(query);
            qDebug() << "    save archive current index (title)" << value;

        }
}

void libraryManager::processSelectedTitleSave(QString value)
{
    if (value !="")
        {
            QString query = "insert or replace into var (id, key, value) values  ((select id from var where key = 'lastProcessTitle'), 'lastProcessTitle', '"+value+"');";
            db.exec(query);
            qDebug() << "    save process current index (title)" << value;

        }
}

void libraryManager::currentModuleSave(QString value)
{
    if (value !="")
        {
            QString query = "insert or replace into var (id, key, value) values  ((select id from var where key = 'lastModule'), 'lastModule', '"+value+"');";
            db.exec(query);
            qDebug() << "    save current module (title)" << value;

        }
}

void libraryManager::archiveFileNaviSizeSave(QString value)
{
    if ( ( value !="" ) & ( value !="0" ) )
        {
            QString query = "insert or replace into var (id, key, value) values  ((select id from var where key = 'fileNaviSize'), 'fileNaviSize', '"+value+"');";
            db.exec(query);
            qDebug() << "    save current module (title)" << value;

        }
}

void libraryManager::archiveSidebarStatusSave(QString value)
{
    if (value !=""  )
        {
            QString query = "insert or replace into var (id, key, value) values  ((select id from var where key = 'archiveSidebarStatus'), 'archiveSidebarStatus', '"+value+"');";
            db.exec(query);
            qDebug() << "    save current sidebar status" << value;

        }
}

void libraryManager::archiveFileNaviVisibleSave(QString value)
{

            //QString value;
            //(visible) ? value="true" : value="false";
            QString query = "insert or replace into var (id, key, value) values  ((select id from var where key = 'fileNaviVisible'), 'fileNaviVisible', '"+value+"');";
            db.exec(query);
            qDebug() << "    save current module (title)" << value;


}

void libraryManager::archiveZoomlevelSave( int value)
{
    // save archive browser zoom level
    QString query = "insert or replace into var (id, key, value) values  ((select id from var where key = 'archiveZoomLevel'), 'archiveZoomLevel', '"+ QString::number( value ) +"');";
    db.exec(query); //exec(query);
    qDebug() << "    save archive browser zoom level" << value;
}

QString libraryManager::loadQuery(QString fileName)
{
    QFile file( ":/queries/" + fileName);
    file.open(QIODevice::ReadOnly);
    QString s;

    QTextStream in(&file);
    s.append(in.readAll());
    return s;

}

void libraryManager::open()
{
    // open file dialog
    QString fileName = QFileDialog::getOpenFileName(0, tr("Open File"),"",tr("Files (*.pldb)"));

    // return if cancel pressed
    if (fileName=="") return;

    // set library path and load the library
    libraryPath = fileName;
    this->load();
}

void libraryManager::createNew()
{
    // open directory dialog
    QString dirName = QFileDialog::getExistingDirectory(0,tr("Open Directory"));

    // return if cancel pressed
    if (dirName=="") return;

    // file name from path, filename = directory name + pldb
    QString fileName = dirName.split("/").last();
    QFile file(dirName+"/"+fileName+".pldb");

    // check file exists
    if (file.exists())
        {
            // popup file exists message box
            QMessageBox Msgbox;
            Msgbox.setText("Library already exists in this folder!");
            Msgbox.exec();
        }
        else
        {
            // create the database file
            file.open(QIODevice::ReadWrite);
            file.close();
            libraryPath = dirName+"/"+fileName+".pldb";
            db = QSqlDatabase::addDatabase("QSQLITE","conn2");
            db.setDatabaseName( libraryPath );
            db.open();
            // create projects table
            db.exec("CREATE TABLE projects ( id INTEGER PRIMARY KEY ,  projectname TEXT , path TEXT , added TEXT , isFinished NUMERIC , isPano NUMERIC , thumbnail TEXT , iconData BLOB );");
            // create archive table
            db.exec("CREATE TABLE archive ( id INTEGER PRIMARY KEY , title TEXT , projectDir TEXT , image TEXT , date TEXT , keywords  TEXT , notes TEXT );");
            // create sites table
            db.exec("CREATE TABLE sites ( id INTEGER PRIMARY KEY , projectId NUMERIC , title TEXT , url TEXT, date TEXT  comments NUMERIC , favs NUMERIC , views NUMERIC , comments NUMERIC , updated TEXT );");
            // create var table
            db.exec("CREATE TABLE var (id INTEGER PRIMARY KEY , key TEXT,value TEXT);");
            // insert id string to var table
            db.exec("INSERT INTO var (key,value) VALUES ('dbver','1.0')");
            db.close();
            QSqlDatabase::removeDatabase("conn2");

            // create cache and backup folders
            QDir dir;
            dir.mkpath(dirName+"/"+"cache");
            dir.mkpath(dirName+"/"+"backups");
            dir.mkpath(dirName+"/"+"previews");

            // load the library
            this->load();
        }



}

void libraryManager::loadFrom(QString path)
{
    libraryPath = path;
    this->load();
}

void libraryManager::load()
{
    // check file exists
    if (!QFile::exists(libraryPath))
        {
            QMessageBox Msgbox;
            Msgbox.setText("File: " +  libraryPath + " not found!");
            Msgbox.exec();
            return;
        }

    // connect to database and read from var table, the dbver key value must be 1.0
    db = QSqlDatabase::addDatabase("QSQLITE","conn2");
    db.setDatabaseName( libraryPath );
    db.open();
    QSqlQuery qry = db.exec("SELECT value FROM var WHERE key='dbver'");
    qry.first();
    if ( qry.value(0).toString() != "1.0" )
        {
            QMessageBox Msgbox;
            Msgbox.setText("Cannot read library version! Maybe wrong file...");
            Msgbox.exec();
            qry.finish();
            qry.clear();
            db.close();
            db.removeDatabase("conn2");
            QSqlDatabase::removeDatabase("conn2");
            return;
        }

    QStringList pathEx = libraryPath.split("/");
    pathEx.removeLast();
    libraryFolder = pathEx.join("/") + "/";

    //qry.finish();
    //qry.clear();
    //db.close();
    //db.removeDatabase("conn2");
    //QSqlDatabase::removeDatabase("conn2");

    // emit signal for mainwindow
    emit libraryLoaded();
}
