#include "clickable_qwidget.h"

#include <QStyle>
#include <QStyleOption>
#include <QPainter>

clickable_QWidget::clickable_QWidget(QWidget *parent) :
    QWidget(parent)
{
}

void clickable_QWidget::mousePressEvent(QMouseEvent *ev)
{
    Q_UNUSED(ev);
    emit Mouse_Pressed();
}

void clickable_QWidget::paintEvent (QPaintEvent *)
{
// ! important for style sheet handling
    QStyleOption opt;
    opt.init (this);
    QPainter p (this);
    style()->drawPrimitive (QStyle::PE_Widget, &opt, &p, this);
}
