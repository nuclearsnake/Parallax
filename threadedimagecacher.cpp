#include "threadedimagecacher.h"

#include <QDebug>
#include <QImage>
#include <QImageReader>
#include <QImageWriter>

threadedImageCacher::threadedImageCacher(QObject *parent) :
    QThread(parent)
{
}

void threadedImageCacher::run()
{
    qDebug() << "run cacher";
    qDebug() << filesTobeCached;

    for (int i=0 ; i<filesTobeCached.count();++i )
        {
            QString src = filesTobeCached.at(i);
            QString fname = src.split("/").last();
            QString dst = cacheFolder+fname+".jpg";
            qDebug() << src;
            qDebug() << "   " << dst;

            QImageReader pixR;
            QImageWriter pixW;
            QImage pix;

            pixR.setFileName(src);
            pixR.setScaledSize(QSize(200,200/(pixR.size().width()*1.0)*(pixR.size().height()*1.0)));
            pixR.read(&pix);

            pixW.setFileName(dst);
            pixW.setFormat("jpg");
            pixW.setCompression(95);
            if (pix.width()) pixW.write(pix);

            emit itemLoaded();
        }

}

