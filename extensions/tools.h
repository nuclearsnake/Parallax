#ifndef TOOLS_H
#define TOOLS_H
#include <QString>
#include <QDebug>
#include <QAction>
#include <QSqlDatabase>
#include <QListView>
#include <QListWidget>
#include <QSqlQueryModel>

class tools
{
public:
    tools();
    static QString getPiece(QString content, QString beginStr, QString endStr);
    void getStats(QString url, QString pageContent, int projectId = 0);
    int statViews,statFavs,statComments;
    QString submitDate,submitTime,origTitle,theUrl,option1;
    QStringList months;
    QStringList months1;
    QStringList months2;
    static QString numGrouped(qint64 numInt);
    static QString numNA(QString num);
    static int subDirCount(QString folder);
    static QString fileIndexToPath(QModelIndex index);
    static QAction *menuAddCheckable(QString title, bool checked);
    static QString queryFirst(QSqlDatabase db, QString query);
    static QString dataFromIndex(QModelIndex index, int col);
    static void openLocalFile(QString url);
    static void listViewIconMode(QListView *widget);
    static void openFolderInExplorer(QString folder);
    static QString modelSearch(QSqlQueryModel *model, int colsSrc, int colTarg, QString srcString);
    static void copyToClipboard(QString s);
    static QString kiloMegaGiga(qint64 numInt);
    static QString padSpaceBefore(QString str, int width);
    static QString extractNumber(QString str);
    static QString toRoman(int num);
    QString getPieceBack(QString content, QString beginStr, QString endStr);
private slots:

};

#endif // TOOLS_H
