#ifndef OVERLAY_H
#define OVERLAY_H
#include <QWidget>
#include <QTimer>

class Overlay : public QWidget
{
public:
    Overlay(QWidget *parent);
    QString msg;
    bool msgVisible;
    QTimer *timer;

    void showMessage(QString m);
public slots:
    void hideMessage();
protected:
    void paintEvent(QPaintEvent *event);
private slots:

};

#endif // OVERLAY_H
