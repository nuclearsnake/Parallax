#include "threadedimageloader.h"
#include <QtCore>
#include <QDebug>
#include <QDir>
#include <QImage>
#include <QPixmap>
#include <QImageReader>
#include <QImageWriter>
#include <QModelIndex>
#include <QSqlQuery>
//#include <libqpsd-master/qpsdhandler.h>
//#include <libqpsd-master/qpsdplugin.h>

threadedImageLoader::threadedImageLoader(QObject *parent) :
    QThread(parent)
{

}

void threadedImageLoader::run()
{

    // get files from mem db
    model = new QSqlQueryModel();
    model->setQuery("Select id,name FROM files",dbmem);

    // set cahce folder
    //QString cacheFolder="z:/!cache/";
    qDebug() << cacheFolder;

    // loop through files
    for (int i=0;i<model->rowCount();i++)
    {
        QByteArray blob;
        QByteArray blob64;
        QString fileName=model->index(i,1).data().toString();
        //qDebug() << "imgldr file:" + fileName;
        if (fileName.contains(".tif") || fileName.contains(".psd") || fileName.contains(".jpg"))
        {
            //qDebug() << i << model->index(i,0).data() << model->index(i,1).data();
            QImageReader pixR;
            QImageWriter pixW;
            QImage pix;

            QString id = model->index(i,0).data().toString();

            QString fullPath=path+fileName;

            QFileInfo inFileInfo;
            inFileInfo.setFile(fullPath);
            uint timestamp = inFileInfo.lastModified().toTime_t();
            QString thumbFileName = fileName+"."+QString::number(timestamp)+".jpg";
            //qDebug() << "thumbname >>>>>>>>>>>>>>>>>>" << thumbFileName;

            QFile thumbFile;
            thumbFile.setFileName(cacheFolder+thumbFileName);
            //qDebug() << "thumbname >>>>>>>>>>>>>>>>>>" << thumbFile.fileName();

            if ( !thumbFile.exists() || thumbFile.size()==0 )
            {
                pixR.setFileName(fullPath);
                pixR.setScaledSize(QSize(200,200/(pixR.size().width()*1.0)*(pixR.size().height()*1.0)));
                pixR.read(&pix);

                pixW.setFileName(cacheFolder+thumbFileName);
                pixW.setFormat("jpg");
                pixW.setCompression(95);
                if (pix.width()) pixW.write(pix);
            }
            else
            {
                pixR.setFileName(thumbFile.fileName());
                pixR.read(&pix);
            }


            QBuffer buffer(&blob);
            pix.save(&buffer,"JPG");

            blob64 = blob.toBase64();
            //qDebug() << "      path" << fullPath;
            //qDebug() << "    blob64" << blob64.size();

            QSqlQuery query(dbmem);
            //query.prepare("UPDATE files SET icondata = (?) WHERE id="+id);
            query.prepare("UPDATE files SET icondata = (?) , thumb = 'thumb.jpg' WHERE id="+id);
            query.addBindValue(blob64);
            query.exec();


        }

        emit loaded(i+1,model->rowCount(),blob64);

    }



}
