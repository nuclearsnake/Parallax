#include "overlay.h"
#include <QPainter>
#include <QPen>

#include <QDebug>

Overlay::Overlay(QWidget *parent)
    : QWidget(parent)
{
    setPalette(Qt::transparent);
    setAttribute(Qt::WA_TransparentForMouseEvents);
    timer = new QTimer(this);
    msgVisible = false;

    //connect ( timer , SIGNAL(timeout()) , this  , SLOT(hideMessage()) );
}

void Overlay::hideMessage()
{
    this->msg = "";
    qDebug() << "HIDE";
    //timer->stop();
    this->msgVisible = false;
    this->repaint();
}

void Overlay::showMessage(QString m)
{
    qDebug() << "SHOW";
    this->msg = "    "+m+"    ";
    this->msgVisible = true;

    //timer->setInterval(1000);
    //timer->start();

    this->repaint();
    //QTimer::singleShot(2000,this, SLOT(hideMessage()));

}

void Overlay::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);

    //msg="This is a message...";
    if (msgVisible)
        {
            int msgLen = msg.count();
            int boxWidth = msgLen*10 + 40;

            QFont font;
            font.setBold(true);
            font.setPixelSize(20);
            painter.setFont(font);
            painter.setPen(QColor(222,222,222,128));
            painter.setBrush(QColor(0,0,0,176));
            painter.drawRoundedRect(QRect((width()-boxWidth)/2,height()-205,boxWidth,34),4,4);
            painter.setPen(QColor("#EEE"));
            painter.drawText(QRect(0,height()-200,width(),24),Qt::AlignCenter,msg);
        }

    //painter.setPen(QPen(Qt::red));
    //painter.drawLine(width()/8, height()/8, 7*width()/8, 7*height()/8);
    //painter.drawLine(width()/8, 7*height()/8, 7*width()/8, height()/8);
}
