#include "clickable_qlabel.h"

clickable_QLabel::clickable_QLabel(QWidget *parent) :
    QLabel(parent)
{
}

void clickable_QLabel::mousePressEvent(QMouseEvent *ev)
{
    Q_UNUSED(ev);
    emit Mouse_Pressed();
}
