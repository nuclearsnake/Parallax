#include "tools.h"

#include <QDate>
#include <QDir>
#include <QModelIndex>
#include <QSqlQuery>
#include <QDesktopServices>
#include <QUrl>
#include <QProcess>
#include <QClipboard>
#include <QApplication>

QString tools::padSpaceBefore(QString str, int width)
{
    //qDebug() << "akdsalksdj" << width-str.length();
    //return str.rightJustified(width-str.length(),'X');
    QString s="";
    for (int i = 0; i<width-str.length();i++)
    {
        s+=" ";
    }
    return s+str;
}


tools::tools()
{
    months << "" << "January" << "February" << "March" << "April" << "May" << "June" << "July" << "August" << "September" << "October" << "November" << "December";
    months1 << "" << " jan" << " febr" << " márc" << " ápr" << " máj" << " jún" << " júl" << " aug" << " szept" << " okt" << " nov" << " dec";
    months2 << "" << "Jan" << "Feb" << "Mar" << "Apr" << "May" << "Jun" << "Jul" << "Aug" << "Sep" << "Oct" << "Nov" << "Dec";
}

QString tools::extractNumber(QString str)
{
    int n1 ;//= test1.toInt();
    QString n1s="";
    for (int i=0;i<str.length();++i)
    {
        QString x = str.mid(i,1);
        if (x=="0" || x=="1" || x=="2" || x=="3" || x=="4" || x=="5" || x=="6" || x=="7" || x=="8" || x=="9" )
            n1s.append(x);
    }
    //log (n1s+" = " + QString::number(n1s.toInt()) + " = " + toRoman(n1s.toInt()));
    //str.replace(n1s,toRoman(n1s.toInt()));
    //str.replace("pt." , "");
    //str.append('.');
    return n1s;
}

QString tools::toRoman(int num)
{
    QString roman="";
    QString is = QString::number(num);
    int i = num;
    //log(is);

    if (i<10)
    {
        QString n = is.mid(0,1);
        //log(is+" 2. "+n);
        if (n=="1") roman+="I";
        if (n=="2") roman+="II";
        if (n=="3") roman+="III";
        if (n=="4") roman+="IV";
        if (n=="5") roman+="V";
        if (n=="6") roman+="VI";
        if (n=="7") roman+="VII";
        if (n=="8") roman+="VIII";
        if (n=="9") roman+="IX";
    }

    if (i>9 && i<100)
    {
        QString n = is.mid(0,1);
        //log(is+" 2. "+n);
        if (n=="1") roman+="X";
        if (n=="2") roman+="XX";
        if (n=="3") roman+="XXX";
        if (n=="4") roman+="XL";
        if (n=="5") roman+="L";
        if (n=="6") roman+="LX";
        if (n=="7") roman+="LXX";
        if (n=="8") roman+="LXXX";
        if (n=="9") roman+="XC";

        n = is.mid(1,1);
        //log(is+" 2. "+n);
        if (n=="1") roman+="I";
        if (n=="2") roman+="II";
        if (n=="3") roman+="III";
        if (n=="4") roman+="IV";
        if (n=="5") roman+="V";
        if (n=="6") roman+="VI";
        if (n=="7") roman+="VII";
        if (n=="8") roman+="VIII";
        if (n=="9") roman+="IX";
    }

    if (i>99)
    {
        QString n = is.mid(0,1);
        //log(is+" 1. "+n);
        if (n=="1") roman+="C";
        if (n=="2") roman+="CC";
        if (n=="3") roman+="CCC";
        if (n=="4") roman+="CD";
        if (n=="5") roman+="D";

        n = is.mid(1,1);
       // log(is+" 2. "+n);
        if (n=="1") roman+="X";
        if (n=="2") roman+="XX";
        if (n=="3") roman+="XXX";
        if (n=="4") roman+="XL";
        if (n=="5") roman+="L";
        if (n=="6") roman+="LX";
        if (n=="7") roman+="LXX";
        if (n=="8") roman+="LXXX";
        if (n=="9") roman+="XC";

        n = is.mid(2,1);
        //log(is+" 2. "+n);
        if (n=="1") roman+="I";
        if (n=="2") roman+="II";
        if (n=="3") roman+="III";
        if (n=="4") roman+="IV";
        if (n=="5") roman+="V";
        if (n=="6") roman+="VI";
        if (n=="7") roman+="VII";
        if (n=="8") roman+="VIII";
        if (n=="9") roman+="IX";

    }

    return roman;
}

void tools::copyToClipboard(QString s)
{
    QClipboard *clipboard = QApplication::clipboard() ;
    clipboard->setText(s);
}

QString tools::modelSearch(QSqlQueryModel *model,int colsSrc, int colTarg, QString srcString)
{
    QString result;
    for (int i=0;i < model->rowCount();i++)
        {
            if (model->index(i,colsSrc).data().toString()==srcString)
                {
                    result = model->index(i,colTarg).data().toString();
                }
        }
    return result;
}

void tools::openFolderInExplorer(QString folder)
{
    QString param = QLatin1String(" explorer.exe /root,");
    param += '"' + QDir::toNativeSeparators(folder) + '"';
    qDebug() << param;
    QProcess::startDetached( param   );
}

void tools::listViewIconMode(QListView* widget)
{
///
///  set the widget mode to iconmode, and set up scrollbars, icon sizes and spacing
///
    qDebug() << "        --> listViewIconMode()";
    qDebug() << "           " << widget;
        widget->setViewMode(QListWidget::IconMode);
        widget->setResizeMode(QListWidget::Adjust );
        widget->setDragEnabled(false);
        widget->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        widget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        widget->setSpacing(0);
        widget->setBaseSize(QSize(160,160));
    qDebug() << "        <-- listViewIconMode()";
}

void tools::openLocalFile(QString url)
{
    QDesktopServices::openUrl(QUrl::fromLocalFile(url));
}

QString tools::dataFromIndex(QModelIndex index, int col)
{
    return index.sibling(index.row(),col).data().toString();
}

QAction* tools::menuAddCheckable(QString title , bool checked)
{
    QAction *tAction = new QAction(title,0) ;
    tAction->setCheckable(true);
    tAction->setChecked(checked);
    return tAction;
}

QString tools::queryFirst(QSqlDatabase db, QString query)
{
    QSqlQuery qry =  db.exec( query );
    qry.first();
    return qry.value(0).toString();
}

int tools::subDirCount(QString folder)
{
    QDir *dir = new QDir();
    dir->setPath(folder);
    dir->setFilter(QDir::NoDotAndDotDot | QDir::Dirs );
    return dir->entryList().count();
}

QString tools::fileIndexToPath(QModelIndex index)
{
    QModelIndex p = index;
    QString path = "";
    while (p.isValid())
        {
            path = p.data().toString() + "/" + path;
            p = p.parent();
        }
    return path;
}

QString tools::numNA(QString num)
{
    //if (num=="0" || num=="1") return "N/A"; else return num;
    return num;
}

QString tools::numGrouped(qint64 numInt )
{
    //int numInt = numStr.toInt();
    QString numStr = QString::number(numInt);
    if (numInt>1000)          numStr.insert(numStr.size()-3," ");
    if (numInt>1000000)       numStr.insert(numStr.size()-7," ");
    if (numInt>1000000000)    numStr.insert(numStr.size()-11," ");
    return numStr;
}

QString tools::kiloMegaGiga(qint64 numInt)
{
    if (numInt<1024)
        {
            return numGrouped(numInt) + " Bytes";
        }
    else
        {
            numInt = numInt / 1024;
            if (numInt<1024)
                {
                    return numGrouped(numInt) + " KB";
                }
            else
                {
                    numInt = numInt / 1024;
                    if (numInt<1024)
                        {
                            return numGrouped(numInt) + " MB";
                        }
                    else
                        {
                            numInt = numInt / 1024;
                            return numGrouped(numInt) + " GB";

                        }
                }
        }

}

QString tools::getPiece(QString content, QString beginStr,QString endStr)
{

    QString piece = content.mid(content.indexOf(beginStr)+beginStr.length(),50);
    piece = piece.mid(0,piece.indexOf(endStr));
    piece = piece.replace(",","");
    return piece;

}


QString tools::getPieceBack(QString content, QString beginStr,QString endStr)
{
    QString piece = content.mid(content.indexOf(endStr)-150,150);
    qDebug() << "B!!!!!!!" << piece;
    piece = piece.mid(piece.indexOf(beginStr)+beginStr.length(),50);
    qDebug() << "B!!!!!!!" << piece;
    return piece;

}

void tools::getStats(QString url, QString pageContent , int projectId  )
{

    qDebug() << "         << tools::getStats()";

    if (url.contains("deviantart"))
    {
        statViews       = getPiece(pageContent,"<dt>Views</dt><dd>"," ").toInt();
        if (statViews==0) statViews       = getPiece(pageContent,"<dt>Views</dt><dd>","<").toInt();
        statFavs        =getPiece(pageContent,"<dt>Favourites</dt><dd>"," ").toInt();
        statComments    =getPiece(pageContent,"<dt>Comments</dt><dd>","<").toInt();
        QString tempA      =getPiece(pageContent,"offset=\"-1\">","<");
        if (tempA.contains("ago"))
        {
            QString tempA = getPiece(pageContent,"Submitted on</dt><dd><span title=\"","\"");
            QStringList tempB = tempA.split(" ");
            if (tempB.count()>1)
            {
                int curYear = QDate::currentDate().year();
                //qDebug() << curYear;
                submitDate.sprintf("%d-%02d-%02d",curYear, months.indexOf(tempB.at(0)) , tempB.at(1).toInt() );
            }
        }
        else
        {
            QStringList tempB = tempA.split(" ");
            //qDebug() << tempB;
            //qDebug() << tempB.at(0);
            if (tempB.count()>1)
            {
                int year;
                if (tempB.count()==3) year = tempB.at(2).toInt(); else year = 2014;
                submitDate.sprintf("%d-%02d-%02d",year, months.indexOf(tempB.at(0)) , tempB.at(1).toInt() );
            }
        }

        QString tss = getPiece(pageContent,"\" ts=\"","\"");
        //tss="1444556451";
        qDebug() << "timeStamp=" << tss;
        QString ofss= getPiece(pageContent,"\" offset=\"","\"");
        qDebug() << "offset=" << ofss;

        QDateTime uploadDateTime;
        uploadDateTime.setTime_t(tss.toUInt());
        //uploadDateTime = uploadDateTime.addSecs(ofss.toInt()*60*60);

        QString uTime;
        uTime.sprintf( "%02d:%02d:%02d" , uploadDateTime.time().hour()  , uploadDateTime.time().minute() , uploadDateTime.time().second()  );
        qDebug() << "upload time=" << uTime;
        submitTime = uTime;

        submitDate = "";
        submitDate.sprintf("%d-%02d-%02d",uploadDateTime.date().year(),uploadDateTime.date().month(),uploadDateTime.date().day());

        //submitDate.sprintf("%d",tempB.at(2).toInt());
        origTitle       =getPiece(pageContent,"<meta name=\"og:title\" content=\"","\"");
    }

    if (url.contains("500px"))
    {
        qDebug() << "             \\-> 500px detected ";

        qDebug() << "                    \\-> get Views";
        statViews       = getPiece(pageContent,"times_viewed\":"    ,",").toInt();
        qDebug() << "                          " << statViews;

        qDebug() << "                    \\-> get Favs";
        statFavs        = getPiece(pageContent,"votes_count\":"     ,",").toInt();
        qDebug() << "                          " << statFavs;

        qDebug() << "                    \\-> get Comments";
        statComments    = getPiece(pageContent,"comments_count\":"  ,",").toInt();
        qDebug() << "                          " << statComments;

        qDebug() << "                    \\-> get Date";
        submitDate      = getPiece(pageContent,"created_at\":\""    ,"T");
        qDebug() << "                          " << submitDate;

        qDebug() << "                    \\-> get Time";
        QString tempA   = getPiece(pageContent,"created_at\":\""    ,",");
        qDebug() << "                          tempA" << tempA;
        QString tempB   = getPiece(tempA,"T","-");
        qDebug() << "                          tempB" << tempB;
        QStringList tempC = tempB.split(":");
        if (tempC.count()>2)
            {
                qDebug() << "                          tempC" << tempC;
                int hour =  tempC.at(0).toInt() + 6 ;
                if (hour == 24) hour = 0;
                qDebug() << "                           hour" << hour;
                submitTime.sprintf("%02d:%02d:%02d" , hour, tempC.at(1).toInt() ,tempC.at(2).toInt() );
                qDebug() << "                          " << submitTime;
            }
            else qDebug() << "                          ERROR";

        qDebug() << "                    \\-> get Title";
        origTitle       = getPiece(pageContent,"name\":\""          ,"\"");
        qDebug() << "                          " << origTitle;

        qDebug() << "                    \\-> get highest rating";
        option1       = getPiece(pageContent,"highest_rating\":"          ,",");
        qDebug() << "                          " << option1;
    }

    if (url.contains("pixoto"))
    {
        statViews       = getPiece(pageContent,"Unique Views</span><span class=\"attr-value\">" ,"</").toInt();
        statFavs        = getPiece(pageContent,"img-score IMG_SCORE\">"              ,"</").toInt();
        statComments    = 0;
        QString tempA   = getPiece(pageContent,"itemprop=\"datePublished\">"                    ,"</");
        origTitle       = getPiece(pageContent,"<title>"                                        ," |");
        tempA.replace(",","");
        QStringList tempB = tempA.split(" ");
        //qDebug() << tempA << tempB;
        submitDate.sprintf("%d-%02d-%02d",tempB.at(2).toInt(), months2.indexOf(tempB.at(0)) , tempB.at(1).toInt() );
    }

    if (url.contains("viewbug"))
    {
        QString statViewsS       = getPieceBack(pageContent,"<div class=\"info-number\">" , "<div class=\"info-title\">views");
        qDebug() << "SV1 " << statViewsS;
        statViewsS = statViewsS.mid(0,statViewsS.indexOf("<"));
        qDebug() << "SV1 " << statViewsS;
        statViews = statViewsS.toInt();
        statFavs        = getPiece(pageContent,"class=\"vblikes-button button gray \"><span class=\"mask-ani\"><span class=\"icon\"></span><span class=\"mask-text\">","</").toInt();
        statComments    = 0;
        QString tempA   = getPiece(pageContent,"file: '/media/mediafiles/"  ,",");
        origTitle       = getPiece(pageContent,"<title>"                    ," -");
        //tempA.replace(",","");
        QStringList tempB = tempA.split("/");
        //qDebug() << tempA << tempB;
        submitDate.sprintf("%d-%02d-%02d",tempB.at(0).toInt(), tempB.at(1).toInt() , tempB.at(2).toInt() );
    }

    if (url.contains("youpic"))
    {
        statViews       = getPiece(pageContent,"\"views\":"         ,",").toInt();
        statFavs        = getPiece(pageContent,"\"favs\":"          ,",").toInt();
        statComments    = 0;
        submitDate      = getPiece(pageContent,"\",\"created\":\""  ,"T");
        origTitle       = getPiece(pageContent,"\"image_name\":\""  ,"\"");
        submitTime      = getPiece(pageContent,submitDate+"T",".");
        qDebug() << "captured date " << submitDate;
        qDebug() << "captured time " << submitTime;

        QDateTime timeStamp;
        //timeStamp.setUtcOffset(2*60*60);
        timeStamp = timeStamp.fromString(submitDate+"T"+submitTime,Qt::ISODate);
        timeStamp = timeStamp.addSecs(1*60*60);
        //timeStamp = timeStamp.toLocalTime();
        submitDate ="";
        submitDate.sprintf("%04d-%02d-%02d",timeStamp.date().year(),timeStamp.date().month(),timeStamp.date().day() );
        submitTime = "";
        submitTime.sprintf("%02d:%02d:%02d",timeStamp.time().hour(),timeStamp.time().minute(),timeStamp.time().second());

        qDebug() << "corrected date " << submitDate;
        qDebug() << "corrected time " << submitTime;


    }

    if (url.contains("flickr"))
    {
        qDebug() << "FLICKR detected!";
        statViews       = getPiece(pageContent,"view-count-label\">"    ,"</").toInt();
        qDebug() << "views=" << statViews;
        statFavs        = getPiece(pageContent,"fave-count-label\">"     ,"</").toInt();
        qDebug() << "statFavs=" << statFavs;
        statComments    = getPiece(pageContent,"comment-count-label\">"  ,"</").toInt();
        qDebug() << "statComments=" << statComments;
        //submitDate      = getPiece(pageContent,"date_taken\":\""    ," ");  //title="Uploaded
        QString tempA   = getPiece(pageContent,"title=\"Uploaded "    ,".");

            //qDebug() << tempA;
            tempA = tempA.replace(",","");
            //qDebug() << tempA;
            QStringList tempB = tempA.split(" "); // September 3, 2014
            //qDebug() << tempB;

            submitDate.sprintf("%d-%02d-%02d",tempB.at(2).toInt(), months.indexOf(tempB.at(0)) , tempB.at(1).toInt() );

            QString tss = getPiece(pageContent,"\"datePosted\":\"","\"");
            //tss="1444556451";
            qDebug() << "timeStamp=" << tss;
            //QString ofss= getPiece(pageContent,"\" offset=\"","\"");
            //qDebug() << "offset=" << ofss;

            QDateTime uploadDateTime;
            uploadDateTime.setTime_t(tss.toUInt());
            //uploadDateTime = uploadDateTime.addSecs(ofss.toInt()*60*60);

            QString uTime;
            uTime.sprintf( "%02d:%02d:%02d" , uploadDateTime.time().hour()  , uploadDateTime.time().minute() , uploadDateTime.time().second()  );
            qDebug() << "upload time=" << uTime;
            submitTime = uTime;

            submitDate = "";
            submitDate.sprintf("%d-%02d-%02d",uploadDateTime.date().year(),uploadDateTime.date().month(),uploadDateTime.date().day());

        origTitle       = getPiece(pageContent,"title\":\""          ,"\"");
    }

    if (url.contains("facebook"))
    {
        //statViews       = getPiece(pageContent,"times_viewed\":"    ,",").toInt();
        statViews = 0;
        statFavs        = getPiece(pageContent,"likecount\":"     ,",").toInt();
        statComments    = getPiece(pageContent,"commentcount\":"  ,",").toInt();
        //submitDate      = getPiece(pageContent,"created_at\":\""    ,"T");
        submitDate =    "";
        origTitle       = getPiece(pageContent,"<meta name=\"description\" content=\""          ,"\"");

        QString tss = getPiece(pageContent,"content_timestamp&quot;:&quot;","&quot;");
        //tss="1444556451";
        qDebug() << "timeStamp=" << tss;
        //QString ofss= getPiece(pageContent,"\" offset=\"","\"");
        //qDebug() << "offset=" << ofss;

        QDateTime uploadDateTime;
        uploadDateTime.setTime_t(tss.toUInt());
        //uploadDateTime = uploadDateTime.addSecs(ofss.toInt()*60*60);

        QString uTime;
        uTime.sprintf( "%02d:%02d:%02d" , uploadDateTime.time().hour()  , uploadDateTime.time().minute() , uploadDateTime.time().second()  );
        qDebug() << "upload time=" << uTime;
        submitTime = uTime;

        submitDate = "";
        submitDate.sprintf("%d-%02d-%02d",uploadDateTime.date().year(),uploadDateTime.date().month(),uploadDateTime.date().day());
    }

    if (url.contains("instagram"))
    {
        //statViews       = getPiece(pageContent,"times_viewed\":"    ,",").toInt();
        statViews = 0;
        statFavs        = getPiece(pageContent,"likes\":{\"count\":"     ,",").toInt();
        statComments    = 0;
        //submitDate      = getPiece(pageContent,"created_at\":\""    ,"T");
        submitDate =    "";
        origTitle       = ""; // getPiece(pageContent,"<meta name=\"description\" content=\""          ,"\"");

        QString tss = getPiece(pageContent,",\"date\":",".");
        //tss="1444556451";
        qDebug() << "timeStamp=" << tss;
        //QString ofss= getPiece(pageContent,"\" offset=\"","\"");
        //qDebug() << "offset=" << ofss;

        QDateTime uploadDateTime;
        uploadDateTime.setTime_t(tss.toUInt());
        //uploadDateTime = uploadDateTime.addSecs(ofss.toInt()*60*60);

        QString uTime;
        uTime.sprintf( "%02d:%02d:%02d" , uploadDateTime.time().hour()  , uploadDateTime.time().minute() , uploadDateTime.time().second()  );
        qDebug() << "upload time=" << uTime;
        submitTime = uTime;

        submitDate = "";
        submitDate.sprintf("%d-%02d-%02d",uploadDateTime.date().year(),uploadDateTime.date().month(),uploadDateTime.date().day());
    }

    if (url.contains("realitydream.hu"))
    {
        //statViews       = getPiece(pageContent,"times_viewed\":"    ,",").toInt();
        statViews       = getPiece(pageContent,"PID" + QString::number(projectId) + "VC:'"    , "';").toInt();;
        statFavs        = 0;
        statComments    = 0;
        submitDate    = getPiece(pageContent,"PID" + QString::number(projectId) + "DT:'"    ,"';");
        //QString date ; date.sprintf("%d-%02d-%02d" ,  QDate::currentDate().year()  , QDate::currentDate().month() , QDate::currentDate().day());
        //submitDate      = date;
        origTitle       = "";
    }

    if (url.contains("plus.google"))
    {
        //statViews       = getPiece(pageContent,"times_viewed\":"    ,",").toInt();
        //qDebug() <<  "   G+ Views";
        statViews       = getPiece(pageContent,"Megtekintések száma: "    ,"<").toInt();  // Megtekintések száma:
        //qDebug() <<  "   G+ Favs";
        //statFavs        = getPiece(pageContent,"</td></tr></tbody></table></div></div></div></div></div><div class=\"ttc fXb\" style=""><div class=\"WGb\"><div><div tabindex=\"0\" role=\"button\" class=\"esw eswd Ae qk Gc\" title=\"A +1 elemre kattintva jelezheted, hogy tetszik ez a fotó\" aria-pressed=\"false\"><span dir=\"ltr\" class=\"tf yda\"><span class=\"gr\"></span><span class=\"H3\" jsname=\"NnAfwf\">"     ,"<").toInt();
        statFavs        = getPiece(pageContent,"aria-pressed=\"false\"><span dir=\"ltr\" class=\"tf yda\"><span class=\"gr\"></span><span class=\"H3\" jsname=\"NnAfwf\">"     ,"<").toInt();
        //qDebug() <<  "   G+ Comments";
        statComments    = getPiece(pageContent,"cr Rs\">"  ,"<").toInt();
        //qDebug() <<  "   G+ Date";
        QString tempA      = getPiece(pageContent,"fsfO0e\">"    ,"<");
        submitDate ="";
        QStringList tempB = tempA.split("."); // 2014. szept.1.
            //qDebug() << tempB;
            if (tempB.count()>2)
                submitDate.sprintf("%d-%02d-%02d",tempB.at(0).toInt(), months1.indexOf(tempB.at(1)) , tempB.at(2).toInt() );

        //submitDate =    "";
        //qDebug() <<  "   G+ Title";
        origTitle       = getPiece(pageContent,"class=\"Lkb\">"          ,"<");
    }

}

