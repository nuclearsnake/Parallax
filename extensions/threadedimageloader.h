#ifndef THREADEDIMAGELOADER_H
#define THREADEDIMAGELOADER_H

#include <QThread>
#include <QDir>
#include <QList>
#include <QPixmap>
#include <QListWidgetItem>
#include <QSqlDatabase>
#include <QSqlQueryModel>


class threadedImageLoader : public QThread
{
    Q_OBJECT
public:
    explicit threadedImageLoader(QObject *parent = 0);
    void run();
    bool Stop;
    QString path;
    QString file;
    QList<QListWidgetItem*> wItemList;
    QListWidgetItem *item;
    QDir *dir;
    QSqlDatabase dbmem;
    QSqlQueryModel* model;
    QString cacheFolder;

signals:
    void numberChanged(int);
    //void loaded(QString filename,QPixmap pix);
    //void loaded(QListWidgetItem* item , QString fName ,QImage pix,int curItem,int allItems);
    void loaded(int cur,int all, QByteArray blob64);

public slots:

};

#endif // THREADEDIMAGELOADER_H
