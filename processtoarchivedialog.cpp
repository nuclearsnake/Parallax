#include "processtoarchivedialog.h"
#include "ui_processtoarchivedialog.h"

#include <QDebug>
#include <QDir>
#include <QDirIterator>
#include <QElapsedTimer>

#include "extensions/tools.h"

processToArchiveDialog::processToArchiveDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::processToArchiveDialog)
{
    ui->setupUi(this);

    ui->stackedWidget->setCurrentIndex(0);
    connect ( ui->lineEdit_title    , SIGNAL(textChanged(QString))  , this  , SLOT(titleToFolderName(QString)) );
    connect ( ui->pushButton_next   , SIGNAL(clicked())             , this  , SLOT(next()) );
}

void processToArchiveDialog::next()
{

    ui->stackedWidget->setCurrentIndex(1);
    ui->label_s2_from->setText(sourceFolder);
    ui->label_s2_to->setText(targetFolder);
    QDir dir;
    //qDebug() <<  dir.rename(sourceFolder,targetFolder);
    QElapsedTimer tmr;
    tmr.start();
    qDebug() <<  copyRecursively(sourceFolder,targetFolder);
    qDebug() << "ELAPSED:" << tmr.elapsed();
    accept();
}

bool processToArchiveDialog::copyRecursively(const QString &from,const QString &to)
{
    QStringList dirList;

    QDirIterator iterator(QDir( from ).absolutePath(), QDirIterator::Subdirectories);
       while (iterator.hasNext())
       {
          iterator.next();
          if ( /*iterator.fileName()!="." & iterator.fileName()!=".." */ !iterator.fileInfo().isDir()  )
            {
                qDebug() << "  " << iterator.fileName();
                QString srcPath = iterator.filePath();
                QString targetPath = srcPath;
                targetPath = targetPath.replace(from,to);
                qDebug() << srcPath << targetPath;
                moveList.append(qMakePair( srcPath , targetPath ));

            }
            else if( ( iterator.fileName()!="." ) & ( iterator.fileName()!=".." ) )
            {
                QString srcPath = iterator.filePath();
                QString targetPath = srcPath;
                targetPath = targetPath.replace(from,to);
                dirList << targetPath;
            }

       }

    int progressAll = dirList.length()+moveList.length();

    qDebug() << dirList;
    QDir dir;
    dir.mkpath(to);

    for (int i=0;i<dirList.length();++i)
       {
            qDebug() << i << dirList.at(i) << dir.mkpath(dirList.at(i));
            ui->progressBar->setValue(100*i/progressAll);
            QCoreApplication::processEvents();
       }

    qDebug() << moveList;
    for (int i=0;i<moveList.length();++i)
        {

            QCoreApplication::processEvents();
            //qDebug() << (i+1) << "/" << moveList.length() << QFile::rename(moveList.at(i).first,moveList.at(i).second);
            QFile *fileFrom = new QFile(this); fileFrom->setFileName(moveList.at(i).first);
            QFile *fileTo   = new QFile(this);   fileTo->setFileName(moveList.at(i).second);

            ui->label_filename->setText(moveList.at(i).first.split("/").last());
            QCoreApplication::processEvents();


            fileFrom->open(QIODevice::ReadOnly);
            fileTo->open(QIODevice::WriteOnly);

            qint64 fSize = fileFrom->size();
            ui->label_filesize->setText( tools::numGrouped(fSize) + " bytes" );
            QCoreApplication::processEvents();

            int lastPercent=0;
            int percent = 0;

            qint64 chunkSize = 1024 * 512 ;

            QElapsedTimer speedTimer;
            speedTimer.start();

            for (qint64 n=0; n<fSize; n+=chunkSize)
                {
                    lastPercent = percent;
                    percent = 100*n/fSize;
                    if (lastPercent != percent) copyProgress( n , fSize , speedTimer.elapsed() );



                    fileFrom->seek(n);
                    char data[chunkSize] ;
                    qint64 bytesRead = fileFrom->read( data,chunkSize);

                    fileTo->seek(n);
                    fileTo->write(data,bytesRead);

                }

            fileFrom->close();
            fileTo->close();


            //fileFrom->copy(fileTo->fileName());

            qDebug() << fileFrom->fileName() << "Done";
            ui->progressBar->setValue(100*(i+dirList.length()+1)/progressAll);
            QCoreApplication::processEvents();
        }

    return true;
}

void processToArchiveDialog::copyProgress(quint64 done, quint64 all , qint64 tmr)
{
    QCoreApplication::processEvents();
    //qDebug() << p;
    ui->progressBar_file->setValue(100*done/all);
    ui->label_written->setText(tools::numGrouped(done)+" bytes");
    ui->label_speed->setText( tools::numGrouped( done / tmr ) + " kbytes/sec" );
    QCoreApplication::processEvents();
}


void processToArchiveDialog::titleToFolderName(QString s)
{
    bool folderOk = false;
    bool titleOk = false;

    QString s1 = s;
    s = s.toUpper();

    ui->lineEdit_target2->setText(s1.replace(" ","_")+"/");

    QString topDir = s1.at(0).toUpper();
    if (topDir.toInt()!=0) topDir = "#";
    if (topDir == "0") topDir = "#";

    ui->lineEdit_target1->setText(defaultTargetBaseDir+"/"+topDir+"/");


    QFontMetrics metrics(QApplication::font());
    ui->lineEdit_target1->setFixedSize(metrics.width(ui->lineEdit_target1->text())+10,20);

    qDebug() << s << s1;
    if (s.length()>0)
        {
            if (titleList.contains(s))
                {
                    ui->label_titleChk->setText("<b><span style=\"color:red\">Title already used</span></b>");
                    titleOk = false;
                }
                else
                {
                    ui->label_titleChk->setText("<b><span style=\"color:green\">Title available</span></b>");
                    titleOk = true;
                }

            targetFolder = defaultTargetBaseDir+"/"+topDir+"/"+s1+"/";
            qDebug() << targetFolder;
            qDebug() << QDir(targetFolder).exists();

            if (QDir(targetFolder).exists())
                {
                    ui->label_folderChk->setText("<b><span style=\"color:red\">Folder already exists</span></b>");
                    folderOk = false;
                }
                else
                {
                    ui->label_folderChk->setText("<b><span style=\"color:green\">Folder available</span></b>");
                    folderOk = true;
                }
        }
        else
        {
            titleOk = false;
            folderOk = false;
            ui->label_folderChk->setText("Enter title...");
            ui->label_titleChk->setText("Enter title...");
        }

    ui->pushButton_next->setEnabled(folderOk & titleOk);

}

void processToArchiveDialog::setDefaultBase(QString path)
{
    defaultTargetBaseDir = path;
    ui->lineEdit_target1->setText(path);
    QFontMetrics metrics(QApplication::font());
    ui->lineEdit_target1->setFixedSize(metrics.width(ui->lineEdit_target1->text())+10,20);

}

void processToArchiveDialog::setSource(QString s)
{
    sourceFolder = s.replace("\\","/");
    ui->lineEdit_source->setText(s);
}

void processToArchiveDialog::setTitleList(QStringList t)
{
    titleList = t;
    qDebug() << t;
}

processToArchiveDialog::~processToArchiveDialog()
{
    delete ui;
}
