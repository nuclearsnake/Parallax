#include "siteupdaternew.h"
#include "ui_siteupdaternew.h"
#include "extensions/tools.h"

#include <QNetworkDiskCache>
#include <QStandardPaths>
#include <QTimer>
#include <QWebFrame>
#include <QWebHistory>

siteUpdaterNew::siteUpdaterNew(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::siteUpdaterNew)
{
    ui->setupUi(this);

    Qt::WindowFlags flags = Qt::Window | Qt::WindowSystemMenuHint
                                | Qt::WindowMaximizeButtonHint
                                | Qt::WindowCloseButtonHint;
    this->setWindowFlags(flags);
    this->setWindowTitle("Site Updater | Parallax");

    ui->radioButton_single->setEnabled(false);
    ui->plainTextEdit_2->setVisible(false);
    ui->plainTextEdit->setVisible(false);
    ui->webView->setVisible(false);
    ui->pushButton_go->setEnabled(false);


    model = new QSqlQueryModel(this);

    tableModel = new QStandardItemModel(this);
    ui->tableView->setModel(tableModel);

    setTableColumnWidths();


    gPlusLoggedin = false;
    disableLoad = false;
    counter = 0;


    // network cache
    /*
    QNetworkDiskCache *cache = new QNetworkDiskCache(this);
    cache->setCacheDirectory(QStandardPaths::standardLocations( QStandardPaths::DataLocation ).at(0) + "/cache");
    cache->setMaximumCacheSize(1073741824);

    */
    // web views
    //ui->webView->page()->networkAccessManager()->setCache(cache);

    //ui->webView->settings()->setAttribute(ui->webView->settings()->AutoLoadImages,false);


    QWebSettings::globalSettings()->setMaximumPagesInCache(1);
    QWebSettings::globalSettings()->setObjectCacheCapacities(0, 0, 0);
    QWebSettings::globalSettings()->setOfflineStorageDefaultQuota(0);
    QWebSettings::globalSettings()->setOfflineWebApplicationCacheQuota(0);
    QWebSettings::globalSettings()->clearIconDatabase();
    QWebSettings::globalSettings()->clearMemoryCaches();


    connect( ui->pushButton_load    , SIGNAL(clicked())     , this  , SLOT(loadSites()) );
    connect( ui->pushButton_go      , SIGNAL(clicked())     , this  , SLOT(go())   );
    connect( ui->pushButton_close   , SIGNAL(clicked())     , this  , SLOT(accept())    );

    connect( ui->webView->page()      , SIGNAL(loadFinished(bool))    , this  , SLOT(test1(bool)) );
    connect( ui->webView->page()      , SIGNAL(loadProgress(int))     , this  , SLOT(loadDetect(int))  );

}

void siteUpdaterNew::attachLibman(libraryManager *l)
{
    libMan = l;
}



void siteUpdaterNew::setSingleMode(int id,QString title)
{
    Q_UNUSED(title);
    ui->radioButton_single->setEnabled(true);
    ui->radioButton_single->setChecked(true);
    ui->radioButton_site->setEnabled(false);
    ui->radioButton_updated->setEnabled(false);
    ui->radioButton_uploaded->setEnabled(false);
    singleId = id;
    //loadSites();
}

void siteUpdaterNew::loadSites()
{
    model->clear();
    tableModel->clear();
    QString query;

    if (ui->radioButton_uploaded->isChecked())
        {
            QString frame = "";

            if (ui->comboBox_uploaded->currentText()== "2 Days")   frame="3";
            if (ui->comboBox_uploaded->currentText()== "1 Week")   frame="8";
            if (ui->comboBox_uploaded->currentText()== "2 Weeks")  frame="14";
            if (ui->comboBox_uploaded->currentText()== "1 Month")  frame="32";
            if (ui->comboBox_uploaded->currentText()== "2 Months") frame="63";
            query = "SELECT p.id id,p.title, s.url,s.title 'original title',s.views,s.favs,s.comments ,s.updated,s.site,s.id siteid FROM archive p LEFT JOIN sites s ON s.projectid=p.id WHERE s.url NOT NULL    AND (julianday(Date('now')) - julianday(s.date)) <  "+frame;
        }

    if (ui->radioButton_updated->isChecked())
        {
            QString frame = "";

            if (ui->comboBox_updated->currentText()== "1 Week")   frame="8";
            if (ui->comboBox_updated->currentText()== "1 Month")  frame="32";
            if (ui->comboBox_updated->currentText()== "2 Months") frame="62";
            if (ui->comboBox_updated->currentText()== "3 Months") frame="92";
            if (ui->comboBox_updated->currentText()== "1 Year")   frame="365";
            query = "SELECT p.id id,p.title, s.url,s.title 'original title',s.views,s.favs,s.comments ,s.updated,s.site,s.id siteid FROM archive p LEFT JOIN sites s ON s.projectid=p.id WHERE s.url NOT NULL  AND (julianday(Date('now')) - julianday(s.updated)) >  "+frame;
        }

    if (ui->radioButton_single->isChecked())
        {
            query = "SELECT p.id id,p.title, s.url,s.title 'original title',s.views,s.favs,s.comments ,s.updated,s.site,s.id siteid FROM archive p LEFT JOIN sites s ON s.projectid=p.id WHERE s.url NOT NULL AND s.site !='RD' AND s.site != 'VB' AND p.id="+QString::number(singleId);
        }

    if (ui->radioButton_site->isChecked())
        {
            QString site;
            if (ui->comboBox_site->currentText()=="500px")      site="5P";
            if (ui->comboBox_site->currentText()=="DeviantArt") site="DA";
            if (ui->comboBox_site->currentText()=="Facebook")   site="FB";
            if (ui->comboBox_site->currentText()=="Google+")    site="G+";
            if (ui->comboBox_site->currentText()=="Pixoto")     site="PX";
            if (ui->comboBox_site->currentText()=="Flickr")     site="FL";
            if (ui->comboBox_site->currentText()=="ViewBug")    site="VB";
            if (ui->comboBox_site->currentText()=="Youpic")     site="YP";
            query = "SELECT p.id id,p.title, s.url,s.title 'original title',s.views,s.favs,s.comments ,s.updated,s.site,s.id siteid FROM archive p LEFT JOIN sites s ON s.projectid=p.id WHERE s.url NOT NULL AND s.site='"+site+"'  ORDER BY s.updated ";
        }

    qDebug() << query;
    model->setQuery(query,libMan->db);
    while(model->canFetchMore()) model->fetchMore();
    modelConvert();
    setTableColumnWidths();

    ui->pushButton_go->setEnabled(true);

    currentItem = 0;
    allItemCount = tableModel->rowCount();
}

void siteUpdaterNew::go()
{
    ui->pushButton_load->setEnabled(false);
    ui->pushButton_go->setEnabled(false);
    ui->comboBox_site->setEnabled(false);
    ui->comboBox_updated->setEnabled(false);
    ui->comboBox_uploaded->setEnabled(false);
    ui->radioButton_single->setEnabled(false);
    ui->radioButton_site->setEnabled(false);
    ui->radioButton_updated->setEnabled(false);
    ui->radioButton_uploaded->setEnabled(false);

    loadUrl();
    tmr = new QTime;
    tmr->start();
}

void siteUpdaterNew::loadUrl()
{
    qDebug() << "LASTDATE>>>>>>>>>>>>>>" << model->record(currentItem).value("updated").toString();
    QString date ; date.sprintf("%d-%02d-%02d" ,  QDate::currentDate().year()  , QDate::currentDate().month() , QDate::currentDate().day());
    qDebug() << "TODAY>>>>>>>>>>>>>>>>>" << date;


    if ( /* model->record(currentItem).value("updated").toString() == date || */
            model->record(currentItem).value("site").toString()=="G+"
         || model->record(currentItem).value("site").toString()=="PX"
         )
    {
        modifyTableCellWbg(currentItem,1,"Skipped",Qt::cyan);
        if ( currentItem < allItemCount-1 )
            {
                currentItem++;
                disableLoad = false;
                loadUrl();
            }

    }
    else
    {

    QString url = tableModel->index(currentItem,3).data().toString();

    if (url.contains("plus.google") & url.contains("album"))
        url = "https://plus.google.com/photos/?"+url.split("?").at(1);

    currentUrl = url;
    ui->tableView->setCurrentIndex( tableModel->index(currentItem,0) );
    modifyTableCellWbg(currentItem,1,"Loading",Qt::yellow);
    finishedCount=0;

    ui->label_progress->setText( QString::number(currentItem+1) + "/" + QString::number(allItemCount) );
    ui->progressBar->setValue( currentItem * 100 / allItemCount  );
     this->setWindowTitle( QString::number(currentItem+1) + " / " + QString::number(allItemCount) + " | Site Updater | Parallax");
    if (currentItem > 0)
        {
            qDebug() << "  elapsed time" << tmr->elapsed();
            qDebug() << "   time / item" << tmr->elapsed() / (currentItem + 0.00001) ;
            qDebug() << "     remaining" << (tmr->elapsed() / (currentItem + 0.00001))*(allItemCount-currentItem+1) ;
            QTime t(0,0,0);
            qDebug() << t;
            t = t.addMSecs((tmr->elapsed() / (currentItem + 0.00001))*(allItemCount-currentItem+1));
            qDebug() << t;
            QString rem;
            rem.sprintf("%02d:%02d:%02d" ,t.hour(),t.minute(),t.second() );
            ui->label_remTime->setText("Remaining time: "+rem );

        }

    ui->webView->setUrl(QUrl(url));
    }
}

void siteUpdaterNew::test1(bool a)
{
    Q_UNUSED(a);
}

void siteUpdaterNew::loadDetect(int a)
{

    if (a==100)
        {
            if (!disableLoad)
                {
                    disableLoad = true;
                    int sleep = 3000;

                    if (currentUrl.contains("plus.google")) sleep = 10000;
                    qDebug() << "       sleeping " << sleep << " msecs";

                    QTimer::singleShot(sleep,this,SLOT(fetchData()));
                }
        }
}


void siteUpdaterNew::fetchData()
{
    qDebug() << "<< fetchData()";
    qDebug() << "    \\-> URL:" << currentUrl;

    //bool todayUpdated=false; //UNUSED

    /*
    qDebug() << "LASTDATE>>>>>>>>>>>>>>" << model->record(currentItem).value("updated").toString();
    QString date ; date.sprintf("%d-%02d-%02d" ,  QDate::currentDate().year()  , QDate::currentDate().month() , QDate::currentDate().day());
    qDebug() << "TODAY>>>>>>>>>>>>>>>>>" << date;
    */
    QWebSettings::globalSettings()->clearMemoryCaches();
    ui->webView->history()->clear();

    /*
    if (model->record(currentItem).value("updated").toString() == date )
    {
         modifyTableCellWbg(currentItem,1,"Skipped",Qt::cyan);
    }
    else
    {
    */
    if (currentUrl.contains("plus.google") & !gPlusLoggedin )
        {
            modifyTableCellWbg(currentItem,1,"Logging in",Qt::yellow);

            ui->webView->page()->mainFrame()->evaluateJavaScript("document.getElementById('Email').value='hungarianskies';");
            ui->webView->page()->mainFrame()->evaluateJavaScript("document.getElementById('Passwd').value='Hanem793';");
            ui->webView->page()->mainFrame()->evaluateJavaScript("document.getElementById('signIn').click();");
            gPlusLoggedin = true;

            disableLoad = false;
            //loadUrl();
            return;
        }

    modifyTableCellWbg(currentItem,1,"Fetching",Qt::yellow);

    qDebug() << "    \\-> init Pg";
    tools* pG = new tools;
    qDebug() << "    \\-> getStats";
    pG->getStats(ui->webView->url().toString(),  ui->webView->page()->mainFrame()->toHtml()  );

    ui->plainTextEdit_2->setPlainText(ui->webView->page()->mainFrame()->toHtml());

    bool viewsValid=false;
    bool favsValid=false;
    bool commValid=false;

    enum Qt::GlobalColor color;

    qDebug() << "    \\-> check stats validity";

    if (pG->statViews >= model->record(currentItem).value("views").toInt() - 2 )
        {
            viewsValid =true;
            color = Qt::black;
        }
        else
        {
            color = Qt::red;
        }
    modifyTableCellWfg(currentItem,5, pG->statViews     ,color);


    if (pG->statFavs >= model->record(currentItem).value("favs").toInt() - 33 )
        {
            favsValid =true;
            color = Qt::black;
        }
        else
        {
            color = Qt::red;
        }
    modifyTableCellWfg(currentItem,7, pG->statFavs      ,color);


    if (pG->statComments >= model->record(currentItem).value("comments").toInt() - 5 )
        {
            commValid =true;
            color = Qt::black;
        }
        else
        {
            color = Qt::red;
        }
    modifyTableCellWfg(currentItem,9, pG->statComments  ,color);

    if (favsValid & viewsValid & commValid)
        {
            modifyTableCellWbg(currentItem,1,"Done",Qt::green);

            // save
            QString date ; date.sprintf("%d-%02d-%02d" ,  QDate::currentDate().year()  , QDate::currentDate().month() , QDate::currentDate().day());

            QString updateQuery = "UPDATE sites SET "
                    " views = "    + QString::number( pG->statViews )   + ","
                    " favs = "     + QString::number(pG->statFavs)      + ","
                    " comments = " + QString::number(pG->statComments)  + ","
                    " time = '"     + pG->submitTime  + "',"
                    " option1 = '"  + pG->option1  + "',"
                    " updated = '"    + date + "'"
                    " WHERE id = "
                    + model->record(currentItem).value("siteid").toString()  ;

            QString logQuery = "INSERT OR REPLACE INTO siteUpdateLog (id,siteid,views,favs,comments,update_date) VALUES ( ( SELECT id FROM siteUpdateLog WHERE siteId = "
                    + model->record(currentItem).value("siteid").toString()
                    + " AND update_date = '" + date + "'),"
                    + model->record(currentItem).value("siteid").toString()  + ","
                    +  QString::number( pG->statViews ) +","
                    + QString::number(pG->statFavs) +","
                    + QString::number(pG->statComments) +",'"
                    + date
                    + "' )  "
                     ;

            qDebug() << updateQuery;

            libMan->db.exec(logQuery);
            libMan->db.exec(updateQuery);

        }
        else
        {
            modifyTableCellWbg(currentItem,1,"Error",Qt::red);
        }
    //}

    if ( currentItem < allItemCount-1 )
        {
            currentItem++;
            disableLoad = false;
            loadUrl();
        }

}

void siteUpdaterNew::modifyTableCellWbg(int row, int col, QString text, enum Qt::GlobalColor bgcolor)
{
    QStandardItem *cell = new QStandardItem(text);
    cell->setData(QBrush(bgcolor), Qt::BackgroundRole);
    tableModel->setItem(row,col,cell);
}


void siteUpdaterNew::modifyTableCellWfg(int row, int col, QVariant text, enum Qt::GlobalColor fgcolor)
{
    QStandardItem *cell = new QStandardItem(text.toString());
    cell->setData(QBrush(fgcolor), Qt::ForegroundRole);
    tableModel->setItem(row,col,cell);
}

void siteUpdaterNew::modelConvert()
{
    for (int i = 0; i< model->rowCount();++i)
        {
            QSqlRecord record =  model->record(i);
            QList<QStandardItem*> row;

            //0
            row << new QStandardItem( QString::number( i+1 ) );

            //1 status
            row << new QStandardItem("");

            //2 title
            row << new QStandardItem( record.field(1).value().toString() );

            //3 url
            row << new QStandardItem( record.field(2).value().toString() );

            //4 V
            row << new QStandardItem( record.field(4).value().toString() );
            //5
            row << new QStandardItem( "" );

            //6 F
            row << new QStandardItem( record.field(5).value().toString() );
            //7
            row << new QStandardItem( "" );

            //8 C
            row << new QStandardItem( record.field(6).value().toString() );
            //9
            row << new QStandardItem( "" );
            //10
            row << new QStandardItem( record.field(7).value().toString() );

            tableModel->appendRow(row);
        }

}

siteUpdaterNew::~siteUpdaterNew()
{
    delete ui;
}

void siteUpdaterNew::setTableColumnWidths()
{
    QStringList labels;
    //        0      1           2          3        4          5          6          7          8          9           10
    labels << "#" << "status" << "title" << "url" << "V old" << "V new" << "F old" << "F new" << "C old" << "C new"  << "Last Update";
    tableModel->setHorizontalHeaderLabels(labels);

    ui->tableView->setColumnWidth(0,30);
    ui->tableView->setColumnWidth(1,60);
    ui->tableView->setColumnWidth(2,140);
    ui->tableView->setColumnWidth(3,510);
    ui->tableView->setColumnWidth(4,60);
    ui->tableView->setColumnWidth(5,60);
    ui->tableView->setColumnWidth(6,60);
    ui->tableView->setColumnWidth(7,60);
    ui->tableView->setColumnWidth(8,60);
    ui->tableView->setColumnWidth(9,60);
}
