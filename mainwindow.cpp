#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "dialogs/settingsdialog.h"
#include "dialogs/libraryprefsdialog.h"
#include "dialogs/droppedselectdialog.h"
#include "extensions/tools.h"

#include <QShortcut>
#include <QDesktopServices>
#include <QMessageBox>
#include <QCompleter>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent) ,
    ui(new Ui::MainWindow)
{

    // DISABLE QDEBUG OUTPUT !!!
    // qInstallMessageHandler(myMessageOutput);


    qDebug() << "<<- MainWindow()";

    ui->setupUi(this);



    this->setAcceptDrops(true);


    //tools *t = new tools();

    //this->loadStyleSheet(":/stylesheets/stylesheets/pscs6.css");

    this->initOverlay();
    this->initSplash();

    this->uiInits();

    //this->processUiInit();
    //this->archiveUiInit();

    this->defaultValues();

    this->initManagers();

    //mod = new Modules(ui,libMan);

    this->uiRestore();
    this->setWindowTitleAndIcon();

    //this->initWatchDogs();
    //this->initDBMem();

    this->connectionsForLibrary();
    this->connectionsForMainMenu();
    //this->connectionsForProcessor();
    //this->connectionsForArchive();
    archive->initConnections();
    process->initConnections();
    this->connectionsCommon();

    this->keyboardInitShortcuts();

    this->libraryAutoLoad();

    this->loadHistory();

    archive->init_1();
    archive->createFilterMenu();

    this->closeSplash();


    ui->actionBrowse->setVisible(false);

    //QPushButton *searchButton = new QPushButton;
    //searchButton->setIcon( QIcon(":/icons/icons/Search.png"));
    //searchButton->setStyleSheet("margin-right:20px;height:16px;padding:4px;width:20px;border-top-right-radius:8px;border-bottom-right-radius:8px;");
    QLabel *searchIcon = new QLabel;
    searchIcon->setPixmap(QPixmap(":/icons/icons/Search.png"));

    QLineEdit *searchInput = new QLineEdit;

    searchInput->setMaximumWidth(300);
    searchInput->setStyleSheet("image: url(:/icons/icons/Search.png);image-position:right center;height:14px;border-radius:8px;font-size:12px;padding:4px;font-weight:bold;margin-right:20px;");
    //ui->toolBarTabSwitcher->addWidget(searchIcon);
    ui->toolBarTabSwitcher->addWidget(searchInput);

    searchCompleter = new QCompleter(this);
    // proxymodel???
    //QSortFilterProxyModel *searchModel = new QSortFilterProxyModel(this) ;
    //searchModel->setDynamicSortFilter(true);
    //searchModel->setFilterCaseSensitivity(Qt::CaseInsensitive);

    //searchModel->setSortRole();
    archive->searchModel = new QSqlQueryModel(this);
    archive->searchModel->setQuery("SELECT title FROM archive ORDER BY title",libMan->db);

    //searchModel->setSourceModel( archive->model );


    searchCompleter->setModel(archive->searchModel);

    //searchCompleter->setModelSorting(QCompleter::CaseInsensitivelySortedModel);
    searchCompleter->setMaxVisibleItems(40);

    searchCompleter->setCaseSensitivity(Qt::CaseInsensitive);
    searchCompleter->setCompletionMode( QCompleter::PopupCompletion );

    //searchCompleter->setModelSorting(   QCompleter::CaseInsensitivelySortedModel );
    // unfiletered??
    searchInput->setCompleter(searchCompleter);


    connect( searchInput    , SIGNAL(textChanged(QString))  ,   this    , SLOT(search(QString))    );

    qDebug() << "->> MainWindow()";
    qDebug() << "";

    //mod->init();
    //mod->archive->initA();
    //archive->testUi();


}



//-------------------------------------------- @CONNECTIONS --------------------------------

void MainWindow::search(QString str)
{


    if (str!="")
        {
            QString title = tools::queryFirst(libMan->db,"SELECT title FROM archive WHERE title LIKE '%"+str+"%'");
            if (title!="")
                {
                    qDebug() << title;
                    QStringList completionList;
                    completionList << "asdawe" << "aweqokfasof" << "wieaksdj" << "askdjqiwej";

                    archive->selectByTitle(title);
                    archive->focusLists();
                }
        }

}

/*
void MainWindow::Modules::Archive::initA()
{
    qDebug() << "mod->Archive->initA()";
    //ui->pushButton_archiveGroupByAz->setVisible(false);

}

void MainWindow::Modules::init()
{
    qDebug() << "mod->init()";


}
*/

void MainWindow::connectionsCommon()
{
    // TABS (COMMON)
    connect(ui->actionProcess   , SIGNAL(triggered())  , this  , SLOT(tabSelector())   );
    connect(ui->actionBrowse    , SIGNAL(triggered())  , this  , SLOT(tabSelector())   );
    connect(ui->actionArchive   , SIGNAL(triggered())  , this  , SLOT(tabSelector())   );

    // OVERLAY
    connect(archive             , SIGNAL(overlayMsg(QString))  , this  , SLOT(showOverlay(QString)) );

    // ARCHIVE
    connect(archive     , SIGNAL(openWithPS(QStringList))           , this  , SLOT(openWithPS(QStringList)) );

    // PROCESS
    connect(process     , SIGNAL(openWithPS(QStringList))           , this  , SLOT(openWithPS(QStringList)) );
    connect(process     , SIGNAL(openWithPhotoMatix(QStringList))   , this  , SLOT(openWithPhotoMatix(QStringList)) );
    connect(process     , SIGNAL(openWithPTGui(QStringList))        , this  , SLOT(openWithPTGui(QStringList))   );
}

void MainWindow::connectionsForLibrary()
{
    connect( ui->pushButton_cleanStart_createLib    , SIGNAL(clicked())         , libMan    , SLOT(createNew()) );
    connect( ui->pushButton_cleanStart_loadLib      , SIGNAL(clicked())         , libMan    , SLOT(open())      );
    connect( ui->pushButton_loadFromList            , SIGNAL(clicked())         , this      , SLOT(loadFromRecent()) );
    connect( libMan                                 , SIGNAL(libraryLoaded())   , this      , SLOT(libraryLoaded())  );
}

void MainWindow::connectionsForMainMenu()
{
    // main menu connections (COMMON)
    connect( ui->actionOpen                     , SIGNAL(triggered())   , libMan    , SLOT(open())          );
    connect( ui->actionNew                      , SIGNAL(triggered())   , libMan    , SLOT(createNew())     );
    connect( ui->actionClose                    , SIGNAL(triggered())   , this      , SLOT(libraryClose())  );
    connect( ui->actionAdd_newProcess           , SIGNAL(triggered())   , this      , SLOT(addNewProcess()) );
    connect( ui->actionAdd_new_Process_toolBar  , SIGNAL(triggered())   , this      , SLOT(addNewProcess()) );
    connect( ui->actionAdd_newArchive           , SIGNAL(triggered())   , this      , SLOT(addNewArchiveProxy())    );
    connect( ui->actionAdd_new_Archive_toolBar  , SIGNAL(triggered())   , this      , SLOT(addNewArchiveProxy())    );
    connect( ui->actionOptions                  , SIGNAL(triggered())   , this      , SLOT(optionsDialog())         );
    connect( ui->actionNotes                    , SIGNAL(triggered())   , this      , SLOT(archiveNotesBrowser())   );
    connect( ui->actionTable_View               , SIGNAL(triggered())   , this      , SLOT(archiveAllBrowser())     );
    connect( ui->actionScore                    , SIGNAL(triggered())   , this      , SLOT(archiveScores()) );
    connect( ui->actionSites                    , SIGNAL(triggered())   , this      , SLOT(archiveSitesBrowser())   );
    connect( ui->actionFolder_Sizes             , SIGNAL(triggered())   , this      , SLOT(archiveFolderSizes())    );
    connect( ui->actionShow_toolbar_text        , SIGNAL(triggered(bool))   , this  , SLOT(viewShowToolbarText(bool)) );
    connect( ui->actionSite_updater             , SIGNAL(triggered())   , this      , SLOT(siteUpdaterDialog()) );
    connect( ui->actionPreferences              , SIGNAL(triggered())   , this      , SLOT(libraryPreferencesDialog())  );
    connect( ui->actionMonthly_uploads          , SIGNAL(triggered())   , this      , SLOT(archiveMonthlyUploads())     );
    connect( ui->actionKeywords                 , SIGNAL(triggered())   , this      , SLOT(archiveKeywordCloud())       );
    connect( ui->actionAutomatic_Tonemap        , SIGNAL(triggered())   , this      , SLOT(process_auToneMapper())      );
}


// ------------------------------------------- @INITS --------------------------------------

void MainWindow::loadStyleSheet(QString qss)
{    
    QFile file(qss);
        if(file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            setStyleSheet(file.readAll());
            file.close();
        }    
}

void MainWindow::initManagers()
{
    libMan  = new libraryManager(this);
    setMan  = new settingsManager(this);
    archive = new archiveManager(this);
    archive->attachLibMan(libMan);
    archive->attachUi(ui);
    archive->init();

    process = new processManager(this);
    process->attachUi(ui);
    process->attachLibMan(libMan);
    process->attachSetMan(setMan);
}

void MainWindow::defaultValues()
{
///
///  Default global variables
///
    qDebug() << "";
    qDebug() << "  --> defaultValues()";
    //processCurrentPath  = "";
    processSelectedId   = 0;
    //archiveSelectedId   = 0;
    timerIsRunning      = false;
    qDebug() << "       Default values loaded:";
    qDebug() << "        processCurrentPath =\"\"";
    qDebug() << "        processSelectedId=0";
    qDebug() << "        archiveSelectedId=0";
    qDebug() << "        timerIsRunning=false";
    qDebug() << "  <-- defaultValues()";
    qDebug() << "";
}

//------------------------------------------------ @PROGRAMS -----------------------------------------------

void MainWindow::openWithPS(QStringList arg)
{
    QString exe = setMan->value("ProgramPaths/PhotoShop");

    if (exe=="")
        {
            QMessageBox Msgbox;
            Msgbox.setText("Location of PhotoShop.exe not set!");
            Msgbox.exec();
            return;
        }

    QProcess cmd;
    qDebug() << arg;
    cmd.startDetached(exe,arg);
}

void MainWindow::openWithPTGui(QStringList arg)
{
    QString exe = setMan->value("ProgramPaths/PTGUi");

    if (exe=="")
        {
            QMessageBox Msgbox;
            Msgbox.setText("Location of PTGUi.exe not set!");
            Msgbox.exec();
            return;
        }

    QProcess cmd;
    qDebug() << arg;
    cmd.startDetached(exe,arg);
}

void MainWindow::openWithPhotoMatix(QStringList arg)
{
    QString exe = setMan->value("ProgramPaths/PhotoMatix");

    if (exe=="")
        {
            QMessageBox Msgbox;
            Msgbox.setText("Location of PhotoMatix.exe not set!");
            Msgbox.exec();
            return;
        }

    QProcess cmd;
    qDebug() << arg;
    cmd.startDetached(exe,arg);
}



//------------------------------------------------ @OVERLAY ------------------------------------------------

void MainWindow::initOverlay()
{
    overlay         = new Overlay(ui->centralWidget);
    overlayCounter  = 0;
}

void MainWindow::hideOverlay()
{
    overlayCounter--;
    if (overlayCounter<=0)  overlay->hideMessage();
}

void MainWindow::showOverlay(QString msg)
{
    overlay->showMessage( msg );
    overlayCounter++;
    QTimer::singleShot(3500,this,SLOT(hideOverlay()));
}


//------------------------------------------------- @EVENTS -------------------------------------------------

void MainWindow::dragEnterEvent(QDragEnterEvent *event)
{
    event->accept(); // !!!!!! important
    qDebug() << "dragEnter";
}

void MainWindow::dropEvent(QDropEvent *event)
{
        //this->raise();
        this->activateWindow();
        QList<QUrl> urls = event->mimeData()->urls();
        QString droppedFolder = urls.at(0).toString().replace("file:///","");

        qDebug() << QDir(droppedFolder).exists();

        if (!QDir(droppedFolder).exists())
        {
            QMessageBox Msgbox;
            Msgbox.setText("Not a folder: '"+droppedFolder+"' !");
            Msgbox.exec();
        }
        else
        {
            droppedSelectDialog drpDialog;
            drpDialog.setFolder(droppedFolder);
            if (drpDialog.exec())
                {
                    if (drpDialog.result=="PROCESS")
                        {
                            process->droppedFolder=droppedFolder;
                            process->dialog_addNew();
                        }
                    if (drpDialog.result=="ARCHIVE") archive->dialog_addNew(droppedFolder);
                }
        }
}

void MainWindow::resizeEvent (QResizeEvent *event)
{
///
///  event wrapper, called on every window resize, calls the processor thumbnail views resizer function
///
    qDebug() << "";
    qDebug() << "--> resizeEvent()";
    qDebug() << event;

    // overlay
    overlay->resize(event->size());

    // processor thumbnail views
    //this->processListViewsResize();

    // archive preview
    //this->archivePreviewImageResize();
    archive->previewImageResize();
    //archive->fileNaviThumbResize();
    //mod->archive->center->previewImage->resize();

    // archive thumbnails
    archive->abDelegate->thumbSize   = 180+10*4*ui->horizontalSlider_archiveThumbSize->value();
    archive->abDelegate->parentWidth = ui->listView_archiveBrowser->width();

    qDebug() << ui->splitter->sizes();
    qDebug() << ui->splitter->height();
    qDebug() << ui->widget_11->height();

    qDebug() << "<-- resizeEvent()";
    qDebug() << "";
}

//------------------------------------------------- @SPLASH -------------------------------------------------

void MainWindow::initSplash()
{
    QPixmap pixmap(":/icons/icons/splash-1.jpg");
    splash= new QSplashScreen(pixmap.scaled(640,480,Qt::KeepAspectRatio , Qt::SmoothTransformation),Qt::WindowStaysOnTopHint);
    splash->show();
    splash->showMessage(QObject::tr("Setting up the main window..."), Qt::TopRightCorner, Qt::white);
}

void MainWindow::closeSplash()
{
    splashOpacity = 1;
    QTimer::singleShot(1000,this,SLOT(fadeOutSplash()));
}

void MainWindow::fadeOutSplash()
{
   splash->setWindowOpacity(splashOpacity);
   splashOpacity = splashOpacity - 0.05;
   if (splashOpacity>0) QTimer::singleShot(25,this,SLOT(fadeOutSplash()));
        else splash->close();
}


//------------------------------------------------ @DIALOGS -------------------------------------------------

void MainWindow::optionsDialog()
{
    settingsDialog aDialog;
    aDialog.attachLibMan(libMan);
    if (aDialog.exec())
    {
        setMan->save("ProgramPaths/PhotoMatix"              , aDialog.path_photoMatix   );
        setMan->save("ProgramPaths/PhotoShop"               , aDialog.path_photoShop    );
        setMan->save("ProgramPaths/PTGui"                   , aDialog.path_ptGui        );
        setMan->save("GeneralSettings/defaultArchivePath"   , aDialog.path_defaultArchiveFolder);
        archive->scoringMode=aDialog.scoringMode;
        archive->filterDo(ui->lineEdit_archiveFilter->text());
    }
}

void MainWindow::libraryPreferencesDialog()
{
    libraryPrefsDialog lbpDialog;
    lbpDialog.attachLibMan(libMan);
    lbpDialog.init();
    if (lbpDialog.exec())
        {
            libMan->scoreBias = lbpDialog.scoreBias;
            archive->filter("");
        }
}

//----------------------------------------------- @UI --------------------------------------------------------

/*
void MainWindow::listViewIconMode(QListView* widget)
{
///
///  set the widget mode to iconmode, and set up scrollbars, icon sizes and spacing
///
    qDebug() << "        --> listViewIconMode()";
    qDebug() << "           " << widget;
        widget->setViewMode(QListWidget::IconMode);
        widget->setResizeMode(QListWidget::Adjust );
        widget->setDragEnabled(false);
        widget->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        widget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        widget->setSpacing(0);
        widget->setBaseSize(QSize(160,160));
    qDebug() << "        <-- listViewIconMode()";
}
*/



void MainWindow::setWindowTitleAndIcon()
{
    this->setWindowTitle(setMan->programName+" ("+setMan->programVersion+")");
    this->setWindowIcon(QIcon(":/icons/icons/P.png"));
}

void MainWindow::uiRestore()
{
    // set main toolbar icon mode (saved to registry) (COMMON)
        if (setMan->value("GeneralSettings/mainToolbarMode")=="iconOnly")
            {
                ui->mainToolBar->setToolButtonStyle(Qt::ToolButtonIconOnly);
                ui->actionShow_toolbar_text->setChecked(false);
            }

    // restore geometry from previous state (saved on program normal exit, to registry) (COMMON)
        QByteArray winGeometry;
        QByteArray winState;
        winGeometry = winGeometry.fromBase64(setMan->value("GeneralSettings/mainWindowGeometry").toLocal8Bit() );
        winState    = winState.fromBase64(setMan->value("GeneralSettings/MainWindowState").toLocal8Bit() );
        restoreGeometry(winGeometry);
        restoreState(winState);
}

void MainWindow::uiInits()
{
    // set default font
    QFont f=QApplication::font();
    f.setStyleStrategy(QFont::PreferQuality);
    QApplication::setFont(f);

    // toolbar
        ui->actionAdd_new_Archive_toolBar->setEnabled(true);
        ui->actionAdd_new_Process_toolBar->setEnabled(false);
        ui->actionMove_to_Archive_toolBar->setEnabled(false);
        ui->actionShow_toolbar_text->setChecked(true);

    // tabbar of main tab widget
        ui->tabWidget->tabBar()->setVisible(false);

    // actions
        ui->actionArchive->setChecked(true);
}


void MainWindow::viewShowToolbarText(bool check)
{
    if (check)
        {
            ui->mainToolBar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
            setMan->save("GeneralSettings/mainToolbarMode"  , "textBesideIcon"   );
        }
        else
        {
            ui->mainToolBar->setToolButtonStyle(Qt::ToolButtonIconOnly);
            setMan->save("GeneralSettings/mainToolbarMode"  , "iconOnly"   );
        }

}

//----------------------------------------------- @TABS ------------------------------------------

void MainWindow::tabSelector()
{

    if (sender()->objectName()=="actionProcess")            tabSelectByName("Process");
    if (sender()->objectName()=="listView_processBrowser")  tabSelectByName("Process");;
    if (sender()->objectName()=="actionBrowse")             tabSelectByName("Browse");
    if (sender()->objectName()=="actionArchive")            tabSelectByName("Archive");

}

void MainWindow::tabSelectByName(QString name)
{
    ui->actionProcess->setChecked(name=="Process");
    ui->actionBrowse->setChecked(name=="Browse");
    ui->actionArchive->setChecked(name=="Archive");
    ui->actionAdd_new_Archive_toolBar->setEnabled(name=="Archive");
    ui->actionAdd_new_Process_toolBar->setEnabled(name=="Process");
    ui->actionMove_to_Archive_toolBar->setEnabled(name=="Process");
    if (name=="Process") ui->tabWidget->setCurrentIndex(0);
    if (name=="Browse")  ui->tabWidget->setCurrentIndex(1);
    if (name=="Archive") ui->tabWidget->setCurrentIndex(2);
}

//---------------------------------------------- @KEYBOARD ----------------------------------------------

void MainWindow::keyboardInitShortcuts()
{
    // Keyboard shortcuts (ARCHIVES)
    new QShortcut ( QKeySequence(Qt::Key_Left)  ,this   ,SLOT(shortCutKeyLeft()));
    new QShortcut ( QKeySequence(Qt::Key_Right) ,this   ,SLOT(shortCutKeyRight()));

    new QShortcut ( QKeySequence(Qt::Key_X)     ,this   ,SLOT(shortCutKeyLeft()));
    new QShortcut ( QKeySequence(Qt::Key_V)     ,this   ,SLOT(shortCutKeyRight()));

    //new QShortcut ( QKeySequence(Qt::Key_C)     ,archive   ,SLOT(collect()));
    new QShortcut ( QKeySequence(Qt::Key_D)     ,this   ,SLOT(proxy_key_D()) );
}

void MainWindow::proxy_key_D()  {   archive->collectionTargetCycle();   }


void MainWindow::shortCutKeyRight()
{
    archive->selectNext();
}

void MainWindow::shortCutKeyLeft()
{
    archive->selectPrev();
}


void MainWindow::shortCutKeyRightProxy()
{
    this->shortCutKeyRight();
}

void MainWindow::shortCutKeyLeftProxy()
{
    this->shortCutKeyLeft();
}




//------------------------------------------- @BASIC -----------------------------------------

MainWindow::~MainWindow()
{
//
//  Program close delete ui
//
    qDebug() << "";
    qDebug() << "->> ~MainWindow()";

    libMan->archiveSelectedTitleSave(archive->selected->title);
    libMan->processSelectedTitleSave(process->selected->title);
    libMan->archiveZoomlevelSave(ui->horizontalSlider_archiveThumbSize->value());
    libMan->archiveTreeExpandStateSave(ui->pushButton_archiveTreeExpand->isChecked());
    libMan->archiveTreeGroupedStateSave(archive->treeGrouped);
    libMan->archiveGroupTypeSave(archive->groupType);
    QString curModule;
    qDebug() << ui->tabWidget->currentIndex();
    if (ui->tabWidget->currentIndex()==0) curModule="Process";
    if (ui->tabWidget->currentIndex()==1) curModule="Browse";
    if (ui->tabWidget->currentIndex()==2) curModule="Archive";
    libMan->currentModuleSave(curModule);
    libMan->archiveFileNaviVisibleSave(ui->widget_archiveFolderNavi->property("status").toString() );
    //if (!ui->widget_archiveFolderNavi->isVisible()) archive->fileNaviToggle();
    libMan->archiveFileNaviSizeSave( QString::number( ui->splitter->sizes().at(1) ));

    // sidebar status
    QString value="";
    for (int i=0;i<archive->sidebarItems.count();++i)
        {
            //(archive->sidebarItems.at(i).content->isVisible()) ? value.append( "1" ): value.append( "0" );
            (archive->sidebarItems.at(i)->getVisible()) ? value.append( "1" ): value.append( "0" );
        }
    libMan->archiveSidebarStatusSave(value);

    setMan->saveWindowParams(saveGeometry(),saveState());

    delete ui;
    qDebug() << "<<- ~MainWindow()";
    qDebug() << "";
}

// ---------------------------------------- @LIBRARY ---------------------------------------------------------------------

void MainWindow::libraryAutoLoad()
{
    // autoload (COMMON)
    if (setMan->contains("GeneralSettings/autoLoad"))
            libMan->loadFrom(setMan->value("GeneralSettings/autoLoad"));
    else
            ui->mainStack->setCurrentIndex(1);
}

void MainWindow::loadHistory()
{
///
/// load the recently opene file paths from registry and fill the recentfiles listWidget
///

    qDebug() << "";
    qDebug() << "  --> loadHistory()";


    ui->listWidget_recentFiles->clear();
    ui->pushButton_loadFromList->setEnabled(true);
    QStringList history = setMan->loadHistory();
    if (history.count()==0) ui->pushButton_loadFromList->setEnabled(false);
    foreach (QString s, history ) ui->listWidget_recentFiles->addItem(s);
    if (history.count()!=0) ui->listWidget_recentFiles->setCurrentRow(0);

    qDebug() << "  <-- loadHistory()";
    qDebug() << "";
}

void MainWindow::libraryClose()
{
///
/// close library and switch GUI to cleanstart
///
    ui->mainStack->setCurrentIndex(1);
    this->setWindowTitle(setMan->programName+" ("+setMan->programVersion + ")" );
    this->loadHistory();
}

void MainWindow::loadFromRecent()
{
    qDebug() << "--> loadFromRecent()";
    libMan->libraryPath = ui->listWidget_recentFiles->currentIndex().data().toString();
    if (ui->checkBox_autoLoad->isChecked())
            setMan->save("GeneralSettings/autoLoad",libMan->libraryPath);
        else
            setMan->remove("GeneralSettings/autoLoad");

    libMan->load();

    qDebug() << "<-- loadFromRecent()";
}

void MainWindow::libraryLoaded()
{
    qDebug() << "";
    qDebug() << "  --> libraryLoaded()";
    // !!
    // libraryLoaded signal must be emitted after library loaded **libraryManager.cpp
    // !!
    //
    ui->mainStack->setCurrentIndex(0);
    this->setWindowTitle(setMan->programName+" ("+setMan->programVersion + ") [" + libMan->libraryPath.split("/").last() + "]" );
    setMan->saveHistory(libMan->libraryPath);

    QSqlQuery qry = libMan->db.exec("SELECT value FROM var WHERE key='scoreBias'");
    qry.first();
    QString value = qry.value(0).toString();
    if (value == "") libMan->scoreBias = 1; else libMan->scoreBias = value.toFloat();

    archive->init_2();
    //this->processInitModels();
    process->init();
    this->loadSavedUiThings();

    QString url = libMan->libraryFolder+"HTML/google_maps_full_test17.html";
    qDebug() << url;
    ui->webView_fullMap->load(QUrl::fromLocalFile(url));

    //ui->listView_processTree->setCurrentIndex(worxModel->index(0,0));

    qDebug() << "  <-- libraryLoaded()";
    qDebug() << "";
}

void MainWindow::loadSavedUiThings()
{

    qDebug() << "    --> loadSavedUiThings()";

    QString value;

    // load last module
    qDebug() << "       ..1";
    value = tools::queryFirst(libMan->db,"SELECT value FROM var WHERE key='lastModule'");
    if ( value != "" ) tabSelectByName(value);

    // load archive browser zoom level
    qDebug() << "       ..2";
    value = tools::queryFirst(libMan->db,"SELECT value FROM var WHERE key='archiveZoomLevel'");
    if ( value != "" ) ui->horizontalSlider_archiveThumbSize->setValue(value.toInt());

    // load archive tree group type
    qDebug() << "       ..3";
    value = tools::queryFirst(libMan->db,"SELECT value FROM var WHERE key='archiveGroupType'");
    if ( value != "" )
        {
            if (value=="A-Z"  ) ui->pushButton_archiveGroupByAz->click();
            if (value=="DATE" ) ui->pushButton_archiveGroupByDate->click();
            if (value=="SCORE") ui->pushButton_archiveGroupByScore->click();
            if (value=="LOC")   ui->pushButton_archiveGroupByLoc->click();
        }

    // load archive tree groupeded state
    qDebug() << "       ..4";
    value = tools::queryFirst(libMan->db,"SELECT value FROM var WHERE key='archiveTreeGrouped'");
    if ( value == "0" )
        {
            archive->treeGrouped = false;
            ui->pushButton_archiveTreeGroup->setFlat(false);
            archive->treeModelConvert();
            ui->treeView_archiveTree->setModel(archive->modelTree);
        }

    // load expand button state
    qDebug() << "       ..5";
    value = tools::queryFirst(libMan->db,"SELECT value FROM var WHERE key='archiveTreeExpanded'");
    if ( value == "1" )
        {
            ui->pushButton_archiveTreeExpand->setChecked(true);
            archive->treeExpandCollapse(true);
        }

    // LOAD LAST SELECTED ARCHIVE (IF SAVED) else select first
    qDebug() << "       ..6";
    value = tools::queryFirst(libMan->db,"SELECT value FROM var WHERE key='lastArchiveTitle'");
    qDebug() << "       ..6.1" << value;
    if ( value != "" )
        {
            if ( !archive->selectByTitle(value) ) archive->selectFirst();
        }
            else archive->selectFirst();

    // LOAD LAST SELECTED PROCESS (IF SAVED) else select first
    qDebug() << "       ..7";
    value = tools::queryFirst(libMan->db,"SELECT value FROM var WHERE key='lastProcessTitle'");
    if ( value != "" )
        {
            //QModelIndex index=process->worxModel->fin findItems(value,Qt::MatchRecursive).first()->index();
            bool found = false;
            int i;
            for (i=0;i<process->worxModel->rowCount();++i)
                {
                    if (process->worxModel->index(i,0).data().toString()==value) found = true;
                    if (found) break;
                }
            if (found) process->select(process->worxModel->index(i,0));
        }

    // LOAD file navi height
    qDebug() << "       ..8";
    value = tools::queryFirst(libMan->db,"SELECT value FROM var WHERE key='fileNaviSize'");
    if ( value != "" )
        {
            int fullHeight = ui->widget_11->height();
            int val = value.toInt();
            QList<int> newSizes;
            newSizes << fullHeight-val << val;
            ui->splitter->setSizes(newSizes);
            QTimer::singleShot(100,archive,SLOT(fileNaviThumbResize()));
        }

    qDebug() << "       ..9";
    value = tools::queryFirst(libMan->db,"SELECT value FROM var WHERE key='fileNaviVisible'");
    if ( value != "" )
        {
            if (value=="hidden") QTimer::singleShot(200,archive,SLOT(fileNaviToggle()));
                else ui->widget_archiveFolderNavi->setProperty("status","visible");
        }
    qDebug() << "      ..10";
    value = tools::queryFirst(libMan->db,"SELECT value FROM var WHERE key='archiveSidebarStatus'");
    if ( value != "" )
        {

            for (int i=0;i<value.length();++i)
                {
                    if (i<archive->sidebarItems.count())
                        {
                            qDebug() << "            >> " << i << value.at(i) << archive->sidebarItems.at(i)->name;
                            if (value.at(i)=='0')
                                {
                                    qDebug() << "      TOGGLE!";
                                    archive->sidebarItems.at(i)->toggle();
                                }
                        }
                }
        }
    qDebug() << "      ..11";



    qDebug() << "    <-- loadSavedUiThings()";
}


//----------------------------------------------- @TOOLS --------------------------------------

/*
void MainWindow::openLocalFile(QString url)
{
    QDesktopServices::openUrl(QUrl::fromLocalFile(url));

}
*/



//--------------------------------------------  @NESTED CLASSES EXPERIMENT -------------------------

/*
void MainWindow::Modules::Archive::Center::Files::doubleClick(QModelIndex index)
{

}

*
 * ARCHIVE
 *        \->CENTER
 *                 \->PREVIEWIMAGE
 *                                \->LOAD
 *

void MainWindow::Modules::Archive::Center::PreviewImage::load()
{
    int ph = 120;
    int pw = 216;

    pixmap.load(libMan->libraryFolder + "previews/" + imagePath);

    pixmapWidth       = pixmap.width();
    pixmapHeight      = pixmap.height();
    pixmapAspectRatio = (float) pixmapWidth  / (float) pixmapHeight ;

    // TO SIDEBAR!!!
    if (ph*pixmapAspectRatio < pw)
        ui->label_sideBarPreview->setPixmap(pixmap.scaled(ph*pixmapAspectRatio,ph,Qt::KeepAspectRatio , Qt::SmoothTransformation));
    else
        ui->label_sideBarPreview->setPixmap(pixmap.scaled(pw,pw/pixmapAspectRatio,Qt::KeepAspectRatio , Qt::SmoothTransformation));


}

void MainWindow::Modules::Archive::Center::PreviewImage::resize()
{
    qDebug() << "        --> archivePreviewImageResize()";
    int containerWidth=ui->label_previewImage->width();
    int containerHeight=ui->label_previewImage->height();
    float containerAspectRatio = (float) containerWidth / (float) containerHeight;

    if ( pixmapAspectRatio > containerAspectRatio)
        {
            int newWidth = pixmapWidth;
            if ( containerWidth<pixmapWidth ) newWidth=containerWidth;
            ui->label_previewImage->setPixmap(pixmap.scaled(newWidth,newWidth/pixmapAspectRatio,Qt::KeepAspectRatio , Qt::SmoothTransformation));
        }
        else
        {
            int newHeight=pixmapHeight;
            if (containerHeight<pixmapHeight) newHeight=containerHeight;
            ui->label_previewImage->setPixmap(pixmap.scaled(newHeight*pixmapAspectRatio,newHeight,Qt::KeepAspectRatio , Qt::SmoothTransformation));
        }
    qDebug() << "        <-- archivePreviewImageResize()";
}

*/




