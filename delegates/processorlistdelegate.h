#ifndef PROCESSORLISTDELEGATE_H
#define PROCESSORLISTDELEGATE_H

#include <QStyledItemDelegate>

class processorListDelegate : public QStyledItemDelegate
{
public:
    processorListDelegate();
    QString cacheFolder;
protected:
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;
};

#endif // PROCESSORLISTDELEGATE_H
