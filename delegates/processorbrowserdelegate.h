#ifndef PROCESSORBROWSERDELEGATE_H
#define PROCESSORBROWSERDELEGATE_H

#include <QStyledItemDelegate>

class processorBrowserDelegate : public QStyledItemDelegate
{
public:
    processorBrowserDelegate();
protected:
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;
};

#endif // PROCESSORBROWSERDELEGATE_H
