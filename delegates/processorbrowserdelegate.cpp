#include "processorbrowserdelegate.h"

#include <QApplication>
#include <QImageReader>
#include <QPainter>

processorBrowserDelegate::processorBrowserDelegate()
{
}

void processorBrowserDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    // init style
    QStyleOptionViewItemV4 opt = option;
    initStyleOption(&opt, index);

    // get data from model
    QString line0 = index.model()->data(index.model()->index(index.row(), 0)).toString();
    QString path  = index.model()->data(index.model()->index(index.row(), 2)).toString();
    QString isPano= index.model()->data(index.model()->index(index.row(), 3)).toString();
    QString thumb = index.model()->data(index.model()->index(index.row(), 4)).toString();

    // load icon from model (BLOB)
    QByteArray iconData ;
    iconData = iconData.fromBase64(index.model()->data(index.model()->index(index.row(), 5)).toByteArray() );

    // draw correct background
    opt.text = "";
    QStyle *style = opt.widget ? opt.widget->style() : QApplication::style();
    style->drawControl(QStyle::CE_ItemViewItem , &opt, painter, opt.widget);
    QRect rect = opt.rect;
    QPalette::ColorGroup cg = opt.state & QStyle::State_Enabled ? QPalette::Normal : QPalette::Disabled;
    if (cg == QPalette::Normal && !(opt.state & QStyle::State_Active)) cg = QPalette::Inactive;

    // draw thumbnail
    if (thumb!="")
    {
        // load icondata to pixmap
        QPixmap pix;
        if (thumb.contains(".jpg")) pix.loadFromData(iconData,"JPG");
        if (thumb.contains(".png")) pix.loadFromData(iconData,"PNG");

        // calculate aspect ratio of pixmap (width/height)
        int pixH = pix.height();
        int pixW = pix.width();
        float aR=(pixW*1.0)/(pixH*1.0);
        int sz=180;

        // draw the pixmap (resizing based on aspect ratio)
        if (aR>=1) painter->drawPixmap(QRect(rect.left()+10, rect.top()+2+sz*(1-1/aR)/2, sz, sz/aR),pix );
        else       painter->drawPixmap(QRect(rect.left()+10+sz*(1-1*aR)/2, rect.top()+2, sz*aR, sz),pix );
    }

    // cut the beginning of title if too long and put '...'
    if (line0.length()>22) line0 = "..."+line0.right(22);

    // draw text (title)
    painter->drawText(QRect(rect.left()-20, rect.top()+90, rect.width()+40, rect.height()), opt.displayAlignment, line0);
}

QSize processorBrowserDelegate::sizeHint(const QStyleOptionViewItem & option, const QModelIndex & index ) const
{
    // get size from default
    QSize result = QStyledItemDelegate::sizeHint(option, index);
    // set height and width to 200px
    result.setHeight(200);
    result.setWidth(200);
    return result;
}
