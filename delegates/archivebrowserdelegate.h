#ifndef ARCHIVEBROWSERDELEGATE_H
#define ARCHIVEBROWSERDELEGATE_H

#include <QStyledItemDelegate>

class archiveBrowserDelegate : public QStyledItemDelegate
{
public:
    archiveBrowserDelegate();
    QString libraryFolder;
    int thumbSize;
    int parentWidth;
    QString style;
    QString filter;
    QString filterRole;
protected:
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;
};


#endif // ARCHIVEBROWSERDELEGATE_H
