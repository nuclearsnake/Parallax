#ifndef COLLECTIONLISTDELEGATE_H
#define COLLECTIONLISTDELEGATE_H

#include <QStyledItemDelegate>

class collectionListDelegate : public QStyledItemDelegate
{
public:
    collectionListDelegate();
protected:
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;
};

#endif // COLLECTIONLISTDELEGATE_H
