#include "processorlistdelegate.h"

#include <QApplication>
#include <QImageReader>
#include <QPainter>
#include <QDebug>

processorListDelegate::processorListDelegate()
{
}



void processorListDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    // init style
    QStyleOptionViewItemV4 opt = option;
    initStyleOption(&opt, index);

    // get data from model
    QString line0 = index.model()->data(index.model()->index(index.row(), 0)).toString();
    QString path  = index.model()->data(index.model()->index(index.row(), 2)).toString();
    QString isPano= index.model()->data(index.model()->index(index.row(), 3)).toString();
    QString thumb = index.model()->data(index.model()->index(index.row(), 4)).toString();
    //qDebug() << thumb;
    // load icon from model (BLOB)
    QByteArray iconData ;
    iconData = iconData.fromBase64(index.model()->data(index.model()->index(index.row(), 5)).toByteArray() );

    // draw correct background
    opt.text = "";
    QStyle *style = opt.widget ? opt.widget->style() : QApplication::style();
    style->drawControl(QStyle::CE_ItemViewItem, &opt, painter, opt.widget);
    QRect rect = opt.rect;
    QPalette::ColorGroup cg = opt.state & QStyle::State_Enabled ? QPalette::Normal : QPalette::Disabled;
    if (cg == QPalette::Normal && !(opt.state & QStyle::State_Active)) cg = QPalette::Inactive;

    // draw thumbnail
    if (thumb!="")
    {
        // load icondata to pixmap
        QPixmap pix;
        QImageReader pixR;

        QImage img;
        pixR.setFileName(cacheFolder+thumb+".jpg");
        //qDebug() << line0 << cacheFolder+thumb;
        pixR.read(&img);
        pix = QPixmap::fromImage(img);
        //pix.loadFromData(iconData,"JPG");

        // load default icon if no icondata
        //qDebug() << pix.width();
        if (pix.width()==0)
        {
            //QImage img;
            pixR.setFileName(":/icons/icons/file-icon-a.png");
            pixR.read(&img);
            pix = QPixmap::fromImage(img);
        }

        // calculate aspect ratio of pixmap (width/height)
        int pixH = pix.height();
        int pixW = pix.width();
        float aR=(pixW*1.0)/(pixH*1.0);

        // draw the pixmap (resizing based on aspect ratio)
        if (aR>=1) painter->drawPixmap(QRect(rect.left()+2, rect.top()+2+32*(1-1/aR)/2, 32, 32/aR),pix );
        else       painter->drawPixmap(QRect(rect.left()+2+32*(1-1*aR)/2, rect.top()+2, 32*aR, 32),pix );
    }

    // draw text
    // title
    //qDebug() << QPalette::text();

   // QPalette::ColorGroup cg = opt.state & QStyle::State_Enabled ? QPalette::Normal : QPalette::Disabled;
    //if (cg == QPalette::Normal && !(opt.state & QStyle::State_Active)) cg = QPalette::Inactive;



    painter->setPen( QColor("#A2A2A2"));
    if (opt.state & QStyle::State_Selected) painter->setPen( QColor("#E7E7E7"));
    painter->drawText(QRect(rect.left()+38, rect.top(), rect.width(), rect.height()), opt.displayAlignment, line0);
    // P marker if pano mode
    // if (isPano=="1") painter->drawText(QRect(rect.left()+170, rect.top(), rect.width(), rect.height()), opt.displayAlignment, "P");

}


QSize processorListDelegate::sizeHint(const QStyleOptionViewItem & option, const QModelIndex & index ) const
{
    // get size from default
    QSize result = QStyledItemDelegate::sizeHint(option, index);
    // set height to 36px
    result.setHeight(36);
    return result;
}
