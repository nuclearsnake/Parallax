#include "archivebrowserdelegate.h"


#include <QApplication>
#include <QImageReader>
#include <QPainter>
#include <QDebug>
#include <QFile>

archiveBrowserDelegate::archiveBrowserDelegate( /*QString path,int tSize, int pW, QString style */)
{
    //libraryFolder=path;
    //thumbSize=tSize;
    //parentWidth=pW;
    //this->style=style;
    //int m = parentWidth/thumbSize;
    //int newSize = parentWidth/(m+0.0000001)-6 ;

    //qDebug() << this->parent();

    //qDebug() << "             item delegate initialized:"; //<< thumbSize << pW << m << newSize;
}

void archiveBrowserDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    // init style
    QStyleOptionViewItemV4 opt = option;
    initStyleOption(&opt, index);

    //qDebug() << this->sender();

    //qDebug() << this->libraryFolder;

    // get data from model
    QString line0 = index.model()->data(index.model()->index(index.row(), 0)).toString();
    QString title = line0;
    QString path  = index.model()->data(index.model()->index(index.row(), 2)).toString();
    QString keyWords = index.model()->data(index.model()->index(index.row(), 3)).toString();
    QString date = index.model()->data(index.model()->index(index.row(), 4)).toString();
    QString thumb = index.model()->data(index.model()->index(index.row(), 5)).toString();
    QString notes = index.model()->data(index.model()->index(index.row(), 6)).toString();
    QString score = index.model()->data(index.model()->index(index.row(), 7)).toString();
    QString locLat = index.model()->data(index.model()->index(index.row(), 8)).toString();
    QString locTxt = index.model()->data(index.model()->index(index.row(), 10)).toString();
    QString sites  = index.model()->data(index.model()->index(index.row(), 12)).toString();
    // qDebug() << "SITES " << sites;

    // load icon from model (BLOB)
    QByteArray iconData ;
    iconData = iconData.fromBase64(index.model()->data(index.model()->index(index.row(), 5)).toByteArray() );

    // draw correct background

    opt.text = "";
    QStyle *style = opt.widget ? opt.widget->style() : QApplication::style();
    style->drawControl(QStyle::CE_ItemViewItem , &opt, painter, opt.widget);
    QRect rect = opt.rect;
    QPalette::ColorGroup cg = opt.state & QStyle::State_Enabled ? QPalette::Normal : QPalette::Disabled;
    if (cg == QPalette::Normal && !(opt.state & QStyle::State_Active)) cg = QPalette::Inactive;


    // draw thumbnail
    if (thumb!="")
    {

        // load icondata to pixmap
        QPixmap pix;
        //if (thumb.contains(".jpg")) pix.loadFromData(iconData,"JPG");
        //if (thumb.contains(".png")) pix.loadFromData(iconData,"PNG");
        //qDebug() << "z:/Parallax/test2/previews/"+thumb;

        QString originPath = libraryFolder + "/previews/"+thumb;
        QString cachedPath = libraryFolder + "/cache/"+thumb;

        //qDebug() << "OPT" << rect.width()/6;
        int w = rect.width()/6;
        if (line0.length()>w) line0 = line0.left(w/2-2)+"...."+line0.right(w/2-2);

        // draw text (title)
        QPen pen =painter->pen();
        QFont font = painter->font();
        QRect txtFrame=QRect(rect.left(), rect.bottom()-23 , rect.width(), 20);
        painter->drawText( txtFrame, opt.displayAlignment, line0);

        painter->setPen(QColor(0,0,0,20));
        painter->setFont(QFont("",32,QFont::Bold));
        if (this->style!="1") painter->drawText( QPoint(rect.left()+0,rect.top()+32)  , QString::number(  index.row()+1 ) );

        if (score!="")
            {
                painter->setPen(QColor(255,255,255,125));
                painter->setFont(QFont("",8));
                QPixmap p(":/icons/icons/star_off_d_32.png");
                // :/icons/icons/star_off_d_32.png
                //qDebug() << 1 ;
                painter->drawPixmap(rect.right()-40,rect.top()+3,12,12,p.scaled(12,12,Qt::KeepAspectRatio, Qt::SmoothTransformation));
                score = score.sprintf("%1.2f",score.toFloat());
                painter->drawText( QPoint(rect.right()-26,rect.top()+14)  , score );
            }

        if (notes!="")
            {
                QPixmap p(":/icons/icons/Sticky_Notes.png");

                painter->drawPixmap(rect.right()-76,rect.top()+3,12,12,p.scaled(12,12,Qt::KeepAspectRatio, Qt::SmoothTransformation));
            }

        if (keyWords!="")
            {
                QPixmap p(":/icons/icons/notes-1.png");

                painter->drawPixmap(rect.right()-58,rect.top()+3,12,12,p.scaled(12,12,Qt::KeepAspectRatio, Qt::SmoothTransformation));
            }
        if (locLat!="")
            {
                QPixmap p(":/icons/icons/map-pin-1.png");

                painter->drawPixmap(rect.right()-94,rect.top()+3,12,12,p.scaled(12,12,Qt::KeepAspectRatio, Qt::SmoothTransformation));
            }

        // S I T E S
        /*
            int vpos = 16;

            if (sites.contains("RD"))
            {
                QPixmap p(":/icons/icons/siteIconRd_GREY.png");
                painter->drawPixmap(rect.right()-16,rect.top()+vpos,12,12,p.scaled(12,12,Qt::KeepAspectRatio, Qt::SmoothTransformation));
            }
            else
            {
                QPixmap p(":/icons/icons/siteIconRd_GREY.png");
               // painter->drawPixmap(rect.right()-16,rect.top()+vpos,12,12,p.scaled(12,12,Qt::KeepAspectRatio, Qt::SmoothTransformation));
            }

            if (sites.contains("VB"))
            {
                QPixmap p(":/icons/icons/siteIconViewbug_GREY.png");
                painter->drawPixmap(rect.right()-30,rect.top()+vpos,12,12,p.scaled(12,12,Qt::KeepAspectRatio, Qt::SmoothTransformation));
            }
            else
            {
                QPixmap p(":/icons/icons/siteIconViewbug_GREY.png");
                //painter->drawPixmap(rect.right()-30,rect.top()+vpos,12,12,p.scaled(12,12,Qt::KeepAspectRatio, Qt::SmoothTransformation));
            }

            if (sites.contains("YP"))
            {
                QPixmap p(":/icons/icons/siteIconYoupic_GREY.png");
                painter->drawPixmap(rect.right()-44,rect.top()+vpos,12,12,p.scaled(12,12,Qt::KeepAspectRatio, Qt::SmoothTransformation));
            }
            else
            {
                QPixmap p(":/icons/icons/siteIconYoupic_GREY.png");
                //painter->drawPixmap(rect.right()-44,rect.top()+vpos,12,12,p.scaled(12,12,Qt::KeepAspectRatio, Qt::SmoothTransformation));
            }

            if (sites.contains("PX"))
            {
                QPixmap p(":/icons/icons/siteIconPixoto1_GREY.png");
                painter->drawPixmap(rect.right()-58,rect.top()+vpos,12,12,p.scaled(12,12,Qt::KeepAspectRatio, Qt::SmoothTransformation));
            }
            else
            {
                QPixmap p(":/icons/icons/siteIconPixoto1_GREY.png");
                //painter->drawPixmap(rect.right()-58,rect.top()+vpos,12,12,p.scaled(12,12,Qt::KeepAspectRatio, Qt::SmoothTransformation));
            }

            if (sites.contains("FB"))
            {
                QPixmap p(":/icons/icons/siteIconFaceBook_GREY.png");
                painter->drawPixmap(rect.right()-72,rect.top()+vpos,12,12,p.scaled(12,12,Qt::KeepAspectRatio, Qt::SmoothTransformation));
            }
            else
            {
                QPixmap p(":/icons/icons/siteIconFaceBook_GREY.png");
                //painter->drawPixmap(rect.right()-72,rect.top()+vpos,12,12,p.scaled(12,12,Qt::KeepAspectRatio, Qt::SmoothTransformation));
            }

            if (sites.contains("G+"))
            {
                QPixmap p(":/icons/icons/siteIconGoogle+_GREY.png");
                painter->drawPixmap(rect.right()-86,rect.top()+vpos,12,12,p.scaled(12,12,Qt::KeepAspectRatio, Qt::SmoothTransformation));
            }
            else
            {
                QPixmap p(":/icons/icons/siteIconGoogle+_GREY.png");
                //painter->drawPixmap(rect.right()-86,rect.top()+vpos,12,12,p.scaled(12,12,Qt::KeepAspectRatio, Qt::SmoothTransformation));
            }

            if (sites.contains("5P"))
            {
                QPixmap p(":/icons/icons/siteIcon500px_GREY.png");
                painter->drawPixmap(rect.right()-100,rect.top()+vpos,12,12,p.scaled(12,12,Qt::KeepAspectRatio, Qt::SmoothTransformation));
            }
            else
            {
                QPixmap p(":/icons/icons/siteIcon500px_GREY.png");
                //painter->drawPixmap(rect.right()-100,rect.top()+vpos,12,12,p.scaled(12,12,Qt::KeepAspectRatio, Qt::SmoothTransformation));
            }

            if (sites.contains("DA"))
            {
                QPixmap p(":/icons/icons/siteIconDeviantArt_GREY.png");
                painter->drawPixmap(rect.right()-114,rect.top()+vpos,12,12,p.scaled(12,12,Qt::KeepAspectRatio, Qt::SmoothTransformation));
            }
            else
            {
                QPixmap p(":/icons/icons/siteIconDeviantArt_GREY.png");
                //painter->drawPixmap(rect.right()-114,rect.top()+vpos,12,12,p.scaled(12,12,Qt::KeepAspectRatio, Qt::SmoothTransformation));
            }
        */
        // S I T E S   E N D


        bool cached = QFile::exists(cachedPath);
        //cached=false;
        //qDebug() << "cached=" << cached;



        if (cached) pix.load( cachedPath ); else pix.load( originPath );


        // calculate aspect ratio of pixmap (width/height)
        int pixH = pix.height();
        int pixW = pix.width();
        float aR=(pixW*1.0)/(pixH*1.0);
        int sz=280;


        if (aR>=1)
            {
                if (pix.width()!=0) pix = pix.scaled(sz, sz/aR,Qt::KeepAspectRatio , Qt::SmoothTransformation);

            }
        else
            {
                if (pix.width()!=0) pix = pix.scaled(sz*aR, sz,Qt::KeepAspectRatio , Qt::SmoothTransformation);

            }

        if (!cached) pix.save(cachedPath);

        sz=rect.width()-30;

        // draw the pixmap (resizing based on aspect ratio)
        if (aR>=1)
            {
                if (pix.width()!=0) pix.scaled(sz, sz/aR,Qt::KeepAspectRatio , Qt::SmoothTransformation);
                painter->drawPixmap(QRect(rect.left()+15, rect.top()+sz*(1-1/aR)/2+18, sz, sz/aR),pix );
            }
        else
            {
                if (pix.width()!=0) pix = pix.scaled(sz*aR, sz,Qt::KeepAspectRatio , Qt::SmoothTransformation);
                painter->drawPixmap(QRect(rect.left()+15+sz*(1-1*aR)/2, rect.top()+18, sz*aR, sz),pix );
            }

        //qDebug() << filterRole;
        bool filteredOut=false;
        if (filterRole!="")
            {
                QStringList F = filterRole.split(":");
                //qDebug() << F;
                if (F.count()>1)
                    {
                        QString filterType = F.at(0);
                        QString filterString = F.at(1);
                        //qDebug() << filterString;
                        if (filterType=="title")
                            {
                                if ( ( filterString!="#" ) & ( title.mid(0,1)!=filterString ) ) filteredOut = true;
                                if ( ( filterString=="#" ) & ( title.mid(0,1).toInt() == 0  ) ) filteredOut = true;
                            }
                        if ( ( filterType=="date" ) && ( filterString!=date.mid(0,4)           ) ) filteredOut = true;
                        if ( ( filterType=="loc"  ) && ( filterString!=locTxt.split(",").at(0) ) ) filteredOut = true;
                        if ( filterType=="score")
                            {
                                //qDebug() << filterString;
                                //qDebug() << "HIGH" << filterString.split(" - ").at(0).toFloat() << score.toFloat();
                                if (score.toFloat() >  filterString.split(" - ").at(0).toFloat() ) filteredOut = true;
                                if (score.toFloat() <= filterString.split(" - ").at(1).mid(0,4).toFloat() ) filteredOut = true;
                                /*
                                if( score.toFloat() >  filterString.split(" - ").at(0).toFloat() ||
                                    score.toFloat() <= filterString.split(" - ").at(1).toFloat()   ) filteredOut=true;
                                */
                            }
                    }
            }

        if (filteredOut)
            {
                QColor fade = QColor(128,128,128,200);
                painter->setPen( fade );
                painter->setBrush(fade);
                painter->drawRect(rect.left()+1,rect.top()+1, rect.width()-2,rect.height()-2 );
            }

        painter->setPen(pen);
        painter->setFont(font);
    }


    // cut the beginning of title if too long and put '...'


    //QRect nFrame=QRect(rect.left(), rect.top() , rect.width(), 20);

    //int m = parentWidth/thumbSize;  //unused

    /*painter->drawText( txtFrame, opt.displayAlignment,
             QString::number(m)
             + " | " + QString::number(parentWidth)
             + " | " + QString::number( ( (parentWidth+0.0)/m-6 ))
             + " | " + QString::number( ( (parentWidth)/m-6 ))
             + " | " + QString::number( qRound( (parentWidth+0.0)/m-6 ))
             );*/
    //painter->drawRect(txtFrame);
}

QSize archiveBrowserDelegate::sizeHint(const QStyleOptionViewItem & option, const QModelIndex & index ) const
{
    /*
    // get size from default
    QSize result = QStyledItemDelegate::sizeHint(option, index);
    QStyleOptionViewItemV4 opt = option;
    initStyleOption(&opt, index);
    qDebug()  << "SH";
    qDebug() << opt.rect.width();
    // set height and width to 200px
    //result.setHeight(200);
    //result.setWidth(200);
    result.setHeight(opt.rect.height());
    result.setWidth(opt.rect.width());
    */
    //QSize result = QStyledItemDelegate::sizeHint(option, index);

    //return result;



    QSize result = QStyledItemDelegate::sizeHint(option, index);
    // set height to 36px
    //result.setHeight(36);
    int m = parentWidth/thumbSize;



    int newSize = parentWidth/(m+0.0000001)-6 ;

    result.setWidth(newSize);
    result.setHeight(newSize+10);
    return result;
}

