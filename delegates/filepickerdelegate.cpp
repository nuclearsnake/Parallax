#include "filepickerdelegate.h"


#include <QApplication>
#include <QImageReader>
#include <QPainter>
#include <QDebug>
#include <QFile>

filePickerDelegate::filePickerDelegate( /*QString path,int tSize, int pW, QString style */)
{

        //thumbWidth = 220;

}

void filePickerDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    QStyleOptionViewItemV4 opt = option;
    initStyleOption(&opt, index);

    //get patams
    QString fileName    = index.model()->data(index.model()->index(index.row(), 0)).toString();
    QString fileName1   = index.model()->data(index.model()->index(index.row(), 4)).toString();
    QString filePath    = index.model()->data(index.model()->index(index.row(), 1)).toString();
    QString cachedFile  = cacheFolder + fileName  + ".jpg";
    QString cachedFile1 = cacheFolder + fileName1 + ".jpg";
    QString extension   = fileName.right(3).toUpper();

    QSize imageSize = QImageReader(filePath).size();

    int imageWidth = imageSize.width();
    int imageHeight = imageSize.height();

    QPen pen;
    QColor penColor;
    QFont font1;
    QFont font2;


    opt.text = "";

    // get style
    QStyle *style = opt.widget ? opt.widget->style() : QApplication::style();
    style->drawControl(QStyle::CE_ItemViewItem , &opt, painter, opt.widget);
    QRect rect = opt.rect;
    QPalette::ColorGroup cg = opt.state & QStyle::State_Enabled ? QPalette::Normal : QPalette::Disabled;
    if (cg == QPalette::Normal && !(opt.state & QStyle::State_Active)) cg = QPalette::Inactive;

    // colorize!
    painter->setPen(  QColor(0,0,0,0) );
    if ( extension == "JPG"  &&  imageWidth < 2000 &&  !fileName.toUpper().contains("WP") )
    {
        painter->setBrush(QColor(0,255,0,33) );
        painter->drawRect(rect.left(),rect.top(),rect.width(),rect.height());
    }
    if ( extension == "PSD"  &&  imageWidth < 2000 &&  !fileName.toUpper().contains("WP") )
    {
        painter->setBrush(QColor(255,255,0,33) );
        painter->drawRect(rect.left(),rect.top(),rect.width(),rect.height());
    }
    if ( extension == "PSD"  &&  imageWidth >= 2000 && !fileName.toUpper().contains("PRINT") &&  !fileName.toUpper().contains("WP")  )
    {
        painter->setBrush(QColor(0,255,255,33) );
        painter->drawRect(rect.left(),rect.top(),rect.width(),rect.height());
    }
    if ( fileName.toUpper().contains("PRINT") )
    {
        painter->setBrush(QColor(0,0,255,33) );
        painter->drawRect(rect.left(),rect.top(),rect.width(),rect.height());
    }
    if ( fileName.toUpper().contains("WP") )
    {
        painter->setBrush(QColor(255,0,55,33) );
        painter->drawRect(rect.left(),rect.top(),rect.width(),rect.height());
    }


    // draw filename below image
    penColor = QColor(255,255,255,255);
    pen.setColor(penColor);
    painter->setPen(pen);
    QRect txtFrame=QRect(rect.left(), rect.bottom()-23 , rect.width(), 20);
    int w = thumbWidth/6;
    if (fileName.count()>w) fileName =  ".."+fileName.right(w-2);
    painter->drawText( txtFrame, opt.displayAlignment, fileName);

    // draw file icon
    if (extension=="PSD") painter->drawPixmap(rect.left()+3,rect.top()+3,16,16,QPixmap(":/icons/icons/filetype-psd.png"));
    if (extension=="JPG") painter->drawPixmap(rect.left()+3,rect.top()+3,16,16,QPixmap(":/icons/icons/filetype-jpg.png"));

    // draw info text (top)
    penColor = QColor(0,0,0,155);
    pen.setColor(penColor);
    painter->setPen(pen);
    font2 = painter->font();
    font1 = font2;
    font1.setPixelSize(11);
    painter->setFont(font1);



    txtFrame=QRect(rect.left()+6, rect.top()+3 , rect.width()-10, 20);

    if (imageSize.width()>0)
        painter->drawText( txtFrame , Qt::AlignRight , QString::number( imageSize.width() ) + " x " + QString::number(imageSize.height()) );

    painter->setFont(font2);

    // draw image
    QPixmap pix;


        if ( QFile::exists(cachedFile) ) pix.load( cachedFile );
            else if ( QFile::exists(cachedFile1) ) pix.load( cachedFile1 );
                else pix.load( ":/icons/icons/file-icon-a.png" );

    int pixH = pix.height();
    int pixW = pix.width();
    float aR=(pixW*1.0)/(pixH*1.0);
    int sz=thumbWidth-10;


    if (aR>=1)
        {
            if (pix.width()!=0) pix = pix.scaled(sz, sz/aR,Qt::KeepAspectRatio , Qt::SmoothTransformation);

        }
    else
        {
            if (pix.width()!=0) pix = pix.scaled(sz*aR, sz,Qt::KeepAspectRatio , Qt::SmoothTransformation);

        }
    if (aR>=1)
        {
            if (pix.width()!=0) pix.scaled(sz, sz/aR,Qt::KeepAspectRatio , Qt::SmoothTransformation);
            painter->drawPixmap(
                QRect( rect.left()+5,               rect.top()+sz*(1-1/aR)/2+5+18,     sz,         sz/aR),pix
            );
        }
    else
        {
            if (pix.width()!=0) pix = pix.scaled(sz*aR, sz,Qt::KeepAspectRatio , Qt::SmoothTransformation);
            painter->drawPixmap(
                QRect(rect.left()+5+sz*(1-1*aR)/2,  rect.top()+5+18,                   sz*aR,      sz),pix
            );
        }


}

QSize filePickerDelegate::sizeHint(const QStyleOptionViewItem & option, const QModelIndex & index ) const
{

    QSize result = QStyledItemDelegate::sizeHint(option, index);

    result.setWidth(thumbWidth);
    result.setHeight(thumbWidth+35);
    return result;
}
