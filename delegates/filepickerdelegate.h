#ifndef FILEPICKERDELEGATE_H
#define FILEPICKERDELEGATE_H

#include <QStyledItemDelegate>

class filePickerDelegate : public QStyledItemDelegate
{
public:
    filePickerDelegate();
    //QString libraryFolder;
    //int thumbSize;
    //int parentWidth;
    //QString style;
    //QString filter;
    //QString filterRole;
    QString cacheFolder;
    QString defaultThumb;
    int thumbWidth;

protected:
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;
};

#endif // FILEPICKERDELEGATE_H
