#include "collectionlistdelegate.h"

#include <QApplication>
#include <QImageReader>
#include <QPainter>
#include <QDebug>

collectionListDelegate::collectionListDelegate()
{
}

void collectionListDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    // init style
    QStyleOptionViewItemV4 opt = option;
    initStyleOption(&opt, index);

    // get data from model
    QString line0 = index.model()->data(index.model()->index(index.row(), 0)).toString();
    QString type  = index.model()->data(index.model()->index(index.row(), 1)).toString();
    QString target= index.model()->data(index.model()->index(index.row(), 4)).toString();

    // draw correct background
    opt.text = "";
    QStyle *style = opt.widget ? opt.widget->style() : QApplication::style();
    style->drawControl(QStyle::CE_ItemViewItem, &opt, painter, opt.widget);
    QRect rect = opt.rect;
    QPalette::ColorGroup cg = opt.state & QStyle::State_Enabled ? QPalette::Normal : QPalette::Disabled;
    if (cg == QPalette::Normal && !(opt.state & QStyle::State_Active)) cg = QPalette::Inactive;


    painter->setPen( QColor("#A2A2A2"));
    if (opt.state & QStyle::State_Selected) painter->setPen( QColor("#E7E7E7"));
    QFont font;
    QPixmap p;

    if (type=="smart") p.load(":/icons/icons/notes-1.png");
        else p.load(":/icons/icons/star_off_32.png");

    font.setBold(false);

    if (target!="")
        {
            font.setBold(true);
            p.load(":/icons/icons/star_32.png");
            //font.setItalic(true);
        }


    painter->drawPixmap(rect.left()+2,rect.top()+2,12,12,p.scaled(12,12,Qt::KeepAspectRatio, Qt::SmoothTransformation));

    painter->setFont(font);
    painter->drawText(QRect(rect.left()+18, rect.top(), rect.width(), rect.height()), opt.displayAlignment, line0);

}


QSize collectionListDelegate::sizeHint(const QStyleOptionViewItem & option, const QModelIndex & index ) const
{
    QSize result = QStyledItemDelegate::sizeHint(option, index);
    return result;
}
