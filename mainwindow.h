#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QString>
#include <QtSql>
#include <QCoreApplication>
#include <QSettings>
#include <QSqlQueryModel>
#include <QFileSystemWatcher>
#include <QTimer>
#include <QDir>
#include <QFileSystemModel>
#include <QStandardItemModel>

#include "extensions/overlay.h"

#include "delegates/archivebrowserdelegate.h"
#include "delegates/filepickerdelegate.h"

#include "managers/librarymanager.h"
#include "managers/settingsmanager.h"
#include "managers/uimanager.h"
#include "processmanager.h"
#include "extensions/threadedimageloader.h"
#include "managers/archivemanager.h"

#include "widgets/bargraphwidget.h"
#include "widgets/smoothscrolllistview.h"
#include "widgets/verticalscrolllistview.h"
#include "widgets/linegraphwidget.h"

#include <QSplashScreen>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    Ui::MainWindow *ui;
    void uiInits();

    QCompleter *searchCompleter;


    // manager objects
    libraryManager *libMan;
    settingsManager *setMan;
    uiManager *uiMan;
    archiveManager *archive;
    processManager *process;

    // overlay
    Overlay *overlay;
    int overlayCounter;
    qreal splashOpacity;

    // splash
    QSplashScreen *splash;

    // timers
    QTimer *timer;
    bool timerIsRunning;


    // NEEDS to move or delete
    filePickerDelegate *fDelegate;
    QByteArray defaultIcon;
    QByteArray lastValidBlob64;
    int processIsPano;
    int processSelectedId;
    QModelIndex processSelectedIndex;
    bool processModelsAssigned;
    QString filterOpt;
    archiveBrowserDelegate *fsDelegate;

    void tabSelectByName(QString name);

public slots:



    void openWithPS(QStringList arg);
    void openWithPhotoMatix(QStringList arg);
    void openWithPTGui(QStringList arg);
    void search(QString str);
private slots:

    // OVERLAY
    void showOverlay(QString msg);
    void hideOverlay();

    // SPLASH
    void fadeOutSplash();
    void closeSplash();

    // LIBRARY
    void libraryLoaded();
    void libraryClose();
    void loadFromRecent();

    // DIALOGS
    void optionsDialog();
    void libraryPreferencesDialog();

    // SHORTCUTS
    void shortCutKeyLeft();
    void shortCutKeyRight();
    void shortCutKeyRightProxy();
    void shortCutKeyLeftProxy();
    void proxy_key_D();

    // MISC
    void viewShowToolbarText(bool check);
    void tabSelector();

protected:
    void dropEvent(QDropEvent *event);
    void dragEnterEvent(QDragEnterEvent *event);
private:
    // LIBRARY
    void libraryLoad();
    void loadHistory();
    void libraryAutoLoad();

    // EVENTS
    void resizeEvent(QResizeEvent *event);

    // MISC
    void defaultValues();

    // UI
    void loadSavedUiThings();
    void uiRestore();
    void setWindowTitleAndIcon();
    void initSplash();
    void initOverlay();
    void loadStyleSheet(QString qss);

    // INITS
    void initManagers();
    void connectionsForLibrary();
    void connectionsForMainMenu();
    void connectionsCommon();
    void keyboardInitShortcuts();

    // DELETE
    //void openLocalFile(QString url);
    //void listViewIconMode(QListView *widget);
    //void processRunPhotoMatix(QStringList arg);
    //void processRunPtgui(QStringList arg);
};

#endif // MAINWINDOW_H

