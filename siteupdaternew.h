#ifndef SITEUPDATERNEW_H
#define SITEUPDATERNEW_H

#include "managers/librarymanager.h"

#include <QTime>
#include <QDialog>
#include <QStandardItemModel>
#include <QSqlQueryModel>

namespace Ui {
class siteUpdaterNew;
}

class siteUpdaterNew : public QDialog
{
    Q_OBJECT

public:
    explicit siteUpdaterNew(QWidget *parent = 0);
    ~siteUpdaterNew();

    int allItemCount,currentItem;
    int singleId;
    int finishedCount;
    int counter;
    bool gPlusLoggedin;
    bool disableLoad;
    QString currentUrl;
    QTime *tmr;

    QSqlQueryModel* model;
    QStandardItemModel* tableModel;

    libraryManager* libMan;

    void attachLibman(libraryManager *l);
    void setSingleMode(int id, QString title);
    void modelConvert();
    void setTableColumnWidths();
    void modifyTableCellWbg(int row, int col, QString text, Qt::GlobalColor bgcolor);
    void modifyTableCellWfg(int row, int col, QVariant text, Qt::GlobalColor fgcolor);
public slots:
    void loadSites();
    void loadUrl();
    void fetchData();

    void test1(bool a);
    //void test2(int a);
    void loadDetect(int a);
    void go();
private:
    Ui::siteUpdaterNew *ui;
};

#endif // SITEUPDATERNEW_H
