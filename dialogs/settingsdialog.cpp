#include "settingsdialog.h"
#include "ui_settingsdialog.h"
#include "managers/settingsmanager.h"
#include <QFileDialog>

settingsDialog::settingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::settingsDialog)
{
    ui->setupUi(this);
    this->setWindowTitle("Options");
    this->setWindowIcon(QIcon(":/icons/icons/tools_32.png"));
    settingsManager *setMan = new settingsManager(this);

    ui->lineEdit_photoMatix->setText(setMan->value("ProgramPaths/PhotoMatix"));
    ui->lineEdit_photoShop->setText(setMan->value("ProgramPaths/PhotoShop"));
    ui->lineEdit_ptGui->setText(setMan->value("ProgramPaths/PTGui"));
    ui->lineEdit_archivePath->setText(setMan->value("GeneralSettings/defaultArchivePath"));


    connect ( ui->pushButton_photoMatix     , SIGNAL(clicked()) , this , SLOT(selectPhotoMatix()) );
    connect ( ui->pushButton_photoShop      , SIGNAL(clicked()) , this , SLOT(selectPhotoShop()) );
    connect ( ui->pushButton_ptGui          , SIGNAL(clicked()) , this , SLOT(selectPtGui()) );
    connect ( ui->pushButton_cancel         , SIGNAL(clicked()) , this , SLOT(close()) );
    connect ( ui->pushButton_save           , SIGNAL(clicked()) , this , SLOT(submit()) );
    connect ( ui->pushButton_archivePath    , SIGNAL(clicked()) , this , SLOT(selectArchiveFolder()) );




}

settingsDialog::~settingsDialog()
{
    delete ui;
}

void settingsDialog::attachLibMan(libraryManager *l)
{
    libMan = l;

    QString query = "SELECT id, site, profileUrl,username,password FROM sitePrefs";
    QSqlQueryModel *tmpModel = new QSqlQueryModel(this);
    tmpModel->setQuery(query,libMan->db);

    for (int i=0;i<tmpModel->rowCount();++i)
        {
            if (tmpModel->index(i,1).data().toString()=="500px")
                {
                    ui->lineEdit_URL_500px->setText(tmpModel->index(i,2).data().toString());
                    ui->lineEdit_username_500px->setText(tmpModel->index(i,3).data().toString());
                    ui->lineEdit_password_500px->setText(tmpModel->index(i,4).data().toString());
                }
        }
    tmpModel->clear();
    query = "SELECT value FROM var WHERE KEY='syncIP'";
    tmpModel->setQuery(query,libMan->db);;
    ui->lineEdit_syncIP->setText(tmpModel->index(0,0).data().toString());

    tmpModel->clear();
    query = "SELECT value FROM var WHERE KEY='syncDB'";
    tmpModel->setQuery(query,libMan->db);;
    ui->lineEdit_syncDB->setText(tmpModel->index(0,0).data().toString());

    tmpModel->clear();
    query = "SELECT value FROM var WHERE KEY='syncUSR'";
    tmpModel->setQuery(query,libMan->db);;
    ui->lineEdit_syncUSR->setText(tmpModel->index(0,0).data().toString());

    tmpModel->clear();
    query = "SELECT value FROM var WHERE KEY='syncPASS'";
    tmpModel->setQuery(query,libMan->db);;
    ui->lineEdit_syncPASS->setText(tmpModel->index(0,0).data().toString());

    tmpModel->clear();
    query = "SELECT value FROM var WHERE KEY='scoring'";
    tmpModel->setQuery(query,libMan->db);;
    QString ss = tmpModel->index(0,0).data().toString();
    if (ss=="LNR") ui->comboBox_scoringMode->setCurrentIndex(0);
    if (ss=="SQR") ui->comboBox_scoringMode->setCurrentIndex(1);
    if (ss=="YWG") ui->comboBox_scoringMode->setCurrentIndex(2);

    tmpModel->clear();
    query = "SELECT value FROM var WHERE KEY='browser'";
    tmpModel->setQuery(query,libMan->db);;
    ss = tmpModel->index(0,0).data().toString();
    if (ss=="EXT") ui->comboBox_browser->setCurrentIndex(0);
    if (ss=="INT") ui->comboBox_browser->setCurrentIndex(1);
}

void settingsDialog::submit()
{
    this->path_photoMatix           = ui->lineEdit_photoMatix->text();
    this->path_photoShop            = ui->lineEdit_photoShop->text();
    this->path_ptGui                = ui->lineEdit_ptGui->text();
    this->path_defaultArchiveFolder = ui->lineEdit_archivePath->text();

    QString query = "insert or replace into sitePrefs (id, site, profileUrl,username,password) values  "
                    "((select id from sitePrefs where site = '500px'), '500px' "
                    ", '"+ ui->lineEdit_URL_500px->text() + "' "
                    ", '"+ ui->lineEdit_username_500px->text() + "' "
                    ", '"+ ui->lineEdit_password_500px->text() + "' "
                    " );";
    qDebug() << query;
    libMan->db.exec(query);

    // scoring mode
    QString scrm="";
    if (ui->comboBox_scoringMode->currentText()=="Linear") scrm="LNR";
    if (ui->comboBox_scoringMode->currentText()=="Square root") scrm="SQR";
    if (ui->comboBox_scoringMode->currentText()=="Yearly weighted + square root") scrm="YWG";
    query = "insert or replace into var (id, key, value) values  "
                    "((select id from var where key = 'scoring'), 'scoring', '"+ scrm +"');";
    libMan->db.exec(query);
    scoringMode = scrm; //out

    // urlMode
    QString browser;
    if (ui->comboBox_browser->currentText()=="Default browser") browser="EXT";
    if (ui->comboBox_browser->currentText()=="Internal browser") browser="INT";
    query = "insert or replace into var (id, key, value) values  "
                    "((select id from var where key = 'browser'), 'browser', '"+ browser +"');";
    libMan->db.exec(query);



    // sync settings
    query = "insert or replace into var (id, key, value) values  "
                    "((select id from var where key = 'syncIP'), 'syncIP', '"+ ui->lineEdit_syncIP->text() +"');";
    libMan->db.exec(query);

    query = "insert or replace into var (id, key, value) values  "
                    "((select id from var where key = 'syncDB'), 'syncDB', '"+ ui->lineEdit_syncDB->text() +"');";
    libMan->db.exec(query);

    query = "insert or replace into var (id, key, value) values  "
                    "((select id from var where key = 'syncUSR'), 'syncUSR', '"+ ui->lineEdit_syncUSR->text() +"');";
    libMan->db.exec(query);

    query = "insert or replace into var (id, key, value) values  "
                    "((select id from var where key = 'syncPASS'), 'syncPASS', '"+ ui->lineEdit_syncPASS->text() +"');";
    libMan->db.exec(query);

    accept();
}

void settingsDialog::selectArchiveFolder()
{
     QString dirName = QFileDialog::getExistingDirectory(this,tr("Open Directory"));
     ui->lineEdit_archivePath->setText(dirName);
}

void settingsDialog::selectPhotoMatix()
{
     QString filePath = QFileDialog::getOpenFileName(0, tr("Open File"),"",tr("Files (PhotomatixPro.exe)"));
     ui->lineEdit_photoMatix->setText(filePath);
}

void settingsDialog::selectPhotoShop()
{
     QString filePath = QFileDialog::getOpenFileName(0, tr("Open File"),"",tr("Files (*.exe)"));
     ui->lineEdit_photoShop->setText(filePath);
}

void settingsDialog::selectPtGui()
{
     QString filePath = QFileDialog::getOpenFileName(0, tr("Open File"),"",tr("Files (PTGui.exe)"));
     ui->lineEdit_ptGui->setText(filePath);
}


