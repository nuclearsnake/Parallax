#include "dialogs\droppedselectdialog.h"
#include "ui_droppedselectdialog.h"

droppedSelectDialog::droppedSelectDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::droppedSelectDialog)
{
    ui->setupUi(this);

    connect( ui->pushButton_Cancel  , SIGNAL(clicked()) , this  , SLOT(close()) );
    connect( ui->pushButton_Accept  , SIGNAL(clicked()) , this  ,SLOT(submit()) );
}

droppedSelectDialog::~droppedSelectDialog()
{
    delete ui;
}

void droppedSelectDialog::setFolder(QString folder)
{
    ui->label->setText(folder);
}

void droppedSelectDialog::submit()
{
    if (ui->radioButton_1->isChecked()) result = "PROCESS";
    if (ui->radioButton_2->isChecked()) result = "ARCHIVE";
    this->accept();
}
