#ifndef TABLEBROWSERDIALOG_H
#define TABLEBROWSERDIALOG_H

#include <QDialog>
#include <QSqlDatabase>
#include <QSqlQueryModel>
#include "managers/librarymanager.h"


namespace Ui {
class tableBrowserDialog;
}

class tableBrowserDialog : public QDialog
{
    Q_OBJECT

public:
    explicit tableBrowserDialog(QWidget *parent = 0);
    ~tableBrowserDialog();
    QSqlDatabase db;
    QSqlQueryModel* dataModel;
    QString selecedTitle;
    QString module;
    QString param;
    libraryManager *libMan;

    void setDb(QSqlDatabase database);
    void setModule(QString m);
    void setParam(QString p);
private slots:
    void notesBrowser();
    void lineDblClicked(QModelIndex index);
    void runModule();
private:
    Ui::tableBrowserDialog *ui;
    void queryBased(QString query);
    void folderSizes();
};

#endif // TABLEBROWSERDIALOG_H
