#ifndef CALENDARDIALOG_H
#define CALENDARDIALOG_H

#include <QDialog>
#include <QDate>

namespace Ui {
class calendarDialog;
}

class calendarDialog : public QDialog
{
    Q_OBJECT

public:
    explicit calendarDialog(QWidget *parent = 0);
    QDate date;
    ~calendarDialog();


private slots:
    void submit();
private:
    Ui::calendarDialog *ui;
};

#endif // CALENDARDIALOG_H
