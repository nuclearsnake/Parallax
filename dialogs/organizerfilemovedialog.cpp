#include "organizerfilemovedialog.h"
#include "ui_organizerfilemovedialog.h"
#include "organizerduplicatefiledialog.h"

#include <QDebug>
#include <QFileInfo>
#include <QDir>
#include <QTimer>

organizerFileMoveDialog::organizerFileMoveDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::organizerFileMoveDialog)
{
    //this->setWindowModality(Qt::NonModal);
    ui->setupUi(this);
    ui->pushButton_close->setEnabled(false);
    //handler = new QFile();
    handlerTo = new QFile(this);
    handlerFrom = new QFile(this);
    connect ( ui->pushButton_close  , SIGNAL(clicked()) , this , SLOT(accept()) );
    //connect ( handler               , SIGNAL(handler.bytesWritten(qint64)) , this , SLOT(updateProgress(qint64)) );
    connect ( handlerTo , SIGNAL(bytesWritten(qint64)) , this , SLOT(updateProgress(qint64)) );
    //connect ( ui->pushButton_go     , SIGNAL(clicked()), this , SLOT(runMove()) );
    //connect ( parent , SIGNAL(updateMove(QString,QString,int,int)) , this ,  )

    //this->runMove();

    QTimer *timer=new QTimer(this);
    timer->singleShot(1000,this,SLOT(runMove()));
}

void organizerFileMoveDialog::updateProgress(qint64 copyed)
{
    //ui->progressBar
    qDebug() << "PROOOGRESSSS..............................";
    ui->progressBar_file->setValue( 100*copyed/handler.size() );
    qDebug() << copyed ;
}

void organizerFileMoveDialog::updateStatus(QString from , QString to , int currentItem , int allItems)
{
    ui->label_from->setText(from);
    ui->label_to->setText(to);
    ui->progressBar->setValue( 100 * currentItem / allItems );
}

void organizerFileMoveDialog::showClose()
{
    ui->pushButton_close->setEnabled(true);
}

void organizerFileMoveDialog::setMoveList(QList<QPair<QString,QString> > list)
{
    qDebug() << "SETMOVELIST";
    moveList=list;
}

void organizerFileMoveDialog::runMove()
{
    qDebug() << "RUNMOVE";
    int filesToMove = moveList.count();
    qDebug() << filesToMove;
    for (int i=0;i<filesToMove;++i)
        {
            //ui->la
            ui->label_counter->setText( QString::number(i+1) + " / " + QString::number(filesToMove) );
            qDebug() << i << "/" << filesToMove;
            QString from = moveList.at(i).first;
            QString to   = moveList.at(i).second;
            QString fileName = from.split("/").last();
            ui->label_file->setText(fileName);
            QString labelHold;
            labelHold = from; ui->label_from->setText(labelHold.replace(fileName,""));
            labelHold = to; ui->label_to->setText(labelHold.replace(fileName,""));
            ui->progressBar->setValue( 100 * (i+1) / filesToMove );
            QFileInfo fi(to);
            QString targetPath= fi.absolutePath();
            QDir di(targetPath);
            bool targetExists = di.exists();
            bool targetCreated ;
            if (!targetExists)   targetCreated = di.mkpath(targetPath);
            qDebug() << "from      :" << from;
            qDebug() << "to        :" << to;
            qDebug() << "tagetPath :" << targetPath;
            qDebug() << "target path ok :" << targetExists;
            qDebug() << "taget created  :" << targetCreated;
            //QDir handler;
            //bool success = handler.rename(from,to);
            //QFile handler;
            //bool success = handler.copy(from,to);
            handlerFrom->setFileName(from);
            handlerTo->setFileName(to);
            //** copy method SLOW
            //bool success = handlerFrom->copy(to);
            //if (success) handler.remove(from);
            //
            bool success = handlerFrom->rename(to);
            if (!success)
                {
                    qDebug() << "DUPLICATE--------->";
                    qDebug() << "from      :" << from;
                    qDebug() << "to        :" << to;
                    organizerDuplicateFileDialog dupDialog;
                    dupDialog.setFiles(from,to);
                    if (dupDialog.exec())
                        {
                            handler.remove(from);
                        }
                        else
                        {
                            handler.remove(to);
                            handler.copy(from,to);
                            handler.remove(from);
                        }
                }
            qDebug() << "success:" << success;

            QCoreApplication::processEvents();

            //updateStatus(from,to,i+1,filesToMove);
        }
    ui->pushButton_close->setEnabled(true);
    this->accept();
}

organizerFileMoveDialog::~organizerFileMoveDialog()
{
    delete ui;
}
