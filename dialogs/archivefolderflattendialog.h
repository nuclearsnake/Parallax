#ifndef ARCHIVEFOLDERFLATTENDIALOG_H
#define ARCHIVEFOLDERFLATTENDIALOG_H

#include <QDialog>
#include <QFileSystemModel>
#include <QStandardItemModel>

namespace Ui {
class archiveFolderFlattenDialog;
}

class archiveFolderFlattenDialog : public QDialog
{
    Q_OBJECT

public:
    explicit archiveFolderFlattenDialog(QWidget *parent = 0);
    ~archiveFolderFlattenDialog();
    QString rootFolder;

    void log(QString s);
public slots:
    void go();
private:
    Ui::archiveFolderFlattenDialog *ui;
};

#endif // ARCHIVEFOLDERFLATTENDIALOG_H
