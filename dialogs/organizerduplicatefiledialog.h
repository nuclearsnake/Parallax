#ifndef ORGANIZERDUPLICATEFILEDIALOG_H
#define ORGANIZERDUPLICATEFILEDIALOG_H

#include <QDialog>
#include <QTimer>

namespace Ui {
class organizerDuplicateFileDialog;
}

class organizerDuplicateFileDialog : public QDialog
{
    Q_OBJECT

public:
    explicit organizerDuplicateFileDialog(QWidget *parent = 0);
    ~organizerDuplicateFileDialog();
    QString fileFrom,fileTo;
    //QTimer *timer;
    void setFiles(QString from, QString to);
private slots:
    void init();
    //void updateProgress(qint64 copyed);
private:
    Ui::organizerDuplicateFileDialog *ui;
};

#endif // ORGANIZERDUPLICATEFILEDIALOG_H
