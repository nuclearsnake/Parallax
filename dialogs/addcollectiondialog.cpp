#include "addcollectiondialog.h"
#include "ui_addcollectiondialog.h"

addCollectionDialog::addCollectionDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addCollectionDialog)
{
    ui->setupUi(this);

    connect ( ui->pushButton_add            , SIGNAL(clicked())     , this  , SLOT(submit())  );
    connect ( ui->pushButton_cancel         , SIGNAL(clicked())     , this  , SLOT(close()) );
    connect ( ui->radioButton_itemBased     , SIGNAL(clicked(bool)) , this  , SLOT(setType(bool)) );
    connect ( ui->radioButton_smart         , SIGNAL(clicked(bool)) , this  , SLOT(setType(bool)) );





}

void addCollectionDialog::setEditMode(QString name,QString type,QString text)
{
    ui->pushButton_add->setText("Save");
    ui->lineEdit_title->setText(name);
    ui->lineEdit_text->setText(text);
    if (type=="smart") ui->radioButton_smart->setChecked(true);
    if (type=="item")  ui->radioButton_itemBased->setChecked(true);
}

void addCollectionDialog::setType(bool checked)
{
    if ( ( sender()->objectName()=="radioButton_itemBased" ) & ( checked ) ) {  ui->lineEdit_text->setText(""); ui->lineEdit_text->setEnabled(false); }
    if ( ( sender()->objectName()=="radioButton_smart"     ) & ( checked ) ) {  ui->lineEdit_text->setEnabled(true); }

}

void addCollectionDialog::submit()
{
    this->name = ui->lineEdit_title->text();
    this->param = ui->lineEdit_text->text();

    if (ui->radioButton_smart->isChecked() )    this->type="smart" ;
    if (ui->radioButton_itemBased->isChecked()) this->type="item";


    accept();
}

addCollectionDialog::~addCollectionDialog()
{
    delete ui;
}
