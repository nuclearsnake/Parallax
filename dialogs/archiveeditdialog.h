#ifndef ARCHIVEEDITDIALOG_H
#define ARCHIVEEDITDIALOG_H

#include <QDialog>

namespace Ui {
class archiveEditDialog;
}

class archiveEditDialog : public QDialog
{
    Q_OBJECT

public:
    explicit archiveEditDialog(QWidget *parent = 0);
    ~archiveEditDialog();

private:
    Ui::archiveEditDialog *ui;
};

#endif // ARCHIVEEDITDIALOG_H
