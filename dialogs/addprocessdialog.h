#ifndef ADDPROCESSDIALOG_H
#define ADDPROCESSDIALOG_H

#include <QDialog>
#include <QWidget>

namespace Ui {
class addProcessDialog;
}

class addProcessDialog : public QDialog
{
    Q_OBJECT

public:
    explicit addProcessDialog(QWidget *parent = 0);
    QString name;
    QString path;
    int panoMode;

    ~addProcessDialog();

    void editMode(QString folder, QString title, int isPano);
    void preSelect(QString f);
private slots:
    void submit();
    void selectFolder();
private:
    Ui::addProcessDialog *ui;
};

#endif // ADDPROCESSDIALOG_H
