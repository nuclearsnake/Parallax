#include "uploadtimesdialog.h"
#include "ui_uploadtimesdialog.h"

#include "extensions/clickable_qlabel.h"
#include "extensions/tools.h"


uploadTimesDialog::uploadTimesDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::uploadTimesDialog)
{
    ui->setupUi(this);

    connect( ui->pushButton_close       , SIGNAL(clicked(bool))     , this      , SLOT(close())     );
    connect( ui->pushButton_get         , SIGNAL(clicked(bool))     , this      , SLOT(go())        );


    connectLabels();


    clearMatrix();
    int today = QDate::currentDate().dayOfWeek() ;

    QFont font = ui->label_d1->font();
    font.setBold(true);


    if (today==1) ui->label_d1->setFont(font);
    if (today==2) {ui->label_d2->setFont(font); ui->label_d2->setStyleSheet("QLabel{color:white}"); }
    if (today==3) ui->label_d3->setFont(font);
    if (today==4) ui->label_d4->setFont(font);
    if (today==5) ui->label_d5->setFont(font);
    if (today==6) ui->label_d6->setFont(font);
    if (today==7) ui->label_d7->setFont(font);
}

void uploadTimesDialog::labelClick()
{
    qDebug() << "click";
    qDebug() << sender()->objectName();
    QString sndr = sender()->objectName();
    QString day  = sndr.midRef( 7,1).toString();
    QString hour = sndr.midRef(10,2).toString();

    // da 1 hour minus workaround

    int houri=hour.toInt();
    int dayi=day.toInt();

    if (ui->comboBox_site->currentText()=="DeviantArt")
    {
        houri--;
        if (houri<0)
        {
            houri=23;
            dayi--;
            if (dayi<1) dayi=7;
        }

        day=QString::number(dayi);
        hour=QString::number(houri);
        if (houri<10) hour = "0"+hour;
    }



    if (day=="7") day="0";


    qDebug() << "day=" << day << " hour=" << hour;
    QString siteQuery = "";
    QString orderQuery="ORDER BY views DESC ";
    if (ui->comboBox_site->currentText()=="500PX")      siteQuery=" AND s.site='5P' ";
    if (ui->comboBox_site->currentText()=="DeviantArt") siteQuery=" AND s.site='DA' ";
    if (ui->comboBox_site->currentText()=="Flickr")     siteQuery=" AND s.site='FL' ";
    if (ui->comboBox_site->currentText()=="Facebook")   siteQuery=" AND s.site='FB' ";
    if (ui->comboBox_site->currentText()=="Youpic")     siteQuery=" AND s.site='YP' ";
    if (ui->comboBox_site->currentText()=="Facebook") orderQuery=" ORDER BY favs DESC ";
    QString dateQuery = "";

    if (ui->comboBox_time->currentText()=="This Year")      dateQuery = " AND s.date LIKE '2016%' ";
    if (ui->comboBox_time->currentText()=="Previous Year")  dateQuery = " AND s.date LIKE '2015%' ";

    QString query=  " SELECT a.title,a.image,s.views,s.favs,s.date,s.time FROM sites s "
                    " LEFT JOIN archive a ON a.id=s.projectid "
                    " WHERE cast (strftime('%w', s.date) as integer) =  " + day +
                    " AND time LIKE '"+hour+"%' "
                    + siteQuery + dateQuery + orderQuery +
                    " "
            ;
    qDebug() << query;

    QString info = "";


    QSqlQueryModel *model = new QSqlQueryModel(this);
    model->setQuery(query,libMan->db);
    while(model->canFetchMore()) model->fetchMore();

    if (model->rowCount()>1) info += QString::number(model->rowCount()) + " uploads in ";
        else info += QString::number(model->rowCount()) + " upload in ";

    if (day=="0") info+="Sunday";
    if (day=="1") info+="Monday";
    if (day=="2") info+="Tuesday";
    if (day=="3") info+="Wednesday";
    if (day=="4") info+="Thursday";
    if (day=="5") info+="Friday";
    if (day=="6") info+="Saturday";

    info+=" at "+hour;
    int cnt=0;
    int sum=0;
    if (model->rowCount())
    {
    for (int i=0;i<model->rowCount();++i)
    {
        cnt++;
        sum+=model->record(i).value("views").toInt();
    }
    info+=" with "+ tools::numGrouped(sum/cnt) +" average views.";
    }
    ui->label_selectedInfo->setText(info);


    ui->plainTextEdit->clear();

    ui->label_top_line1_1->clear();
    ui->label_top_line2_1->clear();
    ui->label_top_line1_2->clear();
    ui->label_top_line2_2->clear();
    ui->label_top_line1_3->clear();
    ui->label_top_line2_3->clear();

    ui->label_top_pix_1->clear();
    ui->label_top_pix_2->clear();
    ui->label_top_pix_3->clear();


    for (int i=0;i<model->rowCount();++i)
    {

        qDebug() << libMan->libraryFolder+"previews/"+model->record(i).value("image").toString();
        QString path = libMan->libraryFolder+"previews/"+model->record(i).value("image").toString();

        log(
                      model->record(i).value("title").toString() + "     "
                    + model->record(i).value("image").toString() + " "
                    + model->record(i).value("views").toString() + "     "
                    + model->record(i).value("favs").toString()  + " " " "
                    + model->record(i).value("date").toString()  + " " " "
                    + model->record(i).value("time").toString()  + "     "
            );

        if (i==0)
        {
            int w = ui->label_top_pix_1->width();
            int h = ui->label_top_pix_1->height();
            QPixmap p(path);
            ui->label_top_pix_1->setPixmap(p.scaled(w,h,Qt::KeepAspectRatio,Qt::SmoothTransformation));

            ui->label_top_line1_1->setText(model->record(i).value("title").toString());
            ui->label_top_line2_1->setText(""+ tools::numGrouped( model->record(i).value("views").toInt())
                                           +" / "+tools::numGrouped(model->record(i).value("favs").toInt())
                                           +"    "+model->record(i).value("date").toString()
                                           +"    "+model->record(i).value("time").toString()

                        );
        }
        if (i==1)
        {
            int w = ui->label_top_pix_2->width();
            int h = ui->label_top_pix_2->height();
            QPixmap p(path);
            ui->label_top_pix_2->setPixmap(p.scaled(w,h,Qt::KeepAspectRatio,Qt::SmoothTransformation));

            ui->label_top_line1_2->setText(model->record(i).value("title").toString());
            ui->label_top_line2_2->setText(""+ tools::numGrouped( model->record(i).value("views").toInt())
                                           +" / "+tools::numGrouped(model->record(i).value("favs").toInt())
                                           +"    "+model->record(i).value("date").toString()
                                           +"    "+model->record(i).value("time").toString()

                        );
        }
        if (i==2)
        {
            int w = ui->label_top_pix_3->width();
            int h = ui->label_top_pix_3->height();
            QPixmap p(path);
            ui->label_top_pix_3->setPixmap(p.scaled(w,h,Qt::KeepAspectRatio,Qt::SmoothTransformation));

            ui->label_top_line1_3->setText(model->record(i).value("title").toString());
            ui->label_top_line2_3->setText(""+ tools::numGrouped( model->record(i).value("views").toInt())
                                          +" / "+tools::numGrouped(model->record(i).value("favs").toInt())
                                          +"    "+model->record(i).value("date").toString()
                                          +"    "+model->record(i).value("time").toString()

                        );
        }


    }



}

void uploadTimesDialog::attachLibman(libraryManager *l)
{
    libMan = l;
}

void uploadTimesDialog::go()
{

    ui->plainTextEdit->clear();
    clearMatrix();

    ui->label_top_line1_1->clear();
    ui->label_top_line2_1->clear();
    ui->label_top_line1_2->clear();
    ui->label_top_line2_2->clear();
    ui->label_top_line1_3->clear();
    ui->label_top_line2_3->clear();

    ui->label_top_pix_1->clear();
    ui->label_top_pix_2->clear();
    ui->label_top_pix_3->clear();

    ui->label_selectedInfo->clear();

    /*
    ui->label->setStyleSheet("QLabel { background-color : rgb(255,0,0); }");
    ui->label_2->setStyleSheet("QLabel { background-color : rgb(255,255,0); }");
    ui->label_3->setStyleSheet("QLabel { background-color : rgb(0,255,0); }");
    ui->label_4->setStyleSheet("QLabel { background-color : rgb(255,128,0); }");
    ui->label_5->setStyleSheet("QLabel { background-color : rgb(128,255,0); }");
    */

    QString dateQuery = "";
    QString score1Query = " favs as score1 ";
    if (ui->comboBox_time->currentText()=="This Year")      dateQuery = " AND date LIKE '2016%' ";
    if (ui->comboBox_time->currentText()=="Previous Year")  dateQuery = " AND date LIKE '2015%' ";
    QString siteQuery = "";
    if (ui->comboBox_site->currentText()=="500PX")      siteQuery="AND site='5P'";
    if (ui->comboBox_site->currentText()=="DeviantArt") siteQuery="AND site='DA'";
    if (ui->comboBox_site->currentText()=="Flickr")     siteQuery="AND site='FL'";
    if (ui->comboBox_site->currentText()=="Facebook")   siteQuery="AND site='FB'";
    if (ui->comboBox_site->currentText()=="Youpic")     siteQuery="AND site='YP'";
    if (ui->comboBox_site->currentText()=="Facebook")   score1Query=" favs as score1 ";

    QSqlQueryModel *model = new QSqlQueryModel(this);

    QString query=
            " SELECT t2.daynum day,t2.hour hour,/*t2.napora napora,*/  ROUND(AVG(t2.score),1)  score, ROUND(AVG(t2.score1),0)  score1,COUNT(*) count "
            " FROM                                                                                            "
            " (                                                                                               "
            "   SELECT t1.daynum daynum,t1.hour,t1.nap nap,t1.nap || '-' || t1.hour napora, t1.score score,t1.score1 score1 FROM       "
            "   (                                                                                             "
            "     SELECT                                                                                      "
            "     date,                                                                                       "
            "     cast (strftime('%w', date) as integer) daynum,                                              "
            "     CASE cast (strftime('%w', date) as integer)+1                                               "
            "      WHEN 1 THEN 'Vasárnap'                                                                     "
            "      WHEN 2 THEN 'Hétfõ'                                                                        "
            "      WHEN 3 THEN 'Kedd'                                                                         "
            "      WHEN 4 THEN 'Szerda'                                                                       "
            "      WHEN 5 THEN 'Csütörtök'                                                                    "
            "      WHEN 6 THEN 'Péntek'                                                                       "
            "      WHEN 7 THEN 'Szombat'                                                                      "
            "     END as nap,                                                                                 "
            "     time,                                                                                       "
            "     SUBSTR(time,1,2) as hour,                                                                   "
            "     views,                                                                                      "
            "     favs,                                                                                       "
            "     option1 as score,                                                                           "
            + score1Query +
            //"     views as score1                                                                             "
            "     FROM `sites`                                                                                "
            "     WHERE time!='' /* AND SUBSTRING(time,1,2) = 23*/                                              "
            "     AND time NOT LIKE '25%'                                                                     "
            "     AND notcount IS NULL                                                                     "
            + siteQuery
            + dateQuery +
            "   ) t1                                                                                          "
            " /* WHERE t1.nap='Hétfõ'   */                                                                    "
            " ) t2                                                                                            "
            " GROUP BY t2.daynum,t2.hour                                                                      "
            " ORDER BY AVG(t2.score1) DESC                                                                    ";

    //query="SELECT * FROM sites ";
    //log(query);
    qDebug() << query;

    model->setQuery(query,libMan->db);
    while(model->canFetchMore()) model->fetchMore();

    log("Start");
    log(query);
    log(QString::number(model->rowCount()));

    maxValue=0;
    minValue=10000000;

    int d1avg =0 , d1cnt = 0 , d1sum = 0;
    int d2avg =0 , d2cnt = 0 , d2sum = 0;
    int d3avg =0 , d3cnt = 0 , d3sum = 0;
    int d4avg =0 , d4cnt = 0 , d4sum = 0;
    int d5avg =0 , d5cnt = 0 , d5sum = 0;
    int d6avg =0 , d6cnt = 0 , d6sum = 0;
    int d7avg =0 , d7cnt = 0 , d7sum = 0;

    int h00avg =0 , h00cnt = 0 , h00sum = 0;
    int h01avg =0 , h01cnt = 0 , h01sum = 0;
    int h02avg =0 , h02cnt = 0 , h02sum = 0;
    int h03avg =0 , h03cnt = 0 , h03sum = 0;
    int h04avg =0 , h04cnt = 0 , h04sum = 0;
    int h05avg =0 , h05cnt = 0 , h05sum = 0;
    int h06avg =0 , h06cnt = 0 , h06sum = 0;
    int h07avg =0 , h07cnt = 0 , h07sum = 0;
    int h08avg =0 , h08cnt = 0 , h08sum = 0;
    int h09avg =0 , h09cnt = 0 , h09sum = 0;
    int h10avg =0 , h10cnt = 0 , h10sum = 0;
    int h11avg =0 , h11cnt = 0 , h11sum = 0;
    int h12avg =0 , h12cnt = 0 , h12sum = 0;
    int h13avg =0 , h13cnt = 0 , h13sum = 0;
    int h14avg =0 , h14cnt = 0 , h14sum = 0;
    int h15avg =0 , h15cnt = 0 , h15sum = 0;
    int h16avg =0 , h16cnt = 0 , h16sum = 0;
    int h17avg =0 , h17cnt = 0 , h17sum = 0;
    int h18avg =0 , h18cnt = 0 , h18sum = 0;
    int h19avg =0 , h19cnt = 0 , h19sum = 0;
    int h20avg =0 , h20cnt = 0 , h20sum = 0;
    int h21avg =0 , h21cnt = 0 , h21sum = 0;
    int h22avg =0 , h22cnt = 0 , h22sum = 0;
    int h23avg =0 , h23cnt = 0 , h23sum = 0;


    for (int i=0;i<model->rowCount();++i)
    {
        if (model->record(i).value("score1").toInt() > maxValue) maxValue = model->record(i).value("score1").toInt();
        if (model->record(i).value("score1").toInt() < minValue) minValue = model->record(i).value("score1").toInt();

        int v = model->record(i).value("score1").toInt();

        int day=model->record(i).value("day").toInt();
        int hour= model->record(i).value("hour").toInt();

        // da 1 hour minus workaround

        if (ui->comboBox_site->currentText()=="DeviantArt")
        {
            hour++;
            if (hour>23)
            {
                hour=0;
                day++;
                if (day==7) day=0;
            }

        }

        if (day == 1) { d1cnt++; d1sum+=v; }
        if (day == 2) { d2cnt++; d2sum+=v; }
        if (day == 3) { d3cnt++; d3sum+=v; }
        if (day == 4) { d4cnt++; d4sum+=v; }
        if (day == 5) { d5cnt++; d5sum+=v; }
        if (day == 6) { d6cnt++; d6sum+=v; }
        if (day == 0) { d7cnt++; d7sum+=v; }

        if (hour ==  0) { h00cnt++; h00sum+=v; }
        if (hour ==  1) { h01cnt++; h01sum+=v; }
        if (hour ==  2) { h02cnt++; h02sum+=v; }
        if (hour ==  3) { h03cnt++; h03sum+=v; }
        if (hour ==  4) { h04cnt++; h04sum+=v; }
        if (hour ==  5) { h05cnt++; h05sum+=v; }
        if (hour ==  6) { h06cnt++; h06sum+=v; }
        if (hour ==  7) { h07cnt++; h07sum+=v; }
        if (hour ==  8) { h08cnt++; h08sum+=v; }
        if (hour ==  9) { h09cnt++; h09sum+=v; }
        if (hour == 10) { h10cnt++; h10sum+=v; }
        if (hour == 11) { h11cnt++; h11sum+=v; }
        if (hour == 12) { h12cnt++; h12sum+=v; }
        if (hour == 13) { h13cnt++; h13sum+=v; }
        if (hour == 14) { h14cnt++; h14sum+=v; }
        if (hour == 15) { h15cnt++; h15sum+=v; }
        if (hour == 16) { h16cnt++; h16sum+=v; }
        if (hour == 17) { h17cnt++; h17sum+=v; }
        if (hour == 18) { h18cnt++; h18sum+=v; }
        if (hour == 19) { h19cnt++; h19sum+=v; }
        if (hour == 20) { h20cnt++; h20sum+=v; }
        if (hour == 21) { h21cnt++; h21sum+=v; }
        if (hour == 22) { h22cnt++; h22sum+=v; }
        if (hour == 23) { h23cnt++; h23sum+=v; }
    }

    if (d1cnt) d1avg=d1sum/d1cnt;
    if (d2cnt) d2avg=d2sum/d2cnt;
    if (d3cnt) d3avg=d3sum/d3cnt;
    if (d4cnt) d4avg=d4sum/d4cnt;
    if (d5cnt) d5avg=d5sum/d5cnt;
    if (d6cnt) d6avg=d6sum/d6cnt;
    if (d7cnt) d7avg=d7sum/d7cnt;

    if (h00cnt) h00avg=h00sum/h00cnt;
    if (h01cnt) h01avg=h01sum/h01cnt;
    if (h02cnt) h02avg=h02sum/h02cnt;
    if (h03cnt) h03avg=h03sum/h03cnt;
    if (h04cnt) h04avg=h04sum/h04cnt;
    if (h05cnt) h05avg=h05sum/h05cnt;
    if (h06cnt) h06avg=h06sum/h06cnt;
    if (h07cnt) h07avg=h07sum/h07cnt;
    if (h08cnt) h08avg=h08sum/h08cnt;
    if (h09cnt) h09avg=h09sum/h09cnt;
    if (h10cnt) h10avg=h10sum/h10cnt;
    if (h11cnt) h11avg=h11sum/h11cnt;
    if (h12cnt) h12avg=h12sum/h12cnt;
    if (h13cnt) h13avg=h13sum/h13cnt;
    if (h14cnt) h14avg=h14sum/h14cnt;
    if (h15cnt) h15avg=h15sum/h15cnt;
    if (h16cnt) h16avg=h16sum/h16cnt;
    if (h17cnt) h17avg=h17sum/h17cnt;
    if (h18cnt) h18avg=h18sum/h18cnt;
    if (h19cnt) h19avg=h19sum/h19cnt;
    if (h20cnt) h20avg=h20sum/h20cnt;
    if (h21cnt) h21avg=h21sum/h21cnt;
    if (h22cnt) h22avg=h22sum/h22cnt;
    if (h23cnt) h23avg=h23sum/h23cnt;


    int maxd=0;
    int mind=1000000000;

    if (d1avg>maxd) maxd=d1avg;
    if (d2avg>maxd) maxd=d2avg;
    if (d3avg>maxd) maxd=d3avg;
    if (d4avg>maxd) maxd=d4avg;
    if (d5avg>maxd) maxd=d5avg;
    if (d6avg>maxd) maxd=d6avg;
    if (d7avg>maxd) maxd=d7avg;

    if (d1avg<mind) mind=d1avg;
    if (d2avg<mind) mind=d2avg;
    if (d3avg<mind) mind=d3avg;
    if (d4avg<mind) mind=d4avg;
    if (d5avg<mind) mind=d5avg;
    if (d6avg<mind) mind=d6avg;
    if (d7avg<mind) mind=d7avg;

    int maxh=0;
    int minh=1000000000;

    if (h00avg>maxh) maxh=h00avg;
    if (h01avg>maxh) maxh=h01avg;
    if (h02avg>maxh) maxh=h02avg;
    if (h03avg>maxh) maxh=h03avg;
    if (h04avg>maxh) maxh=h04avg;
    if (h05avg>maxh) maxh=h05avg;
    if (h06avg>maxh) maxh=h06avg;
    if (h07avg>maxh) maxh=h07avg;
    if (h08avg>maxh) maxh=h08avg;
    if (h09avg>maxh) maxh=h09avg;
    if (h10avg>maxh) maxh=h10avg;
    if (h11avg>maxh) maxh=h11avg;
    if (h12avg>maxh) maxh=h12avg;
    if (h13avg>maxh) maxh=h13avg;
    if (h14avg>maxh) maxh=h14avg;
    if (h15avg>maxh) maxh=h15avg;
    if (h16avg>maxh) maxh=h16avg;
    if (h17avg>maxh) maxh=h17avg;
    if (h18avg>maxh) maxh=h18avg;
    if (h19avg>maxh) maxh=h19avg;
    if (h20avg>maxh) maxh=h20avg;
    if (h21avg>maxh) maxh=h21avg;
    if (h22avg>maxh) maxh=h22avg;
    if (h23avg>maxh) maxh=h23avg;

    if (h00avg<minh) minh=h00avg;
    if (h01avg<minh) minh=h01avg;
    if (h02avg<minh) minh=h02avg;
    if (h03avg<minh) minh=h03avg;
    if (h04avg<minh) minh=h04avg;
    if (h05avg<minh) minh=h05avg;
    if (h06avg<minh) minh=h06avg;
    if (h07avg<minh) minh=h07avg;
    if (h08avg<minh) minh=h08avg;
    if (h09avg<minh) minh=h09avg;
    if (h10avg<minh) minh=h10avg;
    if (h11avg<minh) minh=h11avg;
    if (h12avg<minh) minh=h12avg;
    if (h13avg<minh) minh=h13avg;
    if (h14avg<minh) minh=h14avg;
    if (h15avg<minh) minh=h15avg;
    if (h16avg<minh) minh=h16avg;
    if (h17avg<minh) minh=h17avg;
    if (h18avg<minh) minh=h18avg;
    if (h19avg<minh) minh=h19avg;
    if (h20avg<minh) minh=h20avg;
    if (h21avg<minh) minh=h21avg;
    if (h22avg<minh) minh=h22avg;
    if (h23avg<minh) minh=h23avg;


    qDebug() << "D" << maxd << mind;

    log( "MAX="+ QString::number(maxValue));
    log( "MIN="+ QString::number(minValue));

    for (int i=0;i<model->rowCount();++i)
    {
        log( model->record(i).value("day").toString()
             + " - " + model->record(i).value("hour").toString()
             + "  " + model->record(i).value("score1").toString()
             );
        int day = model->record(i).value("day").toInt();
        int hour= model->record(i).value("hour").toInt();
        QString value = model->record(i).value("score1").toString();

        // da 1 hour minus workaround

        if (ui->comboBox_site->currentText()=="DeviantArt")
        {
            hour++;
            if (hour>23)
            {
                hour=0;
                day++;
                if (day==7) day=0;
            }

        }

        // hétfő (1)
        if (day==1 && hour== 0) { ui->label_d1_h00->setText(value); ui->label_d1_h00->setStyleSheet(calColor(value.toInt())); }
        if (day==1 && hour== 1) { ui->label_d1_h01->setText(value); ui->label_d1_h01->setStyleSheet(calColor(value.toInt())); }
        if (day==1 && hour== 2) { ui->label_d1_h02->setText(value); ui->label_d1_h02->setStyleSheet(calColor(value.toInt())); }
        if (day==1 && hour== 3) { ui->label_d1_h03->setText(value); ui->label_d1_h03->setStyleSheet(calColor(value.toInt())); }
        if (day==1 && hour== 4) { ui->label_d1_h04->setText(value); ui->label_d1_h04->setStyleSheet(calColor(value.toInt())); }
        if (day==1 && hour== 5) { ui->label_d1_h05->setText(value); ui->label_d1_h05->setStyleSheet(calColor(value.toInt())); }
        if (day==1 && hour== 6) { ui->label_d1_h06->setText(value); ui->label_d1_h06->setStyleSheet(calColor(value.toInt())); }
        if (day==1 && hour== 7) { ui->label_d1_h07->setText(value); ui->label_d1_h07->setStyleSheet(calColor(value.toInt())); }
        if (day==1 && hour== 8) { ui->label_d1_h08->setText(value); ui->label_d1_h08->setStyleSheet(calColor(value.toInt())); }
        if (day==1 && hour== 9) { ui->label_d1_h09->setText(value); ui->label_d1_h09->setStyleSheet(calColor(value.toInt())); }
        if (day==1 && hour==10) { ui->label_d1_h10->setText(value); ui->label_d1_h10->setStyleSheet(calColor(value.toInt())); }
        if (day==1 && hour==11) { ui->label_d1_h11->setText(value); ui->label_d1_h11->setStyleSheet(calColor(value.toInt())); }
        if (day==1 && hour==12) { ui->label_d1_h12->setText(value); ui->label_d1_h12->setStyleSheet(calColor(value.toInt())); }
        if (day==1 && hour==13) { ui->label_d1_h13->setText(value); ui->label_d1_h13->setStyleSheet(calColor(value.toInt())); }
        if (day==1 && hour==14) { ui->label_d1_h14->setText(value); ui->label_d1_h14->setStyleSheet(calColor(value.toInt())); }
        if (day==1 && hour==15) { ui->label_d1_h15->setText(value); ui->label_d1_h15->setStyleSheet(calColor(value.toInt())); }
        if (day==1 && hour==16) { ui->label_d1_h16->setText(value); ui->label_d1_h16->setStyleSheet(calColor(value.toInt())); }
        if (day==1 && hour==17) { ui->label_d1_h17->setText(value); ui->label_d1_h17->setStyleSheet(calColor(value.toInt())); }
        if (day==1 && hour==18) { ui->label_d1_h18->setText(value); ui->label_d1_h18->setStyleSheet(calColor(value.toInt())); }
        if (day==1 && hour==19) { ui->label_d1_h19->setText(value); ui->label_d1_h19->setStyleSheet(calColor(value.toInt())); }
        if (day==1 && hour==20) { ui->label_d1_h20->setText(value); ui->label_d1_h20->setStyleSheet(calColor(value.toInt())); }
        if (day==1 && hour==21) { ui->label_d1_h21->setText(value); ui->label_d1_h21->setStyleSheet(calColor(value.toInt())); }
        if (day==1 && hour==22) { ui->label_d1_h22->setText(value); ui->label_d1_h22->setStyleSheet(calColor(value.toInt())); }
        if (day==1 && hour==23) { ui->label_d1_h23->setText(value); ui->label_d1_h23->setStyleSheet(calColor(value.toInt())); }

        // kedd (2)
        if (day==2 && hour== 0) { ui->label_d2_h00->setText(value); ui->label_d2_h00->setStyleSheet(calColor(value.toInt())); }
        if (day==2 && hour== 1) { ui->label_d2_h01->setText(value); ui->label_d2_h01->setStyleSheet(calColor(value.toInt())); }
        if (day==2 && hour== 2) { ui->label_d2_h02->setText(value); ui->label_d2_h02->setStyleSheet(calColor(value.toInt())); }
        if (day==2 && hour== 3) { ui->label_d2_h03->setText(value); ui->label_d2_h03->setStyleSheet(calColor(value.toInt())); }
        if (day==2 && hour== 4) { ui->label_d2_h04->setText(value); ui->label_d2_h04->setStyleSheet(calColor(value.toInt())); }
        if (day==2 && hour== 5) { ui->label_d2_h05->setText(value); ui->label_d2_h05->setStyleSheet(calColor(value.toInt())); }
        if (day==2 && hour== 6) { ui->label_d2_h06->setText(value); ui->label_d2_h06->setStyleSheet(calColor(value.toInt())); }
        if (day==2 && hour== 7) { ui->label_d2_h07->setText(value); ui->label_d2_h07->setStyleSheet(calColor(value.toInt())); }
        if (day==2 && hour== 8) { ui->label_d2_h08->setText(value); ui->label_d2_h08->setStyleSheet(calColor(value.toInt())); }
        if (day==2 && hour== 9) { ui->label_d2_h09->setText(value); ui->label_d2_h09->setStyleSheet(calColor(value.toInt())); }
        if (day==2 && hour==10) { ui->label_d2_h10->setText(value); ui->label_d2_h10->setStyleSheet(calColor(value.toInt())); }
        if (day==2 && hour==11) { ui->label_d2_h11->setText(value); ui->label_d2_h11->setStyleSheet(calColor(value.toInt())); }
        if (day==2 && hour==12) { ui->label_d2_h12->setText(value); ui->label_d2_h12->setStyleSheet(calColor(value.toInt())); }
        if (day==2 && hour==13) { ui->label_d2_h13->setText(value); ui->label_d2_h13->setStyleSheet(calColor(value.toInt())); }
        if (day==2 && hour==14) { ui->label_d2_h14->setText(value); ui->label_d2_h14->setStyleSheet(calColor(value.toInt())); }
        if (day==2 && hour==15) { ui->label_d2_h15->setText(value); ui->label_d2_h15->setStyleSheet(calColor(value.toInt())); }
        if (day==2 && hour==16) { ui->label_d2_h16->setText(value); ui->label_d2_h16->setStyleSheet(calColor(value.toInt())); }
        if (day==2 && hour==17) { ui->label_d2_h17->setText(value); ui->label_d2_h17->setStyleSheet(calColor(value.toInt())); }
        if (day==2 && hour==18) { ui->label_d2_h18->setText(value); ui->label_d2_h18->setStyleSheet(calColor(value.toInt())); }
        if (day==2 && hour==19) { ui->label_d2_h19->setText(value); ui->label_d2_h19->setStyleSheet(calColor(value.toInt())); }
        if (day==2 && hour==20) { ui->label_d2_h20->setText(value); ui->label_d2_h20->setStyleSheet(calColor(value.toInt())); }
        if (day==2 && hour==21) { ui->label_d2_h21->setText(value); ui->label_d2_h21->setStyleSheet(calColor(value.toInt())); }
        if (day==2 && hour==22) { ui->label_d2_h22->setText(value); ui->label_d2_h22->setStyleSheet(calColor(value.toInt())); }
        if (day==2 && hour==23) { ui->label_d2_h23->setText(value); ui->label_d2_h23->setStyleSheet(calColor(value.toInt())); }

        // szerda (3)
        if (day==3 && hour== 0) { ui->label_d3_h00->setText(value); ui->label_d3_h00->setStyleSheet(calColor(value.toInt())); }
        if (day==3 && hour== 1) { ui->label_d3_h01->setText(value); ui->label_d3_h01->setStyleSheet(calColor(value.toInt())); }
        if (day==3 && hour== 2) { ui->label_d3_h02->setText(value); ui->label_d3_h02->setStyleSheet(calColor(value.toInt())); }
        if (day==3 && hour== 3) { ui->label_d3_h03->setText(value); ui->label_d3_h03->setStyleSheet(calColor(value.toInt())); }
        if (day==3 && hour== 4) { ui->label_d3_h04->setText(value); ui->label_d3_h04->setStyleSheet(calColor(value.toInt())); }
        if (day==3 && hour== 5) { ui->label_d3_h05->setText(value); ui->label_d3_h05->setStyleSheet(calColor(value.toInt())); }
        if (day==3 && hour== 6) { ui->label_d3_h06->setText(value); ui->label_d3_h06->setStyleSheet(calColor(value.toInt())); }
        if (day==3 && hour== 7) { ui->label_d3_h07->setText(value); ui->label_d3_h07->setStyleSheet(calColor(value.toInt())); }
        if (day==3 && hour== 8) { ui->label_d3_h08->setText(value); ui->label_d3_h08->setStyleSheet(calColor(value.toInt())); }
        if (day==3 && hour== 9) { ui->label_d3_h09->setText(value); ui->label_d3_h09->setStyleSheet(calColor(value.toInt())); }
        if (day==3 && hour==10) { ui->label_d3_h10->setText(value); ui->label_d3_h10->setStyleSheet(calColor(value.toInt())); }
        if (day==3 && hour==11) { ui->label_d3_h11->setText(value); ui->label_d3_h11->setStyleSheet(calColor(value.toInt())); }
        if (day==3 && hour==12) { ui->label_d3_h12->setText(value); ui->label_d3_h12->setStyleSheet(calColor(value.toInt())); }
        if (day==3 && hour==13) { ui->label_d3_h13->setText(value); ui->label_d3_h13->setStyleSheet(calColor(value.toInt())); }
        if (day==3 && hour==14) { ui->label_d3_h14->setText(value); ui->label_d3_h14->setStyleSheet(calColor(value.toInt())); }
        if (day==3 && hour==15) { ui->label_d3_h15->setText(value); ui->label_d3_h15->setStyleSheet(calColor(value.toInt())); }
        if (day==3 && hour==16) { ui->label_d3_h16->setText(value); ui->label_d3_h16->setStyleSheet(calColor(value.toInt())); }
        if (day==3 && hour==17) { ui->label_d3_h17->setText(value); ui->label_d3_h17->setStyleSheet(calColor(value.toInt())); }
        if (day==3 && hour==18) { ui->label_d3_h18->setText(value); ui->label_d3_h18->setStyleSheet(calColor(value.toInt())); }
        if (day==3 && hour==19) { ui->label_d3_h19->setText(value); ui->label_d3_h19->setStyleSheet(calColor(value.toInt())); }
        if (day==3 && hour==20) { ui->label_d3_h20->setText(value); ui->label_d3_h20->setStyleSheet(calColor(value.toInt())); }
        if (day==3 && hour==21) { ui->label_d3_h21->setText(value); ui->label_d3_h21->setStyleSheet(calColor(value.toInt())); }
        if (day==3 && hour==22) { ui->label_d3_h22->setText(value); ui->label_d3_h22->setStyleSheet(calColor(value.toInt())); }
        if (day==3 && hour==23) { ui->label_d3_h23->setText(value); ui->label_d3_h23->setStyleSheet(calColor(value.toInt())); }

        // csütörtök (4)
        if (day==4 && hour== 0) { ui->label_d4_h00->setText(value); ui->label_d4_h00->setStyleSheet(calColor(value.toInt())); }
        if (day==4 && hour== 1) { ui->label_d4_h01->setText(value); ui->label_d4_h01->setStyleSheet(calColor(value.toInt())); }
        if (day==4 && hour== 2) { ui->label_d4_h02->setText(value); ui->label_d4_h02->setStyleSheet(calColor(value.toInt())); }
        if (day==4 && hour== 3) { ui->label_d4_h03->setText(value); ui->label_d4_h03->setStyleSheet(calColor(value.toInt())); }
        if (day==4 && hour== 4) { ui->label_d4_h04->setText(value); ui->label_d4_h04->setStyleSheet(calColor(value.toInt())); }
        if (day==4 && hour== 5) { ui->label_d4_h05->setText(value); ui->label_d4_h05->setStyleSheet(calColor(value.toInt())); }
        if (day==4 && hour== 6) { ui->label_d4_h06->setText(value); ui->label_d4_h06->setStyleSheet(calColor(value.toInt())); }
        if (day==4 && hour== 7) { ui->label_d4_h07->setText(value); ui->label_d4_h07->setStyleSheet(calColor(value.toInt())); }
        if (day==4 && hour== 8) { ui->label_d4_h08->setText(value); ui->label_d4_h08->setStyleSheet(calColor(value.toInt())); }
        if (day==4 && hour== 9) { ui->label_d4_h09->setText(value); ui->label_d4_h09->setStyleSheet(calColor(value.toInt())); }
        if (day==4 && hour==10) { ui->label_d4_h10->setText(value); ui->label_d4_h10->setStyleSheet(calColor(value.toInt())); }
        if (day==4 && hour==11) { ui->label_d4_h11->setText(value); ui->label_d4_h11->setStyleSheet(calColor(value.toInt())); }
        if (day==4 && hour==12) { ui->label_d4_h12->setText(value); ui->label_d4_h12->setStyleSheet(calColor(value.toInt())); }
        if (day==4 && hour==13) { ui->label_d4_h13->setText(value); ui->label_d4_h13->setStyleSheet(calColor(value.toInt())); }
        if (day==4 && hour==14) { ui->label_d4_h14->setText(value); ui->label_d4_h14->setStyleSheet(calColor(value.toInt())); }
        if (day==4 && hour==15) { ui->label_d4_h15->setText(value); ui->label_d4_h15->setStyleSheet(calColor(value.toInt())); }
        if (day==4 && hour==16) { ui->label_d4_h16->setText(value); ui->label_d4_h16->setStyleSheet(calColor(value.toInt())); }
        if (day==4 && hour==17) { ui->label_d4_h17->setText(value); ui->label_d4_h17->setStyleSheet(calColor(value.toInt())); }
        if (day==4 && hour==18) { ui->label_d4_h18->setText(value); ui->label_d4_h18->setStyleSheet(calColor(value.toInt())); }
        if (day==4 && hour==19) { ui->label_d4_h19->setText(value); ui->label_d4_h19->setStyleSheet(calColor(value.toInt())); }
        if (day==4 && hour==20) { ui->label_d4_h20->setText(value); ui->label_d4_h20->setStyleSheet(calColor(value.toInt())); }
        if (day==4 && hour==21) { ui->label_d4_h21->setText(value); ui->label_d4_h21->setStyleSheet(calColor(value.toInt())); }
        if (day==4 && hour==22) { ui->label_d4_h22->setText(value); ui->label_d4_h22->setStyleSheet(calColor(value.toInt())); }
        if (day==4 && hour==23) { ui->label_d4_h23->setText(value); ui->label_d4_h23->setStyleSheet(calColor(value.toInt())); }

        // péntek (5)
        if (day==5 && hour== 0) { ui->label_d5_h00->setText(value); ui->label_d5_h00->setStyleSheet(calColor(value.toInt())); }
        if (day==5 && hour== 1) { ui->label_d5_h01->setText(value); ui->label_d5_h01->setStyleSheet(calColor(value.toInt())); }
        if (day==5 && hour== 2) { ui->label_d5_h02->setText(value); ui->label_d5_h02->setStyleSheet(calColor(value.toInt())); }
        if (day==5 && hour== 3) { ui->label_d5_h03->setText(value); ui->label_d5_h03->setStyleSheet(calColor(value.toInt())); }
        if (day==5 && hour== 4) { ui->label_d5_h04->setText(value); ui->label_d5_h04->setStyleSheet(calColor(value.toInt())); }
        if (day==5 && hour== 5) { ui->label_d5_h05->setText(value); ui->label_d5_h05->setStyleSheet(calColor(value.toInt())); }
        if (day==5 && hour== 6) { ui->label_d5_h06->setText(value); ui->label_d5_h06->setStyleSheet(calColor(value.toInt())); }
        if (day==5 && hour== 7) { ui->label_d5_h07->setText(value); ui->label_d5_h07->setStyleSheet(calColor(value.toInt())); }
        if (day==5 && hour== 8) { ui->label_d5_h08->setText(value); ui->label_d5_h08->setStyleSheet(calColor(value.toInt())); }
        if (day==5 && hour== 9) { ui->label_d5_h09->setText(value); ui->label_d5_h09->setStyleSheet(calColor(value.toInt())); }
        if (day==5 && hour==10) { ui->label_d5_h10->setText(value); ui->label_d5_h10->setStyleSheet(calColor(value.toInt())); }
        if (day==5 && hour==11) { ui->label_d5_h11->setText(value); ui->label_d5_h11->setStyleSheet(calColor(value.toInt())); }
        if (day==5 && hour==12) { ui->label_d5_h12->setText(value); ui->label_d5_h12->setStyleSheet(calColor(value.toInt())); }
        if (day==5 && hour==13) { ui->label_d5_h13->setText(value); ui->label_d5_h13->setStyleSheet(calColor(value.toInt())); }
        if (day==5 && hour==14) { ui->label_d5_h14->setText(value); ui->label_d5_h14->setStyleSheet(calColor(value.toInt())); }
        if (day==5 && hour==15) { ui->label_d5_h15->setText(value); ui->label_d5_h15->setStyleSheet(calColor(value.toInt())); }
        if (day==5 && hour==16) { ui->label_d5_h16->setText(value); ui->label_d5_h16->setStyleSheet(calColor(value.toInt())); }
        if (day==5 && hour==17) { ui->label_d5_h17->setText(value); ui->label_d5_h17->setStyleSheet(calColor(value.toInt())); }
        if (day==5 && hour==18) { ui->label_d5_h18->setText(value); ui->label_d5_h18->setStyleSheet(calColor(value.toInt())); }
        if (day==5 && hour==19) { ui->label_d5_h19->setText(value); ui->label_d5_h19->setStyleSheet(calColor(value.toInt())); }
        if (day==5 && hour==20) { ui->label_d5_h20->setText(value); ui->label_d5_h20->setStyleSheet(calColor(value.toInt())); }
        if (day==5 && hour==21) { ui->label_d5_h21->setText(value); ui->label_d5_h21->setStyleSheet(calColor(value.toInt())); }
        if (day==5 && hour==22) { ui->label_d5_h22->setText(value); ui->label_d5_h22->setStyleSheet(calColor(value.toInt())); }
        if (day==5 && hour==23) { ui->label_d5_h23->setText(value); ui->label_d5_h23->setStyleSheet(calColor(value.toInt())); }

        // szombat (6)
        if (day==6 && hour== 0) { ui->label_d6_h00->setText(value); ui->label_d6_h00->setStyleSheet(calColor(value.toInt())); }
        if (day==6 && hour== 1) { ui->label_d6_h01->setText(value); ui->label_d6_h01->setStyleSheet(calColor(value.toInt())); }
        if (day==6 && hour== 2) { ui->label_d6_h02->setText(value); ui->label_d6_h02->setStyleSheet(calColor(value.toInt())); }
        if (day==6 && hour== 3) { ui->label_d6_h03->setText(value); ui->label_d6_h03->setStyleSheet(calColor(value.toInt())); }
        if (day==6 && hour== 4) { ui->label_d6_h04->setText(value); ui->label_d6_h04->setStyleSheet(calColor(value.toInt())); }
        if (day==6 && hour== 5) { ui->label_d6_h05->setText(value); ui->label_d6_h05->setStyleSheet(calColor(value.toInt())); }
        if (day==6 && hour== 6) { ui->label_d6_h06->setText(value); ui->label_d6_h06->setStyleSheet(calColor(value.toInt())); }
        if (day==6 && hour== 7) { ui->label_d6_h07->setText(value); ui->label_d6_h07->setStyleSheet(calColor(value.toInt())); }
        if (day==6 && hour== 8) { ui->label_d6_h08->setText(value); ui->label_d6_h08->setStyleSheet(calColor(value.toInt())); }
        if (day==6 && hour== 9) { ui->label_d6_h09->setText(value); ui->label_d6_h09->setStyleSheet(calColor(value.toInt())); }
        if (day==6 && hour==10) { ui->label_d6_h10->setText(value); ui->label_d6_h10->setStyleSheet(calColor(value.toInt())); }
        if (day==6 && hour==11) { ui->label_d6_h11->setText(value); ui->label_d6_h11->setStyleSheet(calColor(value.toInt())); }
        if (day==6 && hour==12) { ui->label_d6_h12->setText(value); ui->label_d6_h12->setStyleSheet(calColor(value.toInt())); }
        if (day==6 && hour==13) { ui->label_d6_h13->setText(value); ui->label_d6_h13->setStyleSheet(calColor(value.toInt())); }
        if (day==6 && hour==14) { ui->label_d6_h14->setText(value); ui->label_d6_h14->setStyleSheet(calColor(value.toInt())); }
        if (day==6 && hour==15) { ui->label_d6_h15->setText(value); ui->label_d6_h15->setStyleSheet(calColor(value.toInt())); }
        if (day==6 && hour==16) { ui->label_d6_h16->setText(value); ui->label_d6_h16->setStyleSheet(calColor(value.toInt())); }
        if (day==6 && hour==17) { ui->label_d6_h17->setText(value); ui->label_d6_h17->setStyleSheet(calColor(value.toInt())); }
        if (day==6 && hour==18) { ui->label_d6_h18->setText(value); ui->label_d6_h18->setStyleSheet(calColor(value.toInt())); }
        if (day==6 && hour==19) { ui->label_d6_h19->setText(value); ui->label_d6_h19->setStyleSheet(calColor(value.toInt())); }
        if (day==6 && hour==20) { ui->label_d6_h20->setText(value); ui->label_d6_h20->setStyleSheet(calColor(value.toInt())); }
        if (day==6 && hour==21) { ui->label_d6_h21->setText(value); ui->label_d6_h21->setStyleSheet(calColor(value.toInt())); }
        if (day==6 && hour==22) { ui->label_d6_h22->setText(value); ui->label_d6_h22->setStyleSheet(calColor(value.toInt())); }
        if (day==6 && hour==23) { ui->label_d6_h23->setText(value); ui->label_d6_h23->setStyleSheet(calColor(value.toInt())); }



        // vasárnap (0->7)
        if (day==0 && hour== 0) { ui->label_d7_h00->setText(value); ui->label_d7_h00->setStyleSheet(calColor(value.toInt())); }
        if (day==0 && hour== 1) { ui->label_d7_h01->setText(value); ui->label_d7_h01->setStyleSheet(calColor(value.toInt())); }
        if (day==0 && hour== 2) { ui->label_d7_h02->setText(value); ui->label_d7_h02->setStyleSheet(calColor(value.toInt())); }
        if (day==0 && hour== 3) { ui->label_d7_h03->setText(value); ui->label_d7_h03->setStyleSheet(calColor(value.toInt())); }
        if (day==0 && hour== 4) { ui->label_d7_h04->setText(value); ui->label_d7_h04->setStyleSheet(calColor(value.toInt())); }
        if (day==0 && hour== 5) { ui->label_d7_h05->setText(value); ui->label_d7_h05->setStyleSheet(calColor(value.toInt())); }
        if (day==0 && hour== 6) { ui->label_d7_h06->setText(value); ui->label_d7_h06->setStyleSheet(calColor(value.toInt())); }
        if (day==0 && hour== 7) { ui->label_d7_h07->setText(value); ui->label_d7_h07->setStyleSheet(calColor(value.toInt())); }
        if (day==0 && hour== 8) { ui->label_d7_h08->setText(value); ui->label_d7_h08->setStyleSheet(calColor(value.toInt())); }
        if (day==0 && hour== 9) { ui->label_d7_h09->setText(value); ui->label_d7_h09->setStyleSheet(calColor(value.toInt())); }
        if (day==0 && hour==10) { ui->label_d7_h10->setText(value); ui->label_d7_h10->setStyleSheet(calColor(value.toInt())); }
        if (day==0 && hour==11) { ui->label_d7_h11->setText(value); ui->label_d7_h11->setStyleSheet(calColor(value.toInt())); }
        if (day==0 && hour==12) { ui->label_d7_h12->setText(value); ui->label_d7_h12->setStyleSheet(calColor(value.toInt())); }
        if (day==0 && hour==13) { ui->label_d7_h13->setText(value); ui->label_d7_h13->setStyleSheet(calColor(value.toInt())); }
        if (day==0 && hour==14) { ui->label_d7_h14->setText(value); ui->label_d7_h14->setStyleSheet(calColor(value.toInt())); }
        if (day==0 && hour==15) { ui->label_d7_h15->setText(value); ui->label_d7_h15->setStyleSheet(calColor(value.toInt())); }
        if (day==0 && hour==16) { ui->label_d7_h16->setText(value); ui->label_d7_h16->setStyleSheet(calColor(value.toInt())); }
        if (day==0 && hour==17) { ui->label_d7_h17->setText(value); ui->label_d7_h17->setStyleSheet(calColor(value.toInt())); }
        if (day==0 && hour==18) { ui->label_d7_h18->setText(value); ui->label_d7_h18->setStyleSheet(calColor(value.toInt())); }
        if (day==0 && hour==19) { ui->label_d7_h19->setText(value); ui->label_d7_h19->setStyleSheet(calColor(value.toInt())); }
        if (day==0 && hour==20) { ui->label_d7_h20->setText(value); ui->label_d7_h20->setStyleSheet(calColor(value.toInt())); }
        if (day==0 && hour==21) { ui->label_d7_h21->setText(value); ui->label_d7_h21->setStyleSheet(calColor(value.toInt())); }
        if (day==0 && hour==22) { ui->label_d7_h22->setText(value); ui->label_d7_h22->setStyleSheet(calColor(value.toInt())); }
        if (day==0 && hour==23) { ui->label_d7_h23->setText(value); ui->label_d7_h23->setStyleSheet(calColor(value.toInt())); }





        QCoreApplication::processEvents();
    }


    maxValue = maxd;
    minValue = mind;

    if (d1avg) { ui->label_avg_d1->setText(QString::number(d1avg)); ui->label_avg_d1->setStyleSheet(calColor(d1avg));  }
    if (d2avg) { ui->label_avg_d2->setText(QString::number(d2avg)); ui->label_avg_d2->setStyleSheet(calColor(d2avg));  }
    if (d3avg) { ui->label_avg_d3->setText(QString::number(d3avg)); ui->label_avg_d3->setStyleSheet(calColor(d3avg));  }
    if (d4avg) { ui->label_avg_d4->setText(QString::number(d4avg)); ui->label_avg_d4->setStyleSheet(calColor(d4avg));  }
    if (d5avg) { ui->label_avg_d5->setText(QString::number(d5avg)); ui->label_avg_d5->setStyleSheet(calColor(d5avg));  }
    if (d6avg) { ui->label_avg_d6->setText(QString::number(d6avg)); ui->label_avg_d6->setStyleSheet(calColor(d6avg));  }
    if (d7avg) { ui->label_avg_d7->setText(QString::number(d7avg)); ui->label_avg_d7->setStyleSheet(calColor(d7avg));  }

    maxValue = maxh;
    minValue = minh;

    if (h00avg) { ui->label_avg_h00->setText(QString::number(h00avg)); ui->label_avg_h00->setStyleSheet(calColor(h00avg));  }
    if (h01avg) { ui->label_avg_h01->setText(QString::number(h01avg)); ui->label_avg_h01->setStyleSheet(calColor(h01avg));  }
    if (h02avg) { ui->label_avg_h02->setText(QString::number(h02avg)); ui->label_avg_h02->setStyleSheet(calColor(h02avg));  }
    if (h03avg) { ui->label_avg_h03->setText(QString::number(h03avg)); ui->label_avg_h03->setStyleSheet(calColor(h03avg));  }
    if (h04avg) { ui->label_avg_h04->setText(QString::number(h04avg)); ui->label_avg_h04->setStyleSheet(calColor(h04avg));  }
    if (h05avg) { ui->label_avg_h05->setText(QString::number(h05avg)); ui->label_avg_h05->setStyleSheet(calColor(h05avg));  }
    if (h06avg) { ui->label_avg_h06->setText(QString::number(h06avg)); ui->label_avg_h06->setStyleSheet(calColor(h06avg));  }
    if (h07avg) { ui->label_avg_h07->setText(QString::number(h07avg)); ui->label_avg_h07->setStyleSheet(calColor(h07avg));  }
    if (h08avg) { ui->label_avg_h08->setText(QString::number(h08avg)); ui->label_avg_h08->setStyleSheet(calColor(h08avg));  }
    if (h09avg) { ui->label_avg_h09->setText(QString::number(h09avg)); ui->label_avg_h09->setStyleSheet(calColor(h09avg));  }
    if (h10avg) { ui->label_avg_h10->setText(QString::number(h10avg)); ui->label_avg_h10->setStyleSheet(calColor(h10avg));  }
    if (h11avg) { ui->label_avg_h11->setText(QString::number(h11avg)); ui->label_avg_h11->setStyleSheet(calColor(h11avg));  }
    if (h12avg) { ui->label_avg_h12->setText(QString::number(h12avg)); ui->label_avg_h12->setStyleSheet(calColor(h12avg));  }
    if (h13avg) { ui->label_avg_h13->setText(QString::number(h13avg)); ui->label_avg_h13->setStyleSheet(calColor(h13avg));  }
    if (h14avg) { ui->label_avg_h14->setText(QString::number(h14avg)); ui->label_avg_h14->setStyleSheet(calColor(h14avg));  }
    if (h15avg) { ui->label_avg_h15->setText(QString::number(h15avg)); ui->label_avg_h15->setStyleSheet(calColor(h15avg));  }
    if (h16avg) { ui->label_avg_h16->setText(QString::number(h16avg)); ui->label_avg_h16->setStyleSheet(calColor(h16avg));  }
    if (h17avg) { ui->label_avg_h17->setText(QString::number(h17avg)); ui->label_avg_h17->setStyleSheet(calColor(h17avg));  }
    if (h18avg) { ui->label_avg_h18->setText(QString::number(h18avg)); ui->label_avg_h18->setStyleSheet(calColor(h18avg));  }
    if (h19avg) { ui->label_avg_h19->setText(QString::number(h19avg)); ui->label_avg_h19->setStyleSheet(calColor(h19avg));  }
    if (h20avg) { ui->label_avg_h20->setText(QString::number(h20avg)); ui->label_avg_h20->setStyleSheet(calColor(h20avg));  }
    if (h21avg) { ui->label_avg_h21->setText(QString::number(h21avg)); ui->label_avg_h21->setStyleSheet(calColor(h21avg));  }
    if (h22avg) { ui->label_avg_h22->setText(QString::number(h22avg)); ui->label_avg_h22->setStyleSheet(calColor(h22avg));  }
    if (h23avg) { ui->label_avg_h23->setText(QString::number(h23avg)); ui->label_avg_h23->setStyleSheet(calColor(h23avg));  }


}

void uploadTimesDialog::clearMatrix()
{
    QString value = QString::number(maxValue);
    QString style="QLabel { background-color : rgba(0,0,0,0); border-top:1px solid grey; border-left:1px solid grey;}";

    ui->label_d1_h00->setText(""); ui->label_d1_h00->setStyleSheet(style);
    ui->label_d1_h01->setText(""); ui->label_d1_h01->setStyleSheet(style);
    ui->label_d1_h02->setText(""); ui->label_d1_h02->setStyleSheet(style);
    ui->label_d1_h03->setText(""); ui->label_d1_h03->setStyleSheet(style);
    ui->label_d1_h04->setText(""); ui->label_d1_h04->setStyleSheet(style);
    ui->label_d1_h05->setText(""); ui->label_d1_h05->setStyleSheet(style);
    ui->label_d1_h06->setText(""); ui->label_d1_h06->setStyleSheet(style);
    ui->label_d1_h07->setText(""); ui->label_d1_h07->setStyleSheet(style);
    ui->label_d1_h08->setText(""); ui->label_d1_h08->setStyleSheet(style);
    ui->label_d1_h09->setText(""); ui->label_d1_h09->setStyleSheet(style);
    ui->label_d1_h10->setText(""); ui->label_d1_h10->setStyleSheet(style);
    ui->label_d1_h11->setText(""); ui->label_d1_h11->setStyleSheet(style);
    ui->label_d1_h12->setText(""); ui->label_d1_h12->setStyleSheet(style);
    ui->label_d1_h13->setText(""); ui->label_d1_h13->setStyleSheet(style);
    ui->label_d1_h14->setText(""); ui->label_d1_h14->setStyleSheet(style);
    ui->label_d1_h15->setText(""); ui->label_d1_h15->setStyleSheet(style);
    ui->label_d1_h16->setText(""); ui->label_d1_h16->setStyleSheet(style);
    ui->label_d1_h17->setText(""); ui->label_d1_h17->setStyleSheet(style);
    ui->label_d1_h18->setText(""); ui->label_d1_h18->setStyleSheet(style);
    ui->label_d1_h19->setText(""); ui->label_d1_h19->setStyleSheet(style);
    ui->label_d1_h20->setText(""); ui->label_d1_h20->setStyleSheet(style);
    ui->label_d1_h21->setText(""); ui->label_d1_h21->setStyleSheet(style);
    ui->label_d1_h22->setText(""); ui->label_d1_h22->setStyleSheet(style);
    style="QLabel { background-color : rgba(0,0,0,0); border-top:1px solid grey; border-left:1px solid grey; border-right:1px solid grey}";
    ui->label_d1_h23->setText(""); ui->label_d1_h23->setStyleSheet(style);

    style="QLabel { background-color : rgba(0,0,0,0); border-top:1px solid grey; border-left:1px solid grey}";

    ui->label_d2_h00->setText(""); ui->label_d2_h00->setStyleSheet(style);
    ui->label_d2_h01->setText(""); ui->label_d2_h01->setStyleSheet(style);
    ui->label_d2_h02->setText(""); ui->label_d2_h02->setStyleSheet(style);
    ui->label_d2_h03->setText(""); ui->label_d2_h03->setStyleSheet(style);
    ui->label_d2_h04->setText(""); ui->label_d2_h04->setStyleSheet(style);
    ui->label_d2_h05->setText(""); ui->label_d2_h05->setStyleSheet(style);
    ui->label_d2_h06->setText(""); ui->label_d2_h06->setStyleSheet(style);
    ui->label_d2_h07->setText(""); ui->label_d2_h07->setStyleSheet(style);
    ui->label_d2_h08->setText(""); ui->label_d2_h08->setStyleSheet(style);
    ui->label_d2_h09->setText(""); ui->label_d2_h09->setStyleSheet(style);
    ui->label_d2_h10->setText(""); ui->label_d2_h10->setStyleSheet(style);
    ui->label_d2_h11->setText(""); ui->label_d2_h11->setStyleSheet(style);
    ui->label_d2_h12->setText(""); ui->label_d2_h12->setStyleSheet(style);
    ui->label_d2_h13->setText(""); ui->label_d2_h13->setStyleSheet(style);
    ui->label_d2_h14->setText(""); ui->label_d2_h14->setStyleSheet(style);
    ui->label_d2_h15->setText(""); ui->label_d2_h15->setStyleSheet(style);
    ui->label_d2_h16->setText(""); ui->label_d2_h16->setStyleSheet(style);
    ui->label_d2_h17->setText(""); ui->label_d2_h17->setStyleSheet(style);
    ui->label_d2_h18->setText(""); ui->label_d2_h18->setStyleSheet(style);
    ui->label_d2_h19->setText(""); ui->label_d2_h19->setStyleSheet(style);
    ui->label_d2_h20->setText(""); ui->label_d2_h20->setStyleSheet(style);
    ui->label_d2_h21->setText(""); ui->label_d2_h21->setStyleSheet(style);
    ui->label_d2_h22->setText(""); ui->label_d2_h22->setStyleSheet(style);
    style="QLabel { background-color : rgba(0,0,0,0); border-top:1px solid grey; border-left:1px solid grey; border-right:1px solid grey}";
    ui->label_d2_h23->setText(""); ui->label_d2_h23->setStyleSheet(style);

    style="QLabel { background-color : rgba(0,0,0,0); border-top:1px solid grey; border-left:1px solid grey}";

    ui->label_d3_h00->setText(""); ui->label_d3_h00->setStyleSheet(style);
    ui->label_d3_h01->setText(""); ui->label_d3_h01->setStyleSheet(style);
    ui->label_d3_h02->setText(""); ui->label_d3_h02->setStyleSheet(style);
    ui->label_d3_h03->setText(""); ui->label_d3_h03->setStyleSheet(style);
    ui->label_d3_h04->setText(""); ui->label_d3_h04->setStyleSheet(style);
    ui->label_d3_h05->setText(""); ui->label_d3_h05->setStyleSheet(style);
    ui->label_d3_h06->setText(""); ui->label_d3_h06->setStyleSheet(style);
    ui->label_d3_h07->setText(""); ui->label_d3_h07->setStyleSheet(style);
    ui->label_d3_h08->setText(""); ui->label_d3_h08->setStyleSheet(style);
    ui->label_d3_h09->setText(""); ui->label_d3_h09->setStyleSheet(style);
    ui->label_d3_h10->setText(""); ui->label_d3_h10->setStyleSheet(style);
    ui->label_d3_h11->setText(""); ui->label_d3_h11->setStyleSheet(style);
    ui->label_d3_h12->setText(""); ui->label_d3_h12->setStyleSheet(style);
    ui->label_d3_h13->setText(""); ui->label_d3_h13->setStyleSheet(style);
    ui->label_d3_h14->setText(""); ui->label_d3_h14->setStyleSheet(style);
    ui->label_d3_h15->setText(""); ui->label_d3_h15->setStyleSheet(style);
    ui->label_d3_h16->setText(""); ui->label_d3_h16->setStyleSheet(style);
    ui->label_d3_h17->setText(""); ui->label_d3_h17->setStyleSheet(style);
    ui->label_d3_h18->setText(""); ui->label_d3_h18->setStyleSheet(style);
    ui->label_d3_h19->setText(""); ui->label_d3_h19->setStyleSheet(style);
    ui->label_d3_h20->setText(""); ui->label_d3_h20->setStyleSheet(style);
    ui->label_d3_h21->setText(""); ui->label_d3_h21->setStyleSheet(style);
    ui->label_d3_h22->setText(""); ui->label_d3_h22->setStyleSheet(style);
    style="QLabel { background-color : rgba(0,0,0,0); border-top:1px solid grey; border-left:1px solid grey; border-right:1px solid grey}";
    ui->label_d3_h23->setText(""); ui->label_d3_h23->setStyleSheet(style);

    style="QLabel { background-color : rgba(0,0,0,0); border-top:1px solid grey; border-left:1px solid grey}";

    ui->label_d4_h00->setText(""); ui->label_d4_h00->setStyleSheet(style);
    ui->label_d4_h01->setText(""); ui->label_d4_h01->setStyleSheet(style);
    ui->label_d4_h02->setText(""); ui->label_d4_h02->setStyleSheet(style);
    ui->label_d4_h03->setText(""); ui->label_d4_h03->setStyleSheet(style);
    ui->label_d4_h04->setText(""); ui->label_d4_h04->setStyleSheet(style);
    ui->label_d4_h05->setText(""); ui->label_d4_h05->setStyleSheet(style);
    ui->label_d4_h06->setText(""); ui->label_d4_h06->setStyleSheet(style);
    ui->label_d4_h07->setText(""); ui->label_d4_h07->setStyleSheet(style);
    ui->label_d4_h08->setText(""); ui->label_d4_h08->setStyleSheet(style);
    ui->label_d4_h09->setText(""); ui->label_d4_h09->setStyleSheet(style);
    ui->label_d4_h10->setText(""); ui->label_d4_h10->setStyleSheet(style);
    ui->label_d4_h11->setText(""); ui->label_d4_h11->setStyleSheet(style);
    ui->label_d4_h12->setText(""); ui->label_d4_h12->setStyleSheet(style);
    ui->label_d4_h13->setText(""); ui->label_d4_h13->setStyleSheet(style);
    ui->label_d4_h14->setText(""); ui->label_d4_h14->setStyleSheet(style);
    ui->label_d4_h15->setText(""); ui->label_d4_h15->setStyleSheet(style);
    ui->label_d4_h16->setText(""); ui->label_d4_h16->setStyleSheet(style);
    ui->label_d4_h17->setText(""); ui->label_d4_h17->setStyleSheet(style);
    ui->label_d4_h18->setText(""); ui->label_d4_h18->setStyleSheet(style);
    ui->label_d4_h19->setText(""); ui->label_d4_h19->setStyleSheet(style);
    ui->label_d4_h20->setText(""); ui->label_d4_h20->setStyleSheet(style);
    ui->label_d4_h21->setText(""); ui->label_d4_h21->setStyleSheet(style);
    ui->label_d4_h22->setText(""); ui->label_d4_h22->setStyleSheet(style);
    style="QLabel { background-color : rgba(0,0,0,0); border-top:1px solid grey; border-left:1px solid grey; border-right:1px solid grey}";
    ui->label_d4_h23->setText(""); ui->label_d4_h23->setStyleSheet(style);

    style="QLabel { background-color : rgba(0,0,0,0); border-top:1px solid grey; border-left:1px solid grey}";

    ui->label_d5_h00->setText(""); ui->label_d5_h00->setStyleSheet(style);
    ui->label_d5_h01->setText(""); ui->label_d5_h01->setStyleSheet(style);
    ui->label_d5_h02->setText(""); ui->label_d5_h02->setStyleSheet(style);
    ui->label_d5_h03->setText(""); ui->label_d5_h03->setStyleSheet(style);
    ui->label_d5_h04->setText(""); ui->label_d5_h04->setStyleSheet(style);
    ui->label_d5_h05->setText(""); ui->label_d5_h05->setStyleSheet(style);
    ui->label_d5_h06->setText(""); ui->label_d5_h06->setStyleSheet(style);
    ui->label_d5_h07->setText(""); ui->label_d5_h07->setStyleSheet(style);
    ui->label_d5_h08->setText(""); ui->label_d5_h08->setStyleSheet(style);
    ui->label_d5_h09->setText(""); ui->label_d5_h09->setStyleSheet(style);
    ui->label_d5_h10->setText(""); ui->label_d5_h10->setStyleSheet(style);
    ui->label_d5_h11->setText(""); ui->label_d5_h11->setStyleSheet(style);
    ui->label_d5_h12->setText(""); ui->label_d5_h12->setStyleSheet(style);
    ui->label_d5_h13->setText(""); ui->label_d5_h13->setStyleSheet(style);
    ui->label_d5_h14->setText(""); ui->label_d5_h14->setStyleSheet(style);
    ui->label_d5_h15->setText(""); ui->label_d5_h15->setStyleSheet(style);
    ui->label_d5_h16->setText(""); ui->label_d5_h16->setStyleSheet(style);
    ui->label_d5_h17->setText(""); ui->label_d5_h17->setStyleSheet(style);
    ui->label_d5_h18->setText(""); ui->label_d5_h18->setStyleSheet(style);
    ui->label_d5_h19->setText(""); ui->label_d5_h19->setStyleSheet(style);
    ui->label_d5_h20->setText(""); ui->label_d5_h20->setStyleSheet(style);
    ui->label_d5_h21->setText(""); ui->label_d5_h21->setStyleSheet(style);
    ui->label_d5_h22->setText(""); ui->label_d5_h22->setStyleSheet(style);
    style="QLabel { background-color : rgba(0,0,0,0); border-top:1px solid grey; border-left:1px solid grey; border-right:1px solid grey}";
    ui->label_d5_h23->setText(""); ui->label_d5_h23->setStyleSheet(style);

    style="QLabel { background-color : rgba(0,0,0,0); border-top:1px solid grey; border-left:1px solid grey}";

    ui->label_d6_h00->setText(""); ui->label_d6_h00->setStyleSheet(style);
    ui->label_d6_h01->setText(""); ui->label_d6_h01->setStyleSheet(style);
    ui->label_d6_h02->setText(""); ui->label_d6_h02->setStyleSheet(style);
    ui->label_d6_h03->setText(""); ui->label_d6_h03->setStyleSheet(style);
    ui->label_d6_h04->setText(""); ui->label_d6_h04->setStyleSheet(style);
    ui->label_d6_h05->setText(""); ui->label_d6_h05->setStyleSheet(style);
    ui->label_d6_h06->setText(""); ui->label_d6_h06->setStyleSheet(style);
    ui->label_d6_h07->setText(""); ui->label_d6_h07->setStyleSheet(style);
    ui->label_d6_h08->setText(""); ui->label_d6_h08->setStyleSheet(style);
    ui->label_d6_h09->setText(""); ui->label_d6_h09->setStyleSheet(style);
    ui->label_d6_h10->setText(""); ui->label_d6_h10->setStyleSheet(style);
    ui->label_d6_h11->setText(""); ui->label_d6_h11->setStyleSheet(style);
    ui->label_d6_h12->setText(""); ui->label_d6_h12->setStyleSheet(style);
    ui->label_d6_h13->setText(""); ui->label_d6_h13->setStyleSheet(style);
    ui->label_d6_h14->setText(""); ui->label_d6_h14->setStyleSheet(style);
    ui->label_d6_h15->setText(""); ui->label_d6_h15->setStyleSheet(style);
    ui->label_d6_h16->setText(""); ui->label_d6_h16->setStyleSheet(style);
    ui->label_d6_h17->setText(""); ui->label_d6_h17->setStyleSheet(style);
    ui->label_d6_h18->setText(""); ui->label_d6_h18->setStyleSheet(style);
    ui->label_d6_h19->setText(""); ui->label_d6_h19->setStyleSheet(style);
    ui->label_d6_h20->setText(""); ui->label_d6_h20->setStyleSheet(style);
    ui->label_d6_h21->setText(""); ui->label_d6_h21->setStyleSheet(style);
    ui->label_d6_h22->setText(""); ui->label_d6_h22->setStyleSheet(style);
    style="QLabel { background-color : rgba(0,0,0,0); border-top:1px solid grey; border-left:1px solid grey; border-right:1px solid grey}";
    ui->label_d6_h23->setText(""); ui->label_d6_h23->setStyleSheet(style);

    style="QLabel { background-color : rgba(0,0,0,0); border-top:1px solid grey; border-left:1px solid grey; border-bottom:1px solid grey}";

    ui->label_d7_h00->setText(""); ui->label_d7_h00->setStyleSheet(style);
    ui->label_d7_h01->setText(""); ui->label_d7_h01->setStyleSheet(style);
    ui->label_d7_h02->setText(""); ui->label_d7_h02->setStyleSheet(style);
    ui->label_d7_h03->setText(""); ui->label_d7_h03->setStyleSheet(style);
    ui->label_d7_h04->setText(""); ui->label_d7_h04->setStyleSheet(style);
    ui->label_d7_h05->setText(""); ui->label_d7_h05->setStyleSheet(style);
    ui->label_d7_h06->setText(""); ui->label_d7_h06->setStyleSheet(style);
    ui->label_d7_h07->setText(""); ui->label_d7_h07->setStyleSheet(style);
    ui->label_d7_h08->setText(""); ui->label_d7_h08->setStyleSheet(style);
    ui->label_d7_h09->setText(""); ui->label_d7_h09->setStyleSheet(style);
    ui->label_d7_h10->setText(""); ui->label_d7_h10->setStyleSheet(style);
    ui->label_d7_h11->setText(""); ui->label_d7_h11->setStyleSheet(style);
    ui->label_d7_h12->setText(""); ui->label_d7_h12->setStyleSheet(style);
    ui->label_d7_h13->setText(""); ui->label_d7_h13->setStyleSheet(style);
    ui->label_d7_h14->setText(""); ui->label_d7_h14->setStyleSheet(style);
    ui->label_d7_h15->setText(""); ui->label_d7_h15->setStyleSheet(style);
    ui->label_d7_h16->setText(""); ui->label_d7_h16->setStyleSheet(style);
    ui->label_d7_h17->setText(""); ui->label_d7_h17->setStyleSheet(style);
    ui->label_d7_h18->setText(""); ui->label_d7_h18->setStyleSheet(style);
    ui->label_d7_h19->setText(""); ui->label_d7_h19->setStyleSheet(style);
    ui->label_d7_h20->setText(""); ui->label_d7_h20->setStyleSheet(style);
    ui->label_d7_h21->setText(""); ui->label_d7_h21->setStyleSheet(style);
    ui->label_d7_h22->setText(""); ui->label_d7_h22->setStyleSheet(style);
    style="QLabel { background-color : rgba(0,0,0,0); border:1px solid grey;}";
    ui->label_d7_h23->setText(""); ui->label_d7_h23->setStyleSheet(style);

    style="QLabel { background-color : rgba(0,0,0,0); border-top:1px solid grey; border-right:1px solid grey;border-left:1px solid grey}";


    ui->label_avg_d1->setText(""); ui->label_avg_d1->setStyleSheet(style);
    ui->label_avg_d2->setText(""); ui->label_avg_d2->setStyleSheet(style);
    ui->label_avg_d3->setText(""); ui->label_avg_d3->setStyleSheet(style);
    ui->label_avg_d4->setText(""); ui->label_avg_d4->setStyleSheet(style);
    ui->label_avg_d5->setText(""); ui->label_avg_d5->setStyleSheet(style);
    ui->label_avg_d6->setText(""); ui->label_avg_d6->setStyleSheet(style);
    style="QLabel { background-color : rgba(0,0,0,0); border:1px solid grey; }";
    ui->label_avg_d7->setText(""); ui->label_avg_d7->setStyleSheet(style);

    style="QLabel { background-color : rgba(0,0,0,0); border-top:1px solid grey; border-left:1px solid grey; border-bottom:1px solid grey}";

    ui->label_avg_h00->setText(""); ui->label_avg_h00->setStyleSheet(style);
    ui->label_avg_h01->setText(""); ui->label_avg_h01->setStyleSheet(style);
    ui->label_avg_h02->setText(""); ui->label_avg_h02->setStyleSheet(style);
    ui->label_avg_h03->setText(""); ui->label_avg_h03->setStyleSheet(style);
    ui->label_avg_h04->setText(""); ui->label_avg_h04->setStyleSheet(style);
    ui->label_avg_h05->setText(""); ui->label_avg_h05->setStyleSheet(style);
    ui->label_avg_h06->setText(""); ui->label_avg_h06->setStyleSheet(style);
    ui->label_avg_h07->setText(""); ui->label_avg_h07->setStyleSheet(style);
    ui->label_avg_h08->setText(""); ui->label_avg_h08->setStyleSheet(style);
    ui->label_avg_h09->setText(""); ui->label_avg_h09->setStyleSheet(style);
    ui->label_avg_h10->setText(""); ui->label_avg_h10->setStyleSheet(style);
    ui->label_avg_h11->setText(""); ui->label_avg_h11->setStyleSheet(style);
    ui->label_avg_h12->setText(""); ui->label_avg_h12->setStyleSheet(style);
    ui->label_avg_h13->setText(""); ui->label_avg_h13->setStyleSheet(style);
    ui->label_avg_h14->setText(""); ui->label_avg_h14->setStyleSheet(style);
    ui->label_avg_h15->setText(""); ui->label_avg_h15->setStyleSheet(style);
    ui->label_avg_h16->setText(""); ui->label_avg_h16->setStyleSheet(style);
    ui->label_avg_h17->setText(""); ui->label_avg_h17->setStyleSheet(style);
    ui->label_avg_h18->setText(""); ui->label_avg_h18->setStyleSheet(style);
    ui->label_avg_h19->setText(""); ui->label_avg_h19->setStyleSheet(style);
    ui->label_avg_h20->setText(""); ui->label_avg_h20->setStyleSheet(style);
    ui->label_avg_h21->setText(""); ui->label_avg_h21->setStyleSheet(style);
    ui->label_avg_h22->setText(""); ui->label_avg_h22->setStyleSheet(style);
    style="QLabel { background-color : rgba(0,0,0,0); border:1px solid grey; }";
    ui->label_avg_h23->setText(""); ui->label_avg_h23->setStyleSheet(style);
}

QString uploadTimesDialog::calColor(int v)
{
    /*
    int cR = 255 * (maxValue-v+0.0) / (maxValue-minValue+0.0);
    int cG = 255 * (v-minValue+0.0) / (maxValue-minValue+0.0);
    QString color = " QLabel { background-color : rgb("+QString::number(cR)+","+QString::number(cG)+",0); "
                    " color : rgb(0,0,0) ; border-top:1px solid grey; border-left:1px solid grey ;padding-top:3px;padding-bottom:3px;} " ;
                   // " color : rgb("+QString::number(cG)+","+QString::number(cG)+","+QString::number(cG)+") }";
    */
    //qDebug() << color;
    // 240 152 0
    QString color;

    // 3 color scale
    int limitRed   = (maxValue-minValue) / 3 + minValue;
    int limitGreen = (maxValue-minValue) / 3 * 2 + minValue;


    if ( v <= limitRed                    )  color = " QLabel { background-color : rgb(255,0,0); ";
    if ( v >  limitRed && v <= limitGreen )  color = " QLabel { background-color : rgb(240,152,0); ";
    if ( v >  limitGreen                  )  color = " QLabel { background-color : rgb(0,255,0); ";

    // 5 color scale
    int per5 = (maxValue-minValue)/5;
    //int p1   = minValue + per5 * 1;
    //int p2   = minValue + per5 * 2;
    //int p3   = minValue + per5 * 3;
    //int p4   = minValue + per5 * 4;

    int per12 = (maxValue-minValue)/12;
    int p1   = minValue + per12 * 1;
    int p2   = minValue + per12 * 2;
    int p3   = minValue + per12 * 3;
    int p4   = minValue + per12 * 4;
    int p5   = minValue + per12 * 5;
    int p6   = minValue + per12 * 6;
    int p7   = minValue + per12 * 7;
    int p8   = minValue + per12 * 8;
    int p9   = minValue + per12 * 9;
    int p10  = minValue + per12 *10;
    int p11  = minValue + per12 *11;


    if ( v <= p1            )  color = " QLabel { background-color : rgb(255,  0,0); ";
    if ( v >  p1 && v <= p3 )  color = " QLabel { background-color : rgb(255,128,0); ";
    if ( v >  p3 && v <= p6 )  color = " QLabel { background-color : rgb(255,255,0); ";
    if ( v >  p6 && v <= p9 )  color = " QLabel { background-color : rgb(128,255,0); ";
    if ( v >  p9            )  color = " QLabel { background-color : rgb(  0,255,0); ";


    color += " color : rgb(0,0,0) ; border-top:1px solid grey; border-left:1px solid grey ;padding-top:3px;padding-bottom:3px;} " ;

    return color;
}

void uploadTimesDialog::log(QString s)
{
    ui->plainTextEdit->appendPlainText(s);

}

void uploadTimesDialog::connectLabels()
{
    connect( ui->label_d1_h00       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d1_h01       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d1_h02       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d1_h03       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d1_h04       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d1_h05       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d1_h06       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d1_h07       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d1_h08       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d1_h09       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d1_h10       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d1_h11       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d1_h12       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d1_h13       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d1_h14       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d1_h15       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d1_h16       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d1_h17       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d1_h18       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d1_h19       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d1_h20       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d1_h21       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d1_h22       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d1_h23       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );

    connect( ui->label_d2_h00       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d2_h01       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d2_h02       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d2_h03       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d2_h04       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d2_h05       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d2_h06       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d2_h07       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d2_h08       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d2_h09       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d2_h10       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d2_h11       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d2_h12       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d2_h13       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d2_h14       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d2_h15       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d2_h16       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d2_h17       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d2_h18       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d2_h19       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d2_h20       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d2_h21       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d2_h22       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d2_h23       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );

    connect( ui->label_d3_h00       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d3_h01       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d3_h02       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d3_h03       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d3_h04       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d3_h05       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d3_h06       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d3_h07       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d3_h08       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d3_h09       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d3_h10       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d3_h11       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d3_h12       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d3_h13       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d3_h14       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d3_h15       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d3_h16       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d3_h17       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d3_h18       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d3_h19       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d3_h20       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d3_h21       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d3_h22       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d3_h23       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );

    connect( ui->label_d4_h00       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d4_h01       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d4_h02       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d4_h03       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d4_h04       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d4_h05       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d4_h06       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d4_h07       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d4_h08       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d4_h09       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d4_h10       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d4_h11       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d4_h12       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d4_h13       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d4_h14       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d4_h15       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d4_h16       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d4_h17       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d4_h18       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d4_h19       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d4_h20       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d4_h21       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d4_h22       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d4_h23       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );

    connect( ui->label_d5_h00       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d5_h01       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d5_h02       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d5_h03       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d5_h04       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d5_h05       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d5_h06       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d5_h07       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d5_h08       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d5_h09       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d5_h10       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d5_h11       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d5_h12       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d5_h13       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d5_h14       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d5_h15       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d5_h16       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d5_h17       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d5_h18       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d5_h19       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d5_h20       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d5_h21       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d5_h22       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d5_h23       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );

    connect( ui->label_d6_h00       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d6_h01       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d6_h02       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d6_h03       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d6_h04       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d6_h05       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d6_h06       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d6_h07       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d6_h08       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d6_h09       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d6_h10       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d6_h11       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d6_h12       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d6_h13       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d6_h14       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d6_h15       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d6_h16       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d6_h17       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d6_h18       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d6_h19       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d6_h20       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d6_h21       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d6_h22       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d6_h23       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );

    connect( ui->label_d7_h00       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d7_h01       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d7_h02       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d7_h03       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d7_h04       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d7_h05       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d7_h06       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d7_h07       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d7_h08       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d7_h09       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d7_h10       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d7_h11       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d7_h12       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d7_h13       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d7_h14       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d7_h15       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d7_h16       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d7_h17       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d7_h18       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d7_h19       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d7_h20       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d7_h21       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d7_h22       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
    connect( ui->label_d7_h23       , SIGNAL(Mouse_Pressed())       , this      , SLOT(labelClick()) );
}

uploadTimesDialog::~uploadTimesDialog()
{
    delete ui;
}


