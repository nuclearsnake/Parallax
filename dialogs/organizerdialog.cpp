#include "organizerdialog.h"
#include "ui_organizerdialog.h"
#include "organizerfilemovedialog.h"


#include <QDebug>
#include <QMessageBox>


organizerDialog::organizerDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::organizerDialog)
{
    Qt::WindowFlags flags = Qt::Window | Qt::WindowSystemMenuHint
                                | Qt::WindowMaximizeButtonHint
                                | Qt::WindowCloseButtonHint;
    this->setWindowFlags(flags);

    ui->setupUi(this);

    md00Source  = new QStandardItemModel(this);
    md01Origi   = new QStandardItemModel(this);
    md02Pano    = new QStandardItemModel(this);
    md03Hdr     = new QStandardItemModel(this);
    md04Tmp     = new QStandardItemModel(this);
    md05Post    = new QStandardItemModel(this);
    md06Res     = new QStandardItemModel(this);
    md07Final   = new QStandardItemModel(this);
    md08Print   = new QStandardItemModel(this);
    md09Wall    = new QStandardItemModel(this);
   /* md10Misc    = new QStandardItemModel(this);*/


    ui->lv00Source->setModel(md00Source);
    ui->lv01Origi->setModel(md01Origi);
    ui->lv02Pano->setModel(md02Pano);
    ui->lv03Hdr->setModel(md03Hdr);
    ui->lv04Tmp->setModel(md04Tmp);
    ui->lv05Post->setModel(md05Post);
    ui->lv06Res->setModel(md06Res);
    ui->lv07Final->setModel(md07Final);
    ui->lv08Print->setModel(md08Print);
    ui->lv09Wall->setModel(md09Wall);
    //ui->lv10Misc->setModel(md10Misc);

    //test
    //md05Post->appendRow(new QStandardItem("TEST1"));
    //md05Post->appendRow(new QStandardItem("TEST2"));

    viewList << ui->lv00Source << ui->lv01Origi << ui->lv02Pano  << ui->lv03Hdr << ui->lv04Tmp << ui->lv05Post
             << ui->lv06Res << ui->lv07Final << ui->lv08Print << ui->lv09Wall /* << ui->lv10Misc */ ;
    modelList << md00Source << md01Origi << md02Pano << md03Hdr << md04Tmp << md05Post << md06Res << md07Final << md08Print << md09Wall ;

    foreach(QListView* widget, viewList )
        {
            widget->setDragEnabled(true);
            widget->setSelectionMode(QAbstractItemView::ExtendedSelection);
            widget->viewport()->setAcceptDrops(true);
            widget->setDropIndicatorShown(true);
        }

    connect( md00Source     , SIGNAL(dataChanged(QModelIndex,QModelIndex))  , this  , SLOT(md00Dropped(QModelIndex,QModelIndex))  );
    connect( md01Origi      , SIGNAL(dataChanged(QModelIndex,QModelIndex))  , this  , SLOT(md01Dropped(QModelIndex,QModelIndex))  );
    connect( md02Pano       , SIGNAL(dataChanged(QModelIndex,QModelIndex))  , this  , SLOT(md02Dropped(QModelIndex,QModelIndex))  );
    connect( md03Hdr        , SIGNAL(dataChanged(QModelIndex,QModelIndex))  , this  , SLOT(md03Dropped(QModelIndex,QModelIndex))  );
    connect( md04Tmp        , SIGNAL(dataChanged(QModelIndex,QModelIndex))  , this  , SLOT(md04Dropped(QModelIndex,QModelIndex))  );
    connect( md05Post       , SIGNAL(dataChanged(QModelIndex,QModelIndex))  , this  , SLOT(md05Dropped(QModelIndex,QModelIndex))  );
    connect( md06Res        , SIGNAL(dataChanged(QModelIndex,QModelIndex))  , this  , SLOT(md06Dropped(QModelIndex,QModelIndex))  );
    connect( md07Final      , SIGNAL(dataChanged(QModelIndex,QModelIndex))  , this  , SLOT(md07Dropped(QModelIndex,QModelIndex))  );
    connect( md08Print      , SIGNAL(dataChanged(QModelIndex,QModelIndex))  , this  , SLOT(md08Dropped(QModelIndex,QModelIndex))  );
    connect( md09Wall       , SIGNAL(dataChanged(QModelIndex,QModelIndex))  , this  , SLOT(md09Dropped(QModelIndex,QModelIndex))  );

    connect( ui->pushButton_auto    , SIGNAL(clicked())     , this  , SLOT(autoOrg())  );
    connect( ui->pushButton_go      , SIGNAL(clicked())     , this  , SLOT(moveFiles())  );
    connect( ui->pushButton_cancel  , SIGNAL(clicked())     , this  , SLOT(close()) );

    /*
    //this->setAcceptDrops(true);
    originalFilesModel = new QStandardItemModel(this);
    testModel = new QStandardItemModel(this);


    connect ( ui->pushButton_cancel     , SIGNAL(clicked())     , this  , SLOT(close()) );
    connect ( ui->listView_test         , SIGNAL(viewportEntered()) , this , SLOT(test()) );
    connect ( ui->pushButton            , SIGNAL(clicked())         , this , SLOT(test()) );
    //connect ( ui->listView_test         , SIGNAL(indexesMoved(QModelIndexList)) , this , SLOT(test1(QModelIndexList))  );
    //connect ( testModel                 , SIGNAL(rowsInserted(QModelIndex,int,int))  , this , SLOT(test1(QModelIndex,int,int)) );
    connect ( testModel                 , SIGNAL(dataChanged(QModelIndex,QModelIndex)) , this , SLOT(md));

    */

    this->setWindowState(Qt::WindowMaximized);
}

void organizerDialog::md00Dropped(QModelIndex a,QModelIndex b) { this->itemMover(md00Source);   Q_UNUSED(a);Q_UNUSED(b); }
void organizerDialog::md01Dropped(QModelIndex a,QModelIndex b) { this->itemMover(md01Origi);    Q_UNUSED(a);Q_UNUSED(b);}
void organizerDialog::md02Dropped(QModelIndex a,QModelIndex b) { this->itemMover(md02Pano);     Q_UNUSED(a);Q_UNUSED(b);}
void organizerDialog::md03Dropped(QModelIndex a,QModelIndex b) { this->itemMover(md03Hdr);      Q_UNUSED(a);Q_UNUSED(b);}
void organizerDialog::md04Dropped(QModelIndex a,QModelIndex b) { this->itemMover(md04Tmp);      Q_UNUSED(a);Q_UNUSED(b);}
void organizerDialog::md05Dropped(QModelIndex a,QModelIndex b) { this->itemMover(md05Post);     Q_UNUSED(a);Q_UNUSED(b); }
void organizerDialog::md06Dropped(QModelIndex a,QModelIndex b) { this->itemMover(md06Res);      Q_UNUSED(a);Q_UNUSED(b); }
void organizerDialog::md07Dropped(QModelIndex a,QModelIndex b) { this->itemMover(md07Final);    Q_UNUSED(a);Q_UNUSED(b); }
void organizerDialog::md08Dropped(QModelIndex a,QModelIndex b) { this->itemMover(md08Print);    Q_UNUSED(a);Q_UNUSED(b); }
void organizerDialog::md09Dropped(QModelIndex a,QModelIndex b) { this->itemMover(md09Wall);     Q_UNUSED(a);Q_UNUSED(b); }

void organizerDialog::autoOrg()
{
    QList<int> indexesToBeRemoved;
    for (int i=md00Source->rowCount()-1;i>=0;--i)
        {
            //QModelIndex index = md00Source->index(i,0); //unused
            QString item = md00Source->index(i,0).data().toString();
            qDebug() << "(" << i << ")" << item;
            bool decided=false;
            //  ORIGINALS //
            if (
                        item.endsWith("_0.tif")
                    ||  item.endsWith("_1.tif")
                    ||  item.endsWith("_2.tif")
                    ||  item.endsWith("_3.tif")
                    ||  item.endsWith("_-1.tif")
                    ||  item.endsWith("_0.jpg")
                    ||  item.endsWith("_1.jpg")
                    ||  item.endsWith("_2.jpg")
                    ||  item.endsWith("_3.jpg")
                    ||  item.endsWith("_-1.jpg")
                )
                {
                    md01Origi->appendRow(new QStandardItem(item));
                    indexesToBeRemoved << i;
                    decided = true;
                }
            // FINAL //
            if (
                    !decided &
                    (
                            item.endsWith("r.jpg")
                        ||  item.endsWith("rc.jpg")
                        ||  item.endsWith("rnc.jpg")
                        ||  item.endsWith("rn.jpg")
                        ||  item.endsWith("rnc1.jpg")
                        ||  item.endsWith("r copy.jpg")
                        ||  item.endsWith("rc copy.jpg")
                        ||  item.endsWith("rwm.jpg")
                        ||  item.endsWith("r1wm.jpg")
                        ||  item.endsWith("r1.jpg")
                        ||  item.endsWith("r2wm.jpg")
                        ||  item.endsWith("r2.jpg")
                    )
               )
                {
                    md07Final->appendRow(new QStandardItem(item));
                    indexesToBeRemoved << i;
                    decided = true;
                }

            // PRINT //
            if (
                    !decided &
                    (
                            ( item.endsWith(".psd") & item.contains("PRINT") )
                        ||  ( item.endsWith(".jpg") & item.contains("PRINT") )
                    )
               )
                {
                    md08Print->appendRow(new QStandardItem(item));
                    indexesToBeRemoved << i;
                    decided = true;
                }
            // TONEMAPPED //
            if (
                    !decided &
                    (
                            ( item.endsWith(".tif") & item.contains("tonemapped") )
                        ||  ( item.endsWith(".jpg") & item.contains("tonemapped") )
                        ||    item.endsWith(".xmp")
                        ||    item.endsWith(".dps")
                        ||  ( item.endsWith(".jpg") & item.contains("dph_") )
                        ||  ( item.endsWith(".tif") & item.contains("dph_") )
                        ||  ( item.endsWith(".jpg") & item.contains("DPH_") )
                        ||  ( item.endsWith(".tif") & item.contains("DPH_") )

                    )
               )
                {
                    md04Tmp->appendRow(new QStandardItem(item));
                    indexesToBeRemoved << i;
                    decided = true;
                }
            // PANORAMA //
            if (
                    !decided &
                    (
                             item.endsWith(".pts")
                        || ( item.endsWith(".tif") & item.contains("_ev") )
                        || ( item.endsWith(".jpg") & item.contains("_ev") )
                    )
                )
                {
                    md02Pano->appendRow(new QStandardItem(item));
                    indexesToBeRemoved << i;
                    decided = true;
                }
            // HDR //
            if (
                    !decided &
                    (
                        item.endsWith(".hdr")
                    )
                )
                {
                    md03Hdr->appendRow(new QStandardItem(item));
                    indexesToBeRemoved << i;
                    decided = true;
                }

            // RESIZED //
            if (
                    !decided &
                    (
                            item.endsWith("r.psd")
                        ||  item.endsWith("rc.psd")
                        ||  item.endsWith("rn.psd")
                        ||  item.endsWith("rnc.psd")
                        ||  item.endsWith("rnc1.psd")
                        ||  item.endsWith("rwm.psd")
                        ||  item.endsWith("r1wm.psd")
                        ||  item.endsWith("r1.psd")
                        ||  item.endsWith("r2wm.psd")
                        ||  item.endsWith("r2.psd")
                    )
               )
                {
                    md06Res->appendRow(new QStandardItem(item));
                    indexesToBeRemoved << i;
                    decided = true;
                }

            // POSTPROCESS //
            if (
                    !decided &
                    (
                        item.endsWith(".psd")
                    )
               )
                {
                    md05Post->appendRow(new QStandardItem(item));
                    indexesToBeRemoved << i;
                    decided = true;
                }


        }


    qDebug() << indexesToBeRemoved;

    foreach ( int i , indexesToBeRemoved )
        {
            md00Source->removeRow(i);
        }

    foreach( QStandardItemModel* mdl, modelList )
        {
            mdl->sort(0,Qt::AscendingOrder);
        }
}

void organizerDialog::moveFiles()
{
    qDebug() << "moveFiles";


    QString destinationFolder;
    QList<QPair<QString,QString> > moveList;

    QList<QString> folderList;
    folderList << ""
                              << "01 - Eredeti képek"
                              << "02 - Panoráma"
                              << "03 - HDRI"
                              << "04 - Tonemapped"
                              << "05 - Utómunka"
                              << "06 - Átméretezett"
                              << "07 - Kész"
                              << "08 - Print"
                              << "09 - Wallpaper" ;


    // k from 1 -- skipping source model !!!
    for (int k=1;k<modelList.count();++k )
       {
            qDebug() << k << modelList.at(k);
            destinationFolder = folderList.at(k);
            for (int i=0;i<modelList.at(k)->rowCount();++i)
                {
                    QString relativePath = modelList.at(k)->index(i,0).data().toString() ;
                    QString fileName     = relativePath.split("/").last();
                    QString newPath      = destinationFolder+"/"+fileName;
                    QString fullPathFrom = workingFolder+"/"+relativePath;
                    QString fullPathTo   = workingFolder+"/"+newPath;

                    qDebug() << "relativePath =" << relativePath;
                    qDebug() << "fileName     =" << fileName;
                    qDebug() << "fullPathFrom =" << fullPathFrom;
                    qDebug() << "fullPathTo   =" << fullPathTo;
                    qDebug() << "newPath      =" << newPath;
                    bool pathSame = ( relativePath == newPath );
                    if (!pathSame) moveList.append(qMakePair(fullPathFrom,fullPathTo));
                    qDebug() << pathSame;

                }
       }

    /*
    // 01 ORIGINALS //
    destinationFolder = "01 - Eredeti képek";
    for (int i=0;i<md01Origi->rowCount();++i)
        {
            QString relativePath = md01Origi->index(i,0).data().toString() ;
            QString fileName     = relativePath.split("/").last();
            QString newPath      = destinationFolder+"/"+fileName;
            QString fullPathFrom = workingFolder+"/"+relativePath;
            QString fullPathTo   = workingFolder+"/"+newPath;

            qDebug() << "relativePath =" << relativePath;
            qDebug() << "fileName     =" << fileName;
            qDebug() << "fullPathFrom =" << fullPathFrom;
            qDebug() << "fullPathTo   =" << fullPathTo;
            qDebug() << "newPath      =" << newPath;
            bool pathSame = ( relativePath == newPath );
            if (!pathSame) moveList.append(qMakePair(fullPathFrom,fullPathTo));
            qDebug() << pathSame;

        }

    // 02 - PANORAMA //
    destinationFolder = "02 - Panoráma";
    for (int i=0;i< md02Pano->rowCount();++i)
        {
            QString relativePath = md02Pano->index(i,0).data().toString() ;
            QString fileName     = relativePath.split("/").last();
            QString newPath      = destinationFolder+"/"+fileName;
            QString fullPathFrom = workingFolder+"/"+relativePath;
            QString fullPathTo   = workingFolder+"/"+newPath;

            qDebug() << "relativePath =" << relativePath;
            qDebug() << "fileName     =" << fileName;
            qDebug() << "fullPathFrom =" << fullPathFrom;
            qDebug() << "fullPathTo   =" << fullPathTo;
            qDebug() << "newPath      =" << newPath;
            bool pathSame = ( relativePath == newPath );
            if (!pathSame) moveList.append(qMakePair(fullPathFrom,fullPathTo));
            qDebug() << pathSame;

        }

    // 03 - HDR //
    destinationFolder = "03 - HDRI";
    for (int i=0;i< md03Hdr->rowCount();++i)
        {
            QString relativePath = md03Hdr->index(i,0).data().toString() ;
            QString fileName     = relativePath.split("/").last();
            QString newPath      = destinationFolder+"/"+fileName;
            QString fullPathFrom = workingFolder+"/"+relativePath;
            QString fullPathTo   = workingFolder+"/"+newPath;

            qDebug() << "relativePath =" << relativePath;
            qDebug() << "fileName     =" << fileName;
            qDebug() << "fullPathFrom =" << fullPathFrom;
            qDebug() << "fullPathTo   =" << fullPathTo;
            qDebug() << "newPath      =" << newPath;
            bool pathSame = ( relativePath == newPath );
            if (!pathSame) moveList.append(qMakePair(fullPathFrom,fullPathTo));
            qDebug() << pathSame;

        }
    // 04 - TONEMAPPED //
    destinationFolder = "04 - Tonemapped";
    for (int i=0;i< md04Tmp->rowCount();++i)
        {
            QString relativePath = md04Tmp->index(i,0).data().toString() ;
            QString fileName     = relativePath.split("/").last();
            QString newPath      = destinationFolder+"/"+fileName;
            QString fullPathFrom = workingFolder+"/"+relativePath;
            QString fullPathTo   = workingFolder+"/"+newPath;

            qDebug() << "relativePath =" << relativePath;
            qDebug() << "fileName     =" << fileName;
            qDebug() << "fullPathFrom =" << fullPathFrom;
            qDebug() << "fullPathTo   =" << fullPathTo;
            qDebug() << "newPath      =" << newPath;
            bool pathSame = ( relativePath == newPath );
            if (!pathSame) moveList.append(qMakePair(fullPathFrom,fullPathTo));
            qDebug() << pathSame;

        }
    */

    qDebug() << moveList;
    int filesToMove = moveList.count();
    qDebug() << "files to move:" << filesToMove;

    if (filesToMove)
        {

            organizerFileMoveDialog moveDialog;
            //moveDialog.open();
            //moveDialog.show();
            moveDialog.setMoveList(moveList);
            if ( moveDialog.exec() ) close();

            //moveDialog.runMove();


            /*
            for (int i=0;i<filesToMove;++i)
                {

                    qDebug() << i << "/" << filesToMove;
                    QString from = moveList.at(i).first;
                    QString to   = moveList.at(i).second;
                    QFileInfo fi(to);
                    QString targetPath= fi.absolutePath();
                    QDir di(targetPath);
                    bool targetExists = di.exists();
                    bool targetCreated ;
                    if (!targetExists)   targetCreated = di.mkpath(targetPath);
                    qDebug() << "from      :" << from;
                    qDebug() << "to        :" << to;
                    qDebug() << "tagetPath :" << targetPath;
                    qDebug() << "target path ok :" << targetExists;
                    qDebug() << "taget created  :" << targetCreated;
                    //QDir handler;
                    //bool success = handler.rename(from,to);
                    QFile handler;
                    bool success = handler.copy(from,to);
                    if (success) handler.remove(from);
                    qDebug() << "success:" << success;
                    moveDialog.updateStatus(from,to,i+1,filesToMove);
                }
              */
            //moveDialog.showClose();
            //moveDialog.exec();
        }
    else
        {
            QMessageBox Msgbox;
            Msgbox.setText("No files to move!");
            Msgbox.exec();
        }

}

void organizerDialog::itemMover(QStandardItemModel* modelTO)
{
    qDebug() << "MOVE";



    //qDebug() << md01Origi->index(md01Origi->rowCount()-1,0).data().toString();
    //qDebug() << "------------------------------------";
    foreach( QStandardItemModel* modelFrom, modelList )
        {
            //bool match=(modelFrom==modelTO); //unused
            //qDebug() << modelFrom << modelTO << match ;

            if (modelFrom!=modelTO)
                {
                    for (int i=0;i<modelTO->rowCount();++i)
                        {
                            QString moved = modelTO->index(i,0).data().toString();
                            //qDebug() << moved;
                            if ( modelFrom->findItems(moved,Qt::MatchExactly).count() > 0)
                                 modelFrom->removeRow(modelFrom->findItems(moved,Qt::MatchExactly).first()->index().row() );
                        }
                }

        }

    modelTO->sort(0,Qt::AscendingOrder);
    // remove duplicates (self copy)
    QString prevItemTo="";
    for (int i=0;i<modelTO->rowCount();++i)
        {
            QString itemTo=modelTO->index(i,0).data().toString();
            if (itemTo==prevItemTo) modelTO->removeRow(i);
            prevItemTo=itemTo;
        }



    //qDebug() << moved;
    //qDebug() << md00Source->findItems(moved,Qt::MatchRecursive).first()->data().toString();
    //int row=md00Source->findItems(moved,Qt::MatchRecursive).first()->index().row();
    //qDebug() << row;
    //if (row) md00Source->removeRow(row);
    //md00Source->removeRow()
    //md01Origi->sort(0,Qt::AscendingOrder);



}

/*
void organizerDialog::test2(QStandardItem* item)
{
    qDebug() << item->data().toString();
}
*/
/*
void organizerDialog::test1(QModelIndex ia,QModelIndex ib)
{
    qDebug() << testModel->rowCount();
    testModel->sort(0,Qt::AscendingOrder);
    QString prevItemTo = "";
    for (int i=0; i<testModel->rowCount();++i)
        {
            //qDebug() << i << testModel->index(i,0).data().toString();
            QString itemTo = testModel->index(i,0).data().toString();
            if (itemTo==prevItemTo) testModel->removeRow(i);
            prevItemTo=itemTo;
            for (int j=0;j<originalFilesModel->rowCount();++j)
                {
                    QString itemFrom = originalFilesModel->index(j,0).data().toString();
                    qDebug() << itemFrom << itemTo ;
                    if (itemFrom==itemTo)
                        {
                            qDebug() << "MATCH";
                            //testModel->setItem(i,1,new QStandardItem ( originalFilesModel->index(j,1).data().toString() ) );
                            originalFilesModel->removeRow(j);
                        }
                }
        }
}
*/
/*
Qt::DropActions QAbstractItemModel::supportedDragActions() const
 {
     return  Qt::MoveAction;
 }

Qt::DropActions QAbstractItemModel::supportedDropActions() const
 {
     return  Qt::MoveAction;
 }



void organizerDialog::dropEvent(QDropEvent *event)
{
    qDebug() << "dropEvent";
}
*/

/*
Qt::DropActions QAbstractItemView::startDrag() const
 {
        return  Qt::MoveAction;
}
*/

/*  REIMPLEMENT  */
/*
Qt::DropActions QAbstractItemModel::supportedDropActions() const
 {
     return  Qt::MoveAction;
 }

Qt::DropActions QAbstractItemModel::supportedDragActions() const
 {
     return  Qt::MoveAction;
 }

void QAbstractItemView::dropEvent(QDropEvent *event)
{
    qDebug() << "dropEvent";
}

void QAbstractItemView::dragEnterEvent(QDragEnterEvent *event)
{
    qDebug() << "dragEnterEcent";

}


void organizerDialog::dropEvent(QDropEvent *event)
{
    qDebug() << "dropEvent WIN";
}

void organizerDialog::dragEnterEvent(QDragEnterEvent *event)
{
    //event->accept(); // !!!!!! important
    qDebug() << "dragEnter";
    qDebug() << event;
}
*/

/*  /REIMPLEMENT  */

/*
void QAbstract::dropEvent(QDropEvent *event)
{
    qDebug() << "dropEvent";
}
*/



void organizerDialog::init()
{
    int i=-1;
    QDirIterator iterator(QDir(workingFolder).absolutePath(), QDirIterator::Subdirectories);
    while (iterator.hasNext())
        {
          iterator.next();
          if (!iterator.fileInfo().isDir())
            {
                ++i;
                QString filename = iterator.fileName();
                QString relativePath = iterator.filePath().replace(workingFolder+"/","");
                md00Source->setItem(i,0,new QStandardItem(relativePath));

            }
       }


    /*
    qDebug() << "<-----------------organizer------------------>";
    qDebug() << workingFolder;
    ui->label_folder->setText(workingFolder);







    int i=-1;
    QDirIterator iterator(QDir(workingFolder).absolutePath(), QDirIterator::Subdirectories);
    while (iterator.hasNext())
        {
          iterator.next();
          if (!iterator.fileInfo().isDir())
            {
                ++i;
                QString filename = iterator.fileName();
                QString relativePath = iterator.filePath().replace(workingFolder,"");
                qDebug() << "fileName"  << filename;
                qDebug() << "path    "  << relativePath;



                originalFilesModel->setItem(i,0,new QStandardItem(relativePath));
                originalFilesModel->setItem(i,1,new QStandardItem(relativePath));

                //qDebug() << filename;
                //++fileCount;
                //fileSize+=iterator.fileInfo().size();
                //if (filename.endsWith(".nut") && filename != "main.nut" && filename != "info.nut")
                //   qDebug("Found %s matching pattern.", qPrintable(filename));
            }
       }

    ui->listView_files->setModel(originalFilesModel);
    ui->listView_files->setDragEnabled(true);
    ui->listView_files->setSelectionMode(QAbstractItemView::ExtendedSelection);
    ui->listView_files->viewport()->setAcceptDrops(true);
    ui->listView_files->setDropIndicatorShown(true);
    //ui->listView_files->set
    //ui->listView_files->setDragDropMode(QAbstractItemView::InternalMove );
    ui->listView_files->setDragDropMode(QAbstractItemView::DragDrop );
    originalFilesModel->sort(0,Qt::AscendingOrder);

    ui->listView_test->setModel(testModel);
    ui->listView_test->setDragEnabled(true);
    ui->listView_test->setSelectionMode(QAbstractItemView::ExtendedSelection);
    ui->listView_test->viewport()->setAcceptDrops(true);
    ui->listView_test->setDropIndicatorShown(true);
    //ui->listView_test->setDragDropMode(QAbstractItemView::InternalMove);
    ui->listView_test->setDragDropMode(QAbstractItemView::DragDrop );


    //ui->tableView->setModel(originalFilesModel);

*/

}



/*
void organizerDialog::test()
{
    qDebug() << testModel->rowCount();
    for (int i=0;i<testModel->rowCount();++i)
        {
            qDebug() << "-------------" << i << "--------------";
            qDebug() << originalFilesModel->index(i,0).data().toString();
            qDebug() << originalFilesModel->index(i,1).data().toString();
            qDebug() << "-------------" << i << "--------------";
            qDebug() << testModel->index(i,0).data().toString();
            qDebug() << testModel->index(i,1).data().toString();
        }
}
*/

void organizerDialog::setFolder(QString f)
{
    workingFolder = f;
}


organizerDialog::~organizerDialog()
{
    delete ui;
}


