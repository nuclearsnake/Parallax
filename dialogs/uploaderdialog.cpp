#include "uploaderdialog.h"
#include "ui_uploaderdialog.h"

#include <QTimer>
#include <QWebFrame>
#include <QWebHistory>
#include <QWebView>
#include <QDesktopServices>


uploaderDialog::uploaderDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::uploaderDialog)
{
    ui->setupUi(this);

    QTimer::singleShot(100,this,SLOT(go()));
    fileModel   = new QFileSystemModel(this);

    ui->listView->setModel(fileModel);
    ui->listView->setDragEnabled(true);

    connect ( ui->listView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(preview(QModelIndex)));
}

void uploaderDialog::go()
{
    ui->webView->setUrl(QUrl("http://www.realitydream.hu/upload1"));
    QTimer::singleShot(2000,this,SLOT(fill()));
    fileModel->setRootPath(folder);
    fileModel->setFilter( QDir::NoDotAndDotDot | QDir::Files )  ;

        QStringList filters;
        filters << "*.jpg";

        fileModel->setNameFilters(filters);
        fileModel->setNameFilterDisables(false);
    ui->listView->setRootIndex(fileModel->index(folder));
    qDebug() << folder;
}

void uploaderDialog::preview(QModelIndex index)
{
    //return index.sibling(index.row(),col).data().toString();
    QDesktopServices::openUrl(QUrl::fromLocalFile(folder+"/"+index.sibling(index.row(),0).data().toString()));
}

void uploaderDialog::fill()
{
    ui->webView->page()->mainFrame()->evaluateJavaScript("$('#top').hide();$('#footer').hide();");
    ui->webView->page()->mainFrame()->evaluateJavaScript("document.getElementById('UPtitle').value='"+title+"';");
    ui->webView->page()->mainFrame()->evaluateJavaScript("document.getElementById('UPtitle_sec').value='"+title2+"';");
    ui->webView->page()->mainFrame()->evaluateJavaScript("document.getElementById('UPdate').value='"+date+"';");
    ui->webView->page()->mainFrame()->evaluateJavaScript("document.getElementById('UPparid').value='"+ QString::number(id) +"';");
    ui->webView->page()->mainFrame()->evaluateJavaScript("document.getElementById('UPkeywords').innerHTML='"+keywords+"';");

}

uploaderDialog::~uploaderDialog()
{
    delete ui;
}
