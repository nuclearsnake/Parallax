#ifndef YESNODIALOG_H
#define YESNODIALOG_H

#include <QDialog>

namespace Ui {
class yesNoDialog;
}

class yesNoDialog : public QDialog
{
    Q_OBJECT

public:
    explicit yesNoDialog(QWidget *parent = 0);
    ~yesNoDialog();
    QString message;

    void setMessage(QString text);
    void setDetail(QString text);
private:
    Ui::yesNoDialog *ui;
};

#endif // YESNODIALOG_H
