#include "siteupdater.h"
#include "ui_siteupdater.h"
#include "addsitedialog.h"
//#include "config.cpp"

#include <QNetworkDiskCache>
#include <QSqlField>
#include <QSqlRecord>

siteUpdater::siteUpdater(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::siteUpdater)
{
    ui->setupUi(this);

    Qt::WindowFlags flags = Qt::Window | Qt::WindowSystemMenuHint
                                | Qt::WindowMaximizeButtonHint
                                | Qt::WindowCloseButtonHint;
    this->setWindowFlags(flags);
    this->setWindowTitle("Site Updater | Parallax");

    ui->radioButton_single->setEnabled(false);
    ui->plainTextEdit_2->setVisible(false);
    ui->plainTextEdit->setVisible(false);
    ui->webView->setVisible(false);


    tableModel = new QStandardItemModel(this);
    ui->tableView->setModel(tableModel);
    QStringList labels;
    //        0      1           2          3        4          5          6          7          8          9
    labels << "#" << "status" << "title" << "url" << "V old" << "V new" << "F old" << "F new" << "C old" << "C new";
    tableModel->setHorizontalHeaderLabels(labels);
    ui->tableView->setColumnWidth(0,20);
    ui->tableView->setColumnWidth(1,60);
    ui->tableView->setColumnWidth(2,140);
    ui->tableView->setColumnWidth(3,620);
    ui->tableView->setColumnWidth(4,60);
    ui->tableView->setColumnWidth(5,60);
    ui->tableView->setColumnWidth(6,60);
    ui->tableView->setColumnWidth(7,60);
    ui->tableView->setColumnWidth(8,60);
    ui->tableView->setColumnWidth(9,60);


    gPlusLoggedin = false;


    // network cache
    QNetworkDiskCache *cache = new QNetworkDiskCache(this);
    cache->setCacheDirectory(QStandardPaths::standardLocations( QStandardPaths::DataLocation ).at(0) + "/cache");
    cache->setMaximumCacheSize(1073741824);

    // web views
    //ui->webView->page()->networkAccessManager()->setCache(cache);


    QWebSettings::globalSettings()->setMaximumPagesInCache(1);
    QWebSettings::globalSettings()->setObjectCacheCapacities(0, 0, 0);
    QWebSettings::globalSettings()->setOfflineStorageDefaultQuota(0);
    QWebSettings::globalSettings()->setOfflineWebApplicationCacheQuota(0);
    QWebSettings::globalSettings()->clearIconDatabase();
    QWebSettings::globalSettings()->clearMemoryCaches();

    //connect( ui->pushButton, SIGNAL(clicked()),this,SLOT(go()));

    connect( ui->webView , SIGNAL(loadFinished(bool) ) , this , SLOT(nextUrlProxy()) );



}

void siteUpdater::setSingle(int id, QString title)
{
    singleId    = id;
    singleTitle = title;
    ui->radioButton_single->setEnabled(true);
    ui->radioButton_single->setChecked(true);
    ui->radioButton_updated->setEnabled(false);
    ui->radioButton_uploaded->setEnabled(false);
    ui->comboBox_updated->setEnabled(false);
    ui->comboBox_uploaded->setEnabled(false);
    ui->radioButton_single->setText("Single: "+title+" (ID:"+QString::number(id)+")");

}

void siteUpdater::nextUrlProxy()
{
    QTimer::singleShot(1000, this , SLOT( nextUrl() ) );
}

void siteUpdater::nextUrl()
{

    //ui->webView->disconnect();
    //webView->disconnect();
    QWebSettings::globalSettings()->clearMemoryCaches();
    ui->webView->history()->clear();
    //webView->history()->clear();

    if (currentUrl.contains("plus.google") & !gPlusLoggedin)
        {
            qDebug() << "G+ logging in";
            ui->webView->page()->mainFrame()->evaluateJavaScript("document.getElementById('Email').value='hungarianskies';");
            ui->webView->page()->mainFrame()->evaluateJavaScript("document.getElementById('Passwd').value='Hanem793';");
            ui->webView->page()->mainFrame()->evaluateJavaScript("document.getElementById('signIn').click();");
            gPlusLoggedin = true;

            //connect( ui->webView , SIGNAL(loadFinished(bool) ) , this , SLOT(nextUrl()) );
            return;
        }

    QString pageContent ="";
    pageContent = ui->webView->page()->mainFrame()->toHtml();
    //pageContent = webView->page()->mainFrame()->toHtml();
    ui->plainTextEdit_2->setPlainText(pageContent);



    pG->getStats(ui->webView->url().toString(),pageContent);
    //pG->getStats(webView->url().toString(),pageContent);


    log("    site         : " + model->record(currentItem-1).value("site").toString()   );
    log("    upload title : " + pG->origTitle   );
    log("    upload date  : " + pG->submitDate  );
    log("    last update  : " + model->record(currentItem-1).value("updated").toString()    );
    log("    views        : " + model->record(currentItem-1).value("views").toString()+" -> "+QString::number(pG->statViews)        );
    log("    favorites    : " + model->record(currentItem-1).value("favs").toString()+" -> "+QString::number(pG->statFavs)          );
    log("    comments     : " + model->record(currentItem-1).value("comments").toString()+" -> "+QString::number(pG->statComments)  );






    //bool allValid=false; //UNUSED
    bool viewsValid=false;
    bool favsValid=false;
    bool commValid=false;

    if (pG->statViews >= model->record(currentItem-1).value("views").toInt() - 2 )
        {
            viewsValid =true;
            logHtml("&nbsp; &nbsp; &nbsp; views is <span style=\"color:green;font-weight:bold;\">VALID</span>");
        }
        else
        {
            logHtml("&nbsp; &nbsp; &nbsp; views is <span style=\"color:red;font-weight:bold;\">INVALID</span>");
        }

    if (pG->statFavs >= model->record(currentItem-1).value("favs").toInt() - 33 )
        {
            favsValid =true;
            logHtml("&nbsp; &nbsp; &nbsp; favs is <span style=\"color:green;font-weight:bold;\">VALID</span>");
        }
        else
        {
            logHtml("&nbsp; &nbsp; &nbsp; favs is <span style=\"color:red;font-weight:bold;\">INVALID</span>");
        }

    if (pG->statComments >= model->record(currentItem-1).value("comments").toInt() - 5 )
        {
            commValid =true;
            logHtml("&nbsp; &nbsp; &nbsp; comments is <span style=\"color:green;font-weight:bold;\">VALID</span>");
        }
        else
        {
            logHtml("&nbsp; &nbsp; &nbsp; comments is <span style=\"color:red;font-weight:bold;\">INVALID</span>");
        }

    QStandardItem *cell1 = new QStandardItem(QString::number(pG->statViews));
    (viewsValid) ? cell1->setData(QBrush(Qt::black), Qt::ForegroundRole) : cell1->setData(QBrush(Qt::red), Qt::ForegroundRole) ;
    tableModel->setItem(currentItem-1,5,cell1);


    QStandardItem *cell2 = new QStandardItem(QString::number(pG->statFavs));
    (favsValid) ? cell2->setData(QBrush(Qt::black), Qt::ForegroundRole) : cell2->setData(QBrush(Qt::red), Qt::ForegroundRole) ;
    tableModel->setItem(currentItem-1,7,cell2);

    QStandardItem *cell3 = new QStandardItem(QString::number(pG->statComments));
    (commValid) ? cell3->setData(QBrush(Qt::black), Qt::ForegroundRole) : cell3->setData(QBrush(Qt::red), Qt::ForegroundRole) ;
    tableModel->setItem(currentItem-1,9,cell3);


    if ( viewsValid & favsValid & commValid )
        {
            QStandardItem *cell = new QStandardItem("DONE");
            cell->setData(QBrush(Qt::green), Qt::BackgroundRole);
            tableModel->setItem(currentItem-1,1,cell);

            logHtml("&nbsp; &nbsp; &nbsp; <span style=\"color:green;font-weight:bold;\">ALL DATA VALID... SAVING</span>");

            QString date ; date.sprintf("%d-%02d-%02d" ,  QDate::currentDate().year()  , QDate::currentDate().month() , QDate::currentDate().day());

            /*  save queries:::: */
            /*
            QString date ; date.sprintf("%d-%02d-%02d" ,  QDate::currentDate().year()  , QDate::currentDate().month() , QDate::currentDate().day());
            QString updateQuery = "UPDATE sites (views,favs,comments,update) VALUES ("
                    +  QString::number( pG->statViews ) +","
                    + QString::number(pG->statFavs) +","
                    + QString::number(pG->statComments) +",'"
                    + date
                    + "' ) WHERE id = "
                    + model->record(currentItem-1).value("siteid").toString()  ;
            */
            QString updateQuery = "UPDATE sites SET "
                    " views = "    + QString::number( pG->statViews )   + ","
                    " favs = "     + QString::number(pG->statFavs)      + ","
                    " comments = " + QString::number(pG->statComments)  + ","
                    " updated = '"    + date + "'"
                    " WHERE id = "
                    + model->record(currentItem-1).value("siteid").toString()  ;

            /*
            QString logQuery = "INSERT INTO siteUpdateLog (siteid,views,favs,comments,update_date) VALUES ("
                    + model->record(currentItem-1).value("siteid").toString()  + ","
                    +  QString::number( pG->statViews ) +","
                    + QString::number(pG->statFavs) +","
                    + QString::number(pG->statComments) +",'"
                    + date
                    + "' )  "
                     ;
            */

            QString logQuery = "INSERT OR REPLACE INTO siteUpdateLog (id,siteid,views,favs,comments,update_date) VALUES ( ( SELECT id FROM siteUpdateLog WHERE siteId = "
                    + model->record(currentItem-1).value("siteid").toString()
                    + " AND update_date = '" + date + "'),"
                    + model->record(currentItem-1).value("siteid").toString()  + ","
                    +  QString::number( pG->statViews ) +","
                    + QString::number(pG->statFavs) +","
                    + QString::number(pG->statComments) +",'"
                    + date
                    + "' )  "
                     ;
            // !!!
           db.exec(logQuery);
           db.exec(updateQuery);

            logHtml ( updateQuery );
            logHtml ( logQuery );




        }
        else
        {
            logHtml("&nbsp; &nbsp; &nbsp; <span style=\"color:red;font-weight:bold;\">THERE IS SOME ERROR(S)... NO SAVING</span>");
            retries++;
            QStandardItem *cell9 = new QStandardItem("Retry #"+QString::number(retries));
            //cell9->setData(QBrush(Qt::yellow), Qt::BackgroundRole);
            tableModel->setItem(currentItem-1,1,cell9);
            ui->tableView->setCurrentIndex( tableModel->index(currentItem-1,0) );
            qDebug() << "  A" << retries;
            QCoreApplication::processEvents();


        }

    if (currentItem <= allItemCount)
    {
        qDebug() << "  B" << retries;
        if (retries==0)
            {
                qDebug() << "  C" << retries;
                currentItem++;
            }
            else
            {
                qDebug() << "  D" << retries;
                if (retries==3)
                    {
                        qDebug() << "  E" << retries;
                        retries = 0;

                        QStandardItem *cell = new QStandardItem("FAILED");
                        cell->setData(QBrush(Qt::red), Qt::BackgroundRole);
                        tableModel->setItem(currentItem-1,1,cell);


                        currentItem++;
                        qDebug() << "  F" << retries;

                    }
            }

        qDebug() << currentItem << " / " << allItemCount;
        if (currentItem <= allItemCount) loadUrl(); //QTimer::singleShot( 1000 , this , SLOT( loadUrl() ) );
            else
            {
                ui->plainTextEdit->appendPlainText("DONE!");
                //ui->pushButton->setDisabled(false);
            }
        //connect( ui->webView , SIGNAL(loadFinished(bool) ) , this , SLOT(nextUrl()) );
        //connect( webView , SIGNAL(loadFinished(bool) ) , this , SLOT(nextUrl()) );
    }




}

void siteUpdater::setDb(QSqlDatabase database)
{
    qDebug() << "setdb";
    qDebug() << database;
    db = database;
    QSqlQueryModel *mod = new QSqlQueryModel(this);
    mod->setQuery("SELECT * FROM archive",db);
    qDebug() << mod->rowCount();
}


void siteUpdater::go()
{

    model = new QSqlQueryModel;
    QString query;

    retries = 0;

    if (ui->radioButton_uploaded->isChecked())
        {
            QString frame = "";

            if (ui->comboBox_uploaded->currentText()== "2 Days")   frame="3";
            if (ui->comboBox_uploaded->currentText()== "1 Week")   frame="8";
            if (ui->comboBox_uploaded->currentText()== "2 Weeks")  frame="14";
            if (ui->comboBox_uploaded->currentText()== "1 Month")  frame="32";
            if (ui->comboBox_uploaded->currentText()== "2 Months") frame="63";
            //TEEEST
            //query = "SELECT p.id id,p.title, s.url,s.title 'original title',s.views,s.favs,s.comments ,s.updated,s.site,s.id siteid FROM archive p LEFT JOIN sites s ON s.projectid=p.id WHERE s.id = 621  ";
            query = "SELECT p.id id,p.title, s.url,s.title 'original title',s.views,s.favs,s.comments ,s.updated,s.site,s.id siteid FROM archive p LEFT JOIN sites s ON s.projectid=p.id WHERE s.url NOT NULL AND (julianday(Date('now')) - julianday(s.date)) <  "+frame;
        }

    if (ui->radioButton_updated->isChecked())
        {
            QString frame = "";

            if (ui->comboBox_updated->currentText()== "1 Week")   frame="8";
            if (ui->comboBox_updated->currentText()== "1 Month")  frame="32";
            if (ui->comboBox_updated->currentText()== "2 Months") frame="62";
            if (ui->comboBox_updated->currentText()== "3 Months") frame="92";
            if (ui->comboBox_updated->currentText()== "1 Year")   frame="365";
            // SKIP G+ !!!
            //query = "SELECT p.id id,p.title, s.url,s.title 'original title',s.views,s.favs,s.comments ,s.updated,s.site,s.id siteid FROM archive p LEFT JOIN sites s ON s.projectid=p.id WHERE s.url NOT NULL AND s.site!='G+' AND (julianday(Date('now')) - julianday(s.updated)) >  "+frame;
            query = "SELECT p.id id,p.title, s.url,s.title 'original title',s.views,s.favs,s.comments ,s.updated,s.site,s.id siteid FROM archive p LEFT JOIN sites s ON s.projectid=p.id WHERE s.url NOT NULL  AND (julianday(Date('now')) - julianday(s.updated)) >  "+frame;
            //qDebug() << query;
        }

    if (ui->radioButton_single->isChecked())
        {
            query = "SELECT p.id id,p.title, s.url,s.title 'original title',s.views,s.favs,s.comments ,s.updated,s.site,s.id siteid FROM archive p LEFT JOIN sites s ON s.projectid=p.id WHERE s.url NOT NULL AND p.id="+QString::number(singleId);
        }

    model->setQuery(query,db);
    while(model->canFetchMore()) model->fetchMore();

    for (int i = 0; i< model->rowCount();++i)
        {
            QSqlRecord record =  model->record(i);
            QList<QStandardItem*> row;

            //0
            row << new QStandardItem( QString::number( i+1 ) );

            //1 status
            row << new QStandardItem("");

            //2
            row << new QStandardItem( record.field(1).value().toString() );

            //3
            row << new QStandardItem( record.field(2).value().toString() );

            //4 V
            row << new QStandardItem( record.field(4).value().toString() );
            //5
            row << new QStandardItem( "" );

            //6 F
            row << new QStandardItem( record.field(5).value().toString() );
            //7
            row << new QStandardItem( "" );

            //8 C
            row << new QStandardItem( record.field(6).value().toString() );
            //9
            row << new QStandardItem( "" );

            tableModel->appendRow(row);
        }

    //return;
    log("Fetching data for " + QString::number(model->rowCount()) + " items!" );
    log("");

    pG = new tools;

    if (model->rowCount()>0)
        {
            allItemCount = model->rowCount();
            currentItem  = 1;
            //ui->pushButton->setDisabled(true);
            loadUrl();
        }

}

void siteUpdater::loadUrl()

{
    //webView->disconnect();
    //delete webView;



    QStandardItem *cell = new QStandardItem("Running...");
    cell->setData(QBrush(Qt::yellow), Qt::BackgroundRole);
    if (retries==0)  tableModel->setItem(currentItem-1,1,cell);

    //setData(QBrush(Qt::yellow), Qt::BackgroundRole);

    ui->tableView->setCurrentIndex( tableModel->index(currentItem-1,0) );


    ui->label_2->setText(QString::number(currentItem) +  "/" + QString::number(allItemCount));
    ui->progressBar->setValue((currentItem*1.0)/(allItemCount*1.0)*100);
    currentUrl = model->record(currentItem-1).value("url").toString();

    this->setWindowTitle(QString::number(currentItem) +  "/" + QString::number(allItemCount) + " | " + model->record(currentItem-1).value("title").toString() + " | Site Updater | Parallax");


    qDebug() << "checkPoint 001";
    qDebug() << currentUrl.split("?");
    // google plus album +1 workaround
    qDebug() << "checkPoint 002";
    if (currentUrl.contains("plus.google") & currentUrl.contains("album"))
        currentUrl = "https://plus.google.com/photos/?"+currentUrl.split("?").at(1);
    qDebug() << "checkPoint 003";

    log("");
    logHtml("[" +  QString::number(currentItem) +  "/" + QString::number(allItemCount) + "] <b>" +  model->record(currentItem-1).value("title").toString() +"</b>" );
    log("   Load URL: " + currentUrl);
    qDebug() << "checkPoint 004";
    if (retries==0) ui->webView->setUrl(QUrl(currentUrl));
        else ui->webView->reload();


}

void siteUpdater::log(QString text)
{
    ui->plainTextEdit->appendPlainText(text);
}

void siteUpdater::logHtml(QString text)
{
    ui->plainTextEdit->appendHtml(text);
}

siteUpdater::~siteUpdater()
{
    delete ui;
}
