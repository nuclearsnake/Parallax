#ifndef PTSCLONERDIALOG_H
#define PTSCLONERDIALOG_H

#include <QDialog>
#include <QDir>
namespace Ui {
class PtsClonerDialog;
}

class PtsClonerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PtsClonerDialog(QWidget *parent = 0);
    QString projectPath;
    QString sourceEv;
    QDir *dir;

    ~PtsClonerDialog();

    void setProjectPath(QString p);
    void evCalculator(QString ev);
    void replacer(QString inF, QString outF, QString inEv, QString outEv);
public slots:
    void go();
    void evSelector(QString source);
private:
    Ui::PtsClonerDialog *ui;
};

#endif // PTSCLONERDIALOG_H
