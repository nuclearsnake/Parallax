#ifndef INSTAGSDIALOG_H
#define INSTAGSDIALOG_H

#include <QDialog>
#include "managers/librarymanager.h"

namespace Ui {
class instagsDialog;
}

class instagsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit instagsDialog(QWidget *parent = 0);
    ~instagsDialog();
    libraryManager *libMan;
    QSqlDatabase mysqlDB;

    void attachLibMan(libraryManager *l);


private:
    Ui::instagsDialog *ui;
};

#endif // INSTAGSDIALOG_H
