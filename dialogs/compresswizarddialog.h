#ifndef COMPRESSWIZARDDIALOG_H
#define COMPRESSWIZARDDIALOG_H

#include <QDialog>
#include <QDir>
#include <QStandardItemModel>

namespace Ui {
class compressWizardDialog;
}

class compressWizardDialog : public QDialog
{
    Q_OBJECT

public:
    explicit compressWizardDialog(QWidget *parent = 0);
    QString topFolder;
    QString subFolder;
    qint64 totalBytes;
    QDir *dir;
    QDir *subDir;
    QStandardItemModel *model;
    bool checkState;
    int wizardStep;
    QList<QString> toBeConvertFiles;
    ~compressWizardDialog();

    void setFolder(QString f);
    void init();
private slots:
    void selectSubFolder(QString f);
    void selectAll();
    void selectNone();
    void next();
    void prev();
private:
    Ui::compressWizardDialog *ui;
    void compress();
};

#endif // COMPRESSWIZARDDIALOG_H
