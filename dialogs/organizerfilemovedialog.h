#ifndef ORGANIZERFILEMOVEDIALOG_H
#define ORGANIZERFILEMOVEDIALOG_H

#include <QDialog>
#include <QFile>

namespace Ui {
class organizerFileMoveDialog;
}

class organizerFileMoveDialog : public QDialog
{
    Q_OBJECT

public:
    explicit organizerFileMoveDialog(QWidget *parent = 0);
    ~organizerFileMoveDialog();
    QList<QPair<QString,QString> > moveList;
    void showClose();
    QFile handler,*handlerTo,*handlerFrom;
    void setMoveList(QList<QPair<QString, QString> > list);
public slots:
    void updateStatus(QString from, QString to, int currentItem, int allItems);
    void runMove();
private slots:
    void updateProgress(qint64 copyed);
private:
    Ui::organizerFileMoveDialog *ui;
};

#endif // ORGANIZERFILEMOVEDIALOG_H
