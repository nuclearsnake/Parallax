#include "addsitedialog.h"
#include "ui_addsitedialog.h"
#include <QTextDocument>
#include <QWebFrame>
#include <extensions/tools.h>
#include <QWebElement>
#include <QNetworkDiskCache>
#include <QStandardPaths>
#include <QTimer>

AddSiteDialog::AddSiteDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddSiteDialog)
{
    ui->setupUi(this);

    months << "" << "jan" << "feb" << "marc" << "apr" << "may" << "june" << "July" << "aug" << "sept" << "okt" << "nov" << "dec";

    qDebug() << months;
    qDebug() << months.indexOf("july");

    //ui->webView->setUrl(QUrl("http://realitydream.deviantart.com/art/Glory-129509614"));
    ui->webView->setVisible(true);
    //ui->webView->time
    loginRequired=0;
    loggedIn=0;

    QNetworkDiskCache *cache = new QNetworkDiskCache(this);

    cache->setCacheDirectory(QStandardPaths::standardLocations( QStandardPaths::DataLocation ).at(0) + "/cache");
    cache->setMaximumCacheSize(1073741824);

    qDebug() << "     cache size" << cache->cacheSize();
    qDebug() << " MAX cache size" << cache->maximumCacheSize();
    ui->webView->page()->networkAccessManager()->setCache(cache);


    //ui->lineEditSiteUrl->setText("http://realitydream.deviantart.com/art/Glory-129509614");

    connect( ui->webView->page()->networkAccessManager()    , SIGNAL(sslErrors(QNetworkReply*,QList<QSslError>)) ,
             this                                           , SLOT(sslErrorHandler(QNetworkReply*,QList<QSslError>)) );

    connect( ui->webView            , SIGNAL(loadFinished(bool) )   , this  , SLOT(delayedGetSource()) );
    connect( ui->pushButtonGetData  , SIGNAL(clicked())             , this  , SLOT(getData()));
    connect( ui->pushButtonAdd      , SIGNAL(clicked())             , this  , SLOT(checkValues()));
    connect( ui->pushButtonCancel   , SIGNAL(clicked())             , this  , SLOT(close() ));
}

void AddSiteDialog::sslErrorHandler(QNetworkReply* qnr, const QList<QSslError> & errlist)
{

    qDebug() << "---frmBuyIt::sslErrorHandler: ";
      // show list of all ssl errors
      foreach (QSslError err, errlist)
        qDebug() << "ssl error: " << err;


       qnr->ignoreSslErrors();

}

void AddSiteDialog::getData()
{

    //https://plus.google.com/photos/+ZsoltZsigmond/albums/5634478041265929665/5642332028086687122?pid=5642332028086687122&oid=105072305864951447999

    QString url = ui->lineEditSiteUrl->text();
    qDebug() << "              URL" << url;
    if (url.contains("plus.google") & (url.contains("album") || url.contains("posts")))
        {
            qDebug() << "               G+" << url;
            //url = "https://plus.google.com/photos/?"+url.split("?").at(1);
            //int pos1 = url.indexOf("+");
            //int pos2 = url.indexOf("?");
            ui->lineEditSiteUrl->setText( "https://plus.google.com/photos/?"+url.split("?").at(1) );
        }

    ui->pushButtonGetData->setEnabled(false);
    ui->pushButtonGetData->setText("Fetching URL...");
    ui->lineEditSiteUrl->setEnabled(false);
    theUrl=ui->lineEditSiteUrl->text();
    ui->webView->settings()->setAttribute(QWebSettings::JavascriptEnabled,true);
    //ui->webView->page()->networkAccessManager()->
    ui->webView->setUrl(QUrl(ui->lineEditSiteUrl->text()));
    //ui->webView->load(QUrl(ui->lineEditSiteUrl->text()));


}

void AddSiteDialog::checkValues()
{

    this->origTitle     = ui->lineEditTitle->text();
    this->submitDate    = ui->lineEdit->text();
    this->submitTime    = ui->lineEdit_time->text();
    this->statViews     = ui->lineEdit_2->text().toInt();
    this->statFavs      = ui->lineEdit_3->text().toInt();
    this->statComments  = ui->lineEdit_4->text().toInt();

    /*
    int err=0;


    if (ui->lineEditSiteUrl->text() == "")
    {
        messageDialog msgDialog;
        msgDialog.setMessage("No URL!");
        msgDialog.exec();
        ++err;
    }

    if (!err) accept();
    */
    accept();
}

/*
QString AddSiteDialog::getPiece(QString content, QString beginStr,QString endStr)
{
    qDebug() << "----------------------------------";
    QString piece = content.mid(content.indexOf(beginStr)+beginStr.length(),50);
    qDebug() << "piece 1: " << piece;

    piece = piece.mid(0,piece.indexOf(endStr));
    qDebug() << "piece 3: " << piece;

    piece = piece.replace(",","");
    qDebug() << "piece 2: " << piece;

    return piece;
    //statViews = val;
}
*/

void AddSiteDialog::delayedGetSource()
{
    QTimer::singleShot(1000,this,SLOT(getSource()));
}

void AddSiteDialog::getSource()
{
    qDebug() << " --get source";
    qDebug() << "URL valid:" << ui->webView->url().isValid();
    qDebug() << "URL      :" << ui->webView->url();
    //ui->webView->stop();
    //ui->webView->close();
    ui->webView->disconnect();

    if (ui->webView->url().toString().contains("plus.google") )
    {
        loginRequired=1;
        qDebug() << " --login required";

    }
    else loggedIn=1;

    if (loginRequired && !loggedIn)
    {
        qDebug() << " --Enter login info";
        ui->webView->page()->mainFrame()->evaluateJavaScript("document.getElementById('Email').value='hungarianskies';");
        ui->webView->page()->mainFrame()->evaluateJavaScript("document.getElementById('Passwd').value='Hanem793';");
        ui->webView->page()->mainFrame()->evaluateJavaScript("document.getElementById('signIn').click();");
        connect( ui->webView , SIGNAL(loadFinished(bool) ) , this , SLOT( delayedGetSource() ) );
    }

    //QWebElement liEmail = ui->webView->page()->mainFrame()->findFirstElement("#Email");
    //qDebug() << " *** IIIIIIIIIIII " << liEmail.toPlainText();


    if (loggedIn)
    {

            QString pageContent ="";
            pageContent = ui->webView->page()->mainFrame()->toHtml();
            ui->plainTextEdit->setPlainText(pageContent);

            int err=0;

            if (pageContent=="<html><head></head><body></body></html>") err++;
            //qDebug() << pageContent.indexOf("<dt>Views</dt><dd>");

            if (!err)
            {

                tools* pG = new tools;
                pG->getStats(ui->webView->url().toString(),pageContent,projectId);


                origTitle   = pG->origTitle;
                submitDate  = pG->submitDate;
                submitTime  = pG->submitTime;
                statViews   = pG->statViews;
                statFavs    = pG->statFavs;
                statComments= pG->statComments;


                ui->lineEditTitle->setText(origTitle);
                ui->lineEdit->setText(submitDate);
                ui->lineEdit_time->setText(submitTime);
                ui->lineEdit_2->setText(QString::number(statViews));
                ui->lineEdit_3->setText(QString::number(statFavs));
                ui->lineEdit_4->setText(QString::number(statComments));
                //ui->pushButtonAdd->setEnabled(true);
                ui->pushButtonGetData->setText("Done");
            }

            /*
            else
            {
               qDebug() << "INVALID URL";
               messageDialog msgDialog;
               msgDialog.setMessage("No Response from: '"+ui->lineEditSiteUrl->text()+"'");
               msgDialog.exec();
               ui->pushButtonGetData->setEnabled(true);
               ui->pushButtonGetData->setText("Get data");
               ui->lineEditSiteUrl->setEnabled(true);
            }
            */

    }

    loggedIn =1;
}

void AddSiteDialog::setTitle(QString title)
{
    ui->labelProjectTitle->setText(title);
}

void AddSiteDialog::setId(int id)
{
    qDebug() << "dialog received the id:" << id;
    projectId=id;
    qDebug() << "projectId :            " << projectId;
    ui->labelId->setText( QString::number( projectId) );
}

AddSiteDialog::~AddSiteDialog()
{
    delete ui;
}
