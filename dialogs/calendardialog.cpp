#include "calendardialog.h"
#include "ui_calendardialog.h"



calendarDialog::calendarDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::calendarDialog)
{
    ui->setupUi(this);
    connect ( ui->pushButton_cancel     , SIGNAL(clicked()) , this , SLOT(close()) );
    connect ( ui->pushButton_select     , SIGNAL(clicked()) , this , SLOT(submit()) );

}


void calendarDialog::submit()
{
    date = ui->calendarWidget->selectedDate();
    accept();
}

calendarDialog::~calendarDialog()
{
    delete ui;
}
