#include "extstatsdialog.h"
#include "ui_extstatsdialog.h"

extStatsDialog::extStatsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::extStatsDialog)
{
    ui->setupUi(this);

    QTimer::singleShot(500,this,SLOT(go()));
    connect(ui->horizontalSlider    , SIGNAL(valueChanged(int))  , this , SLOT(go()));
}

void extStatsDialog::go()
{
     qDebug() << "Selected ID=" << selectedId;

     QString sval = QString::number( ui->horizontalSlider->value());
     ui->label_sliderval->setText( sval );

     ui->label_score->setText( this->score );
     ui->label_title->setText( this->title );
     ui->label_date->setText(  this->date  );
     ui->label_score_bias->setText( QString::number(this->scoreBias,'g',3));
    /*
     QString query = ""
                     " SELECT t3.id id, t3.title title, t3.site site, 'V' type, t3.minviews min, t3.maxviews max,t3.views val"
                     "       , sqrt.sqrt as subscore, t3.year year"
                     "    FROM"
                     "    ("
                     "        SELECT  archive.id,archive.title title,sites.site,'V',sa.minViews,sa.maxViews, sites.views, substr(sites.date,1,4) year"
                     "                ,ROUND( ( (CAST(sites.views  AS REAL )-sa.minViews) / (sa.maxViews-sa.minViews) ) ,2) subscore"
                     "        FROM archive"
                     "        LEFT JOIN sites ON sites.projectId=archive.id"
                     "        LEFT JOIN"
                     "            ("
                     "            SELECT  sites.site,"
                     "                    MAX(sites.views) maxViews,"
                     "                    MIN(sites.views) as minViews,"
                     "                    substr(sites.date,1,4) year"
                     "            FROM sites"
                     "                    WHERE sites.notcount IS NULL"
                     "            GROUP BY substr(sites.date,1,4),sites.site"
                     "            ) sa  ON sa.site = sites.site AND sa.year = substr(sites.date,1,4)"
                     "        WHERE sites.site != 'FB'"
                     "            AND sites.notcount IS NULL"

                     "        AND archive.id="+QString::number(selectedId)+
                     "    ) t3"
                     "    LEFT JOIN sqrt ON sqrt.base = t3.subscore"
                     "  UNION"
                     "  SELECT t2.id id, t2.title title, t2.site site, 'F' type,t2.minfavs min,t2.maxfavs max, t2.favs as val"
                     "       , sqrt.sqrt as subscore, t2.year year"
                     "    FROM"
                     "    ("
                     "        SELECT  archive.id,archive.title title,sites.site,'F',sa.minFavs,sa.maxFavs,sites.favs, substr(sites.date,1,4) year"
                     "                ,ROUND( ( (CAST(sites.favs  AS REAL ) - sa.minFavs)  / (sa.maxFavs-sa.MinFavs) ) ,2) subscore"
                     "        FROM archive"
                     "        LEFT JOIN sites"
                     "        ON sites.projectId=archive.id"
                     "        LEFT JOIN"
                     "            ("
                     "            SELECT  sites.site,"
                     "                    MAX(sites.favs) maxFavs ,"
                     "                    MIN(sites.favs) as minFavs,"
                     "                    substr(sites.date,1,4) year"
                     "            FROM sites"
                     "                    WHERE sites.notcount IS NULL"
                     "            GROUP BY substr(sites.date,1,4),sites.site"
                     "            ) sa  ON sa.site = sites.site AND sa.year = substr(sites.date,1,4)"
                     "        WHERE sites.site != 'VB'"
                     "            AND sites.notcount IS NULL"

                     "        AND archive.id="+QString::number(selectedId)+
                     "    ) t2"
                     "    LEFT JOIN sqrt ON sqrt.base = t2.subscore  ";
    */

     QString query="SELECT sa2.id id,sa2.title title,sa2.site site,sa2.date date "
            " ,sa2.minviews minviews,sa2.views views,sa2.maxviews maxviews"
            " , qv.sqrt as scoreviews"
           "  ,sa2.minfavs minfavs,sa2.favs favs,sa2.maxfavs maxfavs"
            " , qf.sqrt as scorefavs"
            " FROM"
            " ( "
                   " SELECT sa1.projectid id,sa1.title title,sa1.site site,sa1.date date "
                   "  ,sa1.minviews minviews,sa1.views views,sa1.maxviews maxviews "
                   "  , CASE WHEN sa1.notcount IS NULL THEN ROUND( ( (CAST(sa1.views  AS REAL )-sa1.minViews) / (sa1.maxViews-sa1.minViews) ) ,2) ELSE NULL END scoreviews"
                   "  ,sa1.minfavs minfavs,sa1.favs favs,sa1.maxfavs maxfavs"
                   "  , CASE WHEN sa1.notcount IS NULL THEN ROUND( ( (CAST(sa1.favs   AS REAL )-sa1.minfavs ) / (sa1.maxfavs -sa1.minfavs ) ) ,2) ELSE NULL END scorefavs"
             " FROM "
             " ("
             " SELECT sa.id,sa.title,sa.site,sa.views,sa.favs ,sa.projectid,sa.notcount,sa.date"
                 ", CASE WHEN minfavs1<minfavs2 THEN minfavs1 ELSE minfavs2 END AS minfavs "
                 ", CASE WHEN maxfavs1>maxfavs2 THEN maxfavs1 ELSE maxfavs2 END AS maxfavs "
                 ", CASE WHEN minviews1<minviews2 THEN minviews1 ELSE minviews2 END AS minviews "
                 ", CASE WHEN maxviews1>maxviews2 THEN maxviews1 ELSE maxviews2 END AS maxviews "
             " FROM"
             " ("
                 " SELECT s.id,a.title,s.site,s.views,s.favs,s.projectid,s.notcount,s.date"
                     " ,(SELECT MIN(favs)  FROM (SELECT favs  FROM sites WHERE sites.site=s.site AND sites.date>=s.date AND sites.notcount IS NULL ORDER BY sites.date ASC  LIMIT "+sval+")) minfavs1 "
                     " ,(SELECT MIN(favs)  FROM (SELECT favs  FROM sites WHERE sites.site=s.site AND sites.date<=s.date AND sites.notcount IS NULL ORDER BY sites.date DESC LIMIT "+sval+")) minfavs2 "
                     " ,(SELECT MAX(favs)  FROM (SELECT favs  FROM sites WHERE sites.site=s.site AND sites.date>=s.date AND sites.notcount IS NULL ORDER BY sites.date ASC  LIMIT "+sval+")) maxfavs1 "
                     " ,(SELECT MAX(favs)  FROM (SELECT favs  FROM sites WHERE sites.site=s.site AND sites.date<=s.date AND sites.notcount IS NULL ORDER BY sites.date DESC LIMIT "+sval+")) maxfavs2 "
                     " ,(SELECT MIN(views) FROM (SELECT views FROM sites WHERE sites.site=s.site AND sites.date>=s.date AND sites.notcount IS NULL ORDER BY sites.date ASC  LIMIT "+sval+")) minviews1 "
                     " ,(SELECT MIN(views) FROM (SELECT views FROM sites WHERE sites.site=s.site AND sites.date<=s.date AND sites.notcount IS NULL ORDER BY sites.date DESC LIMIT "+sval+")) minviews2"
                     " ,(SELECT MAX(views) FROM (SELECT views FROM sites WHERE sites.site=s.site AND sites.date>=s.date AND sites.notcount IS NULL ORDER BY sites.date ASC  LIMIT "+sval+")) maxviews1 "
                     " ,(SELECT MAX(views) FROM (SELECT views FROM sites WHERE sites.site=s.site AND sites.date<=s.date AND sites.notcount IS NULL ORDER BY sites.date DESC LIMIT "+sval+")) maxviews2 "
                 " FROM archive a"
                 " LEFT JOIN sites s ON a.id=s.projectid"
                 " WHERE s.id IN (SELECT MAX(id) FROM sites WHERE sites.projectid = a.id GROUP BY sites.site)"
                 " AND a.id="+QString::number(selectedId)+
             " ) sa"
             " ) sa1"
             " ) sa2"
            " LEFT JOIN sqrt qv ON qv.base=sa2.scoreviews"
            " LEFT JOIN sqrt qf ON qf.base=sa2.scorefavs"
             ;

     //query="SELECT * FROM sites";
     qDebug() << query;

     QSqlQueryModel* model = new QSqlQueryModel(this);
     model->setQuery(query,libMan->db);
     while(model->canFetchMore()) model->fetchMore();

     qDebug() << "rowcount=" << model->rowCount();

     float sum=0;
     int   cnt=0;

     for (int i=0;i<model->rowCount();++i)
     {

         if  ( !model->record(i).value("scorefavs").isNull() )
         {
             cnt++;
             sum+=model->record(i).value("scorefavs").toFloat();
         }
         if  ( !model->record(i).value("scoreviews").isNull() )
         {
             cnt++;
             sum+=model->record(i).value("scoreviews").toFloat();
         }

         //DA
         if (model->record(i).value("site")=="DA")
         {
               ui->label_Vmin_DA->setText(model->record(i).value("minviews").toString());
               ui->label_Vmax_DA->setText(model->record(i).value("maxviews").toString());
                  ui->label_V_DA->setText(model->record(i).value("views").toString());
             ui->label_Vscore_DA->setText(model->record(i).value("scoreviews").toString());
               ui->label_Year_DA->setText(model->record(i).value("date").toString());
             // ui->progressBar_DA->setValue(  (ui->label_Fscore_DA->text().toFloat() + ui->label_Vscore_DA->text().toFloat() )/0.02  );
               ui->label_Fmin_DA->setText(model->record(i).value("minfavs").toString());
               ui->label_Fmax_DA->setText(model->record(i).value("maxfavs").toString());
                  ui->label_F_DA->setText(model->record(i).value("favs").toString());
             ui->label_Fscore_DA->setText(model->record(i).value("scorefavs").toString());
              ui->progressBar_DA->setValue(  (ui->label_Fscore_DA->text().toFloat() + ui->label_Vscore_DA->text().toFloat() )/0.02  );
         }

         //5P
         if (model->record(i).value("site")=="5P" )
         {
               ui->label_Vmin_5P->setText(model->record(i).value("minviews").toString());
               ui->label_Vmax_5P->setText(model->record(i).value("maxviews").toString());
                  ui->label_V_5P->setText(model->record(i).value("views").toString());
             ui->label_Vscore_5P->setText(model->record(i).value("scoreviews").toString());
               ui->label_Year_5P->setText(model->record(i).value("date").toString());
               ui->label_Fmin_5P->setText(model->record(i).value("minfavs").toString());
               ui->label_Fmax_5P->setText(model->record(i).value("maxfavs").toString());
                  ui->label_F_5P->setText(model->record(i).value("favs").toString());
             ui->label_Fscore_5P->setText(model->record(i).value("scorefavs").toString());
              ui->progressBar_5P->setValue(  (ui->label_Fscore_5P->text().toFloat() + ui->label_Vscore_5P->text().toFloat() )/0.02  );
         }

         //G+
         if (model->record(i).value("site")=="G+" )
         {
               ui->label_Vmin_GP->setText(model->record(i).value("minviews").toString());
               ui->label_Vmax_GP->setText(model->record(i).value("maxviews").toString());
                  ui->label_V_GP->setText(model->record(i).value("views").toString());
             ui->label_Vscore_GP->setText(model->record(i).value("scoreviews").toString());
               ui->label_Year_GP->setText(model->record(i).value("date").toString());
              ui->progressBar_GP->setValue(  (ui->label_Fscore_GP->text().toFloat() + ui->label_Vscore_GP->text().toFloat() )/0.02  );

               ui->label_Fmin_GP->setText(model->record(i).value("minfavs").toString());
               ui->label_Fmax_GP->setText(model->record(i).value("maxfavs").toString());
                  ui->label_F_GP->setText(model->record(i).value("favs").toString());
             ui->label_Fscore_GP->setText(model->record(i).value("scorefavs").toString());
              ui->progressBar_GP->setValue(  (ui->label_Fscore_GP->text().toFloat() + ui->label_Vscore_GP->text().toFloat() )/0.02  );
         }

         //FB
         /*
         if (model->record(i).value("site")=="FB" && model->record(i).value("type")=="V")
         {
               ui->label_Vmin_FB->setText(model->record(i).value("min").toString());
               ui->label_Vmax_FB->setText(model->record(i).value("max").toString());
                  ui->label_V_FB->setText(model->record(i).value("val").toString());
             ui->label_Vscore_FB->setText(model->record(i).value("subscore").toString());
               ui->label_Year_FB->setText(model->record(i).value("year").toString());
              ui->progressBar_FB->setValue(  (ui->label_Fscore_FB->text().toFloat() + ui->label_Vscore_FB->text().toFloat() )/0.02  );
         }
         */
         if (model->record(i).value("site")=="FB" )
         {
               ui->label_Fmin_FB->setText(model->record(i).value("minfavs").toString());
               ui->label_Fmax_FB->setText(model->record(i).value("maxfavs").toString());
                  ui->label_F_FB->setText(model->record(i).value("favs").toString());
               ui->label_Year_FB->setText(model->record(i).value("date").toString());
             ui->label_Fscore_FB->setText(model->record(i).value("scorefavs").toString());
              ui->progressBar_FB->setValue(  (ui->label_Fscore_FB->text().toFloat()*100  )  );
         }

         //FL
         if (model->record(i).value("site")=="FL" )
         {
               ui->label_Vmin_FL->setText(model->record(i).value("minviews").toString());
               ui->label_Vmax_FL->setText(model->record(i).value("maxviews").toString());
                  ui->label_V_FL->setText(model->record(i).value("views").toString());
             ui->label_Vscore_FL->setText(model->record(i).value("scoreviews").toString());
               ui->label_Year_FL->setText(model->record(i).value("date").toString());
               ui->label_Fmin_FL->setText(model->record(i).value("minfavs").toString());
               ui->label_Fmax_FL->setText(model->record(i).value("maxfavs").toString());
                  ui->label_F_FL->setText(model->record(i).value("favs").toString());
             ui->label_Fscore_FL->setText(model->record(i).value("scorefavs").toString());
              ui->progressBar_FL->setValue(  (ui->label_Fscore_FL->text().toFloat() + ui->label_Vscore_FL->text().toFloat() )/0.02  );
         }

         //PX
         if (model->record(i).value("site")=="PX" )
         {
               ui->label_Vmin_PX->setText(model->record(i).value("minviews").toString());
               ui->label_Vmax_PX->setText(model->record(i).value("maxviews").toString());
                  ui->label_V_PX->setText(model->record(i).value("views").toString());
             ui->label_Vscore_PX->setText(model->record(i).value("scoreviews").toString());
               ui->label_Year_PX->setText(model->record(i).value("date").toString());

               ui->label_Fmin_PX->setText(model->record(i).value("minfavs").toString());
               ui->label_Fmax_PX->setText(model->record(i).value("maxfavs").toString());
                  ui->label_F_PX->setText(model->record(i).value("favs").toString());
             ui->label_Fscore_PX->setText(model->record(i).value("scorefavs").toString());
              ui->progressBar_PX->setValue(  (ui->label_Fscore_PX->text().toFloat() + ui->label_Vscore_PX->text().toFloat() )/0.02  );
         }

         //YP
         if (model->record(i).value("site")=="YP" )
         {
               ui->label_Vmin_YP->setText(model->record(i).value("minviews").toString());
               ui->label_Vmax_YP->setText(model->record(i).value("maxviews").toString());
                  ui->label_V_YP->setText(model->record(i).value("views").toString());
             ui->label_Vscore_YP->setText(model->record(i).value("scoreviews").toString());
               ui->label_Year_YP->setText(model->record(i).value("date").toString());


               ui->label_Fmin_YP->setText(model->record(i).value("minfavs").toString());
               ui->label_Fmax_YP->setText(model->record(i).value("maxfavs").toString());
                  ui->label_F_YP->setText(model->record(i).value("favs").toString());
             ui->label_Fscore_YP->setText(model->record(i).value("scorefavs").toString());
              ui->progressBar_YP->setValue(  (ui->label_Fscore_YP->text().toFloat() + ui->label_Vscore_YP->text().toFloat() )/0.02  );
         }


         //IG
         if (model->record(i).value("site")=="IG" )
         {
             /*
               ui->label_Vmin_IG->setText(model->record(i).value("minviews").toString());
               ui->label_Vmax_IG->setText(model->record(i).value("maxviews").toString());
                  ui->label_V_IG->setText(model->record(i).value("views").toString());
             ui->label_Vscore_IG->setText(model->record(i).value("scoreviews").toString());
               ui->label_Year_IG->setText(model->record(i).value("date").toString());
                */
               ui->label_Fmin_IG->setText(model->record(i).value("minfavs").toString());
               ui->label_Fmax_IG->setText(model->record(i).value("maxfavs").toString());
                  ui->label_F_IG->setText(model->record(i).value("favs").toString());
             ui->label_Fscore_IG->setText(model->record(i).value("scorefavs").toString());
              ui->progressBar_IG->setValue(  (ui->label_Fscore_IG->text().toFloat() *100 )  );
         }


     }

     ui->label_score_calc->setText( QString::number( sum/cnt*5*this->scoreBias , 'g',3 ) );
}

void extStatsDialog::attachLibman(libraryManager *l)
{
    libMan = l;
}

extStatsDialog::~extStatsDialog()
{
    delete ui;
}
