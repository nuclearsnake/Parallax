#include "compresswizarddialog.h"
#include "ui_compresswizarddialog.h"

#include <extensions/tools.h>

#include <QDebug>
#include <QMessageBox>
#include <QImageReader>
#include <QImageWriter>

compressWizardDialog::compressWizardDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::compressWizardDialog)
{
    ui->setupUi(this);

    connect ( ui->pushButton_Cancel             , SIGNAL(clicked())             , this  , SLOT(close())  );
    connect ( ui->comboBox_folderSelect         , SIGNAL(activated(QString))    , this  , SLOT(selectSubFolder(QString)) );
    connect ( ui->pushButton_filesSelectAll     , SIGNAL(clicked())             , this  , SLOT(selectAll()) );
    connect ( ui->pushButton_filesDeselectAll   , SIGNAL(clicked())             , this  , SLOT(selectNone()) );
    connect ( ui->pushButton_Next               , SIGNAL(clicked())             , this  , SLOT(next()) );
    connect ( ui->pushButton_Prev               , SIGNAL(clicked())             , this  , SLOT(prev()) );

    dir = new QDir;
    subDir = new QDir;
    model = new QStandardItemModel(this);
    ui->tableView_filesSelect->setModel(model);
    ui->tableView_filesSelect->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tableView_filesSelect->setSelectionBehavior(QAbstractItemView::SelectRows);

    ui->pushButton_Prev->setEnabled(false);
    ui->label_noFiles->setVisible(false);
    ui->stackedWidget->setCurrentIndex(0);
    checkState = false;
    wizardStep = 0;





}

void compressWizardDialog::prev()
{
    if (wizardStep==1)
        {
            ui->stackedWidget->setCurrentIndex(0);
            wizardStep = 0;
            ui->pushButton_Prev->setEnabled(false);
        }

}

void compressWizardDialog::compress()
{
    ui->pushButton_Cancel->setEnabled(false);
    int all = toBeConvertFiles.count();
    int i=0;
    foreach(QString fileName,toBeConvertFiles)
        {
            ++i;
            QImageReader pixR;
            QImageWriter pixW;
            QImage pix;
            //bool readSuccess = false; //unused
            bool writeSuccess = false;

            QCoreApplication::processEvents();
            ui->label_file->setText(QString::number(i)+"/"+QString::number(all));
            qDebug() << i << all << 100*i/all;
            ui->progressBar->setValue(100*i/all);
            QCoreApplication::processEvents();

            QString from = topFolder + "/" + subFolder + "/" +fileName;
            ui->label_from->setText(from);

            QString to = from;
            to.replace(".tif",".jpg");
            ui->label_to->setText(to);

            // read
            ui->log->appendPlainText(QString::number(i)+"/"+QString::number(all) + "     " + from);
            pixR.setFileName(from);
            //pixR.setScaledSize(QSize(200,200/(pixR.size().width()*1.0)*(pixR.size().height()*1.0)));
            QCoreApplication::processEvents();
            pixR.read(&pix);
            QCoreApplication::processEvents();
            qDebug() << "read error:" << pixR.error();
            qDebug() << "read error:" << pixR.errorString();
            QCoreApplication::processEvents();

            // write
            ui->log->appendPlainText("         out: " + to);
            pixW.setFileName(to);
            pixW.setFormat("jpg");
            //pixW.setCompression(95);
            pixW.setQuality(95);
            QCoreApplication::processEvents();
            writeSuccess = pixW.write(pix);
            QCoreApplication::processEvents();
            qDebug() << "write success:" << writeSuccess;
            qDebug() << "wite error:" << pixW.error();
            qDebug() << "wite error:" << pixW.errorString();

            // delete
            //delete &pix;
            //delete &pixR;
            //delete &pixW;
            pixR.setFileName("");
            if (writeSuccess) qDebug() << QFile::remove(from);



        }
    ui->pushButton_Cancel->setEnabled(true);
    ui->pushButton_Cancel->setText("Close");
}

void compressWizardDialog::next()
{
    if (wizardStep==1)
        {
            ui->stackedWidget->setCurrentIndex(2);
            this->wizardStep=2;
            ui->pushButton_Prev->setEnabled(false);
            ui->pushButton_Next->setEnabled(false);
            this->compress();
        }

    if (wizardStep==0)
        {
            int selectedCount=0;
            totalBytes=0;
            toBeConvertFiles.clear();
            for (int i=0;i<model->rowCount();++i)
                {
                    QModelIndex index;
                    index = model->index(i,0, QModelIndex());
                    if(index.data(Qt::CheckStateRole) == Qt::Checked)
                        {
                            selectedCount++;
                            toBeConvertFiles << model->index(i,1).data().toString();
                            totalBytes += model->index(i,2).data().toString().replace(" ","").toInt() ;
                        }
                }
            qDebug() << selectedCount;
            qDebug() << toBeConvertFiles;
            qDebug() << totalBytes;
            if (selectedCount==0)
                {
                    QMessageBox Msgbox;
                    Msgbox.setText("No files selected!");
                    Msgbox.exec();
                }
                else
                {
                    ui->stackedWidget->setCurrentIndex(1);
                    wizardStep = 1;
                    ui->pushButton_Prev->setEnabled(true);
                    ui->label_selectedFilesCount->setText(QString::number(selectedCount));
                    ui->label_totalBytes->setText(  tools::numGrouped( totalBytes ) );
                }
        }


}

void compressWizardDialog::init()
{
    qDebug() << topFolder;


    dir->setPath( topFolder );
    dir->setFilter(QDir::NoDotAndDotDot | QDir::Dirs );
    dir->setSorting(QDir::Name );

    qDebug() << dir->count();

    foreach (QString name,dir->entryList())
    {
        qDebug() << name;
        ui->comboBox_folderSelect->addItem(name);

    }

    this->selectSubFolder( dir->entryList().first() );
}

void compressWizardDialog::selectSubFolder(QString f)
{
    qDebug() << f;
    subFolder = f;
    subDir->setPath( topFolder + "/" + f );
    subDir->setFilter(QDir::NoDotAndDotDot | QDir::Files );
    subDir->setSorting(QDir::Name );

    model->clear();
    QStringList labels;
    labels << "select" << "file name" << "file size" ;
    model->setHorizontalHeaderLabels(labels);


    qint64 tB=0;

    foreach (QString name,subDir->entryList())
    {
        if (name.endsWith(".tif"))
            {
                qDebug() << topFolder + "/" + f + "/" + name;

                QList<QStandardItem*> items;

                // checkbox (?)
                //items << new QStandardItem( "" );
                QStandardItem *chk = new QStandardItem(true);
                    chk->setCheckable(true);
                    if (checkState) chk->setCheckState(Qt::Checked); else chk->setCheckState(Qt::Unchecked );
                    chk->setData(Qt::AlignCenter,Qt::AlignVCenter);
                items << chk;

                // name
                items << new QStandardItem( name );

                // file size
                QFileInfo info(topFolder + "/" + f + "/" + name);
                tB+=info.size();
                QStandardItem *sizeItem = new QStandardItem( tools::numGrouped( info.size() ).rightJustified(20,' ') );
                sizeItem->setData(Qt::AlignRight  , Qt::TextAlignmentRole);
                items << sizeItem;

                model->appendRow(items);
            }

    }

    ui->tableView_filesSelect->setColumnWidth(0,30);
    ui->tableView_filesSelect->setColumnWidth(1,400);
    ui->tableView_filesSelect->horizontalHeader()->setStretchLastSection(true);
    ui->label_totalBytes_2->setText(tools::numGrouped(tB)+" bytes total");
    if (tB==0) ui->label_totalBytes_2->setText("");

    if (model->rowCount()==0)
        {
            ui->label_noFiles->setVisible(true);
            ui->tableView_filesSelect->setVisible(false);
            ui->frame_selectButtons->setVisible(false);
        }
        else
        {
            ui->label_noFiles->setVisible(false);
            ui->tableView_filesSelect->setVisible(true);
            ui->frame_selectButtons->setVisible(true);
        }


}

void compressWizardDialog::selectAll()
{
    checkState=true;
    this->selectSubFolder(ui->comboBox_folderSelect->currentText());
    checkState=false;
}

void compressWizardDialog::selectNone()
{
    checkState=false;
    this->selectSubFolder(ui->comboBox_folderSelect->currentText());
    checkState=false;
}

void compressWizardDialog::setFolder(QString f)
{
    topFolder = f;

}

compressWizardDialog::~compressWizardDialog()
{
    delete ui;
}
