#include "syncdialog.h"
#include "ui_syncdialog.h"
#include "extensions/tools.h"

syncDialog::syncDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::syncDialog)
{
    ui->setupUi(this);


}

void syncDialog::attachLibMan(libraryManager *l)
{
    libMan = l;

    connect( ui->pushButton_start   ,SIGNAL(clicked(bool))  ,this   ,SLOT(go()) );

}

void syncDialog::go()
{
    connectRemoteDB();

    if (remoteDBConnected)
    {
        QSqlQueryModel *remoteModel = new QSqlQueryModel(this);
        QSqlQueryModel *localModel  = new QSqlQueryModel(this);
        QString remoteQuery;
        QString localQuery;

        // ! ! !   S Y N C   U P   S I T E S   ! ! !


        log("Sync UP sites");
        log("Check sites tables...");
        remoteModel->setQuery("SELECT MAX(id) maxid FROM sites",mysqlDB);
        QString maxid = remoteModel->record(0).value("maxid").toString();
        log ("Remote max id="+maxid);
        localQuery="SELECT * FROM sites WHERE id>"+maxid+" ORDER BY id ";
        //log(localQuery);
        localModel->setQuery(localQuery,libMan->db);
        log ("Unsynced sites found:" + QString::number(localModel->rowCount()));

        if (localModel->rowCount()>0)
        {
            for (int i=0;i<localModel->rowCount();++i)
            {
                ui->progressBar_sitesUp->setValue((i+1.0)/localModel->rowCount()*100);
                ui->label_sitesUp->setText(QString::number(i+1)+" / "+ QString::number(localModel->rowCount()));
                qDebug() << "ylksdjalksdjaiowejqwe";
                qDebug() << "ylksdjalksdjaiowejqwe";
                qDebug() << "ylksdjalksdjaiowejqwe";
                qDebug() << "ylksdjalksdjaiowejqwe";

                QString q = "INSERT INTO sites (projectid,title,site,url,date,favs,views,comments,updated,time,option1,errorcounter) VALUES("

                            "  "+ localModel->record(i).value("projectid").toString()   +" , "
                            " '"+ localModel->record(i).value("title").toString()       +"'  , "
                            " '"+ localModel->record(i).value("site").toString()        +"'  , "
                            " '"+ localModel->record(i).value("url").toString()         +"' , "
                            " '"+ localModel->record(i).value("date").toString()        +"' , "
                            "  "+ localModel->record(i).value("favs").toString()        +", "
                            "  "+ localModel->record(i).value("views").toString()       +", "
                            "  "+ localModel->record(i).value("comments").toString()    +", "
                            " '"+ localModel->record(i).value("updated").toString()     +"' , "
                            " '"+ localModel->record(i).value("time").toString()        +"' , "
                            " '"+ localModel->record(i).value("option1").toString()     +"' , "
                            "  0 )"

                        ;
                log(q);
                // SQLBACK
                mysqlDB.exec(q);
            }
        }
        else
        {
            ui->progressBar_sitesUp->setValue(100);
            ui->label_sitesUp->setText("0 / 0");
        }

        /// ! ! !   S Y N C   D O W N   S I T E S   ( D A T E   A N D   T I M E   S Y N C )   !!!

                log(" ");
                log(" Sync DOWN sites");
                remoteModel->clear();
                localModel->clear();

                localQuery="SELECT * FROM sites WHERE site!='RD' AND site!='G+' ";
                localModel->setQuery(localQuery,libMan->db);
                while(localModel->canFetchMore()) localModel->fetchMore();
                qDebug() << localModel->record(0);

                int updCount = 0;

                for (int i=0;i<2/*localModel->rowCount()*/;++i)
                {


                    remoteQuery="SELECT * FROM sites WHERE id="+localModel->record(i).value("id").toString()
                                + " AND date!='' AND  date!='"+localModel->record(i).value("date").toString()+"'"
                            ;

                    /// D A T E
                    remoteModel->setQuery(remoteQuery,mysqlDB);
                    if (remoteModel->rowCount())
                    {
                        log(" ");log(" ");
                        log(remoteQuery);
                        log(QString::number(remoteModel->rowCount()));
                        log(remoteModel->record(0).value("site").toString()+" "
                            +remoteModel->record(0).value("date").toString() + " "
                            +remoteModel->record(0).value("url").toString() + " "
                            );
                        localQuery="UPDATE sites SET date='"+remoteModel->record(0).value("date").toString()
                                +"' WHERE id="+localModel->record(i).value("id").toString();
                        log(localQuery);
                        updCount++;
                        //libMan->db.exec(localQuery);
                    }

                    /// T I M E
                    remoteQuery="SELECT * FROM sites WHERE id="+localModel->record(i).value("id").toString()
                            + " AND time!='' AND time!='"+localModel->record(i).value("time").toString()+"'"
                        ;
                    remoteModel->setQuery(remoteQuery,mysqlDB);
                    if (remoteModel->rowCount())
                    {
                        log(" ");log(" ");
                        log(remoteQuery);
                        log(QString::number(remoteModel->rowCount()));
                        log(remoteModel->record(0).value("site").toString()+" "
                            +remoteModel->record(0).value("time").toString() + " "
                            +remoteModel->record(0).value("url").toString() + " "
                            );
                        localQuery="UPDATE sites SET time='"+remoteModel->record(0).value("time").toString()
                                +"' WHERE id="+localModel->record(i).value("id").toString();
                        log(localQuery);
                        updCount++;
                        //libMan->db.exec(localQuery);
                    }

                remoteModel->setQuery(remoteQuery,mysqlDB);

                    ui->progressBar_sitesDown->setValue((i+1.0)/localModel->rowCount()*100);
                    ui->label_sitesDown->setText(QString::number(i+1)+" / "+ QString::number(localModel->rowCount()) + "  ( "+QString::number(updCount)+" )" );
                    QCoreApplication::processEvents();
                }



        /// ! ! !   S Y N C   D O W N   U P D A T E   L O G   ! ! !


        log(" ");
        log("Sync DOWN log");
        remoteModel->clear();
        localModel->clear();

        remoteQuery = "SELECT * FROM siteupdatelog WHERE synced=0 ORDER BY siteid , id";
        remoteModel->setQuery(remoteQuery,mysqlDB);

        int unsyncedRows = remoteModel->rowCount();

        log ("unsynced rows at server:" + QString::number(unsyncedRows) );

        // !!!!!!!!!!!!!!!!!!!!!
        // unsyncedRows=0;
        if(unsyncedRows>0)
        {
        for (int i=0; i<unsyncedRows;i++)
        {
            ui->progressBar_logDown->setValue((i+1.0)/unsyncedRows*100);
            //ui->progressBar_logDown->setValue(50);
            qDebug() << (i+1.0)/(unsyncedRows+0.0)*100;
            ui->label_logDown->setText(QString::number(i+1) + " / " + QString::number(unsyncedRows));

            localQuery = " SELECT s.id id, a.title title, s.views views, s.favs favs, s.comments comments, s.updated updated,s.site site FROM sites s"
                         " LEFT JOIN archive a ON a.id = s.projectid"
                         " WHERE s.id = " + remoteModel->record(i).value("siteid").toString();

            //log ( localQuery);

            localModel->setQuery(localQuery,libMan->db);

            log(" ");
            log(   "#" + QString::number(i+1)  + " > " + localModel->record(0).value("site").toString() + " // " + localModel->record(0).value("title").toString() );
            log(
                  "         id  siteid     views      favs  comments   update_date  time_stamp "
                  );

            log(
                 + "rem: "   + tools::padSpaceBefore( remoteModel->record(i).value("id").toString() , 6)
                 + tools::padSpaceBefore(  remoteModel->record(i).value("siteid").toString()  ,   8 )
                 + tools::padSpaceBefore( tools::numGrouped (  remoteModel->record(i).value("views").toInt() ) ,  10 )
                 + tools::padSpaceBefore( tools::numGrouped (  remoteModel->record(i).value("favs").toInt() )  , 10 )
                 + tools::padSpaceBefore( tools::numGrouped (  remoteModel->record(i).value("comments").toInt() )  ,  10 )
                 + tools::padSpaceBefore( (  remoteModel->record(i).value("update_date").toString() )  ,  13 )
                 + tools::padSpaceBefore( (  remoteModel->record(i).value("time_stamp").toString() )  ,  22  )


                );

            log(
                 + "loc:       "
                 + tools::padSpaceBefore(  localModel->record(0).value("id").toString()  ,   8 )
                 + tools::padSpaceBefore( tools::numGrouped (  localModel->record(0).value("views").toInt() ) ,  10 )
                 + tools::padSpaceBefore( tools::numGrouped (  localModel->record(0).value("favs").toInt()   )  , 10 )
                 + tools::padSpaceBefore( tools::numGrouped (  localModel->record(0).value("comments").toInt() )  ,  10 )
                 + tools::padSpaceBefore(   localModel->record(0).value("updated").toString()   ,  13 )
                 + tools::padSpaceBefore( " "  ,  22  )


                );

            QString remDate = remoteModel->record(i).value("update_date").toString();
            remDate.replace("-","");
            QString locDate = localModel->record(0).value("updated").toString();
            locDate.replace("-","");
            //log("   " + remDate + " -> " + locDate + "  " + QString::number( remDate.toInt()-locDate.toInt() ) );
            if ( remDate.toInt()-locDate.toInt() >= 0)
            {
                log("Updating sites table");
                localQuery =" UPDATE sites SET "
                            " views="       + remoteModel->record(i).value("views").toString() +
                            ", favs="        + remoteModel->record(i).value("favs").toString() +
                            ", comments="    + remoteModel->record(i).value("comments").toString() +
                            ", updated='"    + remoteModel->record(i).value("update_date").toString()+"'"
                            " WHERE id="    + remoteModel->record(i).value("siteid").toString()
                        ;
                log(localQuery);
                // SQLBACK
                libMan->db.exec(localQuery);
            }
            else
            {
                log("Newer local data, no update to sites table!");
            }

            log("Updating local sitelog");
            localQuery= " INSERT INTO siteupdatelog (siteId,favs,views,comments,update_date) VALUES("
                        " " + remoteModel->record(i).value("siteid").toString() +
                        "," + remoteModel->record(i).value("favs").toString() +
                        "," + remoteModel->record(i).value("views").toString() +
                        "," + remoteModel->record(i).value("comments").toString() +
                        ",'" + remoteModel->record(i).value("update_date").toString() +"'"
                        " "
                        " )"
                    ;
            log(localQuery);
            // SQLBACK
            libMan->db.exec(localQuery);

            log("Updating remote sitelog");
            remoteQuery = " UPDATE siteupdatelog SET synced=1 WHERE id=" + remoteModel->record(i).value("id").toString();
            log(remoteQuery);
            // SQLBACK
            mysqlDB.exec(remoteQuery);

            QCoreApplication::processEvents();

        }
        }
        else
        {
            log("Nothing to sync!");
            ui->progressBar_logDown->setValue(100);
            ui->label_logDown->setText("0 / 0");
        }

    }

    // UPDATE SCORE

    QString query = " DROP TABLE IF EXISTS tmp_minmax";
    qDebug() << query;
    libMan->db.exec(query);

    query =" CREATE TABLE tmp_minmax AS"
           "     SELECT 1 id,MAX(views) maxviews,MAX(favs) maxfavs,MIN(views) minviews,MIN(favs) minfavs "
           "     FROM"
           "     ("
           "         SELECT projectid,SUM(views) views,SUM(favs) favs  FROM sites  GROUP BY projectid"
           "     ) t1";
    qDebug() << query;
    libMan->db.exec(query);

    query ="  DROP TABLE IF EXISTS tmp_siteminmax_year";
    qDebug() << query;
    libMan->db.exec(query);

    query = "  CREATE TABLE tmp_siteminmax_year AS"
          "      SELECT site,substr(date,1,4) year,MIN(views) minviews,MAX(views) maxviews, MIN(favs) minfavs, MAX(favs) maxfavs "
          "      FROM sites"
          "      GROUP BY site, substr(date,1,4)";
    qDebug() << query;
    libMan->db.exec(query);

    query ="  DROP TABLE IF EXISTS score";
    qDebug() << query;
    libMan->db.exec(query);

    query ="  CREATE TABLE score AS"
          "    SELECT sa6.projectid projectid, ROUND(sa6.score_yrst*5+sa6.scoreva+sa6.scorefa,3) score"
          "    FROM"
          "    ("
          "      SELECT sa5.projectid,sa5.score_yrst,qv.sqrtsqrt scoreva,qf.sqrtsqrt scorefa"
          "      FROM"
          "      ("
          "         SELECT sa4.projectid,sa4.score_yrst"
          "               ,ROUND( ( (CAST(sa4.sumviews  AS REAL )-sa4.minViewsa) / (sa4.maxViewsa-sa4.minViewsa) ) ,2) scoreva"
          "               ,ROUND( ( (CAST(sa4.sumfavs   AS REAL )-sa4.minfavsa ) / (sa4.maxfavsa -sa4.minfavsa ) ) ,2) scorefa"
          "         FROM"
          "         ("
          "           SELECT sa3.projectid projectid,ROUND((AVG(sa3.scorev) + AVG(sa3.scoref) ) / 2 , 2) score_yrst,SUM(views) sumviews, SUM(favs) sumfavs"
          "                 ,mm.minviews minviewsa,mm.minfavs minfavsa ,mm.maxviews maxviewsa ,mm.maxfavs maxfavsa"
          "           FROM"
          "           ("
          "             SELECT sa2.id,sa2.projectid,sa2.site,sa2.views,sa2.favs,qv.sqrtsqrt as scorev,qf.sqrtsqrt as scoref"
          "             FROM"
          "             ("
          "               SELECT sa1.id,sa1.projectid,sa1.site  ,sa1.views,sa1.favs     "
          "                    , CASE WHEN sa1.notcount IS NULL THEN ROUND( ( (CAST(sa1.views  AS REAL )-sa1.minViews) / (sa1.maxViews-sa1.minViews) ) ,2) ELSE NULL END scorev"
          "                    , CASE WHEN sa1.notcount IS NULL THEN ROUND( ( (CAST(sa1.favs   AS REAL )-sa1.minfavs ) / (sa1.maxfavs -sa1.minfavs ) ) ,2) ELSE NULL END scoref"
          "               FROM"
          "               ("
          "           SELECT s.id,a.title,s.site,s.views,s.favs,s.projectid,s.notcount"
          "                       ,smmy.minviews,smmy.minfavs,smmy.maxviews,smmy.maxfavs       "
          "                 FROM archive a"
          "                 LEFT JOIN sites s ON a.id=s.projectid"
          "                 LEFT JOIN tmp_siteminmax_year smmy ON smmy.site = s.site AND substr(s.date,1,4) = smmy.year  "
          "                 WHERE s.id IN (SELECT MAX(id) FROM sites WHERE sites.projectid = a.id GROUP BY sites.site)"
          "               ) sa1"
          "             ) sa2"
          "             LEFT JOIN sqrt qv ON qv.base=sa2.scorev"
          "             LEFT JOIN sqrt qf ON qf.base=sa2.scoref"
          "           ) sa3"
          "           LEFT JOIN tmp_minmax mm"
          "           GROUP BY sa3.projectid"
          "         ) sa4"
          "       ) sa5"
          "       LEFT JOIN sqrt qv ON qv.base=sa5.scoreva"
          "       LEFT JOIN sqrt qf ON qf.base=sa5.scorefa"
          "     ) sa6"
          "  ORDER BY ROUND(sa6.score_yrst+sa6.scoreva+sa6.scorefa,3) DESC";
    qDebug() << query;
    libMan->db.exec(query);
    // end

}



void syncDialog::connectRemoteDB()
{
    QSqlQueryModel *tmpModel = new QSqlQueryModel(this);
    QString query;

    query = "SELECT value FROM var WHERE KEY='syncIP'";
    tmpModel->setQuery(query,libMan->db);;
    QString remoteIP = tmpModel->index(0,0).data().toString();

    query = "SELECT value FROM var WHERE KEY='syncDB'";
    tmpModel->setQuery(query,libMan->db);;
    QString remoteDB = tmpModel->index(0,0).data().toString();

    query = "SELECT value FROM var WHERE KEY='syncUSR'";
    tmpModel->setQuery(query,libMan->db);;
    QString remoteUSR = tmpModel->index(0,0).data().toString();

    query = "SELECT value FROM var WHERE KEY='syncPASS'";
    tmpModel->setQuery(query,libMan->db);;
    QString remotePASS = tmpModel->index(0,0).data().toString();


    log("Connecting to remote mysql DB '" + remoteDB +  "' at '" + remoteIP +"' username: '" + remoteUSR + "' password: '" + remotePASS +"'");

    mysqlDB = QSqlDatabase::addDatabase("QMYSQL");
    mysqlDB.setHostName(remoteIP);
    mysqlDB.setDatabaseName(remoteDB);
    mysqlDB.setUserName(remoteUSR);
    mysqlDB.setPassword(remotePASS);
    if (!mysqlDB.open())
    {
        log( "database error" );
        remoteDBConnected = false;
    }
    else
    {
        log ( "database connected" );
        remoteDBConnected = true;
    }

}

void syncDialog::log(QString s)
{
    ui->plainTextEdit_log->appendPlainText(s);
}

syncDialog::~syncDialog()
{
    delete ui;
}
