#include "organizerduplicatefiledialog.h"
#include "ui_organizerduplicatefiledialog.h"
#include <QFile>
#include <QFileInfo>

organizerDuplicateFileDialog::organizerDuplicateFileDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::organizerDuplicateFileDialog)
{
    ui->setupUi(this);



    connect ( ui->pushButton_delete     , SIGNAL(clicked()) , this , SLOT(accept()) );
    connect ( ui->pushButton_overWrite  , SIGNAL(clicked()) , this , SLOT(close()) );

    QTimer *timer=new QTimer(this);
    timer->singleShot(100,this,SLOT(init()));

}

void organizerDuplicateFileDialog::init()
{
    ui->label_source->setText(fileFrom);
    if (fileFrom.size()>60) ui->label_source->setText("..."+fileFrom.right(fileFrom.size()-40));
    QFileInfo file1(fileFrom);
    qint64 fileSize = file1.size();
    QString fileSizeStr = QString::number(fileSize);
    if (fileSize>1000)          fileSizeStr.insert(fileSizeStr.size()-3," ");
    if (fileSize>1000000)       fileSizeStr.insert(fileSizeStr.size()-7," ");
    if (fileSize>1000000000)    fileSizeStr.insert(fileSizeStr.size()-11," ");
    ui->label_sourceSize->setText(fileSizeStr + " Bytes");

    ui->label_target->setText(fileTo);
    if (fileTo.size()>60) ui->label_target->setText("..."+fileTo.right(fileTo.size()-40));
    QFileInfo file2(fileTo);
    fileSize = file2.size();
    fileSizeStr = QString::number(fileSize);
    if (fileSize>1000)          fileSizeStr.insert(fileSizeStr.size()-3," ");
    if (fileSize>1000000)       fileSizeStr.insert(fileSizeStr.size()-7," ");
    if (fileSize>1000000000)    fileSizeStr.insert(fileSizeStr.size()-11," ");
    ui->label_targetSize->setText(fileSizeStr + " Bytes");
}

void organizerDuplicateFileDialog::setFiles(QString from, QString to)
{
    fileTo = to;
    fileFrom = from;
}

organizerDuplicateFileDialog::~organizerDuplicateFileDialog()
{
    delete ui;
}
