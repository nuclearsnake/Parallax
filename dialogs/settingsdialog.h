#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>

#include "managers/librarymanager.h"

namespace Ui {
class settingsDialog;
}

class settingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit settingsDialog(QWidget *parent = 0);
    libraryManager *libMan;
    QString path_photoMatix;
    QString path_photoShop;
    QString path_ptGui;
    QString path_defaultArchiveFolder;
    QString scoringMode;


    ~settingsDialog();

    void attachLibMan(libraryManager *l);
private slots:
    void selectPhotoMatix();
    void selectPhotoShop();
    void selectPtGui();
    void submit();
    void selectArchiveFolder();
private:
    Ui::settingsDialog *ui;

};

#endif // SETTINGSDIALOG_H
