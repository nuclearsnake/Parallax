#include "ptsclonerdialog.h"
#include "ui_ptsclonerdialog.h"
#include <QDir>
#include <QDebug>
#include <QFile>
#include <QTextStream>

PtsClonerDialog::PtsClonerDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PtsClonerDialog)
{
    ui->setupUi(this);

    this->setWindowTitle("Clone PTS");
    this->setWindowIcon(QIcon(":/icons/icons/duplicate-icon-md.png"));

    /*
    dir = new QDir;
    dir->setPath(projectPath);
    qDebug() << " ProjectPath: " << projectPath;
    qDebug() << dir->entryList();
    */

    connect ( ui->pushButtonGo          , SIGNAL(clicked())             , this , SLOT(go()) );
    connect ( ui->pushButtonCancel      , SIGNAL(clicked())             , this , SLOT(close()) );
    connect ( ui->pushButtonClose       , SIGNAL(clicked())             , this , SLOT(close()) );
    connect ( ui->comboBoxSourceSelect  , SIGNAL(activated(QString))    , this , SLOT(evSelector(QString)) );

}

void PtsClonerDialog::evSelector(QString source)
{
    QString ev =  source.mid(source.indexOf("ev")+2,1) ;
    ui->lineEdit_inEv->setText(ev);


}

void PtsClonerDialog::evCalculator(QString ev)
{
    if (ev=="0")
    {
        ui->checkBoxEv0->setDisabled(true);
        ui->lineEditEv0->setDisabled(true);
        ui->lineEditEv1->setText(ui->comboBoxSourceSelect->currentText().replace("ev0","ev1"));
        ui->lineEditEv2->setText(ui->comboBoxSourceSelect->currentText().replace("ev0","ev2"));
    }

}

void PtsClonerDialog::replacer(QString inF, QString outF , QString inEv , QString outEv )
{
    QFile inputFile(projectPath + inF);
    QFile outFile(projectPath + outF );
        if (inputFile.open(QIODevice::ReadOnly))
        {
           QTextStream in(&inputFile);
           outFile.open(QIODevice::WriteOnly | QIODevice::Text);
           QTextStream out(&outFile);
           while ( !in.atEnd() )
           {
              QString line = in.readLine();
              //...
              ui->log->appendPlainText(line);
              line = line.replace(inEv+".tif",outEv+".tif");
              out << line + "\n";
           }
           inputFile.close();
           outFile.close();
        }
}

void PtsClonerDialog::go()
{
    QString file = ui->comboBoxSourceSelect->currentText();
    ui->log->appendPlainText( projectPath + file );

    if (ui->checkBoxEv1->isChecked()) replacer( file , ui->lineEditEv1->text() , ui->lineEdit_inEv->text() , "1" );
    if (ui->checkBoxEv2->isChecked()) replacer( file , ui->lineEditEv2->text() , ui->lineEdit_inEv->text() , "2" );



}


void PtsClonerDialog::setProjectPath(QString  p)
{
    projectPath=p;
    qDebug() << "SEEET " << p;
    qDebug() << "SEEET" << projectPath;
    dir = new QDir;
    dir->setPath(projectPath);
    qDebug() << " ProjectPath: " << projectPath;
    qDebug() << dir->entryList();

    int first = 0;
    for ( int i = 0;i<dir->entryList().count();i++ )
    {
        QString s= dir->entryList().at(i);
        qDebug() << s;
        if (s.contains(".pts"))
        {
            ui->log->appendPlainText(s);
            ui->comboBoxSourceSelect->addItem(s);
            if (!first)
            {
                QString ev =  s.mid(s.indexOf("ev")+2,1) ;
                ui->lineEdit_inEv->setText(ev);
                this->evCalculator(ev);
            }
            first++;
        }
    }

}

PtsClonerDialog::~PtsClonerDialog()
{
    delete ui;
}
