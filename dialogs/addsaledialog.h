#ifndef ADDSALEDIALOG_H
#define ADDSALEDIALOG_H

#include <QDialog>
#include "managers/librarymanager.h"

namespace Ui {
class addSaleDialog;
}

class addSaleDialog : public QDialog
{
    Q_OBJECT

public:
    explicit addSaleDialog(QWidget *parent = 0);
    ~addSaleDialog();
    libraryManager *libMan;

    void attachLibMan(libraryManager *l);
    int archiveID;
    QString archiveTitle;

public slots:
    void init();
    void save();
private:
    Ui::addSaleDialog *ui;
};

#endif // ADDSALEDIALOG_H
