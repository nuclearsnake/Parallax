#ifndef ADDARCHIVEDIALOG_H
#define ADDARCHIVEDIALOG_H

#include <QDialog>
#include <QFileSystemModel>
#include <QList>

namespace Ui {
class addArchiveDialog;
}

class addArchiveDialog : public QDialog
{
    Q_OBJECT

public:
    explicit addArchiveDialog(QWidget *parent = 0);
    ~addArchiveDialog();
    QString image;
    QString title;
    QString folder;
    QString keywords;
    QString date;
    QString originalTitle;
    QList<QString> titleList;
    QFileSystemModel *files;
    QString dirName;

    void setTitle(QString value);
    void setFolder(QString value);
    void setImage(QString value);
    void setDate(QString value);
    void setTitleList(QList<QString> value);

    void folderSelect(QString d);
private slots:
    void submit();
    void selectFolder();
    void selectImage();
    void calendar();

    void loadImageFromTree(QModelIndex index);
private:
    Ui::addArchiveDialog *ui;
};

#endif // ADDARCHIVEDIALOG_H
