#ifndef FOLLOWERSANDPAGEVIEWSDIALOG_H
#define FOLLOWERSANDPAGEVIEWSDIALOG_H

#include <QDialog>
#include "managers/librarymanager.h"

namespace Ui {
class followersAndPageviewsDialog;
}

class followersAndPageviewsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit followersAndPageviewsDialog(QWidget *parent = 0);
    ~followersAndPageviewsDialog();
    libraryManager *libMan;
    QSqlDatabase mysqlDB;

    void attachLibMan(libraryManager *l);
    void connectRemoteDB();
    QString theQuery;
    void go(QString filter);
    void reset();
    QFont fontNorm;
    QFont fontBold;
public slots:
    void init();
    void clickHandler();
private:
    Ui::followersAndPageviewsDialog *ui;
};

#endif // FOLLOWERSANDPAGEVIEWSDIALOG_H
