#ifndef EXTSTATSDIALOG_H
#define EXTSTATSDIALOG_H

#include <QDialog>
#include "managers/librarymanager.h"

namespace Ui {
class extStatsDialog;
}

class extStatsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit extStatsDialog(QWidget *parent = 0);
    ~extStatsDialog();
    libraryManager *libMan;
    int selectedId;
    QString title;
    QString score;
    QString date;
    float scoreBias;

    void attachLibman(libraryManager *l);
public slots:
    void go();
private:
    Ui::extStatsDialog *ui;
};

#endif // EXTSTATSDIALOG_H
