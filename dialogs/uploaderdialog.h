#ifndef UPLOADERDIALOG_H
#define UPLOADERDIALOG_H

#include <QDialog>
#include <QFileSystemModel>

namespace Ui {
class uploaderDialog;
}

class uploaderDialog : public QDialog
{
    Q_OBJECT

public:
    explicit uploaderDialog(QWidget *parent = 0);
    ~uploaderDialog();
    QString title;
    QString title2;
    QString date;
    QString keywords;
    QString folder;
    int id;
    QFileSystemModel    *fileModel;

public slots:
    void go();
    void fill();
    void preview(QModelIndex index);
private:
    Ui::uploaderDialog *ui;
};

#endif // UPLOADERDIALOG_H
