#ifndef ADDCOLLECTIONDIALOG_H
#define ADDCOLLECTIONDIALOG_H

#include <QDialog>

namespace Ui {
class addCollectionDialog;
}

class addCollectionDialog : public QDialog
{
    Q_OBJECT

public:
    explicit addCollectionDialog(QWidget *parent = 0);
    ~addCollectionDialog();

    QString type;
    QString name;
    QString param;

    void setEditMode(QString name, QString type, QString text);
private slots:
    void submit();
    void setType(bool checked);
private:
    Ui::addCollectionDialog *ui;
};

#endif // ADDCOLLECTIONDIALOG_H
