#include "tablebrowserdialog.h"
#include "ui_tablebrowserdialog.h"
#include "extensions/tools.h"


#include <QSqlQuery>
#include <QDebug>
#include <QTimer>
#include <QSortFilterProxyModel>
#include <QStandardItemModel>
#include <QSqlRecord>
#include <QSqlField>
#include <QDirIterator>
#include <QHeaderView>
#include <QTableView>


tableBrowserDialog::tableBrowserDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::tableBrowserDialog)
{
    ui->setupUi(this);

    libMan = new libraryManager(this);

    ui->tableView->horizontalHeader()->setStretchLastSection(true);
   // ui->tableView->horizontalHeader()->ResizeMode(QHeaderView::ResizeToContents);
    ui->tableView->setSortingEnabled(true);
   // ui->tableView->verticalHeader()->ResizeMode(QHeaderView::ResizeToContents);

    ui->progressBar->setVisible(false);

    connect ( ui->tableView         , SIGNAL(doubleClicked(QModelIndex))    , this  , SLOT(lineDblClicked(QModelIndex)) );
    connect ( ui->pushButton_close  , SIGNAL(clicked())                     , this  , SLOT(close()) );

    QTimer *timer=new QTimer(this);

    timer->singleShot(100,this,SLOT( runModule()  ));


}

void tableBrowserDialog::lineDblClicked(QModelIndex index)
{
    this->selecedTitle = index.sibling(index.row(),0).data().toString();
    //this->selecedTitle = index.
    this->accept();
}

void tableBrowserDialog::setModule(QString m)
{
    module = m;
}

void tableBrowserDialog::setParam(QString p)
{
    param = p;
}

void tableBrowserDialog::runModule()
{

    if (module=="scoresBrowser")
        {
            //this->queryBased("SELECT t1.title title , ROUND(AVG(t1.subscore),1) score FROM ( SELECT archive.title title, ( CAST(sites.favs  AS REAL ) / sa.maxFavs + CAST(sites.views  AS REAL ) / sa.maxViews )* 2.5 subscore FROM archive LEFT JOIN sites ON sites.projectId=archive.id LEFT JOIN ( SELECT sites.site, CAST(AVG(sites.views) AS INTEGER) avgViews ,MAX(sites.views) maxViews , CAST(AVG(sites.favs) AS INTEGER) avgFavs ,MAX(sites.favs) maxFavs FROM sites GROUP BY sites.site ) sa ON sa.site = sites.site                WHERE sites.site != 'FB'  ) t1 GROUP BY t1.title ORDER BY AVG(t1.subscore) DESC") ;
            //this->queryBased("SELECT t1.title, ROUND(AVG(t1.subscore),2) score            FROM            (            SELECT archive.id,archive.title title,sites.site,                ROUND( ( CAST(sites.views  AS REAL ) / sa.maxViews )* 5 ,2) subscore                     FROM archive             LEFT JOIN sites ON sites.projectId=archive.id             LEFT JOIN                 (                 SELECT sites.site, MAX(sites.views) maxViews                 FROM sites                 GROUP BY sites.site                 ) sa                 ON sa.site = sites.site                            WHERE sites.site != 'FB'                         UNION                        SELECT archive.id,archive.title title,sites.site,                ROUND( ( CAST(sites.favs  AS REAL ) / sa.maxFavs )* 5 ,2) subscore                   FROM archive             LEFT JOIN sites ON sites.projectId=archive.id             LEFT JOIN                 (                 SELECT sites.site, MAX(sites.favs) maxFavs                FROM sites                 GROUP BY sites.site                 ) sa                 ON sa.site = sites.site                                   ) t1        GROUP BY t1.title ORDER BY ROUND(AVG(t1.subscore),2) DESC");
            //QString query="SELECT t5.title title , t5.score SCORE, t5.DA DA, t5.`5P` `5P` , t5.`G+` `G+` , t5.FB  FB, t5.FL FL  FROM  (                    SELECT t4.title                        , CASE WHEN COALESCE(t4.DA,t4.`5P`,t4.`G+`,t4.FB,t4.FL) IS NOT NULL                              THEN                              ROUND(   (COALESCE(2*t4.DA,0) + COALESCE(2*t4.`5P`,0) + COALESCE(2*t4.`G+`,0) + COALESCE(t4.FB,0) + COALESCE(2*t4.FL,0)) /                                (                                    CASE WHEN t4.DA   IS NULL THEN 0 ELSE 2 END +                                    CASE WHEN t4.`5P` IS NULL THEN 0 ELSE 2 END +                                    CASE WHEN t4.`G+` IS NULL THEN 0 ELSE 2 END +                                    CASE WHEN t4.FB   IS NULL THEN 0 ELSE 1 END +                                    CASE WHEN t4.FL   IS NULL THEN 0 ELSE 2 END                                                                 ) ,2 )                             ELSE                                 0                             END AS score                                             ,t4.DA                        ,t4.`5P`                        ,t4.`G+`                        ,t4.FB                        ,t4.FL                                        FROM                        (                        SELECT                                                     t3.title                                                         , SUM(DA) DA, SUM(`5P`) `5P`,SUM(`G+`) `G+`,SUM(FB) FB,SUM(FL) FL                        FROM                            (                            SELECT   t2.title                                   , CASE WHEN t2.site = 'DA' THEN t2.subscore ELSE NULL END as DA                                   , CASE WHEN t2.site = '5P' THEN t2.subscore ELSE NULL END as `5P`                                   , CASE WHEN t2.site = 'G+' THEN t2.subscore ELSE NULL END as `G+`                                   , CASE WHEN t2.site = 'FB' THEN t2.subscore ELSE NULL END as FB                                   , CASE WHEN t2.site = 'FL' THEN t2.subscore ELSE NULL END as FL                            FROM                                (                                SELECT t1.title,t1.site , ROUND( AVG(t1.subscore) ,2) subscore                                FROM                                    (                                    SELECT archive.id,archive.title title,sites.site,                                        ROUND( ( CAST(sites.views  AS REAL ) / sa.maxViews )* 5 ,3) subscore                                             FROM archive                                     LEFT JOIN sites ON sites.projectId=archive.id                                     LEFT JOIN                                         (                                         SELECT sites.site, MAX(sites.views) maxViews                                         FROM sites                                         GROUP BY sites.site                                         ) sa                                         ON sa.site = sites.site                                                    WHERE sites.site != 'FB'                                                                                         UNION                                                                        SELECT archive.id,archive.title title,sites.site,                                        ROUND( ( CAST(sites.favs  AS REAL ) / sa.maxFavs )* 5 ,3) subscore                                           FROM archive                                     LEFT JOIN sites ON sites.projectId=archive.id                                     LEFT JOIN                                         (                                         SELECT sites.site, MAX(sites.favs) maxFavs                                        FROM sites                                         GROUP BY sites.site                                         ) sa                                         ON sa.site = sites.site                                                                                       ) t1                                GROUP BY t1.title,t1.site                                ) t2                            ) t3                        GROUP BY t3.title                        ) t4                    ) t5                    ORDER BY score DESC";
            QString query=libMan->loadQuery("score_subscores.sql");
            this->queryBased(query);
            this->setWindowTitle("Scores");
            ui->tableView->setColumnWidth(0,525);
        }

    if (module=="sitesManage")
        {
            this->queryBased("SELECT * FROM sites WHERE projectId=" + param) ;
            this->setWindowTitle("Manage sites");
            ui->tableView->setColumnWidth(0,25);
            ui->tableView->setColumnWidth(1,50);
            ui->tableView->setColumnWidth(2,150);
            ui->tableView->setColumnWidth(3,450);
            ui->tableView->setColumnWidth(4,75);
            ui->tableView->setColumnWidth(5,50);
            ui->tableView->setColumnWidth(6,50);
            ui->tableView->setColumnWidth(7,50);
        }

    if (module=="notesBrowser")
        {
            this->notesBrowser();
            this->setWindowTitle("Archive notes");
            this->setWindowIcon(QIcon(":/icons/icons/Sticky_Notes.png"));
        }
    if (module=="sitesBrowser")
        {
            //this->queryBased("SELECT title,id,projectDir,keywords,date,image,notes FROM archive ORDER BY title");
            /*
             this->queryBased("SELECT  t3.title as title,"
                                         " t3.DA + t3._500px + t3.google + t3.FB + t3.flickr as total_views ,"
                                         " CASE WHEN t3.DA>0      THEN '*' ELSE ' ' END as DA,"
                                         " CASE WHEN t3._500px>0  THEN '*' ELSE ' ' END as `500px`,"
                                         " CASE WHEN t3.google>0 THEN '*' ELSE ' ' END as `google+`,"
                                         " CASE WHEN t3.FB>0 THEN '*' ELSE ' ' END as FaceBook,"
                                         " CASE WHEN t3.flickr>0 THEN '*' ELSE ' ' END as Flickr"

                                  "FROM"
                                      "("
                                      "SELECT t2.title title ,SUM(t2.DA) DA,SUM(t2._500px) _500px,SUM(t2.google) google,SUM(t2.FB) FB,SUM(t2.flickr) flickr"
                                      "FROM"
                                          "("
                                          "SELECT  t1.title,"
                                                  "t1.url,"
                                                  "CASE WHEN t1.url LIKE '%deviantart%'    THEN t1.views ELSE 0  END as DA,"
                                                  "CASE WHEN t1.url LIKE '%500px.com%'     THEN t1.views ELSE 0  END as _500px,"
                                                  "CASE WHEN t1.url LIKE '%plus.google%'   THEN t1.views ELSE 0  END as google,"
                                                  "CASE WHEN t1.url LIKE '%facebook%'      THEN t1.views ELSE 0  END as FB,"
                                                  "CASE WHEN t1.url LIKE '%flickr.com%'    THEN t1.views ELSE 0  END as flickr        "

                                          "FROM"
                                              "("
                                                  "SELECT archive.id id, archive.title title, sites.url url, sites.views views"
                                                  "FROM archive"
                                                  "LEFT JOIN sites ON sites.projectId = archive.id"
                                                  "--WHERE archive.title=='Glory pt.01'"
                                                  "--ORDER BY archive.title"
                                              ") t1"
                                          ") t2"
                                      "GROUP BY t2.title"
                                      ") t3"
                                  "ORDER BY title" );
                              */

            //QString query = "SELECT  t3.title as title,t3.DA + t3._500px + t3.google + t3.FB + t3.flickr as total_views , CASE WHEN t3.DA>0  THEN '*' ELSE ' ' END as DA,CASE WHEN t3._500px>0  THEN '*' ELSE ' ' END as `500px`,                             CASE WHEN t3.google>0 THEN '*' ELSE ' ' END as `google+`,                             CASE WHEN t3.FB>0 THEN '*' ELSE ' ' END as FB,           CASE WHEN t3.flickr>0 THEN '*' ELSE ' ' END as Flickr FROM ( SELECT t2.title title ,SUM(t2.DA) DA,SUM(t2._500px) _500px,SUM(t2.google) google,SUM(t2.FB) FB,SUM(t2.flickr) flickr FROM ( SELECT  t1.title, t1.url, CASE WHEN t1.url LIKE '%deviantart%'    THEN t1.views+1 ELSE 0  END as DA,  CASE WHEN t1.url LIKE '%500px.com%'     THEN t1.views+1 ELSE 0  END as _500px, CASE WHEN t1.url LIKE '%plus.google%'   THEN t1.views+1 ELSE 0  END as google, CASE WHEN t1.url LIKE '%facebook%'      THEN t1.views+1 ELSE 0  END as FB, CASE WHEN t1.url LIKE '%flickr.com%'    THEN t1.views+1 ELSE 0  END as flickr  FROM ( SELECT archive.id id, archive.title title, sites.url url, sites.views views FROM archive LEFT JOIN sites ON sites.projectId = archive.id  ) t1 ) t2 GROUP BY t2.title ) t3 ORDER BY title";
            //QString query="SELECT t5.title , t5.score , t5.DA , t5.`5P` , t5.`G+` , t5.FB , t5.FL  FROM  (                    SELECT t4.title                        , CASE WHEN COALESCE(t4.DA,t4.`5P`,t4.`G+`,t4.FB,t4.FL) IS NOT NULL                              THEN                              ROUND(   (COALESCE(2*t4.DA,0) + COALESCE(2*t4.`5P`,0) + COALESCE(2*t4.`G+`,0) + COALESCE(t4.FB,0) + COALESCE(2*t4.FL,0)) /                                (                                    CASE WHEN t4.DA   IS NULL THEN 0 ELSE 2 END +                                    CASE WHEN t4.`5P` IS NULL THEN 0 ELSE 2 END +                                    CASE WHEN t4.`G+` IS NULL THEN 0 ELSE 2 END +                                    CASE WHEN t4.FB   IS NULL THEN 0 ELSE 1 END +                                    CASE WHEN t4.FL   IS NULL THEN 0 ELSE 2 END                                                                 ) ,2 )                             ELSE                                 0                             END AS score                                             ,t4.DA                        ,t4.`5P`                        ,t4.`G+`                        ,t4.FB                        ,t4.FL                                        FROM                        (                        SELECT                                                     t3.title                                                         , SUM(DA) DA, SUM(`5P`) `5P`,SUM(`G+`) `G+`,SUM(FB) FB,SUM(FL) FL                        FROM                            (                            SELECT   t2.title                                   , CASE WHEN t2.site = 'DA' THEN t2.subscore ELSE NULL END as DA                                   , CASE WHEN t2.site = '5P' THEN t2.subscore ELSE NULL END as `5P`                                   , CASE WHEN t2.site = 'G+' THEN t2.subscore ELSE NULL END as `G+`                                   , CASE WHEN t2.site = 'FB' THEN t2.subscore ELSE NULL END as FB                                   , CASE WHEN t2.site = 'FL' THEN t2.subscore ELSE NULL END as FL                            FROM                                (                                SELECT t1.title,t1.site , ROUND( AVG(t1.subscore) ,2) subscore                                FROM                                    (                                    SELECT archive.id,archive.title title,sites.site,                                        ROUND( ( CAST(sites.views  AS REAL ) / sa.maxViews )* 5 ,3) subscore                                             FROM archive                                     LEFT JOIN sites ON sites.projectId=archive.id                                     LEFT JOIN                                         (                                         SELECT sites.site, MAX(sites.views) maxViews                                         FROM sites                                         GROUP BY sites.site                                         ) sa                                         ON sa.site = sites.site                                                    WHERE sites.site != 'FB'                                                                                         UNION                                                                        SELECT archive.id,archive.title title,sites.site,                                        ROUND( ( CAST(sites.favs  AS REAL ) / sa.maxFavs )* 5 ,3) subscore                                           FROM archive                                     LEFT JOIN sites ON sites.projectId=archive.id                                     LEFT JOIN                                         (                                         SELECT sites.site, MAX(sites.favs) maxFavs                                        FROM sites                                         GROUP BY sites.site                                         ) sa                                         ON sa.site = sites.site                                                                                       ) t1                                GROUP BY t1.title,t1.site                                ) t2                            ) t3                        GROUP BY t3.title                        ) t4                    ) t5                    ORDER BY score DESC";

            QString query=libMan->loadQuery("score_subscores.sql");

            //this->queryBased(query);

            //***** CUSTOM MODEL *****

            dataModel = new QSqlQueryModel(this);
            dataModel->setQuery(query,db);
            if (dataModel->canFetchMore()) dataModel->fetchMore();

            QStandardItemModel *customModel = new QStandardItemModel();
            customModel->setColumnCount(3);
            QStringList labels;
            labels << "title"  << "" << "" << "" << "" << "" << "total score";
            customModel->setHorizontalHeaderLabels(labels);
            customModel->horizontalHeaderItem(1)->setIcon(QIcon(":/icons/icons/siteIconDeviantArt.png"));
            customModel->horizontalHeaderItem(2)->setIcon(QIcon(":/icons/icons/siteIcon500px.png"));
            customModel->horizontalHeaderItem(3)->setIcon(QIcon(":/icons/icons/siteIconGoogle+.png"));
            customModel->horizontalHeaderItem(4)->setIcon(QIcon(":/icons/icons/siteIconFaceBook.png"));
            customModel->horizontalHeaderItem(5)->setIcon(QIcon(":/icons/icons/siteIconFlickr.png"));

            QIcon iconYes =  QIcon(":/icons/icons/star_32.png");

            int i;
            for (i=0; i<dataModel->rowCount();++i)
                {
                    QSqlRecord record =  dataModel->record(i);
                    QList<QStandardItem*> items;
                    QStandardItem *cell;
                    QString value="";

                    // *0* title
                    items   << new QStandardItem( record.field(0).value().toString() );



                    // *2* deviantArt
                    value = record.field(2).value().toString();
                    qDebug() << value ;

                    if (value!="")
                        {
                            cell = new QStandardItem("");
                            cell->setIcon(iconYes);
                        }
                        else
                        {
                            cell = new QStandardItem(" ");
                            //cell->setIcon(iconYes);
                        }
                    cell->setData(Qt::AlignCenter);
                    items   << cell;

                    // *3* 500px
                    value = record.field(3).value().toString();
                    qDebug() << value ;

                    if (value!="")
                        {
                            cell = new QStandardItem("");
                            cell->setIcon(iconYes);
                        }
                        else
                        {
                            cell = new QStandardItem(" ");
                            //cell->setIcon(iconYes);
                        }
                    cell->setData(Qt::AlignCenter);
                    items   << cell;

                    // *3* Google+
                    value = record.field(4).value().toString();
                    qDebug() << value ;

                    if (value!="")
                        {
                            cell = new QStandardItem("");
                            cell->setIcon(iconYes);
                        }
                        else
                        {
                            cell = new QStandardItem(" ");
                            //cell->setIcon(iconYes);
                        }
                    cell->setData(Qt::AlignCenter);
                    items   << cell;

                    // *3* FaceBook
                    value = record.field(5).value().toString();
                    qDebug() << value ;

                    if (value!="")
                        {
                            cell = new QStandardItem("");
                            cell->setIcon(iconYes);
                        }
                        else
                        {
                            cell = new QStandardItem(" ");
                            //cell->setIcon(iconYes);
                        }
                    cell->setData(Qt::AlignCenter);
                    items   << cell;

                    // *3* Flickr
                    value = record.field(6).value().toString();
                    qDebug() << value ;

                    if (value!="")
                        {
                            cell = new QStandardItem("");
                            cell->setIcon(iconYes);
                        }
                        else
                        {
                            cell = new QStandardItem(" ");
                            //cell->setIcon(iconYes);
                        }
                    cell->setData(Qt::AlignCenter);
                    items   << cell;

                    // *1* total views
                    cell = new QStandardItem( record.field(1).value().toString() /*tools::numGrouped( record.field(1).value().toString().toInt() ).rightJustified(10,' ')*/ );
                    cell->setData(Qt::AlignRight, Qt::TextAlignmentRole);
                    items   << cell;


                    //items   << new QStandardItem( record.field(7).value().toString() );

                    customModel->appendRow(items);
                }


            ui->tableView->setModel(customModel);

            //***** CUSTOM MODEL END *****

            this->setWindowTitle("Sites table view");
            this->setWindowIcon(QIcon(":/icons/icons/table_icon.png"));

            ui->tableView->setColumnWidth(0,350);
            ui->tableView->setColumnWidth(1,25);
            ui->tableView->setColumnWidth(2,25);
            ui->tableView->setColumnWidth(3,25);
            ui->tableView->setColumnWidth(4,25);
            ui->tableView->setColumnWidth(5,25);
            resize(650,600);




            /*
            for (int i=0; dataModel->rowCount()>i;++i)
                {
                    qDebug() << dataModel->index(i,5).data().toString();
                    dataModel->setData(dataModel->index(i,5),QVariant("asjdhjh") ,Qt::EditRole );
                }
            */



        }

    if (module=="allArchives")
        {
            this->queryBased("SELECT title,id,projectDir,keywords,date,image,notes FROM archive  WHERE id > 513 ORDER BY id");
            ui->tableView->setColumnWidth(1,25);
            ui->tableView->setColumnWidth(2,250);
            ui->tableView->setColumnWidth(4,75);
            ui->tableView->resizeRowsToContents();
            this->setWindowTitle("Archive table view");
            this->setWindowIcon(QIcon(":/icons/icons/table_icon.png"));

            for (int i=0;i<dataModel->rowCount();++i)
            {

                QString fname = dataModel->record(i).value("image").toString();
                int fidnum = dataModel->record(i).value("id").toInt();
                QString fidstr = dataModel->record(i).value("id").toString();

                if (fidnum<10)   fidstr = "0" + fidstr;
                if (fidnum<100)  fidstr = "0" + fidstr;
                if (fidnum<1000) fidstr = "0" + fidstr;

                QFile::copy(
                        "f:/Parallax/realitydream/previews/"+ fname ,
                        "f:/Parallax/realitydream/export/"+fidstr+"__" + fname

                            );
                //qDebug() <<  "f:/Parallax/realitydream/previews/"+dataModel->record(i).value("image").toString() ;
            }

        }
    if (module=="folderSizes")
        {
            this->folderSizes();
            this->setWindowTitle("Archive folder sizes");
            this->setWindowIcon(QIcon(":/icons/icons/disk-icon.png"));
        }
}

void tableBrowserDialog::notesBrowser()
{
    this->queryBased("SELECT title,notes FROM archive WHERE notes!=''");
}

void tableBrowserDialog::folderSizes()
{
    dataModel = new QSqlQueryModel(this);
    dataModel->setQuery("SELECT title,projectDir,id,0 as files,0 as size FROM archive ORDER BY title",db);
    if (dataModel->canFetchMore()) dataModel->fetchMore();

    QStandardItemModel *customModel = new QStandardItemModel();
    customModel->setColumnCount(6);
    ui->tableView->setModel(customModel);

    ui->tableView->setColumnWidth(0,200);
    ui->tableView->setColumnWidth(1,400);
    ui->tableView->setColumnWidth(2,60);
    ui->tableView->setColumnWidth(3,60);
    ui->tableView->setColumnWidth(4,60);

    ui->progressBar->setVisible(true);
    //customModel->setSortRole();

    QStringList labels;
    labels << "title" << "projectDir" << "id" << "subfolders" << "files" << "size";
    customModel->setHorizontalHeaderLabels(labels);
    qint64 totalSize=0;
    int i;
    for (i=0; i<dataModel->rowCount();++i)
        {
            QSqlRecord record =  dataModel->record(i);
            QList<QStandardItem*> items;
            //for (int j=0;j<record.count();++j)
            //    {
            //        items << new QStandardItem( record.field(j).value().toString() );
            //    }
            items   << new QStandardItem( record.field(0).value().toString() );
            items   << new QStandardItem( record.field(1).value().toString() );
            QStandardItem *id = new QStandardItem( record.field(2).value().toString().rightJustified(4,' ') );
            id->setData(Qt::AlignRight, Qt::TextAlignmentRole);
            items   <<  id;

            QString folder = record.field(1).value().toString();
            int fileCount=0;
            int folderCount=0;
            qint64 fileSize=0;
            QDirIterator iterator(QDir(folder).absolutePath(), QDirIterator::Subdirectories);
               while (iterator.hasNext()) {
                  iterator.next();
                  if (!iterator.fileInfo().isDir())
                    {
                        QString filename = iterator.fileName();
                        ++fileCount;
                        fileSize+=iterator.fileInfo().size();
                    }
                    else if( ( iterator.fileName()!="." ) & ( iterator.fileName()!=".." ) ) ++folderCount;
               }
            //qDebug() << "foldercount:" << folderCount;
            //items   << new QStandardItem( QString::number(folderCount) );
            QStandardItem* itemCount1 = new QStandardItem( QString::number(folderCount).rightJustified(4,' ') );
            itemCount1->setData(Qt::AlignRight, Qt::TextAlignmentRole);
            items   << itemCount1;

            QString fileSizeStr = QString::number(fileSize);

            totalSize+=fileSize;

            if (fileSize>1000)          fileSizeStr.insert(fileSizeStr.size()-3," ");
            if (fileSize>1000000)       fileSizeStr.insert(fileSizeStr.size()-7," ");
            if (fileSize>1000000000)    fileSizeStr.insert(fileSizeStr.size()-11," ");

            qDebug() << fileSizeStr.rightJustified(20,' ');
            QStandardItem* itemCount = new QStandardItem( QString::number(fileCount).rightJustified(4,' ') );
            itemCount->setData(Qt::AlignRight, Qt::TextAlignmentRole);
            items   << itemCount;
            // .setData(Qt::AlignRight, Qt::TextAlignmentRole)
            QStandardItem* itemSize = new QStandardItem( fileSizeStr.rightJustified(20,' ') + " bytes" );
            itemSize->setData(Qt::AlignRight, Qt::TextAlignmentRole);
            items   << itemSize;

            customModel->appendRow(items);
            ui->progressBar->setValue(100*i/dataModel->rowCount());
            if(i % 10 == 0)  QCoreApplication::processEvents();
        }

    QString totalSizeStr = QString::number(totalSize);
    if (totalSize>1000)          totalSizeStr.insert(totalSizeStr.size()-3," ");
    if (totalSize>1000000)       totalSizeStr.insert(totalSizeStr.size()-7," ");
    if (totalSize>1000000000)    totalSizeStr.insert(totalSizeStr.size()-11," ");

    qint64 avgSize = totalSize / i;
    QString avgSizeStr = QString::number(avgSize);
    if (avgSize>1000)          avgSizeStr.insert(avgSizeStr.size()-3," ");
    if (avgSize>1000000)       avgSizeStr.insert(avgSizeStr.size()-7," ");
    if (avgSize>1000000000)    avgSizeStr.insert(avgSizeStr.size()-11," ");

    ui->label_info->setText("Projects: <b>" + QString::number(i) + "</b>    &nbsp;&nbsp;&nbsp;     Total bytes: <b>" +  totalSizeStr + "</b>  &nbsp;&nbsp;&nbsp;   Average bytes: <b>" + avgSizeStr +"</b>" );


    ui->progressBar->setVisible(false);
}

void tableBrowserDialog::queryBased(QString query)
{
    dataModel = new QSqlQueryModel(this);
    dataModel->setQuery(query,db);
    if (dataModel->canFetchMore()) dataModel->fetchMore();

    QSortFilterProxyModel *proxyModel = new QSortFilterProxyModel(this) ;
    proxyModel->setDynamicSortFilter(true);
    proxyModel->setSourceModel( dataModel );

    ui->tableView->setModel(proxyModel);
}

void tableBrowserDialog::setDb(QSqlDatabase database)
{
    db = database;
}

tableBrowserDialog::~tableBrowserDialog()
{
    delete ui;
}
