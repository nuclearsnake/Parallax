#include "archivefolderflattendialog.h"
#include "ui_archivefolderflattendialog.h"
#include <QTimer>

archiveFolderFlattenDialog::archiveFolderFlattenDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::archiveFolderFlattenDialog)
{
    ui->setupUi(this);
    connect ( ui->pushButton_close      , SIGNAL(clicked(bool))     , this      , SLOT(close()) );
    connect ( ui->pushButton_start      , SIGNAL(clicked(bool))     , this      , SLOT(go())    );

    QTimer::singleShot(100,this,SLOT(go()));
}

void archiveFolderFlattenDialog::go()
{
    log("folder="+rootFolder);

    int i=-1;
    QDirIterator iterator(QDir(rootFolder).absolutePath(), QDirIterator::Subdirectories);
    while (iterator.hasNext())
        {
          iterator.next();
          if (!iterator.fileInfo().isDir())
            {
                ++i;
                QString fileName = iterator.fileName();
                QString relativePath = iterator.filePath().replace(rootFolder+"/","");
                QString from = iterator.filePath();
                QString to   = rootFolder + "/" + iterator.fileName();
                //md00Source->setItem(i,0,new QStandardItem(relativePath));
                log("");
                log(QString::number(i));
                log(fileName);
                log(relativePath);
                log(from);
                log(to);
                // check for the file in root folder, if yes no need to move...
                if (from != to)
                {
                    log("NOT EQUAL, proceeding...");
                    // check target file exists
                    if (QFile::exists(to))
                    {
                        log("SAME file in target (root) path...");
                    }
                    else
                    {
                        log("Target clear, proceeding...");
                        bool result;
                        result = QFile::rename(from,to);
                        if (result) log("File moved to root.");
                          else log("File move failed!!!");
                    }
                }
                else
                {
                    log("EQUAL, SKIP...");
                }

                // delete empty folders

                log("");
                log("Removing empty folders...");
                QDirIterator iterator(QDir(rootFolder).absolutePath(), QDirIterator::Subdirectories);
                    while (iterator.hasNext())
                        {
                          iterator.next();
                          if (iterator.fileInfo().isDir() && iterator.filePath().right(1) != "." )
                            {
                                log("");
                                log(iterator.filePath());
                                if (QDir(iterator.filePath()).entryInfoList(QDir::NoDotAndDotDot|QDir::AllEntries).count() == 0)
                                {
                                    log("EMPTY, proceeding...");
                                    QDir dir(iterator.filePath());
                                    dir.rmdir(iterator.filePath());
                                }
                                else
                                {
                                    log("NOT EMPTY");
                                }
                            }
                        }

            }
       }

    this->close();
}

void archiveFolderFlattenDialog::log(QString s)
{
    ui->plainTextEdit_log->appendPlainText(s);
}

archiveFolderFlattenDialog::~archiveFolderFlattenDialog()
{
    delete ui;
}
