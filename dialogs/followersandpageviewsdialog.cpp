#include "followersandpageviewsdialog.h"
#include "ui_followersandpageviewsdialog.h"
#include <QStandardItemModel>
#include "extensions/tools.h"

followersAndPageviewsDialog::followersAndPageviewsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::followersAndPageviewsDialog)
{
    ui->setupUi(this);

    Qt::WindowFlags flags = Qt::Window | Qt::WindowSystemMenuHint
                                | Qt::WindowMaximizeButtonHint
                                | Qt::WindowCloseButtonHint;
    this->setWindowFlags(flags);

    QTimer::singleShot(100,this,SLOT(init()));

    theQuery =  " SELECT date,SUM(followers) followers, SUM(pageviews) pageviews "
                        " FROM "
                        " ( "
                        "   SELECT date,siteid,MAX(followers) followers,MAX(pageviews) pageviews "
                        "   FROM sites_main_log "
                        "   %filter% "
                        "   GROUP BY date,siteid "
                        " ) t1 "
                        " GROUP BY date "
                        " ORDER BY date ";

    connect( ui->label_all , SIGNAL(Mouse_Pressed()),  this,   SLOT(clickHandler()) );
    connect( ui->label_5p  , SIGNAL(Mouse_Pressed()),  this,   SLOT(clickHandler()) );
    connect( ui->label_da  , SIGNAL(Mouse_Pressed()),  this,   SLOT(clickHandler()) );
    connect( ui->label_fb  , SIGNAL(Mouse_Pressed()),  this,   SLOT(clickHandler()) );
    connect( ui->label_fl  , SIGNAL(Mouse_Pressed()),  this,   SLOT(clickHandler()) );
    connect( ui->label_px  , SIGNAL(Mouse_Pressed()),  this,   SLOT(clickHandler()) );
    connect( ui->label_yp  , SIGNAL(Mouse_Pressed()),  this,   SLOT(clickHandler()) );
    connect( ui->label_rd  , SIGNAL(Mouse_Pressed()),  this,   SLOT(clickHandler()) );

    fontBold = ui->label_all->font();
    fontNorm = ui->label_da->font();

}

void followersAndPageviewsDialog::reset()
{
    ui->label_5p->setFont(fontNorm);
    ui->label_all->setFont(fontNorm);
    ui->label_da->setFont(fontNorm);
    ui->label_fb->setFont(fontNorm);
    ui->label_fl->setFont(fontNorm);
    ui->label_px->setFont(fontNorm);
    ui->label_yp->setFont(fontNorm);
    ui->label_rd->setFont(fontNorm);
}

void followersAndPageviewsDialog::clickHandler()
{
    qDebug() << this->sender()->objectName() ;
    QString sndr = this->sender()->objectName();

    reset();
    ui->widget->disableV = false;
    ui->widget->disableV1= false;

    if (sndr=="label_all")
    {
        ui->widget->disableV = false;
        go( " " );
        ui->label_all->setFont(fontBold);

    }

    if (sndr=="label_da")
    {
        ui->widget->disableV = false;
        go( " WHERE siteid=1 " );
        ui->label_da->setFont(fontBold);

    }

    if (sndr=="label_fb")
    {
        ui->widget->disableV = true;
        go( " WHERE siteid=2 " );
        ui->label_fb->setFont(fontBold);
    }

    if (sndr=="label_5p")
    {
        ui->widget->disableV = false;
        go( " WHERE siteid=3 " );
        ui->label_5p->setFont(fontBold);
    }

    if (sndr=="label_fl")
    {
        ui->widget->disableV = true;
        go( " WHERE siteid=4 " );
        ui->label_fl->setFont(fontBold);
    }

    if (sndr=="label_px")
    {
        ui->widget->disableV = true;
        go( " WHERE siteid=5 " );
        ui->label_px->setFont(fontBold);
    }

    if (sndr=="label_yp")
    {
        ui->widget->disableV = false;
        go( " WHERE siteid=6 " );
        ui->label_yp->setFont(fontBold);
    }
    if (sndr=="label_rd")
    {
        ui->widget->disableV1 = true;
        go( " WHERE siteid=7 " );
        ui->label_rd->setFont(fontBold);
    }
}

void followersAndPageviewsDialog::init()
{
    connectRemoteDB();

    go ( " " );

}

void followersAndPageviewsDialog::go(QString filter)
{
    QString query = theQuery;
    query.replace("%filter%",filter);

    QSqlQueryModel *model = new QSqlQueryModel(this);
    model->setQuery(query);
    while(model->canFetchMore()) model->fetchMore();

    QStandardItemModel *customModel = new QStandardItemModel();
    customModel->setColumnCount(5);
    QStringList labels;
    labels << "date"  << "followers" << "followers diff"  << "pageviews" << "pageviews diff" ;
    customModel->setHorizontalHeaderLabels(labels);

    for (int i=0;i<model->rowCount();++i)
    {

        QSqlRecord precord =  model->record(i-1);
        QSqlRecord record =   model->record(i);
        QList<QStandardItem*> items;

        QStandardItem *cDate = new QStandardItem(record.field(0).value().toString());
        cDate->setData(Qt::AlignHCenter , Qt::TextAlignmentRole);
        items << cDate;

        QStandardItem *cFv = new QStandardItem(record.field(1).value().toString());
        cFv->setData( (Qt::AlignRight )  , Qt::TextAlignmentRole);
        items << cFv;

        QStandardItem *cFvd = new QStandardItem( QString::number(record.field(1).value().toInt()-precord.field(1).value().toInt())  );
        cFvd->setData( (Qt::AlignRight )  , Qt::TextAlignmentRole);
        if (i>0)
        {
            if (record.field(1).value().toInt()-precord.field(1).value().toInt()==0)
                cFvd->setData(QBrush(Qt::yellow), Qt::BackgroundRole);
            if (record.field(1).value().toInt()-precord.field(1).value().toInt()<0)
                cFvd->setData(QBrush(Qt::red), Qt::BackgroundRole);
            if (record.field(1).value().toInt()-precord.field(1).value().toInt()>0)
                cFvd->setData(QBrush(Qt::green), Qt::BackgroundRole);
        }
        if (i==0) cFvd->setText(" ");
        items << cFvd;

        QStandardItem *cPv = new QStandardItem(tools::numGrouped(record.field(2).value().toInt()));
        cPv->setData( (Qt::AlignRight )  , Qt::TextAlignmentRole);
        items << cPv;

        QStandardItem *cPvd = new QStandardItem( QString::number(record.field(2).value().toInt()-precord.field(2).value().toInt())  );
        cPvd->setData( (Qt::AlignRight )  , Qt::TextAlignmentRole);
        if (i==0) cPvd->setText(" ");
        items << cPvd;

        customModel->appendRow(items);

    }



    ui->tableView->setModel(customModel);

    ui->widget->clear();
    ui->widget->startX = model->record(0).value("date").toInt();
    ui->widget->endX   = model->record(model->rowCount()-1).value("date").toInt();

    for (int i=0;i<model->rowCount();++i)
    {
        ui->widget->values1  << model->record(i).value("followers").toInt();
        ui->widget->values   << model->record(i).value("pageviews").toInt();
        ui->widget->labels   << i;
    }



    ui->widget->repaint();
    ui->tableView->setColumnWidth(0,60);
    ui->tableView->setColumnWidth(1,60);
    ui->tableView->setColumnWidth(2,60);
    ui->tableView->setColumnWidth(3,60);
    ui->tableView->setColumnWidth(4,60);
    //ui->tableView->resizeRowsToContents();
    ui->tableView->scrollToBottom();

}


void followersAndPageviewsDialog::attachLibMan(libraryManager *l)
{
    libMan = l;

}



void followersAndPageviewsDialog::connectRemoteDB()
{
    QSqlQueryModel *tmpModel = new QSqlQueryModel(this);
    QString query;

    query = "SELECT value FROM var WHERE KEY='syncIP'";
    tmpModel->setQuery(query,libMan->db);;
    QString remoteIP = tmpModel->index(0,0).data().toString();

    query = "SELECT value FROM var WHERE KEY='syncDB'";
    tmpModel->setQuery(query,libMan->db);;
    QString remoteDB = tmpModel->index(0,0).data().toString();

    query = "SELECT value FROM var WHERE KEY='syncUSR'";
    tmpModel->setQuery(query,libMan->db);;
    QString remoteUSR = tmpModel->index(0,0).data().toString();

    query = "SELECT value FROM var WHERE KEY='syncPASS'";
    tmpModel->setQuery(query,libMan->db);;
    QString remotePASS = tmpModel->index(0,0).data().toString();


    //log("Connecting to remote mysql DB '" + remoteDB +  "' at '" + remoteIP +"' username: '" + remoteUSR + "' password: '" + remotePASS +"'");

    mysqlDB = QSqlDatabase::addDatabase("QMYSQL");
    mysqlDB.setHostName(remoteIP);
    mysqlDB.setDatabaseName(remoteDB);
    mysqlDB.setUserName(remoteUSR);
    mysqlDB.setPassword(remotePASS);
    if (!mysqlDB.open())
    {
        //log( "database error" );
        //remoteDBConnected = false;
    }
    else
    {
        //log ( "database connected" );
        //remoteDBConnected = true;
    }

}

followersAndPageviewsDialog::~followersAndPageviewsDialog()
{
    delete ui;
}
