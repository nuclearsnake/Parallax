#ifndef SITEUPDATER_H
#define SITEUPDATER_H

#include <QDialog>
#include <QtSql>
#include "extensions/tools.h"

#include <QWebFrame>
#include <QWebHistory>
#include <QWebView>
#include <QStandardItemModel>

namespace Ui {
class siteUpdater;
}

class siteUpdater : public QDialog
{
    Q_OBJECT

public:
    explicit siteUpdater(QWidget *parent = 0);
    ~siteUpdater();
    int allItemCount,currentItem;
    QSqlQueryModel* model;
    QString currentUrl;
    //QWebView * webView;
    int statViews,statFavs,statComments;
    QString submitDate,origTitle,theUrl;
    QSqlDatabase db;
    tools *pG;
   // QString getPiece(QString content, QString beginStr,QString endStr);

    void setDb(QSqlDatabase database);
    int singleId;
    QString singleTitle;
    void setSingle(int id, QString title);
    bool gPlusLoggedin;
    QStandardItemModel *tableModel;
    int retries;
public slots:

    void loadUrl();
    void nextUrl();

    void go();
    void nextUrlProxy();
private:
    Ui::siteUpdater *ui;
    void log(QString text);
    void logHtml(QString text);
};

#endif // SITEUPDATER_H
