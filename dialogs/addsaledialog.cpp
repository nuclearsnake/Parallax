#include "addsaledialog.h"
#include "ui_addsaledialog.h"

addSaleDialog::addSaleDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addSaleDialog)
{
    ui->setupUi(this);

    QTimer::singleShot(100,this,SLOT(init()));

    connect( ui->pushButton_Cancel, SIGNAL(clicked()),  this,   SLOT(close()) );
    connect( ui->pushButton_Save,   SIGNAL(clicked()),  this,   SLOT(save()) );

}

void addSaleDialog::save()
{


    QString exl = "0";
    if (ui->comboBox_EXCLUSIVE->currentText()=="Yes") exl = "1";

    QString query=" INSERT INTO sales (archiveid,date,saletype,licensetype,amount,totalprice,currency,site,url,exclusive,transaction_id,buyer,contact) VALUES ("

            + ui->lineEdit_ID->text() + ","
            + "'" + ui->lineEdit_SALEDATE->text() + "',"
            + "'" + ui->comboBox_SALETYPE->currentText() +"',"
            + "'" + ui->comboBox_LICENSETYPE->currentText() +"',"
            + ui->lineEdit_AMOUNT->text().replace(",",".") + ","
            + ui->lineEdit_TOTALPRICE->text().replace(",",".") + ","
            + "'" + ui->lineEdit_CURRENCY->text() + "',"
            + "'" + ui->comboBox_SITE->currentText() + "',"
            + "'" + ui->lineEdit_URL->text() + "',"
            + exl + ","
            + "'" + ui->lineEdit_TRANSACTIONID->text() +"',"
            + "'" + ui->lineEdit_BUYER->text() + "',"
            + "'" + ui->lineEdit_CONTACT->text() + "'"
            ")";

    qDebug() << query;
    libMan->db.exec(query);

    this->accept();

}

void addSaleDialog::init()
{
    ui->lineEdit_ID->setText(QString::number(archiveID));
    ui->lineEdit_TITLE->setText(archiveTitle);

    ui->lineEdit_ID->setEnabled(false);
    ui->lineEdit_TITLE->setEnabled(false);
}

void addSaleDialog::attachLibMan(libraryManager *l)
{
    libMan = l;

}

addSaleDialog::~addSaleDialog()
{
    delete ui;
}
