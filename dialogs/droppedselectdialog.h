#ifndef DROPPEDSELECTDIALOG_H
#define DROPPEDSELECTDIALOG_H

#include <QDialog>

namespace Ui {
class droppedSelectDialog;
}

class droppedSelectDialog : public QDialog
{
    Q_OBJECT

public:
    explicit droppedSelectDialog(QWidget *parent = 0);
    ~droppedSelectDialog();

    void setFolder(QString folder);
    QString result;
private slots:
    void submit();
private:
    Ui::droppedSelectDialog *ui;
};

#endif // DROPPEDSELECTDIALOG_H
