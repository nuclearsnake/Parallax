#include "addarchivedialog.h"
#include "ui_addarchivedialog.h"
#include "calendardialog.h"

#include <QMessageBox>
#include <QFileDialog>
#include <QPixmap>
#include <QDate>
#include <QDebug>


addArchiveDialog::addArchiveDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addArchiveDialog)
{
    ui->setupUi(this);

    this->setWindowTitle("Add new archive");
    this->setWindowIcon(QIcon(":/icons/icons/archive.png"));

    connect ( ui->pushButton_cancel         , SIGNAL(clicked()) , this , SLOT(close()) );
    connect ( ui->pushButton_add            , SIGNAL(clicked()) , this , SLOT(submit()) );
    connect ( ui->pushButton_selectFolder   , SIGNAL(clicked()) , this , SLOT(selectFolder()) );
    connect ( ui->pushButton_selectImage    , SIGNAL(clicked()) , this , SLOT(selectImage()) );
    connect ( ui->pushButton_calendar       , SIGNAL(clicked()) , this , SLOT(calendar()) );
    connect ( ui->treeView                  , SIGNAL(doubleClicked(QModelIndex))    , this , SLOT(loadImageFromTree(QModelIndex))  );
}

void addArchiveDialog::loadImageFromTree(QModelIndex index)
{
    QString fileName = index.sibling(index.row(),0).data().toString();
    if ( fileName.endsWith(".jpg"))
        {
            ui->lineEdit_image->setText( dirName + "/" + fileName );
            QPixmap pixmap;
            pixmap.load(dirName + "/" + fileName);
            //int pixW=pixmap.width();
            //int pixH=pixmap.height();
            //float pixAr= (float) pixW / (float) pixH;
            int contW=ui->label_img->width();
            int contH=ui->label_img->height();
            ui->label_img->setPixmap(pixmap.scaled(contW,contH,Qt::KeepAspectRatio , Qt::SmoothTransformation));
        }
}

void addArchiveDialog::setTitleList(QList<QString> value)
{
    titleList=value;
    qDebug() << titleList;
}

void addArchiveDialog::setTitle(QString value)
{
    ui->lineEdit_title->setText(value);
    originalTitle = value;
    ui->pushButton_add->setText("Update");
}

void addArchiveDialog::setFolder(QString value)
{
    ui->lineEdit_folder->setText(value);
    dirName = value;
    if (dirName!="")
        {
            files = new QFileSystemModel(this);
            files->setRootPath(dirName);

            ui->treeView->setModel(files);
            ui->treeView->setRootIndex(files->index(dirName));
            ui->treeView->hideColumn(1);
            ui->treeView->hideColumn(2);
            ui->treeView->hideColumn(3);
        }
}

void addArchiveDialog::setImage(QString value)
{
    ui->lineEdit_image->setText(value);
}

void addArchiveDialog::setDate(QString value)
{
    //ui->lineEdit_image->setText(value);
    // value.split("-").at(0).toInt() , value.split("-").at(1).toInt() , value.split("-").at(2).toInt()

    if (value.split("-").count()>1)  ui->dateEdit->setDate(  QDate(value.split("-").at(0).toInt() , value.split("-").at(1).toInt() , value.split("-").at(2).toInt()) );
    if (value.split(".").count()>1)  ui->dateEdit->setDate(  QDate(value.split(".").at(0).toInt() , value.split(".").at(1).toInt() , value.split(".").at(2).toInt()) );


}

void addArchiveDialog::calendar()
{
    calendarDialog cDialog;

    if (cDialog.exec())
        {
            ui->dateEdit->setDate(cDialog.date);
        }
}

void addArchiveDialog::selectFolder()
{
     dirName = QFileDialog::getExistingDirectory(this,tr("Open Directory"));
     folderSelect(dirName);

}

void addArchiveDialog::folderSelect(QString d)
{
    dirName = d;

    if (dirName.endsWith("/")) dirName = dirName.mid(0,dirName.count()-1);

    ui->lineEdit_folder->setText(dirName);
    //if (ui->lineEdit_title->text()=="")
    QStringList split = dirName.split("/");

    //if (dirName.endsWith("/")) ui->lineEdit_title->setText(split[split.count()-2].replace("_"," "));
    //  else
    ui->lineEdit_title->setText(split.last().replace("_"," "));

    files = new QFileSystemModel(this);
    files->setRootPath(dirName);

    ui->treeView->setModel(files);
    ui->treeView->setRootIndex(files->index(dirName));
    ui->treeView->hideColumn(1);
    ui->treeView->hideColumn(2);
    ui->treeView->hideColumn(3);
}

void addArchiveDialog::selectImage()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),"",tr("Files (*.jpg)"));
    ui->lineEdit_image->setText(fileName);
    QPixmap pixmap;
    pixmap.load(fileName);
    //int pixW=pixmap.width();
    //int pixH=pixmap.height();
    //float pixAr= (float) pixW / (float) pixH;
    int contW=ui->label_img->width();
    int contH=ui->label_img->height();
    ui->label_img->setPixmap(pixmap.scaled(contW,contH,Qt::KeepAspectRatio , Qt::SmoothTransformation));
    // ,Qt::KeepAspectRatio , Qt::SmoothTransformation

}

void addArchiveDialog::submit()
{
    int err=0;

    if (ui->lineEdit_title->text()=="")
        {
            QMessageBox Msgbox;
            Msgbox.setText("No title!");
            Msgbox.exec();
            err++;
        }

    if ( titleList.contains(ui->lineEdit_title->text()) )
        if (originalTitle != ui->lineEdit_title->text() )
            {
                QMessageBox Msgbox;
                Msgbox.setText("Title: <b>'"+ui->lineEdit_title->text()+"'</b> already used");
                Msgbox.exec();
                err++;
            }

    if (ui->lineEdit_image->text()=="")
        {
            QMessageBox Msgbox;
            Msgbox.setText("No image!");
            Msgbox.exec();
            err++;
        }

    this->title = ui->lineEdit_title->text();
    this->image = ui->lineEdit_image->text();
    this->folder = ui->lineEdit_folder->text();
    this->keywords = ui->plainTextEdit_kewords->toPlainText();
    this->date.sprintf("%d-%02d-%02d",ui->dateEdit->date().year(),ui->dateEdit->date().month(),ui->dateEdit->date().day());

    if (err==0) accept();
}



addArchiveDialog::~addArchiveDialog()
{
    delete ui;
}
