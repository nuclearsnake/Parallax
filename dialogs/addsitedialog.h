#ifndef ADDSITEDIALOG_H
#define ADDSITEDIALOG_H

#include <QDialog>
#include <QDebug>
#include <QNetworkReply>

//#include <messagedialog.h>

namespace Ui {
class AddSiteDialog;
}

class AddSiteDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddSiteDialog(QWidget *parent = 0);
    //QString labelProjectTitle;
    void setTitle(QString title);
    void setId(int id);
    int projectId;
    int statViews,statFavs,statComments;
    int loginRequired;
    int loggedIn;
    QString submitDate,origTitle,theUrl,submitTime;
    QStringList months;
    //QString getPiece(QString content, QString beginStr, QString endStr);

    ~AddSiteDialog();

public slots:
    void checkValues();
    void getSource();
    void getData();

    void sslErrorHandler(QNetworkReply *qnr, const QList<QSslError> &errlist);
    void delayedGetSource();
private:
    Ui::AddSiteDialog *ui;

};

#endif // ADDSITEDIALOG_H
