#include "yesnodialog.h"
#include "ui_yesnodialog.h"

yesNoDialog::yesNoDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::yesNoDialog)
{
    ui->setupUi(this);
    ui->label_message->setText(message);
    connect( ui->pushButton_no  , SIGNAL(clicked()) , this , SLOT(close()) );
    connect( ui->pushButton_yes , SIGNAL(clicked()) , this  ,SLOT(accept()));
}

void yesNoDialog::setMessage(QString text)
{
    this->message=text;
    ui->label_message->setText(message);
}

void yesNoDialog::setDetail(QString text)
{
    //this->message=text;
    ui->label_messageDetail->setText(text);
}

yesNoDialog::~yesNoDialog()
{
    delete ui;
}
