#ifndef ORGANIZERDIALOG_H
#define ORGANIZERDIALOG_H

#include <QDialog>
#include <QList>
#include <QListView>
#include <QString>
#include <QFileSystemModel>
#include <QStandardItemModel>


namespace Ui {
class organizerDialog;
}

class organizerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit organizerDialog(QWidget *parent = 0);
    ~organizerDialog();

    QString workingFolder;
    //QFileSystemModel *fileModel;

    void setFolder(QString f);
    void init();
    //void dropEvent(QDropEvent *event);
protected:
   // void dropEvent(QDropEvent *event);
   // void dragEnterEvent(QDragEnterEvent *event);
signals:
    void updateMove(QString,QString,int,int);
private slots:
    //void test();
    //void test1(QModelIndex ia, QModelIndex ib);
    //void test2(QStandardItem *item);
   // void itemMover(QModelIndex a, QModelIndex b);
    void itemMover(QStandardItemModel *modelTO);

    void md00Dropped(QModelIndex a, QModelIndex b);
    void md01Dropped(QModelIndex a, QModelIndex b);
    void md02Dropped(QModelIndex a, QModelIndex b);
    void md03Dropped(QModelIndex a, QModelIndex b);
    void md04Dropped(QModelIndex a, QModelIndex b);
    void md05Dropped(QModelIndex a, QModelIndex b);
    void md06Dropped(QModelIndex a, QModelIndex b);
    void md07Dropped(QModelIndex a, QModelIndex b);
    void md08Dropped(QModelIndex a, QModelIndex b);
    void md09Dropped(QModelIndex a, QModelIndex b);
    void autoOrg();
    void moveFiles();
private:
    Ui::organizerDialog *ui;
    //QStandardItemModel *originalFilesModel;
    //QStandardItemModel *testModel;
    QStandardItemModel *md00Source,*md01Origi,*md02Pano,*md03Hdr , *md04Tmp , *md05Post , *md06Res , *md07Final , *md08Print , *md09Wall , *md10Misc ;
    QList<QListView*> viewList;
    QList<QStandardItemModel*> modelList;
};

#endif // ORGANIZERDIALOG_H
