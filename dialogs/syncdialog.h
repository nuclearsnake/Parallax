#ifndef SYNCDIALOG_H
#define SYNCDIALOG_H

#include <QDialog>
#include "managers/librarymanager.h"

namespace Ui {
class syncDialog;
}

class syncDialog : public QDialog
{
    Q_OBJECT

public:
    explicit syncDialog(QWidget *parent = 0);
    ~syncDialog();
    libraryManager *libMan;
    QSqlDatabase mysqlDB;
    bool remoteDBConnected;
    void attachLibMan(libraryManager *l);
    void connectRemoteDB();
public slots:
    void go();
private:
    Ui::syncDialog *ui;
    void log(QString s);
};

#endif // SYNCDIALOG_H
