#ifndef UPLOADTIMESDIALOG_H
#define UPLOADTIMESDIALOG_H

#include <QDialog>
#include <managers/librarymanager.h>

namespace Ui {
class uploadTimesDialog;
}

class uploadTimesDialog : public QDialog
{
    Q_OBJECT

public:
    explicit uploadTimesDialog(QWidget *parent = 0);
    ~uploadTimesDialog();
    libraryManager *libMan;

    void attachLibman(libraryManager *l);
    void log(QString s);
    //QSqlQueryModel model;
    QString calColor(int v);
    int maxValue;
    int minValue;
    void clearMatrix();
    void connectLabels();
public slots:
    void go();
    void labelClick();
private:
    Ui::uploadTimesDialog *ui;
};

#endif // UPLOADTIMESDIALOG_H
