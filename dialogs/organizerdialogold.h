#ifndef ORGANIZERDIALOGOLD_H
#define ORGANIZERDIALOGOLD_H

#include <QDialog>

namespace Ui {
class organizerDialogOld;
}

class organizerDialogOld : public QDialog
{
    Q_OBJECT

public:
    explicit organizerDialogOld(QWidget *parent = 0);
    ~organizerDialogOld();

private:
    Ui::organizerDialogOld *ui;
};

#endif // ORGANIZERDIALOGOLD_H
