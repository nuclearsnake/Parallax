#include "addprocessdialog.h"
#include "ui_addprocessdialog.h"
#include "QMessageBox"
#include "QFileDialog"

addProcessDialog::addProcessDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addProcessDialog)
{
    ui->setupUi(this);

    this->setWindowTitle("Add new process");
    this->setWindowIcon(QIcon(":/icons/icons/rotator_1.png"));

    connect ( ui->pushButton_cancel         , SIGNAL(clicked()) , this , SLOT(close()) );
    connect ( ui->pushButton_add            , SIGNAL(clicked()) , this , SLOT(submit()) );
    connect ( ui->pushButton_folderSelect   , SIGNAL(clicked()) , this , SLOT(selectFolder()) );
}

void addProcessDialog::editMode(QString folder,QString title, int isPano)
{
    ui->lineEdit_path->setText(folder);
    ui->lineEdit_path->setEnabled(false);
    ui->lineEdit_name->setText(title);
    ui->checkBox_panoMode->setChecked(isPano==1);
    ui->pushButton_add->setText("Save");
    ui->pushButton_folderSelect->setEnabled(false);
    setWindowTitle("Process properties");
}

void addProcessDialog::preSelect(QString f)
{
    ui->lineEdit_path->setText(f);
     ui->lineEdit_name->setText(f.split("/").last());
}

void addProcessDialog::selectFolder()
{
    // folder select dialog
    QString dirName = QFileDialog::getExistingDirectory(0,tr("Open Directory"));
    // insert data to lineedits
    ui->lineEdit_path->setText(dirName);
    ui->lineEdit_name->setText(dirName.split("/").last());
}

void addProcessDialog::submit()
{
    int err=0;

    // check path
    if (ui->lineEdit_path->text() == "")
        {
            QMessageBox Msgbox;
            Msgbox.setText("No folder selected!");
            Msgbox.exec();
            err++;
        }

    // check name
    if (ui->lineEdit_name->text() == "")
        {
            QMessageBox Msgbox;
            Msgbox.setText("Please specify a name for the project!");
            Msgbox.exec();
            err++;
        }

    // set dialog return vars
    this->name = ui->lineEdit_name->text();
    this->path = ui->lineEdit_path->text();
    this->panoMode = 0;
    if (ui->checkBox_panoMode->isChecked()) this->panoMode = 1;

    // submit only id no error
    if (err==0) this->accept();
}

addProcessDialog::~addProcessDialog()
{
    delete ui;
}
