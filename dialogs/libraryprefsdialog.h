#ifndef LIBRARYPREFSDIALOG_H
#define LIBRARYPREFSDIALOG_H

#include <QDialog>
#include "managers/librarymanager.h"
#include "QSqlQueryModel"

namespace Ui {
class libraryPrefsDialog;
}

class libraryPrefsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit libraryPrefsDialog(QWidget *parent = 0);
    ~libraryPrefsDialog();
    libraryManager *libMan;
    QSqlQueryModel *model;
    float defaultTopScore;
    float scoreBias;
    void attachLibMan(libraryManager *l);
    void init();
private slots:
    void updateBias(int i);
    void save();
private:
    Ui::libraryPrefsDialog *ui;
};

#endif // LIBRARYPREFSDIALOG_H
