#include "libraryprefsdialog.h"
#include "ui_libraryprefsdialog.h"

libraryPrefsDialog::libraryPrefsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::libraryPrefsDialog)
{
    ui->setupUi(this);

    connect ( ui->horizontalSlider_scoreBias    , SIGNAL(valueChanged(int))     , this  , SLOT(updateBias(int))  );
    connect ( ui->pushButton_cancel             , SIGNAL(clicked())             , this  , SLOT(close()) );
    connect ( ui->pushButton_save               , SIGNAL(clicked())             , this  , SLOT(save()) );

}

void libraryPrefsDialog::attachLibMan(libraryManager *l)
{
    libMan = l;
}

void libraryPrefsDialog::init()
{
    ui->horizontalSlider_scoreBias->setValue( libMan->scoreBias*100);
    model = new QSqlQueryModel(this);
    QString query=libMan->loadQuery("archivetree_byscore.sql");
    query.replace("[P2]","1");
    model->setQuery(query,libMan->db);
    defaultTopScore = model->index(0,7).data().toFloat();
    ui->label_topScore->setText( QString::number(  defaultTopScore * libMan->scoreBias ) ) ;
}

void libraryPrefsDialog::updateBias(int i)
{
    scoreBias = (i+0.0)/100;
    QString scoreBiasStr;
    scoreBiasStr.sprintf("%1.2f",scoreBias);
    ui->label_scoreBias->setText(scoreBiasStr);
    ui->label_topScore->setText( QString::number(  defaultTopScore * scoreBias ) ) ;
}

void libraryPrefsDialog::save()
{
    QString scoreBiasStr;
    scoreBiasStr.sprintf("%1.2f",scoreBias);
    QString query = "insert or replace into var (id, key, value) values  ((select id from var where key = 'scoreBias'), 'scoreBias', '"+ scoreBiasStr +"');";
    libMan->db.exec(query);
    accept();
}

libraryPrefsDialog::~libraryPrefsDialog()
{
    delete ui;
}
