#include "mainwindow.h"
#include "ui_mainwindow.h"



//void MainWindow::connectionsForArchive()
//{
    // ARCHIVE TREE & LIST
    //connect (   ui->listView_archiveTree         , SIGNAL(clicked(QModelIndex))  , this  , SLOT(selectArchiveItem(QModelIndex)) );
    //connect ( ui->treeView_archiveTree           , SIGNAL(clicked(QModelIndex))  , archive  , SLOT(select(QModelIndex)) );
    //connect ( ui->pushButton_archiveTreeExpand   , SIGNAL(clicked(bool))         , this  , SLOT(archiveTreeExpandCollapse(bool)) );
    //connect ( ui->pushButton_archiveGroupByAz    , SIGNAL(clicked())             , this , SLOT(archiveGroupByAZ()) );
    //connect ( ui->pushButton_archiveGroupByScore , SIGNAL(clicked())             , this , SLOT(archiveGroupByScore()) );
    //connect ( ui->pushButton_archiveGroupByDate  , SIGNAL(clicked())             , this , SLOT(archiveGroupByDate()) );
    //connect ( ui->pushButton_archiveGroupByLoc   , SIGNAL(clicked())             , this , SLOT(archiveGroupByLoc())  );
    //connect ( ui->pushButton_archiveTreeGroup    , SIGNAL(clicked())             , this , SLOT(archiveTreeSwitchGrouped()) );

    // SEARCH
    //connect ( ui->pushButton_archiveSearchByTitle    , SIGNAL(clicked()) , this , SLOT(archiveFilterTypeT()) );
    //connect ( ui->pushButton_archiveSearchByKeywords , SIGNAL(clicked()) , this , SLOT(archiveFilterTypeK()) );

    // ARCHIVE BROWSER
    //connect ( ui->listView_archiveBrowser  , SIGNAL(clicked(QModelIndex))       , archive , SLOT(browserSelect(QModelIndex)) );
    //connect ( ui->listView_archiveBrowser  , SIGNAL(doubleClicked(QModelIndex)) , archive , SLOT(browserDoubleClick(QModelIndex)));
    //connect ( ui->horizontalSlider_archiveThumbSize , SIGNAL(valueChanged(int)) , this , SLOT(archiveBrowserSetThumbSize(int)) );
    //connect ( ui->listView_archiveBrowser           , SIGNAL(customContextMenuRequested(QPoint)) , this , SLOT(archiveBrowserContextMenu(QPoint)) );
    /*
    //  FILTER
    //connect (   ui->lineEdit_archiveFilter  , SIGNAL(textChanged(QString))  , this  , SLOT(archiveFilter(QString))   );
    connect (   ui->pushButton_searchClear  , SIGNAL(clicked())             , this  , SLOT(archiveFilterClear())     );
    connect ( ui->checkBox_aF_notin         , SIGNAL(toggled(bool))         , this  , SLOT(archiveAdvFilterNotin_toggle(bool)) );
    connect ( ui->label_aF_niDA             , SIGNAL(Mouse_Pressed())       , this  , SLOT(archiveAdvFilterNotin()) );
    connect ( ui->label_aF_ni5P             , SIGNAL(Mouse_Pressed())       , this  , SLOT(archiveAdvFilterNotin()) );
    connect ( ui->label_aF_niGP             , SIGNAL(Mouse_Pressed())       , this  , SLOT(archiveAdvFilterNotin()) );
    connect ( ui->label_aF_niFL             , SIGNAL(Mouse_Pressed())       , this  , SLOT(archiveAdvFilterNotin()) );
    connect ( ui->label_aF_niFB             , SIGNAL(Mouse_Pressed())       , this  , SLOT(archiveAdvFilterNotin()) );

    // KEYWORDS
    connect (   ui->plainTextEdit_archiveKeywords    , SIGNAL(textChanged()) , this  , SLOT(archiveEnableKeywordUpdateButton()) );
    connect (   ui->pushButton_archiveUpdateKeywords , SIGNAL(clicked())     , this  , SLOT(archiveUpdateKeywords()) );
    connect (   ui->pushButton_archiveCopyKeySpace   , SIGNAL(clicked())     , this  , SLOT(archiveKeywordsToClipboradSpaces())   );
    connect (   ui->pushButton_archiveCopyKeyComma   , SIGNAL(clicked())     , this  , SLOT(archiveKeywordsToClipboradCommas())    );
    connect (   ui->pushButton_quickTag              , SIGNAL(clicked())     , this  , SLOT(quickTag()) );
    connect (   ui->listView_quickTag                , SIGNAL(clicked(QModelIndex))  , this  , SLOT(quickTagClick(QModelIndex))  );
    connect (   ui->lineEdit_addKeyword              , SIGNAL(returnPressed())       , this  , SLOT(keywordAdd()) );

    // NOTES
    connect (   ui->plainTextEdit_notes           , SIGNAL(textChanged())  , this  , SLOT(archiveEnableNoteUpdateButton())  );
    connect (   ui->pushButton_archiveUpdateNotes , SIGNAL(clicked())      , this  , SLOT(archiveUpdateNotes())  );

    // FILES & FOLDERS
    connect ( ui->pushButton_toggleFolderNavi      , SIGNAL(clicked())   , this  , SLOT(archiveFolderNaviToggle())  );
    connect ( ui->pushButton_toggleFolderNavi2     , SIGNAL(clicked())   , this  , SLOT(archiveFolderNaviToggle())   );
    connect ( ui->pushButton_archiveOpenFolder     , SIGNAL(clicked())   , this  , SLOT(archiveOpenFolderInExplorer()) );
    connect ( ui->pushButton_archiveOrganizeFolder , SIGNAL(clicked())   , this  , SLOT(archiveOrganizer()) );
    connect ( ui->pushButton_fileListToRoot        , SIGNAL(clicked())   , this  , SLOT(archiveFileListToRoot()) );
    connect ( ui->listView_archiveFiles            , SIGNAL(doubleClicked(QModelIndex))         , archive   , SLOT(fileDoubleClick(QModelIndex))   );
    connect ( ui->treeView_archiveFiles            , SIGNAL(clicked(QModelIndex))               , this      , SLOT(archiveLoadImageFromTree(QModelIndex)) );
    connect ( ui->listView_archiveFiles            , SIGNAL(customContextMenuRequested(QPoint)) , this      , SLOT(archiveFilesContextMenu(QPoint)) );


    // TOOLS
    connect ( ui->pushButton_archiveEditData       , SIGNAL(clicked())   , this  , SLOT(archiveEdit()) );
    connect ( ui->pushButton_addNewSite            , SIGNAL(clicked())   , this  , SLOT(archiveAddSite()) );
    connect ( ui->pushButton_archiveCompressor     , SIGNAL(clicked())   , this  , SLOT(archiveCompressor()) );

    // NAVIGATION
    connect ( ui->pushButton_naviPrev              , SIGNAL(clicked())   , this  , SLOT(shortCutKeyLeft()) );
    connect ( ui->pushButton_naviNext              , SIGNAL(clicked())   , this  , SLOT(shortCutKeyRight()) );

    // SITES
    //connect ( ui->label_siteDaIcon                 , SIGNAL(Mouse_Pressed())  , this  , SLOT(siteClickDa()));
    //connect ( ui->label_site500pxIcon              , SIGNAL(Mouse_Pressed())  , this  , SLOT(siteClick500px()) );
    //connect ( ui->label_siteFlickrIcon             , SIGNAL(Mouse_Pressed())  , this  , SLOT(siteClickFlickr()) );
    connect ( ui->pushButton_sitesManage           , SIGNAL(clicked())        , this  , SLOT(archiveSitesManage()) );

    // WEB
    connect ( ui->pushButton_naviNext_2   , SIGNAL(clicked())          , archive , SLOT(webForward())       );
    connect ( ui->pushButton_naviPrev_2   , SIGNAL(clicked())          , archive , SLOT(webBack())          );
    connect ( ui->webView                 , SIGNAL(loadProgress(int))  , archive , SLOT(webProgress(int))   );
    connect ( ui->webView                 , SIGNAL(urlChanged(QUrl))   , archive , SLOT(webUrlUpdate(QUrl)) );

    // TABS
    connect ( ui->tabWidget_archiveCenter          , SIGNAL(tabCloseRequested(int))  , this , SLOT(archiveTabClose(int))  );

    // MISC SIDEBAR
    connect ( ui->label_sideBarPreview             , SIGNAL(Mouse_Pressed())   , this  , SLOT(archiveFocusLists()) );

    // MAP
    //connect ( ui->pushButton_archiveFSToggle       , SIGNAL(clicked())   , this  , SLOT(archiveFilmstripToggle()) );
    connect ( ui->pushButton_locSave               , SIGNAL(clicked())   , this  , SLOT(archiveSaveLocation())  );
    connect ( ui->pushButton_mapRefresh            , SIGNAL(clicked())   , archive  , SLOT(fullMapRefresh()) );

    // COLLECTIONS
    //connect ( ui->pushButton_addCollection         , SIGNAL(clicked())   , this  , SLOT(archiveAddCollection())  );
    //connect ( ui->listView_collections             , SIGNAL(clicked(QModelIndex))  , this  , SLOT(archiveCollectionSelect(QModelIndex)) );
    //connect ( ui->listView_collections             , SIGNAL(customContextMenuRequested(QPoint)) , this , SLOT(archiveCollectionsContextMenu(QPoint))  );
    */
//}
