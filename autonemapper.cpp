#include "autonemapper.h"
#include "ui_autonemapper.h"

#include <QDebug>
#include <QTimer>
#include <QDir>
#include <QProcess>

auToneMapper::auToneMapper(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::auToneMapper)
{
    ui->setupUi(this);

    QTimer::singleShot(100,this,SLOT(init()));



}

void auToneMapper::init()
{
    qDebug() << "project" << projectPath;
    qDebug() << "library" << libraryPath;

    QDir *pdir = new QDir;
    pdir->setPath(projectPath);

    for ( int i = 0;i<pdir->entryList().count();i++ )
        {
            QString s= pdir->entryList().at(i);
            if (s.endsWith(".hdr"))
                {
                    ui->comboBox_hdrSelect->addItem(s);
                }
        }

    QDir *tdir = new QDir;
    tdir->setPath(libraryPath+"TonemapPresets");

    for ( int i = 0;i<tdir->entryList().count();i++ )
        {
            QString s= tdir->entryList().at(i);
            if ( ( tdir->entryInfoList().at(i).isDir() ) & ( s!="." ) & ( s!=".." ) )
                {
                    ui->comboBox_toneMapGroup->addItem(s);
                }
        }

    model = new QStandardItemModel(this);
    ui->tableView_xmp->setModel(model);
    ui->tableView_xmp->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tableView_xmp->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableView_xmp->setColumnWidth(0,15);
    ui->tableView_xmp->setColumnWidth(1,150);
    //ui->tableView_filesSelect->setColumnWidth(1,450);
    ui->tableView_xmp->horizontalHeader()->setStretchLastSection(true);

    this->getTemplates(ui->comboBox_toneMapGroup->currentText());

    connect ( ui->comboBox_toneMapGroup , SIGNAL(currentIndexChanged(QString))  , this  , SLOT(getTemplates(QString)) );
    connect ( ui->pushButton_go         , SIGNAL(clicked())                     , this  , SLOT(go()) );

}

void auToneMapper::getTemplates(QString branch)
{
    model->clear();

    QDir *tsdir = new QDir;
    tsdir->setFilter(QDir::NoDotAndDotDot | QDir::Files );
    tsdir->setSorting(QDir::Name );
    //qDebug() << libraryPath+"TonemapPresets/"+ui->comboBox_toneMapGroup->currentText();
    tsdir->setPath(libraryPath+"TonemapPresets/"+ branch);

    for ( int i = 0;i<tsdir->entryList().count();i++ )
        {
            QString s= tsdir->entryList().at(i);
            if (s.endsWith(".xmp"))
                {
                    qDebug() << s;

                    QList<QStandardItem*> items;
                    QStandardItem *chk = new QStandardItem(true);
                        chk->setCheckable(true);
                        //if (checkState) chk->setCheckState(Qt::Checked); else
                        chk->setCheckState(Qt::Checked );
                        chk->setData(Qt::AlignCenter,Qt::AlignVCenter);
                    items << chk;
                    items << new QStandardItem( s );
                    QString p = libraryPath+"TonemapPresets/"+ branch + "/"+s;
                    p.replace("/","\\");
                    items << new QStandardItem( p );

                    model->appendRow(items);
                }

        }
}

void auToneMapper::go()
{

    for (int i=0;i<model->rowCount();++i)
        {
            // C:\"Program Files"\PhotomatixPro5\photomatixcl.exe -x d:\tst\_tonemaptst_\xmpa.xmp -d d:\tst\_tonemaptst_\ -bi 8 -co 0
            // -o 001_tonemapped_xmpa  d:\tst\_tonemaptst_\001.hdr
            QString command = "C:\\\"Program Files\"\\PhotomatixPro5\\photomatixcl.exe";
            //QString command = "C:/Program Files/PhotomatixPro5/photomatixcl.exe";


            command += " -x ";
            command += model->index(i,2).data().toString();
            command += " -d ";
            command += projectPath.replace("/","\\");
            command += " -bi 8 ";
            command += " -co 0 ";
            command += " -o ";
            command += ui->comboBox_hdrSelect->currentText().replace(".hdr","")
                    + "_tonemapped_"
                    +  model->index(i,1).data().toString().replace(".xmp","");
            command += " ";
            command += projectPath.replace("/","\\") + ui->comboBox_hdrSelect->currentText();

            qDebug() << command;
            commandList << command;
        }


    /*
    QProcess *proc = new QProcess;

    connect ( proc  , SIGNAL( finished(int) ) , this  , SLOT(test())  );

    proc->start(commandList.at(0));
    */

    allItems = commandList.count();
    currentItem = 0;
    runProc(commandList.at(0));


    /*
    QStringList arg;
    QProcess *proc = new QProcess;
    qDebug() << "RUN" << commandList.at(0);
    proc->start(  "C:/Program Files/PhotomatixPro5/photomatixcl.exe"  );
    */

}

void auToneMapper::runProxy()
{
    runProc(commandList.at(currentItem) );
}

void auToneMapper::runProc(QString command)
{
    QProcess *proc = new QProcess;
    currentItem++;
    if (currentItem < allItems) connect ( proc  , SIGNAL( finished(int) ) , this  , SLOT( runProxy())  );
    proc->start(command);
}

void auToneMapper::test()
{
    qDebug() << " TEEEEST ";
}

void auToneMapper::setProjectPath(QString path)
{
    this->projectPath = path;
}

auToneMapper::~auToneMapper()
{
    delete ui;
}
