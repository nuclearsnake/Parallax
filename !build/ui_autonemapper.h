/********************************************************************************
** Form generated from reading UI file 'autonemapper.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_AUTONEMAPPER_H
#define UI_AUTONEMAPPER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_auToneMapper
{
public:
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QLabel *label_2;
    QLabel *label;
    QComboBox *comboBox_hdrSelect;
    QComboBox *comboBox_toneMapGroup;
    QTableView *tableView_xmp;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_go;
    QPushButton *pushButton_cancel;

    void setupUi(QDialog *auToneMapper)
    {
        if (auToneMapper->objectName().isEmpty())
            auToneMapper->setObjectName(QStringLiteral("auToneMapper"));
        auToneMapper->resize(685, 441);
        verticalLayout = new QVBoxLayout(auToneMapper);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label_2 = new QLabel(auToneMapper);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        label = new QLabel(auToneMapper);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        comboBox_hdrSelect = new QComboBox(auToneMapper);
        comboBox_hdrSelect->setObjectName(QStringLiteral("comboBox_hdrSelect"));

        gridLayout->addWidget(comboBox_hdrSelect, 0, 1, 1, 1);

        comboBox_toneMapGroup = new QComboBox(auToneMapper);
        comboBox_toneMapGroup->setObjectName(QStringLiteral("comboBox_toneMapGroup"));

        gridLayout->addWidget(comboBox_toneMapGroup, 1, 1, 1, 1);

        gridLayout->setColumnStretch(1, 1);

        verticalLayout->addLayout(gridLayout);

        tableView_xmp = new QTableView(auToneMapper);
        tableView_xmp->setObjectName(QStringLiteral("tableView_xmp"));

        verticalLayout->addWidget(tableView_xmp);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton_go = new QPushButton(auToneMapper);
        pushButton_go->setObjectName(QStringLiteral("pushButton_go"));
        pushButton_go->setMinimumSize(QSize(100, 25));

        horizontalLayout->addWidget(pushButton_go);

        pushButton_cancel = new QPushButton(auToneMapper);
        pushButton_cancel->setObjectName(QStringLiteral("pushButton_cancel"));
        pushButton_cancel->setMinimumSize(QSize(100, 25));
        pushButton_cancel->setMaximumSize(QSize(100, 25));

        horizontalLayout->addWidget(pushButton_cancel);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(auToneMapper);

        QMetaObject::connectSlotsByName(auToneMapper);
    } // setupUi

    void retranslateUi(QDialog *auToneMapper)
    {
        auToneMapper->setWindowTitle(QApplication::translate("auToneMapper", "Dialog", 0));
        label_2->setText(QApplication::translate("auToneMapper", "Templates", 0));
        label->setText(QApplication::translate("auToneMapper", "Source", 0));
        pushButton_go->setText(QApplication::translate("auToneMapper", "Go", 0));
        pushButton_cancel->setText(QApplication::translate("auToneMapper", "Cancel", 0));
    } // retranslateUi

};

namespace Ui {
    class auToneMapper: public Ui_auToneMapper {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_AUTONEMAPPER_H
