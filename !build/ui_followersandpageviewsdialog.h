/********************************************************************************
** Form generated from reading UI file 'followersandpageviewsdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FOLLOWERSANDPAGEVIEWSDIALOG_H
#define UI_FOLLOWERSANDPAGEVIEWSDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>
#include <widgets/linegraphwidget.h>
#include "extensions/clickable_qlabel.h"

QT_BEGIN_NAMESPACE

class Ui_followersAndPageviewsDialog
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    clickable_QLabel *label_all;
    clickable_QLabel *label_da;
    clickable_QLabel *label_5p;
    clickable_QLabel *label_px;
    clickable_QLabel *label_yp;
    clickable_QLabel *label_fl;
    clickable_QLabel *label_fb;
    clickable_QLabel *label_rd;
    QSpacerItem *horizontalSpacer;
    QHBoxLayout *horizontalLayout_2;
    lineGraphWidget *widget;
    QTableView *tableView;

    void setupUi(QDialog *followersAndPageviewsDialog)
    {
        if (followersAndPageviewsDialog->objectName().isEmpty())
            followersAndPageviewsDialog->setObjectName(QStringLiteral("followersAndPageviewsDialog"));
        followersAndPageviewsDialog->resize(751, 389);
        verticalLayout = new QVBoxLayout(followersAndPageviewsDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(20);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label_all = new clickable_QLabel(followersAndPageviewsDialog);
        label_all->setObjectName(QStringLiteral("label_all"));
        label_all->setMinimumSize(QSize(35, 0));
        QFont font;
        font.setPointSize(10);
        font.setBold(true);
        font.setUnderline(true);
        font.setWeight(75);
        label_all->setFont(font);
        label_all->setCursor(QCursor(Qt::PointingHandCursor));

        horizontalLayout->addWidget(label_all);

        label_da = new clickable_QLabel(followersAndPageviewsDialog);
        label_da->setObjectName(QStringLiteral("label_da"));
        label_da->setMinimumSize(QSize(35, 0));
        QFont font1;
        font1.setPointSize(10);
        font1.setUnderline(false);
        label_da->setFont(font1);
        label_da->setCursor(QCursor(Qt::PointingHandCursor));

        horizontalLayout->addWidget(label_da);

        label_5p = new clickable_QLabel(followersAndPageviewsDialog);
        label_5p->setObjectName(QStringLiteral("label_5p"));
        label_5p->setMinimumSize(QSize(35, 0));
        label_5p->setFont(font1);
        label_5p->setCursor(QCursor(Qt::PointingHandCursor));

        horizontalLayout->addWidget(label_5p);

        label_px = new clickable_QLabel(followersAndPageviewsDialog);
        label_px->setObjectName(QStringLiteral("label_px"));
        label_px->setMinimumSize(QSize(35, 0));
        label_px->setFont(font1);
        label_px->setCursor(QCursor(Qt::PointingHandCursor));

        horizontalLayout->addWidget(label_px);

        label_yp = new clickable_QLabel(followersAndPageviewsDialog);
        label_yp->setObjectName(QStringLiteral("label_yp"));
        label_yp->setMinimumSize(QSize(35, 0));
        label_yp->setFont(font1);
        label_yp->setCursor(QCursor(Qt::PointingHandCursor));

        horizontalLayout->addWidget(label_yp);

        label_fl = new clickable_QLabel(followersAndPageviewsDialog);
        label_fl->setObjectName(QStringLiteral("label_fl"));
        label_fl->setMinimumSize(QSize(35, 0));
        label_fl->setFont(font1);
        label_fl->setCursor(QCursor(Qt::PointingHandCursor));

        horizontalLayout->addWidget(label_fl);

        label_fb = new clickable_QLabel(followersAndPageviewsDialog);
        label_fb->setObjectName(QStringLiteral("label_fb"));
        label_fb->setMinimumSize(QSize(35, 0));
        label_fb->setFont(font1);
        label_fb->setCursor(QCursor(Qt::PointingHandCursor));

        horizontalLayout->addWidget(label_fb);

        label_rd = new clickable_QLabel(followersAndPageviewsDialog);
        label_rd->setObjectName(QStringLiteral("label_rd"));
        label_rd->setMinimumSize(QSize(35, 0));
        label_rd->setFont(font1);
        label_rd->setCursor(QCursor(Qt::PointingHandCursor));

        horizontalLayout->addWidget(label_rd);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        widget = new lineGraphWidget(followersAndPageviewsDialog);
        widget->setObjectName(QStringLiteral("widget"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(widget->sizePolicy().hasHeightForWidth());
        widget->setSizePolicy(sizePolicy);
        widget->setMinimumSize(QSize(0, 0));

        horizontalLayout_2->addWidget(widget);

        tableView = new QTableView(followersAndPageviewsDialog);
        tableView->setObjectName(QStringLiteral("tableView"));
        tableView->setMinimumSize(QSize(312, 0));
        tableView->setMaximumSize(QSize(312, 16777215));
        QFont font2;
        font2.setPointSize(7);
        tableView->setFont(font2);
        tableView->verticalHeader()->setVisible(false);
        tableView->verticalHeader()->setDefaultSectionSize(12);
        tableView->verticalHeader()->setMinimumSectionSize(12);

        horizontalLayout_2->addWidget(tableView);


        verticalLayout->addLayout(horizontalLayout_2);


        retranslateUi(followersAndPageviewsDialog);

        QMetaObject::connectSlotsByName(followersAndPageviewsDialog);
    } // setupUi

    void retranslateUi(QDialog *followersAndPageviewsDialog)
    {
        followersAndPageviewsDialog->setWindowTitle(QApplication::translate("followersAndPageviewsDialog", "Dialog", 0));
        label_all->setText(QApplication::translate("followersAndPageviewsDialog", "ALL", 0));
        label_da->setText(QApplication::translate("followersAndPageviewsDialog", "DA", 0));
        label_5p->setText(QApplication::translate("followersAndPageviewsDialog", "5P", 0));
        label_px->setText(QApplication::translate("followersAndPageviewsDialog", "PX", 0));
        label_yp->setText(QApplication::translate("followersAndPageviewsDialog", "YP", 0));
        label_fl->setText(QApplication::translate("followersAndPageviewsDialog", "FL", 0));
        label_fb->setText(QApplication::translate("followersAndPageviewsDialog", "FB", 0));
        label_rd->setText(QApplication::translate("followersAndPageviewsDialog", "RD", 0));
    } // retranslateUi

};

namespace Ui {
    class followersAndPageviewsDialog: public Ui_followersAndPageviewsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FOLLOWERSANDPAGEVIEWSDIALOG_H
