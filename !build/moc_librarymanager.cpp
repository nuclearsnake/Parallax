/****************************************************************************
** Meta object code from reading C++ file 'librarymanager.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../managers/librarymanager.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'librarymanager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_libraryManager_t {
    QByteArrayData data[6];
    char stringdata[50];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_libraryManager_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_libraryManager_t qt_meta_stringdata_libraryManager = {
    {
QT_MOC_LITERAL(0, 0, 14), // "libraryManager"
QT_MOC_LITERAL(1, 15, 13), // "libraryLoaded"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 4), // "open"
QT_MOC_LITERAL(4, 35, 9), // "createNew"
QT_MOC_LITERAL(5, 45, 4) // "load"

    },
    "libraryManager\0libraryLoaded\0\0open\0"
    "createNew\0load"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_libraryManager[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   34,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    0,   35,    2, 0x0a /* Public */,
       4,    0,   36,    2, 0x0a /* Public */,
       5,    0,   37,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void libraryManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        libraryManager *_t = static_cast<libraryManager *>(_o);
        switch (_id) {
        case 0: _t->libraryLoaded(); break;
        case 1: _t->open(); break;
        case 2: _t->createNew(); break;
        case 3: _t->load(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (libraryManager::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&libraryManager::libraryLoaded)) {
                *result = 0;
            }
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject libraryManager::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_libraryManager.data,
      qt_meta_data_libraryManager,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *libraryManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *libraryManager::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_libraryManager.stringdata))
        return static_cast<void*>(const_cast< libraryManager*>(this));
    return QObject::qt_metacast(_clname);
}

int libraryManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void libraryManager::libraryLoaded()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
