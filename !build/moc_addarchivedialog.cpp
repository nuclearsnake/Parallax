/****************************************************************************
** Meta object code from reading C++ file 'addarchivedialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../dialogs/addarchivedialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'addarchivedialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_addArchiveDialog_t {
    QByteArrayData data[8];
    char stringdata[83];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_addArchiveDialog_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_addArchiveDialog_t qt_meta_stringdata_addArchiveDialog = {
    {
QT_MOC_LITERAL(0, 0, 16), // "addArchiveDialog"
QT_MOC_LITERAL(1, 17, 6), // "submit"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 12), // "selectFolder"
QT_MOC_LITERAL(4, 38, 11), // "selectImage"
QT_MOC_LITERAL(5, 50, 8), // "calendar"
QT_MOC_LITERAL(6, 59, 17), // "loadImageFromTree"
QT_MOC_LITERAL(7, 77, 5) // "index"

    },
    "addArchiveDialog\0submit\0\0selectFolder\0"
    "selectImage\0calendar\0loadImageFromTree\0"
    "index"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_addArchiveDialog[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   39,    2, 0x08 /* Private */,
       3,    0,   40,    2, 0x08 /* Private */,
       4,    0,   41,    2, 0x08 /* Private */,
       5,    0,   42,    2, 0x08 /* Private */,
       6,    1,   43,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,    7,

       0        // eod
};

void addArchiveDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        addArchiveDialog *_t = static_cast<addArchiveDialog *>(_o);
        switch (_id) {
        case 0: _t->submit(); break;
        case 1: _t->selectFolder(); break;
        case 2: _t->selectImage(); break;
        case 3: _t->calendar(); break;
        case 4: _t->loadImageFromTree((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject addArchiveDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_addArchiveDialog.data,
      qt_meta_data_addArchiveDialog,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *addArchiveDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *addArchiveDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_addArchiveDialog.stringdata))
        return static_cast<void*>(const_cast< addArchiveDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int addArchiveDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
