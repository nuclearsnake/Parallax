/********************************************************************************
** Form generated from reading UI file 'organizerfilemovedialog.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ORGANIZERFILEMOVEDIALOG_H
#define UI_ORGANIZERFILEMOVEDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_organizerFileMoveDialog
{
public:
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QLabel *label_2;
    QLabel *label;
    QLabel *label_from;
    QLabel *label_to;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_file;
    QLabel *label_counter;
    QProgressBar *progressBar_file;
    QHBoxLayout *horizontalLayout_2;
    QProgressBar *progressBar;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_close;
    QSpacerItem *horizontalSpacer_2;

    void setupUi(QDialog *organizerFileMoveDialog)
    {
        if (organizerFileMoveDialog->objectName().isEmpty())
            organizerFileMoveDialog->setObjectName(QStringLiteral("organizerFileMoveDialog"));
        organizerFileMoveDialog->resize(521, 194);
        verticalLayout = new QVBoxLayout(organizerFileMoveDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label_2 = new QLabel(organizerFileMoveDialog);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 2, 0, 1, 1);

        label = new QLabel(organizerFileMoveDialog);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 3, 0, 1, 1);

        label_from = new QLabel(organizerFileMoveDialog);
        label_from->setObjectName(QStringLiteral("label_from"));

        gridLayout->addWidget(label_from, 2, 1, 1, 1);

        label_to = new QLabel(organizerFileMoveDialog);
        label_to->setObjectName(QStringLiteral("label_to"));

        gridLayout->addWidget(label_to, 3, 1, 1, 1);

        label_3 = new QLabel(organizerFileMoveDialog);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout->addWidget(label_3, 0, 0, 1, 1);

        label_4 = new QLabel(organizerFileMoveDialog);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout->addWidget(label_4, 1, 0, 1, 1);

        label_file = new QLabel(organizerFileMoveDialog);
        label_file->setObjectName(QStringLiteral("label_file"));

        gridLayout->addWidget(label_file, 1, 1, 1, 1);

        label_counter = new QLabel(organizerFileMoveDialog);
        label_counter->setObjectName(QStringLiteral("label_counter"));

        gridLayout->addWidget(label_counter, 0, 1, 1, 1);

        gridLayout->setColumnStretch(1, 1);

        verticalLayout->addLayout(gridLayout);

        progressBar_file = new QProgressBar(organizerFileMoveDialog);
        progressBar_file->setObjectName(QStringLiteral("progressBar_file"));
        progressBar_file->setValue(0);
        progressBar_file->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(progressBar_file);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(-1, 10, -1, -1);
        progressBar = new QProgressBar(organizerFileMoveDialog);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setValue(0);
        progressBar->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(progressBar);


        verticalLayout->addLayout(horizontalLayout_2);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton_close = new QPushButton(organizerFileMoveDialog);
        pushButton_close->setObjectName(QStringLiteral("pushButton_close"));
        pushButton_close->setMinimumSize(QSize(100, 25));
        QFont font;
        font.setPointSize(10);
        pushButton_close->setFont(font);

        horizontalLayout->addWidget(pushButton_close);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(organizerFileMoveDialog);

        QMetaObject::connectSlotsByName(organizerFileMoveDialog);
    } // setupUi

    void retranslateUi(QDialog *organizerFileMoveDialog)
    {
        organizerFileMoveDialog->setWindowTitle(QApplication::translate("organizerFileMoveDialog", "Dialog", 0));
        label_2->setText(QApplication::translate("organizerFileMoveDialog", "From:", 0));
        label->setText(QApplication::translate("organizerFileMoveDialog", "To:", 0));
        label_from->setText(QString());
        label_to->setText(QString());
        label_3->setText(QApplication::translate("organizerFileMoveDialog", "Files:", 0));
        label_4->setText(QApplication::translate("organizerFileMoveDialog", "Filename:", 0));
        label_file->setText(QString());
        label_counter->setText(QString());
        pushButton_close->setText(QApplication::translate("organizerFileMoveDialog", "Close", 0));
    } // retranslateUi

};

namespace Ui {
    class organizerFileMoveDialog: public Ui_organizerFileMoveDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ORGANIZERFILEMOVEDIALOG_H
