/********************************************************************************
** Form generated from reading UI file 'processtoarchivedialog.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PROCESSTOARCHIVEDIALOG_H
#define UI_PROCESSTOARCHIVEDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_processToArchiveDialog
{
public:
    QVBoxLayout *verticalLayout_2;
    QStackedWidget *stackedWidget;
    QWidget *page;
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QLineEdit *lineEdit_source;
    QLabel *label_4;
    QHBoxLayout *horizontalLayout_2;
    QLineEdit *lineEdit_target1;
    QLineEdit *lineEdit_target2;
    QLineEdit *lineEdit_title;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_titleChk;
    QLabel *label_folderChk;
    QSpacerItem *verticalSpacer;
    QWidget *page_2;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_3;
    QGridLayout *gridLayout_2;
    QLabel *label_filename;
    QLabel *label_8;
    QLabel *label_s2_to;
    QLabel *label_5;
    QLabel *label_s2_from;
    QLabel *label_6;
    QLabel *label_9;
    QProgressBar *progressBar;
    QProgressBar *progressBar_file;
    QLabel *label_7;
    QLabel *label_11;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_filesize;
    QSpacerItem *horizontalSpacer_3;
    QLabel *label_13;
    QLabel *label_written;
    QSpacerItem *horizontalSpacer_4;
    QLabel *label_10;
    QLabel *label_speed;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_next;
    QPushButton *pushButton_cancel;

    void setupUi(QDialog *processToArchiveDialog)
    {
        if (processToArchiveDialog->objectName().isEmpty())
            processToArchiveDialog->setObjectName(QStringLiteral("processToArchiveDialog"));
        processToArchiveDialog->resize(572, 259);
        verticalLayout_2 = new QVBoxLayout(processToArchiveDialog);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        stackedWidget = new QStackedWidget(processToArchiveDialog);
        stackedWidget->setObjectName(QStringLiteral("stackedWidget"));
        page = new QWidget();
        page->setObjectName(QStringLiteral("page"));
        verticalLayout = new QVBoxLayout(page);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, -1, 0, -1);
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setHorizontalSpacing(0);
        gridLayout->setVerticalSpacing(6);
        lineEdit_source = new QLineEdit(page);
        lineEdit_source->setObjectName(QStringLiteral("lineEdit_source"));
        lineEdit_source->setEnabled(false);

        gridLayout->addWidget(lineEdit_source, 2, 1, 1, 1);

        label_4 = new QLabel(page);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout->addWidget(label_4, 4, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        lineEdit_target1 = new QLineEdit(page);
        lineEdit_target1->setObjectName(QStringLiteral("lineEdit_target1"));
        lineEdit_target1->setEnabled(false);

        horizontalLayout_2->addWidget(lineEdit_target1);

        lineEdit_target2 = new QLineEdit(page);
        lineEdit_target2->setObjectName(QStringLiteral("lineEdit_target2"));

        horizontalLayout_2->addWidget(lineEdit_target2);

        horizontalLayout_2->setStretch(1, 1);

        gridLayout->addLayout(horizontalLayout_2, 4, 1, 1, 1);

        lineEdit_title = new QLineEdit(page);
        lineEdit_title->setObjectName(QStringLiteral("lineEdit_title"));

        gridLayout->addWidget(lineEdit_title, 0, 1, 1, 1);

        label = new QLabel(page);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        label_2 = new QLabel(page);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setMinimumSize(QSize(40, 0));

        gridLayout->addWidget(label_2, 2, 0, 1, 1);

        label_titleChk = new QLabel(page);
        label_titleChk->setObjectName(QStringLiteral("label_titleChk"));
        label_titleChk->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_titleChk, 1, 1, 1, 1);

        label_folderChk = new QLabel(page);
        label_folderChk->setObjectName(QStringLiteral("label_folderChk"));
        label_folderChk->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_folderChk, 5, 1, 1, 1);


        verticalLayout->addLayout(gridLayout);

        verticalSpacer = new QSpacerItem(20, 13, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        stackedWidget->addWidget(page);
        page_2 = new QWidget();
        page_2->setObjectName(QStringLiteral("page_2"));
        verticalLayout_3 = new QVBoxLayout(page_2);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, -1, 0, -1);
        label_3 = new QLabel(page_2);
        label_3->setObjectName(QStringLiteral("label_3"));
        QFont font;
        font.setPointSize(10);
        font.setBold(true);
        font.setWeight(75);
        label_3->setFont(font);
        label_3->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(label_3);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        label_filename = new QLabel(page_2);
        label_filename->setObjectName(QStringLiteral("label_filename"));

        gridLayout_2->addWidget(label_filename, 2, 1, 1, 1);

        label_8 = new QLabel(page_2);
        label_8->setObjectName(QStringLiteral("label_8"));

        gridLayout_2->addWidget(label_8, 2, 0, 1, 1);

        label_s2_to = new QLabel(page_2);
        label_s2_to->setObjectName(QStringLiteral("label_s2_to"));

        gridLayout_2->addWidget(label_s2_to, 1, 1, 1, 1);

        label_5 = new QLabel(page_2);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout_2->addWidget(label_5, 0, 0, 1, 1);

        label_s2_from = new QLabel(page_2);
        label_s2_from->setObjectName(QStringLiteral("label_s2_from"));

        gridLayout_2->addWidget(label_s2_from, 0, 1, 1, 1);

        label_6 = new QLabel(page_2);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout_2->addWidget(label_6, 1, 0, 1, 1);

        label_9 = new QLabel(page_2);
        label_9->setObjectName(QStringLiteral("label_9"));

        gridLayout_2->addWidget(label_9, 5, 0, 1, 1);

        progressBar = new QProgressBar(page_2);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        QFont font1;
        font1.setBold(true);
        font1.setWeight(75);
        progressBar->setFont(font1);
        progressBar->setValue(0);
        progressBar->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(progressBar, 5, 1, 1, 1);

        progressBar_file = new QProgressBar(page_2);
        progressBar_file->setObjectName(QStringLiteral("progressBar_file"));
        progressBar_file->setFont(font1);
        progressBar_file->setValue(0);
        progressBar_file->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(progressBar_file, 4, 1, 1, 1);

        label_7 = new QLabel(page_2);
        label_7->setObjectName(QStringLiteral("label_7"));

        gridLayout_2->addWidget(label_7, 4, 0, 1, 1);

        label_11 = new QLabel(page_2);
        label_11->setObjectName(QStringLiteral("label_11"));

        gridLayout_2->addWidget(label_11, 3, 0, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label_filesize = new QLabel(page_2);
        label_filesize->setObjectName(QStringLiteral("label_filesize"));

        horizontalLayout_3->addWidget(label_filesize);

        horizontalSpacer_3 = new QSpacerItem(40, 5, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);

        label_13 = new QLabel(page_2);
        label_13->setObjectName(QStringLiteral("label_13"));

        horizontalLayout_3->addWidget(label_13);

        label_written = new QLabel(page_2);
        label_written->setObjectName(QStringLiteral("label_written"));

        horizontalLayout_3->addWidget(label_written);

        horizontalSpacer_4 = new QSpacerItem(40, 5, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_4);

        label_10 = new QLabel(page_2);
        label_10->setObjectName(QStringLiteral("label_10"));

        horizontalLayout_3->addWidget(label_10);

        label_speed = new QLabel(page_2);
        label_speed->setObjectName(QStringLiteral("label_speed"));

        horizontalLayout_3->addWidget(label_speed);

        horizontalSpacer_2 = new QSpacerItem(40, 5, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);


        gridLayout_2->addLayout(horizontalLayout_3, 3, 1, 1, 1);

        gridLayout_2->setColumnStretch(1, 1);

        verticalLayout_3->addLayout(gridLayout_2);

        verticalSpacer_2 = new QSpacerItem(20, 44, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_2);

        stackedWidget->addWidget(page_2);

        verticalLayout_2->addWidget(stackedWidget);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton_next = new QPushButton(processToArchiveDialog);
        pushButton_next->setObjectName(QStringLiteral("pushButton_next"));
        pushButton_next->setEnabled(false);
        pushButton_next->setMinimumSize(QSize(100, 25));

        horizontalLayout->addWidget(pushButton_next);

        pushButton_cancel = new QPushButton(processToArchiveDialog);
        pushButton_cancel->setObjectName(QStringLiteral("pushButton_cancel"));
        pushButton_cancel->setMinimumSize(QSize(100, 25));

        horizontalLayout->addWidget(pushButton_cancel);


        verticalLayout_2->addLayout(horizontalLayout);


        retranslateUi(processToArchiveDialog);

        stackedWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(processToArchiveDialog);
    } // setupUi

    void retranslateUi(QDialog *processToArchiveDialog)
    {
        processToArchiveDialog->setWindowTitle(QApplication::translate("processToArchiveDialog", "Dialog", 0));
        label_4->setText(QApplication::translate("processToArchiveDialog", "To", 0));
        label->setText(QApplication::translate("processToArchiveDialog", "Title", 0));
        label_2->setText(QApplication::translate("processToArchiveDialog", "From", 0));
        label_titleChk->setText(QApplication::translate("processToArchiveDialog", "Enter title...", 0));
        label_folderChk->setText(QApplication::translate("processToArchiveDialog", "Enter title...", 0));
        label_3->setText(QApplication::translate("processToArchiveDialog", "Move Files:", 0));
        label_filename->setText(QApplication::translate("processToArchiveDialog", "TextLabel", 0));
        label_8->setText(QApplication::translate("processToArchiveDialog", "File:", 0));
        label_s2_to->setText(QApplication::translate("processToArchiveDialog", "TextLabel", 0));
        label_5->setText(QApplication::translate("processToArchiveDialog", "From:", 0));
        label_s2_from->setText(QApplication::translate("processToArchiveDialog", "TextLabel", 0));
        label_6->setText(QApplication::translate("processToArchiveDialog", "To:", 0));
        label_9->setText(QApplication::translate("processToArchiveDialog", "Overall progress", 0));
        label_7->setText(QApplication::translate("processToArchiveDialog", "File progress:", 0));
        label_11->setText(QApplication::translate("processToArchiveDialog", "Size:", 0));
        label_filesize->setText(QApplication::translate("processToArchiveDialog", "TextLabel", 0));
        label_13->setText(QApplication::translate("processToArchiveDialog", "Written:", 0));
        label_written->setText(QApplication::translate("processToArchiveDialog", "TextLabel", 0));
        label_10->setText(QApplication::translate("processToArchiveDialog", "Speed: ", 0));
        label_speed->setText(QApplication::translate("processToArchiveDialog", "TextLabel", 0));
        pushButton_next->setText(QApplication::translate("processToArchiveDialog", "Next", 0));
        pushButton_cancel->setText(QApplication::translate("processToArchiveDialog", "Cancel", 0));
    } // retranslateUi

};

namespace Ui {
    class processToArchiveDialog: public Ui_processToArchiveDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PROCESSTOARCHIVEDIALOG_H
