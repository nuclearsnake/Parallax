/****************************************************************************
** Meta object code from reading C++ file 'processtoarchivedialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../processtoarchivedialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'processtoarchivedialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_processToArchiveDialog_t {
    QByteArrayData data[9];
    char stringdata[75];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_processToArchiveDialog_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_processToArchiveDialog_t qt_meta_stringdata_processToArchiveDialog = {
    {
QT_MOC_LITERAL(0, 0, 22), // "processToArchiveDialog"
QT_MOC_LITERAL(1, 23, 17), // "titleToFolderName"
QT_MOC_LITERAL(2, 41, 0), // ""
QT_MOC_LITERAL(3, 42, 1), // "s"
QT_MOC_LITERAL(4, 44, 4), // "next"
QT_MOC_LITERAL(5, 49, 12), // "copyProgress"
QT_MOC_LITERAL(6, 62, 4), // "done"
QT_MOC_LITERAL(7, 67, 3), // "all"
QT_MOC_LITERAL(8, 71, 3) // "tmr"

    },
    "processToArchiveDialog\0titleToFolderName\0"
    "\0s\0next\0copyProgress\0done\0all\0tmr"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_processToArchiveDialog[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   29,    2, 0x08 /* Private */,
       4,    0,   32,    2, 0x08 /* Private */,
       5,    3,   33,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void,
    QMetaType::Void, QMetaType::ULongLong, QMetaType::ULongLong, QMetaType::LongLong,    6,    7,    8,

       0        // eod
};

void processToArchiveDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        processToArchiveDialog *_t = static_cast<processToArchiveDialog *>(_o);
        switch (_id) {
        case 0: _t->titleToFolderName((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->next(); break;
        case 2: _t->copyProgress((*reinterpret_cast< quint64(*)>(_a[1])),(*reinterpret_cast< quint64(*)>(_a[2])),(*reinterpret_cast< qint64(*)>(_a[3]))); break;
        default: ;
        }
    }
}

const QMetaObject processToArchiveDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_processToArchiveDialog.data,
      qt_meta_data_processToArchiveDialog,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *processToArchiveDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *processToArchiveDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_processToArchiveDialog.stringdata))
        return static_cast<void*>(const_cast< processToArchiveDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int processToArchiveDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
