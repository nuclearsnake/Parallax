/********************************************************************************
** Form generated from reading UI file 'addsitedialog.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADDSITEDIALOG_H
#define UI_ADDSITEDIALOG_H

#include <QtCore/QVariant>
#include <QtWebKitWidgets/QWebView>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_AddSiteDialog
{
public:
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QLabel *label_3;
    QLabel *label_7;
    QLabel *label;
    QLabel *labelProjectTitle;
    QWebView *webView;
    QSpacerItem *verticalSpacer;
    QLineEdit *lineEditSiteUrl;
    QPlainTextEdit *plainTextEdit;
    QLineEdit *lineEditTitle;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_2;
    QLineEdit *lineEdit;
    QLabel *label_9;
    QLineEdit *lineEdit_time;
    QLabel *label_4;
    QLineEdit *lineEdit_2;
    QLabel *label_5;
    QLineEdit *lineEdit_3;
    QLabel *label_6;
    QLineEdit *lineEdit_4;
    QLabel *label_8;
    QLabel *labelId;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButtonGetData;
    QPushButton *pushButtonAdd;
    QPushButton *pushButtonCancel;

    void setupUi(QDialog *AddSiteDialog)
    {
        if (AddSiteDialog->objectName().isEmpty())
            AddSiteDialog->setObjectName(QStringLiteral("AddSiteDialog"));
        AddSiteDialog->resize(1387, 837);
        horizontalLayout = new QHBoxLayout(AddSiteDialog);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label_3 = new QLabel(AddSiteDialog);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        label_7 = new QLabel(AddSiteDialog);
        label_7->setObjectName(QStringLiteral("label_7"));

        gridLayout->addWidget(label_7, 3, 0, 1, 1);

        label = new QLabel(AddSiteDialog);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        labelProjectTitle = new QLabel(AddSiteDialog);
        labelProjectTitle->setObjectName(QStringLiteral("labelProjectTitle"));

        gridLayout->addWidget(labelProjectTitle, 0, 1, 1, 1);

        webView = new QWebView(AddSiteDialog);
        webView->setObjectName(QStringLiteral("webView"));
        webView->setEnabled(true);
        webView->setMaximumSize(QSize(16777215, 1565465));

        gridLayout->addWidget(webView, 4, 1, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 12, 1, 1, 1);

        lineEditSiteUrl = new QLineEdit(AddSiteDialog);
        lineEditSiteUrl->setObjectName(QStringLiteral("lineEditSiteUrl"));

        gridLayout->addWidget(lineEditSiteUrl, 2, 1, 1, 1);

        plainTextEdit = new QPlainTextEdit(AddSiteDialog);
        plainTextEdit->setObjectName(QStringLiteral("plainTextEdit"));

        gridLayout->addWidget(plainTextEdit, 6, 1, 1, 1);

        lineEditTitle = new QLineEdit(AddSiteDialog);
        lineEditTitle->setObjectName(QStringLiteral("lineEditTitle"));

        gridLayout->addWidget(lineEditTitle, 3, 1, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label_2 = new QLabel(AddSiteDialog);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_3->addWidget(label_2);

        lineEdit = new QLineEdit(AddSiteDialog);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));

        horizontalLayout_3->addWidget(lineEdit);

        label_9 = new QLabel(AddSiteDialog);
        label_9->setObjectName(QStringLiteral("label_9"));

        horizontalLayout_3->addWidget(label_9);

        lineEdit_time = new QLineEdit(AddSiteDialog);
        lineEdit_time->setObjectName(QStringLiteral("lineEdit_time"));

        horizontalLayout_3->addWidget(lineEdit_time);

        label_4 = new QLabel(AddSiteDialog);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout_3->addWidget(label_4);

        lineEdit_2 = new QLineEdit(AddSiteDialog);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));

        horizontalLayout_3->addWidget(lineEdit_2);

        label_5 = new QLabel(AddSiteDialog);
        label_5->setObjectName(QStringLiteral("label_5"));

        horizontalLayout_3->addWidget(label_5);

        lineEdit_3 = new QLineEdit(AddSiteDialog);
        lineEdit_3->setObjectName(QStringLiteral("lineEdit_3"));

        horizontalLayout_3->addWidget(lineEdit_3);

        label_6 = new QLabel(AddSiteDialog);
        label_6->setObjectName(QStringLiteral("label_6"));

        horizontalLayout_3->addWidget(label_6);

        lineEdit_4 = new QLineEdit(AddSiteDialog);
        lineEdit_4->setObjectName(QStringLiteral("lineEdit_4"));

        horizontalLayout_3->addWidget(lineEdit_4);


        gridLayout->addLayout(horizontalLayout_3, 8, 1, 1, 1);

        label_8 = new QLabel(AddSiteDialog);
        label_8->setObjectName(QStringLiteral("label_8"));

        gridLayout->addWidget(label_8, 1, 0, 1, 1);

        labelId = new QLabel(AddSiteDialog);
        labelId->setObjectName(QStringLiteral("labelId"));

        gridLayout->addWidget(labelId, 1, 1, 1, 1);


        verticalLayout->addLayout(gridLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        pushButtonGetData = new QPushButton(AddSiteDialog);
        pushButtonGetData->setObjectName(QStringLiteral("pushButtonGetData"));
        pushButtonGetData->setMinimumSize(QSize(100, 25));

        horizontalLayout_2->addWidget(pushButtonGetData);

        pushButtonAdd = new QPushButton(AddSiteDialog);
        pushButtonAdd->setObjectName(QStringLiteral("pushButtonAdd"));
        pushButtonAdd->setMinimumSize(QSize(100, 25));

        horizontalLayout_2->addWidget(pushButtonAdd);

        pushButtonCancel = new QPushButton(AddSiteDialog);
        pushButtonCancel->setObjectName(QStringLiteral("pushButtonCancel"));
        pushButtonCancel->setMinimumSize(QSize(100, 25));

        horizontalLayout_2->addWidget(pushButtonCancel);


        verticalLayout->addLayout(horizontalLayout_2);


        horizontalLayout->addLayout(verticalLayout);


        retranslateUi(AddSiteDialog);

        QMetaObject::connectSlotsByName(AddSiteDialog);
    } // setupUi

    void retranslateUi(QDialog *AddSiteDialog)
    {
        AddSiteDialog->setWindowTitle(QApplication::translate("AddSiteDialog", "Dialog", 0));
        label_3->setText(QApplication::translate("AddSiteDialog", "URL:", 0));
        label_7->setText(QApplication::translate("AddSiteDialog", "Title:", 0));
        label->setText(QApplication::translate("AddSiteDialog", "Project:", 0));
        labelProjectTitle->setText(QApplication::translate("AddSiteDialog", "TextLabel", 0));
        label_2->setText(QApplication::translate("AddSiteDialog", "Date:", 0));
        label_9->setText(QApplication::translate("AddSiteDialog", "Time:", 0));
        label_4->setText(QApplication::translate("AddSiteDialog", "Views:", 0));
        label_5->setText(QApplication::translate("AddSiteDialog", "Favs:", 0));
        label_6->setText(QApplication::translate("AddSiteDialog", "Comments:", 0));
        label_8->setText(QApplication::translate("AddSiteDialog", "ID:", 0));
        labelId->setText(QApplication::translate("AddSiteDialog", "TextLabel", 0));
        pushButtonGetData->setText(QApplication::translate("AddSiteDialog", "Get data", 0));
        pushButtonAdd->setText(QApplication::translate("AddSiteDialog", "Add", 0));
        pushButtonCancel->setText(QApplication::translate("AddSiteDialog", "Cancel", 0));
    } // retranslateUi

};

namespace Ui {
    class AddSiteDialog: public Ui_AddSiteDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADDSITEDIALOG_H
