/****************************************************************************
** Meta object code from reading C++ file 'siteupdater.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../dialogs/siteupdater.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'siteupdater.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_siteUpdater_t {
    QByteArrayData data[6];
    char stringdata[45];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_siteUpdater_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_siteUpdater_t qt_meta_stringdata_siteUpdater = {
    {
QT_MOC_LITERAL(0, 0, 11), // "siteUpdater"
QT_MOC_LITERAL(1, 12, 7), // "loadUrl"
QT_MOC_LITERAL(2, 20, 0), // ""
QT_MOC_LITERAL(3, 21, 7), // "nextUrl"
QT_MOC_LITERAL(4, 29, 2), // "go"
QT_MOC_LITERAL(5, 32, 12) // "nextUrlProxy"

    },
    "siteUpdater\0loadUrl\0\0nextUrl\0go\0"
    "nextUrlProxy"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_siteUpdater[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   34,    2, 0x0a /* Public */,
       3,    0,   35,    2, 0x0a /* Public */,
       4,    0,   36,    2, 0x0a /* Public */,
       5,    0,   37,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void siteUpdater::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        siteUpdater *_t = static_cast<siteUpdater *>(_o);
        switch (_id) {
        case 0: _t->loadUrl(); break;
        case 1: _t->nextUrl(); break;
        case 2: _t->go(); break;
        case 3: _t->nextUrlProxy(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject siteUpdater::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_siteUpdater.data,
      qt_meta_data_siteUpdater,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *siteUpdater::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *siteUpdater::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_siteUpdater.stringdata))
        return static_cast<void*>(const_cast< siteUpdater*>(this));
    return QDialog::qt_metacast(_clname);
}

int siteUpdater::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
