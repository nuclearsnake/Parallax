/********************************************************************************
** Form generated from reading UI file 'extstatsdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EXTSTATSDIALOG_H
#define UI_EXTSTATSDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_extStatsDialog
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_title;
    QLabel *label_date;
    QSpacerItem *horizontalSpacer;
    QLabel *label_score_calc;
    QLabel *label_score_bias;
    QLabel *label_score;
    QHBoxLayout *horizontalLayout_2;
    QSlider *horizontalSlider;
    QLabel *label_sliderval;
    QGridLayout *gridLayout;
    QLabel *label_Fmax_FB;
    QLabel *label_F_FB;
    QLabel *label_Fscore_FB;
    QLabel *label_Fscore_FL;
    QLabel *label_F_FL;
    QLabel *label_Fmax_FL;
    QLabel *label_Fmax_PX;
    QLabel *label_Fmin_FL;
    QLabel *label_Vscore_FL;
    QLabel *label_Year_PX;
    QLabel *label_V_FL;
    QLabel *label_Vmax_FL;
    QLabel *label_Vmin_FL;
    QLabel *label_Year_FL;
    QLabel *label_36;
    QLabel *label_Vmin_PX;
    QLabel *label_37;
    QLabel *label_Fmin_PX;
    QLabel *label_Vscore_PX;
    QLabel *label_V_PX;
    QLabel *label_Vmax_PX;
    QLabel *label_10;
    QLabel *label_Fscore_GP;
    QProgressBar *progressBar_5P;
    QLabel *label_Vmin_GP;
    QLabel *label_Vscore_5P;
    QLabel *label_Fmin_5P;
    QLabel *label_Fmax_5P;
    QLabel *label_F_5P;
    QLabel *label_Year_IG;
    QLabel *label_F_IG;
    QLabel *label_Vmin_IG;
    QLabel *label_V_IG;
    QLabel *label_Vmax_IG;
    QLabel *label_Fscore_PX;
    QLabel *label_F_PX;
    QLabel *label_6;
    QLabel *label_5;
    QLabel *label_Vmin_DA;
    QLabel *label_Vmax_5P;
    QLabel *label_Vmin_5P;
    QLabel *label_F_DA;
    QLabel *label_Fscore_DA;
    QLabel *label_Year_5P;
    QLabel *label_21;
    QLabel *label_22;
    QLabel *label_Year_DA;
    QLabel *label_Fmin_DA;
    QLabel *label_2;
    QLabel *label_Vmax_DA;
    QLabel *label_4;
    QLabel *label_8;
    QLabel *label_V_DA;
    QLabel *label_9;
    QLabel *label_Vscore_DA;
    QLabel *label_7;
    QProgressBar *progressBar_FL;
    QProgressBar *progressBar_PX;
    QLabel *label_35;
    QLabel *label_Year_FB;
    QLabel *label_Vmin_FB;
    QLabel *label_Vmax_FB;
    QLabel *label_V_FB;
    QLabel *label_Vscore_FB;
    QLabel *label_Fmax_DA;
    QLabel *label_3;
    QLabel *label;
    QProgressBar *progressBar_DA;
    QLabel *label_Fscore_5P;
    QLabel *label_V_5P;
    QProgressBar *progressBar_GP;
    QLabel *label_Fmin_GP;
    QLabel *label_Vscore_GP;
    QProgressBar *progressBar_FB;
    QLabel *label_Vmax_GP;
    QLabel *label_Fmax_GP;
    QLabel *label_F_GP;
    QLabel *label_V_GP;
    QLabel *label_Year_GP;
    QProgressBar *progressBar_YP;
    QLabel *label_38;
    QLabel *label_Year_YP;
    QLabel *label_Vmin_YP;
    QLabel *label_Vmax_YP;
    QLabel *label_V_YP;
    QLabel *label_Vscore_YP;
    QLabel *label_Fmin_YP;
    QLabel *label_Fmax_YP;
    QLabel *label_F_YP;
    QLabel *label_Fscore_YP;
    QLabel *label_Fmin_FB;
    QLabel *label_Fmax_IG;
    QLabel *label_Fscore_IG;
    QProgressBar *progressBar_IG;
    QLabel *label_Vscore_IG;
    QLabel *label_Fmin_IG;
    QLabel *label_39;
    QSpacerItem *verticalSpacer;

    void setupUi(QDialog *extStatsDialog)
    {
        if (extStatsDialog->objectName().isEmpty())
            extStatsDialog->setObjectName(QStringLiteral("extStatsDialog"));
        extStatsDialog->resize(787, 372);
        verticalLayout = new QVBoxLayout(extStatsDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(-1, 10, -1, -1);
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        label_title = new QLabel(extStatsDialog);
        label_title->setObjectName(QStringLiteral("label_title"));
        QFont font;
        font.setPointSize(11);
        font.setBold(true);
        font.setWeight(75);
        label_title->setFont(font);
        label_title->setAlignment(Qt::AlignBottom|Qt::AlignLeading|Qt::AlignLeft);

        verticalLayout_2->addWidget(label_title);

        label_date = new QLabel(extStatsDialog);
        label_date->setObjectName(QStringLiteral("label_date"));
        label_date->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        verticalLayout_2->addWidget(label_date);


        horizontalLayout->addLayout(verticalLayout_2);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label_score_calc = new QLabel(extStatsDialog);
        label_score_calc->setObjectName(QStringLiteral("label_score_calc"));
        label_score_calc->setMinimumSize(QSize(100, 0));
        QFont font1;
        font1.setPointSize(30);
        font1.setBold(false);
        font1.setWeight(50);
        label_score_calc->setFont(font1);
        label_score_calc->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(label_score_calc);

        label_score_bias = new QLabel(extStatsDialog);
        label_score_bias->setObjectName(QStringLiteral("label_score_bias"));
        label_score_bias->setMinimumSize(QSize(100, 0));
        label_score_bias->setFont(font1);
        label_score_bias->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(label_score_bias);

        label_score = new QLabel(extStatsDialog);
        label_score->setObjectName(QStringLiteral("label_score"));
        label_score->setMinimumSize(QSize(100, 0));
        label_score->setFont(font1);
        label_score->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(label_score);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(-1, 0, -1, -1);
        horizontalSlider = new QSlider(extStatsDialog);
        horizontalSlider->setObjectName(QStringLiteral("horizontalSlider"));
        horizontalSlider->setMinimum(1);
        horizontalSlider->setMaximum(300);
        horizontalSlider->setValue(60);
        horizontalSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_2->addWidget(horizontalSlider);

        label_sliderval = new QLabel(extStatsDialog);
        label_sliderval->setObjectName(QStringLiteral("label_sliderval"));
        label_sliderval->setMinimumSize(QSize(40, 0));
        QFont font2;
        font2.setKerning(true);
        label_sliderval->setFont(font2);
        label_sliderval->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(label_sliderval);


        verticalLayout->addLayout(horizontalLayout_2);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label_Fmax_FB = new QLabel(extStatsDialog);
        label_Fmax_FB->setObjectName(QStringLiteral("label_Fmax_FB"));
        label_Fmax_FB->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Fmax_FB, 5, 9, 1, 1);

        label_F_FB = new QLabel(extStatsDialog);
        label_F_FB->setObjectName(QStringLiteral("label_F_FB"));
        QFont font3;
        font3.setBold(true);
        font3.setWeight(75);
        label_F_FB->setFont(font3);
        label_F_FB->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_F_FB, 5, 8, 1, 1);

        label_Fscore_FB = new QLabel(extStatsDialog);
        label_Fscore_FB->setObjectName(QStringLiteral("label_Fscore_FB"));
        label_Fscore_FB->setFont(font3);
        label_Fscore_FB->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Fscore_FB, 5, 11, 1, 1);

        label_Fscore_FL = new QLabel(extStatsDialog);
        label_Fscore_FL->setObjectName(QStringLiteral("label_Fscore_FL"));
        label_Fscore_FL->setFont(font3);
        label_Fscore_FL->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Fscore_FL, 6, 11, 1, 1);

        label_F_FL = new QLabel(extStatsDialog);
        label_F_FL->setObjectName(QStringLiteral("label_F_FL"));
        label_F_FL->setFont(font3);
        label_F_FL->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_F_FL, 6, 8, 1, 1);

        label_Fmax_FL = new QLabel(extStatsDialog);
        label_Fmax_FL->setObjectName(QStringLiteral("label_Fmax_FL"));
        label_Fmax_FL->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Fmax_FL, 6, 9, 1, 1);

        label_Fmax_PX = new QLabel(extStatsDialog);
        label_Fmax_PX->setObjectName(QStringLiteral("label_Fmax_PX"));
        label_Fmax_PX->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Fmax_PX, 7, 9, 1, 1);

        label_Fmin_FL = new QLabel(extStatsDialog);
        label_Fmin_FL->setObjectName(QStringLiteral("label_Fmin_FL"));
        label_Fmin_FL->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Fmin_FL, 6, 7, 1, 1);

        label_Vscore_FL = new QLabel(extStatsDialog);
        label_Vscore_FL->setObjectName(QStringLiteral("label_Vscore_FL"));
        label_Vscore_FL->setFont(font3);
        label_Vscore_FL->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Vscore_FL, 6, 6, 1, 1);

        label_Year_PX = new QLabel(extStatsDialog);
        label_Year_PX->setObjectName(QStringLiteral("label_Year_PX"));
        label_Year_PX->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Year_PX, 7, 1, 1, 1);

        label_V_FL = new QLabel(extStatsDialog);
        label_V_FL->setObjectName(QStringLiteral("label_V_FL"));
        label_V_FL->setFont(font3);
        label_V_FL->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_V_FL, 6, 3, 1, 1);

        label_Vmax_FL = new QLabel(extStatsDialog);
        label_Vmax_FL->setObjectName(QStringLiteral("label_Vmax_FL"));
        label_Vmax_FL->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Vmax_FL, 6, 4, 1, 1);

        label_Vmin_FL = new QLabel(extStatsDialog);
        label_Vmin_FL->setObjectName(QStringLiteral("label_Vmin_FL"));
        label_Vmin_FL->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Vmin_FL, 6, 2, 1, 1);

        label_Year_FL = new QLabel(extStatsDialog);
        label_Year_FL->setObjectName(QStringLiteral("label_Year_FL"));
        label_Year_FL->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Year_FL, 6, 1, 1, 1);

        label_36 = new QLabel(extStatsDialog);
        label_36->setObjectName(QStringLiteral("label_36"));

        gridLayout->addWidget(label_36, 6, 0, 1, 1);

        label_Vmin_PX = new QLabel(extStatsDialog);
        label_Vmin_PX->setObjectName(QStringLiteral("label_Vmin_PX"));
        label_Vmin_PX->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Vmin_PX, 7, 2, 1, 1);

        label_37 = new QLabel(extStatsDialog);
        label_37->setObjectName(QStringLiteral("label_37"));

        gridLayout->addWidget(label_37, 7, 0, 1, 1);

        label_Fmin_PX = new QLabel(extStatsDialog);
        label_Fmin_PX->setObjectName(QStringLiteral("label_Fmin_PX"));
        label_Fmin_PX->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Fmin_PX, 7, 7, 1, 1);

        label_Vscore_PX = new QLabel(extStatsDialog);
        label_Vscore_PX->setObjectName(QStringLiteral("label_Vscore_PX"));
        label_Vscore_PX->setFont(font3);
        label_Vscore_PX->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Vscore_PX, 7, 6, 1, 1);

        label_V_PX = new QLabel(extStatsDialog);
        label_V_PX->setObjectName(QStringLiteral("label_V_PX"));
        label_V_PX->setFont(font3);
        label_V_PX->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_V_PX, 7, 3, 1, 1);

        label_Vmax_PX = new QLabel(extStatsDialog);
        label_Vmax_PX->setObjectName(QStringLiteral("label_Vmax_PX"));
        label_Vmax_PX->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Vmax_PX, 7, 4, 1, 1);

        label_10 = new QLabel(extStatsDialog);
        label_10->setObjectName(QStringLiteral("label_10"));

        gridLayout->addWidget(label_10, 3, 0, 1, 1);

        label_Fscore_GP = new QLabel(extStatsDialog);
        label_Fscore_GP->setObjectName(QStringLiteral("label_Fscore_GP"));
        label_Fscore_GP->setFont(font3);
        label_Fscore_GP->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Fscore_GP, 4, 11, 1, 1);

        progressBar_5P = new QProgressBar(extStatsDialog);
        progressBar_5P->setObjectName(QStringLiteral("progressBar_5P"));
        progressBar_5P->setMaximum(100);
        progressBar_5P->setValue(0);

        gridLayout->addWidget(progressBar_5P, 3, 12, 1, 1);

        label_Vmin_GP = new QLabel(extStatsDialog);
        label_Vmin_GP->setObjectName(QStringLiteral("label_Vmin_GP"));
        label_Vmin_GP->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Vmin_GP, 4, 2, 1, 1);

        label_Vscore_5P = new QLabel(extStatsDialog);
        label_Vscore_5P->setObjectName(QStringLiteral("label_Vscore_5P"));
        label_Vscore_5P->setFont(font3);
        label_Vscore_5P->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Vscore_5P, 3, 6, 1, 1);

        label_Fmin_5P = new QLabel(extStatsDialog);
        label_Fmin_5P->setObjectName(QStringLiteral("label_Fmin_5P"));
        label_Fmin_5P->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Fmin_5P, 3, 7, 1, 1);

        label_Fmax_5P = new QLabel(extStatsDialog);
        label_Fmax_5P->setObjectName(QStringLiteral("label_Fmax_5P"));
        label_Fmax_5P->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Fmax_5P, 3, 9, 1, 1);

        label_F_5P = new QLabel(extStatsDialog);
        label_F_5P->setObjectName(QStringLiteral("label_F_5P"));
        label_F_5P->setFont(font3);
        label_F_5P->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_F_5P, 3, 8, 1, 1);

        label_Year_IG = new QLabel(extStatsDialog);
        label_Year_IG->setObjectName(QStringLiteral("label_Year_IG"));
        label_Year_IG->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Year_IG, 9, 1, 1, 1);

        label_F_IG = new QLabel(extStatsDialog);
        label_F_IG->setObjectName(QStringLiteral("label_F_IG"));
        label_F_IG->setFont(font3);
        label_F_IG->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_F_IG, 9, 8, 1, 1);

        label_Vmin_IG = new QLabel(extStatsDialog);
        label_Vmin_IG->setObjectName(QStringLiteral("label_Vmin_IG"));
        label_Vmin_IG->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Vmin_IG, 9, 2, 1, 1);

        label_V_IG = new QLabel(extStatsDialog);
        label_V_IG->setObjectName(QStringLiteral("label_V_IG"));
        label_V_IG->setFont(font3);
        label_V_IG->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_V_IG, 9, 3, 1, 1);

        label_Vmax_IG = new QLabel(extStatsDialog);
        label_Vmax_IG->setObjectName(QStringLiteral("label_Vmax_IG"));
        label_Vmax_IG->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Vmax_IG, 9, 4, 1, 1);

        label_Fscore_PX = new QLabel(extStatsDialog);
        label_Fscore_PX->setObjectName(QStringLiteral("label_Fscore_PX"));
        label_Fscore_PX->setFont(font3);
        label_Fscore_PX->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Fscore_PX, 7, 11, 1, 1);

        label_F_PX = new QLabel(extStatsDialog);
        label_F_PX->setObjectName(QStringLiteral("label_F_PX"));
        label_F_PX->setFont(font3);
        label_F_PX->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_F_PX, 7, 8, 1, 1);

        label_6 = new QLabel(extStatsDialog);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setMinimumSize(QSize(60, 0));
        label_6->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_6, 0, 7, 1, 1);

        label_5 = new QLabel(extStatsDialog);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setMinimumSize(QSize(60, 0));
        label_5->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_5, 0, 6, 1, 1);

        label_Vmin_DA = new QLabel(extStatsDialog);
        label_Vmin_DA->setObjectName(QStringLiteral("label_Vmin_DA"));
        label_Vmin_DA->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Vmin_DA, 2, 2, 1, 1);

        label_Vmax_5P = new QLabel(extStatsDialog);
        label_Vmax_5P->setObjectName(QStringLiteral("label_Vmax_5P"));
        label_Vmax_5P->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Vmax_5P, 3, 4, 1, 1);

        label_Vmin_5P = new QLabel(extStatsDialog);
        label_Vmin_5P->setObjectName(QStringLiteral("label_Vmin_5P"));
        label_Vmin_5P->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Vmin_5P, 3, 2, 1, 1);

        label_F_DA = new QLabel(extStatsDialog);
        label_F_DA->setObjectName(QStringLiteral("label_F_DA"));
        label_F_DA->setFont(font3);
        label_F_DA->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_F_DA, 2, 8, 1, 1);

        label_Fscore_DA = new QLabel(extStatsDialog);
        label_Fscore_DA->setObjectName(QStringLiteral("label_Fscore_DA"));
        label_Fscore_DA->setFont(font3);
        label_Fscore_DA->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Fscore_DA, 2, 11, 1, 1);

        label_Year_5P = new QLabel(extStatsDialog);
        label_Year_5P->setObjectName(QStringLiteral("label_Year_5P"));
        label_Year_5P->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Year_5P, 3, 1, 1, 1);

        label_21 = new QLabel(extStatsDialog);
        label_21->setObjectName(QStringLiteral("label_21"));
        label_21->setMinimumSize(QSize(60, 0));
        label_21->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_21, 0, 1, 1, 1);

        label_22 = new QLabel(extStatsDialog);
        label_22->setObjectName(QStringLiteral("label_22"));

        gridLayout->addWidget(label_22, 4, 0, 1, 1);

        label_Year_DA = new QLabel(extStatsDialog);
        label_Year_DA->setObjectName(QStringLiteral("label_Year_DA"));
        label_Year_DA->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Year_DA, 2, 1, 1, 1);

        label_Fmin_DA = new QLabel(extStatsDialog);
        label_Fmin_DA->setObjectName(QStringLiteral("label_Fmin_DA"));
        label_Fmin_DA->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Fmin_DA, 2, 7, 1, 1);

        label_2 = new QLabel(extStatsDialog);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setMinimumSize(QSize(60, 0));
        label_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_2, 0, 2, 1, 1);

        label_Vmax_DA = new QLabel(extStatsDialog);
        label_Vmax_DA->setObjectName(QStringLiteral("label_Vmax_DA"));
        label_Vmax_DA->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Vmax_DA, 2, 4, 1, 1);

        label_4 = new QLabel(extStatsDialog);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setMinimumSize(QSize(60, 0));
        label_4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_4, 0, 4, 1, 1);

        label_8 = new QLabel(extStatsDialog);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setMinimumSize(QSize(60, 0));
        label_8->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_8, 0, 8, 1, 1);

        label_V_DA = new QLabel(extStatsDialog);
        label_V_DA->setObjectName(QStringLiteral("label_V_DA"));
        label_V_DA->setFont(font3);
        label_V_DA->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_V_DA, 2, 3, 1, 1);

        label_9 = new QLabel(extStatsDialog);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setMinimumSize(QSize(60, 0));
        label_9->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_9, 0, 11, 1, 1);

        label_Vscore_DA = new QLabel(extStatsDialog);
        label_Vscore_DA->setObjectName(QStringLiteral("label_Vscore_DA"));
        label_Vscore_DA->setFont(font3);
        label_Vscore_DA->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Vscore_DA, 2, 6, 1, 1);

        label_7 = new QLabel(extStatsDialog);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setMinimumSize(QSize(60, 0));
        label_7->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_7, 0, 9, 1, 1);

        progressBar_FL = new QProgressBar(extStatsDialog);
        progressBar_FL->setObjectName(QStringLiteral("progressBar_FL"));
        progressBar_FL->setMaximum(100);
        progressBar_FL->setValue(0);

        gridLayout->addWidget(progressBar_FL, 6, 12, 1, 1);

        progressBar_PX = new QProgressBar(extStatsDialog);
        progressBar_PX->setObjectName(QStringLiteral("progressBar_PX"));
        progressBar_PX->setMaximum(100);
        progressBar_PX->setValue(0);

        gridLayout->addWidget(progressBar_PX, 7, 12, 1, 1);

        label_35 = new QLabel(extStatsDialog);
        label_35->setObjectName(QStringLiteral("label_35"));

        gridLayout->addWidget(label_35, 5, 0, 1, 1);

        label_Year_FB = new QLabel(extStatsDialog);
        label_Year_FB->setObjectName(QStringLiteral("label_Year_FB"));
        label_Year_FB->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Year_FB, 5, 1, 1, 1);

        label_Vmin_FB = new QLabel(extStatsDialog);
        label_Vmin_FB->setObjectName(QStringLiteral("label_Vmin_FB"));
        label_Vmin_FB->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Vmin_FB, 5, 2, 1, 1);

        label_Vmax_FB = new QLabel(extStatsDialog);
        label_Vmax_FB->setObjectName(QStringLiteral("label_Vmax_FB"));
        label_Vmax_FB->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Vmax_FB, 5, 4, 1, 1);

        label_V_FB = new QLabel(extStatsDialog);
        label_V_FB->setObjectName(QStringLiteral("label_V_FB"));
        label_V_FB->setFont(font3);
        label_V_FB->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_V_FB, 5, 3, 1, 1);

        label_Vscore_FB = new QLabel(extStatsDialog);
        label_Vscore_FB->setObjectName(QStringLiteral("label_Vscore_FB"));
        label_Vscore_FB->setFont(font3);
        label_Vscore_FB->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Vscore_FB, 5, 6, 1, 1);

        label_Fmax_DA = new QLabel(extStatsDialog);
        label_Fmax_DA->setObjectName(QStringLiteral("label_Fmax_DA"));
        label_Fmax_DA->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Fmax_DA, 2, 9, 1, 1);

        label_3 = new QLabel(extStatsDialog);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setMinimumSize(QSize(60, 0));
        label_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_3, 0, 3, 1, 1);

        label = new QLabel(extStatsDialog);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 2, 0, 1, 1);

        progressBar_DA = new QProgressBar(extStatsDialog);
        progressBar_DA->setObjectName(QStringLiteral("progressBar_DA"));
        progressBar_DA->setMaximum(100);
        progressBar_DA->setValue(0);

        gridLayout->addWidget(progressBar_DA, 2, 12, 1, 1);

        label_Fscore_5P = new QLabel(extStatsDialog);
        label_Fscore_5P->setObjectName(QStringLiteral("label_Fscore_5P"));
        label_Fscore_5P->setFont(font3);
        label_Fscore_5P->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Fscore_5P, 3, 11, 1, 1);

        label_V_5P = new QLabel(extStatsDialog);
        label_V_5P->setObjectName(QStringLiteral("label_V_5P"));
        label_V_5P->setFont(font3);
        label_V_5P->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_V_5P, 3, 3, 1, 1);

        progressBar_GP = new QProgressBar(extStatsDialog);
        progressBar_GP->setObjectName(QStringLiteral("progressBar_GP"));
        progressBar_GP->setMaximum(100);
        progressBar_GP->setValue(0);

        gridLayout->addWidget(progressBar_GP, 4, 12, 1, 1);

        label_Fmin_GP = new QLabel(extStatsDialog);
        label_Fmin_GP->setObjectName(QStringLiteral("label_Fmin_GP"));
        label_Fmin_GP->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Fmin_GP, 4, 7, 1, 1);

        label_Vscore_GP = new QLabel(extStatsDialog);
        label_Vscore_GP->setObjectName(QStringLiteral("label_Vscore_GP"));
        label_Vscore_GP->setFont(font3);
        label_Vscore_GP->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Vscore_GP, 4, 6, 1, 1);

        progressBar_FB = new QProgressBar(extStatsDialog);
        progressBar_FB->setObjectName(QStringLiteral("progressBar_FB"));
        progressBar_FB->setMaximum(100);
        progressBar_FB->setValue(0);

        gridLayout->addWidget(progressBar_FB, 5, 12, 1, 1);

        label_Vmax_GP = new QLabel(extStatsDialog);
        label_Vmax_GP->setObjectName(QStringLiteral("label_Vmax_GP"));
        label_Vmax_GP->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Vmax_GP, 4, 4, 1, 1);

        label_Fmax_GP = new QLabel(extStatsDialog);
        label_Fmax_GP->setObjectName(QStringLiteral("label_Fmax_GP"));
        label_Fmax_GP->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Fmax_GP, 4, 9, 1, 1);

        label_F_GP = new QLabel(extStatsDialog);
        label_F_GP->setObjectName(QStringLiteral("label_F_GP"));
        label_F_GP->setFont(font3);
        label_F_GP->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_F_GP, 4, 8, 1, 1);

        label_V_GP = new QLabel(extStatsDialog);
        label_V_GP->setObjectName(QStringLiteral("label_V_GP"));
        label_V_GP->setFont(font3);
        label_V_GP->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_V_GP, 4, 3, 1, 1);

        label_Year_GP = new QLabel(extStatsDialog);
        label_Year_GP->setObjectName(QStringLiteral("label_Year_GP"));
        label_Year_GP->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Year_GP, 4, 1, 1, 1);

        progressBar_YP = new QProgressBar(extStatsDialog);
        progressBar_YP->setObjectName(QStringLiteral("progressBar_YP"));
        progressBar_YP->setMaximum(100);
        progressBar_YP->setValue(0);

        gridLayout->addWidget(progressBar_YP, 8, 12, 1, 1);

        label_38 = new QLabel(extStatsDialog);
        label_38->setObjectName(QStringLiteral("label_38"));

        gridLayout->addWidget(label_38, 8, 0, 1, 1);

        label_Year_YP = new QLabel(extStatsDialog);
        label_Year_YP->setObjectName(QStringLiteral("label_Year_YP"));
        label_Year_YP->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Year_YP, 8, 1, 1, 1);

        label_Vmin_YP = new QLabel(extStatsDialog);
        label_Vmin_YP->setObjectName(QStringLiteral("label_Vmin_YP"));
        label_Vmin_YP->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Vmin_YP, 8, 2, 1, 1);

        label_Vmax_YP = new QLabel(extStatsDialog);
        label_Vmax_YP->setObjectName(QStringLiteral("label_Vmax_YP"));
        label_Vmax_YP->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Vmax_YP, 8, 4, 1, 1);

        label_V_YP = new QLabel(extStatsDialog);
        label_V_YP->setObjectName(QStringLiteral("label_V_YP"));
        label_V_YP->setFont(font3);
        label_V_YP->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_V_YP, 8, 3, 1, 1);

        label_Vscore_YP = new QLabel(extStatsDialog);
        label_Vscore_YP->setObjectName(QStringLiteral("label_Vscore_YP"));
        label_Vscore_YP->setFont(font3);
        label_Vscore_YP->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Vscore_YP, 8, 6, 1, 1);

        label_Fmin_YP = new QLabel(extStatsDialog);
        label_Fmin_YP->setObjectName(QStringLiteral("label_Fmin_YP"));
        label_Fmin_YP->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Fmin_YP, 8, 7, 1, 1);

        label_Fmax_YP = new QLabel(extStatsDialog);
        label_Fmax_YP->setObjectName(QStringLiteral("label_Fmax_YP"));
        label_Fmax_YP->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Fmax_YP, 8, 9, 1, 1);

        label_F_YP = new QLabel(extStatsDialog);
        label_F_YP->setObjectName(QStringLiteral("label_F_YP"));
        label_F_YP->setFont(font3);
        label_F_YP->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_F_YP, 8, 8, 1, 1);

        label_Fscore_YP = new QLabel(extStatsDialog);
        label_Fscore_YP->setObjectName(QStringLiteral("label_Fscore_YP"));
        label_Fscore_YP->setFont(font3);
        label_Fscore_YP->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Fscore_YP, 8, 11, 1, 1);

        label_Fmin_FB = new QLabel(extStatsDialog);
        label_Fmin_FB->setObjectName(QStringLiteral("label_Fmin_FB"));
        label_Fmin_FB->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Fmin_FB, 5, 7, 1, 1);

        label_Fmax_IG = new QLabel(extStatsDialog);
        label_Fmax_IG->setObjectName(QStringLiteral("label_Fmax_IG"));
        label_Fmax_IG->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Fmax_IG, 9, 9, 1, 1);

        label_Fscore_IG = new QLabel(extStatsDialog);
        label_Fscore_IG->setObjectName(QStringLiteral("label_Fscore_IG"));
        label_Fscore_IG->setFont(font3);
        label_Fscore_IG->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Fscore_IG, 9, 11, 1, 1);

        progressBar_IG = new QProgressBar(extStatsDialog);
        progressBar_IG->setObjectName(QStringLiteral("progressBar_IG"));
        progressBar_IG->setMaximum(100);
        progressBar_IG->setValue(0);

        gridLayout->addWidget(progressBar_IG, 9, 12, 1, 1);

        label_Vscore_IG = new QLabel(extStatsDialog);
        label_Vscore_IG->setObjectName(QStringLiteral("label_Vscore_IG"));
        label_Vscore_IG->setFont(font3);
        label_Vscore_IG->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Vscore_IG, 9, 6, 1, 1);

        label_Fmin_IG = new QLabel(extStatsDialog);
        label_Fmin_IG->setObjectName(QStringLiteral("label_Fmin_IG"));
        label_Fmin_IG->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_Fmin_IG, 9, 7, 1, 1);

        label_39 = new QLabel(extStatsDialog);
        label_39->setObjectName(QStringLiteral("label_39"));

        gridLayout->addWidget(label_39, 9, 0, 1, 1);


        verticalLayout->addLayout(gridLayout);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        retranslateUi(extStatsDialog);

        QMetaObject::connectSlotsByName(extStatsDialog);
    } // setupUi

    void retranslateUi(QDialog *extStatsDialog)
    {
        extStatsDialog->setWindowTitle(QApplication::translate("extStatsDialog", "Dialog", 0));
        label_title->setText(QString());
        label_date->setText(QString());
        label_score_calc->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_score_bias->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_score->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_sliderval->setText(QApplication::translate("extStatsDialog", "60", 0));
        label_Fmax_FB->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_F_FB->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Fscore_FB->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Fscore_FL->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_F_FL->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Fmax_FL->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Fmax_PX->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Fmin_FL->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Vscore_FL->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Year_PX->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_V_FL->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Vmax_FL->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Vmin_FL->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Year_FL->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_36->setText(QApplication::translate("extStatsDialog", "FL", 0));
        label_Vmin_PX->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_37->setText(QApplication::translate("extStatsDialog", "PX", 0));
        label_Fmin_PX->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Vscore_PX->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_V_PX->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Vmax_PX->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_10->setText(QApplication::translate("extStatsDialog", "5P", 0));
        label_Fscore_GP->setText(QApplication::translate("extStatsDialog", "-", 0));
        progressBar_5P->setFormat(QApplication::translate("extStatsDialog", "%p", 0));
        label_Vmin_GP->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Vscore_5P->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Fmin_5P->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Fmax_5P->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_F_5P->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Year_IG->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_F_IG->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Vmin_IG->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_V_IG->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Vmax_IG->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Fscore_PX->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_F_PX->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_6->setText(QApplication::translate("extStatsDialog", "favs min", 0));
        label_5->setText(QApplication::translate("extStatsDialog", "views score", 0));
        label_Vmin_DA->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Vmax_5P->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Vmin_5P->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_F_DA->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Fscore_DA->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Year_5P->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_21->setText(QApplication::translate("extStatsDialog", "date", 0));
        label_22->setText(QApplication::translate("extStatsDialog", "G+", 0));
        label_Year_DA->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Fmin_DA->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_2->setText(QApplication::translate("extStatsDialog", "views min", 0));
        label_Vmax_DA->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_4->setText(QApplication::translate("extStatsDialog", "views max", 0));
        label_8->setText(QApplication::translate("extStatsDialog", "favs", 0));
        label_V_DA->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_9->setText(QApplication::translate("extStatsDialog", "favs score", 0));
        label_Vscore_DA->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_7->setText(QApplication::translate("extStatsDialog", "favs max", 0));
        progressBar_FL->setFormat(QApplication::translate("extStatsDialog", "%p", 0));
        progressBar_PX->setFormat(QApplication::translate("extStatsDialog", "%p", 0));
        label_35->setText(QApplication::translate("extStatsDialog", "FB", 0));
        label_Year_FB->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Vmin_FB->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Vmax_FB->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_V_FB->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Vscore_FB->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Fmax_DA->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_3->setText(QApplication::translate("extStatsDialog", "views", 0));
        label->setText(QApplication::translate("extStatsDialog", "DA", 0));
        progressBar_DA->setFormat(QApplication::translate("extStatsDialog", "%p", 0));
        label_Fscore_5P->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_V_5P->setText(QApplication::translate("extStatsDialog", "-", 0));
        progressBar_GP->setFormat(QApplication::translate("extStatsDialog", "%p", 0));
        label_Fmin_GP->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Vscore_GP->setText(QApplication::translate("extStatsDialog", "-", 0));
        progressBar_FB->setFormat(QApplication::translate("extStatsDialog", "%p", 0));
        label_Vmax_GP->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Fmax_GP->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_F_GP->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_V_GP->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Year_GP->setText(QApplication::translate("extStatsDialog", "-", 0));
        progressBar_YP->setFormat(QApplication::translate("extStatsDialog", "%p", 0));
        label_38->setText(QApplication::translate("extStatsDialog", "YP", 0));
        label_Year_YP->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Vmin_YP->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Vmax_YP->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_V_YP->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Vscore_YP->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Fmin_YP->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Fmax_YP->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_F_YP->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Fscore_YP->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Fmin_FB->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Fmax_IG->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Fscore_IG->setText(QApplication::translate("extStatsDialog", "-", 0));
        progressBar_IG->setFormat(QApplication::translate("extStatsDialog", "%p", 0));
        label_Vscore_IG->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_Fmin_IG->setText(QApplication::translate("extStatsDialog", "-", 0));
        label_39->setText(QApplication::translate("extStatsDialog", "IG", 0));
    } // retranslateUi

};

namespace Ui {
    class extStatsDialog: public Ui_extStatsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EXTSTATSDIALOG_H
