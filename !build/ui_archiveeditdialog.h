/********************************************************************************
** Form generated from reading UI file 'archiveeditdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ARCHIVEEDITDIALOG_H
#define UI_ARCHIVEEDITDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_archiveEditDialog
{
public:
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_Save;
    QPushButton *pushButton_Cancel;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QLabel *label;
    QLabel *label_2;

    void setupUi(QDialog *archiveEditDialog)
    {
        if (archiveEditDialog->objectName().isEmpty())
            archiveEditDialog->setObjectName(QStringLiteral("archiveEditDialog"));
        archiveEditDialog->resize(533, 315);
        horizontalLayoutWidget = new QWidget(archiveEditDialog);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(70, 190, 381, 51));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton_Save = new QPushButton(horizontalLayoutWidget);
        pushButton_Save->setObjectName(QStringLiteral("pushButton_Save"));

        horizontalLayout->addWidget(pushButton_Save);

        pushButton_Cancel = new QPushButton(horizontalLayoutWidget);
        pushButton_Cancel->setObjectName(QStringLiteral("pushButton_Cancel"));

        horizontalLayout->addWidget(pushButton_Cancel);

        gridLayoutWidget = new QWidget(archiveEditDialog);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(90, 10, 331, 151));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(gridLayoutWidget);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        label_2 = new QLabel(gridLayoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);


        retranslateUi(archiveEditDialog);

        QMetaObject::connectSlotsByName(archiveEditDialog);
    } // setupUi

    void retranslateUi(QDialog *archiveEditDialog)
    {
        archiveEditDialog->setWindowTitle(QApplication::translate("archiveEditDialog", "Dialog", 0));
        pushButton_Save->setText(QApplication::translate("archiveEditDialog", "Save", 0));
        pushButton_Cancel->setText(QApplication::translate("archiveEditDialog", "Cancel", 0));
        label->setText(QApplication::translate("archiveEditDialog", "Title:", 0));
        label_2->setText(QApplication::translate("archiveEditDialog", "Folder:", 0));
    } // retranslateUi

};

namespace Ui {
    class archiveEditDialog: public Ui_archiveEditDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ARCHIVEEDITDIALOG_H
