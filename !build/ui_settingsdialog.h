/********************************************************************************
** Form generated from reading UI file 'settingsdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SETTINGSDIALOG_H
#define UI_SETTINGSDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_settingsDialog
{
public:
    QVBoxLayout *verticalLayout;
    QTabWidget *tabWidget_settings;
    QWidget *tab_6;
    QVBoxLayout *verticalLayout_6;
    QGridLayout *gridLayout_5;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label_16;
    QComboBox *comboBox_scoringMode;
    QLabel *label_17;
    QComboBox *comboBox_browser;
    QSpacerItem *verticalSpacer_5;
    QWidget *tab;
    QVBoxLayout *verticalLayout_2;
    QGridLayout *gridLayout;
    QPushButton *pushButton_photoShop;
    QLineEdit *lineEdit_ptGui;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *lineEdit_photoShop;
    QLineEdit *lineEdit_photoMatix;
    QPushButton *pushButton_photoMatix;
    QLabel *label_4;
    QPushButton *pushButton_ptGui;
    QSpacerItem *verticalSpacer;
    QWidget *tab_2;
    QVBoxLayout *verticalLayout_3;
    QGridLayout *gridLayout_2;
    QLabel *label_3;
    QLineEdit *lineEdit_archivePath;
    QPushButton *pushButton_archivePath;
    QSpacerItem *verticalSpacer_2;
    QWidget *tab_3;
    QVBoxLayout *verticalLayout_4;
    QGridLayout *gridLayout_3;
    QLabel *label_6;
    QLabel *label_5;
    QLabel *label_7;
    QLineEdit *lineEdit_URL_500px;
    QLineEdit *lineEdit_password_500px;
    QLineEdit *lineEdit_username_500px;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_10;
    QLabel *label_11;
    QLineEdit *lineEdit_URL_Deviantart;
    QLineEdit *lineEdit_username_Deviantart;
    QLineEdit *lineEdit_password_deviantart;
    QSpacerItem *verticalSpacer_3;
    QWidget *tab_4;
    QVBoxLayout *verticalLayout_5;
    QGridLayout *gridLayout_4;
    QLabel *label_13;
    QLineEdit *lineEdit_syncIP;
    QLabel *label_12;
    QLineEdit *lineEdit_syncDB;
    QLineEdit *lineEdit_syncUSR;
    QLineEdit *lineEdit_syncPASS;
    QLabel *label_14;
    QLabel *label_15;
    QSpacerItem *verticalSpacer_4;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_save;
    QPushButton *pushButton_cancel;

    void setupUi(QDialog *settingsDialog)
    {
        if (settingsDialog->objectName().isEmpty())
            settingsDialog->setObjectName(QStringLiteral("settingsDialog"));
        settingsDialog->resize(692, 286);
        verticalLayout = new QVBoxLayout(settingsDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        tabWidget_settings = new QTabWidget(settingsDialog);
        tabWidget_settings->setObjectName(QStringLiteral("tabWidget_settings"));
        tab_6 = new QWidget();
        tab_6->setObjectName(QStringLiteral("tab_6"));
        verticalLayout_6 = new QVBoxLayout(tab_6);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        gridLayout_5 = new QGridLayout();
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_5->addItem(horizontalSpacer_2, 0, 2, 1, 1);

        label_16 = new QLabel(tab_6);
        label_16->setObjectName(QStringLiteral("label_16"));

        gridLayout_5->addWidget(label_16, 0, 0, 1, 1);

        comboBox_scoringMode = new QComboBox(tab_6);
        comboBox_scoringMode->setObjectName(QStringLiteral("comboBox_scoringMode"));
        comboBox_scoringMode->setMinimumSize(QSize(250, 0));

        gridLayout_5->addWidget(comboBox_scoringMode, 0, 1, 1, 1);

        label_17 = new QLabel(tab_6);
        label_17->setObjectName(QStringLiteral("label_17"));

        gridLayout_5->addWidget(label_17, 1, 0, 1, 1);

        comboBox_browser = new QComboBox(tab_6);
        comboBox_browser->setObjectName(QStringLiteral("comboBox_browser"));

        gridLayout_5->addWidget(comboBox_browser, 1, 1, 1, 1);


        verticalLayout_6->addLayout(gridLayout_5);

        verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_6->addItem(verticalSpacer_5);

        tabWidget_settings->addTab(tab_6, QString());
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        verticalLayout_2 = new QVBoxLayout(tab);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        pushButton_photoShop = new QPushButton(tab);
        pushButton_photoShop->setObjectName(QStringLiteral("pushButton_photoShop"));
        pushButton_photoShop->setMaximumSize(QSize(50, 16777215));

        gridLayout->addWidget(pushButton_photoShop, 2, 2, 1, 1);

        lineEdit_ptGui = new QLineEdit(tab);
        lineEdit_ptGui->setObjectName(QStringLiteral("lineEdit_ptGui"));

        gridLayout->addWidget(lineEdit_ptGui, 1, 1, 1, 1);

        label = new QLabel(tab);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        label_2 = new QLabel(tab);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        lineEdit_photoShop = new QLineEdit(tab);
        lineEdit_photoShop->setObjectName(QStringLiteral("lineEdit_photoShop"));

        gridLayout->addWidget(lineEdit_photoShop, 2, 1, 1, 1);

        lineEdit_photoMatix = new QLineEdit(tab);
        lineEdit_photoMatix->setObjectName(QStringLiteral("lineEdit_photoMatix"));

        gridLayout->addWidget(lineEdit_photoMatix, 0, 1, 1, 1);

        pushButton_photoMatix = new QPushButton(tab);
        pushButton_photoMatix->setObjectName(QStringLiteral("pushButton_photoMatix"));
        pushButton_photoMatix->setMaximumSize(QSize(50, 16777215));

        gridLayout->addWidget(pushButton_photoMatix, 0, 2, 1, 1);

        label_4 = new QLabel(tab);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout->addWidget(label_4, 2, 0, 1, 1);

        pushButton_ptGui = new QPushButton(tab);
        pushButton_ptGui->setObjectName(QStringLiteral("pushButton_ptGui"));
        pushButton_ptGui->setMaximumSize(QSize(50, 16777215));

        gridLayout->addWidget(pushButton_ptGui, 1, 2, 1, 1);


        verticalLayout_2->addLayout(gridLayout);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);

        tabWidget_settings->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        verticalLayout_3 = new QVBoxLayout(tab_2);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        label_3 = new QLabel(tab_2);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout_2->addWidget(label_3, 0, 0, 1, 1);

        lineEdit_archivePath = new QLineEdit(tab_2);
        lineEdit_archivePath->setObjectName(QStringLiteral("lineEdit_archivePath"));

        gridLayout_2->addWidget(lineEdit_archivePath, 0, 1, 1, 1);

        pushButton_archivePath = new QPushButton(tab_2);
        pushButton_archivePath->setObjectName(QStringLiteral("pushButton_archivePath"));
        pushButton_archivePath->setMaximumSize(QSize(50, 16777215));

        gridLayout_2->addWidget(pushButton_archivePath, 0, 2, 1, 1);


        verticalLayout_3->addLayout(gridLayout_2);

        verticalSpacer_2 = new QSpacerItem(20, 84, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_2);

        tabWidget_settings->addTab(tab_2, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QStringLiteral("tab_3"));
        verticalLayout_4 = new QVBoxLayout(tab_3);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        label_6 = new QLabel(tab_3);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout_3->addWidget(label_6, 2, 1, 1, 1);

        label_5 = new QLabel(tab_3);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout_3->addWidget(label_5, 1, 1, 1, 1);

        label_7 = new QLabel(tab_3);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setMaximumSize(QSize(16, 16));
        label_7->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/siteIcon500px.png")));
        label_7->setScaledContents(true);

        gridLayout_3->addWidget(label_7, 1, 0, 1, 1);

        lineEdit_URL_500px = new QLineEdit(tab_3);
        lineEdit_URL_500px->setObjectName(QStringLiteral("lineEdit_URL_500px"));

        gridLayout_3->addWidget(lineEdit_URL_500px, 1, 2, 1, 1);

        lineEdit_password_500px = new QLineEdit(tab_3);
        lineEdit_password_500px->setObjectName(QStringLiteral("lineEdit_password_500px"));
        lineEdit_password_500px->setMaximumSize(QSize(100, 16777215));

        gridLayout_3->addWidget(lineEdit_password_500px, 1, 4, 1, 1);

        lineEdit_username_500px = new QLineEdit(tab_3);
        lineEdit_username_500px->setObjectName(QStringLiteral("lineEdit_username_500px"));
        lineEdit_username_500px->setMaximumSize(QSize(100, 16777215));

        gridLayout_3->addWidget(lineEdit_username_500px, 1, 3, 1, 1);

        label_8 = new QLabel(tab_3);
        label_8->setObjectName(QStringLiteral("label_8"));

        gridLayout_3->addWidget(label_8, 0, 2, 1, 1);

        label_9 = new QLabel(tab_3);
        label_9->setObjectName(QStringLiteral("label_9"));

        gridLayout_3->addWidget(label_9, 0, 3, 1, 1);

        label_10 = new QLabel(tab_3);
        label_10->setObjectName(QStringLiteral("label_10"));

        gridLayout_3->addWidget(label_10, 0, 4, 1, 1);

        label_11 = new QLabel(tab_3);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setMaximumSize(QSize(16, 16));
        label_11->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/siteIconDeviantArt.png")));
        label_11->setScaledContents(true);

        gridLayout_3->addWidget(label_11, 2, 0, 1, 1);

        lineEdit_URL_Deviantart = new QLineEdit(tab_3);
        lineEdit_URL_Deviantart->setObjectName(QStringLiteral("lineEdit_URL_Deviantart"));

        gridLayout_3->addWidget(lineEdit_URL_Deviantart, 2, 2, 1, 1);

        lineEdit_username_Deviantart = new QLineEdit(tab_3);
        lineEdit_username_Deviantart->setObjectName(QStringLiteral("lineEdit_username_Deviantart"));
        lineEdit_username_Deviantart->setMaximumSize(QSize(100, 16777215));

        gridLayout_3->addWidget(lineEdit_username_Deviantart, 2, 3, 1, 1);

        lineEdit_password_deviantart = new QLineEdit(tab_3);
        lineEdit_password_deviantart->setObjectName(QStringLiteral("lineEdit_password_deviantart"));
        lineEdit_password_deviantart->setMaximumSize(QSize(100, 16777215));

        gridLayout_3->addWidget(lineEdit_password_deviantart, 2, 4, 1, 1);


        verticalLayout_4->addLayout(gridLayout_3);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer_3);

        tabWidget_settings->addTab(tab_3, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QStringLiteral("tab_4"));
        verticalLayout_5 = new QVBoxLayout(tab_4);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        gridLayout_4 = new QGridLayout();
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        label_13 = new QLabel(tab_4);
        label_13->setObjectName(QStringLiteral("label_13"));

        gridLayout_4->addWidget(label_13, 1, 0, 1, 1);

        lineEdit_syncIP = new QLineEdit(tab_4);
        lineEdit_syncIP->setObjectName(QStringLiteral("lineEdit_syncIP"));

        gridLayout_4->addWidget(lineEdit_syncIP, 0, 1, 1, 1);

        label_12 = new QLabel(tab_4);
        label_12->setObjectName(QStringLiteral("label_12"));

        gridLayout_4->addWidget(label_12, 0, 0, 1, 1);

        lineEdit_syncDB = new QLineEdit(tab_4);
        lineEdit_syncDB->setObjectName(QStringLiteral("lineEdit_syncDB"));

        gridLayout_4->addWidget(lineEdit_syncDB, 1, 1, 1, 1);

        lineEdit_syncUSR = new QLineEdit(tab_4);
        lineEdit_syncUSR->setObjectName(QStringLiteral("lineEdit_syncUSR"));

        gridLayout_4->addWidget(lineEdit_syncUSR, 2, 1, 1, 1);

        lineEdit_syncPASS = new QLineEdit(tab_4);
        lineEdit_syncPASS->setObjectName(QStringLiteral("lineEdit_syncPASS"));

        gridLayout_4->addWidget(lineEdit_syncPASS, 3, 1, 1, 1);

        label_14 = new QLabel(tab_4);
        label_14->setObjectName(QStringLiteral("label_14"));

        gridLayout_4->addWidget(label_14, 2, 0, 1, 1);

        label_15 = new QLabel(tab_4);
        label_15->setObjectName(QStringLiteral("label_15"));

        gridLayout_4->addWidget(label_15, 3, 0, 1, 1);


        verticalLayout_5->addLayout(gridLayout_4);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacer_4);

        tabWidget_settings->addTab(tab_4, QString());

        verticalLayout->addWidget(tabWidget_settings);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(-1, 0, -1, -1);
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton_save = new QPushButton(settingsDialog);
        pushButton_save->setObjectName(QStringLiteral("pushButton_save"));
        pushButton_save->setMinimumSize(QSize(100, 25));

        horizontalLayout->addWidget(pushButton_save);

        pushButton_cancel = new QPushButton(settingsDialog);
        pushButton_cancel->setObjectName(QStringLiteral("pushButton_cancel"));
        pushButton_cancel->setMinimumSize(QSize(100, 25));

        horizontalLayout->addWidget(pushButton_cancel);


        verticalLayout->addLayout(horizontalLayout);

        QWidget::setTabOrder(tabWidget_settings, lineEdit_photoMatix);
        QWidget::setTabOrder(lineEdit_photoMatix, pushButton_photoMatix);
        QWidget::setTabOrder(pushButton_photoMatix, lineEdit_ptGui);
        QWidget::setTabOrder(lineEdit_ptGui, pushButton_ptGui);
        QWidget::setTabOrder(pushButton_ptGui, lineEdit_photoShop);
        QWidget::setTabOrder(lineEdit_photoShop, pushButton_photoShop);
        QWidget::setTabOrder(pushButton_photoShop, lineEdit_archivePath);
        QWidget::setTabOrder(lineEdit_archivePath, pushButton_archivePath);
        QWidget::setTabOrder(pushButton_archivePath, lineEdit_URL_500px);
        QWidget::setTabOrder(lineEdit_URL_500px, lineEdit_username_500px);
        QWidget::setTabOrder(lineEdit_username_500px, lineEdit_password_500px);
        QWidget::setTabOrder(lineEdit_password_500px, lineEdit_URL_Deviantart);
        QWidget::setTabOrder(lineEdit_URL_Deviantart, lineEdit_username_Deviantart);
        QWidget::setTabOrder(lineEdit_username_Deviantart, lineEdit_password_deviantart);
        QWidget::setTabOrder(lineEdit_password_deviantart, pushButton_save);
        QWidget::setTabOrder(pushButton_save, pushButton_cancel);

        retranslateUi(settingsDialog);

        tabWidget_settings->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(settingsDialog);
    } // setupUi

    void retranslateUi(QDialog *settingsDialog)
    {
        settingsDialog->setWindowTitle(QApplication::translate("settingsDialog", "Dialog", 0));
        label_16->setText(QApplication::translate("settingsDialog", "Scoring mode    ", 0));
        comboBox_scoringMode->clear();
        comboBox_scoringMode->insertItems(0, QStringList()
         << QApplication::translate("settingsDialog", "Linear", 0)
         << QApplication::translate("settingsDialog", "Square root", 0)
         << QApplication::translate("settingsDialog", "Yearly weighted + square root", 0)
        );
        label_17->setText(QApplication::translate("settingsDialog", "Open URLs in:", 0));
        comboBox_browser->clear();
        comboBox_browser->insertItems(0, QStringList()
         << QApplication::translate("settingsDialog", "Default browser", 0)
         << QApplication::translate("settingsDialog", "Internal browser", 0)
        );
        tabWidget_settings->setTabText(tabWidget_settings->indexOf(tab_6), QApplication::translate("settingsDialog", "Settings", 0));
        pushButton_photoShop->setText(QApplication::translate("settingsDialog", ">>", 0));
        label->setText(QApplication::translate("settingsDialog", "PhotoMatix", 0));
        label_2->setText(QApplication::translate("settingsDialog", "PTGui", 0));
        pushButton_photoMatix->setText(QApplication::translate("settingsDialog", ">>", 0));
        label_4->setText(QApplication::translate("settingsDialog", "Photoshop", 0));
        pushButton_ptGui->setText(QApplication::translate("settingsDialog", ">>", 0));
        tabWidget_settings->setTabText(tabWidget_settings->indexOf(tab), QApplication::translate("settingsDialog", "Programs", 0));
        label_3->setText(QApplication::translate("settingsDialog", "Default archive path", 0));
        pushButton_archivePath->setText(QApplication::translate("settingsDialog", ">>", 0));
        tabWidget_settings->setTabText(tabWidget_settings->indexOf(tab_2), QApplication::translate("settingsDialog", "Paths", 0));
        label_6->setText(QApplication::translate("settingsDialog", "DeviantArt", 0));
        label_5->setText(QApplication::translate("settingsDialog", "500px", 0));
        label_7->setText(QString());
        label_8->setText(QApplication::translate("settingsDialog", "Profile URL", 0));
        label_9->setText(QApplication::translate("settingsDialog", "Username", 0));
        label_10->setText(QApplication::translate("settingsDialog", "Password", 0));
        label_11->setText(QString());
        tabWidget_settings->setTabText(tabWidget_settings->indexOf(tab_3), QApplication::translate("settingsDialog", "Sites", 0));
        label_13->setText(QApplication::translate("settingsDialog", "Database name", 0));
        label_12->setText(QApplication::translate("settingsDialog", "Server IP", 0));
        label_14->setText(QApplication::translate("settingsDialog", "username", 0));
        label_15->setText(QApplication::translate("settingsDialog", "password", 0));
        tabWidget_settings->setTabText(tabWidget_settings->indexOf(tab_4), QApplication::translate("settingsDialog", "Sync", 0));
        pushButton_save->setText(QApplication::translate("settingsDialog", "Save", 0));
        pushButton_cancel->setText(QApplication::translate("settingsDialog", "Cancel", 0));
    } // retranslateUi

};

namespace Ui {
    class settingsDialog: public Ui_settingsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SETTINGSDIALOG_H
