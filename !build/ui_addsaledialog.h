/********************************************************************************
** Form generated from reading UI file 'addsaledialog.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADDSALEDIALOG_H
#define UI_ADDSALEDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_addSaleDialog
{
public:
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QLabel *label_7;
    QLabel *label_13;
    QLabel *label_12;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_10;
    QComboBox *comboBox_SALETYPE;
    QLabel *label_11;
    QLineEdit *lineEdit_TITLE;
    QLineEdit *lineEdit_ID;
    QComboBox *comboBox_EXCLUSIVE;
    QLineEdit *lineEdit_SALEDATE;
    QLineEdit *lineEdit_URL;
    QLineEdit *lineEdit_CURRENCY;
    QLineEdit *lineEdit_TOTALPRICE;
    QComboBox *comboBox_SITE;
    QLineEdit *lineEdit_AMOUNT;
    QLineEdit *lineEdit_CONTACT;
    QLineEdit *lineEdit_BUYER;
    QLineEdit *lineEdit_TRANSACTIONID;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_14;
    QComboBox *comboBox_LICENSETYPE;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_Save;
    QPushButton *pushButton_Cancel;

    void setupUi(QDialog *addSaleDialog)
    {
        if (addSaleDialog->objectName().isEmpty())
            addSaleDialog->setObjectName(QStringLiteral("addSaleDialog"));
        addSaleDialog->resize(364, 463);
        verticalLayout = new QVBoxLayout(addSaleDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label_7 = new QLabel(addSaleDialog);
        label_7->setObjectName(QStringLiteral("label_7"));

        gridLayout->addWidget(label_7, 6, 0, 1, 1);

        label_13 = new QLabel(addSaleDialog);
        label_13->setObjectName(QStringLiteral("label_13"));

        gridLayout->addWidget(label_13, 13, 0, 1, 1);

        label_12 = new QLabel(addSaleDialog);
        label_12->setObjectName(QStringLiteral("label_12"));

        gridLayout->addWidget(label_12, 12, 0, 1, 1);

        label_8 = new QLabel(addSaleDialog);
        label_8->setObjectName(QStringLiteral("label_8"));

        gridLayout->addWidget(label_8, 8, 0, 1, 1);

        label_9 = new QLabel(addSaleDialog);
        label_9->setObjectName(QStringLiteral("label_9"));

        gridLayout->addWidget(label_9, 9, 0, 1, 1);

        label_10 = new QLabel(addSaleDialog);
        label_10->setObjectName(QStringLiteral("label_10"));

        gridLayout->addWidget(label_10, 10, 0, 1, 1);

        comboBox_SALETYPE = new QComboBox(addSaleDialog);
        comboBox_SALETYPE->setObjectName(QStringLiteral("comboBox_SALETYPE"));

        gridLayout->addWidget(comboBox_SALETYPE, 3, 1, 1, 1);

        label_11 = new QLabel(addSaleDialog);
        label_11->setObjectName(QStringLiteral("label_11"));

        gridLayout->addWidget(label_11, 11, 0, 1, 1);

        lineEdit_TITLE = new QLineEdit(addSaleDialog);
        lineEdit_TITLE->setObjectName(QStringLiteral("lineEdit_TITLE"));

        gridLayout->addWidget(lineEdit_TITLE, 1, 1, 1, 1);

        lineEdit_ID = new QLineEdit(addSaleDialog);
        lineEdit_ID->setObjectName(QStringLiteral("lineEdit_ID"));

        gridLayout->addWidget(lineEdit_ID, 0, 1, 1, 1);

        comboBox_EXCLUSIVE = new QComboBox(addSaleDialog);
        comboBox_EXCLUSIVE->setObjectName(QStringLiteral("comboBox_EXCLUSIVE"));

        gridLayout->addWidget(comboBox_EXCLUSIVE, 10, 1, 1, 1);

        lineEdit_SALEDATE = new QLineEdit(addSaleDialog);
        lineEdit_SALEDATE->setObjectName(QStringLiteral("lineEdit_SALEDATE"));

        gridLayout->addWidget(lineEdit_SALEDATE, 2, 1, 1, 1);

        lineEdit_URL = new QLineEdit(addSaleDialog);
        lineEdit_URL->setObjectName(QStringLiteral("lineEdit_URL"));

        gridLayout->addWidget(lineEdit_URL, 9, 1, 1, 1);

        lineEdit_CURRENCY = new QLineEdit(addSaleDialog);
        lineEdit_CURRENCY->setObjectName(QStringLiteral("lineEdit_CURRENCY"));

        gridLayout->addWidget(lineEdit_CURRENCY, 7, 1, 1, 1);

        lineEdit_TOTALPRICE = new QLineEdit(addSaleDialog);
        lineEdit_TOTALPRICE->setObjectName(QStringLiteral("lineEdit_TOTALPRICE"));

        gridLayout->addWidget(lineEdit_TOTALPRICE, 6, 1, 1, 1);

        comboBox_SITE = new QComboBox(addSaleDialog);
        comboBox_SITE->setObjectName(QStringLiteral("comboBox_SITE"));

        gridLayout->addWidget(comboBox_SITE, 8, 1, 1, 1);

        lineEdit_AMOUNT = new QLineEdit(addSaleDialog);
        lineEdit_AMOUNT->setObjectName(QStringLiteral("lineEdit_AMOUNT"));

        gridLayout->addWidget(lineEdit_AMOUNT, 5, 1, 1, 1);

        lineEdit_CONTACT = new QLineEdit(addSaleDialog);
        lineEdit_CONTACT->setObjectName(QStringLiteral("lineEdit_CONTACT"));

        gridLayout->addWidget(lineEdit_CONTACT, 13, 1, 1, 1);

        lineEdit_BUYER = new QLineEdit(addSaleDialog);
        lineEdit_BUYER->setObjectName(QStringLiteral("lineEdit_BUYER"));

        gridLayout->addWidget(lineEdit_BUYER, 12, 1, 1, 1);

        lineEdit_TRANSACTIONID = new QLineEdit(addSaleDialog);
        lineEdit_TRANSACTIONID->setObjectName(QStringLiteral("lineEdit_TRANSACTIONID"));

        gridLayout->addWidget(lineEdit_TRANSACTIONID, 11, 1, 1, 1);

        label = new QLabel(addSaleDialog);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        label_2 = new QLabel(addSaleDialog);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        label_3 = new QLabel(addSaleDialog);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        label_4 = new QLabel(addSaleDialog);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout->addWidget(label_4, 3, 0, 1, 1);

        label_5 = new QLabel(addSaleDialog);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout->addWidget(label_5, 5, 0, 1, 1);

        label_6 = new QLabel(addSaleDialog);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout->addWidget(label_6, 7, 0, 1, 1);

        label_14 = new QLabel(addSaleDialog);
        label_14->setObjectName(QStringLiteral("label_14"));

        gridLayout->addWidget(label_14, 4, 0, 1, 1);

        comboBox_LICENSETYPE = new QComboBox(addSaleDialog);
        comboBox_LICENSETYPE->setObjectName(QStringLiteral("comboBox_LICENSETYPE"));

        gridLayout->addWidget(comboBox_LICENSETYPE, 4, 1, 1, 1);


        verticalLayout->addLayout(gridLayout);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton_Save = new QPushButton(addSaleDialog);
        pushButton_Save->setObjectName(QStringLiteral("pushButton_Save"));
        pushButton_Save->setDefault(false);

        horizontalLayout->addWidget(pushButton_Save);

        pushButton_Cancel = new QPushButton(addSaleDialog);
        pushButton_Cancel->setObjectName(QStringLiteral("pushButton_Cancel"));
        pushButton_Cancel->setAutoDefault(false);

        horizontalLayout->addWidget(pushButton_Cancel);


        verticalLayout->addLayout(horizontalLayout);

        QWidget::setTabOrder(lineEdit_ID, lineEdit_TITLE);
        QWidget::setTabOrder(lineEdit_TITLE, lineEdit_SALEDATE);
        QWidget::setTabOrder(lineEdit_SALEDATE, comboBox_SALETYPE);
        QWidget::setTabOrder(comboBox_SALETYPE, comboBox_LICENSETYPE);
        QWidget::setTabOrder(comboBox_LICENSETYPE, lineEdit_AMOUNT);
        QWidget::setTabOrder(lineEdit_AMOUNT, lineEdit_TOTALPRICE);
        QWidget::setTabOrder(lineEdit_TOTALPRICE, lineEdit_CURRENCY);
        QWidget::setTabOrder(lineEdit_CURRENCY, comboBox_SITE);
        QWidget::setTabOrder(comboBox_SITE, lineEdit_URL);
        QWidget::setTabOrder(lineEdit_URL, comboBox_EXCLUSIVE);
        QWidget::setTabOrder(comboBox_EXCLUSIVE, lineEdit_TRANSACTIONID);
        QWidget::setTabOrder(lineEdit_TRANSACTIONID, lineEdit_BUYER);
        QWidget::setTabOrder(lineEdit_BUYER, lineEdit_CONTACT);
        QWidget::setTabOrder(lineEdit_CONTACT, pushButton_Cancel);
        QWidget::setTabOrder(pushButton_Cancel, pushButton_Save);

        retranslateUi(addSaleDialog);

        QMetaObject::connectSlotsByName(addSaleDialog);
    } // setupUi

    void retranslateUi(QDialog *addSaleDialog)
    {
        addSaleDialog->setWindowTitle(QApplication::translate("addSaleDialog", "Add Sale", 0));
        label_7->setText(QApplication::translate("addSaleDialog", "Total Price", 0));
        label_13->setText(QApplication::translate("addSaleDialog", "Contact", 0));
        label_12->setText(QApplication::translate("addSaleDialog", "Buyer", 0));
        label_8->setText(QApplication::translate("addSaleDialog", "Site", 0));
        label_9->setText(QApplication::translate("addSaleDialog", "URL", 0));
        label_10->setText(QApplication::translate("addSaleDialog", "Exclusive", 0));
        comboBox_SALETYPE->clear();
        comboBox_SALETYPE->insertItems(0, QStringList()
         << QApplication::translate("addSaleDialog", "Print", 0)
         << QApplication::translate("addSaleDialog", "License", 0)
        );
        label_11->setText(QApplication::translate("addSaleDialog", "Transaction ID", 0));
        comboBox_EXCLUSIVE->clear();
        comboBox_EXCLUSIVE->insertItems(0, QStringList()
         << QApplication::translate("addSaleDialog", "No", 0)
         << QApplication::translate("addSaleDialog", "Yes", 0)
        );
        lineEdit_CURRENCY->setText(QApplication::translate("addSaleDialog", "USD", 0));
        comboBox_SITE->clear();
        comboBox_SITE->insertItems(0, QStringList()
         << QApplication::translate("addSaleDialog", "DeviantArt", 0)
         << QApplication::translate("addSaleDialog", "Pixsy", 0)
         << QApplication::translate("addSaleDialog", "500px", 0)
         << QApplication::translate("addSaleDialog", "500px prime", 0)
         << QApplication::translate("addSaleDialog", "Fotomoto", 0)
         << QApplication::translate("addSaleDialog", "Fine Art America", 0)
         << QApplication::translate("addSaleDialog", "BlueCanvas", 0)
         << QApplication::translate("addSaleDialog", "Artist Rising", 0)
        );
        label->setText(QApplication::translate("addSaleDialog", "ID", 0));
        label_2->setText(QApplication::translate("addSaleDialog", "Title", 0));
        label_3->setText(QApplication::translate("addSaleDialog", "Sale Date", 0));
        label_4->setText(QApplication::translate("addSaleDialog", "Sale Type", 0));
        label_5->setText(QApplication::translate("addSaleDialog", "Amount", 0));
        label_6->setText(QApplication::translate("addSaleDialog", "Currency", 0));
        label_14->setText(QApplication::translate("addSaleDialog", "License Type", 0));
        comboBox_LICENSETYPE->clear();
        comboBox_LICENSETYPE->insertItems(0, QStringList()
         << QString()
         << QApplication::translate("addSaleDialog", "Editorial", 0)
         << QApplication::translate("addSaleDialog", "Web Ready RF", 0)
         << QApplication::translate("addSaleDialog", "Print Ready RF", 0)
         << QApplication::translate("addSaleDialog", "Personal", 0)
         << QApplication::translate("addSaleDialog", "Penalty", 0)
        );
        pushButton_Save->setText(QApplication::translate("addSaleDialog", "Save", 0));
        pushButton_Cancel->setText(QApplication::translate("addSaleDialog", "Cancel", 0));
    } // retranslateUi

};

namespace Ui {
    class addSaleDialog: public Ui_addSaleDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADDSALEDIALOG_H
