/********************************************************************************
** Form generated from reading UI file 'addarchivedialog.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADDARCHIVEDIALOG_H
#define UI_ADDARCHIVEDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTreeView>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_addArchiveDialog
{
public:
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_2;
    QGridLayout *gridLayout;
    QLabel *label;
    QLabel *label_5;
    QLabel *label_3;
    QLineEdit *lineEdit_title;
    QPushButton *pushButton_selectFolder;
    QPushButton *pushButton_calendar;
    QLineEdit *lineEdit_folder;
    QPlainTextEdit *plainTextEdit_kewords;
    QPushButton *pushButton_selectImage;
    QDateEdit *dateEdit;
    QLabel *label_2;
    QLineEdit *lineEdit_image;
    QLabel *label_4;
    QTreeView *treeView;
    QVBoxLayout *verticalLayout;
    QLabel *label_img;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_add;
    QPushButton *pushButton_cancel;

    void setupUi(QDialog *addArchiveDialog)
    {
        if (addArchiveDialog->objectName().isEmpty())
            addArchiveDialog->setObjectName(QStringLiteral("addArchiveDialog"));
        addArchiveDialog->resize(999, 642);
        verticalLayout_2 = new QVBoxLayout(addArchiveDialog);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label = new QLabel(addArchiveDialog);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        label_5 = new QLabel(addArchiveDialog);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        gridLayout->addWidget(label_5, 5, 0, 1, 1);

        label_3 = new QLabel(addArchiveDialog);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout->addWidget(label_3, 1, 0, 1, 1);

        lineEdit_title = new QLineEdit(addArchiveDialog);
        lineEdit_title->setObjectName(QStringLiteral("lineEdit_title"));

        gridLayout->addWidget(lineEdit_title, 0, 1, 1, 1);

        pushButton_selectFolder = new QPushButton(addArchiveDialog);
        pushButton_selectFolder->setObjectName(QStringLiteral("pushButton_selectFolder"));
        pushButton_selectFolder->setMaximumSize(QSize(50, 16777215));

        gridLayout->addWidget(pushButton_selectFolder, 1, 2, 1, 1);

        pushButton_calendar = new QPushButton(addArchiveDialog);
        pushButton_calendar->setObjectName(QStringLiteral("pushButton_calendar"));
        pushButton_calendar->setMaximumSize(QSize(50, 16777215));

        gridLayout->addWidget(pushButton_calendar, 4, 2, 1, 1);

        lineEdit_folder = new QLineEdit(addArchiveDialog);
        lineEdit_folder->setObjectName(QStringLiteral("lineEdit_folder"));

        gridLayout->addWidget(lineEdit_folder, 1, 1, 1, 1);

        plainTextEdit_kewords = new QPlainTextEdit(addArchiveDialog);
        plainTextEdit_kewords->setObjectName(QStringLiteral("plainTextEdit_kewords"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(plainTextEdit_kewords->sizePolicy().hasHeightForWidth());
        plainTextEdit_kewords->setSizePolicy(sizePolicy);
        plainTextEdit_kewords->setMinimumSize(QSize(0, 100));
        plainTextEdit_kewords->setMaximumSize(QSize(16777215, 250));
        plainTextEdit_kewords->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);

        gridLayout->addWidget(plainTextEdit_kewords, 5, 1, 1, 1);

        pushButton_selectImage = new QPushButton(addArchiveDialog);
        pushButton_selectImage->setObjectName(QStringLiteral("pushButton_selectImage"));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(pushButton_selectImage->sizePolicy().hasHeightForWidth());
        pushButton_selectImage->setSizePolicy(sizePolicy1);
        pushButton_selectImage->setMaximumSize(QSize(50, 16777215));

        gridLayout->addWidget(pushButton_selectImage, 2, 2, 1, 1);

        dateEdit = new QDateEdit(addArchiveDialog);
        dateEdit->setObjectName(QStringLiteral("dateEdit"));
        dateEdit->setDateTime(QDateTime(QDate(2006, 1, 1), QTime(0, 0, 0)));

        gridLayout->addWidget(dateEdit, 4, 1, 1, 1);

        label_2 = new QLabel(addArchiveDialog);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 2, 0, 1, 1);

        lineEdit_image = new QLineEdit(addArchiveDialog);
        lineEdit_image->setObjectName(QStringLiteral("lineEdit_image"));

        gridLayout->addWidget(lineEdit_image, 2, 1, 1, 1);

        label_4 = new QLabel(addArchiveDialog);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout->addWidget(label_4, 4, 0, 1, 1);

        treeView = new QTreeView(addArchiveDialog);
        treeView->setObjectName(QStringLiteral("treeView"));
        treeView->setFrameShadow(QFrame::Plain);
        treeView->header()->setVisible(false);

        gridLayout->addWidget(treeView, 6, 1, 1, 1);


        horizontalLayout_2->addLayout(gridLayout);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label_img = new QLabel(addArchiveDialog);
        label_img->setObjectName(QStringLiteral("label_img"));
        label_img->setMinimumSize(QSize(200, 200));
        label_img->setFrameShape(QFrame::NoFrame);
        label_img->setFrameShadow(QFrame::Plain);
        label_img->setLineWidth(0);
        label_img->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_img);


        horizontalLayout_2->addLayout(verticalLayout);

        horizontalLayout_2->setStretch(0, 4);
        horizontalLayout_2->setStretch(1, 5);

        verticalLayout_2->addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton_add = new QPushButton(addArchiveDialog);
        pushButton_add->setObjectName(QStringLiteral("pushButton_add"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(pushButton_add->sizePolicy().hasHeightForWidth());
        pushButton_add->setSizePolicy(sizePolicy2);
        pushButton_add->setMinimumSize(QSize(100, 25));
        QFont font;
        font.setItalic(false);
        pushButton_add->setFont(font);

        horizontalLayout->addWidget(pushButton_add);

        pushButton_cancel = new QPushButton(addArchiveDialog);
        pushButton_cancel->setObjectName(QStringLiteral("pushButton_cancel"));
        pushButton_cancel->setMinimumSize(QSize(100, 25));

        horizontalLayout->addWidget(pushButton_cancel);


        verticalLayout_2->addLayout(horizontalLayout);

        QWidget::setTabOrder(lineEdit_title, lineEdit_folder);
        QWidget::setTabOrder(lineEdit_folder, pushButton_selectFolder);
        QWidget::setTabOrder(pushButton_selectFolder, lineEdit_image);
        QWidget::setTabOrder(lineEdit_image, pushButton_selectImage);
        QWidget::setTabOrder(pushButton_selectImage, dateEdit);
        QWidget::setTabOrder(dateEdit, plainTextEdit_kewords);
        QWidget::setTabOrder(plainTextEdit_kewords, pushButton_add);
        QWidget::setTabOrder(pushButton_add, pushButton_cancel);
        QWidget::setTabOrder(pushButton_cancel, treeView);
        QWidget::setTabOrder(treeView, pushButton_calendar);

        retranslateUi(addArchiveDialog);

        QMetaObject::connectSlotsByName(addArchiveDialog);
    } // setupUi

    void retranslateUi(QDialog *addArchiveDialog)
    {
        addArchiveDialog->setWindowTitle(QApplication::translate("addArchiveDialog", "Dialog", 0));
        label->setText(QApplication::translate("addArchiveDialog", "Title", 0));
        label_5->setText(QApplication::translate("addArchiveDialog", "Keywords", 0));
        label_3->setText(QApplication::translate("addArchiveDialog", "Folder", 0));
        pushButton_selectFolder->setText(QApplication::translate("addArchiveDialog", "+", 0));
        pushButton_calendar->setText(QApplication::translate("addArchiveDialog", ">>", 0));
        pushButton_selectImage->setText(QApplication::translate("addArchiveDialog", "+", 0));
        label_2->setText(QApplication::translate("addArchiveDialog", "Preview Image", 0));
        label_4->setText(QApplication::translate("addArchiveDialog", "Date", 0));
        label_img->setText(QString());
        pushButton_add->setText(QApplication::translate("addArchiveDialog", "Add", 0));
        pushButton_cancel->setText(QApplication::translate("addArchiveDialog", "Cancel", 0));
    } // retranslateUi

};

namespace Ui {
    class addArchiveDialog: public Ui_addArchiveDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADDARCHIVEDIALOG_H
