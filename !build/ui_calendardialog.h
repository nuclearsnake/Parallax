/********************************************************************************
** Form generated from reading UI file 'calendardialog.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CALENDARDIALOG_H
#define UI_CALENDARDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCalendarWidget>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_calendarDialog
{
public:
    QVBoxLayout *verticalLayout;
    QCalendarWidget *calendarWidget;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_select;
    QPushButton *pushButton_cancel;

    void setupUi(QDialog *calendarDialog)
    {
        if (calendarDialog->objectName().isEmpty())
            calendarDialog->setObjectName(QStringLiteral("calendarDialog"));
        calendarDialog->resize(341, 284);
        verticalLayout = new QVBoxLayout(calendarDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        calendarWidget = new QCalendarWidget(calendarDialog);
        calendarWidget->setObjectName(QStringLiteral("calendarWidget"));

        verticalLayout->addWidget(calendarWidget);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton_select = new QPushButton(calendarDialog);
        pushButton_select->setObjectName(QStringLiteral("pushButton_select"));

        horizontalLayout->addWidget(pushButton_select);

        pushButton_cancel = new QPushButton(calendarDialog);
        pushButton_cancel->setObjectName(QStringLiteral("pushButton_cancel"));

        horizontalLayout->addWidget(pushButton_cancel);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(calendarDialog);

        QMetaObject::connectSlotsByName(calendarDialog);
    } // setupUi

    void retranslateUi(QDialog *calendarDialog)
    {
        calendarDialog->setWindowTitle(QApplication::translate("calendarDialog", "Dialog", 0));
        pushButton_select->setText(QApplication::translate("calendarDialog", "Select", 0));
        pushButton_cancel->setText(QApplication::translate("calendarDialog", "Cancel", 0));
    } // retranslateUi

};

namespace Ui {
    class calendarDialog: public Ui_calendarDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CALENDARDIALOG_H
