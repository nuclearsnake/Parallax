/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[26];
    char stringdata[332];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 10), // "openWithPS"
QT_MOC_LITERAL(2, 22, 0), // ""
QT_MOC_LITERAL(3, 23, 3), // "arg"
QT_MOC_LITERAL(4, 27, 18), // "openWithPhotoMatix"
QT_MOC_LITERAL(5, 46, 13), // "openWithPTGui"
QT_MOC_LITERAL(6, 60, 6), // "search"
QT_MOC_LITERAL(7, 67, 3), // "str"
QT_MOC_LITERAL(8, 71, 11), // "showOverlay"
QT_MOC_LITERAL(9, 83, 3), // "msg"
QT_MOC_LITERAL(10, 87, 11), // "hideOverlay"
QT_MOC_LITERAL(11, 99, 13), // "fadeOutSplash"
QT_MOC_LITERAL(12, 113, 11), // "closeSplash"
QT_MOC_LITERAL(13, 125, 13), // "libraryLoaded"
QT_MOC_LITERAL(14, 139, 12), // "libraryClose"
QT_MOC_LITERAL(15, 152, 14), // "loadFromRecent"
QT_MOC_LITERAL(16, 167, 13), // "optionsDialog"
QT_MOC_LITERAL(17, 181, 24), // "libraryPreferencesDialog"
QT_MOC_LITERAL(18, 206, 15), // "shortCutKeyLeft"
QT_MOC_LITERAL(19, 222, 16), // "shortCutKeyRight"
QT_MOC_LITERAL(20, 239, 21), // "shortCutKeyRightProxy"
QT_MOC_LITERAL(21, 261, 20), // "shortCutKeyLeftProxy"
QT_MOC_LITERAL(22, 282, 11), // "proxy_key_D"
QT_MOC_LITERAL(23, 294, 19), // "viewShowToolbarText"
QT_MOC_LITERAL(24, 314, 5), // "check"
QT_MOC_LITERAL(25, 320, 11) // "tabSelector"

    },
    "MainWindow\0openWithPS\0\0arg\0"
    "openWithPhotoMatix\0openWithPTGui\0"
    "search\0str\0showOverlay\0msg\0hideOverlay\0"
    "fadeOutSplash\0closeSplash\0libraryLoaded\0"
    "libraryClose\0loadFromRecent\0optionsDialog\0"
    "libraryPreferencesDialog\0shortCutKeyLeft\0"
    "shortCutKeyRight\0shortCutKeyRightProxy\0"
    "shortCutKeyLeftProxy\0proxy_key_D\0"
    "viewShowToolbarText\0check\0tabSelector"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      20,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,  114,    2, 0x0a /* Public */,
       4,    1,  117,    2, 0x0a /* Public */,
       5,    1,  120,    2, 0x0a /* Public */,
       6,    1,  123,    2, 0x0a /* Public */,
       8,    1,  126,    2, 0x08 /* Private */,
      10,    0,  129,    2, 0x08 /* Private */,
      11,    0,  130,    2, 0x08 /* Private */,
      12,    0,  131,    2, 0x08 /* Private */,
      13,    0,  132,    2, 0x08 /* Private */,
      14,    0,  133,    2, 0x08 /* Private */,
      15,    0,  134,    2, 0x08 /* Private */,
      16,    0,  135,    2, 0x08 /* Private */,
      17,    0,  136,    2, 0x08 /* Private */,
      18,    0,  137,    2, 0x08 /* Private */,
      19,    0,  138,    2, 0x08 /* Private */,
      20,    0,  139,    2, 0x08 /* Private */,
      21,    0,  140,    2, 0x08 /* Private */,
      22,    0,  141,    2, 0x08 /* Private */,
      23,    1,  142,    2, 0x08 /* Private */,
      25,    0,  145,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::QStringList,    3,
    QMetaType::Void, QMetaType::QStringList,    3,
    QMetaType::Void, QMetaType::QStringList,    3,
    QMetaType::Void, QMetaType::QString,    7,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   24,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->openWithPS((*reinterpret_cast< QStringList(*)>(_a[1]))); break;
        case 1: _t->openWithPhotoMatix((*reinterpret_cast< QStringList(*)>(_a[1]))); break;
        case 2: _t->openWithPTGui((*reinterpret_cast< QStringList(*)>(_a[1]))); break;
        case 3: _t->search((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: _t->showOverlay((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 5: _t->hideOverlay(); break;
        case 6: _t->fadeOutSplash(); break;
        case 7: _t->closeSplash(); break;
        case 8: _t->libraryLoaded(); break;
        case 9: _t->libraryClose(); break;
        case 10: _t->loadFromRecent(); break;
        case 11: _t->optionsDialog(); break;
        case 12: _t->libraryPreferencesDialog(); break;
        case 13: _t->shortCutKeyLeft(); break;
        case 14: _t->shortCutKeyRight(); break;
        case 15: _t->shortCutKeyRightProxy(); break;
        case 16: _t->shortCutKeyLeftProxy(); break;
        case 17: _t->proxy_key_D(); break;
        case 18: _t->viewShowToolbarText((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 19: _t->tabSelector(); break;
        default: ;
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 20)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 20;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 20)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 20;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
