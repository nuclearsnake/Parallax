/****************************************************************************
** Meta object code from reading C++ file 'archivemanager.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../managers/archivemanager.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'archivemanager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_archiveManager_t {
    QByteArrayData data[102];
    char stringdata[1446];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_archiveManager_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_archiveManager_t qt_meta_stringdata_archiveManager = {
    {
QT_MOC_LITERAL(0, 0, 14), // "archiveManager"
QT_MOC_LITERAL(1, 15, 10), // "overlayMsg"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 3), // "msg"
QT_MOC_LITERAL(4, 31, 10), // "openWithPS"
QT_MOC_LITERAL(5, 42, 3), // "arg"
QT_MOC_LITERAL(6, 46, 18), // "openWithPhotoMatix"
QT_MOC_LITERAL(7, 65, 13), // "openWithPTGui"
QT_MOC_LITERAL(8, 79, 19), // "fileNaviThumbResize"
QT_MOC_LITERAL(9, 99, 17), // "treeSwitchGrouped"
QT_MOC_LITERAL(10, 117, 13), // "groupBySelect"
QT_MOC_LITERAL(11, 131, 18), // "treeExpandCollapse"
QT_MOC_LITERAL(12, 150, 5), // "state"
QT_MOC_LITERAL(13, 156, 6), // "filter"
QT_MOC_LITERAL(14, 163, 4), // "text"
QT_MOC_LITERAL(15, 168, 15), // "fileDoubleClick"
QT_MOC_LITERAL(16, 184, 5), // "index"
QT_MOC_LITERAL(17, 190, 18), // "fileNaviLoadFolder"
QT_MOC_LITERAL(18, 209, 14), // "fileNaviToggle"
QT_MOC_LITERAL(19, 224, 15), // "fileNaviSetRoot"
QT_MOC_LITERAL(20, 240, 7), // "webBack"
QT_MOC_LITERAL(21, 248, 10), // "webForward"
QT_MOC_LITERAL(22, 259, 12), // "webUrlUpdate"
QT_MOC_LITERAL(23, 272, 3), // "url"
QT_MOC_LITERAL(24, 276, 11), // "webProgress"
QT_MOC_LITERAL(25, 288, 1), // "p"
QT_MOC_LITERAL(26, 290, 19), // "browserSetThumbSize"
QT_MOC_LITERAL(27, 310, 1), // "s"
QT_MOC_LITERAL(28, 312, 24), // "browserZoomlevelHideText"
QT_MOC_LITERAL(29, 337, 18), // "browserDoubleClick"
QT_MOC_LITERAL(30, 356, 13), // "browserSelect"
QT_MOC_LITERAL(31, 370, 12), // "treeSetIndex"
QT_MOC_LITERAL(32, 383, 6), // "select"
QT_MOC_LITERAL(33, 390, 10), // "focusLists"
QT_MOC_LITERAL(34, 401, 14), // "fullMapRefresh"
QT_MOC_LITERAL(35, 416, 7), // "collect"
QT_MOC_LITERAL(36, 424, 21), // "collectionTargetCycle"
QT_MOC_LITERAL(37, 446, 16), // "collectionSelect"
QT_MOC_LITERAL(38, 463, 21), // "collectionContextMenu"
QT_MOC_LITERAL(39, 485, 3), // "pos"
QT_MOC_LITERAL(40, 489, 22), // "noteUpdateButtonEnable"
QT_MOC_LITERAL(41, 512, 20), // "dialog_collectionAdd"
QT_MOC_LITERAL(42, 533, 16), // "dialog_organizer"
QT_MOC_LITERAL(43, 550, 17), // "dialog_browserAll"
QT_MOC_LITERAL(44, 568, 19), // "dialog_notesBrowser"
QT_MOC_LITERAL(45, 588, 19), // "dialog_sitesBrowser"
QT_MOC_LITERAL(46, 608, 11), // "dialog_edit"
QT_MOC_LITERAL(47, 620, 17), // "dialog_compressor"
QT_MOC_LITERAL(48, 638, 13), // "dialog_scores"
QT_MOC_LITERAL(49, 652, 18), // "dialog_addNewProxy"
QT_MOC_LITERAL(50, 671, 13), // "dialog_addNew"
QT_MOC_LITERAL(51, 685, 6), // "preDir"
QT_MOC_LITERAL(52, 692, 18), // "dialog_siteUpdater"
QT_MOC_LITERAL(53, 711, 14), // "dialog_addSite"
QT_MOC_LITERAL(54, 726, 18), // "dialog_siteManager"
QT_MOC_LITERAL(55, 745, 15), // "tab_folderSizes"
QT_MOC_LITERAL(56, 761, 18), // "tab_monthlyUploads"
QT_MOC_LITERAL(57, 780, 17), // "sidebarMapSaveLoc"
QT_MOC_LITERAL(58, 798, 13), // "sidebarMapPin"
QT_MOC_LITERAL(59, 812, 12), // "keywordCloud"
QT_MOC_LITERAL(60, 825, 25), // "keywordCloudDoubleClicked"
QT_MOC_LITERAL(61, 851, 19), // "keywordCloudClicked"
QT_MOC_LITERAL(62, 871, 13), // "quickTagClick"
QT_MOC_LITERAL(63, 885, 8), // "quickTag"
QT_MOC_LITERAL(64, 894, 26), // "advancedFilterNotin_toggle"
QT_MOC_LITERAL(65, 921, 2), // "en"
QT_MOC_LITERAL(66, 924, 19), // "advancedFilterNotin"
QT_MOC_LITERAL(67, 944, 8), // "tabClose"
QT_MOC_LITERAL(68, 953, 1), // "i"
QT_MOC_LITERAL(69, 955, 9), // "siteClick"
QT_MOC_LITERAL(70, 965, 10), // "keywordAdd"
QT_MOC_LITERAL(71, 976, 16), // "keywordCompleter"
QT_MOC_LITERAL(72, 993, 25), // "keywordsToClipboradSpaces"
QT_MOC_LITERAL(73, 1019, 25), // "keywordsToClipboradCommas"
QT_MOC_LITERAL(74, 1045, 11), // "notesUpdate"
QT_MOC_LITERAL(75, 1057, 14), // "keywordsUpdate"
QT_MOC_LITERAL(76, 1072, 25), // "keywordUpdateButtonEnable"
QT_MOC_LITERAL(77, 1098, 10), // "filterMenu"
QT_MOC_LITERAL(78, 1109, 8), // "QAction*"
QT_MOC_LITERAL(79, 1118, 6), // "action"
QT_MOC_LITERAL(80, 1125, 11), // "filterClear"
QT_MOC_LITERAL(81, 1137, 15), // "fileContextMenu"
QT_MOC_LITERAL(82, 1153, 18), // "browserContextMenu"
QT_MOC_LITERAL(83, 1172, 10), // "selectNext"
QT_MOC_LITERAL(84, 1183, 10), // "selectPrev"
QT_MOC_LITERAL(85, 1194, 18), // "previewImageResize"
QT_MOC_LITERAL(86, 1213, 10), // "tabChanged"
QT_MOC_LITERAL(87, 1224, 3), // "tab"
QT_MOC_LITERAL(88, 1228, 13), // "tableDblClick"
QT_MOC_LITERAL(89, 1242, 18), // "sidebarBlockToggle"
QT_MOC_LITERAL(90, 1261, 16), // "titleToClipboard"
QT_MOC_LITERAL(91, 1278, 25), // "dialog_siteUpdater_single"
QT_MOC_LITERAL(92, 1304, 19), // "dialog_syncSentinel"
QT_MOC_LITERAL(93, 1324, 18), // "dialog_uploadTimes"
QT_MOC_LITERAL(94, 1343, 20), // "dialog_flattenFolder"
QT_MOC_LITERAL(95, 1364, 10), // "dialog_FPV"
QT_MOC_LITERAL(96, 1375, 14), // "dialog_addSale"
QT_MOC_LITERAL(97, 1390, 15), // "dialog_uploader"
QT_MOC_LITERAL(98, 1406, 20), // "openFolderInExplorer"
QT_MOC_LITERAL(99, 1427, 14), // "splitter1Moved"
QT_MOC_LITERAL(100, 1442, 1), // "a"
QT_MOC_LITERAL(101, 1444, 1) // "b"

    },
    "archiveManager\0overlayMsg\0\0msg\0"
    "openWithPS\0arg\0openWithPhotoMatix\0"
    "openWithPTGui\0fileNaviThumbResize\0"
    "treeSwitchGrouped\0groupBySelect\0"
    "treeExpandCollapse\0state\0filter\0text\0"
    "fileDoubleClick\0index\0fileNaviLoadFolder\0"
    "fileNaviToggle\0fileNaviSetRoot\0webBack\0"
    "webForward\0webUrlUpdate\0url\0webProgress\0"
    "p\0browserSetThumbSize\0s\0"
    "browserZoomlevelHideText\0browserDoubleClick\0"
    "browserSelect\0treeSetIndex\0select\0"
    "focusLists\0fullMapRefresh\0collect\0"
    "collectionTargetCycle\0collectionSelect\0"
    "collectionContextMenu\0pos\0"
    "noteUpdateButtonEnable\0dialog_collectionAdd\0"
    "dialog_organizer\0dialog_browserAll\0"
    "dialog_notesBrowser\0dialog_sitesBrowser\0"
    "dialog_edit\0dialog_compressor\0"
    "dialog_scores\0dialog_addNewProxy\0"
    "dialog_addNew\0preDir\0dialog_siteUpdater\0"
    "dialog_addSite\0dialog_siteManager\0"
    "tab_folderSizes\0tab_monthlyUploads\0"
    "sidebarMapSaveLoc\0sidebarMapPin\0"
    "keywordCloud\0keywordCloudDoubleClicked\0"
    "keywordCloudClicked\0quickTagClick\0"
    "quickTag\0advancedFilterNotin_toggle\0"
    "en\0advancedFilterNotin\0tabClose\0i\0"
    "siteClick\0keywordAdd\0keywordCompleter\0"
    "keywordsToClipboradSpaces\0"
    "keywordsToClipboradCommas\0notesUpdate\0"
    "keywordsUpdate\0keywordUpdateButtonEnable\0"
    "filterMenu\0QAction*\0action\0filterClear\0"
    "fileContextMenu\0browserContextMenu\0"
    "selectNext\0selectPrev\0previewImageResize\0"
    "tabChanged\0tab\0tableDblClick\0"
    "sidebarBlockToggle\0titleToClipboard\0"
    "dialog_siteUpdater_single\0dialog_syncSentinel\0"
    "dialog_uploadTimes\0dialog_flattenFolder\0"
    "dialog_FPV\0dialog_addSale\0dialog_uploader\0"
    "openFolderInExplorer\0splitter1Moved\0"
    "a\0b"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_archiveManager[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      83,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  429,    2, 0x06 /* Public */,
       4,    1,  432,    2, 0x06 /* Public */,
       6,    1,  435,    2, 0x06 /* Public */,
       7,    1,  438,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    0,  441,    2, 0x0a /* Public */,
       9,    0,  442,    2, 0x0a /* Public */,
      10,    0,  443,    2, 0x0a /* Public */,
      11,    1,  444,    2, 0x0a /* Public */,
      13,    1,  447,    2, 0x0a /* Public */,
      15,    1,  450,    2, 0x0a /* Public */,
      17,    1,  453,    2, 0x0a /* Public */,
      18,    0,  456,    2, 0x0a /* Public */,
      19,    0,  457,    2, 0x0a /* Public */,
      20,    0,  458,    2, 0x0a /* Public */,
      21,    0,  459,    2, 0x0a /* Public */,
      22,    1,  460,    2, 0x0a /* Public */,
      24,    1,  463,    2, 0x0a /* Public */,
      26,    1,  466,    2, 0x0a /* Public */,
      28,    0,  469,    2, 0x0a /* Public */,
      29,    1,  470,    2, 0x0a /* Public */,
      30,    1,  473,    2, 0x0a /* Public */,
      31,    1,  476,    2, 0x0a /* Public */,
      32,    1,  479,    2, 0x0a /* Public */,
      33,    0,  482,    2, 0x0a /* Public */,
      34,    0,  483,    2, 0x0a /* Public */,
      35,    0,  484,    2, 0x0a /* Public */,
      36,    0,  485,    2, 0x0a /* Public */,
      37,    1,  486,    2, 0x0a /* Public */,
      38,    1,  489,    2, 0x0a /* Public */,
      40,    0,  492,    2, 0x0a /* Public */,
      41,    0,  493,    2, 0x0a /* Public */,
      42,    0,  494,    2, 0x0a /* Public */,
      43,    0,  495,    2, 0x0a /* Public */,
      44,    0,  496,    2, 0x0a /* Public */,
      45,    0,  497,    2, 0x0a /* Public */,
      46,    0,  498,    2, 0x0a /* Public */,
      47,    0,  499,    2, 0x0a /* Public */,
      48,    0,  500,    2, 0x0a /* Public */,
      49,    0,  501,    2, 0x0a /* Public */,
      50,    1,  502,    2, 0x0a /* Public */,
      52,    0,  505,    2, 0x0a /* Public */,
      53,    0,  506,    2, 0x0a /* Public */,
      54,    0,  507,    2, 0x0a /* Public */,
      55,    0,  508,    2, 0x0a /* Public */,
      56,    0,  509,    2, 0x0a /* Public */,
      57,    0,  510,    2, 0x0a /* Public */,
      58,    1,  511,    2, 0x0a /* Public */,
      59,    0,  514,    2, 0x0a /* Public */,
      60,    1,  515,    2, 0x0a /* Public */,
      61,    1,  518,    2, 0x0a /* Public */,
      62,    1,  521,    2, 0x0a /* Public */,
      63,    0,  524,    2, 0x0a /* Public */,
      64,    1,  525,    2, 0x0a /* Public */,
      66,    0,  528,    2, 0x0a /* Public */,
      67,    1,  529,    2, 0x0a /* Public */,
      69,    0,  532,    2, 0x0a /* Public */,
      70,    0,  533,    2, 0x0a /* Public */,
      71,    1,  534,    2, 0x0a /* Public */,
      72,    0,  537,    2, 0x0a /* Public */,
      73,    0,  538,    2, 0x0a /* Public */,
      74,    0,  539,    2, 0x0a /* Public */,
      75,    0,  540,    2, 0x0a /* Public */,
      76,    0,  541,    2, 0x0a /* Public */,
      77,    1,  542,    2, 0x0a /* Public */,
      80,    0,  545,    2, 0x0a /* Public */,
      81,    1,  546,    2, 0x0a /* Public */,
      82,    1,  549,    2, 0x0a /* Public */,
      83,    0,  552,    2, 0x0a /* Public */,
      84,    0,  553,    2, 0x0a /* Public */,
      85,    0,  554,    2, 0x0a /* Public */,
      86,    1,  555,    2, 0x0a /* Public */,
      88,    1,  558,    2, 0x0a /* Public */,
      89,    0,  561,    2, 0x0a /* Public */,
      90,    0,  562,    2, 0x0a /* Public */,
      91,    0,  563,    2, 0x0a /* Public */,
      92,    0,  564,    2, 0x0a /* Public */,
      93,    0,  565,    2, 0x0a /* Public */,
      94,    0,  566,    2, 0x0a /* Public */,
      95,    0,  567,    2, 0x0a /* Public */,
      96,    0,  568,    2, 0x0a /* Public */,
      97,    0,  569,    2, 0x0a /* Public */,
      98,    0,  570,    2, 0x08 /* Private */,
      99,    2,  571,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QStringList,    5,
    QMetaType::Void, QMetaType::QStringList,    5,
    QMetaType::Void, QMetaType::QStringList,    5,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   12,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void, QMetaType::QModelIndex,   16,
    QMetaType::Void, QMetaType::QModelIndex,   16,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QUrl,   23,
    QMetaType::Void, QMetaType::Int,   25,
    QMetaType::Void, QMetaType::Int,   27,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,   16,
    QMetaType::Void, QMetaType::QModelIndex,   16,
    QMetaType::Void, QMetaType::QModelIndex,   16,
    QMetaType::Void, QMetaType::QModelIndex,   16,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,   16,
    QMetaType::Void, QMetaType::QPoint,   39,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   51,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   27,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,   16,
    QMetaType::Void, QMetaType::QModelIndex,   16,
    QMetaType::Void, QMetaType::QModelIndex,   16,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   65,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   68,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 78,   79,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QPoint,   39,
    QMetaType::Void, QMetaType::QPoint,   39,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   87,
    QMetaType::Void, QMetaType::QModelIndex,   16,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,  100,  101,

       0        // eod
};

void archiveManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        archiveManager *_t = static_cast<archiveManager *>(_o);
        switch (_id) {
        case 0: _t->overlayMsg((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->openWithPS((*reinterpret_cast< QStringList(*)>(_a[1]))); break;
        case 2: _t->openWithPhotoMatix((*reinterpret_cast< QStringList(*)>(_a[1]))); break;
        case 3: _t->openWithPTGui((*reinterpret_cast< QStringList(*)>(_a[1]))); break;
        case 4: _t->fileNaviThumbResize(); break;
        case 5: _t->treeSwitchGrouped(); break;
        case 6: _t->groupBySelect(); break;
        case 7: _t->treeExpandCollapse((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->filter((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 9: _t->fileDoubleClick((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 10: _t->fileNaviLoadFolder((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 11: _t->fileNaviToggle(); break;
        case 12: _t->fileNaviSetRoot(); break;
        case 13: _t->webBack(); break;
        case 14: _t->webForward(); break;
        case 15: _t->webUrlUpdate((*reinterpret_cast< QUrl(*)>(_a[1]))); break;
        case 16: _t->webProgress((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 17: _t->browserSetThumbSize((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 18: _t->browserZoomlevelHideText(); break;
        case 19: _t->browserDoubleClick((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 20: _t->browserSelect((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 21: _t->treeSetIndex((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 22: _t->select((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 23: _t->focusLists(); break;
        case 24: _t->fullMapRefresh(); break;
        case 25: _t->collect(); break;
        case 26: _t->collectionTargetCycle(); break;
        case 27: _t->collectionSelect((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 28: _t->collectionContextMenu((*reinterpret_cast< const QPoint(*)>(_a[1]))); break;
        case 29: _t->noteUpdateButtonEnable(); break;
        case 30: _t->dialog_collectionAdd(); break;
        case 31: _t->dialog_organizer(); break;
        case 32: _t->dialog_browserAll(); break;
        case 33: _t->dialog_notesBrowser(); break;
        case 34: _t->dialog_sitesBrowser(); break;
        case 35: _t->dialog_edit(); break;
        case 36: _t->dialog_compressor(); break;
        case 37: _t->dialog_scores(); break;
        case 38: _t->dialog_addNewProxy(); break;
        case 39: _t->dialog_addNew((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 40: _t->dialog_siteUpdater(); break;
        case 41: _t->dialog_addSite(); break;
        case 42: _t->dialog_siteManager(); break;
        case 43: _t->tab_folderSizes(); break;
        case 44: _t->tab_monthlyUploads(); break;
        case 45: _t->sidebarMapSaveLoc(); break;
        case 46: _t->sidebarMapPin((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 47: _t->keywordCloud(); break;
        case 48: _t->keywordCloudDoubleClicked((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 49: _t->keywordCloudClicked((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 50: _t->quickTagClick((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 51: _t->quickTag(); break;
        case 52: _t->advancedFilterNotin_toggle((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 53: _t->advancedFilterNotin(); break;
        case 54: _t->tabClose((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 55: _t->siteClick(); break;
        case 56: _t->keywordAdd(); break;
        case 57: _t->keywordCompleter((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 58: _t->keywordsToClipboradSpaces(); break;
        case 59: _t->keywordsToClipboradCommas(); break;
        case 60: _t->notesUpdate(); break;
        case 61: _t->keywordsUpdate(); break;
        case 62: _t->keywordUpdateButtonEnable(); break;
        case 63: _t->filterMenu((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 64: _t->filterClear(); break;
        case 65: _t->fileContextMenu((*reinterpret_cast< const QPoint(*)>(_a[1]))); break;
        case 66: _t->browserContextMenu((*reinterpret_cast< const QPoint(*)>(_a[1]))); break;
        case 67: _t->selectNext(); break;
        case 68: _t->selectPrev(); break;
        case 69: _t->previewImageResize(); break;
        case 70: _t->tabChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 71: _t->tableDblClick((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 72: _t->sidebarBlockToggle(); break;
        case 73: _t->titleToClipboard(); break;
        case 74: _t->dialog_siteUpdater_single(); break;
        case 75: _t->dialog_syncSentinel(); break;
        case 76: _t->dialog_uploadTimes(); break;
        case 77: _t->dialog_flattenFolder(); break;
        case 78: _t->dialog_FPV(); break;
        case 79: _t->dialog_addSale(); break;
        case 80: _t->dialog_uploader(); break;
        case 81: _t->openFolderInExplorer(); break;
        case 82: _t->splitter1Moved((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (archiveManager::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&archiveManager::overlayMsg)) {
                *result = 0;
            }
        }
        {
            typedef void (archiveManager::*_t)(QStringList );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&archiveManager::openWithPS)) {
                *result = 1;
            }
        }
        {
            typedef void (archiveManager::*_t)(QStringList );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&archiveManager::openWithPhotoMatix)) {
                *result = 2;
            }
        }
        {
            typedef void (archiveManager::*_t)(QStringList );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&archiveManager::openWithPTGui)) {
                *result = 3;
            }
        }
    }
}

const QMetaObject archiveManager::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_archiveManager.data,
      qt_meta_data_archiveManager,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *archiveManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *archiveManager::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_archiveManager.stringdata))
        return static_cast<void*>(const_cast< archiveManager*>(this));
    return QObject::qt_metacast(_clname);
}

int archiveManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 83)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 83;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 83)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 83;
    }
    return _id;
}

// SIGNAL 0
void archiveManager::overlayMsg(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void archiveManager::openWithPS(QStringList _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void archiveManager::openWithPhotoMatix(QStringList _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void archiveManager::openWithPTGui(QStringList _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_END_MOC_NAMESPACE
