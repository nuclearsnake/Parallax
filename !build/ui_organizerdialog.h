/********************************************************************************
** Form generated from reading UI file 'organizerdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ORGANIZERDIALOG_H
#define UI_ORGANIZERDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_organizerDialog
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_2;
    QLabel *label;
    QListView *lv00Source;
    QPushButton *pushButton_auto;
    QGridLayout *gridLayout;
    QLabel *label_2;
    QLabel *label_4;
    QListView *lv02Pano;
    QListView *lv01Origi;
    QLabel *label_3;
    QListView *lv03Hdr;
    QLabel *label_5;
    QListView *lv04Tmp;
    QLabel *label_6;
    QListView *lv05Post;
    QLabel *label_7;
    QListView *lv06Res;
    QListView *lv07Final;
    QLabel *label_8;
    QLabel *label_9;
    QListView *lv08Print;
    QLabel *label_10;
    QListView *lv09Wall;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_go;
    QPushButton *pushButton_cancel;

    void setupUi(QDialog *organizerDialog)
    {
        if (organizerDialog->objectName().isEmpty())
            organizerDialog->setObjectName(QStringLiteral("organizerDialog"));
        organizerDialog->resize(921, 578);
        verticalLayout = new QVBoxLayout(organizerDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(-1, 0, -1, -1);
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(-1, 0, -1, -1);
        label = new QLabel(organizerDialog);
        label->setObjectName(QStringLiteral("label"));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);

        verticalLayout_2->addWidget(label);

        lv00Source = new QListView(organizerDialog);
        lv00Source->setObjectName(QStringLiteral("lv00Source"));

        verticalLayout_2->addWidget(lv00Source);


        horizontalLayout_2->addLayout(verticalLayout_2);

        pushButton_auto = new QPushButton(organizerDialog);
        pushButton_auto->setObjectName(QStringLiteral("pushButton_auto"));
        pushButton_auto->setMinimumSize(QSize(50, 50));
        pushButton_auto->setMaximumSize(QSize(50, 50));

        horizontalLayout_2->addWidget(pushButton_auto);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(organizerDialog);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setFont(font);

        gridLayout->addWidget(label_2, 0, 0, 1, 1);

        label_4 = new QLabel(organizerDialog);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setFont(font);

        gridLayout->addWidget(label_4, 0, 2, 1, 1);

        lv02Pano = new QListView(organizerDialog);
        lv02Pano->setObjectName(QStringLiteral("lv02Pano"));

        gridLayout->addWidget(lv02Pano, 1, 1, 1, 1);

        lv01Origi = new QListView(organizerDialog);
        lv01Origi->setObjectName(QStringLiteral("lv01Origi"));

        gridLayout->addWidget(lv01Origi, 1, 0, 1, 1);

        label_3 = new QLabel(organizerDialog);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setFont(font);

        gridLayout->addWidget(label_3, 0, 1, 1, 1);

        lv03Hdr = new QListView(organizerDialog);
        lv03Hdr->setObjectName(QStringLiteral("lv03Hdr"));

        gridLayout->addWidget(lv03Hdr, 1, 2, 1, 1);

        label_5 = new QLabel(organizerDialog);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setFont(font);

        gridLayout->addWidget(label_5, 2, 0, 1, 1);

        lv04Tmp = new QListView(organizerDialog);
        lv04Tmp->setObjectName(QStringLiteral("lv04Tmp"));

        gridLayout->addWidget(lv04Tmp, 3, 0, 1, 1);

        label_6 = new QLabel(organizerDialog);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setFont(font);

        gridLayout->addWidget(label_6, 2, 1, 1, 1);

        lv05Post = new QListView(organizerDialog);
        lv05Post->setObjectName(QStringLiteral("lv05Post"));

        gridLayout->addWidget(lv05Post, 3, 1, 1, 1);

        label_7 = new QLabel(organizerDialog);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setFont(font);

        gridLayout->addWidget(label_7, 2, 2, 1, 1);

        lv06Res = new QListView(organizerDialog);
        lv06Res->setObjectName(QStringLiteral("lv06Res"));

        gridLayout->addWidget(lv06Res, 3, 2, 1, 1);

        lv07Final = new QListView(organizerDialog);
        lv07Final->setObjectName(QStringLiteral("lv07Final"));

        gridLayout->addWidget(lv07Final, 5, 0, 1, 1);

        label_8 = new QLabel(organizerDialog);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setFont(font);

        gridLayout->addWidget(label_8, 4, 0, 1, 1);

        label_9 = new QLabel(organizerDialog);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setFont(font);

        gridLayout->addWidget(label_9, 4, 1, 1, 1);

        lv08Print = new QListView(organizerDialog);
        lv08Print->setObjectName(QStringLiteral("lv08Print"));

        gridLayout->addWidget(lv08Print, 5, 1, 1, 1);

        label_10 = new QLabel(organizerDialog);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setFont(font);

        gridLayout->addWidget(label_10, 4, 2, 1, 1);

        lv09Wall = new QListView(organizerDialog);
        lv09Wall->setObjectName(QStringLiteral("lv09Wall"));

        gridLayout->addWidget(lv09Wall, 5, 2, 1, 1);


        horizontalLayout_2->addLayout(gridLayout);

        horizontalLayout_2->setStretch(0, 1);
        horizontalLayout_2->setStretch(2, 3);

        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        pushButton = new QPushButton(organizerDialog);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        horizontalLayout->addWidget(pushButton);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton_go = new QPushButton(organizerDialog);
        pushButton_go->setObjectName(QStringLiteral("pushButton_go"));
        pushButton_go->setMinimumSize(QSize(100, 25));

        horizontalLayout->addWidget(pushButton_go);

        pushButton_cancel = new QPushButton(organizerDialog);
        pushButton_cancel->setObjectName(QStringLiteral("pushButton_cancel"));
        pushButton_cancel->setMinimumSize(QSize(100, 25));

        horizontalLayout->addWidget(pushButton_cancel);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(organizerDialog);

        QMetaObject::connectSlotsByName(organizerDialog);
    } // setupUi

    void retranslateUi(QDialog *organizerDialog)
    {
        organizerDialog->setWindowTitle(QApplication::translate("organizerDialog", "Dialog", 0));
        label->setText(QApplication::translate("organizerDialog", "00 - files", 0));
        pushButton_auto->setText(QApplication::translate("organizerDialog", "Auto >>", 0));
        label_2->setText(QApplication::translate("organizerDialog", "01 - Original Files", 0));
        label_4->setText(QApplication::translate("organizerDialog", "03 - HDR", 0));
        label_3->setText(QApplication::translate("organizerDialog", "02 - Panorama", 0));
        label_5->setText(QApplication::translate("organizerDialog", "04 - Tonemapped", 0));
        label_6->setText(QApplication::translate("organizerDialog", "05 - PostProcess", 0));
        label_7->setText(QApplication::translate("organizerDialog", "06 - Resized", 0));
        label_8->setText(QApplication::translate("organizerDialog", "07 - Final", 0));
        label_9->setText(QApplication::translate("organizerDialog", "08 - Print", 0));
        label_10->setText(QApplication::translate("organizerDialog", "09 - Wallpaper", 0));
        pushButton->setText(QApplication::translate("organizerDialog", "PushButton", 0));
        pushButton_go->setText(QApplication::translate("organizerDialog", "Go", 0));
        pushButton_cancel->setText(QApplication::translate("organizerDialog", "Cancel", 0));
    } // retranslateUi

};

namespace Ui {
    class organizerDialog: public Ui_organizerDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ORGANIZERDIALOG_H
