/********************************************************************************
** Form generated from reading UI file 'addcollectiondialog.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADDCOLLECTIONDIALOG_H
#define UI_ADDCOLLECTIONDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_addCollectionDialog
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QRadioButton *radioButton_smart;
    QRadioButton *radioButton_itemBased;
    QSpacerItem *horizontalSpacer_2;
    QGridLayout *gridLayout;
    QLineEdit *lineEdit_text;
    QLineEdit *lineEdit_title;
    QLabel *label;
    QLabel *label_2;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_cancel;
    QPushButton *pushButton_add;

    void setupUi(QDialog *addCollectionDialog)
    {
        if (addCollectionDialog->objectName().isEmpty())
            addCollectionDialog->setObjectName(QStringLiteral("addCollectionDialog"));
        addCollectionDialog->resize(358, 147);
        verticalLayout = new QVBoxLayout(addCollectionDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(-1, 0, -1, -1);
        radioButton_smart = new QRadioButton(addCollectionDialog);
        radioButton_smart->setObjectName(QStringLiteral("radioButton_smart"));
        radioButton_smart->setChecked(true);

        horizontalLayout_2->addWidget(radioButton_smart);

        radioButton_itemBased = new QRadioButton(addCollectionDialog);
        radioButton_itemBased->setObjectName(QStringLiteral("radioButton_itemBased"));

        horizontalLayout_2->addWidget(radioButton_itemBased);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout_2);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        lineEdit_text = new QLineEdit(addCollectionDialog);
        lineEdit_text->setObjectName(QStringLiteral("lineEdit_text"));

        gridLayout->addWidget(lineEdit_text, 2, 1, 1, 1);

        lineEdit_title = new QLineEdit(addCollectionDialog);
        lineEdit_title->setObjectName(QStringLiteral("lineEdit_title"));

        gridLayout->addWidget(lineEdit_title, 1, 1, 1, 1);

        label = new QLabel(addCollectionDialog);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 1, 0, 1, 1);

        label_2 = new QLabel(addCollectionDialog);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 2, 0, 1, 1);


        verticalLayout->addLayout(gridLayout);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton_cancel = new QPushButton(addCollectionDialog);
        pushButton_cancel->setObjectName(QStringLiteral("pushButton_cancel"));
        pushButton_cancel->setMinimumSize(QSize(100, 25));

        horizontalLayout->addWidget(pushButton_cancel);

        pushButton_add = new QPushButton(addCollectionDialog);
        pushButton_add->setObjectName(QStringLiteral("pushButton_add"));
        pushButton_add->setMinimumSize(QSize(100, 25));
        pushButton_add->setMaximumSize(QSize(100, 25));

        horizontalLayout->addWidget(pushButton_add);


        verticalLayout->addLayout(horizontalLayout);

        QWidget::setTabOrder(radioButton_smart, radioButton_itemBased);
        QWidget::setTabOrder(radioButton_itemBased, lineEdit_title);
        QWidget::setTabOrder(lineEdit_title, lineEdit_text);
        QWidget::setTabOrder(lineEdit_text, pushButton_cancel);
        QWidget::setTabOrder(pushButton_cancel, pushButton_add);

        retranslateUi(addCollectionDialog);

        QMetaObject::connectSlotsByName(addCollectionDialog);
    } // setupUi

    void retranslateUi(QDialog *addCollectionDialog)
    {
        addCollectionDialog->setWindowTitle(QApplication::translate("addCollectionDialog", "Dialog", 0));
        radioButton_smart->setText(QApplication::translate("addCollectionDialog", "Smart collection", 0));
        radioButton_itemBased->setText(QApplication::translate("addCollectionDialog", "Item based", 0));
        label->setText(QApplication::translate("addCollectionDialog", "Collection title", 0));
        label_2->setText(QApplication::translate("addCollectionDialog", "Search text", 0));
        pushButton_cancel->setText(QApplication::translate("addCollectionDialog", "Cancel", 0));
        pushButton_add->setText(QApplication::translate("addCollectionDialog", "Add", 0));
    } // retranslateUi

};

namespace Ui {
    class addCollectionDialog: public Ui_addCollectionDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADDCOLLECTIONDIALOG_H
