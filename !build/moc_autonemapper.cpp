/****************************************************************************
** Meta object code from reading C++ file 'autonemapper.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../autonemapper.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'autonemapper.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_auToneMapper_t {
    QByteArrayData data[10];
    char stringdata[72];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_auToneMapper_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_auToneMapper_t qt_meta_stringdata_auToneMapper = {
    {
QT_MOC_LITERAL(0, 0, 12), // "auToneMapper"
QT_MOC_LITERAL(1, 13, 4), // "init"
QT_MOC_LITERAL(2, 18, 0), // ""
QT_MOC_LITERAL(3, 19, 12), // "getTemplates"
QT_MOC_LITERAL(4, 32, 6), // "branch"
QT_MOC_LITERAL(5, 39, 2), // "go"
QT_MOC_LITERAL(6, 42, 4), // "test"
QT_MOC_LITERAL(7, 47, 7), // "runProc"
QT_MOC_LITERAL(8, 55, 7), // "command"
QT_MOC_LITERAL(9, 63, 8) // "runProxy"

    },
    "auToneMapper\0init\0\0getTemplates\0branch\0"
    "go\0test\0runProc\0command\0runProxy"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_auToneMapper[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   44,    2, 0x08 /* Private */,
       3,    1,   45,    2, 0x08 /* Private */,
       5,    0,   48,    2, 0x08 /* Private */,
       6,    0,   49,    2, 0x08 /* Private */,
       7,    1,   50,    2, 0x08 /* Private */,
       9,    0,   53,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    8,
    QMetaType::Void,

       0        // eod
};

void auToneMapper::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auToneMapper *_t = static_cast<auToneMapper *>(_o);
        switch (_id) {
        case 0: _t->init(); break;
        case 1: _t->getTemplates((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->go(); break;
        case 3: _t->test(); break;
        case 4: _t->runProc((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 5: _t->runProxy(); break;
        default: ;
        }
    }
}

const QMetaObject auToneMapper::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_auToneMapper.data,
      qt_meta_data_auToneMapper,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *auToneMapper::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *auToneMapper::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_auToneMapper.stringdata))
        return static_cast<void*>(const_cast< auToneMapper*>(this));
    return QDialog::qt_metacast(_clname);
}

int auToneMapper::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
