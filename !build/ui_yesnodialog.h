/********************************************************************************
** Form generated from reading UI file 'yesnodialog.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_YESNODIALOG_H
#define UI_YESNODIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_yesNoDialog
{
public:
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_3;
    QHBoxLayout *horizontalLayout;
    QLabel *label_2;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_message;
    QLabel *label_messageDetail;
    QSpacerItem *horizontalSpacer_4;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *pushButton_yes;
    QPushButton *pushButton_no;
    QSpacerItem *horizontalSpacer;

    void setupUi(QDialog *yesNoDialog)
    {
        if (yesNoDialog->objectName().isEmpty())
            yesNoDialog->setObjectName(QStringLiteral("yesNoDialog"));
        yesNoDialog->resize(650, 160);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(yesNoDialog->sizePolicy().hasHeightForWidth());
        yesNoDialog->setSizePolicy(sizePolicy);
        yesNoDialog->setMinimumSize(QSize(650, 160));
        yesNoDialog->setMaximumSize(QSize(850, 160));
        verticalLayout = new QVBoxLayout(yesNoDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(-1, 0, -1, 20);
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label_2 = new QLabel(yesNoDialog);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setMinimumSize(QSize(48, 48));
        label_2->setMaximumSize(QSize(48, 48));
        label_2->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/question.png")));
        label_2->setScaledContents(true);

        horizontalLayout->addWidget(label_2);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        label_message = new QLabel(yesNoDialog);
        label_message->setObjectName(QStringLiteral("label_message"));
        QFont font;
        font.setPointSize(11);
        font.setBold(true);
        font.setWeight(75);
        label_message->setFont(font);

        verticalLayout_2->addWidget(label_message);

        label_messageDetail = new QLabel(yesNoDialog);
        label_messageDetail->setObjectName(QStringLiteral("label_messageDetail"));
        label_messageDetail->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        verticalLayout_2->addWidget(label_messageDetail);


        horizontalLayout->addLayout(verticalLayout_2);


        horizontalLayout_3->addLayout(horizontalLayout);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_4);


        verticalLayout->addLayout(horizontalLayout_3);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        pushButton_yes = new QPushButton(yesNoDialog);
        pushButton_yes->setObjectName(QStringLiteral("pushButton_yes"));
        pushButton_yes->setMinimumSize(QSize(75, 25));

        horizontalLayout_2->addWidget(pushButton_yes);

        pushButton_no = new QPushButton(yesNoDialog);
        pushButton_no->setObjectName(QStringLiteral("pushButton_no"));
        pushButton_no->setMinimumSize(QSize(75, 25));

        horizontalLayout_2->addWidget(pushButton_no);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);


        verticalLayout->addLayout(horizontalLayout_2);


        retranslateUi(yesNoDialog);

        QMetaObject::connectSlotsByName(yesNoDialog);
    } // setupUi

    void retranslateUi(QDialog *yesNoDialog)
    {
        yesNoDialog->setWindowTitle(QApplication::translate("yesNoDialog", "Dialog", 0));
        label_2->setText(QString());
        label_message->setText(QApplication::translate("yesNoDialog", "TextLabel", 0));
        label_messageDetail->setText(QApplication::translate("yesNoDialog", "TextLabel", 0));
        pushButton_yes->setText(QApplication::translate("yesNoDialog", "Yes", 0));
        pushButton_no->setText(QApplication::translate("yesNoDialog", "No", 0));
    } // retranslateUi

};

namespace Ui {
    class yesNoDialog: public Ui_yesNoDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_YESNODIALOG_H
