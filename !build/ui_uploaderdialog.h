/********************************************************************************
** Form generated from reading UI file 'uploaderdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UPLOADERDIALOG_H
#define UI_UPLOADERDIALOG_H

#include <QtCore/QVariant>
#include <QtWebKitWidgets/QWebView>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QListView>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_uploaderDialog
{
public:
    QVBoxLayout *verticalLayout;
    QListView *listView;
    QWebView *webView;

    void setupUi(QDialog *uploaderDialog)
    {
        if (uploaderDialog->objectName().isEmpty())
            uploaderDialog->setObjectName(QStringLiteral("uploaderDialog"));
        uploaderDialog->resize(1051, 917);
        verticalLayout = new QVBoxLayout(uploaderDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        listView = new QListView(uploaderDialog);
        listView->setObjectName(QStringLiteral("listView"));
        listView->setMaximumSize(QSize(16777215, 150));

        verticalLayout->addWidget(listView);

        webView = new QWebView(uploaderDialog);
        webView->setObjectName(QStringLiteral("webView"));
        webView->setUrl(QUrl(QStringLiteral("about:blank")));

        verticalLayout->addWidget(webView);


        retranslateUi(uploaderDialog);

        QMetaObject::connectSlotsByName(uploaderDialog);
    } // setupUi

    void retranslateUi(QDialog *uploaderDialog)
    {
        uploaderDialog->setWindowTitle(QApplication::translate("uploaderDialog", "Dialog", 0));
    } // retranslateUi

};

namespace Ui {
    class uploaderDialog: public Ui_uploaderDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UPLOADERDIALOG_H
