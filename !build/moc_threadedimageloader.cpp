/****************************************************************************
** Meta object code from reading C++ file 'threadedimageloader.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../extensions/threadedimageloader.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'threadedimageloader.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_threadedImageLoader_t {
    QByteArrayData data[7];
    char stringdata[57];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_threadedImageLoader_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_threadedImageLoader_t qt_meta_stringdata_threadedImageLoader = {
    {
QT_MOC_LITERAL(0, 0, 19), // "threadedImageLoader"
QT_MOC_LITERAL(1, 20, 13), // "numberChanged"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 6), // "loaded"
QT_MOC_LITERAL(4, 42, 3), // "cur"
QT_MOC_LITERAL(5, 46, 3), // "all"
QT_MOC_LITERAL(6, 50, 6) // "blob64"

    },
    "threadedImageLoader\0numberChanged\0\0"
    "loaded\0cur\0all\0blob64"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_threadedImageLoader[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   24,    2, 0x06 /* Public */,
       3,    3,   27,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::QByteArray,    4,    5,    6,

       0        // eod
};

void threadedImageLoader::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        threadedImageLoader *_t = static_cast<threadedImageLoader *>(_o);
        switch (_id) {
        case 0: _t->numberChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->loaded((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< QByteArray(*)>(_a[3]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (threadedImageLoader::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&threadedImageLoader::numberChanged)) {
                *result = 0;
            }
        }
        {
            typedef void (threadedImageLoader::*_t)(int , int , QByteArray );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&threadedImageLoader::loaded)) {
                *result = 1;
            }
        }
    }
}

const QMetaObject threadedImageLoader::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_threadedImageLoader.data,
      qt_meta_data_threadedImageLoader,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *threadedImageLoader::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *threadedImageLoader::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_threadedImageLoader.stringdata))
        return static_cast<void*>(const_cast< threadedImageLoader*>(this));
    return QThread::qt_metacast(_clname);
}

int threadedImageLoader::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void threadedImageLoader::numberChanged(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void threadedImageLoader::loaded(int _t1, int _t2, QByteArray _t3)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
