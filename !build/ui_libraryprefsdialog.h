/********************************************************************************
** Form generated from reading UI file 'libraryprefsdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LIBRARYPREFSDIALOG_H
#define UI_LIBRARYPREFSDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_libraryPrefsDialog
{
public:
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QSlider *horizontalSlider_scoreBias;
    QLabel *label;
    QLabel *label_scoreBias;
    QLabel *label_2;
    QLabel *label_topScore;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_save;
    QPushButton *pushButton_cancel;

    void setupUi(QDialog *libraryPrefsDialog)
    {
        if (libraryPrefsDialog->objectName().isEmpty())
            libraryPrefsDialog->setObjectName(QStringLiteral("libraryPrefsDialog"));
        libraryPrefsDialog->resize(488, 218);
        verticalLayout = new QVBoxLayout(libraryPrefsDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        horizontalSlider_scoreBias = new QSlider(libraryPrefsDialog);
        horizontalSlider_scoreBias->setObjectName(QStringLiteral("horizontalSlider_scoreBias"));
        horizontalSlider_scoreBias->setMinimum(100);
        horizontalSlider_scoreBias->setMaximum(300);
        horizontalSlider_scoreBias->setPageStep(1);
        horizontalSlider_scoreBias->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(horizontalSlider_scoreBias, 0, 1, 1, 1);

        label = new QLabel(libraryPrefsDialog);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        label_scoreBias = new QLabel(libraryPrefsDialog);
        label_scoreBias->setObjectName(QStringLiteral("label_scoreBias"));

        gridLayout->addWidget(label_scoreBias, 0, 2, 1, 1);

        label_2 = new QLabel(libraryPrefsDialog);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        label_topScore = new QLabel(libraryPrefsDialog);
        label_topScore->setObjectName(QStringLiteral("label_topScore"));

        gridLayout->addWidget(label_topScore, 1, 1, 1, 1);


        verticalLayout->addLayout(gridLayout);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton_save = new QPushButton(libraryPrefsDialog);
        pushButton_save->setObjectName(QStringLiteral("pushButton_save"));
        pushButton_save->setMinimumSize(QSize(100, 25));

        horizontalLayout->addWidget(pushButton_save);

        pushButton_cancel = new QPushButton(libraryPrefsDialog);
        pushButton_cancel->setObjectName(QStringLiteral("pushButton_cancel"));
        pushButton_cancel->setMinimumSize(QSize(100, 25));

        horizontalLayout->addWidget(pushButton_cancel);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(libraryPrefsDialog);

        QMetaObject::connectSlotsByName(libraryPrefsDialog);
    } // setupUi

    void retranslateUi(QDialog *libraryPrefsDialog)
    {
        libraryPrefsDialog->setWindowTitle(QApplication::translate("libraryPrefsDialog", "Dialog", 0));
        label->setText(QApplication::translate("libraryPrefsDialog", "Score Bias", 0));
        label_scoreBias->setText(QApplication::translate("libraryPrefsDialog", "1.00", 0));
        label_2->setText(QApplication::translate("libraryPrefsDialog", "Calculated top score:", 0));
        label_topScore->setText(QString());
        pushButton_save->setText(QApplication::translate("libraryPrefsDialog", "Save", 0));
        pushButton_cancel->setText(QApplication::translate("libraryPrefsDialog", "Cancel", 0));
    } // retranslateUi

};

namespace Ui {
    class libraryPrefsDialog: public Ui_libraryPrefsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LIBRARYPREFSDIALOG_H
