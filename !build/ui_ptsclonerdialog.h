/********************************************************************************
** Form generated from reading UI file 'ptsclonerdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PTSCLONERDIALOG_H
#define UI_PTSCLONERDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_PtsClonerDialog
{
public:
    QVBoxLayout *verticalLayout;
    QFormLayout *formLayout;
    QLabel *label;
    QLabel *label_2;
    QHBoxLayout *horizontalLayout;
    QComboBox *comboBoxSourceSelect;
    QLabel *label_3;
    QLineEdit *lineEdit_inEv;
    QCheckBox *checkBoxEv0;
    QCheckBox *checkBoxEv1;
    QCheckBox *checkBoxEv2;
    QLineEdit *lineEditEv0;
    QLineEdit *lineEditEv1;
    QLineEdit *lineEditEv2;
    QPlainTextEdit *log;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButtonCancel;
    QPushButton *pushButtonGo;
    QPushButton *pushButtonClose;

    void setupUi(QDialog *PtsClonerDialog)
    {
        if (PtsClonerDialog->objectName().isEmpty())
            PtsClonerDialog->setObjectName(QStringLiteral("PtsClonerDialog"));
        PtsClonerDialog->resize(425, 302);
        verticalLayout = new QVBoxLayout(PtsClonerDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        formLayout = new QFormLayout();
        formLayout->setObjectName(QStringLiteral("formLayout"));
        label = new QLabel(PtsClonerDialog);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        label_2 = new QLabel(PtsClonerDialog);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        comboBoxSourceSelect = new QComboBox(PtsClonerDialog);
        comboBoxSourceSelect->setObjectName(QStringLiteral("comboBoxSourceSelect"));

        horizontalLayout->addWidget(comboBoxSourceSelect);

        label_3 = new QLabel(PtsClonerDialog);
        label_3->setObjectName(QStringLiteral("label_3"));

        horizontalLayout->addWidget(label_3);

        lineEdit_inEv = new QLineEdit(PtsClonerDialog);
        lineEdit_inEv->setObjectName(QStringLiteral("lineEdit_inEv"));
        lineEdit_inEv->setMaximumSize(QSize(50, 16777215));

        horizontalLayout->addWidget(lineEdit_inEv);

        horizontalLayout->setStretch(0, 1);

        formLayout->setLayout(0, QFormLayout::FieldRole, horizontalLayout);

        checkBoxEv0 = new QCheckBox(PtsClonerDialog);
        checkBoxEv0->setObjectName(QStringLiteral("checkBoxEv0"));

        formLayout->setWidget(2, QFormLayout::LabelRole, checkBoxEv0);

        checkBoxEv1 = new QCheckBox(PtsClonerDialog);
        checkBoxEv1->setObjectName(QStringLiteral("checkBoxEv1"));

        formLayout->setWidget(3, QFormLayout::LabelRole, checkBoxEv1);

        checkBoxEv2 = new QCheckBox(PtsClonerDialog);
        checkBoxEv2->setObjectName(QStringLiteral("checkBoxEv2"));

        formLayout->setWidget(4, QFormLayout::LabelRole, checkBoxEv2);

        lineEditEv0 = new QLineEdit(PtsClonerDialog);
        lineEditEv0->setObjectName(QStringLiteral("lineEditEv0"));

        formLayout->setWidget(2, QFormLayout::FieldRole, lineEditEv0);

        lineEditEv1 = new QLineEdit(PtsClonerDialog);
        lineEditEv1->setObjectName(QStringLiteral("lineEditEv1"));

        formLayout->setWidget(3, QFormLayout::FieldRole, lineEditEv1);

        lineEditEv2 = new QLineEdit(PtsClonerDialog);
        lineEditEv2->setObjectName(QStringLiteral("lineEditEv2"));

        formLayout->setWidget(4, QFormLayout::FieldRole, lineEditEv2);


        verticalLayout->addLayout(formLayout);

        log = new QPlainTextEdit(PtsClonerDialog);
        log->setObjectName(QStringLiteral("log"));

        verticalLayout->addWidget(log);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(-1, 0, -1, -1);
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        pushButtonCancel = new QPushButton(PtsClonerDialog);
        pushButtonCancel->setObjectName(QStringLiteral("pushButtonCancel"));
        pushButtonCancel->setMinimumSize(QSize(100, 25));

        horizontalLayout_2->addWidget(pushButtonCancel);

        pushButtonGo = new QPushButton(PtsClonerDialog);
        pushButtonGo->setObjectName(QStringLiteral("pushButtonGo"));
        pushButtonGo->setMinimumSize(QSize(100, 25));

        horizontalLayout_2->addWidget(pushButtonGo);

        pushButtonClose = new QPushButton(PtsClonerDialog);
        pushButtonClose->setObjectName(QStringLiteral("pushButtonClose"));
        pushButtonClose->setMinimumSize(QSize(100, 25));

        horizontalLayout_2->addWidget(pushButtonClose);


        verticalLayout->addLayout(horizontalLayout_2);


        retranslateUi(PtsClonerDialog);

        QMetaObject::connectSlotsByName(PtsClonerDialog);
    } // setupUi

    void retranslateUi(QDialog *PtsClonerDialog)
    {
        PtsClonerDialog->setWindowTitle(QApplication::translate("PtsClonerDialog", "Dialog", 0));
        label->setText(QApplication::translate("PtsClonerDialog", "Source", 0));
        label_2->setText(QApplication::translate("PtsClonerDialog", "Target(s)", 0));
        label_3->setText(QApplication::translate("PtsClonerDialog", "EV:", 0));
        checkBoxEv0->setText(QApplication::translate("PtsClonerDialog", "EV0", 0));
        checkBoxEv1->setText(QApplication::translate("PtsClonerDialog", "EV1", 0));
        checkBoxEv2->setText(QApplication::translate("PtsClonerDialog", "EV2", 0));
        pushButtonCancel->setText(QApplication::translate("PtsClonerDialog", "Cancel", 0));
        pushButtonGo->setText(QApplication::translate("PtsClonerDialog", "Go", 0));
        pushButtonClose->setText(QApplication::translate("PtsClonerDialog", "Close", 0));
    } // retranslateUi

};

namespace Ui {
    class PtsClonerDialog: public Ui_PtsClonerDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PTSCLONERDIALOG_H
