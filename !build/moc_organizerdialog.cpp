/****************************************************************************
** Meta object code from reading C++ file 'organizerdialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../dialogs/organizerdialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'organizerdialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_organizerDialog_t {
    QByteArrayData data[20];
    char stringdata[208];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_organizerDialog_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_organizerDialog_t qt_meta_stringdata_organizerDialog = {
    {
QT_MOC_LITERAL(0, 0, 15), // "organizerDialog"
QT_MOC_LITERAL(1, 16, 10), // "updateMove"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 9), // "itemMover"
QT_MOC_LITERAL(4, 38, 19), // "QStandardItemModel*"
QT_MOC_LITERAL(5, 58, 7), // "modelTO"
QT_MOC_LITERAL(6, 66, 11), // "md00Dropped"
QT_MOC_LITERAL(7, 78, 1), // "a"
QT_MOC_LITERAL(8, 80, 1), // "b"
QT_MOC_LITERAL(9, 82, 11), // "md01Dropped"
QT_MOC_LITERAL(10, 94, 11), // "md02Dropped"
QT_MOC_LITERAL(11, 106, 11), // "md03Dropped"
QT_MOC_LITERAL(12, 118, 11), // "md04Dropped"
QT_MOC_LITERAL(13, 130, 11), // "md05Dropped"
QT_MOC_LITERAL(14, 142, 11), // "md06Dropped"
QT_MOC_LITERAL(15, 154, 11), // "md07Dropped"
QT_MOC_LITERAL(16, 166, 11), // "md08Dropped"
QT_MOC_LITERAL(17, 178, 11), // "md09Dropped"
QT_MOC_LITERAL(18, 190, 7), // "autoOrg"
QT_MOC_LITERAL(19, 198, 9) // "moveFiles"

    },
    "organizerDialog\0updateMove\0\0itemMover\0"
    "QStandardItemModel*\0modelTO\0md00Dropped\0"
    "a\0b\0md01Dropped\0md02Dropped\0md03Dropped\0"
    "md04Dropped\0md05Dropped\0md06Dropped\0"
    "md07Dropped\0md08Dropped\0md09Dropped\0"
    "autoOrg\0moveFiles"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_organizerDialog[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    4,   84,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    1,   93,    2, 0x08 /* Private */,
       6,    2,   96,    2, 0x08 /* Private */,
       9,    2,  101,    2, 0x08 /* Private */,
      10,    2,  106,    2, 0x08 /* Private */,
      11,    2,  111,    2, 0x08 /* Private */,
      12,    2,  116,    2, 0x08 /* Private */,
      13,    2,  121,    2, 0x08 /* Private */,
      14,    2,  126,    2, 0x08 /* Private */,
      15,    2,  131,    2, 0x08 /* Private */,
      16,    2,  136,    2, 0x08 /* Private */,
      17,    2,  141,    2, 0x08 /* Private */,
      18,    0,  146,    2, 0x08 /* Private */,
      19,    0,  147,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::Int, QMetaType::Int,    2,    2,    2,    2,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void, QMetaType::QModelIndex, QMetaType::QModelIndex,    7,    8,
    QMetaType::Void, QMetaType::QModelIndex, QMetaType::QModelIndex,    7,    8,
    QMetaType::Void, QMetaType::QModelIndex, QMetaType::QModelIndex,    7,    8,
    QMetaType::Void, QMetaType::QModelIndex, QMetaType::QModelIndex,    7,    8,
    QMetaType::Void, QMetaType::QModelIndex, QMetaType::QModelIndex,    7,    8,
    QMetaType::Void, QMetaType::QModelIndex, QMetaType::QModelIndex,    7,    8,
    QMetaType::Void, QMetaType::QModelIndex, QMetaType::QModelIndex,    7,    8,
    QMetaType::Void, QMetaType::QModelIndex, QMetaType::QModelIndex,    7,    8,
    QMetaType::Void, QMetaType::QModelIndex, QMetaType::QModelIndex,    7,    8,
    QMetaType::Void, QMetaType::QModelIndex, QMetaType::QModelIndex,    7,    8,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void organizerDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        organizerDialog *_t = static_cast<organizerDialog *>(_o);
        switch (_id) {
        case 0: _t->updateMove((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4]))); break;
        case 1: _t->itemMover((*reinterpret_cast< QStandardItemModel*(*)>(_a[1]))); break;
        case 2: _t->md00Dropped((*reinterpret_cast< QModelIndex(*)>(_a[1])),(*reinterpret_cast< QModelIndex(*)>(_a[2]))); break;
        case 3: _t->md01Dropped((*reinterpret_cast< QModelIndex(*)>(_a[1])),(*reinterpret_cast< QModelIndex(*)>(_a[2]))); break;
        case 4: _t->md02Dropped((*reinterpret_cast< QModelIndex(*)>(_a[1])),(*reinterpret_cast< QModelIndex(*)>(_a[2]))); break;
        case 5: _t->md03Dropped((*reinterpret_cast< QModelIndex(*)>(_a[1])),(*reinterpret_cast< QModelIndex(*)>(_a[2]))); break;
        case 6: _t->md04Dropped((*reinterpret_cast< QModelIndex(*)>(_a[1])),(*reinterpret_cast< QModelIndex(*)>(_a[2]))); break;
        case 7: _t->md05Dropped((*reinterpret_cast< QModelIndex(*)>(_a[1])),(*reinterpret_cast< QModelIndex(*)>(_a[2]))); break;
        case 8: _t->md06Dropped((*reinterpret_cast< QModelIndex(*)>(_a[1])),(*reinterpret_cast< QModelIndex(*)>(_a[2]))); break;
        case 9: _t->md07Dropped((*reinterpret_cast< QModelIndex(*)>(_a[1])),(*reinterpret_cast< QModelIndex(*)>(_a[2]))); break;
        case 10: _t->md08Dropped((*reinterpret_cast< QModelIndex(*)>(_a[1])),(*reinterpret_cast< QModelIndex(*)>(_a[2]))); break;
        case 11: _t->md09Dropped((*reinterpret_cast< QModelIndex(*)>(_a[1])),(*reinterpret_cast< QModelIndex(*)>(_a[2]))); break;
        case 12: _t->autoOrg(); break;
        case 13: _t->moveFiles(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QStandardItemModel* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (organizerDialog::*_t)(QString , QString , int , int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&organizerDialog::updateMove)) {
                *result = 0;
            }
        }
    }
}

const QMetaObject organizerDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_organizerDialog.data,
      qt_meta_data_organizerDialog,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *organizerDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *organizerDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_organizerDialog.stringdata))
        return static_cast<void*>(const_cast< organizerDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int organizerDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    }
    return _id;
}

// SIGNAL 0
void organizerDialog::updateMove(QString _t1, QString _t2, int _t3, int _t4)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
