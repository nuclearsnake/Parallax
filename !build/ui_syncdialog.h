/********************************************************************************
** Form generated from reading UI file 'syncdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SYNCDIALOG_H
#define UI_SYNCDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_syncDialog
{
public:
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QProgressBar *progressBar_logDown;
    QLabel *label;
    QLabel *label_logDown;
    QLabel *label_sitesUp;
    QProgressBar *progressBar_sitesUp;
    QLabel *label_3;
    QLabel *label_2;
    QLabel *label_sitesDown;
    QProgressBar *progressBar_sitesDown;
    QPlainTextEdit *plainTextEdit_log;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_start;

    void setupUi(QDialog *syncDialog)
    {
        if (syncDialog->objectName().isEmpty())
            syncDialog->setObjectName(QStringLiteral("syncDialog"));
        syncDialog->resize(778, 489);
        verticalLayout = new QVBoxLayout(syncDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(-1, 0, -1, -1);
        progressBar_logDown = new QProgressBar(syncDialog);
        progressBar_logDown->setObjectName(QStringLiteral("progressBar_logDown"));
        progressBar_logDown->setValue(0);

        gridLayout->addWidget(progressBar_logDown, 2, 2, 1, 1);

        label = new QLabel(syncDialog);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        label_logDown = new QLabel(syncDialog);
        label_logDown->setObjectName(QStringLiteral("label_logDown"));
        label_logDown->setMinimumSize(QSize(80, 0));
        label_logDown->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_logDown, 2, 1, 1, 1);

        label_sitesUp = new QLabel(syncDialog);
        label_sitesUp->setObjectName(QStringLiteral("label_sitesUp"));
        label_sitesUp->setMinimumSize(QSize(50, 0));
        label_sitesUp->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_sitesUp, 0, 1, 1, 1);

        progressBar_sitesUp = new QProgressBar(syncDialog);
        progressBar_sitesUp->setObjectName(QStringLiteral("progressBar_sitesUp"));
        progressBar_sitesUp->setValue(0);

        gridLayout->addWidget(progressBar_sitesUp, 0, 2, 1, 1);

        label_3 = new QLabel(syncDialog);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        label_2 = new QLabel(syncDialog);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        label_sitesDown = new QLabel(syncDialog);
        label_sitesDown->setObjectName(QStringLiteral("label_sitesDown"));
        label_sitesDown->setMinimumSize(QSize(120, 0));
        label_sitesDown->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_sitesDown, 1, 1, 1, 1);

        progressBar_sitesDown = new QProgressBar(syncDialog);
        progressBar_sitesDown->setObjectName(QStringLiteral("progressBar_sitesDown"));
        progressBar_sitesDown->setValue(0);

        gridLayout->addWidget(progressBar_sitesDown, 1, 2, 1, 1);


        verticalLayout->addLayout(gridLayout);

        plainTextEdit_log = new QPlainTextEdit(syncDialog);
        plainTextEdit_log->setObjectName(QStringLiteral("plainTextEdit_log"));
        QFont font;
        font.setFamily(QStringLiteral("Consolas"));
        plainTextEdit_log->setFont(font);

        verticalLayout->addWidget(plainTextEdit_log);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(-1, 0, -1, -1);
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton_start = new QPushButton(syncDialog);
        pushButton_start->setObjectName(QStringLiteral("pushButton_start"));

        horizontalLayout->addWidget(pushButton_start);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(syncDialog);

        QMetaObject::connectSlotsByName(syncDialog);
    } // setupUi

    void retranslateUi(QDialog *syncDialog)
    {
        syncDialog->setWindowTitle(QApplication::translate("syncDialog", "Dialog", 0));
        label->setText(QApplication::translate("syncDialog", "SITES UP", 0));
        label_logDown->setText(QApplication::translate("syncDialog", "- / -", 0));
        label_sitesUp->setText(QApplication::translate("syncDialog", "- / -", 0));
        label_3->setText(QApplication::translate("syncDialog", "LOG DOWN", 0));
        label_2->setText(QApplication::translate("syncDialog", "SITES DOWN", 0));
        label_sitesDown->setText(QApplication::translate("syncDialog", "- / -", 0));
        pushButton_start->setText(QApplication::translate("syncDialog", "START", 0));
    } // retranslateUi

};

namespace Ui {
    class syncDialog: public Ui_syncDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SYNCDIALOG_H
