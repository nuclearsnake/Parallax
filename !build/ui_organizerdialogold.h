/********************************************************************************
** Form generated from reading UI file 'organizerdialogold.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ORGANIZERDIALOGOLD_H
#define UI_ORGANIZERDIALOGOLD_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>

QT_BEGIN_NAMESPACE

class Ui_organizerDialogOld
{
public:

    void setupUi(QDialog *organizerDialogOld)
    {
        if (organizerDialogOld->objectName().isEmpty())
            organizerDialogOld->setObjectName(QStringLiteral("organizerDialogOld"));
        organizerDialogOld->resize(400, 300);

        retranslateUi(organizerDialogOld);

        QMetaObject::connectSlotsByName(organizerDialogOld);
    } // setupUi

    void retranslateUi(QDialog *organizerDialogOld)
    {
        organizerDialogOld->setWindowTitle(QApplication::translate("organizerDialogOld", "Dialog", 0));
    } // retranslateUi

};

namespace Ui {
    class organizerDialogOld: public Ui_organizerDialogOld {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ORGANIZERDIALOGOLD_H
