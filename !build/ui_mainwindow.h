/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWebKitWidgets/QWebView>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListView>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableView>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QTreeView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include <clickable_qwidget.h>
#include <widgets/linegraphwidget.h>
#include <widgets/smoothscrolllistview.h>
#include <widgets/verticalscrolllistview.h>
#include "extensions/clickable_qlabel.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionOptions;
    QAction *actionNew;
    QAction *actionOpen;
    QAction *actionClose;
    QAction *actionExit;
    QAction *actionAdd_newProcess;
    QAction *actionAdd_newArchive;
    QAction *actionProcess;
    QAction *actionBrowse;
    QAction *actionArchive;
    QAction *actionNotes;
    QAction *actionTable_View;
    QAction *actionFolder_Sizes;
    QAction *actionAdd_new_Archive_toolBar;
    QAction *actionAdd_new_Process_toolBar;
    QAction *actionMove_to_Archive_toolBar;
    QAction *actionShow_toolbar_text;
    QAction *actionSites;
    QAction *actionScore;
    QAction *actionSite_updater;
    QAction *actionPreferences;
    QAction *actionMonthly_uploads;
    QAction *actionKeywords;
    QAction *actionAutomatic_Tonemap;
    QAction *actionSync_with_Sentinel;
    QAction *actionUpload_Times;
    QAction *actionFollowers_Pageviews;
    QAction *actionInstagram_Tags;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QStackedWidget *mainStack;
    QWidget *main_frame;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *main_frame_vlayout_top;
    QTabWidget *tabWidget;
    QWidget *tab_process;
    QHBoxLayout *horizontalLayout_3;
    QWidget *widget_8;
    QVBoxLayout *verticalLayout_9;
    QLabel *label_14;
    QStackedWidget *stackedWidget_processLeft;
    QWidget *page;
    QVBoxLayout *verticalLayout_4;
    QListView *listView_processTree;
    QWidget *page_2;
    QVBoxLayout *verticalLayout_6;
    SmoothScrollListView *listView_processNewBrowser;
    QPushButton *pushButton_aniMate;
    QSpacerItem *horizontalSpacer_19;
    QVBoxLayout *vlayout_process_right;
    QWidget *widget_3;
    QVBoxLayout *verticalLayout_37;
    SmoothScrollListView *listView_processFiles;
    QWidget *widget;
    QVBoxLayout *verticalLayout_30;
    QWidget *widget_4;
    QVBoxLayout *verticalLayout_33;
    QWidget *widget_6;
    QVBoxLayout *verticalLayout_35;
    QLabel *label_processTitle;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_processPath;
    QPushButton *pushButton_processPathToClipboard;
    QLabel *label_processPano;
    QSpacerItem *verticalSpacer_7;
    QGridLayout *gridLayout_7;
    QPushButton *pushButton_processProperties;
    QPushButton *pushButton_processOpenFolder;
    QPushButton *pushButton_processToArchive;
    QPushButton *pushButton_processDelete;
    QWidget *widget_5;
    QVBoxLayout *verticalLayout_34;
    QPushButton *pushButton_processFileGroupOrigall;
    QPushButton *pushButton_processFileGroupEV0;
    QPushButton *pushButton_processFileGroupEV1;
    QPushButton *pushButton_processFileGroupEV2;
    QPushButton *pushButton_processFileGroupPanorama;
    QPushButton *pushButton_processFileGroupHDR;
    QPushButton *pushButton_processFileGroupPostProcess;
    QWidget *widget_2;
    QVBoxLayout *verticalLayout_32;
    QWidget *widget_7;
    QVBoxLayout *verticalLayout_36;
    QGridLayout *gridLayout_6;
    QPushButton *pushButton_processHDR;
    QPushButton *pushButton_processStich;
    QPushButton *pushButton_processBatchStich;
    QPushButton *pushButton_processClonePTS;
    QPushButton *pushButton_processAlltoPS;
    QWidget *widget_9;
    QVBoxLayout *verticalLayout_10;
    QLabel *label_processFileName;
    QHBoxLayout *horizontalLayout_12;
    QLabel *label_process_fileinfoPreview;
    QGridLayout *gridLayout_9;
    QPushButton *pushButton_processFileOpen;
    QPushButton *pushButton_processFileEdit;
    QSpacerItem *verticalSpacer_8;
    QGridLayout *gridLayout_8;
    QLabel *label_process_fileinfoType;
    QLabel *label_15;
    QLabel *label_process_fileinfoCreated;
    QLabel *label_17;
    QLabel *label_process_fileinfoSize;
    QLabel *label_16;
    QLabel *label_process_fileinfoResolutionLabel;
    QLabel *label_process_fileinfoResolution;
    QSpacerItem *verticalSpacer_6;
    QWidget *tab_processBrowser;
    QVBoxLayout *verticalLayout_3;
    QListView *listView_processBrowser;
    QWidget *tab_archives;
    QHBoxLayout *horizontalLayout_4;
    QVBoxLayout *vlayout_archive_left;
    QTabWidget *tabWidget_archiveTrees;
    QWidget *tabArchiveProjects;
    QVBoxLayout *verticalLayout_8;
    QHBoxLayout *horizontalLayout_7;
    QPushButton *pushButton_archiveTreeExpand;
    QSpacerItem *horizontalSpacer_28;
    QPushButton *pushButton_archiveTreeGroup;
    QSpacerItem *horizontalSpacer_21;
    QPushButton *pushButton_archiveGroupByAz;
    QPushButton *pushButton_archiveGroupByDate;
    QPushButton *pushButton_archiveGroupByLoc;
    QPushButton *pushButton_archiveGroupByScore;
    QSpacerItem *horizontalSpacer_4;
    QTreeView *treeView_archiveTree;
    QListView *listView_archiveTree;
    QWidget *tab_16;
    QVBoxLayout *verticalLayout_11;
    QHBoxLayout *horizontalLayout_29;
    QPushButton *pushButton_addCollection;
    QSpacerItem *horizontalSpacer_31;
    QListView *listView_collections;
    QWidget *widget_filter;
    QVBoxLayout *verticalLayout_29;
    QHBoxLayout *horizontalLayout_6;
    QLineEdit *lineEdit_archiveFilter;
    QPushButton *pushButton_archiveSearchByTitle;
    QPushButton *pushButton_archiveSearchByKeywords;
    QPushButton *pushButton_filterFields;
    QPushButton *pushButton_archiveFilterClear;
    QTabWidget *tabWidget_archiveCenter;
    QWidget *tab_archivePreview;
    QVBoxLayout *verticalLayout_15;
    QWidget *widget_11;
    QVBoxLayout *verticalLayout_16;
    QSplitter *splitter;
    QWidget *widget_10;
    QVBoxLayout *verticalLayout_14;
    QLabel *label_archiveTitle;
    QLabel *label_archiveDate;
    QLabel *label_previewImage;
    QWidget *widget_navi;
    QHBoxLayout *horizontalLayout_13;
    QSpacerItem *horizontalSpacer_9;
    QPushButton *pushButton_naviPrev;
    QSpacerItem *horizontalSpacer_11;
    QPushButton *pushButton_naviNext;
    QSpacerItem *horizontalSpacer_10;
    QStackedWidget *widget_archiveFolderNavi;
    QWidget *stackedWidget_archiveFolderNaviPage1;
    QVBoxLayout *verticalLayout_31;
    QHBoxLayout *horizontalLayout_31;
    QPushButton *pushButton_fileListToRoot;
    QPushButton *pushButton_imgOnly;
    QPushButton *pushButton_archiveOpenFolder;
    QPushButton *pushButton_archiveOrganizeFolder;
    QPushButton *pushButton_archiveFlattenFolder;
    QPushButton *pushButton_archiveCompressor;
    QPushButton *pushButton_toggleFolderNavi2;
    QHBoxLayout *horizontalLayout_32;
    QTreeView *treeView_archiveFiles;
    VerticalScrollListView *listView_archiveFiles;
    QWidget *page_dirWarning;
    QVBoxLayout *verticalLayout_17;
    QSpacerItem *verticalSpacer_10;
    QWidget *widget_dirWarning;
    QHBoxLayout *horizontalLayout_8;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label_2;
    QLabel *label_dirWarning;
    QSpacerItem *horizontalSpacer_5;
    QSpacerItem *verticalSpacer_11;
    QPushButton *pushButton_toggleFolderNavi;
    QWidget *tab_archiveThumbnails;
    QVBoxLayout *verticalLayout_24;
    SmoothScrollListView *listView_archiveBrowser;
    QHBoxLayout *horizontalLayout_24;
    QLabel *label_11;
    QCheckBox *checkBox_aF_notin;
    QWidget *widget_af_ni;
    QHBoxLayout *horizontalLayout_25;
    QRadioButton *radioButton_FNUP;
    QRadioButton *radioButton_FUP;
    clickable_QLabel *label_aF_niDA;
    clickable_QLabel *label_aF_niRD;
    clickable_QLabel *label_aF_ni5P;
    clickable_QLabel *label_aF_niGP;
    clickable_QLabel *label_aF_niFL;
    clickable_QLabel *label_aF_niFB;
    clickable_QLabel *label_aF_niPX;
    clickable_QLabel *label_aF_niYP;
    clickable_QLabel *label_aF_niVB;
    clickable_QLabel *label_aF_niIG;
    QSpacerItem *horizontalSpacer_29;
    QLabel *label_archiveThumbSize;
    QSlider *horizontalSlider_archiveThumbSize;
    QWidget *tab_archiveWeb;
    QVBoxLayout *verticalLayout_25;
    QHBoxLayout *horizontalLayout_21;
    QSpacerItem *verticalSpacer_5;
    QPushButton *pushButton_naviPrev_2;
    QPushButton *pushButton_naviNext_2;
    QVBoxLayout *verticalLayout_26;
    QLineEdit *lineEdit_archiveUrl;
    QProgressBar *progressBar_web;
    QWebView *webView;
    QWidget *tab_14;
    QVBoxLayout *verticalLayout_28;
    QHBoxLayout *horizontalLayout_27;
    QSpacerItem *horizontalSpacer_30;
    QPushButton *pushButton_mapRefresh;
    QWebView *webView_fullMap;
    QVBoxLayout *vlayout_archive_right;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents_5;
    QVBoxLayout *verticalLayout_23;
    clickable_QWidget *widget_archive_sidebar_preview_header;
    QHBoxLayout *horizontalLayout_30;
    QLabel *label_28;
    QLabel *label_23;
    QLabel *label_archive_sidebar_preview;
    QSpacerItem *horizontalSpacer_6;
    QLabel *label_archive_sidebar_preview_toggle;
    QWidget *widget_archive_sidebar_preview_shadow;
    QWidget *widget_archive_sidebar_preview_content;
    QVBoxLayout *verticalLayout_7;
    clickable_QLabel *label_sideBarPreview;
    clickable_QWidget *widget_archive_sidebar_info_header;
    QHBoxLayout *horizontalLayout_33;
    QLabel *label_29;
    QLabel *label_22;
    QLabel *label_archive_sidebar_info;
    QSpacerItem *horizontalSpacer_12;
    QLabel *label_archive_sidebar_info_toggle;
    QWidget *widget_archive_sidebar_info_shadow;
    QWidget *widget_archive_sidebar_info_content;
    QVBoxLayout *verticalLayout_18;
    QGridLayout *gridLayout;
    QLabel *label_archiveInfoTitle;
    QLabel *label_archiveInfoID;
    QLabel *label_5;
    QLabel *label_3;
    QLabel *label_7;
    QLabel *label_archiveInfoDate;
    QLabel *label_6;
    QLabel *label_archiveInfoLocation;
    QLabel *label_fileSize;
    QLabel *label_36;
    QLabel *label_archiveInfoTitle_2;
    QHBoxLayout *horizontalLayout_10;
    QSpacerItem *horizontalSpacer_7;
    QPushButton *pushButton_archiveEditData;
    QVBoxLayout *verticalLayout_12;
    QTableView *tableView_sales;
    QHBoxLayout *horizontalLayout_16;
    QLabel *label_salesCount;
    QLabel *label_salesTotal;
    QPushButton *pushButton_addSale;
    clickable_QWidget *widget_archive_sidebar_tools_header;
    QHBoxLayout *horizontalLayout_43;
    QLabel *label_37;
    QLabel *label_38;
    QLabel *label_archive_sidebar_tools;
    QSpacerItem *horizontalSpacer_33;
    QLabel *label_archive_sidebar_tools_toggle;
    QWidget *widget_archive_sidebar_tools_shadow;
    QWidget *widget_archive_sidebar_tools_content;
    QVBoxLayout *verticalLayout_5;
    QGridLayout *gridLayout_10;
    QPushButton *pushButton_archiveCompressor_1;
    QPushButton *pushButton_archiveCopyKeyComma_1;
    QPushButton *pushButton_archiveEditData_1;
    QPushButton *pushButton_sitesManage_1;
    QPushButton *pushButton_archiveCopyKeySpace_1;
    QPushButton *pushButton_addNewSite_1;
    QPushButton *pushButton_archiveOrganizeFolder_1;
    QPushButton *pushButton_archiveOpenFolder_1;
    QPushButton *pushButton_archiveCopyTitle;
    QPushButton *pushButton_archiveCopyTitle_2;
    QPushButton *pushButton_upload;
    clickable_QWidget *widget_archive_sidebar_map_header;
    QHBoxLayout *horizontalLayout_34;
    QLabel *label_30;
    QLabel *label;
    QLabel *label_archive_sidebar_map;
    QSpacerItem *horizontalSpacer_13;
    QLabel *label_archive_sidebar_map_toggle;
    QWidget *widget_archive_sidebar_map_shadow;
    QWidget *widget_archive_sidebar_map_content;
    QVBoxLayout *verticalLayout_22;
    QWebView *webView_Map;
    QPlainTextEdit *plainTextEdit_loc;
    QHBoxLayout *horizontalLayout_26;
    QGridLayout *gridLayout_5;
    QLineEdit *lineEdit_locLat;
    QLabel *label_13;
    QLabel *label_12;
    QLineEdit *lineEdit_locLng;
    QPushButton *pushButton_locSave;
    clickable_QWidget *widget_archive_sidebar_score_header;
    QHBoxLayout *horizontalLayout_36;
    QLabel *label_31;
    QLabel *label_24;
    QLabel *label_archive_sidebar_score;
    QSpacerItem *horizontalSpacer_15;
    QLabel *label_archive_sidebar_score_toggle;
    QWidget *widget_archive_sidebar_score_shadow;
    QWidget *widget_archive_sidebar_score_content;
    QVBoxLayout *verticalLayout_13;
    QLabel *label_score;
    QHBoxLayout *horizontalLayout_20;
    QSpacerItem *horizontalSpacer_26;
    QLabel *label_scoreStar1;
    QLabel *label_scoreStar2;
    QLabel *label_scoreStar3;
    QLabel *label_scoreStar4;
    QLabel *label_scoreStar5;
    QSpacerItem *horizontalSpacer_27;
    clickable_QWidget *widget_archive_sidebar_sites_header;
    QHBoxLayout *horizontalLayout_38;
    QLabel *label_32;
    QLabel *label_27;
    QLabel *label_archive_sidebar_sites;
    QSpacerItem *horizontalSpacer_22;
    QLabel *label_archive_sidebar_sites_toggle;
    QWidget *widget_archive_sidebar_sites_shadow;
    QWidget *widget_archive_sidebar_sites_content;
    QVBoxLayout *verticalLayout_19;
    QGridLayout *gridLayout_3;
    QLabel *label_siteFlickrViews;
    QHBoxLayout *horizontalLayout_17;
    clickable_QLabel *label_siteDaIcon;
    QLabel *label_siteFBFavs;
    QLabel *label_siteFBViews;
    QHBoxLayout *horizontalLayout_19;
    clickable_QLabel *label_siteGPIcon;
    QHBoxLayout *horizontalLayout_18;
    clickable_QLabel *label_site500pxIcon;
    QLabel *label_siteFlickrFavs;
    QLabel *label_siteGPFavs;
    QLabel *label_9;
    QLabel *label_10;
    QSpacerItem *verticalSpacer_9;
    QHBoxLayout *horizontalLayout_28;
    clickable_QLabel *label_sitePixotoIcon;
    QLabel *label_sitePixotoViews;
    QLabel *label_sitePixotoFavs;
    QLabel *label_siteYoupicViews;
    QLabel *label_siteYoupicFavs;
    QHBoxLayout *horizontalLayout_44;
    clickable_QLabel *label_siteYoupicIcon;
    QLabel *label_18;
    QHBoxLayout *horizontalLayout_22;
    clickable_QLabel *label_siteFlickrIcon;
    QHBoxLayout *horizontalLayout_23;
    clickable_QLabel *label_siteFBIcon;
    QLabel *label_site500pxFavs;
    QLabel *label_siteGPViews;
    QLabel *label_siteDaFavs;
    QLabel *label_siteDaViews;
    QLabel *label_site500pxViews;
    QLabel *label_19;
    QHBoxLayout *horizontalLayout_45;
    clickable_QLabel *label_siteViewbugIcon;
    QLabel *label_siteViewbugViews;
    QLabel *label_siteViewbugFavs;
    QHBoxLayout *horizontalLayout_46;
    clickable_QLabel *label_siteRdIcon;
    QLabel *label_siteRdViews;
    QLabel *label_siteRdFavs;
    QHBoxLayout *horizontalLayout_47;
    clickable_QLabel *label_siteIgIcon;
    QLabel *label_siteIgViews;
    QLabel *label_siteIgFavs;
    QFrame *frame_line;
    QGridLayout *gridLayout_4;
    QLabel *label_siteTotalFavs;
    QLabel *label_siteTotalViews;
    QLabel *label_20;
    QLabel *label_8;
    QLabel *label_21;
    QLabel *label_4;
    QVBoxLayout *verticalLayout_lineGraph;
    lineGraphWidget *widget_lineGraph;
    QFrame *frame_line_2;
    QHBoxLayout *horizontalLayout_14;
    QLabel *label_35;
    QLabel *label_archive_uptoDate;
    QHBoxLayout *horizontalLayout_15;
    QSpacerItem *horizontalSpacer_20;
    QPushButton *pushButton_sitesManage;
    QPushButton *pushButton_sitesUpdate;
    QPushButton *pushButton_addNewSite;
    clickable_QWidget *widget_archive_sidebar_keywords_header;
    QHBoxLayout *horizontalLayout_40;
    QLabel *label_33;
    QLabel *label_25;
    QLabel *label_archive_sidebar_keywords;
    QSpacerItem *horizontalSpacer_24;
    QLabel *label_archive_sidebar_keywords_toggle;
    QWidget *widget_archive_sidebar_keywords_shadow;
    QWidget *widget_archive_sidebar_keywords_content;
    QVBoxLayout *verticalLayout_20;
    QPlainTextEdit *plainTextEdit_archiveKeywords;
    QLineEdit *lineEdit_addKeyword;
    QHBoxLayout *horizontalLayout_5;
    QPushButton *pushButton_quickTag;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *pushButton_archiveCopyKeyComma;
    QPushButton *pushButton_archiveCopyKeySpace;
    QPushButton *pushButton_archiveUpdateKeywords;
    QListView *listView_quickTag;
    clickable_QWidget *widget_archive_sidebar_notes_header;
    QHBoxLayout *horizontalLayout_41;
    QLabel *label_34;
    QLabel *label_26;
    QLabel *label_archive_sidebar_notes;
    QSpacerItem *horizontalSpacer_25;
    QLabel *label_archive_sidebar_notes_toggle;
    QWidget *widget_archive_sidebar_notes_shadow;
    QWidget *widget_archive_sidebar_notes_content;
    QVBoxLayout *verticalLayout_21;
    QPlainTextEdit *plainTextEdit_notes;
    QHBoxLayout *horizontalLayout_11;
    QSpacerItem *horizontalSpacer_8;
    QPushButton *pushButton_archiveUpdateNotes;
    QSpacerItem *verticalSpacer;
    QWidget *cleanstart_frame;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_cleanStart;
    QGridLayout *gridLayout_2;
    QHBoxLayout *horizontalLayout;
    QCheckBox *checkBox_autoLoad;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_loadFromList;
    QPushButton *pushButton_cleanStart_loadLib;
    QPushButton *pushButton_cleanStart_createLib;
    QSpacerItem *horizontalSpacer_18;
    QListWidget *listWidget_recentFiles;
    QSpacerItem *horizontalSpacer_17;
    QLabel *label_recentFiles;
    QSpacerItem *verticalSpacer_2;
    QMenuBar *menuBar;
    QMenu *menuTools;
    QMenu *menuFile;
    QMenu *menuProcess;
    QMenu *menuArchive;
    QMenu *menuView;
    QMenu *menuSites;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;
    QToolBar *toolBarTabSwitcher;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1130, 992);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setAcceptDrops(true);
        actionOptions = new QAction(MainWindow);
        actionOptions->setObjectName(QStringLiteral("actionOptions"));
        QIcon icon;
        icon.addFile(QStringLiteral(":/icons/icons/tools_32.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionOptions->setIcon(icon);
        actionNew = new QAction(MainWindow);
        actionNew->setObjectName(QStringLiteral("actionNew"));
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/icons/icons/library-add-icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionNew->setIcon(icon1);
        actionOpen = new QAction(MainWindow);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/icons/icons/library-open-icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionOpen->setIcon(icon2);
        actionClose = new QAction(MainWindow);
        actionClose->setObjectName(QStringLiteral("actionClose"));
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/icons/icons/library-close-icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionClose->setIcon(icon3);
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QStringLiteral("actionExit"));
        QIcon icon4;
        icon4.addFile(QStringLiteral(":/icons/icons/close_16.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionExit->setIcon(icon4);
        actionAdd_newProcess = new QAction(MainWindow);
        actionAdd_newProcess->setObjectName(QStringLiteral("actionAdd_newProcess"));
        QIcon icon5;
        icon5.addFile(QStringLiteral(":/icons/icons/rotator_1.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionAdd_newProcess->setIcon(icon5);
        actionAdd_newArchive = new QAction(MainWindow);
        actionAdd_newArchive->setObjectName(QStringLiteral("actionAdd_newArchive"));
        QIcon icon6;
        icon6.addFile(QStringLiteral(":/icons/icons/archive.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionAdd_newArchive->setIcon(icon6);
        actionProcess = new QAction(MainWindow);
        actionProcess->setObjectName(QStringLiteral("actionProcess"));
        actionProcess->setCheckable(true);
        actionBrowse = new QAction(MainWindow);
        actionBrowse->setObjectName(QStringLiteral("actionBrowse"));
        actionBrowse->setCheckable(true);
        actionArchive = new QAction(MainWindow);
        actionArchive->setObjectName(QStringLiteral("actionArchive"));
        actionArchive->setCheckable(true);
        actionNotes = new QAction(MainWindow);
        actionNotes->setObjectName(QStringLiteral("actionNotes"));
        QIcon icon7;
        icon7.addFile(QStringLiteral(":/icons/icons/Sticky_Notes.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionNotes->setIcon(icon7);
        actionTable_View = new QAction(MainWindow);
        actionTable_View->setObjectName(QStringLiteral("actionTable_View"));
        QIcon icon8;
        icon8.addFile(QStringLiteral(":/icons/icons/table_icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionTable_View->setIcon(icon8);
        actionFolder_Sizes = new QAction(MainWindow);
        actionFolder_Sizes->setObjectName(QStringLiteral("actionFolder_Sizes"));
        QIcon icon9;
        icon9.addFile(QStringLiteral(":/icons/icons/disk-icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionFolder_Sizes->setIcon(icon9);
        actionAdd_new_Archive_toolBar = new QAction(MainWindow);
        actionAdd_new_Archive_toolBar->setObjectName(QStringLiteral("actionAdd_new_Archive_toolBar"));
        actionAdd_new_Archive_toolBar->setIcon(icon6);
        actionAdd_new_Process_toolBar = new QAction(MainWindow);
        actionAdd_new_Process_toolBar->setObjectName(QStringLiteral("actionAdd_new_Process_toolBar"));
        actionAdd_new_Process_toolBar->setIcon(icon5);
        actionMove_to_Archive_toolBar = new QAction(MainWindow);
        actionMove_to_Archive_toolBar->setObjectName(QStringLiteral("actionMove_to_Archive_toolBar"));
        QIcon icon10;
        icon10.addFile(QStringLiteral(":/icons/icons/convert-icon.PNG"), QSize(), QIcon::Normal, QIcon::Off);
        actionMove_to_Archive_toolBar->setIcon(icon10);
        actionShow_toolbar_text = new QAction(MainWindow);
        actionShow_toolbar_text->setObjectName(QStringLiteral("actionShow_toolbar_text"));
        actionShow_toolbar_text->setCheckable(true);
        actionSites = new QAction(MainWindow);
        actionSites->setObjectName(QStringLiteral("actionSites"));
        QIcon icon11;
        icon11.addFile(QStringLiteral(":/icons/icons/siteIconPixoto1.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSites->setIcon(icon11);
        actionScore = new QAction(MainWindow);
        actionScore->setObjectName(QStringLiteral("actionScore"));
        QIcon icon12;
        icon12.addFile(QStringLiteral(":/icons/icons/star_32.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionScore->setIcon(icon12);
        actionSite_updater = new QAction(MainWindow);
        actionSite_updater->setObjectName(QStringLiteral("actionSite_updater"));
        QIcon icon13;
        icon13.addFile(QStringLiteral(":/icons/icons/arrow_up_32.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSite_updater->setIcon(icon13);
        actionPreferences = new QAction(MainWindow);
        actionPreferences->setObjectName(QStringLiteral("actionPreferences"));
        actionMonthly_uploads = new QAction(MainWindow);
        actionMonthly_uploads->setObjectName(QStringLiteral("actionMonthly_uploads"));
        QIcon icon14;
        icon14.addFile(QStringLiteral(":/icons/icons/calendar.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionMonthly_uploads->setIcon(icon14);
        actionKeywords = new QAction(MainWindow);
        actionKeywords->setObjectName(QStringLiteral("actionKeywords"));
        actionAutomatic_Tonemap = new QAction(MainWindow);
        actionAutomatic_Tonemap->setObjectName(QStringLiteral("actionAutomatic_Tonemap"));
        actionSync_with_Sentinel = new QAction(MainWindow);
        actionSync_with_Sentinel->setObjectName(QStringLiteral("actionSync_with_Sentinel"));
        QIcon icon15;
        icon15.addFile(QStringLiteral(":/icons/icons/sync.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSync_with_Sentinel->setIcon(icon15);
        actionUpload_Times = new QAction(MainWindow);
        actionUpload_Times->setObjectName(QStringLiteral("actionUpload_Times"));
        QIcon icon16;
        icon16.addFile(QStringLiteral(":/icons/icons/clock_32.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionUpload_Times->setIcon(icon16);
        actionFollowers_Pageviews = new QAction(MainWindow);
        actionFollowers_Pageviews->setObjectName(QStringLiteral("actionFollowers_Pageviews"));
        actionInstagram_Tags = new QAction(MainWindow);
        actionInstagram_Tags->setObjectName(QStringLiteral("actionInstagram_Tags"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setAcceptDrops(true);
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        mainStack = new QStackedWidget(centralWidget);
        mainStack->setObjectName(QStringLiteral("mainStack"));
        mainStack->setLineWidth(0);
        main_frame = new QWidget();
        main_frame->setObjectName(QStringLiteral("main_frame"));
        horizontalLayout_2 = new QHBoxLayout(main_frame);
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        main_frame_vlayout_top = new QVBoxLayout();
        main_frame_vlayout_top->setSpacing(6);
        main_frame_vlayout_top->setObjectName(QStringLiteral("main_frame_vlayout_top"));
        main_frame_vlayout_top->setContentsMargins(-1, 9, 0, 0);
        tabWidget = new QTabWidget(main_frame);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tab_process = new QWidget();
        tab_process->setObjectName(QStringLiteral("tab_process"));
        horizontalLayout_3 = new QHBoxLayout(tab_process);
        horizontalLayout_3->setSpacing(0);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(4, 0, 4, 0);
        widget_8 = new QWidget(tab_process);
        widget_8->setObjectName(QStringLiteral("widget_8"));
        verticalLayout_9 = new QVBoxLayout(widget_8);
        verticalLayout_9->setSpacing(6);
        verticalLayout_9->setContentsMargins(11, 11, 11, 11);
        verticalLayout_9->setObjectName(QStringLiteral("verticalLayout_9"));
        verticalLayout_9->setContentsMargins(-1, 4, 0, -1);
        label_14 = new QLabel(widget_8);
        label_14->setObjectName(QStringLiteral("label_14"));
        QFont font;
        font.setPointSize(8);
        font.setBold(false);
        font.setWeight(50);
        label_14->setFont(font);

        verticalLayout_9->addWidget(label_14);

        stackedWidget_processLeft = new QStackedWidget(widget_8);
        stackedWidget_processLeft->setObjectName(QStringLiteral("stackedWidget_processLeft"));
        stackedWidget_processLeft->setMinimumSize(QSize(230, 0));
        stackedWidget_processLeft->setMaximumSize(QSize(230, 16777215));
        page = new QWidget();
        page->setObjectName(QStringLiteral("page"));
        verticalLayout_4 = new QVBoxLayout(page);
        verticalLayout_4->setSpacing(0);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        listView_processTree = new QListView(page);
        listView_processTree->setObjectName(QStringLiteral("listView_processTree"));
        listView_processTree->setMinimumSize(QSize(0, 0));
        listView_processTree->setMaximumSize(QSize(16777215, 16777215));
        listView_processTree->setEditTriggers(QAbstractItemView::NoEditTriggers);

        verticalLayout_4->addWidget(listView_processTree);

        stackedWidget_processLeft->addWidget(page);
        page_2 = new QWidget();
        page_2->setObjectName(QStringLiteral("page_2"));
        verticalLayout_6 = new QVBoxLayout(page_2);
        verticalLayout_6->setSpacing(0);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        verticalLayout_6->setContentsMargins(0, 0, 0, 0);
        listView_processNewBrowser = new SmoothScrollListView(page_2);
        listView_processNewBrowser->setObjectName(QStringLiteral("listView_processNewBrowser"));

        verticalLayout_6->addWidget(listView_processNewBrowser);

        stackedWidget_processLeft->addWidget(page_2);

        verticalLayout_9->addWidget(stackedWidget_processLeft);


        horizontalLayout_3->addWidget(widget_8);

        pushButton_aniMate = new QPushButton(tab_process);
        pushButton_aniMate->setObjectName(QStringLiteral("pushButton_aniMate"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(pushButton_aniMate->sizePolicy().hasHeightForWidth());
        pushButton_aniMate->setSizePolicy(sizePolicy1);
        pushButton_aniMate->setMaximumSize(QSize(10, 16777215));
        pushButton_aniMate->setCursor(QCursor(Qt::ArrowCursor));
        QIcon icon17;
        icon17.addFile(QStringLiteral(":/icons/icons/stylesheet-branch-closed.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_aniMate->setIcon(icon17);

        horizontalLayout_3->addWidget(pushButton_aniMate);

        horizontalSpacer_19 = new QSpacerItem(9, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_19);

        vlayout_process_right = new QVBoxLayout();
        vlayout_process_right->setSpacing(0);
        vlayout_process_right->setObjectName(QStringLiteral("vlayout_process_right"));
        widget_3 = new QWidget(tab_process);
        widget_3->setObjectName(QStringLiteral("widget_3"));
        widget_3->setMinimumSize(QSize(0, 300));
        verticalLayout_37 = new QVBoxLayout(widget_3);
        verticalLayout_37->setSpacing(6);
        verticalLayout_37->setContentsMargins(11, 11, 11, 11);
        verticalLayout_37->setObjectName(QStringLiteral("verticalLayout_37"));
        listView_processFiles = new SmoothScrollListView(widget_3);
        listView_processFiles->setObjectName(QStringLiteral("listView_processFiles"));
        listView_processFiles->setEditTriggers(QAbstractItemView::NoEditTriggers);

        verticalLayout_37->addWidget(listView_processFiles);


        vlayout_process_right->addWidget(widget_3);


        horizontalLayout_3->addLayout(vlayout_process_right);

        widget = new QWidget(tab_process);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setMinimumSize(QSize(250, 0));
        widget->setMaximumSize(QSize(250, 16777215));
        verticalLayout_30 = new QVBoxLayout(widget);
        verticalLayout_30->setSpacing(0);
        verticalLayout_30->setContentsMargins(11, 11, 11, 11);
        verticalLayout_30->setObjectName(QStringLiteral("verticalLayout_30"));
        verticalLayout_30->setContentsMargins(0, 0, -1, 0);
        widget_4 = new QWidget(widget);
        widget_4->setObjectName(QStringLiteral("widget_4"));
        verticalLayout_33 = new QVBoxLayout(widget_4);
        verticalLayout_33->setSpacing(6);
        verticalLayout_33->setContentsMargins(11, 11, 11, 11);
        verticalLayout_33->setObjectName(QStringLiteral("verticalLayout_33"));
        verticalLayout_33->setContentsMargins(-1, 0, 0, 0);
        widget_6 = new QWidget(widget_4);
        widget_6->setObjectName(QStringLiteral("widget_6"));
        verticalLayout_35 = new QVBoxLayout(widget_6);
        verticalLayout_35->setSpacing(6);
        verticalLayout_35->setContentsMargins(11, 11, 11, 11);
        verticalLayout_35->setObjectName(QStringLiteral("verticalLayout_35"));
        label_processTitle = new QLabel(widget_6);
        label_processTitle->setObjectName(QStringLiteral("label_processTitle"));

        verticalLayout_35->addWidget(label_processTitle);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(0);
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        horizontalLayout_9->setContentsMargins(-1, 0, -1, -1);
        label_processPath = new QLabel(widget_6);
        label_processPath->setObjectName(QStringLiteral("label_processPath"));
        label_processPath->setMinimumSize(QSize(200, 0));
        label_processPath->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_9->addWidget(label_processPath);

        pushButton_processPathToClipboard = new QPushButton(widget_6);
        pushButton_processPathToClipboard->setObjectName(QStringLiteral("pushButton_processPathToClipboard"));
        pushButton_processPathToClipboard->setMinimumSize(QSize(16, 16));
        pushButton_processPathToClipboard->setMaximumSize(QSize(16, 16));
        QIcon icon18;
        icon18.addFile(QStringLiteral(":/icons/icons/clipboard-icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_processPathToClipboard->setIcon(icon18);
        pushButton_processPathToClipboard->setFlat(true);

        horizontalLayout_9->addWidget(pushButton_processPathToClipboard);


        verticalLayout_35->addLayout(horizontalLayout_9);

        label_processPano = new QLabel(widget_6);
        label_processPano->setObjectName(QStringLiteral("label_processPano"));
        label_processPano->setMinimumSize(QSize(200, 0));
        label_processPano->setAlignment(Qt::AlignBottom|Qt::AlignLeading|Qt::AlignLeft);

        verticalLayout_35->addWidget(label_processPano);

        verticalSpacer_7 = new QSpacerItem(20, 10, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_35->addItem(verticalSpacer_7);

        gridLayout_7 = new QGridLayout();
        gridLayout_7->setSpacing(6);
        gridLayout_7->setObjectName(QStringLiteral("gridLayout_7"));
        gridLayout_7->setContentsMargins(-1, 0, -1, -1);
        pushButton_processProperties = new QPushButton(widget_6);
        pushButton_processProperties->setObjectName(QStringLiteral("pushButton_processProperties"));
        pushButton_processProperties->setMinimumSize(QSize(80, 0));
        pushButton_processProperties->setIcon(icon8);
        pushButton_processProperties->setFlat(false);

        gridLayout_7->addWidget(pushButton_processProperties, 1, 0, 1, 1);

        pushButton_processOpenFolder = new QPushButton(widget_6);
        pushButton_processOpenFolder->setObjectName(QStringLiteral("pushButton_processOpenFolder"));
        pushButton_processOpenFolder->setMinimumSize(QSize(80, 0));
        QIcon icon19;
        icon19.addFile(QStringLiteral(":/icons/icons/folder-icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_processOpenFolder->setIcon(icon19);
        pushButton_processOpenFolder->setFlat(false);

        gridLayout_7->addWidget(pushButton_processOpenFolder, 2, 0, 1, 1);

        pushButton_processToArchive = new QPushButton(widget_6);
        pushButton_processToArchive->setObjectName(QStringLiteral("pushButton_processToArchive"));
        pushButton_processToArchive->setMinimumSize(QSize(80, 0));
        pushButton_processToArchive->setIcon(icon10);
        pushButton_processToArchive->setFlat(false);

        gridLayout_7->addWidget(pushButton_processToArchive, 2, 1, 1, 1);

        pushButton_processDelete = new QPushButton(widget_6);
        pushButton_processDelete->setObjectName(QStringLiteral("pushButton_processDelete"));
        pushButton_processDelete->setMinimumSize(QSize(80, 0));
        pushButton_processDelete->setIcon(icon4);
        pushButton_processDelete->setFlat(false);

        gridLayout_7->addWidget(pushButton_processDelete, 1, 1, 1, 1);


        verticalLayout_35->addLayout(gridLayout_7);


        verticalLayout_33->addWidget(widget_6);


        verticalLayout_30->addWidget(widget_4);

        widget_5 = new QWidget(widget);
        widget_5->setObjectName(QStringLiteral("widget_5"));
        verticalLayout_34 = new QVBoxLayout(widget_5);
        verticalLayout_34->setSpacing(6);
        verticalLayout_34->setContentsMargins(11, 11, 11, 11);
        verticalLayout_34->setObjectName(QStringLiteral("verticalLayout_34"));
        verticalLayout_34->setContentsMargins(0, -1, 0, -1);
        pushButton_processFileGroupOrigall = new QPushButton(widget_5);
        pushButton_processFileGroupOrigall->setObjectName(QStringLiteral("pushButton_processFileGroupOrigall"));
        pushButton_processFileGroupOrigall->setCursor(QCursor(Qt::PointingHandCursor));
        pushButton_processFileGroupOrigall->setCheckable(true);
        pushButton_processFileGroupOrigall->setChecked(true);

        verticalLayout_34->addWidget(pushButton_processFileGroupOrigall);

        pushButton_processFileGroupEV0 = new QPushButton(widget_5);
        pushButton_processFileGroupEV0->setObjectName(QStringLiteral("pushButton_processFileGroupEV0"));
        pushButton_processFileGroupEV0->setCursor(QCursor(Qt::PointingHandCursor));
        pushButton_processFileGroupEV0->setCheckable(true);

        verticalLayout_34->addWidget(pushButton_processFileGroupEV0);

        pushButton_processFileGroupEV1 = new QPushButton(widget_5);
        pushButton_processFileGroupEV1->setObjectName(QStringLiteral("pushButton_processFileGroupEV1"));
        pushButton_processFileGroupEV1->setCursor(QCursor(Qt::PointingHandCursor));
        pushButton_processFileGroupEV1->setCheckable(true);

        verticalLayout_34->addWidget(pushButton_processFileGroupEV1);

        pushButton_processFileGroupEV2 = new QPushButton(widget_5);
        pushButton_processFileGroupEV2->setObjectName(QStringLiteral("pushButton_processFileGroupEV2"));
        pushButton_processFileGroupEV2->setCursor(QCursor(Qt::PointingHandCursor));
        pushButton_processFileGroupEV2->setCheckable(true);

        verticalLayout_34->addWidget(pushButton_processFileGroupEV2);

        pushButton_processFileGroupPanorama = new QPushButton(widget_5);
        pushButton_processFileGroupPanorama->setObjectName(QStringLiteral("pushButton_processFileGroupPanorama"));
        pushButton_processFileGroupPanorama->setCursor(QCursor(Qt::PointingHandCursor));
        pushButton_processFileGroupPanorama->setCheckable(true);

        verticalLayout_34->addWidget(pushButton_processFileGroupPanorama);

        pushButton_processFileGroupHDR = new QPushButton(widget_5);
        pushButton_processFileGroupHDR->setObjectName(QStringLiteral("pushButton_processFileGroupHDR"));
        pushButton_processFileGroupHDR->setCursor(QCursor(Qt::PointingHandCursor));
        pushButton_processFileGroupHDR->setCheckable(true);

        verticalLayout_34->addWidget(pushButton_processFileGroupHDR);

        pushButton_processFileGroupPostProcess = new QPushButton(widget_5);
        pushButton_processFileGroupPostProcess->setObjectName(QStringLiteral("pushButton_processFileGroupPostProcess"));
        pushButton_processFileGroupPostProcess->setCursor(QCursor(Qt::PointingHandCursor));
        pushButton_processFileGroupPostProcess->setCheckable(true);

        verticalLayout_34->addWidget(pushButton_processFileGroupPostProcess);


        verticalLayout_30->addWidget(widget_5);

        widget_2 = new QWidget(widget);
        widget_2->setObjectName(QStringLiteral("widget_2"));
        verticalLayout_32 = new QVBoxLayout(widget_2);
        verticalLayout_32->setSpacing(6);
        verticalLayout_32->setContentsMargins(11, 11, 11, 11);
        verticalLayout_32->setObjectName(QStringLiteral("verticalLayout_32"));
        verticalLayout_32->setContentsMargins(-1, 0, 0, 0);
        widget_7 = new QWidget(widget_2);
        widget_7->setObjectName(QStringLiteral("widget_7"));
        verticalLayout_36 = new QVBoxLayout(widget_7);
        verticalLayout_36->setSpacing(6);
        verticalLayout_36->setContentsMargins(11, 11, 11, 11);
        verticalLayout_36->setObjectName(QStringLiteral("verticalLayout_36"));
        gridLayout_6 = new QGridLayout();
        gridLayout_6->setSpacing(6);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        gridLayout_6->setContentsMargins(-1, 0, -1, 0);
        pushButton_processHDR = new QPushButton(widget_7);
        pushButton_processHDR->setObjectName(QStringLiteral("pushButton_processHDR"));
        pushButton_processHDR->setEnabled(false);
        QFont font1;
        font1.setBold(true);
        font1.setWeight(75);
        pushButton_processHDR->setFont(font1);
        QIcon icon20;
        icon20.addFile(QStringLiteral(":/icons/icons/appIcon-Photomatix.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_processHDR->setIcon(icon20);

        gridLayout_6->addWidget(pushButton_processHDR, 5, 0, 1, 1);

        pushButton_processStich = new QPushButton(widget_7);
        pushButton_processStich->setObjectName(QStringLiteral("pushButton_processStich"));
        pushButton_processStich->setEnabled(false);
        pushButton_processStich->setFont(font1);
        QIcon icon21;
        icon21.addFile(QStringLiteral(":/icons/icons/appIcon-PTGui.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_processStich->setIcon(icon21);

        gridLayout_6->addWidget(pushButton_processStich, 7, 0, 1, 1);

        pushButton_processBatchStich = new QPushButton(widget_7);
        pushButton_processBatchStich->setObjectName(QStringLiteral("pushButton_processBatchStich"));
        pushButton_processBatchStich->setEnabled(false);
        pushButton_processBatchStich->setIcon(icon21);

        gridLayout_6->addWidget(pushButton_processBatchStich, 7, 1, 1, 1);

        pushButton_processClonePTS = new QPushButton(widget_7);
        pushButton_processClonePTS->setObjectName(QStringLiteral("pushButton_processClonePTS"));
        pushButton_processClonePTS->setEnabled(false);
        QIcon icon22;
        icon22.addFile(QStringLiteral(":/icons/icons/duplicate-icon-md.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_processClonePTS->setIcon(icon22);

        gridLayout_6->addWidget(pushButton_processClonePTS, 5, 1, 1, 1);

        pushButton_processAlltoPS = new QPushButton(widget_7);
        pushButton_processAlltoPS->setObjectName(QStringLiteral("pushButton_processAlltoPS"));
        pushButton_processAlltoPS->setEnabled(false);
        QIcon icon23;
        icon23.addFile(QStringLiteral(":/icons/icons/appIcon-PhotoShop.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_processAlltoPS->setIcon(icon23);

        gridLayout_6->addWidget(pushButton_processAlltoPS, 8, 0, 1, 1);


        verticalLayout_36->addLayout(gridLayout_6);


        verticalLayout_32->addWidget(widget_7);

        widget_9 = new QWidget(widget_2);
        widget_9->setObjectName(QStringLiteral("widget_9"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(widget_9->sizePolicy().hasHeightForWidth());
        widget_9->setSizePolicy(sizePolicy2);
        widget_9->setMinimumSize(QSize(0, 0));
        verticalLayout_10 = new QVBoxLayout(widget_9);
        verticalLayout_10->setSpacing(6);
        verticalLayout_10->setContentsMargins(11, 11, 11, 11);
        verticalLayout_10->setObjectName(QStringLiteral("verticalLayout_10"));
        label_processFileName = new QLabel(widget_9);
        label_processFileName->setObjectName(QStringLiteral("label_processFileName"));
        label_processFileName->setFont(font1);

        verticalLayout_10->addWidget(label_processFileName);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setSpacing(6);
        horizontalLayout_12->setObjectName(QStringLiteral("horizontalLayout_12"));
        horizontalLayout_12->setContentsMargins(-1, -1, -1, 0);
        label_process_fileinfoPreview = new QLabel(widget_9);
        label_process_fileinfoPreview->setObjectName(QStringLiteral("label_process_fileinfoPreview"));
        QSizePolicy sizePolicy3(QSizePolicy::Fixed, QSizePolicy::MinimumExpanding);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(label_process_fileinfoPreview->sizePolicy().hasHeightForWidth());
        label_process_fileinfoPreview->setSizePolicy(sizePolicy3);
        label_process_fileinfoPreview->setMinimumSize(QSize(100, 50));
        label_process_fileinfoPreview->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        horizontalLayout_12->addWidget(label_process_fileinfoPreview);

        gridLayout_9 = new QGridLayout();
        gridLayout_9->setSpacing(6);
        gridLayout_9->setObjectName(QStringLiteral("gridLayout_9"));
        gridLayout_9->setContentsMargins(-1, 0, -1, -1);
        pushButton_processFileOpen = new QPushButton(widget_9);
        pushButton_processFileOpen->setObjectName(QStringLiteral("pushButton_processFileOpen"));
        pushButton_processFileOpen->setIcon(icon19);

        gridLayout_9->addWidget(pushButton_processFileOpen, 1, 1, 1, 1);

        pushButton_processFileEdit = new QPushButton(widget_9);
        pushButton_processFileEdit->setObjectName(QStringLiteral("pushButton_processFileEdit"));
        pushButton_processFileEdit->setIcon(icon23);

        gridLayout_9->addWidget(pushButton_processFileEdit, 0, 1, 1, 1);

        verticalSpacer_8 = new QSpacerItem(20, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_9->addItem(verticalSpacer_8, 2, 1, 1, 1);


        horizontalLayout_12->addLayout(gridLayout_9);


        verticalLayout_10->addLayout(horizontalLayout_12);

        gridLayout_8 = new QGridLayout();
        gridLayout_8->setSpacing(6);
        gridLayout_8->setObjectName(QStringLiteral("gridLayout_8"));
        gridLayout_8->setContentsMargins(-1, 0, -1, -1);
        label_process_fileinfoType = new QLabel(widget_9);
        label_process_fileinfoType->setObjectName(QStringLiteral("label_process_fileinfoType"));
        label_process_fileinfoType->setFont(font1);

        gridLayout_8->addWidget(label_process_fileinfoType, 0, 1, 1, 1);

        label_15 = new QLabel(widget_9);
        label_15->setObjectName(QStringLiteral("label_15"));

        gridLayout_8->addWidget(label_15, 0, 0, 1, 1);

        label_process_fileinfoCreated = new QLabel(widget_9);
        label_process_fileinfoCreated->setObjectName(QStringLiteral("label_process_fileinfoCreated"));
        label_process_fileinfoCreated->setFont(font1);

        gridLayout_8->addWidget(label_process_fileinfoCreated, 2, 1, 1, 1);

        label_17 = new QLabel(widget_9);
        label_17->setObjectName(QStringLiteral("label_17"));

        gridLayout_8->addWidget(label_17, 2, 0, 1, 1);

        label_process_fileinfoSize = new QLabel(widget_9);
        label_process_fileinfoSize->setObjectName(QStringLiteral("label_process_fileinfoSize"));
        label_process_fileinfoSize->setFont(font1);

        gridLayout_8->addWidget(label_process_fileinfoSize, 1, 1, 1, 1);

        label_16 = new QLabel(widget_9);
        label_16->setObjectName(QStringLiteral("label_16"));

        gridLayout_8->addWidget(label_16, 1, 0, 1, 1);

        label_process_fileinfoResolutionLabel = new QLabel(widget_9);
        label_process_fileinfoResolutionLabel->setObjectName(QStringLiteral("label_process_fileinfoResolutionLabel"));

        gridLayout_8->addWidget(label_process_fileinfoResolutionLabel, 3, 0, 1, 1);

        label_process_fileinfoResolution = new QLabel(widget_9);
        label_process_fileinfoResolution->setObjectName(QStringLiteral("label_process_fileinfoResolution"));
        label_process_fileinfoResolution->setFont(font1);

        gridLayout_8->addWidget(label_process_fileinfoResolution, 3, 1, 1, 1);

        gridLayout_8->setColumnStretch(0, 1);
        gridLayout_8->setColumnStretch(1, 5);

        verticalLayout_10->addLayout(gridLayout_8);


        verticalLayout_32->addWidget(widget_9);

        verticalSpacer_6 = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);

        verticalLayout_32->addItem(verticalSpacer_6);

        verticalLayout_32->setStretch(2, 10);

        verticalLayout_30->addWidget(widget_2);

        verticalLayout_30->setStretch(2, 1);

        horizontalLayout_3->addWidget(widget);

        horizontalLayout_3->setStretch(3, 1);
        tabWidget->addTab(tab_process, QString());
        tab_processBrowser = new QWidget();
        tab_processBrowser->setObjectName(QStringLiteral("tab_processBrowser"));
        verticalLayout_3 = new QVBoxLayout(tab_processBrowser);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(4, 0, 4, 0);
        listView_processBrowser = new QListView(tab_processBrowser);
        listView_processBrowser->setObjectName(QStringLiteral("listView_processBrowser"));

        verticalLayout_3->addWidget(listView_processBrowser);

        tabWidget->addTab(tab_processBrowser, QString());
        tab_archives = new QWidget();
        tab_archives->setObjectName(QStringLiteral("tab_archives"));
        horizontalLayout_4 = new QHBoxLayout(tab_archives);
        horizontalLayout_4->setSpacing(4);
        horizontalLayout_4->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalLayout_4->setContentsMargins(4, 0, 4, 0);
        vlayout_archive_left = new QVBoxLayout();
        vlayout_archive_left->setSpacing(0);
        vlayout_archive_left->setObjectName(QStringLiteral("vlayout_archive_left"));
        vlayout_archive_left->setContentsMargins(0, 0, 0, -1);
        tabWidget_archiveTrees = new QTabWidget(tab_archives);
        tabWidget_archiveTrees->setObjectName(QStringLiteral("tabWidget_archiveTrees"));
        tabWidget_archiveTrees->setMaximumSize(QSize(248, 16777215));
        tabArchiveProjects = new QWidget();
        tabArchiveProjects->setObjectName(QStringLiteral("tabArchiveProjects"));
        verticalLayout_8 = new QVBoxLayout(tabArchiveProjects);
        verticalLayout_8->setSpacing(6);
        verticalLayout_8->setContentsMargins(11, 11, 11, 11);
        verticalLayout_8->setObjectName(QStringLiteral("verticalLayout_8"));
        verticalLayout_8->setContentsMargins(9, 9, 4, 9);
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(0);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        horizontalLayout_7->setContentsMargins(-1, 0, -1, -1);
        pushButton_archiveTreeExpand = new QPushButton(tabArchiveProjects);
        pushButton_archiveTreeExpand->setObjectName(QStringLiteral("pushButton_archiveTreeExpand"));
        pushButton_archiveTreeExpand->setMinimumSize(QSize(16, 16));
        pushButton_archiveTreeExpand->setMaximumSize(QSize(16, 16));
        pushButton_archiveTreeExpand->setToolTipDuration(-3);
        pushButton_archiveTreeExpand->setCheckable(true);

        horizontalLayout_7->addWidget(pushButton_archiveTreeExpand);

        horizontalSpacer_28 = new QSpacerItem(6, 10, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_28);

        pushButton_archiveTreeGroup = new QPushButton(tabArchiveProjects);
        pushButton_archiveTreeGroup->setObjectName(QStringLiteral("pushButton_archiveTreeGroup"));
        pushButton_archiveTreeGroup->setMinimumSize(QSize(22, 16));
        pushButton_archiveTreeGroup->setMaximumSize(QSize(22, 16));
        pushButton_archiveTreeGroup->setToolTipDuration(-3);
        pushButton_archiveTreeGroup->setCheckable(true);
        pushButton_archiveTreeGroup->setFlat(true);

        horizontalLayout_7->addWidget(pushButton_archiveTreeGroup);

        horizontalSpacer_21 = new QSpacerItem(6, 10, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_21);

        pushButton_archiveGroupByAz = new QPushButton(tabArchiveProjects);
        pushButton_archiveGroupByAz->setObjectName(QStringLiteral("pushButton_archiveGroupByAz"));
        pushButton_archiveGroupByAz->setMinimumSize(QSize(40, 16));
        pushButton_archiveGroupByAz->setMaximumSize(QSize(40, 16));
        pushButton_archiveGroupByAz->setFlat(true);

        horizontalLayout_7->addWidget(pushButton_archiveGroupByAz);

        pushButton_archiveGroupByDate = new QPushButton(tabArchiveProjects);
        pushButton_archiveGroupByDate->setObjectName(QStringLiteral("pushButton_archiveGroupByDate"));
        pushButton_archiveGroupByDate->setMinimumSize(QSize(40, 16));
        pushButton_archiveGroupByDate->setMaximumSize(QSize(40, 16));
        pushButton_archiveGroupByDate->setFlat(false);

        horizontalLayout_7->addWidget(pushButton_archiveGroupByDate);

        pushButton_archiveGroupByLoc = new QPushButton(tabArchiveProjects);
        pushButton_archiveGroupByLoc->setObjectName(QStringLiteral("pushButton_archiveGroupByLoc"));
        pushButton_archiveGroupByLoc->setMinimumSize(QSize(40, 16));
        pushButton_archiveGroupByLoc->setMaximumSize(QSize(40, 16));
        pushButton_archiveGroupByLoc->setFlat(false);

        horizontalLayout_7->addWidget(pushButton_archiveGroupByLoc);

        pushButton_archiveGroupByScore = new QPushButton(tabArchiveProjects);
        pushButton_archiveGroupByScore->setObjectName(QStringLiteral("pushButton_archiveGroupByScore"));
        pushButton_archiveGroupByScore->setMinimumSize(QSize(40, 16));
        pushButton_archiveGroupByScore->setMaximumSize(QSize(40, 16));
        pushButton_archiveGroupByScore->setIcon(icon12);
        pushButton_archiveGroupByScore->setIconSize(QSize(12, 12));
        pushButton_archiveGroupByScore->setFlat(false);

        horizontalLayout_7->addWidget(pushButton_archiveGroupByScore);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_4);


        verticalLayout_8->addLayout(horizontalLayout_7);

        treeView_archiveTree = new QTreeView(tabArchiveProjects);
        treeView_archiveTree->setObjectName(QStringLiteral("treeView_archiveTree"));
        treeView_archiveTree->setMinimumSize(QSize(230, 0));
        treeView_archiveTree->setMaximumSize(QSize(230, 16777215));
        treeView_archiveTree->setFocusPolicy(Qt::NoFocus);
        treeView_archiveTree->setEditTriggers(QAbstractItemView::NoEditTriggers);
        treeView_archiveTree->setIndentation(10);
        treeView_archiveTree->header()->setVisible(false);

        verticalLayout_8->addWidget(treeView_archiveTree);

        listView_archiveTree = new QListView(tabArchiveProjects);
        listView_archiveTree->setObjectName(QStringLiteral("listView_archiveTree"));
        listView_archiveTree->setMinimumSize(QSize(230, 0));
        listView_archiveTree->setMaximumSize(QSize(230, 16777215));
        listView_archiveTree->setTabKeyNavigation(true);

        verticalLayout_8->addWidget(listView_archiveTree);

        tabWidget_archiveTrees->addTab(tabArchiveProjects, QString());
        tab_16 = new QWidget();
        tab_16->setObjectName(QStringLiteral("tab_16"));
        verticalLayout_11 = new QVBoxLayout(tab_16);
        verticalLayout_11->setSpacing(6);
        verticalLayout_11->setContentsMargins(11, 11, 11, 11);
        verticalLayout_11->setObjectName(QStringLiteral("verticalLayout_11"));
        verticalLayout_11->setContentsMargins(9, -1, -1, -1);
        horizontalLayout_29 = new QHBoxLayout();
        horizontalLayout_29->setSpacing(6);
        horizontalLayout_29->setObjectName(QStringLiteral("horizontalLayout_29"));
        horizontalLayout_29->setContentsMargins(-1, 0, -1, -1);
        pushButton_addCollection = new QPushButton(tab_16);
        pushButton_addCollection->setObjectName(QStringLiteral("pushButton_addCollection"));
        pushButton_addCollection->setMinimumSize(QSize(16, 16));
        pushButton_addCollection->setMaximumSize(QSize(16, 16));

        horizontalLayout_29->addWidget(pushButton_addCollection);

        horizontalSpacer_31 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_29->addItem(horizontalSpacer_31);


        verticalLayout_11->addLayout(horizontalLayout_29);

        listView_collections = new QListView(tab_16);
        listView_collections->setObjectName(QStringLiteral("listView_collections"));
        listView_collections->setMinimumSize(QSize(230, 0));
        listView_collections->setMaximumSize(QSize(230, 16777215));
        listView_collections->setDragDropMode(QAbstractItemView::DragDrop);

        verticalLayout_11->addWidget(listView_collections);

        tabWidget_archiveTrees->addTab(tab_16, QString());

        vlayout_archive_left->addWidget(tabWidget_archiveTrees);

        widget_filter = new QWidget(tab_archives);
        widget_filter->setObjectName(QStringLiteral("widget_filter"));
        widget_filter->setMinimumSize(QSize(0, 20));
        verticalLayout_29 = new QVBoxLayout(widget_filter);
        verticalLayout_29->setSpacing(6);
        verticalLayout_29->setContentsMargins(11, 11, 11, 11);
        verticalLayout_29->setObjectName(QStringLiteral("verticalLayout_29"));
        verticalLayout_29->setContentsMargins(-1, 0, -1, -1);
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(0);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(-1, 0, 0, -1);
        lineEdit_archiveFilter = new QLineEdit(widget_filter);
        lineEdit_archiveFilter->setObjectName(QStringLiteral("lineEdit_archiveFilter"));
        lineEdit_archiveFilter->setMinimumSize(QSize(0, 0));
        lineEdit_archiveFilter->setMaximumSize(QSize(16777215, 16777215));
        QFont font2;
        font2.setPointSize(10);
        lineEdit_archiveFilter->setFont(font2);

        horizontalLayout_6->addWidget(lineEdit_archiveFilter);

        pushButton_archiveSearchByTitle = new QPushButton(widget_filter);
        pushButton_archiveSearchByTitle->setObjectName(QStringLiteral("pushButton_archiveSearchByTitle"));
        pushButton_archiveSearchByTitle->setMinimumSize(QSize(24, 16));
        pushButton_archiveSearchByTitle->setMaximumSize(QSize(24, 16));
        pushButton_archiveSearchByTitle->setFlat(true);

        horizontalLayout_6->addWidget(pushButton_archiveSearchByTitle);

        pushButton_archiveSearchByKeywords = new QPushButton(widget_filter);
        pushButton_archiveSearchByKeywords->setObjectName(QStringLiteral("pushButton_archiveSearchByKeywords"));
        pushButton_archiveSearchByKeywords->setMinimumSize(QSize(24, 16));
        pushButton_archiveSearchByKeywords->setMaximumSize(QSize(24, 16));
        pushButton_archiveSearchByKeywords->setFlat(false);

        horizontalLayout_6->addWidget(pushButton_archiveSearchByKeywords);

        pushButton_filterFields = new QPushButton(widget_filter);
        pushButton_filterFields->setObjectName(QStringLiteral("pushButton_filterFields"));
        pushButton_filterFields->setMinimumSize(QSize(20, 20));
        pushButton_filterFields->setMaximumSize(QSize(20, 20));

        horizontalLayout_6->addWidget(pushButton_filterFields);

        pushButton_archiveFilterClear = new QPushButton(widget_filter);
        pushButton_archiveFilterClear->setObjectName(QStringLiteral("pushButton_archiveFilterClear"));
        pushButton_archiveFilterClear->setMinimumSize(QSize(20, 20));
        pushButton_archiveFilterClear->setMaximumSize(QSize(20, 20));

        horizontalLayout_6->addWidget(pushButton_archiveFilterClear);


        verticalLayout_29->addLayout(horizontalLayout_6);


        vlayout_archive_left->addWidget(widget_filter);


        horizontalLayout_4->addLayout(vlayout_archive_left);

        tabWidget_archiveCenter = new QTabWidget(tab_archives);
        tabWidget_archiveCenter->setObjectName(QStringLiteral("tabWidget_archiveCenter"));
        tabWidget_archiveCenter->setTabsClosable(true);
        tab_archivePreview = new QWidget();
        tab_archivePreview->setObjectName(QStringLiteral("tab_archivePreview"));
        verticalLayout_15 = new QVBoxLayout(tab_archivePreview);
        verticalLayout_15->setSpacing(6);
        verticalLayout_15->setContentsMargins(11, 11, 11, 11);
        verticalLayout_15->setObjectName(QStringLiteral("verticalLayout_15"));
        widget_11 = new QWidget(tab_archivePreview);
        widget_11->setObjectName(QStringLiteral("widget_11"));
        verticalLayout_16 = new QVBoxLayout(widget_11);
        verticalLayout_16->setSpacing(0);
        verticalLayout_16->setContentsMargins(11, 11, 11, 11);
        verticalLayout_16->setObjectName(QStringLiteral("verticalLayout_16"));
        verticalLayout_16->setContentsMargins(0, 0, 0, 0);
        splitter = new QSplitter(widget_11);
        splitter->setObjectName(QStringLiteral("splitter"));
        splitter->setOrientation(Qt::Vertical);
        widget_10 = new QWidget(splitter);
        widget_10->setObjectName(QStringLiteral("widget_10"));
        verticalLayout_14 = new QVBoxLayout(widget_10);
        verticalLayout_14->setSpacing(0);
        verticalLayout_14->setContentsMargins(11, 11, 11, 11);
        verticalLayout_14->setObjectName(QStringLiteral("verticalLayout_14"));
        verticalLayout_14->setContentsMargins(0, 0, 0, 0);
        label_archiveTitle = new QLabel(widget_10);
        label_archiveTitle->setObjectName(QStringLiteral("label_archiveTitle"));
        label_archiveTitle->setTextInteractionFlags(Qt::LinksAccessibleByMouse|Qt::TextSelectableByMouse);

        verticalLayout_14->addWidget(label_archiveTitle);

        label_archiveDate = new QLabel(widget_10);
        label_archiveDate->setObjectName(QStringLiteral("label_archiveDate"));

        verticalLayout_14->addWidget(label_archiveDate);

        label_previewImage = new QLabel(widget_10);
        label_previewImage->setObjectName(QStringLiteral("label_previewImage"));
        QSizePolicy sizePolicy4(QSizePolicy::Maximum, QSizePolicy::Expanding);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(label_previewImage->sizePolicy().hasHeightForWidth());
        label_previewImage->setSizePolicy(sizePolicy4);
        label_previewImage->setMinimumSize(QSize(100, 100));
        label_previewImage->setMaximumSize(QSize(5000, 5000));
        label_previewImage->setAlignment(Qt::AlignCenter);

        verticalLayout_14->addWidget(label_previewImage);

        widget_navi = new QWidget(widget_10);
        widget_navi->setObjectName(QStringLiteral("widget_navi"));
        widget_navi->setMinimumSize(QSize(200, 0));
        horizontalLayout_13 = new QHBoxLayout(widget_navi);
        horizontalLayout_13->setSpacing(6);
        horizontalLayout_13->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_13->setObjectName(QStringLiteral("horizontalLayout_13"));
        horizontalLayout_13->setContentsMargins(0, 0, 0, 6);
        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_9);

        pushButton_naviPrev = new QPushButton(widget_navi);
        pushButton_naviPrev->setObjectName(QStringLiteral("pushButton_naviPrev"));
        pushButton_naviPrev->setCursor(QCursor(Qt::PointingHandCursor));

        horizontalLayout_13->addWidget(pushButton_naviPrev);

        horizontalSpacer_11 = new QSpacerItem(10, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_11);

        pushButton_naviNext = new QPushButton(widget_navi);
        pushButton_naviNext->setObjectName(QStringLiteral("pushButton_naviNext"));
        pushButton_naviNext->setCursor(QCursor(Qt::PointingHandCursor));

        horizontalLayout_13->addWidget(pushButton_naviNext);

        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_10);


        verticalLayout_14->addWidget(widget_navi);

        splitter->addWidget(widget_10);
        widget_archiveFolderNavi = new QStackedWidget(splitter);
        widget_archiveFolderNavi->setObjectName(QStringLiteral("widget_archiveFolderNavi"));
        widget_archiveFolderNavi->setMinimumSize(QSize(0, 160));
        widget_archiveFolderNavi->setMaximumSize(QSize(16777215, 280));
        stackedWidget_archiveFolderNaviPage1 = new QWidget();
        stackedWidget_archiveFolderNaviPage1->setObjectName(QStringLiteral("stackedWidget_archiveFolderNaviPage1"));
        verticalLayout_31 = new QVBoxLayout(stackedWidget_archiveFolderNaviPage1);
        verticalLayout_31->setSpacing(0);
        verticalLayout_31->setContentsMargins(11, 11, 11, 11);
        verticalLayout_31->setObjectName(QStringLiteral("verticalLayout_31"));
        verticalLayout_31->setSizeConstraint(QLayout::SetDefaultConstraint);
        verticalLayout_31->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_31 = new QHBoxLayout();
        horizontalLayout_31->setSpacing(0);
        horizontalLayout_31->setObjectName(QStringLiteral("horizontalLayout_31"));
        horizontalLayout_31->setContentsMargins(-1, 0, -1, -1);
        pushButton_fileListToRoot = new QPushButton(stackedWidget_archiveFolderNaviPage1);
        pushButton_fileListToRoot->setObjectName(QStringLiteral("pushButton_fileListToRoot"));
        pushButton_fileListToRoot->setMinimumSize(QSize(0, 0));
        pushButton_fileListToRoot->setMaximumSize(QSize(16777215, 16));
        pushButton_fileListToRoot->setFont(font2);
        pushButton_fileListToRoot->setStyleSheet(QStringLiteral("Text-align:left"));
        pushButton_fileListToRoot->setIcon(icon17);
        pushButton_fileListToRoot->setFlat(false);

        horizontalLayout_31->addWidget(pushButton_fileListToRoot);

        pushButton_imgOnly = new QPushButton(stackedWidget_archiveFolderNaviPage1);
        pushButton_imgOnly->setObjectName(QStringLiteral("pushButton_imgOnly"));
        pushButton_imgOnly->setMaximumSize(QSize(70, 16));
        pushButton_imgOnly->setCheckable(true);
        pushButton_imgOnly->setChecked(false);

        horizontalLayout_31->addWidget(pushButton_imgOnly);

        pushButton_archiveOpenFolder = new QPushButton(stackedWidget_archiveFolderNaviPage1);
        pushButton_archiveOpenFolder->setObjectName(QStringLiteral("pushButton_archiveOpenFolder"));
        pushButton_archiveOpenFolder->setMaximumSize(QSize(70, 16));

        horizontalLayout_31->addWidget(pushButton_archiveOpenFolder);

        pushButton_archiveOrganizeFolder = new QPushButton(stackedWidget_archiveFolderNaviPage1);
        pushButton_archiveOrganizeFolder->setObjectName(QStringLiteral("pushButton_archiveOrganizeFolder"));
        pushButton_archiveOrganizeFolder->setMaximumSize(QSize(70, 16));

        horizontalLayout_31->addWidget(pushButton_archiveOrganizeFolder);

        pushButton_archiveFlattenFolder = new QPushButton(stackedWidget_archiveFolderNaviPage1);
        pushButton_archiveFlattenFolder->setObjectName(QStringLiteral("pushButton_archiveFlattenFolder"));
        pushButton_archiveFlattenFolder->setMaximumSize(QSize(70, 16));

        horizontalLayout_31->addWidget(pushButton_archiveFlattenFolder);

        pushButton_archiveCompressor = new QPushButton(stackedWidget_archiveFolderNaviPage1);
        pushButton_archiveCompressor->setObjectName(QStringLiteral("pushButton_archiveCompressor"));
        pushButton_archiveCompressor->setMaximumSize(QSize(70, 16));

        horizontalLayout_31->addWidget(pushButton_archiveCompressor);

        pushButton_toggleFolderNavi2 = new QPushButton(stackedWidget_archiveFolderNaviPage1);
        pushButton_toggleFolderNavi2->setObjectName(QStringLiteral("pushButton_toggleFolderNavi2"));
        pushButton_toggleFolderNavi2->setMaximumSize(QSize(25, 16));
        QIcon icon24;
        icon24.addFile(QStringLiteral(":/icons/icons/stylesheet-branch-open.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_toggleFolderNavi2->setIcon(icon24);

        horizontalLayout_31->addWidget(pushButton_toggleFolderNavi2);


        verticalLayout_31->addLayout(horizontalLayout_31);

        horizontalLayout_32 = new QHBoxLayout();
        horizontalLayout_32->setSpacing(0);
        horizontalLayout_32->setObjectName(QStringLiteral("horizontalLayout_32"));
        horizontalLayout_32->setContentsMargins(-1, -1, 0, -1);
        treeView_archiveFiles = new QTreeView(stackedWidget_archiveFolderNaviPage1);
        treeView_archiveFiles->setObjectName(QStringLiteral("treeView_archiveFiles"));
        treeView_archiveFiles->setEnabled(true);
        treeView_archiveFiles->setMinimumSize(QSize(180, 0));
        treeView_archiveFiles->setMaximumSize(QSize(180, 16777215));
        treeView_archiveFiles->setEditTriggers(QAbstractItemView::NoEditTriggers);
        treeView_archiveFiles->setSelectionBehavior(QAbstractItemView::SelectRows);
        treeView_archiveFiles->setAnimated(true);
        treeView_archiveFiles->setHeaderHidden(true);
        treeView_archiveFiles->header()->setVisible(false);

        horizontalLayout_32->addWidget(treeView_archiveFiles);

        listView_archiveFiles = new VerticalScrollListView(stackedWidget_archiveFolderNaviPage1);
        listView_archiveFiles->setObjectName(QStringLiteral("listView_archiveFiles"));
        listView_archiveFiles->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);

        horizontalLayout_32->addWidget(listView_archiveFiles);

        horizontalLayout_32->setStretch(1, 2);

        verticalLayout_31->addLayout(horizontalLayout_32);

        widget_archiveFolderNavi->addWidget(stackedWidget_archiveFolderNaviPage1);
        page_dirWarning = new QWidget();
        page_dirWarning->setObjectName(QStringLiteral("page_dirWarning"));
        verticalLayout_17 = new QVBoxLayout(page_dirWarning);
        verticalLayout_17->setSpacing(6);
        verticalLayout_17->setContentsMargins(11, 11, 11, 11);
        verticalLayout_17->setObjectName(QStringLiteral("verticalLayout_17"));
        verticalSpacer_10 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_17->addItem(verticalSpacer_10);

        widget_dirWarning = new QWidget(page_dirWarning);
        widget_dirWarning->setObjectName(QStringLiteral("widget_dirWarning"));
        horizontalLayout_8 = new QHBoxLayout(widget_dirWarning);
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_2);

        label_2 = new QLabel(widget_dirWarning);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setMaximumSize(QSize(32, 32));
        label_2->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/warning_32.png")));

        horizontalLayout_8->addWidget(label_2);

        label_dirWarning = new QLabel(widget_dirWarning);
        label_dirWarning->setObjectName(QStringLiteral("label_dirWarning"));
        label_dirWarning->setFont(font1);

        horizontalLayout_8->addWidget(label_dirWarning);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_5);


        verticalLayout_17->addWidget(widget_dirWarning);

        verticalSpacer_11 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_17->addItem(verticalSpacer_11);

        widget_archiveFolderNavi->addWidget(page_dirWarning);
        splitter->addWidget(widget_archiveFolderNavi);

        verticalLayout_16->addWidget(splitter);


        verticalLayout_15->addWidget(widget_11);

        pushButton_toggleFolderNavi = new QPushButton(tab_archivePreview);
        pushButton_toggleFolderNavi->setObjectName(QStringLiteral("pushButton_toggleFolderNavi"));
        pushButton_toggleFolderNavi->setMaximumSize(QSize(16777215, 20));
        pushButton_toggleFolderNavi->setIcon(icon24);
        pushButton_toggleFolderNavi->setFlat(true);

        verticalLayout_15->addWidget(pushButton_toggleFolderNavi);

        tabWidget_archiveCenter->addTab(tab_archivePreview, QString());
        tab_archiveThumbnails = new QWidget();
        tab_archiveThumbnails->setObjectName(QStringLiteral("tab_archiveThumbnails"));
        verticalLayout_24 = new QVBoxLayout(tab_archiveThumbnails);
        verticalLayout_24->setSpacing(6);
        verticalLayout_24->setContentsMargins(11, 11, 11, 11);
        verticalLayout_24->setObjectName(QStringLiteral("verticalLayout_24"));
        listView_archiveBrowser = new SmoothScrollListView(tab_archiveThumbnails);
        listView_archiveBrowser->setObjectName(QStringLiteral("listView_archiveBrowser"));
        listView_archiveBrowser->setToolTipDuration(1);
        listView_archiveBrowser->setEditTriggers(QAbstractItemView::NoEditTriggers);
        listView_archiveBrowser->setDragDropMode(QAbstractItemView::DragDrop);
        listView_archiveBrowser->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
        listView_archiveBrowser->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
        listView_archiveBrowser->setViewMode(QListView::IconMode);

        verticalLayout_24->addWidget(listView_archiveBrowser);

        horizontalLayout_24 = new QHBoxLayout();
        horizontalLayout_24->setSpacing(6);
        horizontalLayout_24->setObjectName(QStringLiteral("horizontalLayout_24"));
        horizontalLayout_24->setContentsMargins(-1, -1, -1, 0);
        label_11 = new QLabel(tab_archiveThumbnails);
        label_11->setObjectName(QStringLiteral("label_11"));

        horizontalLayout_24->addWidget(label_11);

        checkBox_aF_notin = new QCheckBox(tab_archiveThumbnails);
        checkBox_aF_notin->setObjectName(QStringLiteral("checkBox_aF_notin"));

        horizontalLayout_24->addWidget(checkBox_aF_notin);

        widget_af_ni = new QWidget(tab_archiveThumbnails);
        widget_af_ni->setObjectName(QStringLiteral("widget_af_ni"));
        widget_af_ni->setMinimumSize(QSize(50, 0));
        horizontalLayout_25 = new QHBoxLayout(widget_af_ni);
        horizontalLayout_25->setSpacing(3);
        horizontalLayout_25->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_25->setObjectName(QStringLiteral("horizontalLayout_25"));
        horizontalLayout_25->setContentsMargins(0, 0, 0, 0);
        radioButton_FNUP = new QRadioButton(widget_af_ni);
        radioButton_FNUP->setObjectName(QStringLiteral("radioButton_FNUP"));
        radioButton_FNUP->setChecked(true);

        horizontalLayout_25->addWidget(radioButton_FNUP);

        radioButton_FUP = new QRadioButton(widget_af_ni);
        radioButton_FUP->setObjectName(QStringLiteral("radioButton_FUP"));

        horizontalLayout_25->addWidget(radioButton_FUP);

        label_aF_niDA = new clickable_QLabel(widget_af_ni);
        label_aF_niDA->setObjectName(QStringLiteral("label_aF_niDA"));
        label_aF_niDA->setMinimumSize(QSize(16, 16));
        label_aF_niDA->setMaximumSize(QSize(16, 16));
        label_aF_niDA->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/siteIconDeviantArt.png")));
        label_aF_niDA->setScaledContents(true);

        horizontalLayout_25->addWidget(label_aF_niDA);

        label_aF_niRD = new clickable_QLabel(widget_af_ni);
        label_aF_niRD->setObjectName(QStringLiteral("label_aF_niRD"));
        label_aF_niRD->setMinimumSize(QSize(16, 16));
        label_aF_niRD->setMaximumSize(QSize(16, 16));
        label_aF_niRD->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/siteIconRd_GREY.png")));
        label_aF_niRD->setScaledContents(true);

        horizontalLayout_25->addWidget(label_aF_niRD);

        label_aF_ni5P = new clickable_QLabel(widget_af_ni);
        label_aF_ni5P->setObjectName(QStringLiteral("label_aF_ni5P"));
        label_aF_ni5P->setMinimumSize(QSize(16, 16));
        label_aF_ni5P->setMaximumSize(QSize(16, 16));
        label_aF_ni5P->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/siteIcon500px_GREY.png")));
        label_aF_ni5P->setScaledContents(true);

        horizontalLayout_25->addWidget(label_aF_ni5P);

        label_aF_niGP = new clickable_QLabel(widget_af_ni);
        label_aF_niGP->setObjectName(QStringLiteral("label_aF_niGP"));
        label_aF_niGP->setMinimumSize(QSize(16, 16));
        label_aF_niGP->setMaximumSize(QSize(16, 16));
        label_aF_niGP->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/siteIconGoogle+_GREY.png")));
        label_aF_niGP->setScaledContents(true);

        horizontalLayout_25->addWidget(label_aF_niGP);

        label_aF_niFL = new clickable_QLabel(widget_af_ni);
        label_aF_niFL->setObjectName(QStringLiteral("label_aF_niFL"));
        label_aF_niFL->setMinimumSize(QSize(16, 16));
        label_aF_niFL->setMaximumSize(QSize(16, 16));
        label_aF_niFL->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/siteIconFlickr_GREY.png")));
        label_aF_niFL->setScaledContents(true);

        horizontalLayout_25->addWidget(label_aF_niFL);

        label_aF_niFB = new clickable_QLabel(widget_af_ni);
        label_aF_niFB->setObjectName(QStringLiteral("label_aF_niFB"));
        label_aF_niFB->setMinimumSize(QSize(16, 16));
        label_aF_niFB->setMaximumSize(QSize(16, 16));
        label_aF_niFB->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/siteIconFaceBook_GREY.png")));
        label_aF_niFB->setScaledContents(true);

        horizontalLayout_25->addWidget(label_aF_niFB);

        label_aF_niPX = new clickable_QLabel(widget_af_ni);
        label_aF_niPX->setObjectName(QStringLiteral("label_aF_niPX"));
        label_aF_niPX->setMinimumSize(QSize(16, 16));
        label_aF_niPX->setMaximumSize(QSize(16, 16));
        label_aF_niPX->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/siteIconPixoto1_GREY.png")));
        label_aF_niPX->setScaledContents(true);

        horizontalLayout_25->addWidget(label_aF_niPX);

        label_aF_niYP = new clickable_QLabel(widget_af_ni);
        label_aF_niYP->setObjectName(QStringLiteral("label_aF_niYP"));
        label_aF_niYP->setMinimumSize(QSize(16, 16));
        label_aF_niYP->setMaximumSize(QSize(16, 16));
        label_aF_niYP->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/siteIconYoupic_GREY.png")));
        label_aF_niYP->setScaledContents(true);

        horizontalLayout_25->addWidget(label_aF_niYP);

        label_aF_niVB = new clickable_QLabel(widget_af_ni);
        label_aF_niVB->setObjectName(QStringLiteral("label_aF_niVB"));
        label_aF_niVB->setMinimumSize(QSize(16, 16));
        label_aF_niVB->setMaximumSize(QSize(16, 16));
        label_aF_niVB->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/siteIconViewbug_GREY.png")));
        label_aF_niVB->setScaledContents(true);

        horizontalLayout_25->addWidget(label_aF_niVB);

        label_aF_niIG = new clickable_QLabel(widget_af_ni);
        label_aF_niIG->setObjectName(QStringLiteral("label_aF_niIG"));
        label_aF_niIG->setMinimumSize(QSize(16, 16));
        label_aF_niIG->setMaximumSize(QSize(16, 16));
        label_aF_niIG->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/siteIconInstagram_GREY.png")));
        label_aF_niIG->setScaledContents(true);

        horizontalLayout_25->addWidget(label_aF_niIG);


        horizontalLayout_24->addWidget(widget_af_ni);

        horizontalSpacer_29 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_24->addItem(horizontalSpacer_29);

        label_archiveThumbSize = new QLabel(tab_archiveThumbnails);
        label_archiveThumbSize->setObjectName(QStringLiteral("label_archiveThumbSize"));

        horizontalLayout_24->addWidget(label_archiveThumbSize);

        horizontalSlider_archiveThumbSize = new QSlider(tab_archiveThumbnails);
        horizontalSlider_archiveThumbSize->setObjectName(QStringLiteral("horizontalSlider_archiveThumbSize"));
        horizontalSlider_archiveThumbSize->setMinimumSize(QSize(100, 0));
        horizontalSlider_archiveThumbSize->setMaximumSize(QSize(150, 16777215));
        horizontalSlider_archiveThumbSize->setMinimum(0);
        horizontalSlider_archiveThumbSize->setMaximum(4);
        horizontalSlider_archiveThumbSize->setSingleStep(4);
        horizontalSlider_archiveThumbSize->setPageStep(4);
        horizontalSlider_archiveThumbSize->setValue(0);
        horizontalSlider_archiveThumbSize->setSliderPosition(0);
        horizontalSlider_archiveThumbSize->setTracking(true);
        horizontalSlider_archiveThumbSize->setOrientation(Qt::Horizontal);
        horizontalSlider_archiveThumbSize->setTickPosition(QSlider::NoTicks);
        horizontalSlider_archiveThumbSize->setTickInterval(1);

        horizontalLayout_24->addWidget(horizontalSlider_archiveThumbSize);


        verticalLayout_24->addLayout(horizontalLayout_24);

        tabWidget_archiveCenter->addTab(tab_archiveThumbnails, QString());
        tab_archiveWeb = new QWidget();
        tab_archiveWeb->setObjectName(QStringLiteral("tab_archiveWeb"));
        verticalLayout_25 = new QVBoxLayout(tab_archiveWeb);
        verticalLayout_25->setSpacing(6);
        verticalLayout_25->setContentsMargins(11, 11, 11, 11);
        verticalLayout_25->setObjectName(QStringLiteral("verticalLayout_25"));
        horizontalLayout_21 = new QHBoxLayout();
        horizontalLayout_21->setSpacing(0);
        horizontalLayout_21->setObjectName(QStringLiteral("horizontalLayout_21"));
        verticalSpacer_5 = new QSpacerItem(2, 20, QSizePolicy::Minimum, QSizePolicy::Fixed);

        horizontalLayout_21->addItem(verticalSpacer_5);

        pushButton_naviPrev_2 = new QPushButton(tab_archiveWeb);
        pushButton_naviPrev_2->setObjectName(QStringLiteral("pushButton_naviPrev_2"));
        pushButton_naviPrev_2->setCursor(QCursor(Qt::ArrowCursor));

        horizontalLayout_21->addWidget(pushButton_naviPrev_2);

        pushButton_naviNext_2 = new QPushButton(tab_archiveWeb);
        pushButton_naviNext_2->setObjectName(QStringLiteral("pushButton_naviNext_2"));
        pushButton_naviNext_2->setCursor(QCursor(Qt::ArrowCursor));

        horizontalLayout_21->addWidget(pushButton_naviNext_2);

        verticalLayout_26 = new QVBoxLayout();
        verticalLayout_26->setSpacing(0);
        verticalLayout_26->setObjectName(QStringLiteral("verticalLayout_26"));
        verticalLayout_26->setContentsMargins(-1, -1, 0, -1);
        lineEdit_archiveUrl = new QLineEdit(tab_archiveWeb);
        lineEdit_archiveUrl->setObjectName(QStringLiteral("lineEdit_archiveUrl"));
        lineEdit_archiveUrl->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        verticalLayout_26->addWidget(lineEdit_archiveUrl);

        progressBar_web = new QProgressBar(tab_archiveWeb);
        progressBar_web->setObjectName(QStringLiteral("progressBar_web"));
        progressBar_web->setMinimumSize(QSize(0, 0));
        progressBar_web->setMaximumSize(QSize(16565, 2));
        progressBar_web->setFont(font1);
        progressBar_web->setValue(0);
        progressBar_web->setAlignment(Qt::AlignCenter);
        progressBar_web->setTextVisible(false);

        verticalLayout_26->addWidget(progressBar_web);


        horizontalLayout_21->addLayout(verticalLayout_26);


        verticalLayout_25->addLayout(horizontalLayout_21);

        webView = new QWebView(tab_archiveWeb);
        webView->setObjectName(QStringLiteral("webView"));
        webView->setUrl(QUrl(QStringLiteral("about:blank")));

        verticalLayout_25->addWidget(webView);

        tabWidget_archiveCenter->addTab(tab_archiveWeb, QString());
        tab_14 = new QWidget();
        tab_14->setObjectName(QStringLiteral("tab_14"));
        verticalLayout_28 = new QVBoxLayout(tab_14);
        verticalLayout_28->setSpacing(0);
        verticalLayout_28->setContentsMargins(11, 11, 11, 11);
        verticalLayout_28->setObjectName(QStringLiteral("verticalLayout_28"));
        horizontalLayout_27 = new QHBoxLayout();
        horizontalLayout_27->setSpacing(0);
        horizontalLayout_27->setObjectName(QStringLiteral("horizontalLayout_27"));
        horizontalSpacer_30 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_27->addItem(horizontalSpacer_30);

        pushButton_mapRefresh = new QPushButton(tab_14);
        pushButton_mapRefresh->setObjectName(QStringLiteral("pushButton_mapRefresh"));

        horizontalLayout_27->addWidget(pushButton_mapRefresh);


        verticalLayout_28->addLayout(horizontalLayout_27);

        webView_fullMap = new QWebView(tab_14);
        webView_fullMap->setObjectName(QStringLiteral("webView_fullMap"));
        webView_fullMap->setUrl(QUrl(QStringLiteral("about:blank")));

        verticalLayout_28->addWidget(webView_fullMap);

        tabWidget_archiveCenter->addTab(tab_14, QString());

        horizontalLayout_4->addWidget(tabWidget_archiveCenter);

        vlayout_archive_right = new QVBoxLayout();
        vlayout_archive_right->setSpacing(6);
        vlayout_archive_right->setObjectName(QStringLiteral("vlayout_archive_right"));
        vlayout_archive_right->setContentsMargins(-1, 0, 0, -1);
        scrollArea = new QScrollArea(tab_archives);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setMinimumSize(QSize(261, 0));
        scrollArea->setMaximumSize(QSize(251, 16777215));
        scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents_5 = new QWidget();
        scrollAreaWidgetContents_5->setObjectName(QStringLiteral("scrollAreaWidgetContents_5"));
        scrollAreaWidgetContents_5->setGeometry(QRect(0, -1040, 278, 1983));
        verticalLayout_23 = new QVBoxLayout(scrollAreaWidgetContents_5);
        verticalLayout_23->setSpacing(0);
        verticalLayout_23->setContentsMargins(11, 11, 11, 11);
        verticalLayout_23->setObjectName(QStringLiteral("verticalLayout_23"));
        verticalLayout_23->setContentsMargins(0, 0, 0, 0);
        widget_archive_sidebar_preview_header = new clickable_QWidget(scrollAreaWidgetContents_5);
        widget_archive_sidebar_preview_header->setObjectName(QStringLiteral("widget_archive_sidebar_preview_header"));
        widget_archive_sidebar_preview_header->setMinimumSize(QSize(0, 0));
        widget_archive_sidebar_preview_header->setCursor(QCursor(Qt::PointingHandCursor));
        horizontalLayout_30 = new QHBoxLayout(widget_archive_sidebar_preview_header);
        horizontalLayout_30->setSpacing(4);
        horizontalLayout_30->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_30->setObjectName(QStringLiteral("horizontalLayout_30"));
        horizontalLayout_30->setContentsMargins(4, 2, 4, 2);
        label_28 = new QLabel(widget_archive_sidebar_preview_header);
        label_28->setObjectName(QStringLiteral("label_28"));
        label_28->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/handle1.png")));

        horizontalLayout_30->addWidget(label_28);

        label_23 = new QLabel(widget_archive_sidebar_preview_header);
        label_23->setObjectName(QStringLiteral("label_23"));
        label_23->setMaximumSize(QSize(12, 12));
        label_23->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/appIcon-Photomatix.png")));
        label_23->setScaledContents(true);

        horizontalLayout_30->addWidget(label_23);

        label_archive_sidebar_preview = new QLabel(widget_archive_sidebar_preview_header);
        label_archive_sidebar_preview->setObjectName(QStringLiteral("label_archive_sidebar_preview"));
        label_archive_sidebar_preview->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_archive_sidebar_preview->setMargin(0);
        label_archive_sidebar_preview->setIndent(-1);

        horizontalLayout_30->addWidget(label_archive_sidebar_preview);

        horizontalSpacer_6 = new QSpacerItem(40, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_30->addItem(horizontalSpacer_6);

        label_archive_sidebar_preview_toggle = new QLabel(widget_archive_sidebar_preview_header);
        label_archive_sidebar_preview_toggle->setObjectName(QStringLiteral("label_archive_sidebar_preview_toggle"));
        label_archive_sidebar_preview_toggle->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/stylesheet-branch-open.png")));

        horizontalLayout_30->addWidget(label_archive_sidebar_preview_toggle);


        verticalLayout_23->addWidget(widget_archive_sidebar_preview_header);

        widget_archive_sidebar_preview_shadow = new QWidget(scrollAreaWidgetContents_5);
        widget_archive_sidebar_preview_shadow->setObjectName(QStringLiteral("widget_archive_sidebar_preview_shadow"));
        widget_archive_sidebar_preview_shadow->setMinimumSize(QSize(0, 9));
        widget_archive_sidebar_preview_shadow->setMaximumSize(QSize(16777215, 9));

        verticalLayout_23->addWidget(widget_archive_sidebar_preview_shadow);

        widget_archive_sidebar_preview_content = new QWidget(scrollAreaWidgetContents_5);
        widget_archive_sidebar_preview_content->setObjectName(QStringLiteral("widget_archive_sidebar_preview_content"));
        verticalLayout_7 = new QVBoxLayout(widget_archive_sidebar_preview_content);
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setContentsMargins(11, 11, 11, 11);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        verticalLayout_7->setContentsMargins(-1, 0, -1, -1);
        label_sideBarPreview = new clickable_QLabel(widget_archive_sidebar_preview_content);
        label_sideBarPreview->setObjectName(QStringLiteral("label_sideBarPreview"));
        sizePolicy.setHeightForWidth(label_sideBarPreview->sizePolicy().hasHeightForWidth());
        label_sideBarPreview->setSizePolicy(sizePolicy);
        label_sideBarPreview->setMinimumSize(QSize(0, 192));
        label_sideBarPreview->setMaximumSize(QSize(16655, 192));
        label_sideBarPreview->setFrameShape(QFrame::NoFrame);
        label_sideBarPreview->setFrameShadow(QFrame::Plain);
        label_sideBarPreview->setAlignment(Qt::AlignCenter);

        verticalLayout_7->addWidget(label_sideBarPreview);


        verticalLayout_23->addWidget(widget_archive_sidebar_preview_content);

        widget_archive_sidebar_info_header = new clickable_QWidget(scrollAreaWidgetContents_5);
        widget_archive_sidebar_info_header->setObjectName(QStringLiteral("widget_archive_sidebar_info_header"));
        widget_archive_sidebar_info_header->setMinimumSize(QSize(0, 0));
        widget_archive_sidebar_info_header->setCursor(QCursor(Qt::PointingHandCursor));
        horizontalLayout_33 = new QHBoxLayout(widget_archive_sidebar_info_header);
        horizontalLayout_33->setSpacing(4);
        horizontalLayout_33->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_33->setObjectName(QStringLiteral("horizontalLayout_33"));
        horizontalLayout_33->setContentsMargins(4, 2, 4, 2);
        label_29 = new QLabel(widget_archive_sidebar_info_header);
        label_29->setObjectName(QStringLiteral("label_29"));
        label_29->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/handle1.png")));

        horizontalLayout_33->addWidget(label_29);

        label_22 = new QLabel(widget_archive_sidebar_info_header);
        label_22->setObjectName(QStringLiteral("label_22"));
        label_22->setMaximumSize(QSize(12, 12));
        label_22->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/Info.png")));
        label_22->setScaledContents(true);
        label_22->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_33->addWidget(label_22);

        label_archive_sidebar_info = new QLabel(widget_archive_sidebar_info_header);
        label_archive_sidebar_info->setObjectName(QStringLiteral("label_archive_sidebar_info"));

        horizontalLayout_33->addWidget(label_archive_sidebar_info);

        horizontalSpacer_12 = new QSpacerItem(40, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_33->addItem(horizontalSpacer_12);

        label_archive_sidebar_info_toggle = new QLabel(widget_archive_sidebar_info_header);
        label_archive_sidebar_info_toggle->setObjectName(QStringLiteral("label_archive_sidebar_info_toggle"));
        label_archive_sidebar_info_toggle->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/stylesheet-branch-open.png")));

        horizontalLayout_33->addWidget(label_archive_sidebar_info_toggle);


        verticalLayout_23->addWidget(widget_archive_sidebar_info_header);

        widget_archive_sidebar_info_shadow = new QWidget(scrollAreaWidgetContents_5);
        widget_archive_sidebar_info_shadow->setObjectName(QStringLiteral("widget_archive_sidebar_info_shadow"));
        widget_archive_sidebar_info_shadow->setMinimumSize(QSize(0, 9));
        widget_archive_sidebar_info_shadow->setMaximumSize(QSize(16777215, 9));

        verticalLayout_23->addWidget(widget_archive_sidebar_info_shadow);

        widget_archive_sidebar_info_content = new QWidget(scrollAreaWidgetContents_5);
        widget_archive_sidebar_info_content->setObjectName(QStringLiteral("widget_archive_sidebar_info_content"));
        widget_archive_sidebar_info_content->setMinimumSize(QSize(0, 0));
        verticalLayout_18 = new QVBoxLayout(widget_archive_sidebar_info_content);
        verticalLayout_18->setSpacing(0);
        verticalLayout_18->setContentsMargins(11, 11, 11, 11);
        verticalLayout_18->setObjectName(QStringLiteral("verticalLayout_18"));
        verticalLayout_18->setContentsMargins(-1, 0, -1, -1);
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(4);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label_archiveInfoTitle = new QLabel(widget_archive_sidebar_info_content);
        label_archiveInfoTitle->setObjectName(QStringLiteral("label_archiveInfoTitle"));
        label_archiveInfoTitle->setFont(font1);

        gridLayout->addWidget(label_archiveInfoTitle, 0, 1, 1, 1);

        label_archiveInfoID = new QLabel(widget_archive_sidebar_info_content);
        label_archiveInfoID->setObjectName(QStringLiteral("label_archiveInfoID"));
        label_archiveInfoID->setFont(font1);

        gridLayout->addWidget(label_archiveInfoID, 3, 1, 1, 1);

        label_5 = new QLabel(widget_archive_sidebar_info_content);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout->addWidget(label_5, 3, 0, 1, 1);

        label_3 = new QLabel(widget_archive_sidebar_info_content);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout->addWidget(label_3, 0, 0, 1, 1);

        label_7 = new QLabel(widget_archive_sidebar_info_content);
        label_7->setObjectName(QStringLiteral("label_7"));

        gridLayout->addWidget(label_7, 2, 0, 1, 1);

        label_archiveInfoDate = new QLabel(widget_archive_sidebar_info_content);
        label_archiveInfoDate->setObjectName(QStringLiteral("label_archiveInfoDate"));
        label_archiveInfoDate->setFont(font1);

        gridLayout->addWidget(label_archiveInfoDate, 2, 1, 1, 1);

        label_6 = new QLabel(widget_archive_sidebar_info_content);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        gridLayout->addWidget(label_6, 4, 0, 1, 1);

        label_archiveInfoLocation = new QLabel(widget_archive_sidebar_info_content);
        label_archiveInfoLocation->setObjectName(QStringLiteral("label_archiveInfoLocation"));
        label_archiveInfoLocation->setFont(font1);
        label_archiveInfoLocation->setWordWrap(true);

        gridLayout->addWidget(label_archiveInfoLocation, 4, 1, 1, 1);

        label_fileSize = new QLabel(widget_archive_sidebar_info_content);
        label_fileSize->setObjectName(QStringLiteral("label_fileSize"));

        gridLayout->addWidget(label_fileSize, 5, 1, 1, 1);

        label_36 = new QLabel(widget_archive_sidebar_info_content);
        label_36->setObjectName(QStringLiteral("label_36"));

        gridLayout->addWidget(label_36, 1, 0, 1, 1);

        label_archiveInfoTitle_2 = new QLabel(widget_archive_sidebar_info_content);
        label_archiveInfoTitle_2->setObjectName(QStringLiteral("label_archiveInfoTitle_2"));
        label_archiveInfoTitle_2->setFont(font1);

        gridLayout->addWidget(label_archiveInfoTitle_2, 1, 1, 1, 1);

        gridLayout->setColumnStretch(0, 1);
        gridLayout->setColumnStretch(1, 8);

        verticalLayout_18->addLayout(gridLayout);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(0);
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        horizontalSpacer_7 = new QSpacerItem(40, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_7);

        pushButton_archiveEditData = new QPushButton(widget_archive_sidebar_info_content);
        pushButton_archiveEditData->setObjectName(QStringLiteral("pushButton_archiveEditData"));
        QFont font3;
        font3.setStyleStrategy(QFont::PreferAntialias);
        pushButton_archiveEditData->setFont(font3);

        horizontalLayout_10->addWidget(pushButton_archiveEditData);


        verticalLayout_18->addLayout(horizontalLayout_10);

        verticalLayout_12 = new QVBoxLayout();
        verticalLayout_12->setSpacing(6);
        verticalLayout_12->setObjectName(QStringLiteral("verticalLayout_12"));
        verticalLayout_12->setContentsMargins(-1, 10, -1, 10);
        tableView_sales = new QTableView(widget_archive_sidebar_info_content);
        tableView_sales->setObjectName(QStringLiteral("tableView_sales"));
        QFont font4;
        font4.setPointSize(7);
        tableView_sales->setFont(font4);
        tableView_sales->verticalHeader()->setVisible(false);
        tableView_sales->verticalHeader()->setDefaultSectionSize(16);

        verticalLayout_12->addWidget(tableView_sales);

        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setSpacing(6);
        horizontalLayout_16->setObjectName(QStringLiteral("horizontalLayout_16"));
        horizontalLayout_16->setContentsMargins(-1, 10, -1, 10);
        label_salesCount = new QLabel(widget_archive_sidebar_info_content);
        label_salesCount->setObjectName(QStringLiteral("label_salesCount"));
        QFont font5;
        font5.setPointSize(12);
        font5.setBold(true);
        font5.setWeight(75);
        label_salesCount->setFont(font5);
        label_salesCount->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_16->addWidget(label_salesCount);

        label_salesTotal = new QLabel(widget_archive_sidebar_info_content);
        label_salesTotal->setObjectName(QStringLiteral("label_salesTotal"));
        label_salesTotal->setFont(font5);
        label_salesTotal->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_16->addWidget(label_salesTotal);


        verticalLayout_12->addLayout(horizontalLayout_16);

        pushButton_addSale = new QPushButton(widget_archive_sidebar_info_content);
        pushButton_addSale->setObjectName(QStringLiteral("pushButton_addSale"));
        pushButton_addSale->setMinimumSize(QSize(0, 25));
        pushButton_addSale->setMaximumSize(QSize(1616, 16777215));

        verticalLayout_12->addWidget(pushButton_addSale);


        verticalLayout_18->addLayout(verticalLayout_12);


        verticalLayout_23->addWidget(widget_archive_sidebar_info_content);

        widget_archive_sidebar_tools_header = new clickable_QWidget(scrollAreaWidgetContents_5);
        widget_archive_sidebar_tools_header->setObjectName(QStringLiteral("widget_archive_sidebar_tools_header"));
        widget_archive_sidebar_tools_header->setMinimumSize(QSize(0, 0));
        widget_archive_sidebar_tools_header->setCursor(QCursor(Qt::PointingHandCursor));
        horizontalLayout_43 = new QHBoxLayout(widget_archive_sidebar_tools_header);
        horizontalLayout_43->setSpacing(4);
        horizontalLayout_43->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_43->setObjectName(QStringLiteral("horizontalLayout_43"));
        horizontalLayout_43->setContentsMargins(4, 2, 4, 2);
        label_37 = new QLabel(widget_archive_sidebar_tools_header);
        label_37->setObjectName(QStringLiteral("label_37"));
        label_37->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/handle1.png")));

        horizontalLayout_43->addWidget(label_37);

        label_38 = new QLabel(widget_archive_sidebar_tools_header);
        label_38->setObjectName(QStringLiteral("label_38"));
        label_38->setMaximumSize(QSize(12, 12));
        label_38->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/rotator_1.png")));
        label_38->setScaledContents(true);

        horizontalLayout_43->addWidget(label_38);

        label_archive_sidebar_tools = new QLabel(widget_archive_sidebar_tools_header);
        label_archive_sidebar_tools->setObjectName(QStringLiteral("label_archive_sidebar_tools"));

        horizontalLayout_43->addWidget(label_archive_sidebar_tools);

        horizontalSpacer_33 = new QSpacerItem(40, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_43->addItem(horizontalSpacer_33);

        label_archive_sidebar_tools_toggle = new QLabel(widget_archive_sidebar_tools_header);
        label_archive_sidebar_tools_toggle->setObjectName(QStringLiteral("label_archive_sidebar_tools_toggle"));
        label_archive_sidebar_tools_toggle->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/stylesheet-branch-open.png")));

        horizontalLayout_43->addWidget(label_archive_sidebar_tools_toggle);


        verticalLayout_23->addWidget(widget_archive_sidebar_tools_header);

        widget_archive_sidebar_tools_shadow = new QWidget(scrollAreaWidgetContents_5);
        widget_archive_sidebar_tools_shadow->setObjectName(QStringLiteral("widget_archive_sidebar_tools_shadow"));
        widget_archive_sidebar_tools_shadow->setMinimumSize(QSize(0, 9));
        widget_archive_sidebar_tools_shadow->setMaximumSize(QSize(16777215, 9));

        verticalLayout_23->addWidget(widget_archive_sidebar_tools_shadow);

        widget_archive_sidebar_tools_content = new QWidget(scrollAreaWidgetContents_5);
        widget_archive_sidebar_tools_content->setObjectName(QStringLiteral("widget_archive_sidebar_tools_content"));
        verticalLayout_5 = new QVBoxLayout(widget_archive_sidebar_tools_content);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        verticalLayout_5->setContentsMargins(-1, 0, -1, -1);
        gridLayout_10 = new QGridLayout();
        gridLayout_10->setSpacing(6);
        gridLayout_10->setObjectName(QStringLiteral("gridLayout_10"));
        gridLayout_10->setContentsMargins(-1, 0, -1, -1);
        pushButton_archiveCompressor_1 = new QPushButton(widget_archive_sidebar_tools_content);
        pushButton_archiveCompressor_1->setObjectName(QStringLiteral("pushButton_archiveCompressor_1"));
        pushButton_archiveCompressor_1->setMinimumSize(QSize(0, 25));
        pushButton_archiveCompressor_1->setMaximumSize(QSize(1616, 1616));

        gridLayout_10->addWidget(pushButton_archiveCompressor_1, 1, 1, 1, 1);

        pushButton_archiveCopyKeyComma_1 = new QPushButton(widget_archive_sidebar_tools_content);
        pushButton_archiveCopyKeyComma_1->setObjectName(QStringLiteral("pushButton_archiveCopyKeyComma_1"));
        pushButton_archiveCopyKeyComma_1->setMinimumSize(QSize(0, 25));
        pushButton_archiveCopyKeyComma_1->setMaximumSize(QSize(1616, 16777215));

        gridLayout_10->addWidget(pushButton_archiveCopyKeyComma_1, 3, 0, 1, 1);

        pushButton_archiveEditData_1 = new QPushButton(widget_archive_sidebar_tools_content);
        pushButton_archiveEditData_1->setObjectName(QStringLiteral("pushButton_archiveEditData_1"));
        pushButton_archiveEditData_1->setMinimumSize(QSize(0, 25));
        pushButton_archiveEditData_1->setFont(font3);

        gridLayout_10->addWidget(pushButton_archiveEditData_1, 0, 0, 1, 1);

        pushButton_sitesManage_1 = new QPushButton(widget_archive_sidebar_tools_content);
        pushButton_sitesManage_1->setObjectName(QStringLiteral("pushButton_sitesManage_1"));
        pushButton_sitesManage_1->setMinimumSize(QSize(0, 25));

        gridLayout_10->addWidget(pushButton_sitesManage_1, 2, 1, 1, 1);

        pushButton_archiveCopyKeySpace_1 = new QPushButton(widget_archive_sidebar_tools_content);
        pushButton_archiveCopyKeySpace_1->setObjectName(QStringLiteral("pushButton_archiveCopyKeySpace_1"));
        pushButton_archiveCopyKeySpace_1->setMinimumSize(QSize(0, 25));
        pushButton_archiveCopyKeySpace_1->setMaximumSize(QSize(1616, 16777215));

        gridLayout_10->addWidget(pushButton_archiveCopyKeySpace_1, 3, 1, 1, 1);

        pushButton_addNewSite_1 = new QPushButton(widget_archive_sidebar_tools_content);
        pushButton_addNewSite_1->setObjectName(QStringLiteral("pushButton_addNewSite_1"));
        pushButton_addNewSite_1->setMinimumSize(QSize(0, 25));

        gridLayout_10->addWidget(pushButton_addNewSite_1, 2, 0, 1, 1);

        pushButton_archiveOrganizeFolder_1 = new QPushButton(widget_archive_sidebar_tools_content);
        pushButton_archiveOrganizeFolder_1->setObjectName(QStringLiteral("pushButton_archiveOrganizeFolder_1"));
        pushButton_archiveOrganizeFolder_1->setMinimumSize(QSize(0, 25));
        pushButton_archiveOrganizeFolder_1->setMaximumSize(QSize(1616, 1616));

        gridLayout_10->addWidget(pushButton_archiveOrganizeFolder_1, 1, 0, 1, 1);

        pushButton_archiveOpenFolder_1 = new QPushButton(widget_archive_sidebar_tools_content);
        pushButton_archiveOpenFolder_1->setObjectName(QStringLiteral("pushButton_archiveOpenFolder_1"));
        pushButton_archiveOpenFolder_1->setMinimumSize(QSize(0, 25));
        pushButton_archiveOpenFolder_1->setMaximumSize(QSize(1656, 1656));

        gridLayout_10->addWidget(pushButton_archiveOpenFolder_1, 0, 1, 1, 1);

        pushButton_archiveCopyTitle = new QPushButton(widget_archive_sidebar_tools_content);
        pushButton_archiveCopyTitle->setObjectName(QStringLiteral("pushButton_archiveCopyTitle"));
        pushButton_archiveCopyTitle->setMinimumSize(QSize(0, 25));
        pushButton_archiveCopyTitle->setMaximumSize(QSize(1616, 16777215));

        gridLayout_10->addWidget(pushButton_archiveCopyTitle, 4, 0, 1, 1);

        pushButton_archiveCopyTitle_2 = new QPushButton(widget_archive_sidebar_tools_content);
        pushButton_archiveCopyTitle_2->setObjectName(QStringLiteral("pushButton_archiveCopyTitle_2"));
        pushButton_archiveCopyTitle_2->setMinimumSize(QSize(0, 25));
        pushButton_archiveCopyTitle_2->setMaximumSize(QSize(1616, 16777215));

        gridLayout_10->addWidget(pushButton_archiveCopyTitle_2, 4, 1, 1, 1);

        pushButton_upload = new QPushButton(widget_archive_sidebar_tools_content);
        pushButton_upload->setObjectName(QStringLiteral("pushButton_upload"));
        pushButton_upload->setMinimumSize(QSize(0, 25));
        pushButton_upload->setMaximumSize(QSize(1616, 16777215));

        gridLayout_10->addWidget(pushButton_upload, 5, 0, 1, 1);


        verticalLayout_5->addLayout(gridLayout_10);


        verticalLayout_23->addWidget(widget_archive_sidebar_tools_content);

        widget_archive_sidebar_map_header = new clickable_QWidget(scrollAreaWidgetContents_5);
        widget_archive_sidebar_map_header->setObjectName(QStringLiteral("widget_archive_sidebar_map_header"));
        widget_archive_sidebar_map_header->setMinimumSize(QSize(0, 0));
        widget_archive_sidebar_map_header->setCursor(QCursor(Qt::PointingHandCursor));
        horizontalLayout_34 = new QHBoxLayout(widget_archive_sidebar_map_header);
        horizontalLayout_34->setSpacing(4);
        horizontalLayout_34->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_34->setObjectName(QStringLiteral("horizontalLayout_34"));
        horizontalLayout_34->setContentsMargins(4, 2, 4, 2);
        label_30 = new QLabel(widget_archive_sidebar_map_header);
        label_30->setObjectName(QStringLiteral("label_30"));
        label_30->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/handle1.png")));

        horizontalLayout_34->addWidget(label_30);

        label = new QLabel(widget_archive_sidebar_map_header);
        label->setObjectName(QStringLiteral("label"));
        label->setMaximumSize(QSize(12, 12));
        label->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/map-pin-1.png")));
        label->setScaledContents(true);

        horizontalLayout_34->addWidget(label);

        label_archive_sidebar_map = new QLabel(widget_archive_sidebar_map_header);
        label_archive_sidebar_map->setObjectName(QStringLiteral("label_archive_sidebar_map"));

        horizontalLayout_34->addWidget(label_archive_sidebar_map);

        horizontalSpacer_13 = new QSpacerItem(40, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_34->addItem(horizontalSpacer_13);

        label_archive_sidebar_map_toggle = new QLabel(widget_archive_sidebar_map_header);
        label_archive_sidebar_map_toggle->setObjectName(QStringLiteral("label_archive_sidebar_map_toggle"));
        label_archive_sidebar_map_toggle->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/stylesheet-branch-open.png")));

        horizontalLayout_34->addWidget(label_archive_sidebar_map_toggle);


        verticalLayout_23->addWidget(widget_archive_sidebar_map_header);

        widget_archive_sidebar_map_shadow = new QWidget(scrollAreaWidgetContents_5);
        widget_archive_sidebar_map_shadow->setObjectName(QStringLiteral("widget_archive_sidebar_map_shadow"));
        widget_archive_sidebar_map_shadow->setMinimumSize(QSize(0, 9));
        widget_archive_sidebar_map_shadow->setMaximumSize(QSize(16777215, 9));

        verticalLayout_23->addWidget(widget_archive_sidebar_map_shadow);

        widget_archive_sidebar_map_content = new QWidget(scrollAreaWidgetContents_5);
        widget_archive_sidebar_map_content->setObjectName(QStringLiteral("widget_archive_sidebar_map_content"));
        verticalLayout_22 = new QVBoxLayout(widget_archive_sidebar_map_content);
        verticalLayout_22->setSpacing(4);
        verticalLayout_22->setContentsMargins(11, 11, 11, 11);
        verticalLayout_22->setObjectName(QStringLiteral("verticalLayout_22"));
        verticalLayout_22->setContentsMargins(-1, 0, -1, -1);
        webView_Map = new QWebView(widget_archive_sidebar_map_content);
        webView_Map->setObjectName(QStringLiteral("webView_Map"));
        webView_Map->setMinimumSize(QSize(0, 200));
        webView_Map->setMaximumSize(QSize(16777215, 200));
        webView_Map->setUrl(QUrl(QStringLiteral("qrc:/html/google_maps.html")));

        verticalLayout_22->addWidget(webView_Map);

        plainTextEdit_loc = new QPlainTextEdit(widget_archive_sidebar_map_content);
        plainTextEdit_loc->setObjectName(QStringLiteral("plainTextEdit_loc"));
        plainTextEdit_loc->setMinimumSize(QSize(0, 36));
        plainTextEdit_loc->setMaximumSize(QSize(16777215, 36));
        plainTextEdit_loc->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        plainTextEdit_loc->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        verticalLayout_22->addWidget(plainTextEdit_loc);

        horizontalLayout_26 = new QHBoxLayout();
        horizontalLayout_26->setSpacing(6);
        horizontalLayout_26->setObjectName(QStringLiteral("horizontalLayout_26"));
        horizontalLayout_26->setContentsMargins(-1, 0, -1, -1);
        gridLayout_5 = new QGridLayout();
        gridLayout_5->setSpacing(6);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        gridLayout_5->setContentsMargins(-1, 0, -1, -1);
        lineEdit_locLat = new QLineEdit(widget_archive_sidebar_map_content);
        lineEdit_locLat->setObjectName(QStringLiteral("lineEdit_locLat"));

        gridLayout_5->addWidget(lineEdit_locLat, 1, 1, 1, 1);

        label_13 = new QLabel(widget_archive_sidebar_map_content);
        label_13->setObjectName(QStringLiteral("label_13"));

        gridLayout_5->addWidget(label_13, 1, 0, 1, 1);

        label_12 = new QLabel(widget_archive_sidebar_map_content);
        label_12->setObjectName(QStringLiteral("label_12"));

        gridLayout_5->addWidget(label_12, 2, 0, 1, 1);

        lineEdit_locLng = new QLineEdit(widget_archive_sidebar_map_content);
        lineEdit_locLng->setObjectName(QStringLiteral("lineEdit_locLng"));

        gridLayout_5->addWidget(lineEdit_locLng, 2, 1, 1, 1);


        horizontalLayout_26->addLayout(gridLayout_5);

        pushButton_locSave = new QPushButton(widget_archive_sidebar_map_content);
        pushButton_locSave->setObjectName(QStringLiteral("pushButton_locSave"));
        pushButton_locSave->setMinimumSize(QSize(44, 44));
        pushButton_locSave->setMaximumSize(QSize(44, 44));

        horizontalLayout_26->addWidget(pushButton_locSave);


        verticalLayout_22->addLayout(horizontalLayout_26);


        verticalLayout_23->addWidget(widget_archive_sidebar_map_content);

        widget_archive_sidebar_score_header = new clickable_QWidget(scrollAreaWidgetContents_5);
        widget_archive_sidebar_score_header->setObjectName(QStringLiteral("widget_archive_sidebar_score_header"));
        widget_archive_sidebar_score_header->setMinimumSize(QSize(0, 0));
        widget_archive_sidebar_score_header->setCursor(QCursor(Qt::PointingHandCursor));
        horizontalLayout_36 = new QHBoxLayout(widget_archive_sidebar_score_header);
        horizontalLayout_36->setSpacing(4);
        horizontalLayout_36->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_36->setObjectName(QStringLiteral("horizontalLayout_36"));
        horizontalLayout_36->setContentsMargins(4, 2, 4, 2);
        label_31 = new QLabel(widget_archive_sidebar_score_header);
        label_31->setObjectName(QStringLiteral("label_31"));
        label_31->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/handle1.png")));

        horizontalLayout_36->addWidget(label_31);

        label_24 = new QLabel(widget_archive_sidebar_score_header);
        label_24->setObjectName(QStringLiteral("label_24"));
        label_24->setMaximumSize(QSize(12, 12));
        label_24->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/star_32.png")));
        label_24->setScaledContents(true);

        horizontalLayout_36->addWidget(label_24);

        label_archive_sidebar_score = new QLabel(widget_archive_sidebar_score_header);
        label_archive_sidebar_score->setObjectName(QStringLiteral("label_archive_sidebar_score"));

        horizontalLayout_36->addWidget(label_archive_sidebar_score);

        horizontalSpacer_15 = new QSpacerItem(40, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_36->addItem(horizontalSpacer_15);

        label_archive_sidebar_score_toggle = new QLabel(widget_archive_sidebar_score_header);
        label_archive_sidebar_score_toggle->setObjectName(QStringLiteral("label_archive_sidebar_score_toggle"));
        label_archive_sidebar_score_toggle->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/stylesheet-branch-open.png")));

        horizontalLayout_36->addWidget(label_archive_sidebar_score_toggle);


        verticalLayout_23->addWidget(widget_archive_sidebar_score_header);

        widget_archive_sidebar_score_shadow = new QWidget(scrollAreaWidgetContents_5);
        widget_archive_sidebar_score_shadow->setObjectName(QStringLiteral("widget_archive_sidebar_score_shadow"));
        widget_archive_sidebar_score_shadow->setMinimumSize(QSize(0, 9));
        widget_archive_sidebar_score_shadow->setMaximumSize(QSize(16777215, 9));

        verticalLayout_23->addWidget(widget_archive_sidebar_score_shadow);

        widget_archive_sidebar_score_content = new QWidget(scrollAreaWidgetContents_5);
        widget_archive_sidebar_score_content->setObjectName(QStringLiteral("widget_archive_sidebar_score_content"));
        verticalLayout_13 = new QVBoxLayout(widget_archive_sidebar_score_content);
        verticalLayout_13->setSpacing(6);
        verticalLayout_13->setContentsMargins(11, 11, 11, 11);
        verticalLayout_13->setObjectName(QStringLiteral("verticalLayout_13"));
        verticalLayout_13->setContentsMargins(-1, 0, -1, -1);
        label_score = new QLabel(widget_archive_sidebar_score_content);
        label_score->setObjectName(QStringLiteral("label_score"));
        QFont font6;
        font6.setPointSize(22);
        label_score->setFont(font6);
        label_score->setAlignment(Qt::AlignCenter);

        verticalLayout_13->addWidget(label_score);

        horizontalLayout_20 = new QHBoxLayout();
        horizontalLayout_20->setSpacing(6);
        horizontalLayout_20->setObjectName(QStringLiteral("horizontalLayout_20"));
        horizontalLayout_20->setContentsMargins(-1, 0, -1, -1);
        horizontalSpacer_26 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_20->addItem(horizontalSpacer_26);

        label_scoreStar1 = new QLabel(widget_archive_sidebar_score_content);
        label_scoreStar1->setObjectName(QStringLiteral("label_scoreStar1"));
        label_scoreStar1->setMinimumSize(QSize(24, 24));
        label_scoreStar1->setMaximumSize(QSize(24, 24));
        label_scoreStar1->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/star_off_d_32.png")));
        label_scoreStar1->setScaledContents(true);

        horizontalLayout_20->addWidget(label_scoreStar1);

        label_scoreStar2 = new QLabel(widget_archive_sidebar_score_content);
        label_scoreStar2->setObjectName(QStringLiteral("label_scoreStar2"));
        label_scoreStar2->setMinimumSize(QSize(24, 24));
        label_scoreStar2->setMaximumSize(QSize(24, 24));
        label_scoreStar2->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/star_off_d_32.png")));
        label_scoreStar2->setScaledContents(true);

        horizontalLayout_20->addWidget(label_scoreStar2);

        label_scoreStar3 = new QLabel(widget_archive_sidebar_score_content);
        label_scoreStar3->setObjectName(QStringLiteral("label_scoreStar3"));
        label_scoreStar3->setMinimumSize(QSize(24, 24));
        label_scoreStar3->setMaximumSize(QSize(24, 24));
        label_scoreStar3->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/star_off_d_32.png")));
        label_scoreStar3->setScaledContents(true);

        horizontalLayout_20->addWidget(label_scoreStar3);

        label_scoreStar4 = new QLabel(widget_archive_sidebar_score_content);
        label_scoreStar4->setObjectName(QStringLiteral("label_scoreStar4"));
        label_scoreStar4->setMinimumSize(QSize(24, 24));
        label_scoreStar4->setMaximumSize(QSize(24, 24));
        label_scoreStar4->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/star_off_d_32.png")));
        label_scoreStar4->setScaledContents(true);

        horizontalLayout_20->addWidget(label_scoreStar4);

        label_scoreStar5 = new QLabel(widget_archive_sidebar_score_content);
        label_scoreStar5->setObjectName(QStringLiteral("label_scoreStar5"));
        label_scoreStar5->setMinimumSize(QSize(24, 24));
        label_scoreStar5->setMaximumSize(QSize(24, 24));
        label_scoreStar5->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/star_off_d_32.png")));
        label_scoreStar5->setScaledContents(true);

        horizontalLayout_20->addWidget(label_scoreStar5);

        horizontalSpacer_27 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_20->addItem(horizontalSpacer_27);


        verticalLayout_13->addLayout(horizontalLayout_20);


        verticalLayout_23->addWidget(widget_archive_sidebar_score_content);

        widget_archive_sidebar_sites_header = new clickable_QWidget(scrollAreaWidgetContents_5);
        widget_archive_sidebar_sites_header->setObjectName(QStringLiteral("widget_archive_sidebar_sites_header"));
        widget_archive_sidebar_sites_header->setMinimumSize(QSize(0, 0));
        widget_archive_sidebar_sites_header->setCursor(QCursor(Qt::PointingHandCursor));
        horizontalLayout_38 = new QHBoxLayout(widget_archive_sidebar_sites_header);
        horizontalLayout_38->setSpacing(4);
        horizontalLayout_38->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_38->setObjectName(QStringLiteral("horizontalLayout_38"));
        horizontalLayout_38->setContentsMargins(4, 2, 4, 2);
        label_32 = new QLabel(widget_archive_sidebar_sites_header);
        label_32->setObjectName(QStringLiteral("label_32"));
        label_32->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/handle1.png")));

        horizontalLayout_38->addWidget(label_32);

        label_27 = new QLabel(widget_archive_sidebar_sites_header);
        label_27->setObjectName(QStringLiteral("label_27"));
        label_27->setMaximumSize(QSize(12, 12));
        label_27->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/siteIcon500px.png")));
        label_27->setScaledContents(true);

        horizontalLayout_38->addWidget(label_27);

        label_archive_sidebar_sites = new QLabel(widget_archive_sidebar_sites_header);
        label_archive_sidebar_sites->setObjectName(QStringLiteral("label_archive_sidebar_sites"));

        horizontalLayout_38->addWidget(label_archive_sidebar_sites);

        horizontalSpacer_22 = new QSpacerItem(40, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_38->addItem(horizontalSpacer_22);

        label_archive_sidebar_sites_toggle = new QLabel(widget_archive_sidebar_sites_header);
        label_archive_sidebar_sites_toggle->setObjectName(QStringLiteral("label_archive_sidebar_sites_toggle"));
        label_archive_sidebar_sites_toggle->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/stylesheet-branch-open.png")));

        horizontalLayout_38->addWidget(label_archive_sidebar_sites_toggle);


        verticalLayout_23->addWidget(widget_archive_sidebar_sites_header);

        widget_archive_sidebar_sites_shadow = new QWidget(scrollAreaWidgetContents_5);
        widget_archive_sidebar_sites_shadow->setObjectName(QStringLiteral("widget_archive_sidebar_sites_shadow"));
        widget_archive_sidebar_sites_shadow->setMinimumSize(QSize(0, 9));
        widget_archive_sidebar_sites_shadow->setMaximumSize(QSize(16777215, 9));

        verticalLayout_23->addWidget(widget_archive_sidebar_sites_shadow);

        widget_archive_sidebar_sites_content = new QWidget(scrollAreaWidgetContents_5);
        widget_archive_sidebar_sites_content->setObjectName(QStringLiteral("widget_archive_sidebar_sites_content"));
        verticalLayout_19 = new QVBoxLayout(widget_archive_sidebar_sites_content);
        verticalLayout_19->setSpacing(6);
        verticalLayout_19->setContentsMargins(11, 11, 11, 11);
        verticalLayout_19->setObjectName(QStringLiteral("verticalLayout_19"));
        verticalLayout_19->setContentsMargins(-1, 0, -1, -1);
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setSpacing(0);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        gridLayout_3->setContentsMargins(-1, 0, -1, -1);
        label_siteFlickrViews = new QLabel(widget_archive_sidebar_sites_content);
        label_siteFlickrViews->setObjectName(QStringLiteral("label_siteFlickrViews"));
        label_siteFlickrViews->setMinimumSize(QSize(40, 0));
        label_siteFlickrViews->setMaximumSize(QSize(40, 16777215));
        label_siteFlickrViews->setAlignment(Qt::AlignCenter);

        gridLayout_3->addWidget(label_siteFlickrViews, 2, 4, 1, 1);

        horizontalLayout_17 = new QHBoxLayout();
        horizontalLayout_17->setSpacing(6);
        horizontalLayout_17->setObjectName(QStringLiteral("horizontalLayout_17"));
        label_siteDaIcon = new clickable_QLabel(widget_archive_sidebar_sites_content);
        label_siteDaIcon->setObjectName(QStringLiteral("label_siteDaIcon"));
        label_siteDaIcon->setMinimumSize(QSize(32, 32));
        label_siteDaIcon->setMaximumSize(QSize(32, 32));
        label_siteDaIcon->setSizeIncrement(QSize(0, 0));
        label_siteDaIcon->setCursor(QCursor(Qt::PointingHandCursor));
        label_siteDaIcon->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/siteIconDeviantArt_GREY.png")));
        label_siteDaIcon->setScaledContents(true);
        label_siteDaIcon->setAlignment(Qt::AlignCenter);
        label_siteDaIcon->setMargin(0);
        label_siteDaIcon->setIndent(10);

        horizontalLayout_17->addWidget(label_siteDaIcon, 0, Qt::AlignHCenter|Qt::AlignVCenter);


        gridLayout_3->addLayout(horizontalLayout_17, 0, 1, 1, 1);

        label_siteFBFavs = new QLabel(widget_archive_sidebar_sites_content);
        label_siteFBFavs->setObjectName(QStringLiteral("label_siteFBFavs"));
        label_siteFBFavs->setAlignment(Qt::AlignCenter);

        gridLayout_3->addWidget(label_siteFBFavs, 3, 5, 1, 1);

        label_siteFBViews = new QLabel(widget_archive_sidebar_sites_content);
        label_siteFBViews->setObjectName(QStringLiteral("label_siteFBViews"));
        label_siteFBViews->setMinimumSize(QSize(40, 0));
        label_siteFBViews->setMaximumSize(QSize(40, 16777215));
        label_siteFBViews->setAlignment(Qt::AlignCenter);

        gridLayout_3->addWidget(label_siteFBViews, 2, 5, 1, 1);

        horizontalLayout_19 = new QHBoxLayout();
        horizontalLayout_19->setSpacing(6);
        horizontalLayout_19->setObjectName(QStringLiteral("horizontalLayout_19"));
        label_siteGPIcon = new clickable_QLabel(widget_archive_sidebar_sites_content);
        label_siteGPIcon->setObjectName(QStringLiteral("label_siteGPIcon"));
        label_siteGPIcon->setMinimumSize(QSize(30, 30));
        label_siteGPIcon->setMaximumSize(QSize(30, 30));
        label_siteGPIcon->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/siteIconGoogle+_GREY.png")));
        label_siteGPIcon->setScaledContents(true);
        label_siteGPIcon->setAlignment(Qt::AlignCenter);

        horizontalLayout_19->addWidget(label_siteGPIcon);


        gridLayout_3->addLayout(horizontalLayout_19, 0, 3, 1, 1);

        horizontalLayout_18 = new QHBoxLayout();
        horizontalLayout_18->setSpacing(6);
        horizontalLayout_18->setObjectName(QStringLiteral("horizontalLayout_18"));
        label_site500pxIcon = new clickable_QLabel(widget_archive_sidebar_sites_content);
        label_site500pxIcon->setObjectName(QStringLiteral("label_site500pxIcon"));
        label_site500pxIcon->setMinimumSize(QSize(32, 32));
        label_site500pxIcon->setMaximumSize(QSize(32, 32));
        label_site500pxIcon->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/siteIcon500px_GREY.png")));
        label_site500pxIcon->setScaledContents(true);
        label_site500pxIcon->setAlignment(Qt::AlignCenter);

        horizontalLayout_18->addWidget(label_site500pxIcon);


        gridLayout_3->addLayout(horizontalLayout_18, 0, 2, 1, 1);

        label_siteFlickrFavs = new QLabel(widget_archive_sidebar_sites_content);
        label_siteFlickrFavs->setObjectName(QStringLiteral("label_siteFlickrFavs"));
        label_siteFlickrFavs->setAlignment(Qt::AlignCenter);

        gridLayout_3->addWidget(label_siteFlickrFavs, 3, 4, 1, 1);

        label_siteGPFavs = new QLabel(widget_archive_sidebar_sites_content);
        label_siteGPFavs->setObjectName(QStringLiteral("label_siteGPFavs"));
        label_siteGPFavs->setAlignment(Qt::AlignCenter);

        gridLayout_3->addWidget(label_siteGPFavs, 3, 3, 1, 1);

        label_9 = new QLabel(widget_archive_sidebar_sites_content);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setMaximumSize(QSize(18, 18));
        label_9->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/eye.png")));
        label_9->setScaledContents(true);

        gridLayout_3->addWidget(label_9, 2, 0, 1, 1);

        label_10 = new QLabel(widget_archive_sidebar_sites_content);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setMaximumSize(QSize(16, 16));
        label_10->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/star_32.png")));
        label_10->setScaledContents(true);

        gridLayout_3->addWidget(label_10, 3, 0, 1, 1);

        verticalSpacer_9 = new QSpacerItem(20, 16, QSizePolicy::Minimum, QSizePolicy::Fixed);

        gridLayout_3->addItem(verticalSpacer_9, 4, 3, 1, 1);

        horizontalLayout_28 = new QHBoxLayout();
        horizontalLayout_28->setSpacing(6);
        horizontalLayout_28->setObjectName(QStringLiteral("horizontalLayout_28"));
        label_sitePixotoIcon = new clickable_QLabel(widget_archive_sidebar_sites_content);
        label_sitePixotoIcon->setObjectName(QStringLiteral("label_sitePixotoIcon"));
        label_sitePixotoIcon->setMinimumSize(QSize(32, 32));
        label_sitePixotoIcon->setMaximumSize(QSize(32, 32));
        label_sitePixotoIcon->setSizeIncrement(QSize(0, 0));
        label_sitePixotoIcon->setCursor(QCursor(Qt::PointingHandCursor));
        label_sitePixotoIcon->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/siteIconPixoto1_GREY.png")));
        label_sitePixotoIcon->setScaledContents(true);
        label_sitePixotoIcon->setAlignment(Qt::AlignCenter);
        label_sitePixotoIcon->setMargin(0);
        label_sitePixotoIcon->setIndent(10);

        horizontalLayout_28->addWidget(label_sitePixotoIcon, 0, Qt::AlignHCenter|Qt::AlignVCenter);


        gridLayout_3->addLayout(horizontalLayout_28, 5, 1, 1, 1);

        label_sitePixotoViews = new QLabel(widget_archive_sidebar_sites_content);
        label_sitePixotoViews->setObjectName(QStringLiteral("label_sitePixotoViews"));
        label_sitePixotoViews->setMinimumSize(QSize(40, 0));
        label_sitePixotoViews->setMaximumSize(QSize(40, 16777215));
        QFont font7;
        font7.setBold(false);
        font7.setWeight(50);
        label_sitePixotoViews->setFont(font7);
        label_sitePixotoViews->setAlignment(Qt::AlignCenter);

        gridLayout_3->addWidget(label_sitePixotoViews, 6, 1, 1, 1);

        label_sitePixotoFavs = new QLabel(widget_archive_sidebar_sites_content);
        label_sitePixotoFavs->setObjectName(QStringLiteral("label_sitePixotoFavs"));
        label_sitePixotoFavs->setAlignment(Qt::AlignCenter);

        gridLayout_3->addWidget(label_sitePixotoFavs, 7, 1, 1, 1);

        label_siteYoupicViews = new QLabel(widget_archive_sidebar_sites_content);
        label_siteYoupicViews->setObjectName(QStringLiteral("label_siteYoupicViews"));
        label_siteYoupicViews->setMinimumSize(QSize(40, 0));
        label_siteYoupicViews->setMaximumSize(QSize(40, 16777215));
        label_siteYoupicViews->setFont(font7);
        label_siteYoupicViews->setAlignment(Qt::AlignCenter);

        gridLayout_3->addWidget(label_siteYoupicViews, 6, 2, 1, 1);

        label_siteYoupicFavs = new QLabel(widget_archive_sidebar_sites_content);
        label_siteYoupicFavs->setObjectName(QStringLiteral("label_siteYoupicFavs"));
        label_siteYoupicFavs->setAlignment(Qt::AlignCenter);

        gridLayout_3->addWidget(label_siteYoupicFavs, 7, 2, 1, 1);

        horizontalLayout_44 = new QHBoxLayout();
        horizontalLayout_44->setSpacing(6);
        horizontalLayout_44->setObjectName(QStringLiteral("horizontalLayout_44"));
        label_siteYoupicIcon = new clickable_QLabel(widget_archive_sidebar_sites_content);
        label_siteYoupicIcon->setObjectName(QStringLiteral("label_siteYoupicIcon"));
        label_siteYoupicIcon->setMinimumSize(QSize(32, 32));
        label_siteYoupicIcon->setMaximumSize(QSize(32, 32));
        label_siteYoupicIcon->setSizeIncrement(QSize(0, 0));
        label_siteYoupicIcon->setCursor(QCursor(Qt::PointingHandCursor));
        label_siteYoupicIcon->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/siteIconYoupic_GREY.png")));
        label_siteYoupicIcon->setScaledContents(true);
        label_siteYoupicIcon->setAlignment(Qt::AlignCenter);
        label_siteYoupicIcon->setMargin(0);
        label_siteYoupicIcon->setIndent(10);

        horizontalLayout_44->addWidget(label_siteYoupicIcon, 0, Qt::AlignHCenter|Qt::AlignVCenter);


        gridLayout_3->addLayout(horizontalLayout_44, 5, 2, 1, 1);

        label_18 = new QLabel(widget_archive_sidebar_sites_content);
        label_18->setObjectName(QStringLiteral("label_18"));
        label_18->setMaximumSize(QSize(18, 18));
        label_18->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/eye.png")));
        label_18->setScaledContents(true);

        gridLayout_3->addWidget(label_18, 6, 0, 1, 1);

        horizontalLayout_22 = new QHBoxLayout();
        horizontalLayout_22->setSpacing(6);
        horizontalLayout_22->setObjectName(QStringLiteral("horizontalLayout_22"));
        label_siteFlickrIcon = new clickable_QLabel(widget_archive_sidebar_sites_content);
        label_siteFlickrIcon->setObjectName(QStringLiteral("label_siteFlickrIcon"));
        label_siteFlickrIcon->setMinimumSize(QSize(32, 32));
        label_siteFlickrIcon->setMaximumSize(QSize(32, 32));
        label_siteFlickrIcon->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/siteIconFlickr_GREY.png")));
        label_siteFlickrIcon->setScaledContents(true);
        label_siteFlickrIcon->setAlignment(Qt::AlignCenter);

        horizontalLayout_22->addWidget(label_siteFlickrIcon);


        gridLayout_3->addLayout(horizontalLayout_22, 0, 4, 1, 1);

        horizontalLayout_23 = new QHBoxLayout();
        horizontalLayout_23->setSpacing(6);
        horizontalLayout_23->setObjectName(QStringLiteral("horizontalLayout_23"));
        label_siteFBIcon = new clickable_QLabel(widget_archive_sidebar_sites_content);
        label_siteFBIcon->setObjectName(QStringLiteral("label_siteFBIcon"));
        label_siteFBIcon->setMinimumSize(QSize(30, 30));
        label_siteFBIcon->setMaximumSize(QSize(30, 30));
        label_siteFBIcon->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/siteIconFaceBook_GREY.png")));
        label_siteFBIcon->setScaledContents(true);
        label_siteFBIcon->setAlignment(Qt::AlignCenter);

        horizontalLayout_23->addWidget(label_siteFBIcon);


        gridLayout_3->addLayout(horizontalLayout_23, 0, 5, 1, 1);

        label_site500pxFavs = new QLabel(widget_archive_sidebar_sites_content);
        label_site500pxFavs->setObjectName(QStringLiteral("label_site500pxFavs"));
        label_site500pxFavs->setAlignment(Qt::AlignCenter);

        gridLayout_3->addWidget(label_site500pxFavs, 3, 2, 1, 1);

        label_siteGPViews = new QLabel(widget_archive_sidebar_sites_content);
        label_siteGPViews->setObjectName(QStringLiteral("label_siteGPViews"));
        label_siteGPViews->setMinimumSize(QSize(40, 0));
        label_siteGPViews->setMaximumSize(QSize(40, 16777215));
        label_siteGPViews->setAlignment(Qt::AlignCenter);

        gridLayout_3->addWidget(label_siteGPViews, 2, 3, 1, 1);

        label_siteDaFavs = new QLabel(widget_archive_sidebar_sites_content);
        label_siteDaFavs->setObjectName(QStringLiteral("label_siteDaFavs"));
        label_siteDaFavs->setAlignment(Qt::AlignCenter);

        gridLayout_3->addWidget(label_siteDaFavs, 3, 1, 1, 1);

        label_siteDaViews = new QLabel(widget_archive_sidebar_sites_content);
        label_siteDaViews->setObjectName(QStringLiteral("label_siteDaViews"));
        label_siteDaViews->setMinimumSize(QSize(40, 0));
        label_siteDaViews->setMaximumSize(QSize(40, 16777215));
        label_siteDaViews->setFont(font7);
        label_siteDaViews->setAlignment(Qt::AlignCenter);

        gridLayout_3->addWidget(label_siteDaViews, 2, 1, 1, 1);

        label_site500pxViews = new QLabel(widget_archive_sidebar_sites_content);
        label_site500pxViews->setObjectName(QStringLiteral("label_site500pxViews"));
        label_site500pxViews->setMinimumSize(QSize(40, 0));
        label_site500pxViews->setMaximumSize(QSize(40, 16777215));
        label_site500pxViews->setAlignment(Qt::AlignCenter);

        gridLayout_3->addWidget(label_site500pxViews, 2, 2, 1, 1);

        label_19 = new QLabel(widget_archive_sidebar_sites_content);
        label_19->setObjectName(QStringLiteral("label_19"));
        label_19->setMaximumSize(QSize(16, 16));
        label_19->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/star_32.png")));
        label_19->setScaledContents(true);

        gridLayout_3->addWidget(label_19, 7, 0, 1, 1);

        horizontalLayout_45 = new QHBoxLayout();
        horizontalLayout_45->setSpacing(6);
        horizontalLayout_45->setObjectName(QStringLiteral("horizontalLayout_45"));
        label_siteViewbugIcon = new clickable_QLabel(widget_archive_sidebar_sites_content);
        label_siteViewbugIcon->setObjectName(QStringLiteral("label_siteViewbugIcon"));
        label_siteViewbugIcon->setMinimumSize(QSize(32, 32));
        label_siteViewbugIcon->setMaximumSize(QSize(32, 32));
        label_siteViewbugIcon->setSizeIncrement(QSize(0, 0));
        label_siteViewbugIcon->setCursor(QCursor(Qt::PointingHandCursor));
        label_siteViewbugIcon->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/siteIconViewbug_GREY.png")));
        label_siteViewbugIcon->setScaledContents(true);
        label_siteViewbugIcon->setAlignment(Qt::AlignCenter);
        label_siteViewbugIcon->setMargin(0);
        label_siteViewbugIcon->setIndent(10);

        horizontalLayout_45->addWidget(label_siteViewbugIcon, 0, Qt::AlignHCenter|Qt::AlignVCenter);


        gridLayout_3->addLayout(horizontalLayout_45, 5, 3, 1, 1);

        label_siteViewbugViews = new QLabel(widget_archive_sidebar_sites_content);
        label_siteViewbugViews->setObjectName(QStringLiteral("label_siteViewbugViews"));
        label_siteViewbugViews->setMinimumSize(QSize(40, 0));
        label_siteViewbugViews->setMaximumSize(QSize(40, 16777215));
        label_siteViewbugViews->setFont(font7);
        label_siteViewbugViews->setAlignment(Qt::AlignCenter);

        gridLayout_3->addWidget(label_siteViewbugViews, 6, 3, 1, 1);

        label_siteViewbugFavs = new QLabel(widget_archive_sidebar_sites_content);
        label_siteViewbugFavs->setObjectName(QStringLiteral("label_siteViewbugFavs"));
        label_siteViewbugFavs->setAlignment(Qt::AlignCenter);

        gridLayout_3->addWidget(label_siteViewbugFavs, 7, 3, 1, 1);

        horizontalLayout_46 = new QHBoxLayout();
        horizontalLayout_46->setSpacing(6);
        horizontalLayout_46->setObjectName(QStringLiteral("horizontalLayout_46"));
        label_siteRdIcon = new clickable_QLabel(widget_archive_sidebar_sites_content);
        label_siteRdIcon->setObjectName(QStringLiteral("label_siteRdIcon"));
        label_siteRdIcon->setMinimumSize(QSize(32, 32));
        label_siteRdIcon->setMaximumSize(QSize(32, 32));
        label_siteRdIcon->setSizeIncrement(QSize(0, 0));
        label_siteRdIcon->setCursor(QCursor(Qt::PointingHandCursor));
        label_siteRdIcon->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/siteIconRd_GREY.png")));
        label_siteRdIcon->setScaledContents(true);
        label_siteRdIcon->setAlignment(Qt::AlignCenter);
        label_siteRdIcon->setMargin(0);
        label_siteRdIcon->setIndent(10);

        horizontalLayout_46->addWidget(label_siteRdIcon, 0, Qt::AlignHCenter|Qt::AlignVCenter);


        gridLayout_3->addLayout(horizontalLayout_46, 5, 4, 1, 1);

        label_siteRdViews = new QLabel(widget_archive_sidebar_sites_content);
        label_siteRdViews->setObjectName(QStringLiteral("label_siteRdViews"));
        label_siteRdViews->setMinimumSize(QSize(40, 0));
        label_siteRdViews->setMaximumSize(QSize(40, 16777215));
        label_siteRdViews->setFont(font7);
        label_siteRdViews->setAlignment(Qt::AlignCenter);

        gridLayout_3->addWidget(label_siteRdViews, 6, 4, 1, 1);

        label_siteRdFavs = new QLabel(widget_archive_sidebar_sites_content);
        label_siteRdFavs->setObjectName(QStringLiteral("label_siteRdFavs"));
        label_siteRdFavs->setAlignment(Qt::AlignCenter);

        gridLayout_3->addWidget(label_siteRdFavs, 7, 4, 1, 1);

        horizontalLayout_47 = new QHBoxLayout();
        horizontalLayout_47->setSpacing(6);
        horizontalLayout_47->setObjectName(QStringLiteral("horizontalLayout_47"));
        label_siteIgIcon = new clickable_QLabel(widget_archive_sidebar_sites_content);
        label_siteIgIcon->setObjectName(QStringLiteral("label_siteIgIcon"));
        label_siteIgIcon->setMinimumSize(QSize(32, 32));
        label_siteIgIcon->setMaximumSize(QSize(32, 32));
        label_siteIgIcon->setSizeIncrement(QSize(0, 0));
        label_siteIgIcon->setCursor(QCursor(Qt::PointingHandCursor));
        label_siteIgIcon->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/siteIconInstagram_GREY.png")));
        label_siteIgIcon->setScaledContents(true);
        label_siteIgIcon->setAlignment(Qt::AlignCenter);
        label_siteIgIcon->setMargin(0);
        label_siteIgIcon->setIndent(10);

        horizontalLayout_47->addWidget(label_siteIgIcon, 0, Qt::AlignHCenter|Qt::AlignVCenter);


        gridLayout_3->addLayout(horizontalLayout_47, 5, 5, 1, 1);

        label_siteIgViews = new QLabel(widget_archive_sidebar_sites_content);
        label_siteIgViews->setObjectName(QStringLiteral("label_siteIgViews"));
        label_siteIgViews->setMinimumSize(QSize(40, 0));
        label_siteIgViews->setMaximumSize(QSize(40, 16777215));
        label_siteIgViews->setFont(font7);
        label_siteIgViews->setAlignment(Qt::AlignCenter);

        gridLayout_3->addWidget(label_siteIgViews, 6, 5, 1, 1);

        label_siteIgFavs = new QLabel(widget_archive_sidebar_sites_content);
        label_siteIgFavs->setObjectName(QStringLiteral("label_siteIgFavs"));
        label_siteIgFavs->setAlignment(Qt::AlignCenter);

        gridLayout_3->addWidget(label_siteIgFavs, 7, 5, 1, 1);


        verticalLayout_19->addLayout(gridLayout_3);

        frame_line = new QFrame(widget_archive_sidebar_sites_content);
        frame_line->setObjectName(QStringLiteral("frame_line"));
        frame_line->setMinimumSize(QSize(0, 2));
        frame_line->setMaximumSize(QSize(16777215, 2));
        frame_line->setFrameShape(QFrame::StyledPanel);
        frame_line->setFrameShadow(QFrame::Sunken);

        verticalLayout_19->addWidget(frame_line);

        gridLayout_4 = new QGridLayout();
        gridLayout_4->setSpacing(6);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        gridLayout_4->setHorizontalSpacing(6);
        gridLayout_4->setVerticalSpacing(2);
        gridLayout_4->setContentsMargins(2, 0, -1, -1);
        label_siteTotalFavs = new QLabel(widget_archive_sidebar_sites_content);
        label_siteTotalFavs->setObjectName(QStringLiteral("label_siteTotalFavs"));
        label_siteTotalFavs->setFont(font1);
        label_siteTotalFavs->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_4->addWidget(label_siteTotalFavs, 1, 4, 1, 1);

        label_siteTotalViews = new QLabel(widget_archive_sidebar_sites_content);
        label_siteTotalViews->setObjectName(QStringLiteral("label_siteTotalViews"));
        label_siteTotalViews->setMinimumSize(QSize(50, 0));
        label_siteTotalViews->setMaximumSize(QSize(50, 16777215));
        label_siteTotalViews->setFont(font1);
        label_siteTotalViews->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_4->addWidget(label_siteTotalViews, 0, 4, 1, 1);

        label_20 = new QLabel(widget_archive_sidebar_sites_content);
        label_20->setObjectName(QStringLiteral("label_20"));
        label_20->setMaximumSize(QSize(18, 18));
        label_20->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/eye.png")));
        label_20->setScaledContents(true);

        gridLayout_4->addWidget(label_20, 0, 0, 1, 1);

        label_8 = new QLabel(widget_archive_sidebar_sites_content);
        label_8->setObjectName(QStringLiteral("label_8"));

        gridLayout_4->addWidget(label_8, 0, 2, 1, 1);

        label_21 = new QLabel(widget_archive_sidebar_sites_content);
        label_21->setObjectName(QStringLiteral("label_21"));
        label_21->setMaximumSize(QSize(16, 16));
        label_21->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/star_32.png")));
        label_21->setScaledContents(true);

        gridLayout_4->addWidget(label_21, 1, 0, 1, 1);

        label_4 = new QLabel(widget_archive_sidebar_sites_content);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout_4->addWidget(label_4, 1, 2, 1, 1);


        verticalLayout_19->addLayout(gridLayout_4);

        verticalLayout_lineGraph = new QVBoxLayout();
        verticalLayout_lineGraph->setSpacing(6);
        verticalLayout_lineGraph->setObjectName(QStringLiteral("verticalLayout_lineGraph"));
        verticalLayout_lineGraph->setContentsMargins(-1, 10, -1, -1);
        widget_lineGraph = new lineGraphWidget(widget_archive_sidebar_sites_content);
        widget_lineGraph->setObjectName(QStringLiteral("widget_lineGraph"));
        widget_lineGraph->setEnabled(true);
        widget_lineGraph->setMinimumSize(QSize(0, 10));

        verticalLayout_lineGraph->addWidget(widget_lineGraph);


        verticalLayout_19->addLayout(verticalLayout_lineGraph);

        frame_line_2 = new QFrame(widget_archive_sidebar_sites_content);
        frame_line_2->setObjectName(QStringLiteral("frame_line_2"));
        frame_line_2->setMinimumSize(QSize(0, 2));
        frame_line_2->setMaximumSize(QSize(16777215, 2));
        frame_line_2->setFrameShape(QFrame::StyledPanel);
        frame_line_2->setFrameShadow(QFrame::Sunken);

        verticalLayout_19->addWidget(frame_line_2);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setSpacing(6);
        horizontalLayout_14->setObjectName(QStringLiteral("horizontalLayout_14"));
        horizontalLayout_14->setContentsMargins(27, 0, -1, 9);
        label_35 = new QLabel(widget_archive_sidebar_sites_content);
        label_35->setObjectName(QStringLiteral("label_35"));

        horizontalLayout_14->addWidget(label_35);

        label_archive_uptoDate = new QLabel(widget_archive_sidebar_sites_content);
        label_archive_uptoDate->setObjectName(QStringLiteral("label_archive_uptoDate"));
        label_archive_uptoDate->setFont(font1);
        label_archive_uptoDate->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_14->addWidget(label_archive_uptoDate);


        verticalLayout_19->addLayout(horizontalLayout_14);

        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setSpacing(6);
        horizontalLayout_15->setObjectName(QStringLiteral("horizontalLayout_15"));
        horizontalSpacer_20 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_15->addItem(horizontalSpacer_20);

        pushButton_sitesManage = new QPushButton(widget_archive_sidebar_sites_content);
        pushButton_sitesManage->setObjectName(QStringLiteral("pushButton_sitesManage"));

        horizontalLayout_15->addWidget(pushButton_sitesManage);

        pushButton_sitesUpdate = new QPushButton(widget_archive_sidebar_sites_content);
        pushButton_sitesUpdate->setObjectName(QStringLiteral("pushButton_sitesUpdate"));

        horizontalLayout_15->addWidget(pushButton_sitesUpdate);

        pushButton_addNewSite = new QPushButton(widget_archive_sidebar_sites_content);
        pushButton_addNewSite->setObjectName(QStringLiteral("pushButton_addNewSite"));

        horizontalLayout_15->addWidget(pushButton_addNewSite);


        verticalLayout_19->addLayout(horizontalLayout_15);


        verticalLayout_23->addWidget(widget_archive_sidebar_sites_content);

        widget_archive_sidebar_keywords_header = new clickable_QWidget(scrollAreaWidgetContents_5);
        widget_archive_sidebar_keywords_header->setObjectName(QStringLiteral("widget_archive_sidebar_keywords_header"));
        widget_archive_sidebar_keywords_header->setMinimumSize(QSize(0, 0));
        widget_archive_sidebar_keywords_header->setCursor(QCursor(Qt::PointingHandCursor));
        horizontalLayout_40 = new QHBoxLayout(widget_archive_sidebar_keywords_header);
        horizontalLayout_40->setSpacing(4);
        horizontalLayout_40->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_40->setObjectName(QStringLiteral("horizontalLayout_40"));
        horizontalLayout_40->setContentsMargins(4, 2, 4, 2);
        label_33 = new QLabel(widget_archive_sidebar_keywords_header);
        label_33->setObjectName(QStringLiteral("label_33"));
        label_33->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/handle1.png")));

        horizontalLayout_40->addWidget(label_33);

        label_25 = new QLabel(widget_archive_sidebar_keywords_header);
        label_25->setObjectName(QStringLiteral("label_25"));
        label_25->setMaximumSize(QSize(12, 12));
        label_25->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/numbersign.png")));
        label_25->setScaledContents(true);

        horizontalLayout_40->addWidget(label_25);

        label_archive_sidebar_keywords = new QLabel(widget_archive_sidebar_keywords_header);
        label_archive_sidebar_keywords->setObjectName(QStringLiteral("label_archive_sidebar_keywords"));

        horizontalLayout_40->addWidget(label_archive_sidebar_keywords);

        horizontalSpacer_24 = new QSpacerItem(40, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_40->addItem(horizontalSpacer_24);

        label_archive_sidebar_keywords_toggle = new QLabel(widget_archive_sidebar_keywords_header);
        label_archive_sidebar_keywords_toggle->setObjectName(QStringLiteral("label_archive_sidebar_keywords_toggle"));
        label_archive_sidebar_keywords_toggle->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/stylesheet-branch-open.png")));

        horizontalLayout_40->addWidget(label_archive_sidebar_keywords_toggle);


        verticalLayout_23->addWidget(widget_archive_sidebar_keywords_header);

        widget_archive_sidebar_keywords_shadow = new QWidget(scrollAreaWidgetContents_5);
        widget_archive_sidebar_keywords_shadow->setObjectName(QStringLiteral("widget_archive_sidebar_keywords_shadow"));
        widget_archive_sidebar_keywords_shadow->setMinimumSize(QSize(0, 9));
        widget_archive_sidebar_keywords_shadow->setMaximumSize(QSize(16777215, 9));

        verticalLayout_23->addWidget(widget_archive_sidebar_keywords_shadow);

        widget_archive_sidebar_keywords_content = new QWidget(scrollAreaWidgetContents_5);
        widget_archive_sidebar_keywords_content->setObjectName(QStringLiteral("widget_archive_sidebar_keywords_content"));
        verticalLayout_20 = new QVBoxLayout(widget_archive_sidebar_keywords_content);
        verticalLayout_20->setSpacing(6);
        verticalLayout_20->setContentsMargins(11, 11, 11, 11);
        verticalLayout_20->setObjectName(QStringLiteral("verticalLayout_20"));
        verticalLayout_20->setContentsMargins(-1, 0, -1, -1);
        plainTextEdit_archiveKeywords = new QPlainTextEdit(widget_archive_sidebar_keywords_content);
        plainTextEdit_archiveKeywords->setObjectName(QStringLiteral("plainTextEdit_archiveKeywords"));
        plainTextEdit_archiveKeywords->setEnabled(true);
        QSizePolicy sizePolicy5(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy5.setHorizontalStretch(0);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(plainTextEdit_archiveKeywords->sizePolicy().hasHeightForWidth());
        plainTextEdit_archiveKeywords->setSizePolicy(sizePolicy5);
        plainTextEdit_archiveKeywords->setMinimumSize(QSize(0, 0));
        plainTextEdit_archiveKeywords->setBaseSize(QSize(0, 0));
        plainTextEdit_archiveKeywords->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);

        verticalLayout_20->addWidget(plainTextEdit_archiveKeywords);

        lineEdit_addKeyword = new QLineEdit(widget_archive_sidebar_keywords_content);
        lineEdit_addKeyword->setObjectName(QStringLiteral("lineEdit_addKeyword"));

        verticalLayout_20->addWidget(lineEdit_addKeyword);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalLayout_5->setSizeConstraint(QLayout::SetMinAndMaxSize);
        horizontalLayout_5->setContentsMargins(-1, 0, -1, -1);
        pushButton_quickTag = new QPushButton(widget_archive_sidebar_keywords_content);
        pushButton_quickTag->setObjectName(QStringLiteral("pushButton_quickTag"));
        pushButton_quickTag->setMaximumSize(QSize(30, 16777215));

        horizontalLayout_5->addWidget(pushButton_quickTag);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_3);

        pushButton_archiveCopyKeyComma = new QPushButton(widget_archive_sidebar_keywords_content);
        pushButton_archiveCopyKeyComma->setObjectName(QStringLiteral("pushButton_archiveCopyKeyComma"));
        pushButton_archiveCopyKeyComma->setMaximumSize(QSize(30, 16777215));

        horizontalLayout_5->addWidget(pushButton_archiveCopyKeyComma);

        pushButton_archiveCopyKeySpace = new QPushButton(widget_archive_sidebar_keywords_content);
        pushButton_archiveCopyKeySpace->setObjectName(QStringLiteral("pushButton_archiveCopyKeySpace"));
        pushButton_archiveCopyKeySpace->setMaximumSize(QSize(30, 16777215));

        horizontalLayout_5->addWidget(pushButton_archiveCopyKeySpace);

        pushButton_archiveUpdateKeywords = new QPushButton(widget_archive_sidebar_keywords_content);
        pushButton_archiveUpdateKeywords->setObjectName(QStringLiteral("pushButton_archiveUpdateKeywords"));
        pushButton_archiveUpdateKeywords->setEnabled(false);
        pushButton_archiveUpdateKeywords->setMaximumSize(QSize(50, 16777215));

        horizontalLayout_5->addWidget(pushButton_archiveUpdateKeywords);


        verticalLayout_20->addLayout(horizontalLayout_5);

        listView_quickTag = new QListView(widget_archive_sidebar_keywords_content);
        listView_quickTag->setObjectName(QStringLiteral("listView_quickTag"));
        QSizePolicy sizePolicy6(QSizePolicy::Expanding, QSizePolicy::MinimumExpanding);
        sizePolicy6.setHorizontalStretch(0);
        sizePolicy6.setVerticalStretch(0);
        sizePolicy6.setHeightForWidth(listView_quickTag->sizePolicy().hasHeightForWidth());
        listView_quickTag->setSizePolicy(sizePolicy6);
        listView_quickTag->setMinimumSize(QSize(0, 120));
        listView_quickTag->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        listView_quickTag->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        listView_quickTag->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
        listView_quickTag->setEditTriggers(QAbstractItemView::NoEditTriggers);
        listView_quickTag->setViewMode(QListView::IconMode);
        listView_quickTag->setUniformItemSizes(false);
        listView_quickTag->setSelectionRectVisible(false);

        verticalLayout_20->addWidget(listView_quickTag);


        verticalLayout_23->addWidget(widget_archive_sidebar_keywords_content);

        widget_archive_sidebar_notes_header = new clickable_QWidget(scrollAreaWidgetContents_5);
        widget_archive_sidebar_notes_header->setObjectName(QStringLiteral("widget_archive_sidebar_notes_header"));
        widget_archive_sidebar_notes_header->setMinimumSize(QSize(0, 0));
        widget_archive_sidebar_notes_header->setCursor(QCursor(Qt::PointingHandCursor));
        horizontalLayout_41 = new QHBoxLayout(widget_archive_sidebar_notes_header);
        horizontalLayout_41->setSpacing(4);
        horizontalLayout_41->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_41->setObjectName(QStringLiteral("horizontalLayout_41"));
        horizontalLayout_41->setContentsMargins(4, 2, 4, 2);
        label_34 = new QLabel(widget_archive_sidebar_notes_header);
        label_34->setObjectName(QStringLiteral("label_34"));
        label_34->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/handle1.png")));

        horizontalLayout_41->addWidget(label_34);

        label_26 = new QLabel(widget_archive_sidebar_notes_header);
        label_26->setObjectName(QStringLiteral("label_26"));
        label_26->setMaximumSize(QSize(12, 12));
        label_26->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/Sticky_Notes.png")));
        label_26->setScaledContents(true);

        horizontalLayout_41->addWidget(label_26);

        label_archive_sidebar_notes = new QLabel(widget_archive_sidebar_notes_header);
        label_archive_sidebar_notes->setObjectName(QStringLiteral("label_archive_sidebar_notes"));

        horizontalLayout_41->addWidget(label_archive_sidebar_notes);

        horizontalSpacer_25 = new QSpacerItem(40, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_41->addItem(horizontalSpacer_25);

        label_archive_sidebar_notes_toggle = new QLabel(widget_archive_sidebar_notes_header);
        label_archive_sidebar_notes_toggle->setObjectName(QStringLiteral("label_archive_sidebar_notes_toggle"));
        label_archive_sidebar_notes_toggle->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/stylesheet-branch-open.png")));

        horizontalLayout_41->addWidget(label_archive_sidebar_notes_toggle);


        verticalLayout_23->addWidget(widget_archive_sidebar_notes_header);

        widget_archive_sidebar_notes_shadow = new QWidget(scrollAreaWidgetContents_5);
        widget_archive_sidebar_notes_shadow->setObjectName(QStringLiteral("widget_archive_sidebar_notes_shadow"));
        widget_archive_sidebar_notes_shadow->setMinimumSize(QSize(0, 9));
        widget_archive_sidebar_notes_shadow->setMaximumSize(QSize(16777215, 9));

        verticalLayout_23->addWidget(widget_archive_sidebar_notes_shadow);

        widget_archive_sidebar_notes_content = new QWidget(scrollAreaWidgetContents_5);
        widget_archive_sidebar_notes_content->setObjectName(QStringLiteral("widget_archive_sidebar_notes_content"));
        verticalLayout_21 = new QVBoxLayout(widget_archive_sidebar_notes_content);
        verticalLayout_21->setSpacing(6);
        verticalLayout_21->setContentsMargins(11, 11, 11, 11);
        verticalLayout_21->setObjectName(QStringLiteral("verticalLayout_21"));
        verticalLayout_21->setContentsMargins(-1, 0, -1, -1);
        plainTextEdit_notes = new QPlainTextEdit(widget_archive_sidebar_notes_content);
        plainTextEdit_notes->setObjectName(QStringLiteral("plainTextEdit_notes"));
        plainTextEdit_notes->setMaximumSize(QSize(16777215, 16777215));
        plainTextEdit_notes->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);

        verticalLayout_21->addWidget(plainTextEdit_notes);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(6);
        horizontalLayout_11->setObjectName(QStringLiteral("horizontalLayout_11"));
        horizontalLayout_11->setContentsMargins(-1, 0, -1, -1);
        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_11->addItem(horizontalSpacer_8);

        pushButton_archiveUpdateNotes = new QPushButton(widget_archive_sidebar_notes_content);
        pushButton_archiveUpdateNotes->setObjectName(QStringLiteral("pushButton_archiveUpdateNotes"));
        pushButton_archiveUpdateNotes->setEnabled(false);
        pushButton_archiveUpdateNotes->setMaximumSize(QSize(50, 16777215));
        pushButton_archiveUpdateNotes->setSizeIncrement(QSize(0, 0));

        horizontalLayout_11->addWidget(pushButton_archiveUpdateNotes);


        verticalLayout_21->addLayout(horizontalLayout_11);


        verticalLayout_23->addWidget(widget_archive_sidebar_notes_content);

        verticalSpacer = new QSpacerItem(20, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_23->addItem(verticalSpacer);

        verticalLayout_23->setStretch(24, 1);
        scrollArea->setWidget(scrollAreaWidgetContents_5);

        vlayout_archive_right->addWidget(scrollArea);


        horizontalLayout_4->addLayout(vlayout_archive_right);

        horizontalLayout_4->setStretch(1, 1);
        tabWidget->addTab(tab_archives, QString());

        main_frame_vlayout_top->addWidget(tabWidget);


        horizontalLayout_2->addLayout(main_frame_vlayout_top);

        mainStack->addWidget(main_frame);
        cleanstart_frame = new QWidget();
        cleanstart_frame->setObjectName(QStringLiteral("cleanstart_frame"));
        verticalLayout_2 = new QVBoxLayout(cleanstart_frame);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(10, 10, 10, 0);
        label_cleanStart = new QLabel(cleanstart_frame);
        label_cleanStart->setObjectName(QStringLiteral("label_cleanStart"));
        QFont font8;
        font8.setPointSize(13);
        label_cleanStart->setFont(font8);
        label_cleanStart->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_cleanStart);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setContentsMargins(-1, 0, -1, -1);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        checkBox_autoLoad = new QCheckBox(cleanstart_frame);
        checkBox_autoLoad->setObjectName(QStringLiteral("checkBox_autoLoad"));

        horizontalLayout->addWidget(checkBox_autoLoad);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton_loadFromList = new QPushButton(cleanstart_frame);
        pushButton_loadFromList->setObjectName(QStringLiteral("pushButton_loadFromList"));
        pushButton_loadFromList->setMinimumSize(QSize(120, 25));

        horizontalLayout->addWidget(pushButton_loadFromList);

        pushButton_cleanStart_loadLib = new QPushButton(cleanstart_frame);
        pushButton_cleanStart_loadLib->setObjectName(QStringLiteral("pushButton_cleanStart_loadLib"));
        pushButton_cleanStart_loadLib->setMinimumSize(QSize(120, 25));

        horizontalLayout->addWidget(pushButton_cleanStart_loadLib);

        pushButton_cleanStart_createLib = new QPushButton(cleanstart_frame);
        pushButton_cleanStart_createLib->setObjectName(QStringLiteral("pushButton_cleanStart_createLib"));
        pushButton_cleanStart_createLib->setMinimumSize(QSize(120, 25));

        horizontalLayout->addWidget(pushButton_cleanStart_createLib);


        gridLayout_2->addLayout(horizontalLayout, 2, 1, 1, 1);

        horizontalSpacer_18 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_18, 1, 2, 1, 1);

        listWidget_recentFiles = new QListWidget(cleanstart_frame);
        listWidget_recentFiles->setObjectName(QStringLiteral("listWidget_recentFiles"));

        gridLayout_2->addWidget(listWidget_recentFiles, 1, 1, 1, 1);

        horizontalSpacer_17 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_17, 1, 0, 1, 1);

        label_recentFiles = new QLabel(cleanstart_frame);
        label_recentFiles->setObjectName(QStringLiteral("label_recentFiles"));

        gridLayout_2->addWidget(label_recentFiles, 0, 1, 1, 1);


        verticalLayout_2->addLayout(gridLayout_2);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_2);

        mainStack->addWidget(cleanstart_frame);

        verticalLayout->addWidget(mainStack);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1130, 20));
        menuTools = new QMenu(menuBar);
        menuTools->setObjectName(QStringLiteral("menuTools"));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuProcess = new QMenu(menuBar);
        menuProcess->setObjectName(QStringLiteral("menuProcess"));
        menuArchive = new QMenu(menuBar);
        menuArchive->setObjectName(QStringLiteral("menuArchive"));
        menuView = new QMenu(menuBar);
        menuView->setObjectName(QStringLiteral("menuView"));
        menuSites = new QMenu(menuBar);
        menuSites->setObjectName(QStringLiteral("menuSites"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        QSizePolicy sizePolicy7(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy7.setHorizontalStretch(0);
        sizePolicy7.setVerticalStretch(0);
        sizePolicy7.setHeightForWidth(mainToolBar->sizePolicy().hasHeightForWidth());
        mainToolBar->setSizePolicy(sizePolicy7);
        mainToolBar->setMovable(false);
        mainToolBar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);
        toolBarTabSwitcher = new QToolBar(MainWindow);
        toolBarTabSwitcher->setObjectName(QStringLiteral("toolBarTabSwitcher"));
        toolBarTabSwitcher->setEnabled(true);
        sizePolicy7.setHeightForWidth(toolBarTabSwitcher->sizePolicy().hasHeightForWidth());
        toolBarTabSwitcher->setSizePolicy(sizePolicy7);
        toolBarTabSwitcher->setMinimumSize(QSize(0, 0));
        QFont font9;
        font9.setBold(false);
        font9.setItalic(false);
        font9.setUnderline(false);
        font9.setWeight(50);
        toolBarTabSwitcher->setFont(font9);
        toolBarTabSwitcher->setLayoutDirection(Qt::RightToLeft);
        toolBarTabSwitcher->setMovable(false);
        toolBarTabSwitcher->setFloatable(false);
        MainWindow->addToolBar(Qt::TopToolBarArea, toolBarTabSwitcher);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuView->menuAction());
        menuBar->addAction(menuProcess->menuAction());
        menuBar->addAction(menuArchive->menuAction());
        menuBar->addAction(menuSites->menuAction());
        menuBar->addAction(menuTools->menuAction());
        menuTools->addAction(actionOptions);
        menuFile->addAction(actionNew);
        menuFile->addAction(actionOpen);
        menuFile->addAction(actionClose);
        menuFile->addAction(actionPreferences);
        menuFile->addAction(actionExit);
        menuProcess->addAction(actionAdd_newProcess);
        menuProcess->addAction(actionAutomatic_Tonemap);
        menuArchive->addAction(actionAdd_newArchive);
        menuArchive->addAction(actionNotes);
        menuArchive->addAction(actionTable_View);
        menuArchive->addAction(actionFolder_Sizes);
        menuArchive->addAction(actionKeywords);
        menuView->addAction(actionShow_toolbar_text);
        menuSites->addAction(actionSite_updater);
        menuSites->addAction(actionSites);
        menuSites->addAction(actionScore);
        menuSites->addAction(actionMonthly_uploads);
        menuSites->addAction(actionSync_with_Sentinel);
        menuSites->addAction(actionUpload_Times);
        menuSites->addAction(actionFollowers_Pageviews);
        menuSites->addAction(actionInstagram_Tags);
        mainToolBar->addAction(actionAdd_new_Process_toolBar);
        mainToolBar->addAction(actionMove_to_Archive_toolBar);
        mainToolBar->addAction(actionAdd_new_Archive_toolBar);
        mainToolBar->addAction(actionOptions);
        toolBarTabSwitcher->addAction(actionArchive);
        toolBarTabSwitcher->addAction(actionBrowse);
        toolBarTabSwitcher->addAction(actionProcess);

        retranslateUi(MainWindow);

        mainStack->setCurrentIndex(0);
        tabWidget->setCurrentIndex(2);
        stackedWidget_processLeft->setCurrentIndex(0);
        tabWidget_archiveTrees->setCurrentIndex(0);
        tabWidget_archiveCenter->setCurrentIndex(1);
        widget_archiveFolderNavi->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        actionOptions->setText(QApplication::translate("MainWindow", "Options...", 0));
        actionNew->setText(QApplication::translate("MainWindow", "New", 0));
        actionOpen->setText(QApplication::translate("MainWindow", "Open", 0));
        actionClose->setText(QApplication::translate("MainWindow", "Close", 0));
        actionExit->setText(QApplication::translate("MainWindow", "exit", 0));
        actionAdd_newProcess->setText(QApplication::translate("MainWindow", "Add new", 0));
        actionAdd_newArchive->setText(QApplication::translate("MainWindow", "Add new archive", 0));
        actionProcess->setText(QApplication::translate("MainWindow", "Process", 0));
        actionBrowse->setText(QApplication::translate("MainWindow", "Browse", 0));
        actionArchive->setText(QApplication::translate("MainWindow", "Archive", 0));
        actionNotes->setText(QApplication::translate("MainWindow", "Notes", 0));
        actionTable_View->setText(QApplication::translate("MainWindow", "Table View", 0));
        actionFolder_Sizes->setText(QApplication::translate("MainWindow", "Folder Sizes", 0));
        actionAdd_new_Archive_toolBar->setText(QApplication::translate("MainWindow", "New Archive", 0));
#ifndef QT_NO_TOOLTIP
        actionAdd_new_Archive_toolBar->setToolTip(QApplication::translate("MainWindow", "New Archive", 0));
#endif // QT_NO_TOOLTIP
        actionAdd_new_Process_toolBar->setText(QApplication::translate("MainWindow", "New Process", 0));
        actionMove_to_Archive_toolBar->setText(QApplication::translate("MainWindow", "Move to Archive", 0));
        actionShow_toolbar_text->setText(QApplication::translate("MainWindow", "Show toolbar text", 0));
        actionSites->setText(QApplication::translate("MainWindow", "Sites", 0));
        actionScore->setText(QApplication::translate("MainWindow", "Score", 0));
        actionSite_updater->setText(QApplication::translate("MainWindow", "Site updater", 0));
        actionPreferences->setText(QApplication::translate("MainWindow", "Preferences", 0));
        actionMonthly_uploads->setText(QApplication::translate("MainWindow", "Monthly uploads", 0));
        actionKeywords->setText(QApplication::translate("MainWindow", "Keywords", 0));
        actionAutomatic_Tonemap->setText(QApplication::translate("MainWindow", "Automatic Tonemap", 0));
        actionSync_with_Sentinel->setText(QApplication::translate("MainWindow", "Sync with Sentinel", 0));
        actionUpload_Times->setText(QApplication::translate("MainWindow", "Upload Times", 0));
        actionFollowers_Pageviews->setText(QApplication::translate("MainWindow", "Followers & Pageviews", 0));
        actionInstagram_Tags->setText(QApplication::translate("MainWindow", "Instagram Tags", 0));
        label_14->setText(QApplication::translate("MainWindow", "Projects", 0));
        pushButton_aniMate->setText(QString());
        label_processTitle->setText(QApplication::translate("MainWindow", "T", 0));
        label_processPath->setText(QApplication::translate("MainWindow", "P", 0));
        pushButton_processPathToClipboard->setText(QString());
        label_processPano->setText(QApplication::translate("MainWindow", "P", 0));
        pushButton_processProperties->setText(QApplication::translate("MainWindow", "Properties        ", 0));
        pushButton_processOpenFolder->setText(QApplication::translate("MainWindow", "Open Folder    ", 0));
        pushButton_processToArchive->setText(QApplication::translate("MainWindow", "Archive             ", 0));
        pushButton_processDelete->setText(QApplication::translate("MainWindow", "Delete              ", 0));
        pushButton_processFileGroupOrigall->setText(QApplication::translate("MainWindow", "Original Files", 0));
        pushButton_processFileGroupEV0->setText(QApplication::translate("MainWindow", "Originals EV0", 0));
        pushButton_processFileGroupEV1->setText(QApplication::translate("MainWindow", "Originals EV1", 0));
        pushButton_processFileGroupEV2->setText(QApplication::translate("MainWindow", "Originals EV2", 0));
        pushButton_processFileGroupPanorama->setText(QApplication::translate("MainWindow", "Panorama", 0));
        pushButton_processFileGroupHDR->setText(QApplication::translate("MainWindow", "HDR", 0));
        pushButton_processFileGroupPostProcess->setText(QApplication::translate("MainWindow", "Postprocess", 0));
        pushButton_processHDR->setText(QApplication::translate("MainWindow", "HDR / TM       ", 0));
        pushButton_processStich->setText(QApplication::translate("MainWindow", "Stich               ", 0));
        pushButton_processBatchStich->setText(QApplication::translate("MainWindow", "Batch Sticher    ", 0));
        pushButton_processClonePTS->setText(QApplication::translate("MainWindow", "Clone EVs         ", 0));
        pushButton_processAlltoPS->setText(QApplication::translate("MainWindow", "Load all to PS   ", 0));
        label_processFileName->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        label_process_fileinfoPreview->setText(QString());
        pushButton_processFileOpen->setText(QApplication::translate("MainWindow", "   Open          ", 0));
        pushButton_processFileEdit->setText(QApplication::translate("MainWindow", "   Edit             ", 0));
        label_process_fileinfoType->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        label_15->setText(QApplication::translate("MainWindow", "Type:", 0));
        label_process_fileinfoCreated->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        label_17->setText(QApplication::translate("MainWindow", "Created:", 0));
        label_process_fileinfoSize->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        label_16->setText(QApplication::translate("MainWindow", "Size:", 0));
        label_process_fileinfoResolutionLabel->setText(QApplication::translate("MainWindow", "Resolution:", 0));
        label_process_fileinfoResolution->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_process), QApplication::translate("MainWindow", "Tab 1", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_processBrowser), QApplication::translate("MainWindow", "Tab 2", 0));
        pushButton_archiveTreeExpand->setText(QString());
        pushButton_archiveTreeGroup->setText(QApplication::translate("MainWindow", "()", 0));
        pushButton_archiveGroupByAz->setText(QApplication::translate("MainWindow", "A-Z", 0));
        pushButton_archiveGroupByDate->setText(QApplication::translate("MainWindow", "DATE", 0));
        pushButton_archiveGroupByLoc->setText(QApplication::translate("MainWindow", "LOC", 0));
        pushButton_archiveGroupByScore->setText(QString());
        tabWidget_archiveTrees->setTabText(tabWidget_archiveTrees->indexOf(tabArchiveProjects), QApplication::translate("MainWindow", "Projects", 0));
        pushButton_addCollection->setText(QApplication::translate("MainWindow", "+", 0));
        tabWidget_archiveTrees->setTabText(tabWidget_archiveTrees->indexOf(tab_16), QApplication::translate("MainWindow", "Collections", 0));
        pushButton_archiveSearchByTitle->setText(QApplication::translate("MainWindow", "T", 0));
        pushButton_archiveSearchByKeywords->setText(QApplication::translate("MainWindow", "K", 0));
        pushButton_filterFields->setText(QString());
#ifndef QT_NO_TOOLTIP
        pushButton_archiveFilterClear->setToolTip(QString());
#endif // QT_NO_TOOLTIP
        pushButton_archiveFilterClear->setText(QApplication::translate("MainWindow", "X", 0));
        label_archiveTitle->setText(QApplication::translate("MainWindow", "A", 0));
        label_archiveDate->setText(QApplication::translate("MainWindow", "B", 0));
        label_previewImage->setText(QApplication::translate("MainWindow", "C", 0));
        pushButton_naviPrev->setText(QString());
        pushButton_naviNext->setText(QString());
        pushButton_fileListToRoot->setText(QApplication::translate("MainWindow", "ROOT", 0));
        pushButton_imgOnly->setText(QApplication::translate("MainWindow", "IMG", 0));
        pushButton_archiveOpenFolder->setText(QApplication::translate("MainWindow", "Open Folder", 0));
        pushButton_archiveOrganizeFolder->setText(QApplication::translate("MainWindow", "Organize", 0));
        pushButton_archiveFlattenFolder->setText(QApplication::translate("MainWindow", "Flatten", 0));
        pushButton_archiveCompressor->setText(QApplication::translate("MainWindow", "Compress", 0));
        pushButton_toggleFolderNavi2->setText(QString());
        label_2->setText(QString());
        label_dirWarning->setText(QApplication::translate("MainWindow", "WARNING: folder not found!", 0));
        pushButton_toggleFolderNavi->setText(QString());
        tabWidget_archiveCenter->setTabText(tabWidget_archiveCenter->indexOf(tab_archivePreview), QApplication::translate("MainWindow", "Preview", 0));
        label_11->setText(QApplication::translate("MainWindow", "Filter:", 0));
        checkBox_aF_notin->setText(QString());
        radioButton_FNUP->setText(QApplication::translate("MainWindow", "Not Uploaded", 0));
        radioButton_FUP->setText(QApplication::translate("MainWindow", "Uploaded", 0));
        label_aF_niDA->setText(QString());
        label_aF_niRD->setText(QString());
        label_aF_ni5P->setText(QString());
        label_aF_niGP->setText(QString());
        label_aF_niFL->setText(QString());
        label_aF_niFB->setText(QString());
        label_aF_niPX->setText(QString());
        label_aF_niYP->setText(QString());
        label_aF_niVB->setText(QString());
        label_aF_niIG->setText(QString());
        label_archiveThumbSize->setText(QString());
        tabWidget_archiveCenter->setTabText(tabWidget_archiveCenter->indexOf(tab_archiveThumbnails), QApplication::translate("MainWindow", "Thumbnails", 0));
        pushButton_naviPrev_2->setText(QString());
        pushButton_naviNext_2->setText(QString());
        progressBar_web->setFormat(QApplication::translate("MainWindow", "%p%", 0));
        tabWidget_archiveCenter->setTabText(tabWidget_archiveCenter->indexOf(tab_archiveWeb), QApplication::translate("MainWindow", "Web", 0));
        pushButton_mapRefresh->setText(QApplication::translate("MainWindow", "Refresh", 0));
        tabWidget_archiveCenter->setTabText(tabWidget_archiveCenter->indexOf(tab_14), QApplication::translate("MainWindow", "Map", 0));
        label_28->setText(QString());
        label_23->setText(QString());
        label_archive_sidebar_preview->setText(QApplication::translate("MainWindow", "Preview", 0));
        label_archive_sidebar_preview_toggle->setText(QString());
        label_sideBarPreview->setText(QString());
        label_29->setText(QString());
        label_22->setText(QString());
        label_archive_sidebar_info->setText(QApplication::translate("MainWindow", "Info", 0));
        label_archive_sidebar_info_toggle->setText(QString());
        label_archiveInfoTitle->setText(QString());
        label_archiveInfoID->setText(QString());
        label_5->setText(QApplication::translate("MainWindow", "ID", 0));
        label_3->setText(QApplication::translate("MainWindow", "Title", 0));
        label_7->setText(QApplication::translate("MainWindow", "Date", 0));
        label_archiveInfoDate->setText(QString());
        label_6->setText(QApplication::translate("MainWindow", "Location", 0));
        label_archiveInfoLocation->setText(QString());
        label_fileSize->setText(QString());
        label_36->setText(QApplication::translate("MainWindow", "Title 2", 0));
        label_archiveInfoTitle_2->setText(QString());
        pushButton_archiveEditData->setText(QApplication::translate("MainWindow", "Modify...", 0));
        label_salesCount->setText(QApplication::translate("MainWindow", "0", 0));
        label_salesTotal->setText(QApplication::translate("MainWindow", "0.0", 0));
        pushButton_addSale->setText(QApplication::translate("MainWindow", "Add Sale", 0));
        label_37->setText(QString());
        label_38->setText(QString());
        label_archive_sidebar_tools->setText(QApplication::translate("MainWindow", "Tools", 0));
        label_archive_sidebar_tools_toggle->setText(QString());
        pushButton_archiveCompressor_1->setText(QApplication::translate("MainWindow", "Compress", 0));
        pushButton_archiveCopyKeyComma_1->setText(QApplication::translate("MainWindow", "Keywords [ , ]", 0));
        pushButton_archiveEditData_1->setText(QApplication::translate("MainWindow", "Properties", 0));
        pushButton_sitesManage_1->setText(QApplication::translate("MainWindow", "Manage Sites", 0));
        pushButton_archiveCopyKeySpace_1->setText(QApplication::translate("MainWindow", "Keywords [   ]", 0));
        pushButton_addNewSite_1->setText(QApplication::translate("MainWindow", "Add Site", 0));
        pushButton_archiveOrganizeFolder_1->setText(QApplication::translate("MainWindow", "Organize", 0));
        pushButton_archiveOpenFolder_1->setText(QApplication::translate("MainWindow", "Open Folder", 0));
        pushButton_archiveCopyTitle->setText(QApplication::translate("MainWindow", "Title [   ]", 0));
        pushButton_archiveCopyTitle_2->setText(QApplication::translate("MainWindow", "Show on map", 0));
        pushButton_upload->setText(QApplication::translate("MainWindow", "Upload", 0));
        label_30->setText(QString());
        label->setText(QString());
        label_archive_sidebar_map->setText(QApplication::translate("MainWindow", "Map", 0));
        label_archive_sidebar_map_toggle->setText(QString());
        label_13->setText(QApplication::translate("MainWindow", "LAT", 0));
        label_12->setText(QApplication::translate("MainWindow", "LNG", 0));
        pushButton_locSave->setText(QApplication::translate("MainWindow", "Save", 0));
        label_31->setText(QString());
        label_24->setText(QString());
        label_archive_sidebar_score->setText(QApplication::translate("MainWindow", "Score", 0));
        label_archive_sidebar_score_toggle->setText(QString());
        label_score->setText(QApplication::translate("MainWindow", "0.0", 0));
        label_scoreStar1->setText(QString());
        label_scoreStar2->setText(QString());
        label_scoreStar3->setText(QString());
        label_scoreStar4->setText(QString());
        label_scoreStar5->setText(QString());
        label_32->setText(QString());
        label_27->setText(QString());
        label_archive_sidebar_sites->setText(QApplication::translate("MainWindow", "Sites", 0));
        label_archive_sidebar_sites_toggle->setText(QString());
        label_siteFlickrViews->setText(QString());
        label_siteDaIcon->setText(QString());
        label_siteFBFavs->setText(QString());
        label_siteFBViews->setText(QString());
        label_siteGPIcon->setText(QString());
        label_site500pxIcon->setText(QString());
        label_siteFlickrFavs->setText(QString());
        label_siteGPFavs->setText(QString());
        label_9->setText(QString());
        label_10->setText(QString());
        label_sitePixotoIcon->setText(QString());
#ifndef QT_NO_TOOLTIP
        label_sitePixotoViews->setToolTip(QApplication::translate("MainWindow", "views", 0));
#endif // QT_NO_TOOLTIP
        label_sitePixotoViews->setText(QString());
#ifndef QT_NO_TOOLTIP
        label_sitePixotoFavs->setToolTip(QApplication::translate("MainWindow", "favs", 0));
#endif // QT_NO_TOOLTIP
        label_sitePixotoFavs->setText(QString());
#ifndef QT_NO_TOOLTIP
        label_siteYoupicViews->setToolTip(QApplication::translate("MainWindow", "views", 0));
#endif // QT_NO_TOOLTIP
        label_siteYoupicViews->setText(QString());
#ifndef QT_NO_TOOLTIP
        label_siteYoupicFavs->setToolTip(QApplication::translate("MainWindow", "favs", 0));
#endif // QT_NO_TOOLTIP
        label_siteYoupicFavs->setText(QString());
        label_siteYoupicIcon->setText(QString());
        label_18->setText(QString());
        label_siteFlickrIcon->setText(QString());
        label_siteFBIcon->setText(QString());
        label_site500pxFavs->setText(QString());
        label_siteGPViews->setText(QString());
#ifndef QT_NO_TOOLTIP
        label_siteDaFavs->setToolTip(QApplication::translate("MainWindow", "favs", 0));
#endif // QT_NO_TOOLTIP
        label_siteDaFavs->setText(QString());
#ifndef QT_NO_TOOLTIP
        label_siteDaViews->setToolTip(QApplication::translate("MainWindow", "views", 0));
#endif // QT_NO_TOOLTIP
        label_siteDaViews->setText(QString());
        label_site500pxViews->setText(QString());
        label_19->setText(QString());
        label_siteViewbugIcon->setText(QString());
#ifndef QT_NO_TOOLTIP
        label_siteViewbugViews->setToolTip(QApplication::translate("MainWindow", "views", 0));
#endif // QT_NO_TOOLTIP
        label_siteViewbugViews->setText(QApplication::translate("MainWindow", "-", 0));
#ifndef QT_NO_TOOLTIP
        label_siteViewbugFavs->setToolTip(QApplication::translate("MainWindow", "favs", 0));
#endif // QT_NO_TOOLTIP
        label_siteViewbugFavs->setText(QApplication::translate("MainWindow", "-", 0));
        label_siteRdIcon->setText(QString());
#ifndef QT_NO_TOOLTIP
        label_siteRdViews->setToolTip(QApplication::translate("MainWindow", "views", 0));
#endif // QT_NO_TOOLTIP
        label_siteRdViews->setText(QApplication::translate("MainWindow", "-", 0));
#ifndef QT_NO_TOOLTIP
        label_siteRdFavs->setToolTip(QApplication::translate("MainWindow", "favs", 0));
#endif // QT_NO_TOOLTIP
        label_siteRdFavs->setText(QApplication::translate("MainWindow", "-", 0));
        label_siteIgIcon->setText(QString());
#ifndef QT_NO_TOOLTIP
        label_siteIgViews->setToolTip(QApplication::translate("MainWindow", "views", 0));
#endif // QT_NO_TOOLTIP
        label_siteIgViews->setText(QApplication::translate("MainWindow", "-", 0));
#ifndef QT_NO_TOOLTIP
        label_siteIgFavs->setToolTip(QApplication::translate("MainWindow", "favs", 0));
#endif // QT_NO_TOOLTIP
        label_siteIgFavs->setText(QApplication::translate("MainWindow", "-", 0));
        label_siteTotalFavs->setText(QApplication::translate("MainWindow", "0", 0));
        label_siteTotalViews->setText(QApplication::translate("MainWindow", "0", 0));
        label_20->setText(QString());
        label_8->setText(QApplication::translate("MainWindow", "Total Views:", 0));
        label_21->setText(QString());
        label_4->setText(QApplication::translate("MainWindow", "Total Favs:", 0));
        label_35->setText(QApplication::translate("MainWindow", "Up to Date:", 0));
        label_archive_uptoDate->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        pushButton_sitesManage->setText(QApplication::translate("MainWindow", "Manage", 0));
        pushButton_sitesUpdate->setText(QApplication::translate("MainWindow", "Update", 0));
        pushButton_addNewSite->setText(QApplication::translate("MainWindow", "Add new", 0));
        label_33->setText(QString());
        label_25->setText(QString());
        label_archive_sidebar_keywords->setText(QApplication::translate("MainWindow", "Keywords", 0));
        label_archive_sidebar_keywords_toggle->setText(QString());
        pushButton_quickTag->setText(QApplication::translate("MainWindow", "Q", 0));
        pushButton_archiveCopyKeyComma->setText(QApplication::translate("MainWindow", "[ , ]", 0));
        pushButton_archiveCopyKeySpace->setText(QApplication::translate("MainWindow", "[   ]", 0));
        pushButton_archiveUpdateKeywords->setText(QApplication::translate("MainWindow", "Update", 0));
        label_34->setText(QString());
        label_26->setText(QString());
        label_archive_sidebar_notes->setText(QApplication::translate("MainWindow", "Notes", 0));
        label_archive_sidebar_notes_toggle->setText(QString());
        pushButton_archiveUpdateNotes->setText(QApplication::translate("MainWindow", "Update", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_archives), QApplication::translate("MainWindow", "Page", 0));
        label_cleanStart->setText(QApplication::translate("MainWindow", "No library loaded!", 0));
        checkBox_autoLoad->setText(QApplication::translate("MainWindow", "Always load this library on startup", 0));
        pushButton_loadFromList->setText(QApplication::translate("MainWindow", "Load", 0));
        pushButton_cleanStart_loadLib->setText(QApplication::translate("MainWindow", "Open existing library", 0));
        pushButton_cleanStart_createLib->setText(QApplication::translate("MainWindow", "Create new library", 0));
        label_recentFiles->setText(QApplication::translate("MainWindow", "Recent files:", 0));
        menuTools->setTitle(QApplication::translate("MainWindow", "Tools", 0));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", 0));
        menuProcess->setTitle(QApplication::translate("MainWindow", "Process", 0));
        menuArchive->setTitle(QApplication::translate("MainWindow", "Archive", 0));
        menuView->setTitle(QApplication::translate("MainWindow", "View", 0));
        menuSites->setTitle(QApplication::translate("MainWindow", "Sites", 0));
        toolBarTabSwitcher->setWindowTitle(QApplication::translate("MainWindow", "toolBar", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
