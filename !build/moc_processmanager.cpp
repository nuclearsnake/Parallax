/****************************************************************************
** Meta object code from reading C++ file 'processmanager.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../processmanager.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'processmanager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_processManager_t {
    QByteArrayData data[31];
    char stringdata[389];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_processManager_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_processManager_t qt_meta_stringdata_processManager = {
    {
QT_MOC_LITERAL(0, 0, 14), // "processManager"
QT_MOC_LITERAL(1, 15, 10), // "openWithPS"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 3), // "arg"
QT_MOC_LITERAL(4, 31, 18), // "openWithPhotoMatix"
QT_MOC_LITERAL(5, 50, 13), // "openWithPTGui"
QT_MOC_LITERAL(6, 64, 17), // "fileGroupSelector"
QT_MOC_LITERAL(7, 82, 6), // "select"
QT_MOC_LITERAL(8, 89, 5), // "index"
QT_MOC_LITERAL(9, 95, 13), // "dialog_addNew"
QT_MOC_LITERAL(10, 109, 15), // "dialog_clonePts"
QT_MOC_LITERAL(11, 125, 13), // "deleteProject"
QT_MOC_LITERAL(12, 139, 20), // "dialog_moveToArchive"
QT_MOC_LITERAL(13, 160, 16), // "thumbContextMenu"
QT_MOC_LITERAL(14, 177, 3), // "pos"
QT_MOC_LITERAL(15, 181, 15), // "treeContextMenu"
QT_MOC_LITERAL(16, 197, 14), // "animateSideBar"
QT_MOC_LITERAL(17, 212, 8), // "listBack"
QT_MOC_LITERAL(18, 221, 16), // "treeItemDBLClick"
QT_MOC_LITERAL(19, 238, 6), // "listTo"
QT_MOC_LITERAL(20, 245, 15), // "pathToClipboard"
QT_MOC_LITERAL(21, 261, 10), // "folderOpen"
QT_MOC_LITERAL(22, 272, 17), // "dialog_properties"
QT_MOC_LITERAL(23, 290, 10), // "filesToHDR"
QT_MOC_LITERAL(24, 301, 9), // "filesToPS"
QT_MOC_LITERAL(25, 311, 12), // "filesToPTGui"
QT_MOC_LITERAL(26, 324, 17), // "filesToPTGuiBatch"
QT_MOC_LITERAL(27, 342, 12), // "loadFileInfo"
QT_MOC_LITERAL(28, 355, 8), // "fileOpen"
QT_MOC_LITERAL(29, 364, 8), // "fileEdit"
QT_MOC_LITERAL(30, 373, 15) // "fileDoubleClick"

    },
    "processManager\0openWithPS\0\0arg\0"
    "openWithPhotoMatix\0openWithPTGui\0"
    "fileGroupSelector\0select\0index\0"
    "dialog_addNew\0dialog_clonePts\0"
    "deleteProject\0dialog_moveToArchive\0"
    "thumbContextMenu\0pos\0treeContextMenu\0"
    "animateSideBar\0listBack\0treeItemDBLClick\0"
    "listTo\0pathToClipboard\0folderOpen\0"
    "dialog_properties\0filesToHDR\0filesToPS\0"
    "filesToPTGui\0filesToPTGuiBatch\0"
    "loadFileInfo\0fileOpen\0fileEdit\0"
    "fileDoubleClick"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_processManager[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      26,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  144,    2, 0x06 /* Public */,
       4,    1,  147,    2, 0x06 /* Public */,
       5,    1,  150,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    0,  153,    2, 0x0a /* Public */,
       7,    1,  154,    2, 0x0a /* Public */,
       9,    0,  157,    2, 0x0a /* Public */,
      10,    0,  158,    2, 0x0a /* Public */,
      11,    0,  159,    2, 0x0a /* Public */,
      12,    0,  160,    2, 0x0a /* Public */,
      13,    1,  161,    2, 0x0a /* Public */,
      15,    1,  164,    2, 0x0a /* Public */,
      16,    0,  167,    2, 0x0a /* Public */,
      17,    0,  168,    2, 0x0a /* Public */,
      18,    1,  169,    2, 0x0a /* Public */,
      19,    0,  172,    2, 0x0a /* Public */,
      20,    0,  173,    2, 0x0a /* Public */,
      21,    0,  174,    2, 0x0a /* Public */,
      22,    0,  175,    2, 0x0a /* Public */,
      23,    0,  176,    2, 0x0a /* Public */,
      24,    0,  177,    2, 0x0a /* Public */,
      25,    0,  178,    2, 0x0a /* Public */,
      26,    0,  179,    2, 0x0a /* Public */,
      27,    1,  180,    2, 0x0a /* Public */,
      28,    0,  183,    2, 0x0a /* Public */,
      29,    0,  184,    2, 0x0a /* Public */,
      30,    1,  185,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QStringList,    3,
    QMetaType::Void, QMetaType::QStringList,    3,
    QMetaType::Void, QMetaType::QStringList,    3,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,    8,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QPoint,   14,
    QMetaType::Void, QMetaType::QPoint,   14,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,    8,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,    8,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,    8,

       0        // eod
};

void processManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        processManager *_t = static_cast<processManager *>(_o);
        switch (_id) {
        case 0: _t->openWithPS((*reinterpret_cast< QStringList(*)>(_a[1]))); break;
        case 1: _t->openWithPhotoMatix((*reinterpret_cast< QStringList(*)>(_a[1]))); break;
        case 2: _t->openWithPTGui((*reinterpret_cast< QStringList(*)>(_a[1]))); break;
        case 3: _t->fileGroupSelector(); break;
        case 4: _t->select((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 5: _t->dialog_addNew(); break;
        case 6: _t->dialog_clonePts(); break;
        case 7: _t->deleteProject(); break;
        case 8: _t->dialog_moveToArchive(); break;
        case 9: _t->thumbContextMenu((*reinterpret_cast< const QPoint(*)>(_a[1]))); break;
        case 10: _t->treeContextMenu((*reinterpret_cast< const QPoint(*)>(_a[1]))); break;
        case 11: _t->animateSideBar(); break;
        case 12: _t->listBack(); break;
        case 13: _t->treeItemDBLClick((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 14: _t->listTo(); break;
        case 15: _t->pathToClipboard(); break;
        case 16: _t->folderOpen(); break;
        case 17: _t->dialog_properties(); break;
        case 18: _t->filesToHDR(); break;
        case 19: _t->filesToPS(); break;
        case 20: _t->filesToPTGui(); break;
        case 21: _t->filesToPTGuiBatch(); break;
        case 22: _t->loadFileInfo((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 23: _t->fileOpen(); break;
        case 24: _t->fileEdit(); break;
        case 25: _t->fileDoubleClick((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (processManager::*_t)(QStringList );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&processManager::openWithPS)) {
                *result = 0;
            }
        }
        {
            typedef void (processManager::*_t)(QStringList );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&processManager::openWithPhotoMatix)) {
                *result = 1;
            }
        }
        {
            typedef void (processManager::*_t)(QStringList );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&processManager::openWithPTGui)) {
                *result = 2;
            }
        }
    }
}

const QMetaObject processManager::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_processManager.data,
      qt_meta_data_processManager,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *processManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *processManager::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_processManager.stringdata))
        return static_cast<void*>(const_cast< processManager*>(this));
    return QObject::qt_metacast(_clname);
}

int processManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 26)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 26;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 26)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 26;
    }
    return _id;
}

// SIGNAL 0
void processManager::openWithPS(QStringList _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void processManager::openWithPhotoMatix(QStringList _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void processManager::openWithPTGui(QStringList _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
