/********************************************************************************
** Form generated from reading UI file 'siteupdaternew.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SITEUPDATERNEW_H
#define UI_SITEUPDATERNEW_H

#include <QtCore/QVariant>
#include <QtWebKitWidgets/QWebView>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_siteUpdaterNew
{
public:
    QVBoxLayout *verticalLayout;
    QWebView *webView;
    QPlainTextEdit *plainTextEdit_2;
    QHBoxLayout *horizontalLayout_3;
    QRadioButton *radioButton_uploaded;
    QComboBox *comboBox_uploaded;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout_4;
    QRadioButton *radioButton_updated;
    QComboBox *comboBox_updated;
    QSpacerItem *horizontalSpacer_3;
    QHBoxLayout *horizontalLayout_5;
    QRadioButton *radioButton_site;
    QComboBox *comboBox_site;
    QSpacerItem *horizontalSpacer_4;
    QRadioButton *radioButton_single;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLabel *label_progress;
    QProgressBar *progressBar;
    QLabel *label_remTime;
    QTableView *tableView;
    QPlainTextEdit *plainTextEdit;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_load;
    QPushButton *pushButton_go;
    QPushButton *pushButton_close;

    void setupUi(QDialog *siteUpdaterNew)
    {
        if (siteUpdaterNew->objectName().isEmpty())
            siteUpdaterNew->setObjectName(QStringLiteral("siteUpdaterNew"));
        siteUpdaterNew->resize(1270, 537);
        verticalLayout = new QVBoxLayout(siteUpdaterNew);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        webView = new QWebView(siteUpdaterNew);
        webView->setObjectName(QStringLiteral("webView"));
        webView->setMinimumSize(QSize(0, 100));
        webView->setMaximumSize(QSize(16777215, 100));
        webView->setUrl(QUrl(QStringLiteral("about:blank")));

        verticalLayout->addWidget(webView);

        plainTextEdit_2 = new QPlainTextEdit(siteUpdaterNew);
        plainTextEdit_2->setObjectName(QStringLiteral("plainTextEdit_2"));
        QFont font;
        font.setFamily(QStringLiteral("Consolas"));
        plainTextEdit_2->setFont(font);

        verticalLayout->addWidget(plainTextEdit_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, -1, -1);
        radioButton_uploaded = new QRadioButton(siteUpdaterNew);
        radioButton_uploaded->setObjectName(QStringLiteral("radioButton_uploaded"));
        radioButton_uploaded->setMinimumSize(QSize(150, 0));
        radioButton_uploaded->setChecked(true);

        horizontalLayout_3->addWidget(radioButton_uploaded);

        comboBox_uploaded = new QComboBox(siteUpdaterNew);
        comboBox_uploaded->setObjectName(QStringLiteral("comboBox_uploaded"));
        comboBox_uploaded->setMinimumSize(QSize(100, 0));

        horizontalLayout_3->addWidget(comboBox_uploaded);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalLayout_4->setContentsMargins(-1, 0, -1, -1);
        radioButton_updated = new QRadioButton(siteUpdaterNew);
        radioButton_updated->setObjectName(QStringLiteral("radioButton_updated"));
        radioButton_updated->setMinimumSize(QSize(150, 0));

        horizontalLayout_4->addWidget(radioButton_updated);

        comboBox_updated = new QComboBox(siteUpdaterNew);
        comboBox_updated->setObjectName(QStringLiteral("comboBox_updated"));
        comboBox_updated->setMinimumSize(QSize(100, 0));

        horizontalLayout_4->addWidget(comboBox_updated);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_3);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(-1, 0, -1, -1);
        radioButton_site = new QRadioButton(siteUpdaterNew);
        radioButton_site->setObjectName(QStringLiteral("radioButton_site"));
        radioButton_site->setMinimumSize(QSize(150, 0));

        horizontalLayout_5->addWidget(radioButton_site);

        comboBox_site = new QComboBox(siteUpdaterNew);
        comboBox_site->setObjectName(QStringLiteral("comboBox_site"));
        comboBox_site->setMinimumSize(QSize(100, 0));

        horizontalLayout_5->addWidget(comboBox_site);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_4);


        verticalLayout->addLayout(horizontalLayout_5);

        radioButton_single = new QRadioButton(siteUpdaterNew);
        radioButton_single->setObjectName(QStringLiteral("radioButton_single"));

        verticalLayout->addWidget(radioButton_single);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label = new QLabel(siteUpdaterNew);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout->addWidget(label);

        label_progress = new QLabel(siteUpdaterNew);
        label_progress->setObjectName(QStringLiteral("label_progress"));

        horizontalLayout->addWidget(label_progress);

        progressBar = new QProgressBar(siteUpdaterNew);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setValue(0);

        horizontalLayout->addWidget(progressBar);


        verticalLayout->addLayout(horizontalLayout);

        label_remTime = new QLabel(siteUpdaterNew);
        label_remTime->setObjectName(QStringLiteral("label_remTime"));

        verticalLayout->addWidget(label_remTime);

        tableView = new QTableView(siteUpdaterNew);
        tableView->setObjectName(QStringLiteral("tableView"));
        tableView->setSelectionBehavior(QAbstractItemView::SelectRows);

        verticalLayout->addWidget(tableView);

        plainTextEdit = new QPlainTextEdit(siteUpdaterNew);
        plainTextEdit->setObjectName(QStringLiteral("plainTextEdit"));
        plainTextEdit->setFont(font);

        verticalLayout->addWidget(plainTextEdit);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        pushButton_load = new QPushButton(siteUpdaterNew);
        pushButton_load->setObjectName(QStringLiteral("pushButton_load"));
        pushButton_load->setMinimumSize(QSize(100, 25));

        horizontalLayout_2->addWidget(pushButton_load);

        pushButton_go = new QPushButton(siteUpdaterNew);
        pushButton_go->setObjectName(QStringLiteral("pushButton_go"));
        pushButton_go->setMinimumSize(QSize(100, 25));

        horizontalLayout_2->addWidget(pushButton_go);

        pushButton_close = new QPushButton(siteUpdaterNew);
        pushButton_close->setObjectName(QStringLiteral("pushButton_close"));
        pushButton_close->setMinimumSize(QSize(100, 25));

        horizontalLayout_2->addWidget(pushButton_close);


        verticalLayout->addLayout(horizontalLayout_2);


        retranslateUi(siteUpdaterNew);

        QMetaObject::connectSlotsByName(siteUpdaterNew);
    } // setupUi

    void retranslateUi(QDialog *siteUpdaterNew)
    {
        siteUpdaterNew->setWindowTitle(QApplication::translate("siteUpdaterNew", "Dialog", 0));
        plainTextEdit_2->setPlainText(QApplication::translate("siteUpdaterNew", "12345\n"
"asdoiqweqwe\303\251l\n"
"qwoecnaqweiqw\n"
"", 0));
        radioButton_uploaded->setText(QApplication::translate("siteUpdaterNew", "Uploaded in last", 0));
        comboBox_uploaded->clear();
        comboBox_uploaded->insertItems(0, QStringList()
         << QApplication::translate("siteUpdaterNew", "2 Days", 0)
         << QApplication::translate("siteUpdaterNew", "1 Week", 0)
         << QApplication::translate("siteUpdaterNew", "2 Weeks", 0)
         << QApplication::translate("siteUpdaterNew", "1 Month", 0)
         << QApplication::translate("siteUpdaterNew", "2 Months", 0)
        );
        radioButton_updated->setText(QApplication::translate("siteUpdaterNew", "Last update older than", 0));
        comboBox_updated->clear();
        comboBox_updated->insertItems(0, QStringList()
         << QApplication::translate("siteUpdaterNew", "1 Week", 0)
         << QApplication::translate("siteUpdaterNew", "1 Month", 0)
         << QApplication::translate("siteUpdaterNew", "2 Months", 0)
         << QApplication::translate("siteUpdaterNew", "3 Months", 0)
         << QApplication::translate("siteUpdaterNew", "1 Year", 0)
        );
        radioButton_site->setText(QApplication::translate("siteUpdaterNew", "All from site:", 0));
        comboBox_site->clear();
        comboBox_site->insertItems(0, QStringList()
         << QApplication::translate("siteUpdaterNew", "500px", 0)
         << QApplication::translate("siteUpdaterNew", "DeviantArt", 0)
         << QApplication::translate("siteUpdaterNew", "Facebook", 0)
         << QApplication::translate("siteUpdaterNew", "Flickr", 0)
         << QApplication::translate("siteUpdaterNew", "Google+", 0)
         << QApplication::translate("siteUpdaterNew", "Pixoto", 0)
         << QApplication::translate("siteUpdaterNew", "ViewBug", 0)
         << QApplication::translate("siteUpdaterNew", "Youpic", 0)
        );
        radioButton_single->setText(QApplication::translate("siteUpdaterNew", "Single", 0));
        label->setText(QApplication::translate("siteUpdaterNew", "Progress: ", 0));
        label_progress->setText(QString());
        label_remTime->setText(QString());
        pushButton_load->setText(QApplication::translate("siteUpdaterNew", "Load Sites", 0));
        pushButton_go->setText(QApplication::translate("siteUpdaterNew", "Go!", 0));
        pushButton_close->setText(QApplication::translate("siteUpdaterNew", "Close", 0));
    } // retranslateUi

};

namespace Ui {
    class siteUpdaterNew: public Ui_siteUpdaterNew {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SITEUPDATERNEW_H
