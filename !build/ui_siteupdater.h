/********************************************************************************
** Form generated from reading UI file 'siteupdater.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SITEUPDATER_H
#define UI_SITEUPDATER_H

#include <QtCore/QVariant>
#include <QtWebKitWidgets/QWebView>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_siteUpdater
{
public:
    QVBoxLayout *verticalLayout;
    QWebView *webView;
    QPlainTextEdit *plainTextEdit_2;
    QHBoxLayout *horizontalLayout_3;
    QRadioButton *radioButton_uploaded;
    QComboBox *comboBox_uploaded;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout_4;
    QRadioButton *radioButton_updated;
    QComboBox *comboBox_updated;
    QSpacerItem *horizontalSpacer_3;
    QRadioButton *radioButton_single;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLabel *label_2;
    QProgressBar *progressBar;
    QTableView *tableView;
    QPlainTextEdit *plainTextEdit;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_load;
    QPushButton *pushButton_go;
    QPushButton *pushButton_close;

    void setupUi(QDialog *siteUpdater)
    {
        if (siteUpdater->objectName().isEmpty())
            siteUpdater->setObjectName(QStringLiteral("siteUpdater"));
        siteUpdater->resize(1270, 537);
        verticalLayout = new QVBoxLayout(siteUpdater);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        webView = new QWebView(siteUpdater);
        webView->setObjectName(QStringLiteral("webView"));
        webView->setMinimumSize(QSize(0, 100));
        webView->setMaximumSize(QSize(16777215, 100));
        webView->setUrl(QUrl(QStringLiteral("about:blank")));

        verticalLayout->addWidget(webView);

        plainTextEdit_2 = new QPlainTextEdit(siteUpdater);
        plainTextEdit_2->setObjectName(QStringLiteral("plainTextEdit_2"));
        QFont font;
        font.setFamily(QStringLiteral("Consolas"));
        plainTextEdit_2->setFont(font);

        verticalLayout->addWidget(plainTextEdit_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, -1, -1);
        radioButton_uploaded = new QRadioButton(siteUpdater);
        radioButton_uploaded->setObjectName(QStringLiteral("radioButton_uploaded"));
        radioButton_uploaded->setMinimumSize(QSize(150, 0));
        radioButton_uploaded->setChecked(true);

        horizontalLayout_3->addWidget(radioButton_uploaded);

        comboBox_uploaded = new QComboBox(siteUpdater);
        comboBox_uploaded->setObjectName(QStringLiteral("comboBox_uploaded"));
        comboBox_uploaded->setMinimumSize(QSize(100, 0));

        horizontalLayout_3->addWidget(comboBox_uploaded);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalLayout_4->setContentsMargins(-1, 0, -1, -1);
        radioButton_updated = new QRadioButton(siteUpdater);
        radioButton_updated->setObjectName(QStringLiteral("radioButton_updated"));
        radioButton_updated->setMinimumSize(QSize(150, 0));

        horizontalLayout_4->addWidget(radioButton_updated);

        comboBox_updated = new QComboBox(siteUpdater);
        comboBox_updated->setObjectName(QStringLiteral("comboBox_updated"));
        comboBox_updated->setMinimumSize(QSize(100, 0));

        horizontalLayout_4->addWidget(comboBox_updated);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_3);


        verticalLayout->addLayout(horizontalLayout_4);

        radioButton_single = new QRadioButton(siteUpdater);
        radioButton_single->setObjectName(QStringLiteral("radioButton_single"));

        verticalLayout->addWidget(radioButton_single);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label = new QLabel(siteUpdater);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout->addWidget(label);

        label_2 = new QLabel(siteUpdater);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout->addWidget(label_2);

        progressBar = new QProgressBar(siteUpdater);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setValue(0);

        horizontalLayout->addWidget(progressBar);


        verticalLayout->addLayout(horizontalLayout);

        tableView = new QTableView(siteUpdater);
        tableView->setObjectName(QStringLiteral("tableView"));
        tableView->setSelectionBehavior(QAbstractItemView::SelectRows);

        verticalLayout->addWidget(tableView);

        plainTextEdit = new QPlainTextEdit(siteUpdater);
        plainTextEdit->setObjectName(QStringLiteral("plainTextEdit"));
        plainTextEdit->setFont(font);

        verticalLayout->addWidget(plainTextEdit);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        pushButton_load = new QPushButton(siteUpdater);
        pushButton_load->setObjectName(QStringLiteral("pushButton_load"));
        pushButton_load->setMinimumSize(QSize(100, 25));

        horizontalLayout_2->addWidget(pushButton_load);

        pushButton_go = new QPushButton(siteUpdater);
        pushButton_go->setObjectName(QStringLiteral("pushButton_go"));
        pushButton_go->setMinimumSize(QSize(100, 25));

        horizontalLayout_2->addWidget(pushButton_go);

        pushButton_close = new QPushButton(siteUpdater);
        pushButton_close->setObjectName(QStringLiteral("pushButton_close"));
        pushButton_close->setMinimumSize(QSize(100, 25));

        horizontalLayout_2->addWidget(pushButton_close);


        verticalLayout->addLayout(horizontalLayout_2);


        retranslateUi(siteUpdater);

        QMetaObject::connectSlotsByName(siteUpdater);
    } // setupUi

    void retranslateUi(QDialog *siteUpdater)
    {
        siteUpdater->setWindowTitle(QApplication::translate("siteUpdater", "Dialog", 0));
        plainTextEdit_2->setPlainText(QApplication::translate("siteUpdater", "12345\n"
"asdoiqweqwe\303\251l\n"
"qwoecnaqweiqw\n"
"", 0));
        radioButton_uploaded->setText(QApplication::translate("siteUpdater", "Uploaded in last", 0));
        comboBox_uploaded->clear();
        comboBox_uploaded->insertItems(0, QStringList()
         << QApplication::translate("siteUpdater", "2 Days", 0)
         << QApplication::translate("siteUpdater", "1 Week", 0)
         << QApplication::translate("siteUpdater", "2 Weeks", 0)
         << QApplication::translate("siteUpdater", "1 Month", 0)
         << QApplication::translate("siteUpdater", "2 Months", 0)
        );
        radioButton_updated->setText(QApplication::translate("siteUpdater", "Last update older than", 0));
        comboBox_updated->clear();
        comboBox_updated->insertItems(0, QStringList()
         << QApplication::translate("siteUpdater", "1 Week", 0)
         << QApplication::translate("siteUpdater", "1 Month", 0)
         << QApplication::translate("siteUpdater", "2 Months", 0)
         << QApplication::translate("siteUpdater", "3 Months", 0)
         << QApplication::translate("siteUpdater", "1 Year", 0)
        );
        radioButton_single->setText(QApplication::translate("siteUpdater", "Single", 0));
        label->setText(QApplication::translate("siteUpdater", "Progress: ", 0));
        label_2->setText(QApplication::translate("siteUpdater", "TextLabel", 0));
        pushButton_load->setText(QApplication::translate("siteUpdater", "Load Sites", 0));
        pushButton_go->setText(QApplication::translate("siteUpdater", "Go!", 0));
        pushButton_close->setText(QApplication::translate("siteUpdater", "Close", 0));
    } // retranslateUi

};

namespace Ui {
    class siteUpdater: public Ui_siteUpdater {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SITEUPDATER_H
