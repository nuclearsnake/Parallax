/********************************************************************************
** Form generated from reading UI file 'organizerduplicatefiledialog.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ORGANIZERDUPLICATEFILEDIALOG_H
#define UI_ORGANIZERDUPLICATEFILEDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_organizerDuplicateFileDialog
{
public:
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QLabel *label_2;
    QLabel *label_target;
    QLabel *label_targetSize;
    QLabel *label;
    QGridLayout *gridLayout_2;
    QLabel *label_source;
    QLabel *label_4;
    QLabel *label_sourceSize;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_overWrite;
    QPushButton *pushButton_delete;
    QSpacerItem *horizontalSpacer_2;

    void setupUi(QDialog *organizerDuplicateFileDialog)
    {
        if (organizerDuplicateFileDialog->objectName().isEmpty())
            organizerDuplicateFileDialog->setObjectName(QStringLiteral("organizerDuplicateFileDialog"));
        organizerDuplicateFileDialog->resize(529, 157);
        verticalLayout = new QVBoxLayout(organizerDuplicateFileDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label_2 = new QLabel(organizerDuplicateFileDialog);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setMinimumSize(QSize(50, 0));
        label_2->setMaximumSize(QSize(50, 16777215));

        gridLayout->addWidget(label_2, 0, 0, 1, 1);

        label_target = new QLabel(organizerDuplicateFileDialog);
        label_target->setObjectName(QStringLiteral("label_target"));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        label_target->setFont(font);

        gridLayout->addWidget(label_target, 0, 1, 1, 1);

        label_targetSize = new QLabel(organizerDuplicateFileDialog);
        label_targetSize->setObjectName(QStringLiteral("label_targetSize"));
        label_targetSize->setFont(font);

        gridLayout->addWidget(label_targetSize, 1, 1, 1, 1);

        gridLayout->setColumnStretch(0, 1);
        gridLayout->setColumnStretch(1, 7);

        verticalLayout->addLayout(gridLayout);

        label = new QLabel(organizerDuplicateFileDialog);
        label->setObjectName(QStringLiteral("label"));
        label->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/arrow_up_32.png")));
        label->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        label_source = new QLabel(organizerDuplicateFileDialog);
        label_source->setObjectName(QStringLiteral("label_source"));
        label_source->setFont(font);

        gridLayout_2->addWidget(label_source, 0, 1, 1, 1);

        label_4 = new QLabel(organizerDuplicateFileDialog);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setMinimumSize(QSize(50, 0));
        label_4->setMaximumSize(QSize(50, 16777215));

        gridLayout_2->addWidget(label_4, 0, 0, 1, 1);

        label_sourceSize = new QLabel(organizerDuplicateFileDialog);
        label_sourceSize->setObjectName(QStringLiteral("label_sourceSize"));
        label_sourceSize->setFont(font);

        gridLayout_2->addWidget(label_sourceSize, 1, 1, 1, 1);

        gridLayout_2->setColumnStretch(0, 1);
        gridLayout_2->setColumnStretch(1, 7);

        verticalLayout->addLayout(gridLayout_2);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton_overWrite = new QPushButton(organizerDuplicateFileDialog);
        pushButton_overWrite->setObjectName(QStringLiteral("pushButton_overWrite"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(pushButton_overWrite->sizePolicy().hasHeightForWidth());
        pushButton_overWrite->setSizePolicy(sizePolicy);
        pushButton_overWrite->setMinimumSize(QSize(100, 25));

        horizontalLayout->addWidget(pushButton_overWrite);

        pushButton_delete = new QPushButton(organizerDuplicateFileDialog);
        pushButton_delete->setObjectName(QStringLiteral("pushButton_delete"));
        pushButton_delete->setMinimumSize(QSize(100, 25));

        horizontalLayout->addWidget(pushButton_delete);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(organizerDuplicateFileDialog);

        QMetaObject::connectSlotsByName(organizerDuplicateFileDialog);
    } // setupUi

    void retranslateUi(QDialog *organizerDuplicateFileDialog)
    {
        organizerDuplicateFileDialog->setWindowTitle(QApplication::translate("organizerDuplicateFileDialog", "Dialog", 0));
        label_2->setText(QApplication::translate("organizerDuplicateFileDialog", "Target:", 0));
        label_target->setText(QString());
        label_targetSize->setText(QString());
        label->setText(QString());
        label_source->setText(QString());
        label_4->setText(QApplication::translate("organizerDuplicateFileDialog", "Source:", 0));
        label_sourceSize->setText(QString());
        pushButton_overWrite->setText(QApplication::translate("organizerDuplicateFileDialog", "OverWrite", 0));
        pushButton_delete->setText(QApplication::translate("organizerDuplicateFileDialog", "Delete", 0));
    } // retranslateUi

};

namespace Ui {
    class organizerDuplicateFileDialog: public Ui_organizerDuplicateFileDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ORGANIZERDUPLICATEFILEDIALOG_H
