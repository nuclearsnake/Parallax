/********************************************************************************
** Form generated from reading UI file 'uploadtimesdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UPLOADTIMESDIALOG_H
#define UI_UPLOADTIMESDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include "extensions/clickable_qlabel.h"

QT_BEGIN_NAMESPACE

class Ui_uploadTimesDialog
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QComboBox *comboBox_site;
    QComboBox *comboBox_time;
    QPushButton *pushButton_get;
    QSpacerItem *horizontalSpacer;
    QGridLayout *gridLayout;
    clickable_QLabel *label_d3_h04;
    clickable_QLabel *label_d4_h05;
    clickable_QLabel *label_d3_h05;
    clickable_QLabel *label_d3_h06;
    clickable_QLabel *label_d3_h07;
    clickable_QLabel *label_d3_h08;
    clickable_QLabel *label_d3_h09;
    clickable_QLabel *label_d3_h10;
    clickable_QLabel *label_d3_h11;
    clickable_QLabel *label_d3_h12;
    clickable_QLabel *label_d3_h13;
    clickable_QLabel *label_d3_h14;
    clickable_QLabel *label_d3_h15;
    clickable_QLabel *label_d3_h16;
    clickable_QLabel *label_d3_h17;
    clickable_QLabel *label_d3_h18;
    QLabel *label_d3;
    QLabel *label_d4;
    QLabel *label_d6;
    clickable_QLabel *label_d5_h02;
    clickable_QLabel *label_d5_h03;
    clickable_QLabel *label_d5_h04;
    clickable_QLabel *label_d5_h05;
    clickable_QLabel *label_d5_h06;
    clickable_QLabel *label_d5_h07;
    clickable_QLabel *label_d5_h08;
    clickable_QLabel *label_d5_h09;
    clickable_QLabel *label_d5_h10;
    clickable_QLabel *label_d5_h11;
    clickable_QLabel *label_d7_h00;
    QLabel *label_avg_h02;
    QLabel *label_avg_h00;
    QLabel *label_avg_h03;
    QLabel *label_avg_h04;
    QLabel *label_avg_h05;
    QLabel *label_avg_h06;
    QLabel *label_avg_h07;
    QLabel *label_avg_h08;
    QLabel *label_avg_h09;
    QLabel *label_avg_h10;
    QLabel *label_avg_h11;
    QLabel *label_avg_h12;
    QLabel *label_avg_h13;
    QLabel *label_avg_h14;
    QLabel *label_avg_h15;
    QLabel *label_avg_h16;
    QLabel *label_avg_h17;
    QLabel *label_avg_h18;
    QLabel *label_avg_h19;
    QLabel *label_avg_h20;
    QLabel *label_avg_h21;
    QLabel *label_avg_h22;
    QLabel *label_avg_h23;
    QLabel *label_avg_h01;
    QLabel *label_avg_d3;
    QLabel *label_avg_d2;
    QLabel *label_avg_d1;
    QLabel *label_avg_d4;
    QLabel *label_avg_d5;
    QLabel *label_avg_d6;
    QLabel *label_avg_d7;
    clickable_QLabel *label_d5_h12;
    clickable_QLabel *label_d5_h13;
    clickable_QLabel *label_d5_h14;
    clickable_QLabel *label_d5_h15;
    clickable_QLabel *label_d5_h16;
    clickable_QLabel *label_d5_h17;
    clickable_QLabel *label_d5_h18;
    clickable_QLabel *label_d5_h19;
    clickable_QLabel *label_d5_h20;
    clickable_QLabel *label_d5_h21;
    clickable_QLabel *label_d5_h22;
    clickable_QLabel *label_d5_h23;
    clickable_QLabel *label_d5_h01;
    clickable_QLabel *label_d6_h00;
    clickable_QLabel *label_d6_h02;
    clickable_QLabel *label_d6_h03;
    clickable_QLabel *label_d6_h04;
    clickable_QLabel *label_d6_h05;
    clickable_QLabel *label_d6_h06;
    clickable_QLabel *label_d6_h07;
    clickable_QLabel *label_d6_h08;
    clickable_QLabel *label_d6_h09;
    clickable_QLabel *label_d6_h10;
    clickable_QLabel *label_d6_h11;
    clickable_QLabel *label_d6_h12;
    clickable_QLabel *label_d6_h13;
    clickable_QLabel *label_d6_h14;
    clickable_QLabel *label_d6_h15;
    clickable_QLabel *label_d6_h16;
    clickable_QLabel *label_d6_h17;
    clickable_QLabel *label_d6_h18;
    clickable_QLabel *label_d6_h19;
    clickable_QLabel *label_d6_h20;
    clickable_QLabel *label_d6_h22;
    clickable_QLabel *label_d6_h01;
    clickable_QLabel *label_d7_h02;
    clickable_QLabel *label_d7_h03;
    clickable_QLabel *label_d7_h04;
    clickable_QLabel *label_d7_h05;
    clickable_QLabel *label_d7_h06;
    clickable_QLabel *label_d7_h07;
    clickable_QLabel *label_d7_h08;
    clickable_QLabel *label_d7_h09;
    clickable_QLabel *label_d7_h10;
    clickable_QLabel *label_d7_h11;
    clickable_QLabel *label_d7_h12;
    clickable_QLabel *label_d7_h13;
    clickable_QLabel *label_d7_h14;
    clickable_QLabel *label_d7_h15;
    clickable_QLabel *label_d7_h16;
    clickable_QLabel *label_d7_h17;
    clickable_QLabel *label_d7_h18;
    clickable_QLabel *label_d7_h19;
    clickable_QLabel *label_d7_h20;
    clickable_QLabel *label_d7_h21;
    clickable_QLabel *label_d7_h22;
    clickable_QLabel *label_d7_h23;
    clickable_QLabel *label_d7_h01;
    clickable_QLabel *label_d4_h09;
    clickable_QLabel *label_d4_h10;
    clickable_QLabel *label_d4_h11;
    clickable_QLabel *label_d4_h12;
    clickable_QLabel *label_d4_h13;
    clickable_QLabel *label_d4_h14;
    clickable_QLabel *label_d4_h15;
    clickable_QLabel *label_d4_h16;
    clickable_QLabel *label_d4_h17;
    clickable_QLabel *label_d4_h18;
    clickable_QLabel *label_d4_h19;
    clickable_QLabel *label_d4_h20;
    clickable_QLabel *label_d4_h21;
    clickable_QLabel *label_d6_h21;
    QLabel *label_h04;
    QLabel *label_h06;
    clickable_QLabel *label_d2_h13;
    clickable_QLabel *label_d6_h23;
    QLabel *label_d5;
    QLabel *label_d7;
    clickable_QLabel *label_d1_h18;
    QLabel *label_h23;
    QLabel *label_h07;
    QLabel *label_h09;
    QLabel *label_h08;
    QLabel *label_h10;
    QLabel *label_h11;
    QLabel *label_h12;
    QLabel *label_h13;
    clickable_QLabel *label_d2_h19;
    clickable_QLabel *label_d2_h20;
    clickable_QLabel *label_d4_h22;
    clickable_QLabel *label_d4_h23;
    clickable_QLabel *label_d4_h01;
    clickable_QLabel *label_d1_h09;
    clickable_QLabel *label_d1_h10;
    clickable_QLabel *label_d1_h11;
    clickable_QLabel *label_d1_h12;
    clickable_QLabel *label_d1_h13;
    clickable_QLabel *label_d1_h21;
    clickable_QLabel *label_d3_h19;
    clickable_QLabel *label_d3_h20;
    clickable_QLabel *label_d3_h21;
    clickable_QLabel *label_d3_h22;
    clickable_QLabel *label_d3_h23;
    clickable_QLabel *label_d3_h01;
    clickable_QLabel *label_d4_h00;
    clickable_QLabel *label_d4_h04;
    clickable_QLabel *label_d4_h06;
    clickable_QLabel *label_d4_h03;
    clickable_QLabel *label_d4_h02;
    clickable_QLabel *label_d5_h00;
    clickable_QLabel *label_d2_h00;
    clickable_QLabel *label_d2_h03;
    clickable_QLabel *label_d2_h02;
    clickable_QLabel *label_d2_h04;
    clickable_QLabel *label_d2_h05;
    clickable_QLabel *label_d4_h07;
    QLabel *label_h01;
    QLabel *label_h02;
    QLabel *label_h05;
    clickable_QLabel *label_d1_h03;
    clickable_QLabel *label_d1_h08;
    QLabel *label_d1;
    QLabel *label_h00;
    clickable_QLabel *label_d1_h07;
    clickable_QLabel *label_d1_h01;
    clickable_QLabel *label_d1_h02;
    clickable_QLabel *label_d1_h14;
    clickable_QLabel *label_d1_h05;
    clickable_QLabel *label_d1_h06;
    clickable_QLabel *label_d1_h22;
    clickable_QLabel *label_d1_h23;
    clickable_QLabel *label_d3_h03;
    clickable_QLabel *label_d2_h06;
    clickable_QLabel *label_d2_h07;
    clickable_QLabel *label_d2_h08;
    clickable_QLabel *label_d2_h09;
    clickable_QLabel *label_d2_h10;
    clickable_QLabel *label_d2_h11;
    QLabel *label_h14;
    QLabel *label_h15;
    QLabel *label_h16;
    QLabel *label_h17;
    QLabel *label_h18;
    QLabel *label_h19;
    QLabel *label_h20;
    QLabel *label_h21;
    QLabel *label_h22;
    clickable_QLabel *label_d1_h04;
    QLabel *label_d2;
    QLabel *label_h03;
    clickable_QLabel *label_d1_h00;
    clickable_QLabel *label_d1_h15;
    clickable_QLabel *label_d1_h16;
    clickable_QLabel *label_d1_h17;
    clickable_QLabel *label_d1_h19;
    clickable_QLabel *label_d1_h20;
    clickable_QLabel *label_d2_h21;
    clickable_QLabel *label_d3_h00;
    clickable_QLabel *label_d2_h22;
    clickable_QLabel *label_d2_h23;
    clickable_QLabel *label_d2_h01;
    clickable_QLabel *label_d3_h02;
    clickable_QLabel *label_d2_h14;
    clickable_QLabel *label_d2_h15;
    clickable_QLabel *label_d2_h16;
    clickable_QLabel *label_d2_h17;
    clickable_QLabel *label_d2_h18;
    clickable_QLabel *label_d4_h08;
    clickable_QLabel *label_d2_h12;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_selectedInfo;
    QGridLayout *gridLayout_2;
    QLabel *label_top_pix_1;
    QLabel *label_top_pix_2;
    QLabel *label_top_line1_1;
    QLabel *label_top_pix_3;
    QLabel *label_top_line2_1;
    QLabel *label_top_line1_2;
    QLabel *label_top_line2_2;
    QLabel *label_top_line1_3;
    QLabel *label_top_line2_3;
    QPlainTextEdit *plainTextEdit;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *pushButton_close;

    void setupUi(QDialog *uploadTimesDialog)
    {
        if (uploadTimesDialog->objectName().isEmpty())
            uploadTimesDialog->setObjectName(QStringLiteral("uploadTimesDialog"));
        uploadTimesDialog->resize(853, 545);
        verticalLayout = new QVBoxLayout(uploadTimesDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        comboBox_site = new QComboBox(uploadTimesDialog);
        comboBox_site->setObjectName(QStringLiteral("comboBox_site"));
        comboBox_site->setMinimumSize(QSize(120, 0));

        horizontalLayout->addWidget(comboBox_site);

        comboBox_time = new QComboBox(uploadTimesDialog);
        comboBox_time->setObjectName(QStringLiteral("comboBox_time"));
        comboBox_time->setMinimumSize(QSize(120, 0));

        horizontalLayout->addWidget(comboBox_time);

        pushButton_get = new QPushButton(uploadTimesDialog);
        pushButton_get->setObjectName(QStringLiteral("pushButton_get"));

        horizontalLayout->addWidget(pushButton_get);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);


        verticalLayout->addLayout(horizontalLayout);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(0);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(-1, 0, -1, -1);
        label_d3_h04 = new clickable_QLabel(uploadTimesDialog);
        label_d3_h04->setObjectName(QStringLiteral("label_d3_h04"));
        label_d3_h04->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d3_h04, 4, 5, 1, 1);

        label_d4_h05 = new clickable_QLabel(uploadTimesDialog);
        label_d4_h05->setObjectName(QStringLiteral("label_d4_h05"));
        label_d4_h05->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d4_h05, 5, 6, 1, 1);

        label_d3_h05 = new clickable_QLabel(uploadTimesDialog);
        label_d3_h05->setObjectName(QStringLiteral("label_d3_h05"));
        label_d3_h05->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d3_h05, 4, 6, 1, 1);

        label_d3_h06 = new clickable_QLabel(uploadTimesDialog);
        label_d3_h06->setObjectName(QStringLiteral("label_d3_h06"));
        label_d3_h06->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d3_h06, 4, 7, 1, 1);

        label_d3_h07 = new clickable_QLabel(uploadTimesDialog);
        label_d3_h07->setObjectName(QStringLiteral("label_d3_h07"));
        label_d3_h07->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d3_h07, 4, 8, 1, 1);

        label_d3_h08 = new clickable_QLabel(uploadTimesDialog);
        label_d3_h08->setObjectName(QStringLiteral("label_d3_h08"));
        label_d3_h08->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d3_h08, 4, 9, 1, 1);

        label_d3_h09 = new clickable_QLabel(uploadTimesDialog);
        label_d3_h09->setObjectName(QStringLiteral("label_d3_h09"));
        label_d3_h09->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d3_h09, 4, 10, 1, 1);

        label_d3_h10 = new clickable_QLabel(uploadTimesDialog);
        label_d3_h10->setObjectName(QStringLiteral("label_d3_h10"));
        label_d3_h10->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d3_h10, 4, 11, 1, 1);

        label_d3_h11 = new clickable_QLabel(uploadTimesDialog);
        label_d3_h11->setObjectName(QStringLiteral("label_d3_h11"));
        label_d3_h11->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d3_h11, 4, 12, 1, 1);

        label_d3_h12 = new clickable_QLabel(uploadTimesDialog);
        label_d3_h12->setObjectName(QStringLiteral("label_d3_h12"));
        label_d3_h12->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d3_h12, 4, 13, 1, 1);

        label_d3_h13 = new clickable_QLabel(uploadTimesDialog);
        label_d3_h13->setObjectName(QStringLiteral("label_d3_h13"));
        label_d3_h13->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d3_h13, 4, 14, 1, 1);

        label_d3_h14 = new clickable_QLabel(uploadTimesDialog);
        label_d3_h14->setObjectName(QStringLiteral("label_d3_h14"));
        label_d3_h14->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d3_h14, 4, 15, 1, 1);

        label_d3_h15 = new clickable_QLabel(uploadTimesDialog);
        label_d3_h15->setObjectName(QStringLiteral("label_d3_h15"));
        label_d3_h15->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d3_h15, 4, 16, 1, 1);

        label_d3_h16 = new clickable_QLabel(uploadTimesDialog);
        label_d3_h16->setObjectName(QStringLiteral("label_d3_h16"));
        label_d3_h16->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d3_h16, 4, 17, 1, 1);

        label_d3_h17 = new clickable_QLabel(uploadTimesDialog);
        label_d3_h17->setObjectName(QStringLiteral("label_d3_h17"));
        label_d3_h17->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d3_h17, 4, 18, 1, 1);

        label_d3_h18 = new clickable_QLabel(uploadTimesDialog);
        label_d3_h18->setObjectName(QStringLiteral("label_d3_h18"));
        label_d3_h18->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d3_h18, 4, 19, 1, 1);

        label_d3 = new QLabel(uploadTimesDialog);
        label_d3->setObjectName(QStringLiteral("label_d3"));
        label_d3->setMinimumSize(QSize(70, 0));
        label_d3->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout->addWidget(label_d3, 4, 0, 1, 1);

        label_d4 = new QLabel(uploadTimesDialog);
        label_d4->setObjectName(QStringLiteral("label_d4"));
        label_d4->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout->addWidget(label_d4, 5, 0, 1, 1);

        label_d6 = new QLabel(uploadTimesDialog);
        label_d6->setObjectName(QStringLiteral("label_d6"));
        label_d6->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout->addWidget(label_d6, 7, 0, 1, 1);

        label_d5_h02 = new clickable_QLabel(uploadTimesDialog);
        label_d5_h02->setObjectName(QStringLiteral("label_d5_h02"));
        label_d5_h02->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d5_h02, 6, 3, 1, 1);

        label_d5_h03 = new clickable_QLabel(uploadTimesDialog);
        label_d5_h03->setObjectName(QStringLiteral("label_d5_h03"));
        label_d5_h03->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d5_h03, 6, 4, 1, 1);

        label_d5_h04 = new clickable_QLabel(uploadTimesDialog);
        label_d5_h04->setObjectName(QStringLiteral("label_d5_h04"));
        label_d5_h04->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d5_h04, 6, 5, 1, 1);

        label_d5_h05 = new clickable_QLabel(uploadTimesDialog);
        label_d5_h05->setObjectName(QStringLiteral("label_d5_h05"));
        label_d5_h05->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d5_h05, 6, 6, 1, 1);

        label_d5_h06 = new clickable_QLabel(uploadTimesDialog);
        label_d5_h06->setObjectName(QStringLiteral("label_d5_h06"));
        label_d5_h06->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d5_h06, 6, 7, 1, 1);

        label_d5_h07 = new clickable_QLabel(uploadTimesDialog);
        label_d5_h07->setObjectName(QStringLiteral("label_d5_h07"));
        label_d5_h07->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d5_h07, 6, 8, 1, 1);

        label_d5_h08 = new clickable_QLabel(uploadTimesDialog);
        label_d5_h08->setObjectName(QStringLiteral("label_d5_h08"));
        label_d5_h08->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d5_h08, 6, 9, 1, 1);

        label_d5_h09 = new clickable_QLabel(uploadTimesDialog);
        label_d5_h09->setObjectName(QStringLiteral("label_d5_h09"));
        label_d5_h09->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d5_h09, 6, 10, 1, 1);

        label_d5_h10 = new clickable_QLabel(uploadTimesDialog);
        label_d5_h10->setObjectName(QStringLiteral("label_d5_h10"));
        label_d5_h10->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d5_h10, 6, 11, 1, 1);

        label_d5_h11 = new clickable_QLabel(uploadTimesDialog);
        label_d5_h11->setObjectName(QStringLiteral("label_d5_h11"));
        label_d5_h11->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d5_h11, 6, 12, 1, 1);

        label_d7_h00 = new clickable_QLabel(uploadTimesDialog);
        label_d7_h00->setObjectName(QStringLiteral("label_d7_h00"));
        label_d7_h00->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d7_h00, 8, 1, 1, 1);

        label_avg_h02 = new QLabel(uploadTimesDialog);
        label_avg_h02->setObjectName(QStringLiteral("label_avg_h02"));
        label_avg_h02->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_avg_h02, 10, 3, 1, 1);

        label_avg_h00 = new QLabel(uploadTimesDialog);
        label_avg_h00->setObjectName(QStringLiteral("label_avg_h00"));
        label_avg_h00->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_avg_h00, 10, 1, 1, 1);

        label_avg_h03 = new QLabel(uploadTimesDialog);
        label_avg_h03->setObjectName(QStringLiteral("label_avg_h03"));
        label_avg_h03->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_avg_h03, 10, 4, 1, 1);

        label_avg_h04 = new QLabel(uploadTimesDialog);
        label_avg_h04->setObjectName(QStringLiteral("label_avg_h04"));
        label_avg_h04->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_avg_h04, 10, 5, 1, 1);

        label_avg_h05 = new QLabel(uploadTimesDialog);
        label_avg_h05->setObjectName(QStringLiteral("label_avg_h05"));
        label_avg_h05->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_avg_h05, 10, 6, 1, 1);

        label_avg_h06 = new QLabel(uploadTimesDialog);
        label_avg_h06->setObjectName(QStringLiteral("label_avg_h06"));
        label_avg_h06->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_avg_h06, 10, 7, 1, 1);

        label_avg_h07 = new QLabel(uploadTimesDialog);
        label_avg_h07->setObjectName(QStringLiteral("label_avg_h07"));
        label_avg_h07->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_avg_h07, 10, 8, 1, 1);

        label_avg_h08 = new QLabel(uploadTimesDialog);
        label_avg_h08->setObjectName(QStringLiteral("label_avg_h08"));
        label_avg_h08->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_avg_h08, 10, 9, 1, 1);

        label_avg_h09 = new QLabel(uploadTimesDialog);
        label_avg_h09->setObjectName(QStringLiteral("label_avg_h09"));
        label_avg_h09->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_avg_h09, 10, 10, 1, 1);

        label_avg_h10 = new QLabel(uploadTimesDialog);
        label_avg_h10->setObjectName(QStringLiteral("label_avg_h10"));
        label_avg_h10->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_avg_h10, 10, 11, 1, 1);

        label_avg_h11 = new QLabel(uploadTimesDialog);
        label_avg_h11->setObjectName(QStringLiteral("label_avg_h11"));
        label_avg_h11->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_avg_h11, 10, 12, 1, 1);

        label_avg_h12 = new QLabel(uploadTimesDialog);
        label_avg_h12->setObjectName(QStringLiteral("label_avg_h12"));
        label_avg_h12->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_avg_h12, 10, 13, 1, 1);

        label_avg_h13 = new QLabel(uploadTimesDialog);
        label_avg_h13->setObjectName(QStringLiteral("label_avg_h13"));
        label_avg_h13->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_avg_h13, 10, 14, 1, 1);

        label_avg_h14 = new QLabel(uploadTimesDialog);
        label_avg_h14->setObjectName(QStringLiteral("label_avg_h14"));
        label_avg_h14->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_avg_h14, 10, 15, 1, 1);

        label_avg_h15 = new QLabel(uploadTimesDialog);
        label_avg_h15->setObjectName(QStringLiteral("label_avg_h15"));
        label_avg_h15->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_avg_h15, 10, 16, 1, 1);

        label_avg_h16 = new QLabel(uploadTimesDialog);
        label_avg_h16->setObjectName(QStringLiteral("label_avg_h16"));
        label_avg_h16->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_avg_h16, 10, 17, 1, 1);

        label_avg_h17 = new QLabel(uploadTimesDialog);
        label_avg_h17->setObjectName(QStringLiteral("label_avg_h17"));
        label_avg_h17->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_avg_h17, 10, 18, 1, 1);

        label_avg_h18 = new QLabel(uploadTimesDialog);
        label_avg_h18->setObjectName(QStringLiteral("label_avg_h18"));
        label_avg_h18->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_avg_h18, 10, 19, 1, 1);

        label_avg_h19 = new QLabel(uploadTimesDialog);
        label_avg_h19->setObjectName(QStringLiteral("label_avg_h19"));
        label_avg_h19->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_avg_h19, 10, 20, 1, 1);

        label_avg_h20 = new QLabel(uploadTimesDialog);
        label_avg_h20->setObjectName(QStringLiteral("label_avg_h20"));
        label_avg_h20->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_avg_h20, 10, 21, 1, 1);

        label_avg_h21 = new QLabel(uploadTimesDialog);
        label_avg_h21->setObjectName(QStringLiteral("label_avg_h21"));
        label_avg_h21->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_avg_h21, 10, 22, 1, 1);

        label_avg_h22 = new QLabel(uploadTimesDialog);
        label_avg_h22->setObjectName(QStringLiteral("label_avg_h22"));
        label_avg_h22->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_avg_h22, 10, 23, 1, 1);

        label_avg_h23 = new QLabel(uploadTimesDialog);
        label_avg_h23->setObjectName(QStringLiteral("label_avg_h23"));
        label_avg_h23->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_avg_h23, 10, 24, 1, 1);

        label_avg_h01 = new QLabel(uploadTimesDialog);
        label_avg_h01->setObjectName(QStringLiteral("label_avg_h01"));
        label_avg_h01->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_avg_h01, 10, 2, 1, 1);

        label_avg_d3 = new QLabel(uploadTimesDialog);
        label_avg_d3->setObjectName(QStringLiteral("label_avg_d3"));
        label_avg_d3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_avg_d3, 4, 26, 1, 1);

        label_avg_d2 = new QLabel(uploadTimesDialog);
        label_avg_d2->setObjectName(QStringLiteral("label_avg_d2"));
        label_avg_d2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_avg_d2, 3, 26, 1, 1);

        label_avg_d1 = new QLabel(uploadTimesDialog);
        label_avg_d1->setObjectName(QStringLiteral("label_avg_d1"));
        label_avg_d1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_avg_d1, 2, 26, 1, 1);

        label_avg_d4 = new QLabel(uploadTimesDialog);
        label_avg_d4->setObjectName(QStringLiteral("label_avg_d4"));
        label_avg_d4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_avg_d4, 5, 26, 1, 1);

        label_avg_d5 = new QLabel(uploadTimesDialog);
        label_avg_d5->setObjectName(QStringLiteral("label_avg_d5"));
        label_avg_d5->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_avg_d5, 6, 26, 1, 1);

        label_avg_d6 = new QLabel(uploadTimesDialog);
        label_avg_d6->setObjectName(QStringLiteral("label_avg_d6"));
        label_avg_d6->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_avg_d6, 7, 26, 1, 1);

        label_avg_d7 = new QLabel(uploadTimesDialog);
        label_avg_d7->setObjectName(QStringLiteral("label_avg_d7"));
        label_avg_d7->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_avg_d7, 8, 26, 1, 1);

        label_d5_h12 = new clickable_QLabel(uploadTimesDialog);
        label_d5_h12->setObjectName(QStringLiteral("label_d5_h12"));
        label_d5_h12->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d5_h12, 6, 13, 1, 1);

        label_d5_h13 = new clickable_QLabel(uploadTimesDialog);
        label_d5_h13->setObjectName(QStringLiteral("label_d5_h13"));
        label_d5_h13->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d5_h13, 6, 14, 1, 1);

        label_d5_h14 = new clickable_QLabel(uploadTimesDialog);
        label_d5_h14->setObjectName(QStringLiteral("label_d5_h14"));
        label_d5_h14->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d5_h14, 6, 15, 1, 1);

        label_d5_h15 = new clickable_QLabel(uploadTimesDialog);
        label_d5_h15->setObjectName(QStringLiteral("label_d5_h15"));
        label_d5_h15->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d5_h15, 6, 16, 1, 1);

        label_d5_h16 = new clickable_QLabel(uploadTimesDialog);
        label_d5_h16->setObjectName(QStringLiteral("label_d5_h16"));
        label_d5_h16->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d5_h16, 6, 17, 1, 1);

        label_d5_h17 = new clickable_QLabel(uploadTimesDialog);
        label_d5_h17->setObjectName(QStringLiteral("label_d5_h17"));
        label_d5_h17->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d5_h17, 6, 18, 1, 1);

        label_d5_h18 = new clickable_QLabel(uploadTimesDialog);
        label_d5_h18->setObjectName(QStringLiteral("label_d5_h18"));
        label_d5_h18->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d5_h18, 6, 19, 1, 1);

        label_d5_h19 = new clickable_QLabel(uploadTimesDialog);
        label_d5_h19->setObjectName(QStringLiteral("label_d5_h19"));
        label_d5_h19->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d5_h19, 6, 20, 1, 1);

        label_d5_h20 = new clickable_QLabel(uploadTimesDialog);
        label_d5_h20->setObjectName(QStringLiteral("label_d5_h20"));
        label_d5_h20->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d5_h20, 6, 21, 1, 1);

        label_d5_h21 = new clickable_QLabel(uploadTimesDialog);
        label_d5_h21->setObjectName(QStringLiteral("label_d5_h21"));
        label_d5_h21->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d5_h21, 6, 22, 1, 1);

        label_d5_h22 = new clickable_QLabel(uploadTimesDialog);
        label_d5_h22->setObjectName(QStringLiteral("label_d5_h22"));
        label_d5_h22->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d5_h22, 6, 23, 1, 1);

        label_d5_h23 = new clickable_QLabel(uploadTimesDialog);
        label_d5_h23->setObjectName(QStringLiteral("label_d5_h23"));
        label_d5_h23->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d5_h23, 6, 24, 1, 1);

        label_d5_h01 = new clickable_QLabel(uploadTimesDialog);
        label_d5_h01->setObjectName(QStringLiteral("label_d5_h01"));
        label_d5_h01->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d5_h01, 6, 2, 1, 1);

        label_d6_h00 = new clickable_QLabel(uploadTimesDialog);
        label_d6_h00->setObjectName(QStringLiteral("label_d6_h00"));
        label_d6_h00->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d6_h00, 7, 1, 1, 1);

        label_d6_h02 = new clickable_QLabel(uploadTimesDialog);
        label_d6_h02->setObjectName(QStringLiteral("label_d6_h02"));
        label_d6_h02->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d6_h02, 7, 3, 1, 1);

        label_d6_h03 = new clickable_QLabel(uploadTimesDialog);
        label_d6_h03->setObjectName(QStringLiteral("label_d6_h03"));
        label_d6_h03->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d6_h03, 7, 4, 1, 1);

        label_d6_h04 = new clickable_QLabel(uploadTimesDialog);
        label_d6_h04->setObjectName(QStringLiteral("label_d6_h04"));
        label_d6_h04->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d6_h04, 7, 5, 1, 1);

        label_d6_h05 = new clickable_QLabel(uploadTimesDialog);
        label_d6_h05->setObjectName(QStringLiteral("label_d6_h05"));
        label_d6_h05->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d6_h05, 7, 6, 1, 1);

        label_d6_h06 = new clickable_QLabel(uploadTimesDialog);
        label_d6_h06->setObjectName(QStringLiteral("label_d6_h06"));
        label_d6_h06->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d6_h06, 7, 7, 1, 1);

        label_d6_h07 = new clickable_QLabel(uploadTimesDialog);
        label_d6_h07->setObjectName(QStringLiteral("label_d6_h07"));
        label_d6_h07->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d6_h07, 7, 8, 1, 1);

        label_d6_h08 = new clickable_QLabel(uploadTimesDialog);
        label_d6_h08->setObjectName(QStringLiteral("label_d6_h08"));
        label_d6_h08->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d6_h08, 7, 9, 1, 1);

        label_d6_h09 = new clickable_QLabel(uploadTimesDialog);
        label_d6_h09->setObjectName(QStringLiteral("label_d6_h09"));
        label_d6_h09->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d6_h09, 7, 10, 1, 1);

        label_d6_h10 = new clickable_QLabel(uploadTimesDialog);
        label_d6_h10->setObjectName(QStringLiteral("label_d6_h10"));
        label_d6_h10->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d6_h10, 7, 11, 1, 1);

        label_d6_h11 = new clickable_QLabel(uploadTimesDialog);
        label_d6_h11->setObjectName(QStringLiteral("label_d6_h11"));
        label_d6_h11->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d6_h11, 7, 12, 1, 1);

        label_d6_h12 = new clickable_QLabel(uploadTimesDialog);
        label_d6_h12->setObjectName(QStringLiteral("label_d6_h12"));
        label_d6_h12->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d6_h12, 7, 13, 1, 1);

        label_d6_h13 = new clickable_QLabel(uploadTimesDialog);
        label_d6_h13->setObjectName(QStringLiteral("label_d6_h13"));
        label_d6_h13->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d6_h13, 7, 14, 1, 1);

        label_d6_h14 = new clickable_QLabel(uploadTimesDialog);
        label_d6_h14->setObjectName(QStringLiteral("label_d6_h14"));
        label_d6_h14->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d6_h14, 7, 15, 1, 1);

        label_d6_h15 = new clickable_QLabel(uploadTimesDialog);
        label_d6_h15->setObjectName(QStringLiteral("label_d6_h15"));
        label_d6_h15->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d6_h15, 7, 16, 1, 1);

        label_d6_h16 = new clickable_QLabel(uploadTimesDialog);
        label_d6_h16->setObjectName(QStringLiteral("label_d6_h16"));
        label_d6_h16->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d6_h16, 7, 17, 1, 1);

        label_d6_h17 = new clickable_QLabel(uploadTimesDialog);
        label_d6_h17->setObjectName(QStringLiteral("label_d6_h17"));
        label_d6_h17->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d6_h17, 7, 18, 1, 1);

        label_d6_h18 = new clickable_QLabel(uploadTimesDialog);
        label_d6_h18->setObjectName(QStringLiteral("label_d6_h18"));
        label_d6_h18->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d6_h18, 7, 19, 1, 1);

        label_d6_h19 = new clickable_QLabel(uploadTimesDialog);
        label_d6_h19->setObjectName(QStringLiteral("label_d6_h19"));
        label_d6_h19->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d6_h19, 7, 20, 1, 1);

        label_d6_h20 = new clickable_QLabel(uploadTimesDialog);
        label_d6_h20->setObjectName(QStringLiteral("label_d6_h20"));
        label_d6_h20->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d6_h20, 7, 21, 1, 1);

        label_d6_h22 = new clickable_QLabel(uploadTimesDialog);
        label_d6_h22->setObjectName(QStringLiteral("label_d6_h22"));
        label_d6_h22->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d6_h22, 7, 23, 1, 1);

        label_d6_h01 = new clickable_QLabel(uploadTimesDialog);
        label_d6_h01->setObjectName(QStringLiteral("label_d6_h01"));
        label_d6_h01->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d6_h01, 7, 2, 1, 1);

        label_d7_h02 = new clickable_QLabel(uploadTimesDialog);
        label_d7_h02->setObjectName(QStringLiteral("label_d7_h02"));
        label_d7_h02->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d7_h02, 8, 3, 1, 1);

        label_d7_h03 = new clickable_QLabel(uploadTimesDialog);
        label_d7_h03->setObjectName(QStringLiteral("label_d7_h03"));
        label_d7_h03->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d7_h03, 8, 4, 1, 1);

        label_d7_h04 = new clickable_QLabel(uploadTimesDialog);
        label_d7_h04->setObjectName(QStringLiteral("label_d7_h04"));
        label_d7_h04->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d7_h04, 8, 5, 1, 1);

        label_d7_h05 = new clickable_QLabel(uploadTimesDialog);
        label_d7_h05->setObjectName(QStringLiteral("label_d7_h05"));
        label_d7_h05->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d7_h05, 8, 6, 1, 1);

        label_d7_h06 = new clickable_QLabel(uploadTimesDialog);
        label_d7_h06->setObjectName(QStringLiteral("label_d7_h06"));
        label_d7_h06->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d7_h06, 8, 7, 1, 1);

        label_d7_h07 = new clickable_QLabel(uploadTimesDialog);
        label_d7_h07->setObjectName(QStringLiteral("label_d7_h07"));
        label_d7_h07->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d7_h07, 8, 8, 1, 1);

        label_d7_h08 = new clickable_QLabel(uploadTimesDialog);
        label_d7_h08->setObjectName(QStringLiteral("label_d7_h08"));
        label_d7_h08->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d7_h08, 8, 9, 1, 1);

        label_d7_h09 = new clickable_QLabel(uploadTimesDialog);
        label_d7_h09->setObjectName(QStringLiteral("label_d7_h09"));
        label_d7_h09->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d7_h09, 8, 10, 1, 1);

        label_d7_h10 = new clickable_QLabel(uploadTimesDialog);
        label_d7_h10->setObjectName(QStringLiteral("label_d7_h10"));
        label_d7_h10->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d7_h10, 8, 11, 1, 1);

        label_d7_h11 = new clickable_QLabel(uploadTimesDialog);
        label_d7_h11->setObjectName(QStringLiteral("label_d7_h11"));
        label_d7_h11->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d7_h11, 8, 12, 1, 1);

        label_d7_h12 = new clickable_QLabel(uploadTimesDialog);
        label_d7_h12->setObjectName(QStringLiteral("label_d7_h12"));
        label_d7_h12->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d7_h12, 8, 13, 1, 1);

        label_d7_h13 = new clickable_QLabel(uploadTimesDialog);
        label_d7_h13->setObjectName(QStringLiteral("label_d7_h13"));
        label_d7_h13->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d7_h13, 8, 14, 1, 1);

        label_d7_h14 = new clickable_QLabel(uploadTimesDialog);
        label_d7_h14->setObjectName(QStringLiteral("label_d7_h14"));
        label_d7_h14->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d7_h14, 8, 15, 1, 1);

        label_d7_h15 = new clickable_QLabel(uploadTimesDialog);
        label_d7_h15->setObjectName(QStringLiteral("label_d7_h15"));
        label_d7_h15->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d7_h15, 8, 16, 1, 1);

        label_d7_h16 = new clickable_QLabel(uploadTimesDialog);
        label_d7_h16->setObjectName(QStringLiteral("label_d7_h16"));
        label_d7_h16->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d7_h16, 8, 17, 1, 1);

        label_d7_h17 = new clickable_QLabel(uploadTimesDialog);
        label_d7_h17->setObjectName(QStringLiteral("label_d7_h17"));
        label_d7_h17->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d7_h17, 8, 18, 1, 1);

        label_d7_h18 = new clickable_QLabel(uploadTimesDialog);
        label_d7_h18->setObjectName(QStringLiteral("label_d7_h18"));
        label_d7_h18->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d7_h18, 8, 19, 1, 1);

        label_d7_h19 = new clickable_QLabel(uploadTimesDialog);
        label_d7_h19->setObjectName(QStringLiteral("label_d7_h19"));
        label_d7_h19->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d7_h19, 8, 20, 1, 1);

        label_d7_h20 = new clickable_QLabel(uploadTimesDialog);
        label_d7_h20->setObjectName(QStringLiteral("label_d7_h20"));
        label_d7_h20->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d7_h20, 8, 21, 1, 1);

        label_d7_h21 = new clickable_QLabel(uploadTimesDialog);
        label_d7_h21->setObjectName(QStringLiteral("label_d7_h21"));
        label_d7_h21->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d7_h21, 8, 22, 1, 1);

        label_d7_h22 = new clickable_QLabel(uploadTimesDialog);
        label_d7_h22->setObjectName(QStringLiteral("label_d7_h22"));
        label_d7_h22->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d7_h22, 8, 23, 1, 1);

        label_d7_h23 = new clickable_QLabel(uploadTimesDialog);
        label_d7_h23->setObjectName(QStringLiteral("label_d7_h23"));
        label_d7_h23->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d7_h23, 8, 24, 1, 1);

        label_d7_h01 = new clickable_QLabel(uploadTimesDialog);
        label_d7_h01->setObjectName(QStringLiteral("label_d7_h01"));
        label_d7_h01->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d7_h01, 8, 2, 1, 1);

        label_d4_h09 = new clickable_QLabel(uploadTimesDialog);
        label_d4_h09->setObjectName(QStringLiteral("label_d4_h09"));
        label_d4_h09->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d4_h09, 5, 10, 1, 1);

        label_d4_h10 = new clickable_QLabel(uploadTimesDialog);
        label_d4_h10->setObjectName(QStringLiteral("label_d4_h10"));
        label_d4_h10->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d4_h10, 5, 11, 1, 1);

        label_d4_h11 = new clickable_QLabel(uploadTimesDialog);
        label_d4_h11->setObjectName(QStringLiteral("label_d4_h11"));
        label_d4_h11->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d4_h11, 5, 12, 1, 1);

        label_d4_h12 = new clickable_QLabel(uploadTimesDialog);
        label_d4_h12->setObjectName(QStringLiteral("label_d4_h12"));
        label_d4_h12->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d4_h12, 5, 13, 1, 1);

        label_d4_h13 = new clickable_QLabel(uploadTimesDialog);
        label_d4_h13->setObjectName(QStringLiteral("label_d4_h13"));
        label_d4_h13->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d4_h13, 5, 14, 1, 1);

        label_d4_h14 = new clickable_QLabel(uploadTimesDialog);
        label_d4_h14->setObjectName(QStringLiteral("label_d4_h14"));
        label_d4_h14->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d4_h14, 5, 15, 1, 1);

        label_d4_h15 = new clickable_QLabel(uploadTimesDialog);
        label_d4_h15->setObjectName(QStringLiteral("label_d4_h15"));
        label_d4_h15->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d4_h15, 5, 16, 1, 1);

        label_d4_h16 = new clickable_QLabel(uploadTimesDialog);
        label_d4_h16->setObjectName(QStringLiteral("label_d4_h16"));
        label_d4_h16->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d4_h16, 5, 17, 1, 1);

        label_d4_h17 = new clickable_QLabel(uploadTimesDialog);
        label_d4_h17->setObjectName(QStringLiteral("label_d4_h17"));
        label_d4_h17->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d4_h17, 5, 18, 1, 1);

        label_d4_h18 = new clickable_QLabel(uploadTimesDialog);
        label_d4_h18->setObjectName(QStringLiteral("label_d4_h18"));
        label_d4_h18->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d4_h18, 5, 19, 1, 1);

        label_d4_h19 = new clickable_QLabel(uploadTimesDialog);
        label_d4_h19->setObjectName(QStringLiteral("label_d4_h19"));
        label_d4_h19->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d4_h19, 5, 20, 1, 1);

        label_d4_h20 = new clickable_QLabel(uploadTimesDialog);
        label_d4_h20->setObjectName(QStringLiteral("label_d4_h20"));
        label_d4_h20->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d4_h20, 5, 21, 1, 1);

        label_d4_h21 = new clickable_QLabel(uploadTimesDialog);
        label_d4_h21->setObjectName(QStringLiteral("label_d4_h21"));
        label_d4_h21->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d4_h21, 5, 22, 1, 1);

        label_d6_h21 = new clickable_QLabel(uploadTimesDialog);
        label_d6_h21->setObjectName(QStringLiteral("label_d6_h21"));
        label_d6_h21->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d6_h21, 7, 22, 1, 1);

        label_h04 = new QLabel(uploadTimesDialog);
        label_h04->setObjectName(QStringLiteral("label_h04"));
        label_h04->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_h04, 1, 5, 1, 1);

        label_h06 = new QLabel(uploadTimesDialog);
        label_h06->setObjectName(QStringLiteral("label_h06"));
        label_h06->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_h06, 1, 7, 1, 1);

        label_d2_h13 = new clickable_QLabel(uploadTimesDialog);
        label_d2_h13->setObjectName(QStringLiteral("label_d2_h13"));
        label_d2_h13->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d2_h13, 3, 14, 1, 1);

        label_d6_h23 = new clickable_QLabel(uploadTimesDialog);
        label_d6_h23->setObjectName(QStringLiteral("label_d6_h23"));
        label_d6_h23->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d6_h23, 7, 24, 1, 1);

        label_d5 = new QLabel(uploadTimesDialog);
        label_d5->setObjectName(QStringLiteral("label_d5"));
        label_d5->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout->addWidget(label_d5, 6, 0, 1, 1);

        label_d7 = new QLabel(uploadTimesDialog);
        label_d7->setObjectName(QStringLiteral("label_d7"));
        label_d7->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout->addWidget(label_d7, 8, 0, 1, 1);

        label_d1_h18 = new clickable_QLabel(uploadTimesDialog);
        label_d1_h18->setObjectName(QStringLiteral("label_d1_h18"));
        label_d1_h18->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d1_h18, 2, 19, 1, 1);

        label_h23 = new QLabel(uploadTimesDialog);
        label_h23->setObjectName(QStringLiteral("label_h23"));
        label_h23->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_h23, 1, 24, 1, 1);

        label_h07 = new QLabel(uploadTimesDialog);
        label_h07->setObjectName(QStringLiteral("label_h07"));
        label_h07->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_h07, 1, 8, 1, 1);

        label_h09 = new QLabel(uploadTimesDialog);
        label_h09->setObjectName(QStringLiteral("label_h09"));
        label_h09->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_h09, 1, 10, 1, 1);

        label_h08 = new QLabel(uploadTimesDialog);
        label_h08->setObjectName(QStringLiteral("label_h08"));
        label_h08->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_h08, 1, 9, 1, 1);

        label_h10 = new QLabel(uploadTimesDialog);
        label_h10->setObjectName(QStringLiteral("label_h10"));
        label_h10->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_h10, 1, 11, 1, 1);

        label_h11 = new QLabel(uploadTimesDialog);
        label_h11->setObjectName(QStringLiteral("label_h11"));
        label_h11->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_h11, 1, 12, 1, 1);

        label_h12 = new QLabel(uploadTimesDialog);
        label_h12->setObjectName(QStringLiteral("label_h12"));
        label_h12->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_h12, 1, 13, 1, 1);

        label_h13 = new QLabel(uploadTimesDialog);
        label_h13->setObjectName(QStringLiteral("label_h13"));
        label_h13->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_h13, 1, 14, 1, 1);

        label_d2_h19 = new clickable_QLabel(uploadTimesDialog);
        label_d2_h19->setObjectName(QStringLiteral("label_d2_h19"));
        label_d2_h19->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d2_h19, 3, 20, 1, 1);

        label_d2_h20 = new clickable_QLabel(uploadTimesDialog);
        label_d2_h20->setObjectName(QStringLiteral("label_d2_h20"));
        label_d2_h20->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d2_h20, 3, 21, 1, 1);

        label_d4_h22 = new clickable_QLabel(uploadTimesDialog);
        label_d4_h22->setObjectName(QStringLiteral("label_d4_h22"));
        label_d4_h22->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d4_h22, 5, 23, 1, 1);

        label_d4_h23 = new clickable_QLabel(uploadTimesDialog);
        label_d4_h23->setObjectName(QStringLiteral("label_d4_h23"));
        label_d4_h23->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d4_h23, 5, 24, 1, 1);

        label_d4_h01 = new clickable_QLabel(uploadTimesDialog);
        label_d4_h01->setObjectName(QStringLiteral("label_d4_h01"));
        label_d4_h01->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d4_h01, 5, 2, 1, 1);

        label_d1_h09 = new clickable_QLabel(uploadTimesDialog);
        label_d1_h09->setObjectName(QStringLiteral("label_d1_h09"));
        label_d1_h09->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d1_h09, 2, 10, 1, 1);

        label_d1_h10 = new clickable_QLabel(uploadTimesDialog);
        label_d1_h10->setObjectName(QStringLiteral("label_d1_h10"));
        label_d1_h10->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d1_h10, 2, 11, 1, 1);

        label_d1_h11 = new clickable_QLabel(uploadTimesDialog);
        label_d1_h11->setObjectName(QStringLiteral("label_d1_h11"));
        label_d1_h11->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d1_h11, 2, 12, 1, 1);

        label_d1_h12 = new clickable_QLabel(uploadTimesDialog);
        label_d1_h12->setObjectName(QStringLiteral("label_d1_h12"));
        label_d1_h12->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d1_h12, 2, 13, 1, 1);

        label_d1_h13 = new clickable_QLabel(uploadTimesDialog);
        label_d1_h13->setObjectName(QStringLiteral("label_d1_h13"));
        label_d1_h13->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d1_h13, 2, 14, 1, 1);

        label_d1_h21 = new clickable_QLabel(uploadTimesDialog);
        label_d1_h21->setObjectName(QStringLiteral("label_d1_h21"));
        label_d1_h21->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d1_h21, 2, 22, 1, 1);

        label_d3_h19 = new clickable_QLabel(uploadTimesDialog);
        label_d3_h19->setObjectName(QStringLiteral("label_d3_h19"));
        label_d3_h19->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d3_h19, 4, 20, 1, 1);

        label_d3_h20 = new clickable_QLabel(uploadTimesDialog);
        label_d3_h20->setObjectName(QStringLiteral("label_d3_h20"));
        label_d3_h20->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d3_h20, 4, 21, 1, 1);

        label_d3_h21 = new clickable_QLabel(uploadTimesDialog);
        label_d3_h21->setObjectName(QStringLiteral("label_d3_h21"));
        label_d3_h21->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d3_h21, 4, 22, 1, 1);

        label_d3_h22 = new clickable_QLabel(uploadTimesDialog);
        label_d3_h22->setObjectName(QStringLiteral("label_d3_h22"));
        label_d3_h22->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d3_h22, 4, 23, 1, 1);

        label_d3_h23 = new clickable_QLabel(uploadTimesDialog);
        label_d3_h23->setObjectName(QStringLiteral("label_d3_h23"));
        label_d3_h23->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d3_h23, 4, 24, 1, 1);

        label_d3_h01 = new clickable_QLabel(uploadTimesDialog);
        label_d3_h01->setObjectName(QStringLiteral("label_d3_h01"));
        label_d3_h01->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d3_h01, 4, 2, 1, 1);

        label_d4_h00 = new clickable_QLabel(uploadTimesDialog);
        label_d4_h00->setObjectName(QStringLiteral("label_d4_h00"));
        label_d4_h00->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d4_h00, 5, 1, 1, 1);

        label_d4_h04 = new clickable_QLabel(uploadTimesDialog);
        label_d4_h04->setObjectName(QStringLiteral("label_d4_h04"));
        label_d4_h04->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d4_h04, 5, 5, 1, 1);

        label_d4_h06 = new clickable_QLabel(uploadTimesDialog);
        label_d4_h06->setObjectName(QStringLiteral("label_d4_h06"));
        label_d4_h06->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d4_h06, 5, 7, 1, 1);

        label_d4_h03 = new clickable_QLabel(uploadTimesDialog);
        label_d4_h03->setObjectName(QStringLiteral("label_d4_h03"));
        label_d4_h03->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d4_h03, 5, 4, 1, 1);

        label_d4_h02 = new clickable_QLabel(uploadTimesDialog);
        label_d4_h02->setObjectName(QStringLiteral("label_d4_h02"));
        label_d4_h02->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d4_h02, 5, 3, 1, 1);

        label_d5_h00 = new clickable_QLabel(uploadTimesDialog);
        label_d5_h00->setObjectName(QStringLiteral("label_d5_h00"));
        label_d5_h00->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d5_h00, 6, 1, 1, 1);

        label_d2_h00 = new clickable_QLabel(uploadTimesDialog);
        label_d2_h00->setObjectName(QStringLiteral("label_d2_h00"));
        label_d2_h00->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d2_h00, 3, 1, 1, 1);

        label_d2_h03 = new clickable_QLabel(uploadTimesDialog);
        label_d2_h03->setObjectName(QStringLiteral("label_d2_h03"));
        label_d2_h03->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d2_h03, 3, 4, 1, 1);

        label_d2_h02 = new clickable_QLabel(uploadTimesDialog);
        label_d2_h02->setObjectName(QStringLiteral("label_d2_h02"));
        label_d2_h02->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d2_h02, 3, 3, 1, 1);

        label_d2_h04 = new clickable_QLabel(uploadTimesDialog);
        label_d2_h04->setObjectName(QStringLiteral("label_d2_h04"));
        label_d2_h04->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d2_h04, 3, 5, 1, 1);

        label_d2_h05 = new clickable_QLabel(uploadTimesDialog);
        label_d2_h05->setObjectName(QStringLiteral("label_d2_h05"));
        label_d2_h05->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d2_h05, 3, 6, 1, 1);

        label_d4_h07 = new clickable_QLabel(uploadTimesDialog);
        label_d4_h07->setObjectName(QStringLiteral("label_d4_h07"));
        label_d4_h07->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d4_h07, 5, 8, 1, 1);

        label_h01 = new QLabel(uploadTimesDialog);
        label_h01->setObjectName(QStringLiteral("label_h01"));
        label_h01->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_h01, 1, 2, 1, 1);

        label_h02 = new QLabel(uploadTimesDialog);
        label_h02->setObjectName(QStringLiteral("label_h02"));
        label_h02->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_h02, 1, 3, 1, 1);

        label_h05 = new QLabel(uploadTimesDialog);
        label_h05->setObjectName(QStringLiteral("label_h05"));
        label_h05->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_h05, 1, 6, 1, 1);

        label_d1_h03 = new clickable_QLabel(uploadTimesDialog);
        label_d1_h03->setObjectName(QStringLiteral("label_d1_h03"));
        label_d1_h03->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d1_h03, 2, 4, 1, 1);

        label_d1_h08 = new clickable_QLabel(uploadTimesDialog);
        label_d1_h08->setObjectName(QStringLiteral("label_d1_h08"));
        label_d1_h08->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d1_h08, 2, 9, 1, 1);

        label_d1 = new QLabel(uploadTimesDialog);
        label_d1->setObjectName(QStringLiteral("label_d1"));
        label_d1->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout->addWidget(label_d1, 2, 0, 1, 1);

        label_h00 = new QLabel(uploadTimesDialog);
        label_h00->setObjectName(QStringLiteral("label_h00"));
        label_h00->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_h00, 1, 1, 1, 1);

        label_d1_h07 = new clickable_QLabel(uploadTimesDialog);
        label_d1_h07->setObjectName(QStringLiteral("label_d1_h07"));
        label_d1_h07->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d1_h07, 2, 8, 1, 1);

        label_d1_h01 = new clickable_QLabel(uploadTimesDialog);
        label_d1_h01->setObjectName(QStringLiteral("label_d1_h01"));
        label_d1_h01->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d1_h01, 2, 2, 1, 1);

        label_d1_h02 = new clickable_QLabel(uploadTimesDialog);
        label_d1_h02->setObjectName(QStringLiteral("label_d1_h02"));
        label_d1_h02->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d1_h02, 2, 3, 1, 1);

        label_d1_h14 = new clickable_QLabel(uploadTimesDialog);
        label_d1_h14->setObjectName(QStringLiteral("label_d1_h14"));
        label_d1_h14->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d1_h14, 2, 15, 1, 1);

        label_d1_h05 = new clickable_QLabel(uploadTimesDialog);
        label_d1_h05->setObjectName(QStringLiteral("label_d1_h05"));
        label_d1_h05->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d1_h05, 2, 6, 1, 1);

        label_d1_h06 = new clickable_QLabel(uploadTimesDialog);
        label_d1_h06->setObjectName(QStringLiteral("label_d1_h06"));
        label_d1_h06->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d1_h06, 2, 7, 1, 1);

        label_d1_h22 = new clickable_QLabel(uploadTimesDialog);
        label_d1_h22->setObjectName(QStringLiteral("label_d1_h22"));
        label_d1_h22->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d1_h22, 2, 23, 1, 1);

        label_d1_h23 = new clickable_QLabel(uploadTimesDialog);
        label_d1_h23->setObjectName(QStringLiteral("label_d1_h23"));
        label_d1_h23->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d1_h23, 2, 24, 1, 1);

        label_d3_h03 = new clickable_QLabel(uploadTimesDialog);
        label_d3_h03->setObjectName(QStringLiteral("label_d3_h03"));
        label_d3_h03->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d3_h03, 4, 4, 1, 1);

        label_d2_h06 = new clickable_QLabel(uploadTimesDialog);
        label_d2_h06->setObjectName(QStringLiteral("label_d2_h06"));
        label_d2_h06->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d2_h06, 3, 7, 1, 1);

        label_d2_h07 = new clickable_QLabel(uploadTimesDialog);
        label_d2_h07->setObjectName(QStringLiteral("label_d2_h07"));
        label_d2_h07->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d2_h07, 3, 8, 1, 1);

        label_d2_h08 = new clickable_QLabel(uploadTimesDialog);
        label_d2_h08->setObjectName(QStringLiteral("label_d2_h08"));
        label_d2_h08->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d2_h08, 3, 9, 1, 1);

        label_d2_h09 = new clickable_QLabel(uploadTimesDialog);
        label_d2_h09->setObjectName(QStringLiteral("label_d2_h09"));
        label_d2_h09->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d2_h09, 3, 10, 1, 1);

        label_d2_h10 = new clickable_QLabel(uploadTimesDialog);
        label_d2_h10->setObjectName(QStringLiteral("label_d2_h10"));
        label_d2_h10->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d2_h10, 3, 11, 1, 1);

        label_d2_h11 = new clickable_QLabel(uploadTimesDialog);
        label_d2_h11->setObjectName(QStringLiteral("label_d2_h11"));
        label_d2_h11->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d2_h11, 3, 12, 1, 1);

        label_h14 = new QLabel(uploadTimesDialog);
        label_h14->setObjectName(QStringLiteral("label_h14"));
        label_h14->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_h14, 1, 15, 1, 1);

        label_h15 = new QLabel(uploadTimesDialog);
        label_h15->setObjectName(QStringLiteral("label_h15"));
        label_h15->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_h15, 1, 16, 1, 1);

        label_h16 = new QLabel(uploadTimesDialog);
        label_h16->setObjectName(QStringLiteral("label_h16"));
        label_h16->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_h16, 1, 17, 1, 1);

        label_h17 = new QLabel(uploadTimesDialog);
        label_h17->setObjectName(QStringLiteral("label_h17"));
        label_h17->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_h17, 1, 18, 1, 1);

        label_h18 = new QLabel(uploadTimesDialog);
        label_h18->setObjectName(QStringLiteral("label_h18"));
        label_h18->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_h18, 1, 19, 1, 1);

        label_h19 = new QLabel(uploadTimesDialog);
        label_h19->setObjectName(QStringLiteral("label_h19"));
        label_h19->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_h19, 1, 20, 1, 1);

        label_h20 = new QLabel(uploadTimesDialog);
        label_h20->setObjectName(QStringLiteral("label_h20"));
        label_h20->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_h20, 1, 21, 1, 1);

        label_h21 = new QLabel(uploadTimesDialog);
        label_h21->setObjectName(QStringLiteral("label_h21"));
        label_h21->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_h21, 1, 22, 1, 1);

        label_h22 = new QLabel(uploadTimesDialog);
        label_h22->setObjectName(QStringLiteral("label_h22"));
        label_h22->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_h22, 1, 23, 1, 1);

        label_d1_h04 = new clickable_QLabel(uploadTimesDialog);
        label_d1_h04->setObjectName(QStringLiteral("label_d1_h04"));
        label_d1_h04->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d1_h04, 2, 5, 1, 1);

        label_d2 = new QLabel(uploadTimesDialog);
        label_d2->setObjectName(QStringLiteral("label_d2"));
        label_d2->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout->addWidget(label_d2, 3, 0, 1, 1);

        label_h03 = new QLabel(uploadTimesDialog);
        label_h03->setObjectName(QStringLiteral("label_h03"));
        label_h03->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_h03, 1, 4, 1, 1);

        label_d1_h00 = new clickable_QLabel(uploadTimesDialog);
        label_d1_h00->setObjectName(QStringLiteral("label_d1_h00"));
        label_d1_h00->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d1_h00, 2, 1, 1, 1);

        label_d1_h15 = new clickable_QLabel(uploadTimesDialog);
        label_d1_h15->setObjectName(QStringLiteral("label_d1_h15"));
        label_d1_h15->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d1_h15, 2, 16, 1, 1);

        label_d1_h16 = new clickable_QLabel(uploadTimesDialog);
        label_d1_h16->setObjectName(QStringLiteral("label_d1_h16"));
        label_d1_h16->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d1_h16, 2, 17, 1, 1);

        label_d1_h17 = new clickable_QLabel(uploadTimesDialog);
        label_d1_h17->setObjectName(QStringLiteral("label_d1_h17"));
        label_d1_h17->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d1_h17, 2, 18, 1, 1);

        label_d1_h19 = new clickable_QLabel(uploadTimesDialog);
        label_d1_h19->setObjectName(QStringLiteral("label_d1_h19"));
        label_d1_h19->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d1_h19, 2, 20, 1, 1);

        label_d1_h20 = new clickable_QLabel(uploadTimesDialog);
        label_d1_h20->setObjectName(QStringLiteral("label_d1_h20"));
        label_d1_h20->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d1_h20, 2, 21, 1, 1);

        label_d2_h21 = new clickable_QLabel(uploadTimesDialog);
        label_d2_h21->setObjectName(QStringLiteral("label_d2_h21"));
        label_d2_h21->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d2_h21, 3, 22, 1, 1);

        label_d3_h00 = new clickable_QLabel(uploadTimesDialog);
        label_d3_h00->setObjectName(QStringLiteral("label_d3_h00"));
        label_d3_h00->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d3_h00, 4, 1, 1, 1);

        label_d2_h22 = new clickable_QLabel(uploadTimesDialog);
        label_d2_h22->setObjectName(QStringLiteral("label_d2_h22"));
        label_d2_h22->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d2_h22, 3, 23, 1, 1);

        label_d2_h23 = new clickable_QLabel(uploadTimesDialog);
        label_d2_h23->setObjectName(QStringLiteral("label_d2_h23"));
        label_d2_h23->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d2_h23, 3, 24, 1, 1);

        label_d2_h01 = new clickable_QLabel(uploadTimesDialog);
        label_d2_h01->setObjectName(QStringLiteral("label_d2_h01"));
        label_d2_h01->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d2_h01, 3, 2, 1, 1);

        label_d3_h02 = new clickable_QLabel(uploadTimesDialog);
        label_d3_h02->setObjectName(QStringLiteral("label_d3_h02"));
        label_d3_h02->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d3_h02, 4, 3, 1, 1);

        label_d2_h14 = new clickable_QLabel(uploadTimesDialog);
        label_d2_h14->setObjectName(QStringLiteral("label_d2_h14"));
        label_d2_h14->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d2_h14, 3, 15, 1, 1);

        label_d2_h15 = new clickable_QLabel(uploadTimesDialog);
        label_d2_h15->setObjectName(QStringLiteral("label_d2_h15"));
        label_d2_h15->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d2_h15, 3, 16, 1, 1);

        label_d2_h16 = new clickable_QLabel(uploadTimesDialog);
        label_d2_h16->setObjectName(QStringLiteral("label_d2_h16"));
        label_d2_h16->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d2_h16, 3, 17, 1, 1);

        label_d2_h17 = new clickable_QLabel(uploadTimesDialog);
        label_d2_h17->setObjectName(QStringLiteral("label_d2_h17"));
        label_d2_h17->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d2_h17, 3, 18, 1, 1);

        label_d2_h18 = new clickable_QLabel(uploadTimesDialog);
        label_d2_h18->setObjectName(QStringLiteral("label_d2_h18"));
        label_d2_h18->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d2_h18, 3, 19, 1, 1);

        label_d4_h08 = new clickable_QLabel(uploadTimesDialog);
        label_d4_h08->setObjectName(QStringLiteral("label_d4_h08"));
        label_d4_h08->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d4_h08, 5, 9, 1, 1);

        label_d2_h12 = new clickable_QLabel(uploadTimesDialog);
        label_d2_h12->setObjectName(QStringLiteral("label_d2_h12"));
        label_d2_h12->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_d2_h12, 3, 13, 1, 1);

        label = new QLabel(uploadTimesDialog);
        label->setObjectName(QStringLiteral("label"));
        label->setMaximumSize(QSize(5, 16777215));

        gridLayout->addWidget(label, 1, 25, 1, 1);

        label_2 = new QLabel(uploadTimesDialog);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setMaximumSize(QSize(16777215, 5));

        gridLayout->addWidget(label_2, 9, 0, 1, 1);


        verticalLayout->addLayout(gridLayout);

        label_selectedInfo = new QLabel(uploadTimesDialog);
        label_selectedInfo->setObjectName(QStringLiteral("label_selectedInfo"));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        label_selectedInfo->setFont(font);
        label_selectedInfo->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_selectedInfo);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setContentsMargins(-1, 10, -1, 10);
        label_top_pix_1 = new QLabel(uploadTimesDialog);
        label_top_pix_1->setObjectName(QStringLiteral("label_top_pix_1"));
        label_top_pix_1->setMinimumSize(QSize(0, 150));
        label_top_pix_1->setScaledContents(false);
        label_top_pix_1->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_top_pix_1, 0, 0, 1, 1);

        label_top_pix_2 = new QLabel(uploadTimesDialog);
        label_top_pix_2->setObjectName(QStringLiteral("label_top_pix_2"));
        label_top_pix_2->setMinimumSize(QSize(0, 150));
        label_top_pix_2->setScaledContents(false);
        label_top_pix_2->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_top_pix_2, 0, 1, 1, 1);

        label_top_line1_1 = new QLabel(uploadTimesDialog);
        label_top_line1_1->setObjectName(QStringLiteral("label_top_line1_1"));
        QFont font1;
        font1.setPointSize(10);
        font1.setBold(true);
        font1.setWeight(75);
        label_top_line1_1->setFont(font1);
        label_top_line1_1->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_top_line1_1, 1, 0, 1, 1);

        label_top_pix_3 = new QLabel(uploadTimesDialog);
        label_top_pix_3->setObjectName(QStringLiteral("label_top_pix_3"));
        label_top_pix_3->setMinimumSize(QSize(0, 150));
        label_top_pix_3->setScaledContents(false);
        label_top_pix_3->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_top_pix_3, 0, 2, 1, 1);

        label_top_line2_1 = new QLabel(uploadTimesDialog);
        label_top_line2_1->setObjectName(QStringLiteral("label_top_line2_1"));
        label_top_line2_1->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_top_line2_1, 2, 0, 1, 1);

        label_top_line1_2 = new QLabel(uploadTimesDialog);
        label_top_line1_2->setObjectName(QStringLiteral("label_top_line1_2"));
        label_top_line1_2->setFont(font1);
        label_top_line1_2->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_top_line1_2, 1, 1, 1, 1);

        label_top_line2_2 = new QLabel(uploadTimesDialog);
        label_top_line2_2->setObjectName(QStringLiteral("label_top_line2_2"));
        label_top_line2_2->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_top_line2_2, 2, 1, 1, 1);

        label_top_line1_3 = new QLabel(uploadTimesDialog);
        label_top_line1_3->setObjectName(QStringLiteral("label_top_line1_3"));
        label_top_line1_3->setFont(font1);
        label_top_line1_3->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_top_line1_3, 1, 2, 1, 1);

        label_top_line2_3 = new QLabel(uploadTimesDialog);
        label_top_line2_3->setObjectName(QStringLiteral("label_top_line2_3"));
        label_top_line2_3->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_top_line2_3, 2, 2, 1, 1);


        verticalLayout->addLayout(gridLayout_2);

        plainTextEdit = new QPlainTextEdit(uploadTimesDialog);
        plainTextEdit->setObjectName(QStringLiteral("plainTextEdit"));

        verticalLayout->addWidget(plainTextEdit);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        pushButton_close = new QPushButton(uploadTimesDialog);
        pushButton_close->setObjectName(QStringLiteral("pushButton_close"));

        horizontalLayout_2->addWidget(pushButton_close);


        verticalLayout->addLayout(horizontalLayout_2);


        retranslateUi(uploadTimesDialog);

        QMetaObject::connectSlotsByName(uploadTimesDialog);
    } // setupUi

    void retranslateUi(QDialog *uploadTimesDialog)
    {
        uploadTimesDialog->setWindowTitle(QApplication::translate("uploadTimesDialog", "Dialog", 0));
        comboBox_site->clear();
        comboBox_site->insertItems(0, QStringList()
         << QApplication::translate("uploadTimesDialog", "500PX", 0)
         << QApplication::translate("uploadTimesDialog", "DeviantArt", 0)
         << QApplication::translate("uploadTimesDialog", "Flickr", 0)
         << QApplication::translate("uploadTimesDialog", "Facebook", 0)
         << QApplication::translate("uploadTimesDialog", "Youpic", 0)
        );
        comboBox_time->clear();
        comboBox_time->insertItems(0, QStringList()
         << QApplication::translate("uploadTimesDialog", "All Time", 0)
         << QApplication::translate("uploadTimesDialog", "This Year", 0)
         << QApplication::translate("uploadTimesDialog", "Previous Year", 0)
        );
        pushButton_get->setText(QApplication::translate("uploadTimesDialog", "Get", 0));
        label_d3_h04->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d4_h05->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d3_h05->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d3_h06->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d3_h07->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d3_h08->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d3_h09->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d3_h10->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d3_h11->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d3_h12->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d3_h13->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d3_h14->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d3_h15->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d3_h16->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d3_h17->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d3_h18->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d3->setText(QApplication::translate("uploadTimesDialog", "Wednesday", 0));
        label_d4->setText(QApplication::translate("uploadTimesDialog", "Thursday", 0));
        label_d6->setText(QApplication::translate("uploadTimesDialog", "Saturday", 0));
        label_d5_h02->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d5_h03->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d5_h04->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d5_h05->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d5_h06->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d5_h07->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d5_h08->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d5_h09->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d5_h10->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d5_h11->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d7_h00->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_avg_h02->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_avg_h00->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_avg_h03->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_avg_h04->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_avg_h05->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_avg_h06->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_avg_h07->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_avg_h08->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_avg_h09->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_avg_h10->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_avg_h11->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_avg_h12->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_avg_h13->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_avg_h14->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_avg_h15->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_avg_h16->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_avg_h17->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_avg_h18->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_avg_h19->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_avg_h20->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_avg_h21->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_avg_h22->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_avg_h23->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_avg_h01->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_avg_d3->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_avg_d2->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_avg_d1->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_avg_d4->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_avg_d5->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_avg_d6->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_avg_d7->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d5_h12->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d5_h13->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d5_h14->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d5_h15->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d5_h16->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d5_h17->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d5_h18->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d5_h19->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d5_h20->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d5_h21->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d5_h22->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d5_h23->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d5_h01->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d6_h00->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d6_h02->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d6_h03->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d6_h04->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d6_h05->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d6_h06->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d6_h07->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d6_h08->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d6_h09->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d6_h10->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d6_h11->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d6_h12->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d6_h13->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d6_h14->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d6_h15->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d6_h16->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d6_h17->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d6_h18->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d6_h19->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d6_h20->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d6_h22->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d6_h01->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d7_h02->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d7_h03->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d7_h04->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d7_h05->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d7_h06->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d7_h07->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d7_h08->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d7_h09->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d7_h10->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d7_h11->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d7_h12->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d7_h13->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d7_h14->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d7_h15->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d7_h16->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d7_h17->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d7_h18->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d7_h19->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d7_h20->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d7_h21->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d7_h22->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d7_h23->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d7_h01->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d4_h09->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d4_h10->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d4_h11->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d4_h12->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d4_h13->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d4_h14->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d4_h15->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d4_h16->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d4_h17->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d4_h18->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d4_h19->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d4_h20->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d4_h21->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d6_h21->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_h04->setText(QApplication::translate("uploadTimesDialog", "4", 0));
        label_h06->setText(QApplication::translate("uploadTimesDialog", "6", 0));
        label_d2_h13->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d6_h23->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d5->setText(QApplication::translate("uploadTimesDialog", "Friday", 0));
        label_d7->setText(QApplication::translate("uploadTimesDialog", "Sunday", 0));
        label_d1_h18->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_h23->setText(QApplication::translate("uploadTimesDialog", "23", 0));
        label_h07->setText(QApplication::translate("uploadTimesDialog", "7", 0));
        label_h09->setText(QApplication::translate("uploadTimesDialog", "9", 0));
        label_h08->setText(QApplication::translate("uploadTimesDialog", "8", 0));
        label_h10->setText(QApplication::translate("uploadTimesDialog", "10", 0));
        label_h11->setText(QApplication::translate("uploadTimesDialog", "11", 0));
        label_h12->setText(QApplication::translate("uploadTimesDialog", "12", 0));
        label_h13->setText(QApplication::translate("uploadTimesDialog", "13", 0));
        label_d2_h19->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d2_h20->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d4_h22->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d4_h23->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d4_h01->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d1_h09->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d1_h10->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d1_h11->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d1_h12->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d1_h13->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d1_h21->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d3_h19->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d3_h20->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d3_h21->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d3_h22->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d3_h23->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d3_h01->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d4_h00->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d4_h04->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d4_h06->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d4_h03->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d4_h02->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d5_h00->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d2_h00->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d2_h03->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d2_h02->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d2_h04->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d2_h05->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d4_h07->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_h01->setText(QApplication::translate("uploadTimesDialog", "1", 0));
        label_h02->setText(QApplication::translate("uploadTimesDialog", "2", 0));
        label_h05->setText(QApplication::translate("uploadTimesDialog", "5", 0));
        label_d1_h03->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d1_h08->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d1->setText(QApplication::translate("uploadTimesDialog", "Monday", 0));
        label_h00->setText(QApplication::translate("uploadTimesDialog", "0", 0));
        label_d1_h07->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d1_h01->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d1_h02->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d1_h14->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d1_h05->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d1_h06->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d1_h22->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d1_h23->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d3_h03->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d2_h06->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d2_h07->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d2_h08->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d2_h09->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d2_h10->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d2_h11->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_h14->setText(QApplication::translate("uploadTimesDialog", "14", 0));
        label_h15->setText(QApplication::translate("uploadTimesDialog", "15", 0));
        label_h16->setText(QApplication::translate("uploadTimesDialog", "16", 0));
        label_h17->setText(QApplication::translate("uploadTimesDialog", "17", 0));
        label_h18->setText(QApplication::translate("uploadTimesDialog", "18", 0));
        label_h19->setText(QApplication::translate("uploadTimesDialog", "19", 0));
        label_h20->setText(QApplication::translate("uploadTimesDialog", "20", 0));
        label_h21->setText(QApplication::translate("uploadTimesDialog", "21", 0));
        label_h22->setText(QApplication::translate("uploadTimesDialog", "22", 0));
        label_d1_h04->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d2->setText(QApplication::translate("uploadTimesDialog", "Tuesday", 0));
        label_h03->setText(QApplication::translate("uploadTimesDialog", "3", 0));
        label_d1_h00->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d1_h15->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d1_h16->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d1_h17->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d1_h19->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d1_h20->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d2_h21->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d3_h00->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d2_h22->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d2_h23->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d2_h01->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d3_h02->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d2_h14->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d2_h15->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d2_h16->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d2_h17->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d2_h18->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d4_h08->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label_d2_h12->setText(QApplication::translate("uploadTimesDialog", "x", 0));
        label->setText(QString());
        label_2->setText(QString());
        label_selectedInfo->setText(QApplication::translate("uploadTimesDialog", "TextLabel", 0));
        label_top_pix_1->setText(QString());
        label_top_pix_2->setText(QString());
        label_top_line1_1->setText(QString());
        label_top_pix_3->setText(QString());
        label_top_line2_1->setText(QString());
        label_top_line1_2->setText(QString());
        label_top_line2_2->setText(QString());
        label_top_line1_3->setText(QString());
        label_top_line2_3->setText(QString());
        pushButton_close->setText(QApplication::translate("uploadTimesDialog", "Close", 0));
    } // retranslateUi

};

namespace Ui {
    class uploadTimesDialog: public Ui_uploadTimesDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UPLOADTIMESDIALOG_H
