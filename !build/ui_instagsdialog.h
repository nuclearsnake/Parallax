/********************************************************************************
** Form generated from reading UI file 'instagsdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_INSTAGSDIALOG_H
#define UI_INSTAGSDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_instagsDialog
{
public:
    QVBoxLayout *verticalLayout;
    QPlainTextEdit *plainTextEdit;

    void setupUi(QDialog *instagsDialog)
    {
        if (instagsDialog->objectName().isEmpty())
            instagsDialog->setObjectName(QStringLiteral("instagsDialog"));
        instagsDialog->resize(959, 624);
        verticalLayout = new QVBoxLayout(instagsDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        plainTextEdit = new QPlainTextEdit(instagsDialog);
        plainTextEdit->setObjectName(QStringLiteral("plainTextEdit"));

        verticalLayout->addWidget(plainTextEdit);


        retranslateUi(instagsDialog);

        QMetaObject::connectSlotsByName(instagsDialog);
    } // setupUi

    void retranslateUi(QDialog *instagsDialog)
    {
        instagsDialog->setWindowTitle(QApplication::translate("instagsDialog", "Dialog", 0));
    } // retranslateUi

};

namespace Ui {
    class instagsDialog: public Ui_instagsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_INSTAGSDIALOG_H
