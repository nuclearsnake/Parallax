/********************************************************************************
** Form generated from reading UI file 'addprocessdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADDPROCESSDIALOG_H
#define UI_ADDPROCESSDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_addProcessDialog
{
public:
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QLabel *label_2;
    QCheckBox *checkBox_panoMode;
    QLineEdit *lineEdit_path;
    QLabel *label;
    QLabel *label_3;
    QLineEdit *lineEdit_name;
    QPushButton *pushButton_folderSelect;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_add;
    QPushButton *pushButton_cancel;

    void setupUi(QWidget *addProcessDialog)
    {
        if (addProcessDialog->objectName().isEmpty())
            addProcessDialog->setObjectName(QStringLiteral("addProcessDialog"));
        addProcessDialog->resize(434, 163);
        verticalLayout = new QVBoxLayout(addProcessDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label_2 = new QLabel(addProcessDialog);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        checkBox_panoMode = new QCheckBox(addProcessDialog);
        checkBox_panoMode->setObjectName(QStringLiteral("checkBox_panoMode"));

        gridLayout->addWidget(checkBox_panoMode, 2, 1, 1, 1);

        lineEdit_path = new QLineEdit(addProcessDialog);
        lineEdit_path->setObjectName(QStringLiteral("lineEdit_path"));

        gridLayout->addWidget(lineEdit_path, 0, 1, 1, 1);

        label = new QLabel(addProcessDialog);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        label_3 = new QLabel(addProcessDialog);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        lineEdit_name = new QLineEdit(addProcessDialog);
        lineEdit_name->setObjectName(QStringLiteral("lineEdit_name"));

        gridLayout->addWidget(lineEdit_name, 1, 1, 1, 1);

        pushButton_folderSelect = new QPushButton(addProcessDialog);
        pushButton_folderSelect->setObjectName(QStringLiteral("pushButton_folderSelect"));

        gridLayout->addWidget(pushButton_folderSelect, 0, 2, 1, 1);


        verticalLayout->addLayout(gridLayout);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton_add = new QPushButton(addProcessDialog);
        pushButton_add->setObjectName(QStringLiteral("pushButton_add"));
        pushButton_add->setMinimumSize(QSize(100, 25));

        horizontalLayout->addWidget(pushButton_add);

        pushButton_cancel = new QPushButton(addProcessDialog);
        pushButton_cancel->setObjectName(QStringLiteral("pushButton_cancel"));
        pushButton_cancel->setMinimumSize(QSize(100, 25));

        horizontalLayout->addWidget(pushButton_cancel);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(addProcessDialog);

        QMetaObject::connectSlotsByName(addProcessDialog);
    } // setupUi

    void retranslateUi(QWidget *addProcessDialog)
    {
        addProcessDialog->setWindowTitle(QApplication::translate("addProcessDialog", "Form", 0));
        label_2->setText(QApplication::translate("addProcessDialog", "name", 0));
        checkBox_panoMode->setText(QString());
        label->setText(QApplication::translate("addProcessDialog", "path", 0));
        label_3->setText(QApplication::translate("addProcessDialog", "panorama mode", 0));
        pushButton_folderSelect->setText(QApplication::translate("addProcessDialog", ">>", 0));
        pushButton_add->setText(QApplication::translate("addProcessDialog", "Add", 0));
        pushButton_cancel->setText(QApplication::translate("addProcessDialog", "Cancel", 0));
    } // retranslateUi

};

namespace Ui {
    class addProcessDialog: public Ui_addProcessDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADDPROCESSDIALOG_H
