/********************************************************************************
** Form generated from reading UI file 'compresswizarddialog.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_COMPRESSWIZARDDIALOG_H
#define UI_COMPRESSWIZARDDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_compressWizardDialog
{
public:
    QVBoxLayout *verticalLayout;
    QStackedWidget *stackedWidget;
    QWidget *page;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label;
    QComboBox *comboBox_folderSelect;
    QLabel *label_noFiles;
    QTableView *tableView_filesSelect;
    QFrame *frame_selectButtons;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *pushButton_filesSelectAll;
    QPushButton *pushButton_filesDeselectAll;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label_totalBytes_2;
    QWidget *page_2;
    QVBoxLayout *verticalLayout_3;
    QGridLayout *gridLayout;
    QLabel *label_4;
    QLabel *label_3;
    QLabel *label_5;
    QLabel *label_2;
    QLabel *label_6;
    QCheckBox *checkBox;
    QLabel *label_9;
    QLabel *label_10;
    QLabel *label_8;
    QSlider *horizontalSlider;
    QLabel *label_selectedFilesCount;
    QSpacerItem *horizontalSpacer_3;
    QLabel *label_7;
    QLabel *label_totalBytes;
    QSpacerItem *verticalSpacer;
    QWidget *page_3;
    QVBoxLayout *verticalLayout_4;
    QGridLayout *gridLayout_2;
    QLabel *label_13;
    QLabel *label_12;
    QLabel *label_14;
    QLabel *label_11;
    QLabel *label_file;
    QLabel *label_from;
    QLabel *label_to;
    QProgressBar *progressBar;
    QPlainTextEdit *log;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_Prev;
    QPushButton *pushButton_Next;
    QPushButton *pushButton_Cancel;

    void setupUi(QDialog *compressWizardDialog)
    {
        if (compressWizardDialog->objectName().isEmpty())
            compressWizardDialog->setObjectName(QStringLiteral("compressWizardDialog"));
        compressWizardDialog->resize(747, 463);
        verticalLayout = new QVBoxLayout(compressWizardDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        stackedWidget = new QStackedWidget(compressWizardDialog);
        stackedWidget->setObjectName(QStringLiteral("stackedWidget"));
        page = new QWidget();
        page->setObjectName(QStringLiteral("page"));
        verticalLayout_2 = new QVBoxLayout(page);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        label = new QLabel(page);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout_2->addWidget(label);

        comboBox_folderSelect = new QComboBox(page);
        comboBox_folderSelect->setObjectName(QStringLiteral("comboBox_folderSelect"));

        horizontalLayout_2->addWidget(comboBox_folderSelect);

        horizontalLayout_2->setStretch(1, 1);

        verticalLayout_2->addLayout(horizontalLayout_2);

        label_noFiles = new QLabel(page);
        label_noFiles->setObjectName(QStringLiteral("label_noFiles"));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        label_noFiles->setFont(font);
        label_noFiles->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_noFiles);

        tableView_filesSelect = new QTableView(page);
        tableView_filesSelect->setObjectName(QStringLiteral("tableView_filesSelect"));

        verticalLayout_2->addWidget(tableView_filesSelect);

        frame_selectButtons = new QFrame(page);
        frame_selectButtons->setObjectName(QStringLiteral("frame_selectButtons"));
        frame_selectButtons->setFrameShape(QFrame::StyledPanel);
        frame_selectButtons->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame_selectButtons);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        pushButton_filesSelectAll = new QPushButton(frame_selectButtons);
        pushButton_filesSelectAll->setObjectName(QStringLiteral("pushButton_filesSelectAll"));
        pushButton_filesSelectAll->setMinimumSize(QSize(80, 0));

        horizontalLayout_3->addWidget(pushButton_filesSelectAll);

        pushButton_filesDeselectAll = new QPushButton(frame_selectButtons);
        pushButton_filesDeselectAll->setObjectName(QStringLiteral("pushButton_filesDeselectAll"));
        pushButton_filesDeselectAll->setMinimumSize(QSize(80, 0));

        horizontalLayout_3->addWidget(pushButton_filesDeselectAll);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);

        label_totalBytes_2 = new QLabel(frame_selectButtons);
        label_totalBytes_2->setObjectName(QStringLiteral("label_totalBytes_2"));

        horizontalLayout_3->addWidget(label_totalBytes_2);


        verticalLayout_2->addWidget(frame_selectButtons);

        stackedWidget->addWidget(page);
        page_2 = new QWidget();
        page_2->setObjectName(QStringLiteral("page_2"));
        verticalLayout_3 = new QVBoxLayout(page_2);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label_4 = new QLabel(page_2);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout->addWidget(label_4, 3, 0, 1, 1);

        label_3 = new QLabel(page_2);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        label_5 = new QLabel(page_2);
        label_5->setObjectName(QStringLiteral("label_5"));
        QFont font1;
        font1.setBold(true);
        font1.setWeight(75);
        label_5->setFont(font1);

        gridLayout->addWidget(label_5, 2, 1, 1, 1);

        label_2 = new QLabel(page_2);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 0, 0, 1, 1);

        label_6 = new QLabel(page_2);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setFont(font1);

        gridLayout->addWidget(label_6, 3, 1, 1, 1);

        checkBox = new QCheckBox(page_2);
        checkBox->setObjectName(QStringLiteral("checkBox"));
        checkBox->setChecked(true);
        checkBox->setTristate(false);

        gridLayout->addWidget(checkBox, 5, 1, 1, 1);

        label_9 = new QLabel(page_2);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setFont(font1);

        gridLayout->addWidget(label_9, 4, 2, 1, 1);

        label_10 = new QLabel(page_2);
        label_10->setObjectName(QStringLiteral("label_10"));

        gridLayout->addWidget(label_10, 5, 0, 1, 1);

        label_8 = new QLabel(page_2);
        label_8->setObjectName(QStringLiteral("label_8"));

        gridLayout->addWidget(label_8, 4, 0, 1, 1);

        horizontalSlider = new QSlider(page_2);
        horizontalSlider->setObjectName(QStringLiteral("horizontalSlider"));
        horizontalSlider->setMaximumSize(QSize(200, 16777215));
        horizontalSlider->setMaximum(100);
        horizontalSlider->setValue(95);
        horizontalSlider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(horizontalSlider, 4, 1, 1, 1);

        label_selectedFilesCount = new QLabel(page_2);
        label_selectedFilesCount->setObjectName(QStringLiteral("label_selectedFilesCount"));
        label_selectedFilesCount->setFont(font1);

        gridLayout->addWidget(label_selectedFilesCount, 0, 1, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_3, 2, 2, 1, 1);

        label_7 = new QLabel(page_2);
        label_7->setObjectName(QStringLiteral("label_7"));

        gridLayout->addWidget(label_7, 1, 0, 1, 1);

        label_totalBytes = new QLabel(page_2);
        label_totalBytes->setObjectName(QStringLiteral("label_totalBytes"));
        label_totalBytes->setFont(font1);

        gridLayout->addWidget(label_totalBytes, 1, 1, 1, 1);

        gridLayout->setColumnStretch(1, 1);

        verticalLayout_3->addLayout(gridLayout);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer);

        stackedWidget->addWidget(page_2);
        page_3 = new QWidget();
        page_3->setObjectName(QStringLiteral("page_3"));
        verticalLayout_4 = new QVBoxLayout(page_3);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        label_13 = new QLabel(page_3);
        label_13->setObjectName(QStringLiteral("label_13"));

        gridLayout_2->addWidget(label_13, 2, 0, 1, 1);

        label_12 = new QLabel(page_3);
        label_12->setObjectName(QStringLiteral("label_12"));

        gridLayout_2->addWidget(label_12, 1, 0, 1, 1);

        label_14 = new QLabel(page_3);
        label_14->setObjectName(QStringLiteral("label_14"));

        gridLayout_2->addWidget(label_14, 3, 0, 1, 1);

        label_11 = new QLabel(page_3);
        label_11->setObjectName(QStringLiteral("label_11"));

        gridLayout_2->addWidget(label_11, 0, 0, 1, 1);

        label_file = new QLabel(page_3);
        label_file->setObjectName(QStringLiteral("label_file"));
        label_file->setFont(font1);

        gridLayout_2->addWidget(label_file, 0, 1, 1, 1);

        label_from = new QLabel(page_3);
        label_from->setObjectName(QStringLiteral("label_from"));
        label_from->setFont(font1);

        gridLayout_2->addWidget(label_from, 1, 1, 1, 1);

        label_to = new QLabel(page_3);
        label_to->setObjectName(QStringLiteral("label_to"));
        label_to->setFont(font1);

        gridLayout_2->addWidget(label_to, 2, 1, 1, 1);

        progressBar = new QProgressBar(page_3);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setFont(font1);
        progressBar->setValue(50);
        progressBar->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(progressBar, 3, 1, 1, 1);

        gridLayout_2->setColumnStretch(1, 1);

        verticalLayout_4->addLayout(gridLayout_2);

        log = new QPlainTextEdit(page_3);
        log->setObjectName(QStringLiteral("log"));

        verticalLayout_4->addWidget(log);

        stackedWidget->addWidget(page_3);

        verticalLayout->addWidget(stackedWidget);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton_Prev = new QPushButton(compressWizardDialog);
        pushButton_Prev->setObjectName(QStringLiteral("pushButton_Prev"));
        pushButton_Prev->setMinimumSize(QSize(100, 25));

        horizontalLayout->addWidget(pushButton_Prev);

        pushButton_Next = new QPushButton(compressWizardDialog);
        pushButton_Next->setObjectName(QStringLiteral("pushButton_Next"));
        pushButton_Next->setMinimumSize(QSize(100, 25));

        horizontalLayout->addWidget(pushButton_Next);

        pushButton_Cancel = new QPushButton(compressWizardDialog);
        pushButton_Cancel->setObjectName(QStringLiteral("pushButton_Cancel"));
        pushButton_Cancel->setMinimumSize(QSize(100, 25));

        horizontalLayout->addWidget(pushButton_Cancel);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(compressWizardDialog);

        stackedWidget->setCurrentIndex(2);


        QMetaObject::connectSlotsByName(compressWizardDialog);
    } // setupUi

    void retranslateUi(QDialog *compressWizardDialog)
    {
        compressWizardDialog->setWindowTitle(QApplication::translate("compressWizardDialog", "Dialog", 0));
        label->setText(QApplication::translate("compressWizardDialog", "Select folder:", 0));
        label_noFiles->setText(QApplication::translate("compressWizardDialog", "No files!", 0));
        pushButton_filesSelectAll->setText(QApplication::translate("compressWizardDialog", "Select all", 0));
        pushButton_filesDeselectAll->setText(QApplication::translate("compressWizardDialog", "Deselect all", 0));
        label_totalBytes_2->setText(QString());
        label_4->setText(QApplication::translate("compressWizardDialog", "Convert to:", 0));
        label_3->setText(QApplication::translate("compressWizardDialog", "Convert from:", 0));
        label_5->setText(QApplication::translate("compressWizardDialog", "TIFF", 0));
        label_2->setText(QApplication::translate("compressWizardDialog", "Selected files:", 0));
        label_6->setText(QApplication::translate("compressWizardDialog", "JPG", 0));
        checkBox->setText(QString());
        label_9->setText(QApplication::translate("compressWizardDialog", "95", 0));
        label_10->setText(QApplication::translate("compressWizardDialog", "Delete original files:", 0));
        label_8->setText(QApplication::translate("compressWizardDialog", "Compression:", 0));
        label_selectedFilesCount->setText(QApplication::translate("compressWizardDialog", "#", 0));
        label_7->setText(QApplication::translate("compressWizardDialog", "Total bytes:", 0));
        label_totalBytes->setText(QApplication::translate("compressWizardDialog", "#", 0));
        label_13->setText(QApplication::translate("compressWizardDialog", "To:", 0));
        label_12->setText(QApplication::translate("compressWizardDialog", "From:", 0));
        label_14->setText(QApplication::translate("compressWizardDialog", "Progress:", 0));
        label_11->setText(QApplication::translate("compressWizardDialog", "File:", 0));
        label_file->setText(QApplication::translate("compressWizardDialog", "#", 0));
        label_from->setText(QApplication::translate("compressWizardDialog", "#", 0));
        label_to->setText(QApplication::translate("compressWizardDialog", "#", 0));
        pushButton_Prev->setText(QApplication::translate("compressWizardDialog", "<< Prev", 0));
        pushButton_Next->setText(QApplication::translate("compressWizardDialog", "Next >>", 0));
        pushButton_Cancel->setText(QApplication::translate("compressWizardDialog", "Cancel", 0));
    } // retranslateUi

};

namespace Ui {
    class compressWizardDialog: public Ui_compressWizardDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_COMPRESSWIZARDDIALOG_H
