/********************************************************************************
** Form generated from reading UI file 'tablebrowserdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TABLEBROWSERDIALOG_H
#define UI_TABLEBROWSERDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_tableBrowserDialog
{
public:
    QVBoxLayout *verticalLayout;
    QTableView *tableView;
    QProgressBar *progressBar;
    QLabel *label_info;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_close;

    void setupUi(QDialog *tableBrowserDialog)
    {
        if (tableBrowserDialog->objectName().isEmpty())
            tableBrowserDialog->setObjectName(QStringLiteral("tableBrowserDialog"));
        tableBrowserDialog->resize(1041, 686);
        verticalLayout = new QVBoxLayout(tableBrowserDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        tableView = new QTableView(tableBrowserDialog);
        tableView->setObjectName(QStringLiteral("tableView"));
        tableView->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
        tableView->setAlternatingRowColors(true);
        tableView->setSelectionBehavior(QAbstractItemView::SelectRows);

        verticalLayout->addWidget(tableView);

        progressBar = new QProgressBar(tableBrowserDialog);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setMaximumSize(QSize(16777215, 18));
        progressBar->setBaseSize(QSize(0, 0));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        progressBar->setFont(font);
        progressBar->setValue(0);
        progressBar->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(progressBar);

        label_info = new QLabel(tableBrowserDialog);
        label_info->setObjectName(QStringLiteral("label_info"));

        verticalLayout->addWidget(label_info);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton_close = new QPushButton(tableBrowserDialog);
        pushButton_close->setObjectName(QStringLiteral("pushButton_close"));
        pushButton_close->setMinimumSize(QSize(100, 25));

        horizontalLayout->addWidget(pushButton_close);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(tableBrowserDialog);

        QMetaObject::connectSlotsByName(tableBrowserDialog);
    } // setupUi

    void retranslateUi(QDialog *tableBrowserDialog)
    {
        tableBrowserDialog->setWindowTitle(QApplication::translate("tableBrowserDialog", "Dialog", 0));
        label_info->setText(QString());
        pushButton_close->setText(QApplication::translate("tableBrowserDialog", "Close", 0));
    } // retranslateUi

};

namespace Ui {
    class tableBrowserDialog: public Ui_tableBrowserDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TABLEBROWSERDIALOG_H
