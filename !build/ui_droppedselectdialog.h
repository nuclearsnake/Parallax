/********************************************************************************
** Form generated from reading UI file 'droppedselectdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DROPPEDSELECTDIALOG_H
#define UI_DROPPEDSELECTDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_droppedSelectDialog
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QRadioButton *radioButton_1;
    QRadioButton *radioButton_2;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_Accept;
    QPushButton *pushButton_Cancel;

    void setupUi(QDialog *droppedSelectDialog)
    {
        if (droppedSelectDialog->objectName().isEmpty())
            droppedSelectDialog->setObjectName(QStringLiteral("droppedSelectDialog"));
        droppedSelectDialog->resize(232, 116);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(droppedSelectDialog->sizePolicy().hasHeightForWidth());
        droppedSelectDialog->setSizePolicy(sizePolicy);
        verticalLayout = new QVBoxLayout(droppedSelectDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label = new QLabel(droppedSelectDialog);
        label->setObjectName(QStringLiteral("label"));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);

        verticalLayout->addWidget(label);

        radioButton_1 = new QRadioButton(droppedSelectDialog);
        radioButton_1->setObjectName(QStringLiteral("radioButton_1"));
        QIcon icon;
        icon.addFile(QStringLiteral(":/icons/icons/rotator_1.png"), QSize(), QIcon::Normal, QIcon::Off);
        radioButton_1->setIcon(icon);
        radioButton_1->setChecked(true);

        verticalLayout->addWidget(radioButton_1);

        radioButton_2 = new QRadioButton(droppedSelectDialog);
        radioButton_2->setObjectName(QStringLiteral("radioButton_2"));
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/icons/icons/archive.png"), QSize(), QIcon::Normal, QIcon::Off);
        radioButton_2->setIcon(icon1);

        verticalLayout->addWidget(radioButton_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(0, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton_Accept = new QPushButton(droppedSelectDialog);
        pushButton_Accept->setObjectName(QStringLiteral("pushButton_Accept"));
        pushButton_Accept->setMinimumSize(QSize(100, 25));

        horizontalLayout->addWidget(pushButton_Accept);

        pushButton_Cancel = new QPushButton(droppedSelectDialog);
        pushButton_Cancel->setObjectName(QStringLiteral("pushButton_Cancel"));
        pushButton_Cancel->setMinimumSize(QSize(100, 25));

        horizontalLayout->addWidget(pushButton_Cancel);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(droppedSelectDialog);

        QMetaObject::connectSlotsByName(droppedSelectDialog);
    } // setupUi

    void retranslateUi(QDialog *droppedSelectDialog)
    {
        droppedSelectDialog->setWindowTitle(QApplication::translate("droppedSelectDialog", "Dialog", 0));
        label->setText(QApplication::translate("droppedSelectDialog", "TextLabel", 0));
        radioButton_1->setText(QApplication::translate("droppedSelectDialog", "New process", 0));
        radioButton_2->setText(QApplication::translate("droppedSelectDialog", "New archive", 0));
        pushButton_Accept->setText(QApplication::translate("droppedSelectDialog", "Accept", 0));
        pushButton_Cancel->setText(QApplication::translate("droppedSelectDialog", "Cancel", 0));
    } // retranslateUi

};

namespace Ui {
    class droppedSelectDialog: public Ui_droppedSelectDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DROPPEDSELECTDIALOG_H
