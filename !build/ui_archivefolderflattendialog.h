/********************************************************************************
** Form generated from reading UI file 'archivefolderflattendialog.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ARCHIVEFOLDERFLATTENDIALOG_H
#define UI_ARCHIVEFOLDERFLATTENDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_archiveFolderFlattenDialog
{
public:
    QVBoxLayout *verticalLayout;
    QPlainTextEdit *plainTextEdit_log;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_start;
    QPushButton *pushButton_close;

    void setupUi(QDialog *archiveFolderFlattenDialog)
    {
        if (archiveFolderFlattenDialog->objectName().isEmpty())
            archiveFolderFlattenDialog->setObjectName(QStringLiteral("archiveFolderFlattenDialog"));
        archiveFolderFlattenDialog->resize(1003, 368);
        verticalLayout = new QVBoxLayout(archiveFolderFlattenDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        plainTextEdit_log = new QPlainTextEdit(archiveFolderFlattenDialog);
        plainTextEdit_log->setObjectName(QStringLiteral("plainTextEdit_log"));

        verticalLayout->addWidget(plainTextEdit_log);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(-1, 0, -1, -1);
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton_start = new QPushButton(archiveFolderFlattenDialog);
        pushButton_start->setObjectName(QStringLiteral("pushButton_start"));

        horizontalLayout->addWidget(pushButton_start);

        pushButton_close = new QPushButton(archiveFolderFlattenDialog);
        pushButton_close->setObjectName(QStringLiteral("pushButton_close"));

        horizontalLayout->addWidget(pushButton_close);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(archiveFolderFlattenDialog);

        QMetaObject::connectSlotsByName(archiveFolderFlattenDialog);
    } // setupUi

    void retranslateUi(QDialog *archiveFolderFlattenDialog)
    {
        archiveFolderFlattenDialog->setWindowTitle(QApplication::translate("archiveFolderFlattenDialog", "Dialog", 0));
        pushButton_start->setText(QApplication::translate("archiveFolderFlattenDialog", "Start", 0));
        pushButton_close->setText(QApplication::translate("archiveFolderFlattenDialog", "Close", 0));
    } // retranslateUi

};

namespace Ui {
    class archiveFolderFlattenDialog: public Ui_archiveFolderFlattenDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ARCHIVEFOLDERFLATTENDIALOG_H
